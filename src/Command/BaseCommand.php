<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class BaseCommand extends Command
{
    protected $stdOutput;

    protected $exitAfterCurrentItem;

    protected array $errors = [];

    public function signal_handler($signal): void
    {
        $signalType = match ($signal) {
            SIGTERM => 'SIGTERM',
            SIGKILL => 'SIGKILL',
            SIGINT => 'SIGINT',
            SIGHUP => 'SIGHUP',
            default => 'UNKNOWN_SIGNAL',
        };

        $this->output();
        $this->output(0, 'Caught signal [' . $signalType . ']. Finishing processing...');
        $this->exitAfterCurrentItem = true;
        $this->output();
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->stdOutput = $output;
    }

    protected function output($indentationLevel = 0, $message = null, $withNewLine = true)
    {
        if ($message === null) {
            $message = '';
        }

        if ($withNewLine) {
            $this->stdOutput->writeLn(str_repeat(' ', $indentationLevel + 1) . $message);
        } else {
            $this->stdOutput->write(str_repeat(' ', $indentationLevel + 1) . $message);
        }
    }

    protected function tick()
    {
        $this->output(-1, '<info>✔</info>');
    }

    protected function cross()
    {
        $this->output(-1, '<fg=red>x</fg=red>');
    }

    protected function error(string $message, $exit = false)
    {
        $this->output(0);
        $this->output(0, '<error>' . $message . '</error>');
        $this->output(0);

        if ($exit) {
            $this->outputErrors();
            exit;
        }

        $this->errors[] = $message;
    }

    protected function outputErrors()
    {
        if (count($this->errors) > 0) {
            $this->output();
            $this->output(0, 'Errors:');
            $this->output();

            foreach ($this->errors as $error) {
                $this->output(0, $error);
            }

            $this->output();
        }
    }

    protected function setUpSignalHandlers($callable = 'signal_handler')
    {
        $this->output(0, 'Setting up signal handlers.');

        declare(ticks=1);
        pcntl_signal(SIGTERM, [$this, $callable]);
        pcntl_signal(SIGINT, [$this, $callable]);
    }
}
