<?php

namespace App\Command;

use App\Entity\Repository\PurchaseOrderRepository;
use App\Time\TimeService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ArchivePurchaseOrdersCommand extends BaseCommand
{
    protected static $defaultDescription = 'Archives any completed Purchaser Orders.';

    public function __construct(protected TimeService $timeService, protected PurchaseOrderRepository $purchaseOrderRepo, protected EntityManagerInterface $em, ?string $name = null)
    {
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setName('v42:purchaseOrders:archive')
            ->setHelp(
                <<<'EOT'
                    The <info>%command.name%</info> command finds any Purchase Orders which have been completed within the past week and archives them.
                    EOT,
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->output();
        $this->output(0, 'Fleetmanager Purchase Order Archiver');

        $this->output();
        $this->output(2, 'Getting Purchase Orders for archiving...', false);
        $this->tick();

        $now         = $this->timeService->getNewDateTime();

        $purchaseOrders = $this->purchaseOrderRepo->getPurchaseOrdersToBeArchived($now);

        if (!$purchaseOrders) {
            $this->output();
            $this->output(0, 'No Purchase Orders to archive.');
            $this->output();
            $this->output(0, 'Exiting.');

            return (int) Command::SUCCESS;
        }

        foreach ($purchaseOrders as $purchaseOrder) {
            $purchaseOrder->setArchived(true);
            $purchaseOrder->setReinstated(false);
            $this->output(4, 'Archiving Purchase Order with ID "' . $purchaseOrder->getId() . '".', false);

            $this->em->persist($purchaseOrder);
            $this->tick();
        }

        $this->em->flush();

        $this->output();
        $this->output(0, 'No more Purchase Orders to archive.');
        $this->output();
        $this->output(0, 'Exiting.');

        return (int) Command::SUCCESS;
    }
}
