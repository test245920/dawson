<?php

namespace App\Command;

use App\Entity\Repository\AssetLifecyclePeriodRepository;
use App\Manager\AssetManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AssetLifecyclePeriodCleanupCommand extends BaseCommand
{
    protected static $defaultDescription = 'Command to cleanup lifecycles of assets.';

    public function __construct(
        protected EntityManagerInterface $em,
        protected AssetLifecyclePeriodRepository $assetLifecyclePeriodRepository,
        protected AssetManager $assetManager,
        ?string $name = null
    ) {
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setName('v42:assets:cleanupLifecycles')
            ->setHelp(
                <<<'EOT'
                    The <info>%command.name%</info> command cleanup lifecycles of assets.
                    EOT,
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $periods = $this->assetLifecyclePeriodRepository->getExpiredPeriods();

        if (!sizeof($periods)) {
            $this->output();
            $this->output(0, 'No assets to process today.');
            $this->output();
            $this->output(0, 'Exiting.');
        } else {
            $this->output();
            $this->output(0, 'Today ' . sizeof($periods) . ' assets to process.');
        }

        foreach ($periods as $period) {
            $asset = $period->getAsset();

            $this->output(4, 'Returning asset "' . $asset->getNameString() . '"...', false);

            $asset->setCurrentLifecyclePeriod(null);
            $this->assetManager->ensureCurrentLifecyclePeriod($asset);
            $asset->getCurrentLifecyclePeriod()->setStart($period->getEnd());

            $this->em->persist($asset);

            $period->setScheduledLifecyclePeriodCleanup(false);
            $this->em->persist($period);

            $this->tick();
        }

        $this->em->flush();

        $this->output();
        $this->output(0, 'No more assets.');
        $this->output();
        $this->output(0, 'Exiting.');

        return (int) Command::SUCCESS;
    }
}
