<?php

namespace App\Command;

use App\Entity\Repository\JobRepository;
use App\Manager\JobManager;
use App\Model\JobStage;
use App\Time\TimeService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AutoCompleteAndCreateJobCommand extends BaseCommand
{
    protected static $defaultName = 'v42:job-recurring:complete-create';

    protected static $defaultDescription = 'Complete and create recurring jobs.';

    public function __construct(protected JobRepository $jobRepository, protected JobManager $jobManager, protected TimeService $timeService, protected EntityManagerInterface $em, ?string $name = null)
    {
        parent::__construct($name);
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $now = $this->timeService->getNewDateTime();
        $jobRecurring = $this->jobRepository->getJobRecurringEnded();

        foreach ($jobRecurring as $job) {
            $workLogs = $job->getWorkLogs();
            $lastWorkLog = end($workLogs);
            if ($workLogs && $lastWorkLog) {
                $now = $lastWorkLog->getStartDate();
            }
            $job->setCompleted(true);
            $job->setCompletedAt($now);
            $this->jobManager->saveJob($job);

            $duplicateFrom = $job->getId() ? $this->jobRepository->find($job->getId()) : null;

            if ($duplicateFrom) {
                $job = clone $job;
                $job->setJobCode(null);
                $job->setCompletionStageOverridden(true);
                $job->setCompletionStage(JobStage::LIVE);
                $job->setIsLive(true);
                $job->setCompleted(false);

                $this->jobManager->saveJob($job, true, false, true);
                $this->em->flush();
            }
        }

        return (int) Command::SUCCESS;
    }

    protected function configure()
    {
    }
}
