<?php

namespace App\Command;

use App\Entity\AssetTransaction;
use App\Entity\Repository\AssetRepository;
use App\Model\TransactionCategory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MonthlyTruckMaintenanceCommand extends BaseCommand
{
    protected static $defaultDescription = 'Totals the amount of maintenance revenue per asset.';

    protected $jobManager;

    public function __construct(protected EntityManagerInterface $em, protected AssetRepository $assetRepository, ?string $name = null)
    {
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setName('v42:assets:maintenanceTotal')
            ->setHelp(
                <<<'EOT'
                    The <info>%command.name%</info> command maintains a cumulative total of maintenance revenue brought in by each asset.
                    It does so by checking to see if a month has passed since it's last revenue payment.
                    Payment is added on the date of the month the contract was initialised or on the 28th if the day falls on 28/29/30/31.
                    EOT,
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->output();
        $this->output(0, 'Fleetmanager asset maintenance totaller');

        $this->output();
        $this->output(2, 'Getting trucks for updating...', false);
        $this->tick();

        $date = new \DateTime();
        $day = $date->format('d');

        if ($day >= 28) {
            $day = 28;
        }

        $assets = $this->assetRepository->getAssetsWithHireDate($day);

        if (!$assets) {
            $this->output();
            $this->output(0, 'No assets to process today.');
            $this->output();
            $this->output(0, 'Exiting.');
        }

        foreach ($assets as $asset) {
            $lifecyclePeriod = $asset->getCurrentLifecyclePeriod();

            if (!$lifecyclePeriod) {
                continue;
            }

            $rate = $lifecyclePeriod->getMaintenanceRateMonthly();

            $this->output(4, 'Adding £"' . $rate . '" for asset "' . $asset->getNameString() . '".', false);
            $transaction = new AssetTransaction();
            $transaction->setAssetLifecyclePeriod($lifecyclePeriod);
            $transaction->setAmount($rate);
            $transaction->setTransactionCategory(TransactionCategory::IN_TRUCK_MAINTENANCE_REVENUE);

            $this->em->persist($transaction);
            $this->tick();
        }

        $this->em->flush();

        $this->output();
        $this->output(0, 'No more assets.');
        $this->output();
        $this->output(0, 'Exiting.');

        return (int) Command::SUCCESS;
    }
}
