<?php

namespace App\Command;

use App\Entity\Repository\LeadRepository;
use App\Entity\Repository\ReminderRepository;
use App\Manager\LeadManager;
use App\Time\TimeService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AssessFutureTargetsCommand extends BaseCommand
{
    protected static $defaultDescription = 'Checks for elapsed reminders in future targets.';

    public function __construct(
        protected LeadManager $leadManager,
        protected TimeService $timeService,
        protected ReminderRepository $reminderRepository,
        protected LeadRepository $leadRepository,
        ?string $name = null
    ) {
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setName('v42:leads:assessFutureTargets')
            ->setHelp(
                <<<'EOT'
                    The <info>%command.name%</info> Future targets in Fleetmanager are checked to see if all reminders have elapsed.
                    If they have elapsed then the Future target lead is chnaged to a Primary Target lead.
                    EOT,
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->output();
        $this->output(0, 'Fleetmanager Future Target Checker');

        $this->output();
        $this->output(2, 'Getting leads for assessment...', false);
        $this->tick();

        $now       = $this->timeService->getNewDateTime();
        $reminders = $this->reminderRepository->getFutureTargets(null, $now);

        if (sizeof($reminders) < 1) {
            $this->output();
            $this->output(0, 'No Reminders set for Future Targets.');
            $this->output();
        }

        $leads = $this->getReminderLessFutureTaregts($reminders);

        if ((is_countable($leads) ? count($leads) : 0) < 1) {
            $this->output(4, 'No Future Targets with elapsed reminders.');
        } else {
            foreach ($leads as $lead) {
                $customerName = $lead->getCustomer() ? $lead->getCustomer()->getName() : 'No customer name set';

                $this->output(4, 'Changing Future Target for "' . $customerName . '" to a Primary Target.', false);
                $this->changeLeadToPrimaryTarget($lead);
                $this->tick();
            }
        }

        $this->output();
        $this->output(0, 'No more Future Targets.');
        $this->output();

        return (int) Command::SUCCESS;
    }

    private function changeLeadToPrimaryTarget(\App\Entity\Lead $lead): bool
    {
        $lead->setFutureTarget(false);
        $lead->setPrimaryTarget(true);
        $lead->setCompletionStage(3);
        $lead->setStatus('Action required');

        $this->leadManager->saveLead($lead);

        return true;
    }

    private function getReminderLessFutureTaregts($reminders)
    {
        $thisMonth     = $this->timeService->getNewDateTime('last day of this month', null);
        $nextSixMonths = $this->timeService->getNewDateTime('+6 months', null);
        $thisYear      = $this->timeService->getNewDateTime('+1 year', null);

        $sortedFutureTargets = ['thisMonth'     => [], 'nextSixMonths' => [], 'thisYear'      => [], 'longRange'     => [], 'noReminder'    => []];

        $leads = [];

        foreach ($reminders as $reminder) {
            $reminderDate = $reminder['date'];
            if ($reminderDate <= $thisMonth) {
                $sortedFutureTargets['thisMonth'][] = $reminder;
            } elseif ($reminderDate <= $nextSixMonths) {
                $sortedFutureTargets['nextSixMonths'][] = $reminder;
            } elseif ($reminderDate <= $thisYear) {
                $sortedFutureTargets['thisYear'][] = $reminder;
            } else {
                $sortedFutureTargets['longRange'][] = $reminder;
            }

            $leads[$reminder['lead']['id']] = true;
        }

        $leadIdsAlreadyDisplayed           = array_keys($leads);
        $futureTargetsWithElapsedReminders = $this->leadRepository->getLeadObjectsNotInListWithReminders(null, $leadIdsAlreadyDisplayed);

        return $futureTargetsWithElapsedReminders;
    }
}
