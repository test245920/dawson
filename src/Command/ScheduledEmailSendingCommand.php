<?php

namespace App\Command;

use App\Entity\Repository\ReminderRepository;
use App\Manager\EmailManager;
use App\Time\TimeService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Twig\Environment;

class ScheduledEmailSendingCommand extends BaseCommand
{
    protected static $defaultDescription = 'Schedules emails that should be sent on a given date.';

    public function __construct(protected EntityManagerInterface $em, private readonly TimeService $timeService, protected EmailManager $emailManager, protected Environment $templating, private readonly ReminderRepository $reminderRepository, ?string $name = null)
    {
        parent::__construct($name);
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $reminderEmailsToSend = $this->getReminderEmailsThatNeedSent();

        if ((is_countable($reminderEmailsToSend) ? count($reminderEmailsToSend) : 0) < 1) {
            $this->output(4, 'No reminder emails need to be sent.');
        } else {
            foreach ($reminderEmailsToSend as $reminderEmail) {
                $reminder = $this->reminderRepository->findOneBy(['id' => $reminderEmail['id']]);

                $subject = 'Reminder: ' . $reminderEmail['subject'];
                $body    = $this->templating->render('Reminder/_reminderEmail.html.twig', ['reminder' => $reminder, 'username' => $reminder->getLead()->getUser()->getDisplayName()]);

                if ($reminderEmail['leadOwner'] != $reminderEmail['reminderRecipient']) {
                    $this->output(4, 'Sending reminder email to lead owner...', false);
                    $this->emailManager->sendEmail($subject, $body, $reminderEmail['leadOwner'], true);
                    $this->tick();

                    $body = $this->templating->render('Reminder/_reminderEmail.html.twig', ['reminder' => $reminder, 'username' => $reminder->getUser()->getDisplayName()]);

                    $this->output(4, 'Sending reminder email to reminder recipient...', false);
                    $this->emailManager->sendEmail($subject, $body, $reminderEmail['reminderRecipient'], true);
                    $this->tick();
                } else {
                    $this->output(4, 'Sending reminder email to lead owner...', false);
                    $this->emailManager->sendEmail($subject, $body, $reminderEmail['leadOwner'], true);
                    $this->tick();
                }

                $reminder->setNotified(true);
                $this->em->persist($reminder);
            }

            $this->em->flush();
        }

        return (int) Command::SUCCESS;
    }

    protected function configure()
    {
        $this
            ->setName('v42:email:schedule')
            ->setHelp(
                <<<'EOT'
                    The <info>%command.name%</info> command schedules emails that should be sent on a given date, eg, staff reminders, customer alerts.
                    EOT,
            );
    }

    protected function getReminderEmailsThatNeedSent()
    {
        $this->output();
        $this->output(0, 'Fleetmanager Email Auto Scheduler');

        $this->output();
        $this->output(2, 'Getting reminder emails for sending...', false);
        $this->tick();

        $today       = $this->timeService->getNewDateTime('today midnight');
        $tomorrow    = $this->timeService->getNewDateTime('tomorrow midnight');

        return $this->reminderRepository->findEmailsThatNeedSent($today, $tomorrow);
    }
}
