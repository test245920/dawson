<?php

namespace App\Command;

use App\Entity\Repository\SupplierRepository;
use App\Entity\Repository\SupplierStockRepository;
use App\Entity\Stock;
use App\Entity\Supplier;
use App\Entity\SupplierStock;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\File\File;

class UpdatePartsListCommand extends BaseCommand
{
    protected static $defaultDescription = 'Update a supplier\'s parts list.';

    protected Supplier $supplier;

    protected $supplierId;

    public function __construct(protected EntityManagerInterface $em, private readonly SupplierRepository $supplierRepository, private readonly SupplierStockRepository $supplierStockRepository, ?string $name = null)
    {
        parent::__construct($name);
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $filename = $input->getArgument('filename');

        if (!$filename) {
            $this->error('Please supply a file.', true);
        }

        $this->supplierId = $input->getArgument('supplier-id');

        if (!$this->supplierId) {
            $this->error('Please supply a supplier ID.', true);
        }

        $this->supplier = $this->supplierRepository->find($this->supplierId);

        if (!$this->supplier) {
            $this->error('Could not find a Supplier with ID "' . $this->supplierId . '".', true);
        }

        $this->output();
        $this->output(2, 'V42 Dawson Forklift Services Supplier Parts list updater');

        $this->output();
        $this->output(2, '- Updating supplier parts from "' . $filename . '".');

        $this->addBatchFromFile($filename);

        $this->output();
        $this->output(2, '- Updating complete.');
        $this->output();

        $this->outputErrors();

        return (int) Command::SUCCESS;
    }

    public function addBatchFromFile($filename): bool
    {
        $rowNumber = 1;
        $addedRows = 0;
        $updatedRows = 0;
        $errorRows = 0;
        $file = new File(realpath($filename));
        $openFile = $file->openFile();

        $createdAt = new \DateTime();
        $importIdPrefix = $createdAt->format('YmdHis-');

        $batchSize = 0;

        /*
         *  CSV Format:
         *
         *      0 = code
         *      1 = description
         *      3 = cost price
         */

        while (!$openFile->eof()) {
            $csvRow = $openFile->fgetcsv();

            if ($rowNumber == 1) {
                ++$rowNumber;

                continue;
            }

            $importId = $importIdPrefix . $rowNumber;

            if ($csvRow[0] && $csvRow[1] && $csvRow[2]) {
                /** @var SupplierStock $supplierStock */
                $supplierStock = $this->supplierStockRepository->findOneBy(['code' => $csvRow[0]]);

                if ($supplierStock) {
                    $this->output();
                    $this->output(4, '- Updating batch of supplier stock...', false);

                    $supplierStock->setDescription($csvRow[1]);
                    $supplierStock->setCostPrice(floatval($csvRow[2]));

                    ++$updatedRows;
                } else {
                    $this->output();
                    $this->output(4, '- Inserting stock...', false);

                    $stock = new Stock();

                    $stock->setCode($csvRow[0]);
                    $stock->setDescription($csvRow[1]);
                    $stock->setPreferredSupplier($this->supplier);
                    $stock->setImportId($importId);

                    $this->em->persist($stock);

                    $this->output();
                    $this->output(4, '- Inserting supplier stock...', false);

                    $supplierStock = new SupplierStock();

                    $supplierStock->setCode($csvRow[0]);
                    $supplierStock->setDescription($csvRow[1]);
                    $supplierStock->setCostPrice(floatval($csvRow[2]));
                    $supplierStock->setStock($stock);
                    $supplierStock->setSupplier($this->supplier);

                    ++$addedRows;
                }

                $this->em->persist($supplierStock);

                ++$batchSize;

                if ($batchSize % 10000 == 0) {
                    $this->em->flush();
                    $batchSize = 0;

                    $this->em->clear();
                    $this->supplier = $this->supplierRepository->find($this->supplierId);
                }

                $this->tick();
                ++$rowNumber;
            } else {
                ++$errorRows;
            }
        }

        if ($batchSize) {
            $this->em->flush();
        }

        $this->output();
        if ($addedRows > 0) {
            $this->output(2, '- Success, ' . $addedRows . ' parts were successfully added.');
        }
        if ($updatedRows > 0) {
            $this->output(2, '- Success, ' . $updatedRows . ' parts were successfully updated.');
        }
        if ($errorRows > 0) {
            $this->output(2, '- Success, ' . $errorRows . ' error rows.');
        }

        return true;
    }

    protected function configure()
    {
        $this
            ->setName('v42:update:partsList')
            ->setHelp(
                <<<'EOT'
                    The <info>%command.name%</info> command update the list of parts a supplier provides.
                    EOT,
            )
            ->addArgument(
                'supplier-id',
                null,
                InputOption::VALUE_REQUIRED,
                'The corressponding ID in the database for the supplier.',
            )
            ->addArgument(
                'filename',
                null,
                InputOption::VALUE_REQUIRED,
                'The path to the file name you wish to import.',
            );
    }
}
