<?php

namespace App\Command;

use App\Entity\Repository\SupplierRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\File\File;

class ImportPartsListCommand extends BaseCommand
{
    protected static $defaultDescription = 'Import a supplier\'s parts list.';

    protected $supplier;

    public function __construct(protected SupplierRepository $supplierRepository, protected EntityManagerInterface $em, ?string $name = null)
    {
        parent::__construct($name);
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $filename = $input->getArgument('filename');

        if (!$filename) {
            $this->error('Please supply a file.', true);
        }

        $supplierId = $input->getArgument('supplier-id');

        if (!$supplierId) {
            $this->error('Please supply a supplier ID.', true);
        }

        $this->supplier = $this->supplierRepository->find($supplierId);

        if (!$this->supplier) {
            $this->error('Could not find a Supplier with ID "' . $supplierId . '".', true);
        }

        $this->output();
        $this->output(2, 'V42 Dawson Forklift Services Supplier Parts list importer');

        $this->output();
        $this->output(2, '- Importing supplier parts from "' . $filename . '".');

        $this->addBatchFromFile($filename);

        $this->output();
        $this->output(2, '- Importing complete.');
        $this->output();

        $this->outputErrors();

        return (int) Command::SUCCESS;
    }

    public function addBatchFromFile($filename): bool
    {
        set_time_limit(600);

        $rowNumber      = 1;
        $headers        = [];
        $blankRows      = [];
        $incompleteRows = [];
        $addedRows      = 0;
        $file           = new File(realpath($filename));
        $openFile       = $file->openFile();
        $counter        = 0;
        $values         = [];
        $ssValues       = [];
        $sql            = 'INSERT INTO `stock` (`code`, `description`, `prev_code`, `oem_price`, `createdAt`, `import_id`, `last_updated`) VALUES %s;';
        $supplierStockSql = 'INSERT INTO `supplier_stock` (`supplier_id`, `stock_id`, `cost_price`, `issue_price`, `createdAt`, `code`, `description`) VALUES %s;';

        $db = $this->em->getConnection();

        $createdAt = new \DateTime();
        $createdAtStr = $createdAt->format('Y-m-d H:i:s');
        $supplierId = $this->supplier->getId();

        $importIdPrefix = $createdAt->format('YmdHis-');

        /*
         *  CSV Format:
         *
         *      0 = code
         *      1 = description
         *      2 = previous code
         *      3 = OEM price
         *      4 = cost price
         *      5 = issue price
         */

        while (!$openFile->eof()) {
            $csvRow = $openFile->fgetcsv();

            if (!$csvRow || empty($csvRow) || ($csvRow[0] == '' && count($csvRow) > 1) || $csvRow[0] === null) {
                $blankRows[] = $rowNumber;

                continue;
            }

            if ($rowNumber == 1) {
                $this->headers = $csvRow;
                ++$rowNumber;

                continue;
            }

            $importId = $importIdPrefix . $rowNumber;

            if ($counter % 1000 == 0 && $counter != 0) {
                $this->output();
                $this->output(4, '- Inserting batch of stock...', false);

                $command = sprintf($sql, implode(',', $values));
                $stmt    = $db->prepare($command);
                $stmt->execute();
                $command = null;
                $values = [];

                $this->tick();
                $this->output(4, '- Inserting batch of supplier stock...', false);

                $ssCommand = sprintf($supplierStockSql, implode(',', $ssValues));
                $ssStmt    = $db->prepare($ssCommand);
                $ssStmt->execute();
                $ssCommand = null;
                $ssValues = [];
                $this->tick();
            }

            $values[] = sprintf(
                '(%s, %s, %s, %.10f, %s, %s, %s)',
                $db->quote($csvRow[0]),
                $db->quote($csvRow[1]),
                empty($csvRow[2]) ? 'null' : $db->quote($csvRow[2]),
                $csvRow[3],
                $db->quote($createdAtStr),
                $db->quote($importId),
                $db->quote($createdAtStr),
            );

            $ssValues[] = sprintf(
                '(%d, (SELECT id FROM stock WHERE import_id=%s), %.10f, %.10f, %s, %s, %s)',
                $supplierId,
                $db->quote($importId),
                $csvRow[4],
                $csvRow[5],
                $db->quote($createdAtStr),
                $db->quote($csvRow[0]),
                $db->quote($csvRow[1]),
            );

            ++$addedRows;
            ++$rowNumber;
            ++$counter;
        }

        if (!empty($values)) {
            $this->output();
            $this->output(4, '- Inserting batch of stock...', false);

            $command = sprintf($sql, implode(',', $values));
            $stmt    = $db->prepare($command);
            $stmt->execute();

            $this->tick();
            $this->output(4, '- Inserting batch of supplier stock...', false);

            $ssCommand = sprintf($supplierStockSql, implode(',', $ssValues));
            $ssStmt    = $db->prepare($ssCommand);
            $ssStmt->execute();
            $ssValues = null;
            $ssValues = [];
            $this->tick();
        }

        if (count($incompleteRows) > 0) {
            $this->output(2, '- The following row numbers were incomplete and not added: ' . implode(', ', array_keys($incompleteRows)) . '.');
        }

        $this->output();
        $this->output(2, '- Success, ' . $addedRows . ' parts were successfully added.');

        return true;
    }

    protected function configure()
    {
        $this
            ->setName('v42:import:partsList')
            ->setHelp(
                <<<'EOT'
                    The <info>%command.name%</info> command imports the list of parts a supplier provides.
                    EOT,
            )
            ->addArgument(
                'supplier-id',
                null,
                InputOption::VALUE_REQUIRED,
                'The corressponding ID in the database for the supplier.',
            )
            ->addArgument(
                'filename',
                null,
                InputOption::VALUE_REQUIRED,
                'The path to the file name you wish to import.',
            );
    }
}
