<?php

namespace App\Command;

use App\Entity\Contact;
use App\Entity\Repository\ContactRepository;
use App\Entity\Repository\CustomerRepository;
use App\Entity\Repository\SiteAddressRepository;
use App\Entity\Repository\TruckMakeRepository;
use App\Entity\Repository\TruckModelRepository;
use App\Entity\SiteAddress;
use App\Entity\TruckMake;
use App\Entity\TruckModel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\File\File;

class ImportTrucksCommand extends BaseCommand
{
    protected static $defaultDescription = 'Import a list of a customer\'s trucks.';

    protected $customer;

    public function __construct(protected EntityManagerInterface $em, protected CustomerRepository $customerRepository, protected ContactRepository $contactRepository, protected SiteAddressRepository $siteAddressRepository, protected TruckModelRepository $truckModelRepository, protected TruckMakeRepository $truckMakeRepository, ?string $name = null)
    {
        parent::__construct($name);
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $filename = $input->getArgument('filename');

        if (!$filename) {
            $this->cross();
            $this->output(0, 'Please supply a file.');
            $this->output(0, 'Exiting.');
            exit;
        }

        $customerId = $input->getArgument('customer-id');

        if (!$customerId) {
            $this->cross();
            $this->output(0, 'Please supply a customer ID.');
            $this->output(0, 'Exiting.');
            exit;
        }

        $this->customer = $this->customerRepository->find($customerId);
        if (!$this->customer) {
            throw new \Exception('Could not find a Customer with ID "' . $customerId . '" in the database.');
        }

        $this->output();
        $this->output(0, 'V42 Dawson Forklift Services Customer Trucks importer');
        $this->output(4, 'Importing customer trucks...', false);
        $this->tick();

        $this->addBatchFromFile($filename);
        $this->output(0, 'Importing complete.');

        return (int) Command::SUCCESS;
    }

    public function addBatchFromFile($filename): bool
    {
        set_time_limit(600);

        $rowNumber      = 1;
        $headers        = [];
        $blankRows      = [];
        $incompleteRows = [];
        $addedRows      = 0;
        $file           = new File(realpath($filename));
        $openFile       = $file->openFile();
        $values         = [];
        $sql            = 'INSERT INTO `truck` (`customer_id`, `model_id`, `siteAddress_id`, `serial`,`client_flt_num`,`notes`, `location`, `hire_price_monthly`, `contract_hire`, `casual_hire`, `expiry`) VALUES (%d, %d, %d, %s, %s, %s, %s, %.2f, %d, %d, %s);';
        $contactSql     = 'INSERT INTO `truck_contact` (`truck_id`, `contact_id`) VALUES (%d, %d);';

        $db = $this->em->getConnection();

        $db->beginTransaction();

        while (!$openFile->eof()) {
            $csvRow = $openFile->fgetcsv();

            if (!$csvRow || empty($csvRow) || ($csvRow[0] == '' && count($csvRow) > 1) || $csvRow[0] === null) {
                $blankRows[] = $rowNumber;

                continue;
            }

            if ($rowNumber == 1) {
                $this->headers = $csvRow;
                ++$rowNumber;

                continue;
            }

            $contactName = $csvRow[0];
            $contact     = $this->contactRepository->findOneByName($contactName);
            if (!$contact) {
                $contact = new Contact();
                $contact->setName($contactName);
                $contact->setCustomer($this->customer);
                $this->em->persist($contact);
                $this->em->flush();
            }

            $site        = $csvRow[1];
            $siteAddress = $this->siteAddressRepository->findOneByName($site);
            if (!$siteAddress) {
                $siteAddress = new SiteAddress();
                $siteAddress->setName($site);
                $siteAddress->setCustomer($this->customer);
                $this->em->persist($siteAddress);
                $this->em->flush();
            }

            $makeName = $csvRow[2];
            $make = $this->truckMakeRepository->findOneByName($makeName);
            if (!$make) {
                $make = new TruckMake();
                $make->setName($makeName);
                $this->em->persist($make);
                $this->em->flush();
            }

            $modelName = $csvRow[3];
            $model     = $this->truckModelRepository->findOneByName($modelName);
            if (!$model) {
                $model = new TruckModel();
                $model->setName($modelName);
                $model->setMake($make);
                $this->em->persist($model);
                $this->em->flush();
            }

            $serial             = $csvRow[4];
            $client_flt_num     = $csvRow[5];
            $description        = $csvRow[6];
            $location           = $csvRow[7];
            $hire_price_monthly = $csvRow[8];
            $maintenance        = $csvRow[9];
            $order_num          = $csvRow[10];
            $hire_expiry        = $csvRow[11];
            $finance_provider   = $csvRow[12];
            $agreement_number   = $csvRow[13];
            $hire_type          = $csvRow[14];

            if ($hire_type == 'contract') {
                $contract = true;
                $casual   = false;
            } elseif ($hire_type == 'extended') {
                $contract = false;
                $casual   = true;
            } else {
                $contract = false;
                $casual   = false;
            }

            if (!$hire_expiry) {
                $hire_expiry = 'NULL';
            } else {
                $timestamp = strtotime($hire_expiry);
                $hire_expiry = date('Y-m-d H:i:s', $timestamp);
            }

            if ($order_num) {
                $description = $description . ', Order num = "' . $order_num . '"';
            }

            if ($finance_provider) {
                $description = $description . ', Finance provider = "' . $finance_provider . '"';
            }

            if ($agreement_number) {
                $description = $description . ', Agreement num = "' . $agreement_number . '"';
            }

            $command = sprintf(
                $sql,
                $this->customer->getId(),
                $model->getId(),
                $siteAddress->getId(),
                $db->quote($serial),
                $db->quote($client_flt_num),
                $db->quote($description),
                $db->quote($location),
                $hire_price_monthly,
                $contract,
                $casual,
                $db->quote($hire_expiry),
            );

            $stmt = $db->prepare($command);
            $stmt->execute();

            $truckId        = $db->lastInsertId();
            $contactCommand = sprintf($contactSql, $truckId, $contact->getId());
            $contactStmt    = $db->prepare($contactCommand);
            $contactStmt->execute();
            echo '.';

            ++$addedRows;
            ++$rowNumber;
        }

        $db->commit();

        if (count($incompleteRows) > 0) {
            $this->output(4, 'The following row numbers were incomplete and not added: ' . implode(', ', array_keys($incompleteRows)) . '.');
        }

        $this->output(4, 'Success, ' . $addedRows . ' parts were successfully added.');

        return true;
    }

    protected function configure()
    {
        $this
            ->setName('v42:import:trucksList')
            ->setHelp(
                <<<'EOT'
                    The <info>%command.name%</info> command imports the list of a customer\'s trucks.
                    EOT,
            )
            ->addArgument(
                'customer-id',
                null,
                InputOption::VALUE_REQUIRED,
                'The corressponding ID in the database for the customer.',
            )
            ->addArgument(
                'filename',
                null,
                InputOption::VALUE_REQUIRED,
                'The path to the file name you wish to import.',
            );
    }
}
