<?php

namespace App\Command;

use App\Entity\Repository\JobRepository;
use App\Manager\EmailManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;

class ReSendSignatureEmailCommand extends BaseCommand
{
    protected static $defaultName = 'v42:signature:email-send';

    protected static $defaultDescription = 'Resend signature email.';

    public function __construct(protected RouterInterface $router, protected EmailManager $emailManager, protected JobRepository $jobRepository, protected EntityManagerInterface $em, protected Environment $twig, ?string $name = null)
    {
        parent::__construct($name);
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $jobSignature = $this->jobRepository->getJobSignatureEmailSend();

        foreach ($jobSignature as $job) {
            $customer = $job->getCustomer();

            $url = $this->router->generate(
                'job_signature_customer',
                ['id' => $job->getId(), 'uuid' => $customer->getUuid()],
                UrlGeneratorInterface::ABSOLUTE_URL,
            );

            $asset = $job->getAssetLifecyclePeriod();

            $contactsCustomer = $asset ? $asset->getContacts()->toArray() : null;
            if (!$contactsCustomer) {
                return Command::SUCCESS;
            }
            $sentEmail = null;

            foreach ($contactsCustomer as $contact) {
                $body = $this->twig->render(
                    'Job/_signatureEmail.html.twig',
                    ['name' => $contact->getName(), 'url' => $url, 'asset' => $asset->getAsset(), 'job' => $job],
                );

                $id = $job->getId();
                $subject = $job->getCompleted() ? 'Job Sheet Approval Request' : "Dawson Job {$id} - not yet complete";

                $sentEmail = $this->emailManager->sendEmail($subject, $body, $contact->getEmail(), true);
                $job->setSignatureEmailResent($sentEmail->getSendStatusCode());
            }

            $this->em->persist($job);
            $this->em->flush();
        }

        return (int) Command::SUCCESS;
    }

    protected function configure()
    {
    }
}
