<?php

namespace App\Model;

class RecurringType
{
    final public const WEEKLY = 0;
    final public const MONTHLY = 1;
    final public const CHOICES = [
        self::WEEKLY => 'Weekly',
        self::MONTHLY => 'Monthly',
    ];
}
