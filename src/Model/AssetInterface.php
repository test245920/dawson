<?php

namespace App\Model;

interface AssetInterface
{
    public function getNameString();

    public function getNewRoute();

    public function getEditRoute();

    public function getEditParams();
}
