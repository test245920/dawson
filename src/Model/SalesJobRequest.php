<?php

namespace App\Model;

use App\Entity\Customer;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

#[Assert\Callback(callback: 'makeModelSerialRequired')]
class SalesJobRequest
{
    public $model;

    public $clientFleet;

    #[Assert\NotNull(message: 'Required')]
    public $serial;

    public $yearOfManufacture;

    public $hourReading;

    public $purchasePrice;

    public $purchasedDate;

    public $manufacturersWarranty = Warranty::WARRANTY_NONE;

    public $manufacturersWarrantyDuration = 0;

    public $secondLevelManufacturersWarranty = Warranty::WARRANTY_NONE;

    public $secondLevelManufacturersWarrantyDuration = 0;

    public $stockValue;

    public $truckType;

    public $mastHeight;

    public $mast;

    public $mastCode;

    public $hydraulics;

    public $attachmentType;

    public $cabin;

    public $tyreType;

    public $forkDimensions;

    public $sideshift;

    public $detail;

    public $date;

    public ?Customer $customer = null;

    public $contacts;

    public $newMake;

    public $newModel;

    public function __construct()
    {
        $this->contacts = new ArrayCollection();
    }

    public function getModel()
    {
        return $this->model;
    }

    public function getSerial()
    {
        return $this->serial;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function makeModelSerialRequired(ExecutionContextInterface $context): void
    {
        $modelExist = $this->getModel() || $this->newModel;
        $noSerial = !$this->getSerial();
        $formInValid = !$modelExist || $noSerial;

        if ($formInValid) {
            $context->addViolation('A truck must have a "Make", "Model" and "Serial" to be created.');
        }
    }
}
