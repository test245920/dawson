<?php

namespace App\Model;

interface SearchableInterface
{
    public static function getSearchFields();

    public static function getSearchCategory();
}
