<?php

namespace App\Model;

class TransactionCategory
{
    final public const IN_MANUAL                         = 1;
    final public const IN_JOB                            = 2;
    final public const IN_PURCHASE_ORDER                 = 3;
    final public const OUT_MANUAL                        = 4;
    final public const OUT_JOB                           = 5;
    final public const IN_TRUCK_MAINTENANCE_REVENUE      = 6;
    final public const OUT_TRUCK_MAINTENANCE_EXPENDITURE = 7;

    private static array $categoryStrings = [
        self::IN_MANUAL                         => 'IN - Manual Entry',
        self::OUT_MANUAL                        => 'OUT - Manual Entry',
        self::IN_JOB                            => 'IN - Return from Job',
        self::OUT_JOB                           => 'OUT - Job',
        self::IN_PURCHASE_ORDER                 => 'IN - Purchase Order',
        self::IN_TRUCK_MAINTENANCE_REVENUE      => 'IN - Truck Maintenance Revenue',
        self::OUT_TRUCK_MAINTENANCE_EXPENDITURE => 'OUT - Truck Maintenance Expenditure',
    ];

    public static function getCategoryString($category)
    {
        if (!array_key_exists($category, self::$categoryStrings)) {
            return null;
        }

        return self::$categoryStrings[$category];
    }
}
