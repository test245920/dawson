<?php

namespace App\Model;

use App\Entity\Stock;
use Symfony\Component\Validator\Constraints as Assert;

class StockAdjustment
{
    #[Assert\NotNull(message: 'Required')]
    private $supplierStock;

    #[Assert\NotNull(message: 'Required')]
    private int $quantity = 1;

    #[Assert\NotNull(message: 'Required')]
    private $location;

    private $bin;

    public function __construct(#[Assert\NotNull(message: 'Required')]
        private Stock $stock)
    {
    }

    public function getSupplierStock()
    {
        return $this->supplierStock;
    }

    public function setSupplierStock($value): StockAdjustment
    {
        $this->supplierStock = $value;

        return $this;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $value): StockAdjustment
    {
        $this->quantity = $value;

        return $this;
    }

    public function getJob()
    {
        return $this->job;
    }

    public function setJob($value): StockAdjustment
    {
        $this->job = $value;

        return $this;
    }

    public function getStock(): Stock
    {
        return $this->stock;
    }

    public function setStock(Stock $value): StockAdjustment
    {
        $this->stock = $value;

        return $this;
    }

    public function getLocation()
    {
        return $this->location;
    }

    public function setLocation($value): StockAdjustment
    {
        $this->location = $value;

        return $this;
    }

    public function getBin()
    {
        return $this->bin;
    }

    public function setBin($value): StockAdjustment
    {
        $this->bin = $value;

        return $this;
    }
}
