<?php

namespace App\Model;

class WorklogRate
{
    final public const RATE_NORMAL           = '1';
    final public const RATE_OVERTIME         = '1.5';
    final public const RATE_EXTREME_OVERTIME = '2';
    final public const RATE_CHOICES = [
        self::RATE_NORMAL           => 'Normal',
        self::RATE_OVERTIME         => 'Overtime',
        self::RATE_EXTREME_OVERTIME => 'Extreme Overtime',
    ];
}
