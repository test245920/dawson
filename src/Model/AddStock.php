<?php

namespace App\Model;

use Symfony\Component\Validator\Constraints as Assert;

class AddStock
{
    #[Assert\NotNull(message: 'Required')]
    protected $preferredSupplier;

    /**
     * @var float
     */
    #[Assert\NotNull(message: 'Required')]
    protected $costPrice;

    #[Assert\NotNull(message: 'Required description')]
    protected $description;

    protected string $code;

    protected float $oemPrice;

    protected string $bin;

    /**
     * @var int
     */
    protected $minStock;

    /**
     * @var int
     */
    protected $reorderQuantity;

    /**
     * @var object
     */
    protected $supplierStock;

    /**
     * @var int
     */
    protected $carriagePrice;

    /**
     * @var int
     */
    protected $issuePrice;

    /**
     * @var string
     */
    protected $supplierNotes;

    /**
     * @var string
     */
    protected $prevCode;

    /**
     * @var object
     */
    protected $location;

    /**
     * @var int
     */
    protected $quantity;

    /**
     * @return mixed
     */
    public function getPreferredSupplier()
    {
        return $this->preferredSupplier;
    }

    /**
     * @return AddStock
     */
    public function setPreferredSupplier(mixed $preferredSupplier)
    {
        $this->preferredSupplier = $preferredSupplier;

        return $this;
    }

    /**
     * @return float
     */
    public function getCostPrice()
    {
        return $this->costPrice;
    }

    public function setCostPrice(float $costPrice): AddStock
    {
        $this->costPrice = $costPrice;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription(mixed $description): AddStock
    {
        $this->description = $description;

        return $this;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): AddStock
    {
        $this->code = $code;

        return $this;
    }

    public function getOemPrice(): float
    {
        return $this->oemPrice;
    }

    public function setOemPrice(float $oemPrice): AddStock
    {
        $this->oemPrice = $oemPrice;

        return $this;
    }

    public function getBin(): string
    {
        return $this->bin;
    }

    public function setBin(string $bin): AddStock
    {
        $this->bin = $bin;

        return $this;
    }

    /**
     * @return int
     */
    public function getMinStock()
    {
        return $this->minStock;
    }

    /**
     * @param int $minStock
     * @return AddStock
     */
    public function setMinStock($minStock)
    {
        $this->minStock = $minStock;

        return $this;
    }

    /**
     * @return int
     */
    public function getReorderQuantity()
    {
        return $this->reorderQuantity;
    }

    /**
     * @param int $reorderQuantity
     * @return AddStock
     */
    public function setReorderQuantity($reorderQuantity)
    {
        $this->reorderQuantity = $reorderQuantity;

        return $this;
    }

    /**
     * @return object
     */
    public function getSupplierStock()
    {
        return $this->supplierStock;
    }

    public function setSupplierStock(object $supplierStock): AddStock
    {
        $this->supplierStock = $supplierStock;

        return $this;
    }

    /**
     * @return int
     */
    public function getCarriagePrice()
    {
        return $this->carriagePrice;
    }

    /**
     * @param int $carriagePrice
     * @return AddStock
     */
    public function setCarriagePrice($carriagePrice)
    {
        $this->carriagePrice = $carriagePrice;

        return $this;
    }

    /**
     * @return int
     */
    public function getIssuePrice()
    {
        return $this->issuePrice;
    }

    /**
     * @param int $issuePrice
     * @return AddStock
     */
    public function setIssuePrice($issuePrice)
    {
        $this->issuePrice = $issuePrice;

        return $this;
    }

    /**
     * @return string
     */
    public function getSupplierNotes()
    {
        return $this->supplierNotes;
    }

    /**
     * @param string $supplierNotes
     * @return AddStock
     */
    public function setSupplierNotes($supplierNotes)
    {
        $this->supplierNotes = $supplierNotes;

        return $this;
    }

    /**
     * @return string
     */
    public function getPrevCode()
    {
        return $this->prevCode;
    }

    /**
     * @return $this
     */
    public function setPrevCode($prevCode)
    {
        $this->prevCode = $prevCode;

        return $this;
    }

    /**
     * @return object
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param object $location
     */
    public function setLocation($location): void
    {
        $this->location = $location;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }
}
