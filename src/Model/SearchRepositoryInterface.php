<?php

namespace App\Model;

interface SearchRepositoryInterface
{
    public function getItemsForSearch($user, $isFullAdmin);
}
