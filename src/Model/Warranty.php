<?php

namespace App\Model;

class Warranty
{
    final public const WARRANTY_NONE = 0;
    final public const WARRANTY_PARTS = 1;
    final public const WARRANTY_PARTS_LABOUR = 2;
    final public const CHOICES = [
        self::WARRANTY_NONE         => 'None',
        self::WARRANTY_PARTS        => 'Parts',
        self::WARRANTY_PARTS_LABOUR => 'Parts & Labour',
    ];
}
