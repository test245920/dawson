<?php

namespace App\Model;

class JobStage
{
    final public const AWAITING_ASSIGNMENT = 1;
    final public const LIVE                = 2;
    final public const PARTS_REQUIRED      = 3;
    final public const PARTS_ON_ORDER      = 4;
    final public const ESTIMATE_PENDING    = 5;
    final public const COMPLETE            = 6;
    final public const ESTIMATE_UNRESOLVED = 7;
    final public const SIGNED_OFF          = 8;

    public static array $stageChoices = [
        self::AWAITING_ASSIGNMENT => 'Awaiting Assignment',
        self::LIVE                => 'Live',
        self::PARTS_REQUIRED      => 'Parts Required',
        self::PARTS_ON_ORDER      => 'Parts On Order',
        self::ESTIMATE_PENDING    => 'Estimate Pending',
        self::COMPLETE            => 'Complete',
    ];

    private static array $stageStrings = [
        self::AWAITING_ASSIGNMENT => 'Awaiting Assignment',
        self::LIVE                => 'Live',
        self::PARTS_REQUIRED      => 'Parts Required',
        self::PARTS_ON_ORDER      => 'Parts On Order',
        self::ESTIMATE_PENDING    => 'Estimate Pending',
        self::COMPLETE            => 'Complete',
        self::ESTIMATE_UNRESOLVED => 'Estimate Unresolved',
        self::SIGNED_OFF          => 'Signed Off',
    ];

    public static function getStageString($category)
    {
        if (!array_key_exists($category, self::$stageStrings)) {
            return null;
        }

        return self::$stageStrings[$category];
    }
}
