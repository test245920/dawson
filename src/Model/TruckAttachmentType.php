<?php

namespace App\Model;

class TruckAttachmentType
{
    final public const TYPE_FORK_POSITIONER       = 'Fork Positioner';
    final public const TYPE_ROTATOR               = 'Rotator';
    final public const TYPE_DOUBLE_PALLET_HANDLER = 'Double Pallet Handler';
    final public const TYPE_REEL_CLAMP            = 'Reel Clamp';
    final public const TYPE_APPLIANCE_CLAMP       = 'Appliance Clamp';
    final public const TYPE_FORK_CLAMP            = 'Fork Clamp';
    final public const TYPE_BALE_CLAMP            = 'Bale Clamp';
    final public const TYPE_CARPET_BOOM           = 'Carpet Boom';
    final public const TYPES = [
        self::TYPE_FORK_POSITIONER,
        self::TYPE_ROTATOR,
        self::TYPE_DOUBLE_PALLET_HANDLER,
        self::TYPE_REEL_CLAMP,
        self::TYPE_APPLIANCE_CLAMP,
        self::TYPE_FORK_CLAMP,
        self::TYPE_BALE_CLAMP,
        self::TYPE_CARPET_BOOM,
    ];
}
