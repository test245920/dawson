<?php

namespace App\Model;

class ChargeMethod
{
    final public const INVOICE_CUSTOMER = 0;
    final public const CHARGE_AGAINST_MAINTENANCE = 1;
    final public const WARRANTY = 2;
    final public const CHOICES = [
        self::INVOICE_CUSTOMER           => 'Invoice Customer',
        self::CHARGE_AGAINST_MAINTENANCE => 'Charge against Maintenance',
        self::WARRANTY                   => 'Warranty',
    ];
}
