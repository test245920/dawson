<?php

namespace App\Model;

use App\Entity\Job;
use App\Entity\Stock;
use Symfony\Component\Validator\Constraints as Assert;

class StockAssignment
{
    #[Assert\NotNull(message: 'Required')]
    private $supplierStock;

    #[Assert\GreaterThan(value: 0, message: 'Must be greater than 0')]
    private int $quantity = 1;

    #[Assert\NotNull(message: 'Required')]
    private $location;

    public function __construct(#[Assert\NotNull(message: 'Required')]
        private Stock $stock, #[Assert\NotNull(message: 'Required')]
        private ?Job $job = null)
    {
    }

    public function getSupplierStock()
    {
        return $this->supplierStock;
    }

    public function setSupplierStock($value): StockAssignment
    {
        $this->supplierStock = $value;

        return $this;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $value): StockAssignment
    {
        $this->quantity = $value;

        return $this;
    }

    public function getJob(): ?Job
    {
        return $this->job;
    }

    public function setJob(?Job $value): StockAssignment
    {
        $this->job = $value;

        return $this;
    }

    public function getStock(): Stock
    {
        return $this->stock;
    }

    public function setStock(Stock $value): StockAssignment
    {
        $this->stock = $value;

        return $this;
    }

    public function getLocation()
    {
        return $this->location;
    }

    public function setLocation($value): StockAssignment
    {
        $this->location = $value;

        return $this;
    }
}
