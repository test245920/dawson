<?php

namespace App\Model;

use App\Entity\Stock;
use Symfony\Component\Validator\Constraints as Assert;

class StockAddition
{
    #[Assert\GreaterThan(value: 0, message: 'Must be greater than 0')]
    private int $quantity = 1;

    private $dateReceived;

    private $fromLocation;

    private $toLocation;

    public function __construct(private Stock $stock)
    {
    }

    public function getStock(): Stock
    {
        return $this->stock;
    }

    public function setStock(Stock $value): StockAddition
    {
        $this->stock = $value;

        return $this;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $value): StockAddition
    {
        $this->quantity = $value;

        return $this;
    }

    public function getDateReceived()
    {
        return $this->dateReceived;
    }

    public function setDateReceived($value): StockAddition
    {
        $this->dateReceived = $value;

        return $this;
    }

    public function getFromLocation()
    {
        return $this->fromLocation;
    }

    public function setFromLocation($value): StockAddition
    {
        $this->fromLocation = $value;

        return $this;
    }

    public function getToLocation()
    {
        return $this->toLocation;
    }

    public function setToLocation($value): StockAddition
    {
        $this->toLocation = $value;

        return $this;
    }
}
