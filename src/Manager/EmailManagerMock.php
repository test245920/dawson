<?php

namespace App\Manager;

use App\Entity\SentEmail;
use Aws\Ses\SesClient;
use Doctrine\ORM\EntityManagerInterface;

class EmailManagerMock
{
    public function __construct(private readonly string $fromAddress, private readonly string $fromName, private readonly SesClient $ses, private readonly EntityManagerInterface $em, private readonly bool $emailOverrideEnabled, private string $emailToOverride)
    {
    }

    public function sendEmail(string $subject, string $body, $toAddress, $isHtml = false): SentEmail
    {
        $finalEmail = ['Subject'  => ['Data' => $subject, 'Charset' => 'UTF-8'], 'Body' => [($isHtml ? 'Html' : 'Text') => ['Data' => $body, 'Charset' => 'UTF-8']]];
        $sentEmail  = new SentEmail();

        if ($this->emailOverrideEnabled) {
            $toAddress = $this->emailToOverride;
        }

        $toArray = is_array($toAddress) ? $toAddress : [$toAddress];
        $to      = ['ToAddresses'  => $toArray];
        $from    = $this->fromName . ' <' . $this->fromAddress . '>';

        $sentEmail->setRecipients($toArray);
        $sentEmail->setSubject($subject);
        $sentEmail->setBody($body);
        $sentEmail->setSendStatusCode(200);
        $sentEmail->setSendStatusOk(true);
        $sentEmail->setSesMessageId('test123');

        $this->em->persist($sentEmail);
        $this->em->flush();

        return $sentEmail;
    }
}
