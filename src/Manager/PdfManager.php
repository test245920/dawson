<?php

namespace App\Manager;

use Dompdf\Dompdf;

class PdfManager
{
    public function generatePdf($html): Dompdf
    {
        $pdf = new Dompdf();
        $pdf->loadHtml($html);
        $pdf->render();

        return $pdf;
    }

    public function generatePdfFile($html, $filename): bool
    {
        $pdf = $this->generatePdf($html);

        if (file_put_contents($filename, $pdf->output()) === false) {
            return false;
        }

        return true;
    }
}
