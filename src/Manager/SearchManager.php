<?php

namespace App\Manager;

use App\Entity\Contact;
use App\Entity\Customer;
use App\Entity\Estimate;
use App\Entity\Job;
use App\Entity\Lead;
use App\Entity\PurchaseOrder;
use App\Entity\Source;
use App\Entity\Supplier;
use App\Entity\Truck;
use App\Entity\TruckAttachment;
use App\Entity\TruckCharger;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class SearchManager
{
    private array $categories = ['user', 'truck', 'customer', 'contact', 'supplier', 'purchaseOrder', 'job', 'estimate', 'lead', 'source', 'truck-attachment', 'truck-charger'];

    private array $categoryRepoMap = ['user'             => User::class, 'truck'            => Truck::class, 'customer'         => Customer::class, 'contact'          => Contact::class, 'supplier'         => Supplier::class, 'purchaseOrder'    => PurchaseOrder::class, 'job'              => Job::class, 'estimate'         => Estimate::class, 'lead'             => Lead::class, 'source'           => Source::class, 'truck-attachment' => TruckAttachment::class, 'truck-charger'    => TruckCharger::class];

    private array $categoryRoles = ['user'             => 'ROLE_SUPER_ADMIN', 'truck'            => ['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER'], 'customer'         => 'ROLE_SERVICE_ADMIN', 'contact'          => 'ROLE_SERVICE_ADMIN', 'supplier'         => 'ROLE_SERVICE_ADMIN', 'purchaseOrder'    => 'ROLE_SERVICE_ADMIN', 'job'              => ['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER'], 'estimate'         => 'ROLE_SERVICE_ADMIN', 'lead'             => 'ROLE_SALES_ADMIN', 'source'           => 'ROLE_SUPER_ADMIN', 'truck-attachment' => ['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER'], 'truck-charger'    => ['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']];

    public function __construct(private readonly EntityManagerInterface $entityManager, private $securityContext)
    {
    }

    public function isCategoryValid($category): bool
    {
        if ($category === null) {
            return true; // searching all categories
        }

        return in_array($category, $this->categories);
    }

    public function getRepoForCategory(string $category)
    {
        if (!isset($this->categoryRepoMap[$category])) {
            throw new \InvalidArgumentException('SearchManager::getRepoForCategory - Invalid search category "' . $category . '" provided.');
        }

        return $this->entityManager->getRepository($this->categoryRepoMap[$category]);
    }

    public function getRequiredRoleForCategory(string $category)
    {
        if (!isset($this->categoryRoles[$category])) {
            throw new \InvalidArgumentException('SearchManager::getRequiredRoleForCategory - Invalid search category "' . $category . '" provided.');
        }

        return $this->categoryRoles[$category];
    }

    public function getRepositories($category): array
    {
        if ($category) {
            return [$this->getRepoForCategory($category)];
        }

        $repos = [];

        foreach ($this->categories as $category) {
            if ($this->canUserSearchCategory($category)) {
                $repos[] = $this->getRepoForCategory($category);
            }
        }

        return $repos;
    }

    private function canUserSearchCategory($category)
    {
        if ($category === null) {
            return true; // searching all categories
        }

        $roleRequired = $this->getRequiredRoleForCategory($category);

        return $this->securityContext->isGranted($roleRequired);
    }
}
