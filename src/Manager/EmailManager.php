<?php

namespace App\Manager;

use App\Entity\SentEmail;
use Aws\Ses\Exception\SesException;
use Aws\Ses\SesClient;
use Doctrine\ORM\EntityManagerInterface;

class EmailManager
{
    public function __construct(private readonly string $fromAddress, private readonly string $fromName, private readonly SesClient $ses, private readonly EntityManagerInterface $em, private readonly bool $emailOverrideEnabled, private ?string $emailToOverride, private $emailDomains)
    {
    }

    public function sendEmail(string $subject, string $body, $toAddress, $isHtml = false): SentEmail
    {
        $finalEmail = ['Subject'  => ['Data' => $subject, 'Charset' => 'UTF-8'], 'Body' => [($isHtml ? 'Html' : 'Text') => ['Data' => $body, 'Charset' => 'UTF-8']]];
        $sentEmail  = new SentEmail();

        if ($this->emailOverrideEnabled) {
            $toAddress = $this->emailToOverride;
        }

        $toArray = is_array($toAddress) ? $toAddress : [$toAddress];
        $validEmails = [];
        if ($this->emailDomains) {
            $emailDomains = explode(',', (string) $this->emailDomains);

            foreach ($toArray as $email) {
                $domainExp = explode('@', (string) $email);
                $domainSplit = explode('.', $domainExp[1]);
                foreach ($emailDomains as $domain) {
                    $count = count($domainSplit);
                    if (trim($domain) == $domainSplit[$count - 2] . '.' . $domainSplit[$count - 1]) {
                        $validEmails[] = $email;
                        break;
                    }
                }
            }
        }

        $to      = ['ToAddresses'  => $this->emailDomains ? $validEmails : $toArray];
        $from    = $this->fromName . ' <' . $this->fromAddress . '>';

        try {
            $response  = $this->ses->sendEmail(['Source'      => $from, 'Destination' => $to, 'Message'     => $finalEmail]);

            $messageId = $response->get('MessageId');

            $sentEmail->setSendStatusCode(200);
            $sentEmail->setSendStatusOk(true);
            $sentEmail->setSesMessageId($messageId);
        } catch (SesException $e) {
            $sentEmail->setSendStatusCode($e->getCode());
            $sentEmail->setErrorMessage($e->getMessage());
            $sentEmail->setSendStatusOk(false);
            $sentEmail->setSesMessageId(null);
        } catch (\Exception) {
            $sentEmail->setSendStatusCode(-500); // Likely a curl exception
            $sentEmail->setSendStatusOk(false);
            $sentEmail->setSesMessageId(null);
        }

        $sentEmail->setRecipients($this->emailDomains ? $validEmails : $toArray);
        $sentEmail->setSubject($subject);
        $sentEmail->setBody($body);

        $this->em->persist($sentEmail);
        $this->em->flush();

        return $sentEmail;
    }
}
