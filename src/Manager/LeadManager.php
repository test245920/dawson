<?php

namespace App\Manager;

use App\Entity\Lead;
use App\Time\TimeService;
use Doctrine\ORM\EntityManagerInterface;

class LeadManager
{
    final public const STAGE_COMPLETED       = null;
    final public const STAGE_NO_CONTACT      = 1;
    final public const STAGE_CONTACT_MADE    = 2;
    final public const STAGE_ACTION_REQUIRED = 3;
    final public const STAGE_FUTURE_TARGET   = 4;
    final public const STAGE_QUOTING         = 5;
    final public const PRIORITY_OTHER     = 0;
    final public const PRIORITY_NORMAL    = 1;
    final public const PRIORITY_IMPORTANT = 2;
    final public const PRIORITY_URGENT    = 3;
    final public const PRIORITY_NAME_OTHER     = 'other';
    final public const PRIORITY_NAME_NORMAL    = 'normal';
    final public const PRIORITY_NAME_IMPORTANT = 'important';
    final public const PRIORITY_NAME_URGENT    = 'urgent';

    public function __construct(private readonly EntityManagerInterface $em, private readonly TimeService $timeService)
    {
    }

    public function saveLead(Lead $lead, $andFlush = true): void
    {
        $completionStage = $this->getCompletionStage($lead);
        $lead->setCompletionStage($completionStage);

        $now = $this->timeService->getNewDateTime();
        $lead->setUpdatedAt($now);

        if ($andFlush) {
            $this->em->persist($lead);
            $this->em->flush();
        }
    }

    public function removeLead(Lead $lead, $andFlush = true): void
    {
        $em->remove($lead);

        if ($andFlush) {
            $this->em->flush();
        }
    }

    public function getLeadPriority(Lead $lead): array
    {
        $priority = ['priority'       => self::PRIORITY_NORMAL, 'priorityName'   => self::PRIORITY_NAME_NORMAL, 'priorityReason' => null];

        switch ($lead->getPriority()) {
            case 'Urgent':
                $priority['priority']       = self::PRIORITY_URGENT;
                $priority['priorityName']   = self::PRIORITY_NAME_URGENT;
                $priority['priorityReason'] = 'Urgent';

                break;
            case 'Important':
                $priority['priority']       = self::PRIORITY_IMPORTANT;
                $priority['priorityName']   = self::PRIORITY_NAME_IMPORTANT;
                $priority['priorityReason'] = 'Important lead';

                break;
            case 'Normal':
                $priority['priority']       = self::PRIORITY_NORMAL;
                $priority['priorityName']   = self::PRIORITY_NAME_NORMAL;
                $priority['priorityReason'] = 'Normal';

                break;

            default:
                $priority['priority']       = self::PRIORITY_NORMAL;
                $priority['priorityName']   = self::PRIORITY_NAME_NORMAL;
                $priority['priorityReason'] = 'Normal';

                break;
        }

        return $priority;
    }

    /**
     * @return mixed[]
     */
    public function getLeadPriorityForListingsLeads(array $leadSet = []): array
    {
        $priorities = [];

        foreach ($leadSet as $stage => $leads) {
            foreach ($leads as $lead) {
                $priorities[$lead[0]->getId()] = $this->getLeadPriority($lead[0]);
            }
        }

        return $priorities;
    }

    private function getCompletionStage(Lead $lead): ?int
    {
        if ($lead->getCompletionStage() == 0) {
            return self::STAGE_COMPLETED;
        }

        if ($lead->getFutureTarget()) {
            return self::STAGE_FUTURE_TARGET;
        }

        if ($lead->getStatus() == 'Quoting') {
            return self::STAGE_QUOTING;
        }

        if ($lead->getStatus() == 'Action required') {
            return self::STAGE_ACTION_REQUIRED;
        }

        if ($lead->getStatus() == 'Contact made') {
            return self::STAGE_CONTACT_MADE;
        }

        return self::STAGE_NO_CONTACT;
    }
}
