<?php

namespace App\Manager;

use App\Entity\Invoice;
use App\Entity\Job;
use App\Entity\SalesJob;
use App\Model\JobStage;
use App\Model\RecurringType;
use App\Time\TimeService;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Environment;

class JobManager
{
    final public const PRIORITY_OTHER     = 0;
    final public const PRIORITY_NORMAL    = 1;
    final public const PRIORITY_IMPORTANT = 2;
    final public const PRIORITY_URGENT    = 3;
    final public const PRIORITY_NAME_OTHER     = 'other';
    final public const PRIORITY_NAME_NORMAL    = 'normal';
    final public const PRIORITY_NAME_IMPORTANT = 'important';
    final public const PRIORITY_NAME_URGENT    = 'urgent';

    public function __construct(private EntityManagerInterface $em, private TimeService $timeService, private EmailManager $emailManager, private Environment $templating, private $notificationEmails)
    {
    }

    public function setJobNumber(Job $job): void
    {
        $id = $job->getId();
        $job->setJobCode($id);
    }

    public function postponeJob(Job $job, $andFlush = true): void
    {
        $job->setCompletionStage(JobStage::AWAITING_ASSIGNMENT);
        $job->setCompletionStageOverridden(true);

        $job->setAccepted(0);
        $job->setIsLive(0);
        $job->removeAllEngineersFromJob();

        if ($andFlush) {
            $this->em->flush();
        }
    }

    /**
     * @throws \Exception
     */
    public function saveJob(Job $job, $andFlush = true, $sendEmail = true, $autoRecreate = false): void
    {
        $isNew = !$job->getId();

        if ($job->getCompleted()) {
            $job->setCompletionStageOverridden(false);
        }

        $originalCompletionStage = $job->getCompletionStage();
        $completionStage = $this->getCompletionStage($job);

        if (!$job->getCompletionStageOverridden()) {
            $job->setCompletionStage($completionStage);
        } elseif ($originalCompletionStage == $completionStage) {
            $job->setCompletionStageOverridden(false);
        }

        if (!$job->getCompletionStageOverridden()) {
            $completionStage = $this->getCompletionStage($job);
            $job->setCompletionStage($completionStage);
        } else {
            $job->setCompletionStage($originalCompletionStage);
        }
        if ($job->getIsRecurring() && $autoRecreate) {
            $date = new \DateTime();
            $dateMonth = new \DateTime();
            $lastDayThisMonth = new \DateTime($dateMonth->format('Y-m-t'));

            if ($job->getRecurringType() === RecurringType::WEEKLY) {
                $dateModifyFrom = ($date->format('D') == 'Mon') ? $date : $date->modify('next monday');
            } else {
                $dateModifyFrom = $date->format('d') == $lastDayThisMonth->format('d') ? $dateMonth->modify(
                    'first day of next month',
                ) : $dateMonth->modify('first day of this month');
            }

            $job->setJobActiveFrom($dateModifyFrom);
            $dateRecurring = new \DateTime();

            if ($job->getRecurringType() === RecurringType::WEEKLY) {
                $dateModify = ($dateRecurring->format('D') == 'Sun') ? $dateRecurring->modify('next sunday') : $dateRecurring;
            } else {
                $dateModify = $date->format('d') == $lastDayThisMonth->format('d') ? $dateRecurring->modify(
                    'last day of next month',
                ) : $dateRecurring->modify('last day of this month');
            }

            $job->setJobRecurringTime($dateModify);
        }

        if ($andFlush) {
            $this->em->persist($job);
            $this->em->flush();
        }

        if (!$job->getJobCode()) {
            $this->setJobNumber($job);

            if ($andFlush) {
                $this->em->flush();
            }
        }

        if ($isNew && $sendEmail) {
            $this->sendNewJobEmail($job);
        }
    }

    public function removeJob(Job $job, $andFlush = true): void
    {
        $this->em->remove($job);

        if ($andFlush) {
            $this->em->flush();
        }
    }

    public function getJobPriority(Job $job): array
    {
        $priority = ['priority'       => self::PRIORITY_NORMAL, 'priorityName'   => self::PRIORITY_NAME_NORMAL, 'priorityReason' => null];

        if ($job->isUrgent()) {
            $priority['priority']       = self::PRIORITY_URGENT;
            $priority['priorityName']   = self::PRIORITY_NAME_URGENT;
            $priority['priorityReason'] = 'Urgent job';

            return $priority;
        }

        $now           = $this->timeService->getNewDateTime();
        $jobActiveFrom = $job->getJobActiveFrom();

        if ($jobActiveFrom > $now) {
            $priority['priority']       = self::PRIORITY_OTHER;
            $priority['priorityName']   = self::PRIORITY_NAME_OTHER;
            $priority['priorityReason'] = 'Scheduled future job';

            return $priority;
        }

        if ($job->getCompleted() && !$job->getSignedOff()) {
            $priority['priority']       = self::PRIORITY_URGENT;
            $priority['priorityName']   = self::PRIORITY_NAME_URGENT;
            $priority['priorityReason'] = 'Needs signed off';

            return $priority;
        }

        return $priority;
    }

    /**
     * @return mixed[]
     */
    public function getJobPriorityForListingsJobs(array $jobSet = []): array
    {
        $priorities = [];
        foreach ($jobSet as $stage => $jobs) {
            foreach ($jobs as $job) {
                $priorities[$job->getId()] = $this->getJobPriority($job);
            }
        }

        return $priorities;
    }

    /**
     * @return 'red'[]|'blue'[]|'green'[]
     */
    public function getJobSignatureForListingsJobs(array $jobSet = []): array
    {
        $signatureStatuses = [];
        foreach ($jobSet as $stage => $jobs) {
            foreach ($jobs as $job) {
                if ($job->getSignature()) {
                    $signatureStatuses[$job->getId()] = 'green';
                } elseif (!$job->getSignature() && $job->getSignatureEmailSent()) {
                    $signatureStatuses[$job->getId()] = 'blue';
                } elseif (!$job->getSignature() && !$job->getSignatureEmailSent()) {
                    $signatureStatuses[$job->getId()] = 'red';
                }
            }
        }

        return $signatureStatuses;
    }

    public function setSalesJobNumber(SalesJob $job): void
    {
        $job->setJobCode($job->getId());
        $this->em->persist($job);
        $this->em->flush();
    }

    public function saveSalesJob(SalesJob $job): void
    {
        $job->setJobCode($job->getId());
        $this->em->persist($job);
        $this->em->flush();
    }

    public function archivedJob(Invoice $invoice, \DateTime $now): void
    {
        $job = $invoice->getJob();
        $job->setArchived(true);
        $job->setArchivedAt($now);
        $this->em->persist($job);
        $this->em->flush();
    }

    private function sendNewJobEmail(Job $job): void
    {
        if (!$this->notificationEmails) {
            return;
        }

        $subject = 'Job creation notification';
        $body    = $this->templating->render('Job/_jobAlertEmail.html.twig', ['job' => $job]);

        $this->emailManager->sendEmail($subject, $body, $this->notificationEmails, true);
    }

    private function getCompletionStage(Job $job): int
    {
        $estimates = $job->getEstimates();
        if ($estimates) {
            foreach ($estimates as $estimate) {
                $pending = $estimate->getPending();
                if ($pending) {
                    return JobStage::ESTIMATE_PENDING;
                }

                $notResolved = !$estimate->getAccepted() && !$estimate->getRejected();
                if ($pending) {
                    return JobStage::ESTIMATE_UNRESOLVED;
                }
            }
        }

        if ($job->getSignedOff()) {
            return JobStage::SIGNED_OFF;
        }

        if ($job->getCompleted()) {
            return JobStage::COMPLETE;
        }

        $assigned = $job->getEngineer() !== null;

        if (!$assigned) {
            return JobStage::AWAITING_ASSIGNMENT;
        }

        if ($job->isLive()) {
            return JobStage::LIVE;
        }

        return JobStage::COMPLETE;
    }
}
