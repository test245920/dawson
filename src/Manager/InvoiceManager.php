<?php

namespace App\Manager;

use App\Entity\Invoice;
use App\Time\TimeService;
use Doctrine\ORM\EntityManagerInterface;

class InvoiceManager
{
    final public const STAGE_REQUESTED_ORDER = 0;
    final public const STAGE_NOT_SENT = 1;
    final public const STAGE_SENT     = 2;
    final public const STAGE_QUERIED  = 3;
    final public const STAGE_PAID     = 4;
    final public const PRIORITY_OTHER     = 0;
    final public const PRIORITY_NORMAL    = 1;
    final public const PRIORITY_IMPORTANT = 2;
    final public const PRIORITY_URGENT    = 3;
    final public const PRIORITY_NAME_OTHER     = 'other';
    final public const PRIORITY_NAME_NORMAL    = 'normal';
    final public const PRIORITY_NAME_IMPORTANT = 'important';
    final public const PRIORITY_NAME_URGENT    = 'urgent';

    public function __construct(private readonly EntityManagerInterface $em, private readonly TimeService $timeService)
    {
    }

    public function saveInvoice(Invoice $invoice, $andFlush = true): void
    {
        $completionStage = $this->getCompletionStage($invoice);
        $invoice->setCompletionStage($completionStage);

        $now = $this->timeService->getNewDateTime();
        $invoice->setUpdatedAt($now);

        if ($andFlush) {
            $this->em->persist($invoice);
            $this->em->flush();
        }
    }

    public function removeInvoice(Invoice $invoice, $andFlush = true): void
    {
        $this->em->remove($invoice);

        if ($andFlush) {
            $this->em->flush();
        }
    }

    public function getInvoicePriority($completionStage): array
    {
        $priority = ['priority'       => self::PRIORITY_NORMAL, 'priorityName'   => self::PRIORITY_NAME_NORMAL, 'priorityReason' => null];

        switch ($completionStage) {
            default:
                break;
        }

        return $priority;
    }

    /**
     * @return mixed[]
     */
    public function getInvoicePriorityForListingsInvoices(array $invoiceSet = []): array
    {
        $priorities = [];
        foreach ($invoiceSet as $stage => $invoices) {
            foreach ($invoices as $invoice) {
                $priorities[$invoice['id']] = $this->getInvoicePriority($invoice['completionStage']);
            }
        }

        return $priorities;
    }

    private function getCompletionStage(Invoice $invoice): int
    {
        if ($invoice->getIsPaid()) {
            return self::STAGE_PAID;
        }

        if ($invoice->getIsQueried()) {
            return self::STAGE_QUERIED;
        }

        if ($invoice->getIsSent()) {
            return self::STAGE_SENT;
        }

        if ($invoice->getIsRequestedOrder()) {
            return self::STAGE_REQUESTED_ORDER;
        }

        return self::STAGE_NOT_SENT;
    }
}
