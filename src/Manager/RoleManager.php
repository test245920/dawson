<?php

namespace App\Manager;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class RoleManager
{
    public function __construct(private $accessDecisionManager)
    {
    }

    public function doesUserHaveRole(User $user, $attributes)
    {
        $token = new UsernamePasswordToken($user, 'none', 'none', $user->getRoles());
        $attributes = is_array($attributes) ? $attributes : [$attributes];

        return $this->accessDecisionManager->decide($token, $attributes);
    }
}
