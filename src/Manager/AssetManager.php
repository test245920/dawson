<?php

namespace App\Manager;

use App\Entity\Asset;
use App\Entity\Customer;
use App\Entity\Truck;
use App\Model\SalesJobRequest;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Environment;

class AssetManager
{
    public function __construct(private EntityManagerInterface $em, private EmailManager $emailManager, private Environment $templating, private $notificationEmails)
    {
    }

    public function ensureCurrentLifecyclePeriod(Asset $asset): void
    {
        $periodCreated = $asset->ensureCurrentLifecyclePeriod();

        if ($periodCreated) {
            $defaultCustomer = $this->em->getRepository(Customer::class)->getDefaultCustomer();

            if ($defaultCustomer) {
                $asset->getCurrentLifecyclePeriod()->setCustomer($defaultCustomer);
            }
        }
    }

    public function saveAsset(Asset $asset, $andFlush = true): void
    {
        $isNew = !$asset->getId();

        if ($andFlush) {
            $this->em->persist($asset);
            $this->em->flush();
        }

        if ($isNew) {
            $this->sendNewAssetEmail($asset);
        }
    }

    public function preparedTruckAsset(Truck $truck, SalesJobRequest $salesJobRequest): Truck
    {
        $truck->setYearOfManufacture($salesJobRequest->yearOfManufacture);
        $truck->setSerial($salesJobRequest->serial);
        $truck->setHourReading($salesJobRequest->hourReading);
        $truck->setPurchasePrice($salesJobRequest->purchasePrice);
        $truck->setPurchasedDate($salesJobRequest->purchasedDate);
        $truck->setManufacturersWarranty($salesJobRequest->manufacturersWarranty);
        $truck->setManufacturersWarrantyDuration($salesJobRequest->manufacturersWarrantyDuration);
        $truck->setSecondLevelManufacturersWarranty($salesJobRequest->secondLevelManufacturersWarranty);
        $truck->setSecondLevelManufacturersWarrantyDuration($salesJobRequest->secondLevelManufacturersWarrantyDuration);
        $truck->setStockValue($salesJobRequest->stockValue);
        $truck->setTruckType($salesJobRequest->truckType);
        $truck->setMastCode($salesJobRequest->mastCode);
        $truck->setMast($salesJobRequest->mast);
        $truck->setMastHeight($salesJobRequest->mastHeight);
        $truck->setHydraulics($salesJobRequest->hydraulics);
        $truck->setAttachmentType($salesJobRequest->attachmentType);
        $truck->setCabin($salesJobRequest->cabin);
        $truck->setTyreType($salesJobRequest->tyreType);
        $truck->setForkDimensions($salesJobRequest->forkDimensions);
        $truck->setSideshift($salesJobRequest->sideshift);
        $truck->setModel($salesJobRequest->model);

        return $truck;
    }

    private function sendNewAssetEmail(Asset $asset): void
    {
        if (!$this->notificationEmails) {
            return;
        }
        if (!(is_countable($this->notificationEmails) ? count($this->notificationEmails) : 0)) {
            return;
        }

        $subject = 'Asset creation notification';
        $body    = $this->templating->render('Asset/_newAssetEmail.html.twig', ['asset' => $asset]);

        $this->emailManager->sendEmail($subject, $body, $this->notificationEmails, true);
    }
}
