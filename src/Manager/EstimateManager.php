<?php

namespace App\Manager;

use App\Entity\Estimate;
use App\Time\TimeService;
use Doctrine\ORM\EntityManagerInterface;

class EstimateManager
{
    final public const STAGE_PENDING  = 1;
    final public const STAGE_ISSUED   = 2;
    final public const STAGE_QUERIED  = 3;
    final public const STAGE_REJECTED = 4;
    final public const STAGE_APPROVED = 5;
    final public const PRIORITY_OTHER     = 0;
    final public const PRIORITY_NORMAL    = 1;
    final public const PRIORITY_IMPORTANT = 2;
    final public const PRIORITY_URGENT    = 3;
    final public const PRIORITY_NAME_OTHER     = 'other';
    final public const PRIORITY_NAME_NORMAL    = 'normal';
    final public const PRIORITY_NAME_IMPORTANT = 'important';
    final public const PRIORITY_NAME_URGENT    = 'urgent';

    public function __construct(private readonly EntityManagerInterface $em, private readonly TimeService $timeService, private readonly JobManager $jobManager)
    {
    }

    public function saveEstimate(Estimate $estimate): void
    {
        $completionStage = $this->getCompletionStage($estimate);
        $estimate->setCompletionStage($completionStage);

        $this->em->persist($estimate);
        $this->em->flush();

        if (!$estimate->getCode()) {
            $job = $estimate->getJob();

            if ($job) {
                $job->setAccepted(0);
                $job->setIsLive(0);
                $job->removeAllEngineersFromJob();

                $estimate->setCode('EJ' . $job->getId());

                $this->jobManager->saveJob($job);
            } else {
                $estimate->setCode('E' . $estimate->getId());
            }

            $this->em->flush();
        }
    }

    public function removeEstimate(Estimate $estimate, $andFlush = true): void
    {
        $em->remove($estimate);

        if ($andFlush) {
            $this->em->flush();
        }
    }

    public function getEstimatePriority(Estimate $estimate): array
    {
        $priority = ['priority'       => self::PRIORITY_NORMAL, 'priorityName'   => self::PRIORITY_NAME_NORMAL, 'priorityReason' => null];

        return $priority;
    }

    /**
     * @return mixed[]
     */
    public function getEstimatePriorityForListingsEstimates(array $estimateSet = []): array
    {
        $priorities = [];

        foreach ($estimateSet as $stage => $estimates) {
            foreach ($estimates as $estimate) {
                $priorities[$estimate->getId()] = $this->getEstimatePriority($estimate);
            }
        }

        return $priorities;
    }

    private function getCompletionStage(Estimate $estimate)
    {
        if ($estimate->getAccepted()) {
            return self::STAGE_APPROVED;
        }

        if ($estimate->getRejected()) {
            return self::STAGE_REJECTED;
        }

        if ($estimate->getQueried() && $estimate->getAwaitingResponse()) {
            return self::STAGE_QUERIED;
        }
        if ($estimate->getQueried() && !$estimate->getAwaitingResponse()) {
            return self::STAGE_ISSUED;
        }

        if ($estimate->getIssued()) {
            return self::STAGE_ISSUED;
        }

        if ($estimate->getPending()) {
            return self::STAGE_PENDING;
        }
    }
}
