<?php

namespace App\Form\DataTransformers;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class DurationToIntegerTransformer implements DataTransformerInterface
{
    /**
     * @return float|int|mixed
     */
    public function reverseTransform($values)
    {
        $hours   = $values['hour'] ?? 0;
        $minutes = $values['minute'] ?? 0;

        if (!is_int($hours) && !is_int($minutes)) {
            throw new TransformationFailedException('Expected one integer.');
        }

        return $hours * 60 + $minutes;
    }

    public function transform($duration): array
    {
        if ($duration === null) {
            $duration   = 0;
        }

        $hours   = floor($duration / 60);
        $minutes = $duration % 60;

        return ['hour' => $hours, 'minute' => $minutes];
    }
}
