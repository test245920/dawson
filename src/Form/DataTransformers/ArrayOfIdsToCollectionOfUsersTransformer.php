<?php

namespace App\Form\DataTransformers;

use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class ArrayOfIdsToCollectionOfUsersTransformer implements DataTransformerInterface
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function reverseTransform($ids): ?ArrayCollection
    {
        if (!$ids) {
            return null;
        }

        $ra = new ArrayCollection();
        foreach ($ids as $id) {
            $user = $this->em
                ->getRepository(User::class)
                ->find($id);

            if ($user === null) {
                throw new TransformationFailedException(
                    'A user with id "' . $id . '" does not exist!',
                );
            }

            $ra->add($user);
        }

        return $ra;
    }

    public function transform($users): ?array
    {
        if ($users === null) {
            return null;
        }

        $ra = [];
        foreach ($users as $user) {
            $ra[] = ['id' => $user->getId()];
        }

        return $ra;
    }
}
