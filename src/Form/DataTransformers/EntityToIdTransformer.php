<?php

namespace App\Form\DataTransformers;

use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Exception\UnexpectedTypeException;

class EntityToIdTransformer implements DataTransformerInterface
{
    private $entityClass;

    private $entityType;

    private $entityRepository;

    public function __construct(private readonly ObjectManager $em)
    {
    }

    /**
     * @return mixed|void
     */
    public function transform($entity)
    {
        if ($entity === null) {
            return;
        }

        if (!$entity instanceof $this->entityClass) {
            throw new UnexpectedTypeException($entity, $this->entityType);
        }

        return $entity->getId();
    }

    /**
     * @return mixed|object|null
     */
    public function reverseTransform($value)
    {
        if (!$value) {
            return null;
        }

        $entity = $this->em->getRepository($this->entityRepository)->find($value);

        if ($entity === null) {
            throw new TransformationFailedException(sprintf(
                'A(n) %s with id "%s" does not exist!',
                $this->entityType,
                $value,
            ));
        }

        return $entity;
    }

    public function setEntityType($entityType): void
    {
        $this->entityType = $entityType;
    }

    public function setEntityClass($entityClass): void
    {
        $this->entityClass = $entityClass;
    }

    public function setEntityRepository($entityRepository): void
    {
        $this->entityRepository = $entityRepository;
    }
}
