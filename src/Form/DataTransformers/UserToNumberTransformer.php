<?php

namespace App\Form\DataTransformers;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class UserToNumberTransformer implements DataTransformerInterface
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    /**
     * @return \App\Entity\User|mixed|object|null
     */
    public function reverseTransform($value)
    {
        if (!$value) {
            return null;
        }

        $user = $this->em
            ->getRepository(User::class)
            ->findOneBy(['id' => $value]);

        if ($user === null) {
            throw new TransformationFailedException(
                'A user with id "' . $value . '" does not exist!',
            );
        }

        return $user;
    }

    /**
     * @return mixed|null
     */
    public function transform($user)
    {
        if ($user === null) {
            return null;
        }

        return $user->getId();
    }
}
