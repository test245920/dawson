<?php

namespace App\Form\DataTransformers;

use App\Entity\SupplierStock;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class SupplierStockItemToNumberTransformer implements DataTransformerInterface
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    /**
     * @return \App\Entity\SupplierStock|mixed|object|null
     */
    public function reverseTransform($value)
    {
        if (!$value) {
            return null;
        }

        $supplierStockItem = $this->em
            ->getRepository(SupplierStock::class)
            ->findOneBy(['id' => $value]);

        if ($supplierStockItem === null) {
            throw new TransformationFailedException(
                'An supplier stock item with id "' . $value . '" does not exist!',
            );
        }

        return $supplierStockItem;
    }

    /**
     * @return mixed|null
     */
    public function transform($supplierStock)
    {
        if ($supplierStock === null) {
            return null;
        }

        return $supplierStock->getId();
    }
}
