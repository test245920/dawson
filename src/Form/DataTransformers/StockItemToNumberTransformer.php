<?php

namespace App\Form\DataTransformers;

use App\Entity\StockItem;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class StockItemToNumberTransformer implements DataTransformerInterface
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    /**
     * @return StockItem|mixed|object|null
     */
    public function reverseTransform($value)
    {
        if (!$value) {
            return null;
        }

        $stockItem = $this->em
            ->getRepository(StockItem::class)
            ->findOneBy(['id' => $value]);

        if ($stockItem === null) {
            throw new TransformationFailedException(
                'A stock item with id "' . $value . '" does not exist!',
            );
        }

        return $stockItem;
    }

    /**
     * @return mixed|null
     */
    public function transform($stockItem)
    {
        if ($stockItem === null) {
            return null;
        }

        return $stockItem->getId();
    }
}
