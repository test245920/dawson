<?php

namespace App\Form\DataTransformers;

use App\Entity\Stock;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class StockToNumberTransformer implements DataTransformerInterface
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    /**
     * @return mixed|null
     */
    public function reverseTransform($value)
    {
        if (!$value) {
            return null;
        }

        $stock = $this->em
            ->getRepository(Stock::class)
            ->findOneBy(['id' => $value]);

        if ($stock === null) {
            throw new TransformationFailedException(
                'A stock item with id "' . $value . '" does not exist!',
            );
        }

        return $stock;
    }

    /**
     * @return mixed|null
     */
    public function transform($stock)
    {
        if ($stock === null) {
            return null;
        }

        return $stock->getId();
    }
}
