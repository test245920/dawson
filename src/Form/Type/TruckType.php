<?php

namespace App\Form\Type;

use App\Entity\Truck;
use App\Entity\TruckAttachment;
use App\Entity\TruckCharger;
use App\Entity\TruckMake;
use App\Entity\TruckModel;
use App\Model\TruckAttachmentType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TruckType extends AssetType
{
    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        $truck = $builder->getData();

        $truckId = $truck->getId();
        $typeValue = null;

        if ($model = $truck->getModel()) {
            $typeValue = $model->getFuel();
        }

        $customer = $truck->getCurrentLifecyclePeriod() ? $truck->getCurrentLifecyclePeriod()->getCustomer() : null;

        $builder
            ->add('make', EntityType::class, ['class'         => TruckMake::class, 'choice_label'      => 'name', 'placeholder'   => '--', 'mapped'        => false, 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('tm')
                ->orderBy('tm.name', 'ASC'), 'attr' => ['class' => 'truck-make']])
            ->add('model', EntityType::class, ['class'         => TruckModel::class, 'choice_label'      => 'name', 'placeholder'   => '--', 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('tm')
                ->orderBy('tm.name', 'ASC'), 'attr' => ['class' => 'truck-model']])
            ->add('hourReading', TextType::class)
            ->add('hydraulics', ChoiceType::class, ['choices'         => ['1 Function Manual'    => '1 Function Manual', '2 Function Manual'    => '2 Function Manual', '3 Function Manual'    => '3 Function Manual', '4 Function Manual'    => '4 Function Manual', '5 Function Manual'    => '5 Function Manual', '1 Function Fingertip' => '1 Function Fingertip', '2 Function Fingertip' => '2 Function Fingertip', '3 Function Fingertip' => '3 Function Fingertip', '4 Function Fingertip' => '4 Function Fingertip', '5 Function Fingertip' => '5 Function Fingertip'], 'placeholder' => 'Nil'])
            ->add('attachmentType', ChoiceType::class, ['choices' => array_combine(TruckAttachmentType::TYPES, TruckAttachmentType::TYPES), 'placeholder' => 'Nil', 'attr' => ['data-reveal' => '#attachment-detail']])
            ->add('attachment', EntityType::class, ['class'         => TruckAttachment::class, 'expanded'      => false, 'multiple'      => false, 'choice_label'  => 'nameString', 'placeholder'   => '--', 'group_by'      => 'type', 'query_builder' => fn (EntityRepository $er) => $er->getAttachmentsForLinkingQb($truckId, $customer)])
            ->add('truckType', ChoiceType::class, ['choices'         => ['Forklift'           => 'Forklift', 'Reach Truck'       => 'Reach Truck', 'Pallet Truck'     => 'Pallet Truck', 'Stacker'           => 'Stacker', 'Miscellaneous' => 'Miscellaneous'], 'placeholder' => 'Nil'])
            ->add('cabin', TextType::class)
            ->add('mastHeight', IntegerType::class, ['attr' => ['rightAddon' => 'mm']])
            ->add('mast', ChoiceType::class, ['choices' => array_flip(['Boom Lift'         => 'Boom lift', '2 Stage Clearview' => '2 Stage clearview', '2 Stage Free'      => '2 Stage free lift', '3 Stage FreeLift'  => '3 Stage free lift']), 'label' => 'Mast type', 'placeholder' => 'Nil', 'attr' => ['data-reveal' => '#mast-detail']])
            ->add('mastCode', TextType::class)
            ->add('forkDimensions', TextType::class)
            ->add('batterySpec', TextType::class)
            ->add('sideshift', ChoiceType::class, ['choices' => ['Integral' => 'Integral', 'Hookon'   => 'Hookon'], 'placeholder' => '--'])
            ->add('charger', EntityType::class, ['class'    => TruckCharger::class, 'multiple' => false, 'expanded' => true, 'choice_label' => 'nameString', 'placeholder' => '--', 'query_builder' => fn (EntityRepository $er) => $er->getChargersForLinkingQb($truckId, $customer)])
            ->add('tyreType', ChoiceType::class, ['choices'         => array_flip(['solid'             => 'Solid', 'pneumatic'         => 'Pneumatic', 'Solid non-marking' => 'Solid non-marking', 'warehouse'         => 'Warehouse']), 'placeholder' => '--'])
            ->add('type', ChoiceType::class, ['data' => $typeValue, 'label' => ' Type', 'mapped'        => false, 'choices' => ['Diesel'   => 'Diesel', 'LPG'      => 'LPG', 'Electric' => 'Electric']]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'data_class'         => Truck::class,
            'cascade_validation' => true,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'truck_form';
    }
}
