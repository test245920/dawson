<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderDateType extends AbstractType
{
    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $order     = $builder->getData();
        $job       = $order->getJob();
        $engineers = $job ? $job->getEngineers() : [];
        $isEngineer = $options['isEngineer'];

        $builder->setMethod(Request::METHOD_POST);
        $builder->add('orderDate', DateTimeType::class, ['html5' => false, 'widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'data' => new \DateTime('now'), 'attr' => ['class'            => 'datepicker', 'data-date-format' => 'YYYY-MM-DD']]);

        if ((is_countable($engineers) ? count($engineers) : 0) && !$isEngineer) {
            $builder->add('unassign', CheckboxType::class, ['label'  => 'Unassign job?', 'mapped' => false]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired([
            'isEngineer',
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'date_form';
    }
}
