<?php

namespace App\Form\Type;

use App\Entity\EstimateItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class EstimateItemType extends AbstractType
{
    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $builder->setMethod(Request::METHOD_POST);
        $builder->add('item', TextType::class, ['label' => 'Part Description'])
            ->add('partNumber', TextType::class)
            ->add('price', MoneyType::class, ['currency' => 'GBP', 'label' => 'Cost Price', 'attr'     => ['class' => 'price']])
            ->add('retailPrice', MoneyType::class, ['currency' => 'GBP', 'attr'     => ['class' => 'price']])
            ->add('quantity', IntegerType::class, ['constraints' => [new Assert\Regex(
                ['pattern' => '/^[0-9]\d*$/', 'message' => 'Please use only positive numbers.'],
            )], 'attr'   => ['min'   => 0, 'class' => 'qty']])
            ->add('receivedDate', DateTimeType::class, ['html5' => false, 'widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'attr'   => ['class'            => 'datepicker', 'data-date-format' => 'YYYY-MM-DD']])
            ->add('images', CollectionType::class, ['entry_type'         => ImageType::class, 'allow_add'    => true, 'allow_delete' => true, 'by_reference' => false]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EstimateItem::class,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'estimate_item_form';
    }
}
