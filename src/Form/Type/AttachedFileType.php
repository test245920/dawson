<?php

namespace App\Form\Type;

use App\Entity\AttachedFile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AttachedFileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('file', 'file', [
                'label' => false,
            ])
            ->add('description', TextType::class);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AttachedFile::class,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'file';
    }

    public function finishView(FormView $view, FormInterface $form, array $options): void
    {
        $file  = $form->getData();
        $hasId = $file ? $file->getId() : false;
        if ($hasId) {
            $view->vars = array_replace($view->vars, [
                'fileObject' => $form->getData(),
            ]);
        }
    }
}
