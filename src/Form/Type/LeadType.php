<?php

namespace App\Form\Type;

use App\Entity\Contact;
use App\Entity\Customer;
use App\Entity\Lead;
use App\Entity\Source;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\UsageTrackingTokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class LeadType extends AbstractType
{
    private $currentUser;

    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $securityContext   = $options['securityContext'];
        $authorizationChecker = $options['authorizationChecker'];
        $this->currentUser = $securityContext->getToken()->getUser();
        $userId            = $this->currentUser->getId();

        $builder->setMethod(Request::METHOD_POST);
        $builder->add('customer', EntityType::class, ['class'         => Customer::class, 'choice_label'      => 'name', 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('c')
            ->addSelect('partial address.{id}')
            ->leftJoin('c.billingAddress', 'address')
            ->orderBy('c.name', 'ASC')])
            ->add('receivedDate', DateTimeType::class, ['html5' => false, 'widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'data'   => new \DateTime('now'), 'attr'   => ['class'            => 'datepicker', 'data-date-format' => 'YYYY-MM-DD']])
            ->add('newCustomer', TextType::class, ['mapped' => false, 'label'  => 'Customer'])
            ->add('source', EntityType::class, ['class'         => Source::class, 'choice_label'      => 'name', 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('s')
                ->addOrderBy('s.name', 'ASC')])
            ->add('newSource', SourceType::class)
            ->add('priority', ChoiceType::class, ['choices' => ['Normal'    => 'Normal', 'Important' => 'Important', 'Urgent'    => 'Urgent']])
            ->add('newContacts', CollectionType::class, ['entry_type'   => ContactType::class, 'allow_add'    => true, 'allow_delete' => true, 'by_reference' => false, 'mapped'       => false]);

        if ($authorizationChecker->isGranted('ROLE_SUPER_ADMIN')) {
            $builder->add('user', EntityType::class, ['class'    => User::class, 'choice_label' => 'name', 'label'    => 'Owner']);
        } else {
            $builder->add('user', EntityType::class, ['class'         => User::class, 'choice_label'      => 'name', 'label'         => 'Owner', 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('u')
                ->andWhere('u.id = :id')
                ->setParameter('id', $userId)]);
        }

        $formModifier = function (FormInterface $form, ?Customer $customer = null): void {
            $formOptions = ['class'       => Contact::class, 'expanded'    => true, 'multiple'    => true, 'choice_label'    => 'name'];

            if ($customer) {
                $formOptions['query_builder'] = function (EntityRepository $er) use ($customer) {
                    $qb = $er->createQueryBuilder('con');

                    return $qb->select('con')
                        ->andWhere('con.customer = :customer')
                        ->setParameter('customer', $customer);
                };
            } else {
                $formOptions['choices'] = [];
            }

            $form->add('contacts', EntityType::class, $formOptions);
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier): void {
                $form     = $event->getForm();
                $lead     = $event->getData();
                $customer = $lead->getCustomer();

                $formModifier($form, $customer);
            },
        );

        $builder->get('customer')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier): void {
                // It's important here to fetch $event->getForm()->getData(), as
                // $event->getData() will get you the client data (that is, the ID)
                $form     = $event->getForm()->getParent();
                $customer = $event->getForm()->getData();

                // since we've added the listener to the child, we'll have to pass on
                // the parent to the callback functions!
                $formModifier($form, $customer);
            },
        );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => Lead::class,
            ],
        )->setRequired(
            [
                'securityContext',
                'authorizationChecker',
            ],
        )
            ->setAllowedTypes('securityContext', UsageTrackingTokenStorage::class)
            ->setAllowedTypes('authorizationChecker', AuthorizationChecker::class);
    }

    public function getBlockPrefix(): string
    {
        return 'lead_form';
    }
}
