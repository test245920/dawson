<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;

class SelectUserType extends AbstractType
{
    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $builder->setMethod(Request::METHOD_POST);
        $builder->add(
            $builder->create('id', HiddenType::class, [
                'attr' => ['class' => 'dawson-employee-id'],
            ]),
        );
    }

    public function getBlockPrefix(): string
    {
        return 'select_user_form';
    }
}
