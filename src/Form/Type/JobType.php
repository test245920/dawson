<?php

namespace App\Form\Type;

use App\Entity\AssetLifecyclePeriod;
use App\Entity\Contact;
use App\Entity\Customer;
use App\Entity\Job;
use App\Entity\SalesJob;
use App\Entity\SiteAddress;
use App\Entity\WorkLog;
use App\Form\DataTransformers\EntityToIdTransformer;
use App\Model\RecurringType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JobType extends AbstractType
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $job      = $builder->getData();
        $customer = $job->getCustomer();
        $isEdit   = $job && $job->getId();
        $builder->setMethod(Request::METHOD_POST);

        $transformer = new EntityToIdTransformer($this->em);
        $transformer->setEntityClass(AssetLifecyclePeriod::class);
        $transformer->setEntityRepository(AssetLifecyclePeriod::class);
        $transformer->setEntityType(AssetLifecyclePeriod::class);

        $builder->add(
            $builder->create('assetLifecyclePeriod', HiddenType::class)->addModelTransformer($transformer),
        );

        $builder->add(
            'callOutCharge',
            ChoiceType::class,
            [
                'choices' => [
                    'Yes' => 1,
                    'No' => 0,
                ],
                'label' => 'Call Out Charge',
                'expanded' => true,
                'mapped' => false,
                'data' => $isEdit && $job->getCallOutCharge() > 0 ? 1 : 0,
            ],
        )
            ->add(
                'isRecurring',
                ChoiceType::class,
                [
                    'choices' => [
                        'Yes' => true,
                        'No' => false,
                    ],
                    'label' => 'Schedule Job',
                    'expanded' => true,
                ],
            )
            ->add(
                'recurringType',
                ChoiceType::class,
                [
                    'choices' => array_flip(RecurringType::CHOICES),
                    'label' => 'Cadence',
                    'expanded' => true,
                ],
            );

        if ($options['includeAsset']) {
            $builder
                ->add('assetType', ChoiceType::class, ['choices' => array_flip(['Truck'           => 'Truck', 'TruckAttachment' => 'Attachment', 'TruckCharger'    => 'Charger']), 'mapped' => false])
                ->add('asset', TextType::class, ['mapped' => false, 'attr'   => ['placeholder' => 'Start typing...', 'autocomplete' => 'off'], 'data'   => $job->getAssetLifecyclePeriod() ? $job->getAssetLifecyclePeriod()->getAsset()->getNameString() : null]);
        }

        if ($options['includeCustomer']) {
            $builder->add('customer', EntityType::class, ['class'         => Customer::class, 'placeholder'   => 'None', 'choice_label'      => 'name', 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('c')
                    ->addSelect('partial address.{id}')
                    ->leftJoin('c.billingAddress', 'address')
                    ->andWhere('c.isActive = true')
                    ->orderBy('c.name', 'ASC')]);
        }

        $builder
            ->add('siteAddress', EntityType::class, [
                'class'         => SiteAddress::class,
                'choice_label'      => 'addressString',
                'placeholder'   => '-- Use billing address --',
                'label'         => 'Job Location',
                'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('sa')
                    ->addSelect('partial loc.{id}')
                    ->leftJoin('sa.location', 'loc'),
            ])
            ->add('detail', TextareaType::class, [
                'label'      => 'Job detail',
                'empty_data' => 'N/A',
            ])
            ->add('notes', TextareaType::class, [
                'label' => 'Notes to Engineer',
            ])
            ->add('jobActiveFrom', DateTimeType::class, [
                'html5' => false,
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'label'  => 'Start date',
                'data' => new \DateTime('now'),
                'attr' => [
                    'class'            => 'datepicker',
                    'data-date-format' => 'YYYY-MM-DD',
                ]])
            ->add('urgent', CheckboxType::class, [
                'label' => 'Breakdown',
            ])
            ->add('isGeneralRepairs', CheckboxType::class, [
                'label' => 'General repairs',
            ])
            ->add('isSalesJob', CheckboxType::class, [
                'label' => 'Sales Job',
            ])
            ->add('salesJob', EntityType::class, [
                'class'         => SalesJob::class,
                'mapped'        => false,
                'choice_label'  => 'displayTitle',
                'placeholder'   => '-- Select sales job --',
                'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('sj')
                    ->where('sj.status IN(:statuses)')
                    ->andWhere('sj.truck IS NOT NULL')
                    ->setParameter('statuses', [SalesJob::STATUS_INCOMPLETE, SalesJob::STATUS_IN_PREP]),
            ])
            ->add(
                'newContacts',
                CollectionType::class,
                ['entry_type' => ContactType::class, 'allow_add' => true, 'allow_delete' => true, 'by_reference' => false, 'mapped' => false, 'label' => false],
            )
            ->add(
                'images',
                CollectionType::class,
                ['entry_type' => ImageType::class, 'allow_add' => true, 'allow_delete' => true, 'by_reference' => false, 'label' => false],
            )
            ->add('isLolerJob', CheckboxType::class, ['label' => 'LOLER'])
            ->add('isServiceJob', CheckboxType::class, ['label' => 'Service']);

        if ($options['includeCustomer']) {
            $builder->get('customer')->addEventListener(FormEvents::POST_SUBMIT, $this->onPostSubmitCustomer(...));
        }

        $builder->addEventListener(FormEvents::PRE_SET_DATA, $this->onPreSetDataCustomer(...));
        $builder->addEventListener(FormEvents::PRE_SUBMIT, $this->onPreSubmit(...));
    }

    public function onPostSubmitCustomer(FormEvent $event): void
    {
        $form     = $event->getForm()->getParent();
        $customer = $event->getForm()->getData();

        $this->addContacts($form, $customer);
    }

    public function onPreSetDataCustomer(FormEvent $event): void
    {
        $job = $event->getData();
        $form = $event->getForm();
        $customer = $job->getCustomer();
        $this->addContacts($form, $customer);
    }

    public function onPreSubmit(FormEvent $event): void
    {
        $data = $event->getData();
        $form = $event->getForm();
        $job = $form->getData();

        if (isset($data['callOutCharge']) && $data['callOutCharge'] > 0) {
            $job->setCallOutCharge(WorkLog::DEFAULT_LABOUR_RATE);
        } else {
            $job->setCallOutCharge(0);
        }
    }

    public function addContacts(FormInterface $form, $customer = null): void
    {
        $formOptions = ['class'       => Contact::class, 'expanded'    => true, 'multiple'    => true, 'choice_label'    => 'name', 'label'       => false];

        if ($customer) {
            $formOptions['query_builder'] = fn (EntityRepository $er) => $er->createQueryBuilder('con')
                ->andWhere('con.customer = :customer')
                ->setParameter('customer', $customer);
        } else {
            $formOptions['choices'] = [];
        }

        $form->add('contacts', EntityType::class, $formOptions);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class'      => Job::class,
            'includeCustomer' => true,
            'includeAsset'    => true,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'job_form';
    }
}
