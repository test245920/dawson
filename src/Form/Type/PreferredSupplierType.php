<?php

namespace App\Form\Type;

use App\Entity\Stock;
use App\Entity\Supplier;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PreferredSupplierType extends AbstractType
{
    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $id = $options['id'];

        $builder->setMethod(Request::METHOD_POST);
        $builder->add('preferredSupplier', EntityType::class, [
            'class'         => Supplier::class,
            'choice_label'      => 'name',
            'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('sup')
                      ->leftJoin('sup.stockItems', 'supStk')
                      ->andWhere('supStk.stock = :id')
                      ->setParameter('id', $id),
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'stock_preferred_supplier_form';
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Stock::class,
            'id'         => null,
        ]);
    }
}
