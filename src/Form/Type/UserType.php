<?php

namespace App\Form\Type;

use App\Entity\Role;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $builder->setMethod(Request::METHOD_POST);
        $builder->add('username', TextType::class);
        $builder->add('name', TextType::class);
        $builder->add('email', EmailType::class);
        $builder->add('assignedRoles', EntityType::class, [
            'class'        => Role::class,
            'multiple'     => true,
            'expanded'     => true,
            'choice_label' => 'name',
        ]);
        $builder->add('plainPassword', RepeatedType::class, [
            'first_name'  => 'Password',
            'second_name' => 'Confirm',
            'type'        => PasswordType::class,
        ]);
        $builder->add('costMultiplier', NumberType::class, [
            'label' => 'Hourly rate multiplier',
            'help'  => 'E.g. to charge 50% of a customer\'s hourly rate, enter 0.5',
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'user_form';
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'validation_groups' => function (FormInterface $form): array {
                $data = $form->getData();
                if ($data->getId()) { // This is an edit form
                    return ['EditUser', 'Default'];
                }

                return ['NewUser', 'Default'];
            },
        ]);
    }
}
