<?php

namespace App\Form\Type;

use App\Entity\AssetMaintenance;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AssetMaintenanceType extends AbstractType
{
    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $builder->setMethod(Request::METHOD_POST);
        $builder->add('detail', TextareaType::class)
            ->add('amount', MoneyType::class, ['currency' => 'GBP'])
            ->add('date', DateTimeType::class, ['html5' => false, 'widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'attr'   => ['class'            => 'datepicker', 'data-date-format' => 'YYYY-MM-DD']]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AssetMaintenance::class,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'asset_maintenance';
    }
}
