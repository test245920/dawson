<?php

namespace App\Form\Type;

use App\Entity\Stock;
use App\Entity\Supplier;
use App\Form\DataTransformers\StockToNumberTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StockType extends AbstractType
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $transformer = new StockToNumberTransformer($this->em);
        $stockId = $options['stockId'];

        $builder->setMethod(Request::METHOD_POST);

        $builder
            ->add('description', TextType::class)
            ->add('code', TextType::class, ['label' => 'Part No'])
            ->add('bin', TextType::class)
            ->add('stockedItem', CheckboxType::class, ['mapped' => false])
            ->add('minStock', IntegerType::class)
            ->add('reorderQuantity', IntegerType::class)
            ->add('oemPrice', MoneyType::class, ['currency' => 'GBP', 'label'    => 'OEM price'])
            ->add('prevCode', TextType::class);

        if ($stockId) {
            $builder->add('preferredSupplier', EntityType::class, ['class'         => Supplier::class, 'choice_label'      => 'name', 'label'         => 'Preferred Supplier', 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('sup')
                ->leftJoin('sup.stockItems', 'supStk')
                ->andWhere('supStk.stock = :id')
                ->orderBy('sup.name')
                ->setParameter('id', $stockId)]);
        } else {
            $builder->add('preferredSupplier', EntityType::class, ['class'         => Supplier::class, 'choice_label'      => 'name', 'placeholder'   => '--', 'label'         => 'Supplier', 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('sup')
                ->orderBy('sup.name')]);
        }
    }

    public function getBlockPrefix(): string
    {
        return 'stock_form';
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Stock::class,
            'stockId'    => null,
        ]);
    }
}
