<?php

namespace App\Form\Type;

use App\Entity\Location;
use App\Entity\SupplierStock;
use App\Model\StockAdjustment;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ManualStockLevelAdjustmentType extends AbstractType
{
    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $stockId = $builder->getData()->getStock()->getId();
        $bin = $builder->getData()->getStock()->getBin();

        $builder->setMethod(Request::METHOD_POST);
        $builder
            ->add('quantity', ChoiceType::class, ['choices' => range(0, 999)])
            ->add('supplierStock', EntityType::class, ['class'         => SupplierStock::class, 'choice_label'      => 'supplier.name', 'label'         => 'Supplier', 'required'      => true, 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('ss')
                ->leftJoin('ss.stock', 's')
                ->where('s.id = :id')
                ->setParameter('id', $stockId)])
            ->add('location', EntityType::class, ['class'    => Location::class, 'choice_label' => 'locationString'])
            ->add('bin', TextType::class, ['data'    => $bin]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => StockAdjustment::class,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'manual_stock_level_adjustment_form';
    }
}
