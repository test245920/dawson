<?php

namespace App\Form\Type;

use App\Entity\AssetLifecyclePeriod;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EndAssetLifecyclePeriodType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $period = $builder->getData();

        $endLabel = match ($period->getType()) {
            AssetLifecyclePeriod::PERIOD_TYPE_CONTRACT => 'Buyback date',
            AssetLifecyclePeriod::PERIOD_TYPE_CASUAL => 'Hire end date',
            AssetLifecyclePeriod::PERIOD_TYPE_PURCHASE => 'Trade in date',
            default => 'Contract end',
        };

        $builder->setMethod(Request::METHOD_POST);

        $builder->add('end', DateTimeType::class, ['html5' => false, 'widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'label'  => $endLabel, 'attr'   => ['class'            => 'datepicker', 'data-date-format' => 'YYYY-MM-DD']]);

        if ($period->isContractHire()) {
            $builder->add('residualValue', MoneyType::class, ['currency' => 'GBP']);
        }

        if ($period->isPurchase()) {
            $builder->add('tradeInValue', MoneyType::class, ['currency' => 'GBP']);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AssetLifecyclePeriod::class,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'return_asset';
    }
}
