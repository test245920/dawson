<?php

namespace App\Form\Type;

use App\Entity\Image;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('image', FileType::class, [
            'label' => false,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Image::class,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'image';
    }

    public function finishView(FormView $view, FormInterface $form, array $options): void
    {
        $image = $form->getData();
        $hasId = $image ? $image->getId() : false;
        if ($hasId) {
            $view->vars = array_replace($view->vars, [
                'imageObject' => $form->getData(),
            ]);
        }
    }
}
