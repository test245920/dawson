<?php

namespace App\Form\Type;

use App\Entity\Contact;
use App\Entity\Customer;
use App\Entity\PurchaseOrder;
use App\Entity\TruckMake;
use App\Entity\TruckModel;
use App\Model\SalesJobRequest;
use App\Model\TruckAttachmentType;
use App\Model\Warranty;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SalesJobType extends AbstractType
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $job = $builder->getData();

        $builder
            ->add('make', EntityType::class, [
                'class'         => TruckMake::class,
                'choice_label'      => 'name',
                'placeholder'   => '--',
                'mapped'        => false,
                'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('tm')
                    ->orderBy('tm.name', 'ASC'),
                'attr' => [
                    'class' => 'truck-make',
                ],
            ])
            ->add('model', EntityType::class, [
                'class'         => TruckModel::class,
                'choice_label'      => 'name',
                'placeholder'   => '--',
                'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('tm')
                    ->orderBy('tm.name', 'ASC'),
                'attr' => [
                    'class' => 'truck-model',
                ],
            ])
            ->add('newMake', TextType::class)
            ->add('newModel', TextType::class)
            ->add('clientFleet', TextType::class, [
                'attr' => [
                    'placeholder' => 'N/A',
                ],
            ])
            ->add('serial', TextType::class)
            ->add('yearOfManufacture', TextType::class)
            ->add('hourReading', TextType::class)
            ->add('purchasePrice', MoneyType::class, [
                'currency' => 'GBP',
            ])
            ->add('purchasedDate', DateTimeType::class, [
                'html5' => false,
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'attr' => [
                    'class' => 'datepicker',
                    'data-date-format' => 'YYYY-MM-DD',
                ],
            ])
            ->add('manufacturersWarranty', ChoiceType::class, [
                'choices' => array_flip(Warranty::CHOICES),
                'attr' => ['data-reveal' => '#warranty-duration'],
            ])
            ->add('manufacturersWarrantyDuration', IntegerType::class, [
                'attr' => [
                    'rightAddon' => 'months',
                ],
            ])
            ->add('secondLevelManufacturersWarranty', ChoiceType::class, [
                'choices' => array_flip(Warranty::CHOICES),
                'attr' => ['data-reveal' => '#second-warranty-duration'],
            ])
            ->add('secondLevelManufacturersWarrantyDuration', IntegerType::class, [
                'attr' => [
                    'rightAddon' => 'months',
                ],
            ])
            ->add('stockValue', MoneyType::class, [
                'currency' => 'GBP',
            ])
            ->add('truckType', ChoiceType::class, [
                'choices'         => [
                    'Forklift'           => 'Forklift',
                    'Reach Truck'       => 'Reach Truck',
                    'Pallet Truck'     => 'Pallet Truck',
                    'Stacker'           => 'Stacker',
                    'Miscellaneous' => 'Miscellaneous',
                ],
                'placeholder' => 'Nil',
            ])
            ->add('mastHeight', IntegerType::class, [
                'attr' => [
                    'rightAddon' => 'mm',
                ]])
            ->add('mast', ChoiceType::class, [
                'choices' => array_flip([
                    'Boom Lift'         => 'Boom lift',
                    '2 Stage Clearview' => '2 Stage clearview',
                    '2 Stage Free'      => '2 Stage free lift',
                    '3 Stage FreeLift'  => '3 Stage free lift',
                ]),
                'label' => 'Mast type',
                'placeholder' => 'Nil',
                'attr' => ['data-reveal' => '#mast-detail'],
            ])
            ->add('mastCode', TextType::class, [
            ])
            ->add('hydraulics', ChoiceType::class, [
                'choices'         => [
                    '1 Function Manual'    => '1 Function Manual',
                    '2 Function Manual'    => '2 Function Manual',
                    '3 Function Manual'    => '3 Function Manual',
                    '4 Function Manual'    => '4 Function Manual',
                    '5 Function Manual'    => '5 Function Manual',
                    '1 Function Fingertip' => '1 Function Fingertip',
                    '2 Function Fingertip' => '2 Function Fingertip',
                    '3 Function Fingertip' => '3 Function Fingertip',
                    '4 Function Fingertip' => '4 Function Fingertip',
                    '5 Function Fingertip' => '5 Function Fingertip',
                ],
                'placeholder' => 'Nil',
            ])
            ->add('attachmentType', ChoiceType::class, [
                'choices' => array_combine(TruckAttachmentType::TYPES, TruckAttachmentType::TYPES),
                'placeholder' => 'Nil',
                'attr' => ['data-reveal' => '#attachment-detail'],
            ])
            ->add('cabin', TextType::class)
            ->add('tyreType', ChoiceType::class, [
                'choices'         => array_flip([
                    'solid'             => 'Solid',
                    'pneumatic'         => 'Pneumatic',
                    'Solid non-marking' => 'Solid non-marking',
                    'warehouse'         => 'Warehouse',
                ]),
                'placeholder' => '--',
            ])
            ->add('forkDimensions', TextType::class)
            ->add('sideshift', ChoiceType::class, [
                'choices' => [
                    'Integral' => 'Integral',
                    'Hookon'   => 'Hookon',
                ],
                'placeholder' => '--',
            ])
            ->add('detail', TextareaType::class)
            ->add('date', DateTimeType::class, [
                'html5' => false,
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'data'   => new \DateTime('now'),
                'attr'   => [
                    'class'            => 'datepicker',
                    'data-date-format' => 'YYYY-MM-DD',
                ],
            ]);

        $customerOptions = [
            'class' => Customer::class,
            'choice_label' => 'name',
            'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('c')
                ->andWhere('c.isActive = true')
                ->orderBy('c.name', 'ASC'),
        ];

        $customerRepo    = $this->em->getRepository(Customer::class);
        $defaultCustomer = $customerRepo->getDefaultCustomer();

        if ($defaultCustomer) {
            $customerOptions['preferred_choices'] = [$defaultCustomer];
        } else {
            $customerOptions['placeholder'] = 'None (Asset Register)';
        }

        $builder->add('customer', EntityType::class, $customerOptions);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, $this->onPreSetDataCustomer(...));
        //        $builder->addEventListener(FormEvents::PRE_SUBMIT, [$this, 'onPreSubmitData']);
    }

    public function onPreSetDataCustomer(FormEvent $event): void
    {
        $job = $event->getData();
        $form  = $event->getForm();
        $customer = $job->getCustomer();

        $this->addContacts($form, $customer);
    }

    public function addContacts(FormInterface $form, $customer = null): void
    {
        $formOptions = [
            'class' => Contact::class,
            'expanded' => true,
            'multiple' => true,
            'choice_label' => 'name',
            'label' => false,
        ];

        if ($customer) {
            $formOptions['query_builder'] = fn (EntityRepository $er) => $er->createQueryBuilder('con')
                ->andWhere('con.customer = :customer')
                ->setParameter('customer', $customer);
        } else {
            $formOptions['query_builder'] = null;
        }

        $form->add('contacts', EntityType::class, $formOptions);
    }

    //    public function onPreSubmitData(FormEvent $event): void
    //    {
    //        $data = $event->getData();
    //        $form = $event->getForm();
    //        $job = $form->getData();
    //
    //        if (!empty($data['purchaseOrders'])) {
    //            $purchaseOrder = $this->em->getRepository(PurchaseOrder::class)->find($data['purchaseOrders']);
    //            $job->addPurchaseOrder($purchaseOrder);
    //        }
    //    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SalesJobRequest::class,
            'cascade_validation' => true,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'sales_job_form';
    }
}
