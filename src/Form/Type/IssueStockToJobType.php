<?php

namespace App\Form\Type;

use App\Entity\Job;
use App\Entity\Location;
use App\Entity\SupplierStock;
use App\Model\StockAssignment;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IssueStockToJobType extends AbstractType
{
    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $stockId = $builder->getData()->getStock()->getId();

        $builder->setMethod(Request::METHOD_POST);

        $builder
            ->add('quantity', ChoiceType::class, ['choices' => array_combine(range(0, 100), range(0, 100))])
            ->add('supplierStock', EntityType::class, ['class'    => SupplierStock::class, 'choice_label' => 'displayString', 'label'    => 'Part', 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('ss')
                ->leftJoin('ss.stockItems', 'si')
                ->leftJoin('si.stock', 'stk')
                ->andWhere('stk.id = :id')
                ->andWhere('(si.usedOnJob IS NULL OR si.usedOnJob <> true)')
                ->andWhere('si.sold IS NULL')
                ->andWhere('si.job IS NULL')
                ->groupBy('ss.id')
                ->having('COUNT(si) > 0')
                ->setParameter('id', $stockId)])
            ->add('location', EntityType::class, ['class'    => Location::class, 'choice_label' => 'locationString', 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('l')
                ->leftJoin('l.stockItems', 'si')
                ->leftJoin('si.stock', 'stk')
                ->andWhere('stk.id = :id')
                ->andWhere('(si.usedOnJob IS NULL OR si.usedOnJob <> true)')
                ->andWhere('si.sold IS NULL')
                ->andWhere('si.job IS NULL')
                ->groupBy('l.id')
                ->having('COUNT(si) > 0')
                ->setParameter('id', $stockId)])
            ->add('job', EntityType::class, ['class'    => Job::class, 'choice_label' => 'JobIdentifier', 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('j')
                ->andWhere('j.completionStage NOT IN (7, 8)')]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => StockAssignment::class,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'issue_stock_to_job_form';
    }
}
