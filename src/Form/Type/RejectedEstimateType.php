<?php

namespace App\Form\Type;

use App\Entity\Estimate;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RejectedEstimateType extends AbstractType
{
    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $builder->setMethod(Request::METHOD_POST);
        $builder->add('unsuccessfulReason', TextareaType::class, [
            'label' => 'Reason',
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'rejected_estimate_form';
    }

    public function setDefaultOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Estimate::class,
        ]);
    }
}
