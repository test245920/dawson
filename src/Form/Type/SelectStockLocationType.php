<?php

namespace App\Form\Type;

use App\Entity\Location;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SelectStockLocationType extends AbstractType
{
    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $id = $options['id'];
        if ($id) {
            $builder->add('location', EntityType::class, [
                'class'         => Location::class,
                'choice_label'      => 'locationString',
                'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('c')
                          ->andWhere('c.id != :id')
                          ->setParameter('id', $id),
            ]);
        } else {
            $builder->add('location', EntityType::class, [
                'class'    => Location::class,
                'choice_label' => 'locationString',
                'mapped'   => false,
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'id' => null,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'select_stock_location_form';
    }
}
