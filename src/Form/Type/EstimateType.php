<?php

namespace App\Form\Type;

use App\Entity\Contact;
use App\Entity\Customer;
use App\Entity\Estimate;
use App\Entity\Job;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EstimateType extends AbstractType
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $estimate   = $builder->getData();
        $job        = $estimate->getJob();
        $customer   = $job ? $job->getCustomer() : null;
        $isEngineer = $options['isEngineer'];

        $builder->setMethod(Request::METHOD_POST);

        $builder
            ->add('travelTime', DurationType::class)
            ->add('labourTime', DurationType::class)
            ->add('details', TextareaType::class, ['label' => 'Work description'])
            ->add('estimateItems', CollectionType::class, ['entry_type'   => EstimateItemType::class, 'allow_add'    => true, 'allow_delete' => true, 'by_reference' => false]);

        $formModifier = function (FormInterface $form, ?Customer $customer = null): void {
            $formOptions = ['class'       => Contact::class, 'expanded'    => true, 'multiple'    => true, 'choice_label'    => 'name'];

            if ($customer) {
                $formOptions['query_builder'] = function (EntityRepository $er) use ($customer) {
                    $qb = $er->createQueryBuilder('con');

                    return $qb->select('con')
                        ->andWhere('con.customer = :customer')
                        ->setParameter('customer', $customer);
                };
            } else {
                $formOptions['choices'] = [];
            }

            $form->add('contacts', EntityType::class, $formOptions);
        };

        if (!$customer) {
            $builder->add('customer', EntityType::class, ['class'         => Customer::class, 'choice_label'      => 'name', 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('c')
                ->andWhere('c.isActive = true')
                ->orderBy('c.name', 'ASC')]);

            $builder->addEventListener(
                FormEvents::PRE_SET_DATA,
                function (FormEvent $event) use ($formModifier): void {
                    $form     = $event->getForm();
                    $estimate = $event->getData();
                    $customer = $estimate->getCustomer();

                    $formModifier($form, $customer);
                },
            );

            $builder->get('customer')->addEventListener(
                FormEvents::POST_SUBMIT,
                function (FormEvent $event) use ($formModifier): void {
                    $form     = $event->getForm()->getParent();
                    $customer = $event->getForm()->getData();

                    $formModifier($form, $customer);
                },
            );
        }

        if (!$job) {
            $builder->add('job', EntityType::class, ['class'         => Job::class, 'choice_label'      => 'jobIdentifier', 'placeholder'   => '', 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('j')
                ->andWhere('j.completed = 0')
                ->orderBy('j.jobCode', 'ASC')]);
        }

        if (!$isEngineer) {
            $builder
                ->add('totalPrice', MoneyType::class, ['currency' => 'GBP'])
                ->add('partsPrice', MoneyType::class, ['currency' => 'GBP'])
                ->add('labourPrice', MoneyType::class, ['currency' => 'GBP'])
                ->add('estimateStocks', CollectionType::class, ['entry_type'   => EstimateStockType::class, 'allow_add'    => true, 'allow_delete' => true, 'by_reference' => false])
                ->add('issued', CheckboxType::class)
                ->add('newContacts', CollectionType::class, ['entry_type'   => ContactType::class, 'allow_add'    => true, 'allow_delete' => true, 'by_reference' => false, 'mapped'       => false]);

            if (!$estimate->getIssued()) {
                $builder->add('issueImmediately', CheckboxType::class);
            }
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Estimate::class,
            'isEngineer' => false,
        ]);

        $resolver->setRequired(['isEngineer']);
    }

    public function getBlockPrefix(): string
    {
        return 'estimate_form';
    }
}
