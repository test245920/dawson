<?php

namespace App\Form\Type;

use App\Form\DataTransformers\DurationToIntegerTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DurationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $transformer = new DurationToIntegerTransformer();

        $builder->add(
            $builder->create('hour', ChoiceType::class, [
                'choices'        => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                'error_bubbling' => false,
                'label'          => $options['labels'] ? 'Hours' : false,
            ]),
        );

        $builder->add(
            $builder->create('minute', ChoiceType::class, [
                'choices'        => [
                    '0'  => 0,
                    '15' => 15,
                    '30' => 30,
                    '45' => 45,
                ],
                'error_bubbling' => false,
                'label'          => $options['labels'] ? 'Minutes' : false,
            ]),
        );

        $builder->addModelTransformer($transformer);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'hour'           => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            'minute'         => [0, 15, 30, 45],
            'error_bubbling' => false,
            'compound'       => true,
            'required'       => false,
            'labels'         => true,
            'innerColumns'   => null,
        ]);
    }

    public function getParent(): string
    {
        return TextType::class;
    }

    public function getBlockPrefix(): string
    {
        return 'duration';
    }
}
