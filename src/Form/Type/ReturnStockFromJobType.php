<?php

namespace App\Form\Type;

use App\Entity\Job;
use App\Entity\StockItem;
use App\Entity\SupplierStock;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class ReturnStockFromJobType extends AbstractType
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $stockId = $builder->getData()->getStock()->getId();

        $builder->setMethod(Request::METHOD_POST);

        $builder->add('job', EntityType::class, ['class'         => Job::class, 'choice_label'      => 'jobIdentifier', 'label'         => 'Job', 'required'      => true, 'placeholder'   => '-- Choose Job --', 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('j')
            ->leftJoin('j.attachedStockItems', 'si')
            ->leftJoin('si.stock', 's')
            ->andWhere('s.id = :id')
            ->setParameter('id', $stockId)]);

        $addSupplierStock = function (FormInterface $form, $stockId, ?Job $job = null): void {
            if (!$job) {
                return;
            }

            $stockOptions = ['class'    => SupplierStock::class, 'choice_label' => 'displayString', 'label'    => 'Part', 'placeholder'   => '-- Choose Part --', 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('ss')
                ->leftJoin('ss.stockItems', 'si')
                ->andWhere('si.stock = :stockId')
                ->andWhere('si.job = :jobId')
                ->groupBy('ss.id')
                ->having('COUNT(si) > 0')
                ->setParameter('stockId', $stockId)
                ->setParameter('jobId', $job->getId())];

            $form->add('supplierStock', EntityType::class, $stockOptions);
        };

        $stockItemRepo = $this->em->getRepository(StockItem::class);

        $addQuantity = function (FormEvent $event) use ($stockItemRepo): void {
            $data = $event->getData();
            $form = $event->getForm();

            $jobId = array_key_exists('job', $data) ? (int) $data['job'] : null;
            $supplierStockId = array_key_exists('supplierStock', $data) ? (int) $data['supplierStock'] : null;

            if (!$jobId || !$supplierStockId) {
                return;
            }

            $countOnJob = $stockItemRepo->getSupplierStockCountOnJob($supplierStockId, $jobId);

            if ($countOnJob < 1) {
                return;
            }

            $choices = range(1, $countOnJob);
            $choiceMap = array_combine($choices, $choices);

            $form->add('quantity', ChoiceType::class, ['placeholder' => '-- Choose Quantity --', 'choices' => $choiceMap]);
        };

        if (!$builder->has('quantity')) {
            // Prevent validation
            $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event): void {
                $event->stopPropagation();
            }, 900);
        }

        $builder->get('job')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($stockId, $addSupplierStock): void {
                $form = $event->getForm()->getParent();
                $job  = $event->getForm()->getData();

                $addSupplierStock($form, $stockId, $job);
            },
        );

        $builder->addEventListener(
            FormEvents::PRE_SUBMIT,
            function (FormEvent $event) use ($addQuantity): void {
                $addQuantity($event);
            },
        );

        $builder->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($addQuantity): void {
                $addQuantity($event);
            },
        );
    }

    public function getBlockPrefix(): string
    {
        return 'return_stock_from_job_form';
    }
}
