<?php

namespace App\Form\Type;

use App\Entity\TruckCharger;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;

class TruckChargerType extends AssetType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $repo        = $this->em->getRepository(TruckCharger::class);
        $makesModels = $repo->getMakesAndModels();

        parent::buildForm($builder, $options);

        $builder
            ->add('make', ChoiceType::class, [
                'choices'     => array_combine(array_keys($makesModels), array_keys($makesModels)),
                'placeholder' => '-- Choose --',
            ])
            ->add('model', ChoiceType::class, [
                'choices'     => $makesModels,
                'placeholder' => '-- Choose --',
            ])
            ->add('phase', ChoiceType::class, [
                'choices' => array_flip([
                    'Single Phase',
                    'Three Phase',
                ]),
            ])
            ->add('outputVoltage', ChoiceType::class, [
                'choices' => array_flip([
                    24,
                    48,
                    80,
                ]),
            ])
            ->add('outputCurrent', NumberType::class, [
                'attr' => ['rightAddon' => 'A'],
            ]);
    }

    public function getBlockPrefix(): string
    {
        return 'truck_charger';
    }
}
