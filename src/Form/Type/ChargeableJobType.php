<?php

namespace App\Form\Type;

use App\Model\ChargeMethod;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChargeableJobType extends AbstractType
{
    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $builder->setMethod(Request::METHOD_POST);

        $builder->add('chargeMethod', ChoiceType::class, [
            'label'   => 'Select Charge Method',
            'choices' => array_flip($this->choices($options)),
        ]);

        $builder->add('clientChargedReason', TextType::class, [
            'label' => 'Reason',
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'contract' => false,
            'warranty' => false,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'chargeable_job_form';
    }

    private function choices(array $options): array
    {
        $choices = ChargeMethod::CHOICES;

        if (!$options['contract']) {
            unset($choices[ChargeMethod::CHARGE_AGAINST_MAINTENANCE]);
        }

        if (!$options['warranty']) {
            unset($choices[ChargeMethod::WARRANTY]);
        }

        return $choices;
    }
}
