<?php

namespace App\Form\Type;

use App\Entity\Job;
use App\Entity\PurchaseOrder;
use App\Entity\Supplier;
use App\Model\JobStage;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PurchaseOrderType extends AbstractType
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $isEngineer = $options['isEngineer'];

        $order     = $builder->getData();
        $job       = $order->getJob();
        $engineers = $job ? $job->getEngineers() : [];

        $builder->setMethod(Request::METHOD_POST);

        $builder
            ->add('supplier', EntityType::class, ['class'         => Supplier::class, 'choice_label'      => 'name', 'required'      => true, 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('s')
                ->orderBy('s.name', 'ASC')])
            ->add('purchaseOrderItems', CollectionType::class, ['entry_type' => PurchaseOrderItemType::class, 'allow_add' => true, 'allow_delete' => true, 'by_reference' => false, 'label' => false])
            ->add('estimateItems', CollectionType::class, ['entry_type' => EstimateItemType::class, 'allow_add' => true, 'allow_delete' => true, 'by_reference' => false, 'label' => false])
            ->add('orderInstructions', TextType::class)
            ->add('ordered', CheckboxType::class, ['mapped' => false])
            ->add('orderDate', DateTimeType::class, ['html5' => false, 'widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'attr'   => ['class'            => 'datepicker', 'data-date-format' => 'YYYY-MM-DD']])
            ->add('engineerCollection', ChoiceType::class, ['choices' => array_flip([0 => 'Courier', 1 => 'Engineer']), 'label' => 'Order Type']);

        if ((is_countable($engineers) ? count($engineers) : 0) && !$isEngineer) {
            $builder->add('unassign', CheckboxType::class, ['label'  => 'Unassign job?', 'mapped' => false]);
        }

        // Allow editing of a PO's job in case it was created with none, or an incorrect one.
        if ($order->getId() && !$order->hasCheckInStarted()) {
            $builder->add('job', EntityType::class, ['class'         => Job::class, 'choice_label'      => 'displayTitle', 'required'      => true, 'placeholder'   => '--', 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('j')
                ->andWhere('j.completionStage < :complete')
                ->setParameter('complete', JobStage::COMPLETE)
                ->orderBy('j.id', 'ASC')]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => PurchaseOrder::class,
        ])->setRequired([
            'isEngineer',
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'purchase_order_form';
    }
}
