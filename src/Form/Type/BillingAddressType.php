<?php

namespace App\Form\Type;

use App\Entity\BillingAddress;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BillingAddressType extends AbstractType
{
    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $builder->setMethod(Request::METHOD_POST);
        $builder->add('address1', TextType::class, ['label' => 'Address line 1'])
            ->add('address2', TextType::class, ['label' => 'Address line 2'])
            ->add('city', TextType::class)
            ->add('postcode', TextType::class);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => BillingAddress::class,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'billing_address_form';
    }
}
