<?php

namespace App\Form\Type;

use App\Entity\Reminder;
use App\Entity\User;
use App\Form\DataTransformers\ArrayOfIdsToCollectionOfUsersTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\UsageTrackingTokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class ReminderType extends AbstractType
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $transformer = new ArrayOfIdsToCollectionOfUsersTransformer($this->em);

        $securityContext   = $options['securityContext'];
        $authorizationChecker = $options['authorizationChecker'];
        $this->currentUser = $securityContext->getToken();
        $userId            = $this->currentUser->getUser()->getId();

        $builder->setMethod(Request::METHOD_POST);
        $builder->add('type', ChoiceType::class, ['placeholder' => '--', 'placeholder' => '--', 'choices' => ['Future Target'  => 'Future Target', 'Renew Contract' => 'Renew contract', 'Phone Call'     => 'Phone Call', 'Send Email'     => 'Send Email', 'Visit'          => 'Visit', 'Other'          => 'Other', 'Service Call'   => 'Service Call']])
            ->add('detail', TextareaType::class)
            ->add('date', DateTimeType::class, ['html5' => false, 'widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'attr'   => ['class'            => 'datepicker', 'data-date-format' => 'YYYY-MM-DD']]);

        if ($authorizationChecker->isGranted('ROLE_SUPER_ADMIN')) {
            $builder->add('user', EntityType::class, ['class'         => User::class, 'choice_label'      => 'name']);
        } else {
            $builder->add('user', EntityType::class, ['class'         => User::class, 'choice_label'      => 'name', 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('u')
                ->andWhere('u.id = :id')
                ->setParameter('id', $userId)]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Reminder::class,
        ])->setRequired([
            'securityContext',
            'authorizationChecker',
        ])->setAllowedTypes('securityContext', UsageTrackingTokenStorage::class)
        ->setAllowedTypes('authorizationChecker', AuthorizationChecker::class);
    }

    public function getBlockPrefix(): string
    {
        return 'reminder_form';
    }
}
