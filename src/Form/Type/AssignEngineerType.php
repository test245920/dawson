<?php

namespace App\Form\Type;

use App\Entity\Job;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AssignEngineerType extends AbstractType
{
    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $builder->setMethod(Request::METHOD_POST);
        $builder->add('engineers', EntityType::class, ['class'         => User::class, 'choice_label'      => 'name', 'multiple'      => true, 'expanded'      => true, 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('u')
            ->leftJoin('u.roles', 'r')
            ->andWhere('r.role = :engineer')
            ->setParameter('engineer', 'ROLE_ENGINEER')
            ->orderBy('u.username', 'ASC')]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Job::class,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'assign_engineer_form';
    }
}
