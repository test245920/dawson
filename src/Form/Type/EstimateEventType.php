<?php

namespace App\Form\Type;

use App\Entity\Estimate;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;

class EstimateEventType extends AbstractType
{
    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $builder->setMethod(Request::METHOD_POST);
        $builder->add('isCustomerAction', CheckboxType::class)
            ->add('user', EntityType::class, ['class'         => User::class, 'choice_label'      => 'username'])
            ->add('isAdminResponse', CheckboxType::class)
            ->add('adminResponse', TextType::class)
            ->add('accepted', CheckboxType::class)
            ->add('rejected', CheckboxType::class)
            ->add('isCustomerQuery', CheckboxType::class)
            ->add('customerQuery', TextType::class)
            ->add('estimate', EntityType::class, ['class'         => Estimate::class, 'choice_label'      => 'id']);
    }

    public function getBlockPrefix(): string
    {
        return 'estimateEvent_form';
    }
}
