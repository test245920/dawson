<?php

namespace App\Form\Type;

use App\Entity\Location;
use App\Entity\Supplier;
use App\Model\AddStock;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddStockType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->setMethod(Request::METHOD_POST);

        $builder
            ->add('description', TextType::class)
            ->add('code', TextType::class, ['label' => 'Part No'])
            ->add('stockedItem', CheckboxType::class, ['mapped' => false])
            ->add('bin', TextType::class)
            ->add('minStock', IntegerType::class)
            ->add('reorderQuantity', IntegerType::class)
            ->add('location', EntityType::class, ['class'    => Location::class, 'label'    => 'Stock Location', 'choice_label' => 'locationString'])
            ->add('quantity', IntegerType::class)
            ->add('oemPrice', MoneyType::class, ['currency' => 'GBP', 'label' => 'OEM price'])
            ->add('prevCode', TextType::class)
            ->add('preferredSupplier', EntityType::class, ['class' => Supplier::class, 'choice_label' => 'name', 'placeholder' => '--', 'label' => 'Supplier', 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('sup')
                ->orderBy('sup.name')])
            ->add('costPrice', MoneyType::class, ['currency' => 'GBP'])
            ->add('carriagePrice', MoneyType::class, ['currency' => 'GBP', 'attr' => ['placeholder' => '0.00']])
            ->add('issuePrice', MoneyType::class, ['currency' => 'GBP'])
            ->add('supplierNotes', TextareaType::class);
    }

    public function getBlockPrefix(): string
    {
        return 'stock_form';
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AddStock::class,
            'stockId'    => null,
        ]);
    }
}
