<?php

namespace App\Form\Type;

use App\Entity\Location;
use App\Entity\SupplierStock;
use App\Form\DataTransformers\SupplierStockItemToNumberTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;

class StockItemType extends AbstractType
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $transformer = new SupplierStockItemToNumberTransformer($this->em);

        $builder->setMethod(Request::METHOD_POST);
        $builder->add(
            $builder->create('supplierStock', HiddenType::class, ['data_class' => SupplierStock::class])->addViewTransformer($transformer),
        )
            ->add('quantity', ChoiceType::class, ['choices' => array_combine(range(1, 100), range(1, 100)), 'attr'    => ['class' => 'qty', 'min'   => 0]])
            ->add('location', EntityType::class, ['class'       => Location::class, 'choice_label'    => 'locationString', 'placeholder' => '-- Location --']);
    }

    public function getBlockPrefix(): string
    {
        return 'stock_item_form';
    }
}
