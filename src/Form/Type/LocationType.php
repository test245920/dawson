<?php

namespace App\Form\Type;

use App\Entity\BillingAddress;
use App\Entity\Job;
use App\Entity\Location;
use App\Entity\SiteAddress;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LocationType extends AbstractType
{
    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $builder->setMethod(Request::METHOD_POST);
        $builder->add('type', TextType::class)
            ->add('siteAddress', EntityType::class, ['class'         => SiteAddress::class, 'choice_label'      => 'addressString'])->add('billingAddress', EntityType::class, ['class'         => BillingAddress::class, 'choice_label'      => 'addressString'])
            ->add('job', EntityType::class, ['class'         => Job::class, 'choice_label'      => 'jobCode']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Location::class,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'location_form';
    }
}
