<?php

namespace App\Form\Type;

use App\Entity\TruckModel;
use App\Form\DataTransformers\StockToNumberTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TruckModelType extends AbstractType
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $transformer = new StockToNumberTransformer($this->em);

        $builder->setMethod(Request::METHOD_POST);
        $builder->add('name', TextType::class)
                ->add('oilName', SearchType::class, ['attr' => ['class'       => 'search', 'data-search' => 'oil'], 'label'  => 'Oil filter', 'mapped' => false])
                ->add('fuelName', SearchType::class, ['attr' => ['class'       => 'search', 'data-search' => 'fuel'], 'label'  => 'Fuel filter', 'mapped' => false])
                ->add('airName', SearchType::class, ['attr' => ['class'       => 'search', 'data-search' => 'air'], 'label'  => 'Air filter', 'mapped' => false])
                ->add('frontTyres', TextType::class)
                ->add('rearTyres', TextType::class)
                ->add('oilFilter', HiddenType::class)
                ->add('airFilter', HiddenType::class)
                ->add('fuelFilter', HiddenType::class)
                ->add('fuel', ChoiceType::class, ['label' => 'Drive Type', 'choices' => ['Diesel'   => 'Diesel', 'LPG'      => 'LPG', 'Electric' => 'Electric']]);

        $builder->get('oilFilter')->addModelTransformer($transformer);
        $builder->get('airFilter')->addModelTransformer($transformer);
        $builder->get('fuelFilter')->addModelTransformer($transformer);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'data_class'         => TruckModel::class,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'truckModel_form';
    }
}
