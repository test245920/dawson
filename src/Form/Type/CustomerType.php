<?php

namespace App\Form\Type;

use App\Entity\User;
use App\Entity\WorkLog;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;

class CustomerType extends AbstractType
{
    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $builder->setMethod(Request::METHOD_POST);
        $builder->add('name', TextType::class)
            ->add('contacts', CollectionType::class, ['entry_type'   => ContactType::class, 'allow_add'    => true, 'allow_delete' => true, 'by_reference' => false])
            ->add('telephone', TextType::class)
            ->add('fax', TextType::class)
            ->add('email', EmailType::class, ['label' => 'Email (Invoicing)'])
            ->add('billingAddress', BillingAddressType::class)
            ->add('siteAddresses', CollectionType::class, ['entry_type'   => SiteAddressType::class, 'allow_add'    => true, 'allow_delete' => true, 'by_reference' => false])
            ->add('emailInvoice', CheckboxType::class, ['label' => 'Accept email Invoices/Statements?'])
            ->add('paymentTerm', IntegerType::class, ['attr' => ['rightAddon' => 'days']])
            ->add('accountLimit', MoneyType::class, ['currency' => 'GBP'])
            ->add('labourRate', MoneyType::class, ['currency' => 'GBP', 'attr' => ['placeholder' => 'Default (£' . WorkLog::DEFAULT_LABOUR_RATE . ')']])
            ->add('partsDiscount', NumberType::class, ['attr' => ['rightAddon' => '%']])
            ->add('trucksReassignable', ChoiceType::class, ['choices' => array_flip([false => 'no', true  => 'yes'])])
            ->add('requiresOrderNumberOnInvoices', CheckboxType::class, ['label' => 'Order Number on Invoices'])
            ->add('accountManager', EntityType::class, ['class'         => User::class, 'choice_label'      => 'username', 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('u')
                ->orWhere('u.username = \'Karl\'')
                ->orWhere('u.username = \'Graham\'')
                ->orWhere('u.username = \'Chris\'')
                ->orderBy('u.name', 'ASC')]);
    }

    public function getBlockPrefix(): string
    {
        return 'customer_form';
    }
}
