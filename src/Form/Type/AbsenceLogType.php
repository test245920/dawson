<?php

namespace App\Form\Type;

use App\Entity\AbsenceLog;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AbsenceLogType extends AbstractType
{
    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $absenceLog = $builder->getData();

        $builder->setMethod(Request::METHOD_POST);
        $builder->add('engineer', EntityType::class, ['class'         => User::class, 'choice_label'  => 'name', 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('u')
            ->leftJoin('u.roles', 'r')
            ->andWhere('r.role = :engineer')
            ->setParameter('engineer', 'ROLE_ENGINEER')
            ->orderBy('u.username', 'ASC')])
            ->add('date', DateTimeType::class, ['html5' => false, 'widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'label'  => 'Date', 'data'   => $absenceLog->getDate() ?: new \DateTime('now'), 'attr'   => ['class'            => 'datepicker', 'data-date-format' => 'YYYY-MM-DD']])
            ->add('time', DurationType::class)
            ->add('reason', ChoiceType::class, ['choices' => ['holiday' => 'holiday', 'sick'   => 'sick', 'unpaid leave'   => 'unpaid leave'], 'placeholder' => 'Select reason']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AbsenceLog::class,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'absence_log_form';
    }
}
