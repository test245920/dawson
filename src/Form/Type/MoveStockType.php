<?php

namespace App\Form\Type;

use App\Entity\Location;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MoveStockType extends AbstractType
{
    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $stockId = $builder->getData()->getStock()->getId();

        $builder->setMethod(Request::METHOD_POST);
        $builder
            ->add('quantity', IntegerType::class)
            ->add('dateReceived', DateTimeType::class, ['html5' => false, 'widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'data' => new \DateTime('now'), 'attr' => ['class'            => 'datepicker', 'data-date-format' => 'YYYY-MM-DD']])
            ->add('fromLocation', EntityType::class, ['class'    => Location::class, 'label'    => 'From Stock Location', 'choice_label' => 'locationString', 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('l')
                ->innerJoin('l.stockItems', 'si')
                ->where('si.stock = :id')
                ->setParameter('id', $stockId)])
            ->add('toLocation', EntityType::class, ['class'    => Location::class, 'label'    => 'To Stock Location', 'choice_label' => 'locationString']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired(['id']);
    }

    public function getBlockPrefix(): string
    {
        return 'add_stock_form';
    }
}
