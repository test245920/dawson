<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;

class UnsuccessfulLeadType extends AbstractType
{
    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $builder->setMethod(Request::METHOD_POST);
        $builder->add('unsuccessfulReason', TextareaType::class, [
            'label' => 'Reason',
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'unsuccessful_lead_form';
    }
}
