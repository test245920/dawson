<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReturnPartCountType extends AbstractType
{
    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $maxCount = $options['maxCount'];

        $builder->add('count', IntegerType::class, [
            'attr' => [
                'min'   => 1,
                'max'   => $maxCount,
                'value' => 1,
            ],
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'return_part_count';
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'maxCount' => null,
        ]);
    }
}
