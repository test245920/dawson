<?php

namespace App\Form\Type;

use App\Entity\AssetLifecyclePeriod;
use App\Entity\Customer;
use App\Entity\SiteAddress;
use App\Model\Warranty;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AssetLifecyclePeriodType extends AbstractType
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->setMethod(Request::METHOD_POST);

        $period = $builder->getData();
        $asset = $period->getAsset();
        $currentPeriod = $asset ? $asset->getCurrentLifecyclePeriod() : null;
        $currentCustomer = ($currentPeriod && $currentPeriod != $period) ? $currentPeriod->getCustomer() : null;

        $customerOptions = ['class'         => Customer::class, 'choice_label'      => 'name', 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('c')
            ->andWhere('c.isActive = true')
            ->orderBy('c.name', 'ASC')];

        $customerRepo = $this->em->getRepository(Customer::class);
        $defaultCustomer = $customerRepo->getDefaultCustomer();

        if ($defaultCustomer) {
            $customerOptions['preferred_choices'] = [$defaultCustomer];
        } else {
            $customerOptions['placeholder'] = 'None (Asset Register)';
        }

        $builder
            ->add('customer', EntityType::class, $customerOptions);

        if ($currentCustomer && !$currentCustomer->getIsDefault()) {
            $builder->add('type', ChoiceType::class, ['choices' => array_flip([AssetLifecyclePeriod::PERIOD_TYPE_CASUAL => 'Casual Hire'])]);
        } else {
            $builder->add('type', ChoiceType::class, ['choices' => array_flip([AssetLifecyclePeriod::PERIOD_TYPE_NONE     => 'None', AssetLifecyclePeriod::PERIOD_TYPE_CONTRACT => 'Contract Hire', AssetLifecyclePeriod::PERIOD_TYPE_CASUAL   => 'Casual Hire', AssetLifecyclePeriod::PERIOD_TYPE_PURCHASE => 'Purchase'])]);
        }

        $builder->get('customer')->addEventListener(
            FormEvents::POST_SUBMIT,
            $this->postSubmitCustomer(...),
        );

        $builder->get('type')->addEventListener(
            FormEvents::POST_SUBMIT,
            $this->postSubmitType(...),
        );

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            $this->preSetData(...),
        );
    }

    public function postSubmitCustomer(FormEvent $event): void
    {
        $form = $event->getForm()->getParent();
        $customer = $event->getForm()->getData();

        $customerId = $customer ? $customer->getId() : null;

        $this->addCustomerFields($form, $customerId);
    }

    public function postSubmitType(FormEvent $event): void
    {
        $form = $event->getForm()->getParent();
        $type = (int) $event->getForm()->getData();

        $this->addTypeFields($form, $type);
    }

    public function preSetData(FormEvent $event): void
    {
        $form = $event->getForm();
        $data = $event->getData();

        $customerId = $data->getCustomer() ? $data->getCustomer()->getId() : null;

        $this->addCustomerFields($form, $customerId);

        $type = (int) $data->getType();

        $this->addTypeFields($form, $type);
    }

    public function addCustomerFields($form, $customerId): void
    {
        $form->remove('siteAddress');
        $form->remove('clientFleet');

        if ($customerId) {
            $form
                ->add('siteAddress', EntityType::class, ['class'         => SiteAddress::class, 'choice_label'      => 'addressString', 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('s')
                    ->andWhere('s.customer = :customer')
                    ->setParameter('customer', $customerId)])
                ->add('clientFleet', TextType::class, ['attr' => ['placeholder' => 'N/A']]);
        }
    }

    public function addTypeFields($form, $type): void
    {
        $this->clearAdditionalFields($form);

        $startLabel = $type === AssetLifecyclePeriod::PERIOD_TYPE_PURCHASE ? 'Date sold' : 'Start date';

        $form->add('start', DateTimeType::class, ['html5' => false, 'widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'label'  => $startLabel, 'attr'   => ['class'            => 'datepicker', 'data-date-format' => 'YYYY-MM-DD']]);

        if ($type === AssetLifecyclePeriod::PERIOD_TYPE_CASUAL) {
            $this->addCommonHireFields($form);
            $this->addCasualHireFields($form);
        }

        if ($type === AssetLifecyclePeriod::PERIOD_TYPE_CONTRACT) {
            $this->addCommonHireFields($form);
            $this->addContractHireFields($form);
            $this->addNonCasualHireFields($form);
        }

        if ($type === AssetLifecyclePeriod::PERIOD_TYPE_PURCHASE) {
            $this->addPurchaseFields($form);
            $this->addNonCasualHireFields($form);
        }
    }

    public function clearAdditionalFields($form): void
    {
        $form->remove('start');
        $form->remove('hireRateMonthly');
        $form->remove('hireRateWeekly');
        $form->remove('estimatedHireDuration');
        $form->remove('contractHireDuration');
        $form->remove('residualValue');
        $form->remove('underwriter');
        $form->remove('hoursPerAnnum');
        $form->remove('maintenanceRateWeekly');
        $form->remove('maintenanceRateMonthly');
        $form->remove('end');
        $form->remove('saleWarranty');
        $form->remove('saleWarrantyDuration');
        $form->remove('salePrice');
        $form->remove('maintenanceContract');
        $form->remove('maintenanceDuration');
        $form->remove('lolerInterval');
        $form->remove('serviceInterval');
    }

    public function addCommonHireFields($form): void
    {
        $form
            ->add('end', DateTimeType::class, ['html5' => false, 'widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'label'  => 'End date', 'attr'   => ['class'            => 'datepicker', 'data-date-format' => 'YYYY-MM-DD']]);
    }

    public function addCasualHireFields($form): void
    {
        $form
            ->add('estimatedHireDuration', TextType::class, ['attr'   => ['rightAddon' => 'months']])
            ->add('hireRateMonthly', MoneyType::class, ['currency' => 'GBP', 'attr'   => ['rightAddon' => '/monthly']])
            ->add('hireRateWeekly', MoneyType::class, ['currency' => 'GBP', 'attr'   => ['rightAddon' => '/weekly']])
            ->add('lolerInterval', ChoiceType::class, ['choices' => array_flip(['12' => '12 months', '6'  => '6 months'])])
            ->add('serviceInterval', IntegerType::class, ['attr' => ['rightAddon' => 'months']]);
    }

    public function addContractHireFields($form): void
    {
        $form
            ->add('contractHireDuration', TextType::class, ['attr'   => ['rightAddon' => 'months']])
            ->add('residualValue', MoneyType::class, ['currency' => 'GBP'])
            ->add('underwriter', TextType::class)
            ->add('end', DateTimeType::class, ['html5' => false, 'widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'label'  => 'End date', 'attr'   => ['class'            => 'datepicker', 'data-date-format' => 'YYYY-MM-DD']])
            ->add('hireRateMonthly', MoneyType::class, ['currency' => 'GBP', 'attr' => ['rightAddon' => '/monthly']])
            ->add('hireRateWeekly', MoneyType::class, ['currency' => 'GBP', 'attr' => ['rightAddon' => '/weekly']]);
    }

    public function addNonCasualHireFields($form): void
    {
        $form
            ->add('hoursPerAnnum', IntegerType::class, ['attr'   => ['rightAddon' => 'hours']])
            ->add('maintenanceRateWeekly', MoneyType::class, ['currency' => 'GBP', 'attr'     => ['rightAddon' => '/week']])
            ->add('maintenanceRateMonthly', MoneyType::class, ['currency' => 'GBP', 'attr'     => ['rightAddon' => '/month']])
            ->add('salePrice', MoneyType::class, ['currency' => 'GBP'])
            ->add('lolerInterval', ChoiceType::class, ['choices' => array_flip(['12' => '12 months', '6'  => '6 months'])])
            ->add('serviceInterval', IntegerType::class, ['attr' => ['rightAddon' => 'months']]);
    }

    public function addPurchaseFields($form): void
    {
        $form
            ->add('saleWarranty', ChoiceType::class, ['label'   => 'Dawson warranty', 'choices' => array_flip(Warranty::CHOICES), 'attr'    => ['data-reveal' => '#sale-warranty-details']])
            ->add('saleWarrantyDuration', IntegerType::class, ['attr' => ['rightAddon' => 'months']])
            ->add('maintenanceContract', ChoiceType::class, ['choices' => array_flip([0 => 'No', 1 => 'Yes']), 'attr' => ['data-reveal' => '#maintenance-details']])
            ->add('maintenanceDuration', IntegerType::class, ['attr'   => ['rightAddon' => 'months']]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AssetLifecyclePeriod::class,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'transfer_asset';
    }
}
