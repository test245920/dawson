<?php

namespace App\Form\Type;

use App\Entity\Contact;
use App\Entity\Customer;
use App\Model\Warranty;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AssetType extends AbstractType
{
    public function __construct(protected EntityManagerInterface $em)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $isEdit = $builder->getData()->getId();

        $builder->setMethod(Request::METHOD_POST);
        $builder
            ->add('newMake', TextType::class)
            ->add('newModel', TextType::class)
            ->add('serial', TextType::class)
            ->add('yearOfManufacture', TextType::class)
            ->add('purchasePrice', MoneyType::class, ['currency' => 'GBP'])
            ->add('purchasedDate', DateTimeType::class, ['html5' => false, 'widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'attr' => ['class' => 'datepicker', 'data-date-format' => 'YYYY-MM-DD']])
            ->add('manufacturersWarranty', ChoiceType::class, ['choices' => array_flip(Warranty::CHOICES), 'attr' => ['data-reveal' => '#warranty-duration']])
            ->add('manufacturersWarrantyDuration', IntegerType::class, ['attr' => ['rightAddon' => 'months']])
            ->add('secondLevelManufacturersWarranty', ChoiceType::class, ['choices' => array_flip(Warranty::CHOICES), 'attr' => ['data-reveal' => '#second-warranty-duration']])
            ->add('secondLevelManufacturersWarrantyDuration', IntegerType::class, ['attr' => ['rightAddon' => 'months']])
            ->add('stockValue', MoneyType::class, ['currency' => 'GBP']);

        if (!$isEdit) {
            $builder
                ->add('detail', TextareaType::class, ['mapped' => false])
                ->add('date', DateTimeType::class, ['html5' => false, 'mapped' => false, 'widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'data'   => new \DateTime('now'), 'attr'   => ['class'            => 'datepicker', 'data-date-format' => 'YYYY-MM-DD']]);
        }

        if ($options['showCustomer']) {
            $customerOptions = ['class' => Customer::class, 'choice_label' => 'name', 'mapped' => false, 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('c')
                ->andWhere('c.isActive = true')
                ->orderBy('c.name', 'ASC')];

            $customerRepo    = $this->em->getRepository(Customer::class);
            $defaultCustomer = $customerRepo->getDefaultCustomer();

            if ($defaultCustomer) {
                $customerOptions['preferred_choices'] = [$defaultCustomer];
            } else {
                $customerOptions['placeholder'] = 'None (Asset Register)';
            }

            $builder->add('customer', EntityType::class, $customerOptions)
                ->add('clientFleet', TextType::class, ['mapped' => false, 'attr' => ['placeholder' => 'N/A']]);
        }
        $builder->addEventListener(FormEvents::PRE_SET_DATA, $this->onPreSetDataCustomer(...));
    }

    public function onPostSubmitCustomer(FormEvent $event): void
    {
        $form     = $event->getForm()->getParent();
        $customer = $event->getForm()->getData();

        $this->addContacts($form, $customer);
    }

    public function onPreSetDataCustomer(FormEvent $event): void
    {
        $job = $event->getData();
        $form = $event->getForm();

        $customer = $job->getCustomer();
        $this->addContacts($form, $customer);
    }

    public function addContacts(FormInterface $form, $customer = null): void
    {
        $formOptions = ['class' => Contact::class, 'mapped' => false, 'expanded' => true, 'multiple' => true, 'choice_label' => 'name', 'label' => false];

        if ($customer) {
            $formOptions['query_builder'] = fn (EntityRepository $er) => $er->createQueryBuilder('con')
                ->andWhere('con.customer = :customer')
                ->setParameter('customer', $customer);
        } else {
            $formOptions['query_builder'] = null;
        }

        $form->add('contacts', EntityType::class, $formOptions);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'showCustomer' => false,
        ])->setAllowedTypes('showCustomer', ['bool']);
    }
}
