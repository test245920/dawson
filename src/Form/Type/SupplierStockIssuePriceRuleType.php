<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;

class SupplierStockIssuePriceRuleType extends AbstractType
{
    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $builder->setMethod(Request::METHOD_POST);
        $builder
            ->add('startPrice', TextType::class)
            ->add('endPrice', TextType::class)
            ->add('profitMargin', TextType::class);
    }

    public function getBlockPrefix(): string
    {
        return 'supplier_stock_issue_price_rule_form';
    }
}
