<?php

namespace App\Form\Type;

use App\Entity\AssetLifecyclePeriod;
use App\Entity\Truck;
use App\Entity\User;
use App\Form\DataTransformers\DurationToIntegerTransformer;
use App\Form\DataTransformers\EntityToIdTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WorkLogType extends AbstractType
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $transformer = new DurationToIntegerTransformer();

        $workLog   = $builder->getData();
        $job       = $workLog->getJob();
        $asset     = $job->getAsset();

        if ($asset instanceof Truck) {
            $truck = $asset;
        }

        $engineers = $job->getEngineers();

        $builder->setMethod(Request::METHOD_POST);
        $builder->add('user', EntityType::class, ['class'         => User::class, 'choice_label'      => 'displayName', 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('u')
            ->leftJoin('u.roles', 'r')
            ->leftJoin('u.jobs', 'j')
            ->andWhere('r.role = :engineer')
            ->andWhere('j = :job')
            ->setParameter('engineer', 'ROLE_ENGINEER')
            ->setParameter('job', $job)
            ->orderBy('u.username', 'ASC'), 'label' => 'Engineer'])
            ->add('startDate', DateTimeType::class, ['html5' => false, 'widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'attr'   => ['class'            => 'datepicker', 'data-date-format' => 'YYYY-MM-DD']])
            ->add('timeLabour', DurationType::class, ['label' => false, 'innerColumns' => ['label' => 6, 'field' => 6]])
            ->add('timeLabourOvertime', DurationType::class, ['label' => false, 'labels' => false])
            ->add('timeLabourExtremeOvertime', DurationType::class, ['label' => false, 'labels' => false])
            ->add('timeTravel', DurationType::class, ['label' => false, 'innerColumns' => ['label' => 6, 'field' => 6]])
            ->add('timeTravelOvertime', DurationType::class, ['label' => false, 'labels' => false])
            ->add('timeTravelExtremeOvertime', DurationType::class, ['label' => false, 'labels' => false])
            ->add('description', TextareaType::class, ['label' => $this->getDescriptionLabel($options, $job)])
            ->add('requiresParts', CheckboxType::class)
            ->add('requestedParts', TextareaType::class, ['label' => 'Requested Parts']);

        $builder->get('startDate')->addModelTransformer(
            new CallbackTransformer(
                function ($value) {
                    if (!$value) {
                        return new \DateTime('now');
                    }

                    return $value;
                },
                fn ($value) => $value,
            ),
        );

        if (!$job->getHourReading()) {
            $builder->add('hourReading', TextType::class);
        }

        if (is_countable($engineers) ? count($engineers) : 0) {
            $builder->add('unassignJob', CheckboxType::class, ['label' => 'Unassign job?']);
        }

        if (!$job->getCompleted()) {
            $builder->add('completeJob', CheckboxType::class)
                ->add('postponeJob', CheckboxType::class);

            if (!$job->getAssetLifecyclePeriod()) {
                $transformer = new EntityToIdTransformer($this->em);
                $transformer->setEntityClass(AssetLifecyclePeriod::class);
                $transformer->setEntityRepository(AssetLifecyclePeriod::class);
                $transformer->setEntityType(AssetLifecyclePeriod::class);

                $builder->add(
                    $builder->create('assetLifecyclePeriod', HiddenType::class)->addModelTransformer($transformer),
                );

                $builder
                    ->add('assetType', ChoiceType::class, ['choices' => ['Truck'           => 'Truck', 'TruckAttachment' => 'TruckAttachment', 'TruckCharger'    => 'TruckCharger'], 'mapped' => false])
                    ->add('asset', TextType::class, ['mapped' => false, 'attr'   => ['placeholder' => 'Start typing...'], 'data'   => $job->getAssetLifecyclePeriod() ? $job->getAssetLifecyclePeriod()->getAsset()->getNameString() : null]);
            }
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'isEdit' => false,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'add_worklog_form';
    }

    private function getDescriptionLabel(array $options, $job): string
    {
        $label = 'Work Log';

        if (!$options['isEdit']) {
            $existingLogCount = is_countable($job->getWorklogs()) ? count($job->getWorklogs()) : 0;

            if ($existingLogCount > 1) {
                $label .= ' ' . $existingLogCount;
            }
        }

        return $label;
    }
}
