<?php

namespace App\Form\Type;

use App\Entity\EstimateItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class EstimateDMHEItemType extends AbstractType
{
    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $builder->setMethod(\Symfony\Component\HttpFoundation\Request::METHOD_POST);
        $builder->add('item', TextType::class, [
            'label' => 'Description',
        ])
            ->add('partNumber', HiddenType::class)
            ->add('price', MoneyType::class, [
                'currency' => 'GBP',
                'label' => 'Cost',
                'attr'     => [
                    'class' => 'price',
                ],
            ])
            ->add('quantity', IntegerType::class, [
                'constraints' => [
                    new Assert\Regex(
                        [
                            'pattern' => '/^[0-9]\d*$/',
                            'message' => 'Please use only positive numbers.',
                        ],
                    ),
                ],
                'attr'   => [
                    'min'   => 0,
                    'class' => 'qty',
                ],
            ])
            ->add('leadTime', TextType::class, [
                'label' => 'Lead Time',
            ])
            ->add('eta', DateTimeType::class, [
                'html5' => false,
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'attr'   => [
                    'class'            => 'datepicker',
                    'data-date-format' => 'YYYY-MM-DD',
                ],
                'label' => 'Eta',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EstimateItem::class,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'estimate_item_form';
    }
}
