<?php

namespace App\Form\Type;

use App\Entity\Supplier;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;

class SupplierStockType extends AbstractType
{
    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $supplierStock = $builder->getData();
        $stock = $supplierStock->getStock();
        $stockId = $stock ? $stock->getId() : null;

        $isEdit = (bool) $supplierStock->getId();

        $builder->setMethod(Request::METHOD_POST);

        $builder
            ->add('description', TextType::class)
            ->add('costPrice', MoneyType::class, ['currency' => 'GBP'])
            ->add('carriagePrice', MoneyType::class, ['currency' => 'GBP', 'attr'     => ['placeholder' => '0.00']])
            ->add('supplierNotes', TextareaType::class)
            ->add('newContacts', CollectionType::class, ['entry_type'   => ContactType::class, 'allow_add'    => true, 'allow_delete' => true, 'by_reference' => false, 'mapped'       => false])
            ->add('code', TextType::class)
            ->add('issuePrice', MoneyType::class, ['currency' => 'GBP']);

        if (!$isEdit) {
            $builder->add('supplier', EntityType::class, ['class'         => Supplier::class, 'choice_label'      => 'name', 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('sup')
                ->select('sup, partial address.{id}')
                ->leftJoin('sup.supplierAddress', 'address')
                ->andWhere('sup.id NOT IN (SELECT supplier.id FROM App\Entity\SupplierStock supstock LEFT JOIN supstock.stock stock LEFT JOIN supstock.supplier supplier WHERE stock.id = :id)')
                ->setParameter('id', $stockId)]);
        }
    }

    public function getBlockPrefix(): string
    {
        return 'supplier_stock_form';
    }
}
