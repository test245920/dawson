<?php

namespace App\Form\Type;

use App\Entity\EstimateStock;
use App\Form\DataTransformers\SupplierStockItemToNumberTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EstimateStockType extends AbstractType
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $transformer = new SupplierStockItemToNumberTransformer($this->em);

        $builder->setMethod(Request::METHOD_POST);
        $builder->add(
            $builder->create('supplierStock', HiddenType::class)->addViewTransformer($transformer),
        )
                ->add('quantity', ChoiceType::class, ['choices' => array_combine(range(1, 100), range(1, 100)), 'attr'    => ['class' => 'qty', 'min'    => 0]])
                ->add('price', MoneyType::class, ['currency' => 'GBP', 'mapped'   => false, 'attr' => ['class' => 'price']]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EstimateStock::class,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'estimate_stock_form';
    }
}
