<?php

namespace App\Form\Type;

use App\Entity\Note;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NoteType extends AbstractType
{
    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $builder->setMethod(Request::METHOD_POST);

        if ($options['includeReason'] === true) {
            $builder->add('reason', ChoiceType::class, [
                'choices' => [
                    'Visit'                      => 'Visit',
                    'Email'                      => 'Email',
                    'Telephone call'             => 'Telephone call',
                    'Telephone call, no answer.' => 'Telephone call, no answer',
                    'Other'                      => 'Other',
                ],
            ]);
        }

        $builder->add('detail', TextareaType::class);

        if ($options['data']->getDate()) {
            $builder->add('date', DateTimeType::class, [
                'html5' => false,
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'attr'   => [
                    'class'            => 'datepicker',
                    'data-date-format' => 'YYYY-MM-DD',
                ],
            ]);
        } else {
            $builder->add('date', DateTimeType::class, [
                'html5' => false,
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'data'   => new \DateTime('now'),
                'attr'   => [
                    'class'            => 'datepicker',
                    'data-date-format' => 'YYYY-MM-DD',
                ],
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class'    => Note::class,
            'includeReason' => false,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'note_form';
    }
}
