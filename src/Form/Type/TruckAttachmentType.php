<?php

namespace App\Form\Type;

use App\Entity\TruckAttachment;
use App\Model\TruckAttachmentType as AttachmentTypes;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class TruckAttachmentType extends AssetType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $repo        = $this->em->getRepository(TruckAttachment::class);
        $makesModels = $repo->getMakesAndModels();

        parent::buildForm($builder, $options);

        $builder
            ->add('make', ChoiceType::class, [
                'choices'     => array_combine(array_keys($makesModels), array_keys($makesModels)),
                'placeholder' => '-- Choose --',
            ])
            ->add('model', ChoiceType::class, [
                'choices'     => $makesModels,
                'placeholder' => '-- Choose --',
            ])
            ->add('type', ChoiceType::class, [
                'choices' => array_combine(AttachmentTypes::TYPES, AttachmentTypes::TYPES),
            ]);
    }

    public function getBlockPrefix(): string
    {
        return 'truck_attachment';
    }
}
