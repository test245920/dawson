<?php

namespace App\Form\Type;

use App\Model\JobStage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;

class JobStatusType extends AbstractType
{
    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $builder->setMethod(Request::METHOD_POST);
        $builder->add('stage', ChoiceType::class, [
            'choices' => array_flip(JobStage::$stageChoices),
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'job_status';
    }
}
