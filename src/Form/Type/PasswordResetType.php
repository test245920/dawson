<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;

class PasswordResetType extends AbstractType
{
    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $builder->setMethod(Request::METHOD_POST)
            ->add('plainPassword', RepeatedType::class, [
                'first_name'  => 'Password',
                'second_name' => 'Confirm',
                'type'        => PasswordType::class,
            ])
            ->add('submit', SubmitType::class);
    }

    public function getBlockPrefix(): string
    {
        return 'password_reset_form';
    }
}
