<?php

namespace App\Form\Type;

use App\Entity\PurchaseOrder;
use App\Entity\SalesJob;
use App\Entity\Supplier;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DMHEPurchaseOrderType extends AbstractType
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function buildForm(FormbuilderInterface $builder, $options): void
    {
        $builder->setMethod(\Symfony\Component\HttpFoundation\Request::METHOD_POST);
        $order     = $builder->getData();
        $job       = $order->getSalesJob();

        $builder
            ->add('supplier', EntityType::class, [
                'class'         => Supplier::class,
                'choice_label'      => 'name',
                'required'      => true,
                'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('s')
                    ->orderBy('s.name', 'ASC'),
            ])
            ->add('orderType', ChoiceType::class, [
                'choices' => array_flip([
                    'New' => 'New',
                    'Used' => 'Used',
                ]),
                'label' => 'Type',
            ])
            ->add('target', ChoiceType::class, [
                'mapped' => false,
                'choices' => array_flip([
                    'DMHE Stock' => 'DMHE Stock',
                    'Sales Job' => 'Sales Job',
                ]),
                'label' => 'Target',
            ])
            ->add('purchaseOrderItems', CollectionType::class, [
                'entry_type' => PurchaseOrderItemType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label' => false,
            ]);
        $builder->add('estimateItems', CollectionType::class, [
            'entry_type' => EstimateDMHEItemType::class,
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
            'label' => false,
        ])
            ->add('orderInstructions', TextType::class)
            ->add('ordered', CheckboxType::class, [
                'mapped' => false,
            ])
            ->add('orderDate', DateTimeType::class, [
                'html5' => false,
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'attr'   => [
                    'class'            => 'datepicker',
                    'data-date-format' => 'YYYY-MM-DD',
                ],
            ]);

        $builder->add('salesJob', EntityType::class, [
            'class'         => SalesJob::class,
            'choice_label'      => 'displayTitle',
            'required'      => true,
            'placeholder'   => '--',
            'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('j')
                ->andWhere('j.status != :status')
                ->setParameter('status', SalesJob::STATUS_COMPLETE)
                ->orderBy('j.id', 'ASC'),
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => PurchaseOrder::class,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'purchase_order_form';
    }
}
