<?php

namespace App\Form\Type;

use App\Entity\Location;
use App\Entity\PurchaseOrderItem;
use App\Form\DataTransformers\SupplierStockItemToNumberTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PurchaseOrderItemType extends AbstractType
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $transformer = new SupplierStockItemToNumberTransformer($this->em);

        $builder->add('unitCost', HiddenType::class);

        $builder->setMethod(Request::METHOD_POST);
        $builder->add('quantity', IntegerType::class, ['attr' => ['min' => 1]])
            ->add('receivedDate', DateTimeType::class, ['html5' => false, 'widget' => 'single_text', 'format' => 'yyyy-MM-dd', 'attr'   => ['class'            => 'datepicker', 'data-date-format' => 'YYYY-MM-DD']])
            ->add(
                $builder->create('item', TextType::class, ['label' => 'Item ID', 'attr'  => ['class' => 'supplier-stock-id']])->addViewTransformer($transformer),
            );

        if ($options['includeLocation']) {
            $builder->add('location', EntityType::class, ['class'    => Location::class, 'choice_label' => 'locationString', 'mapped'   => false]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class'      => PurchaseOrderItem::class,
            'includeLocation' => false,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'purchase_order_item_form';
    }
}
