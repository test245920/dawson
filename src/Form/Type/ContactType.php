<?php

namespace App\Form\Type;

use App\Entity\Contact;
use App\Entity\Customer;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType
{
    public function buildForm(FormbuilderInterface $builder, array $options): void
    {
        $builder->setMethod(Request::METHOD_POST);
        $builder->add('name', TextType::class, ['attr' => ['class' => 'contact-name']])
            ->add('jobTitle', TextType::class)
            ->add('telephone', TextType::class, ['attr' => ['class' => 'contact-telephone']])
            ->add('mobile', TextType::class)
            ->add('email', EmailType::class, ['attr' => ['class' => 'contact-email']]);

        if ($options['withCustomer']) {
            $builder->add('customer', EntityType::class, ['class'         => Customer::class, 'choice_label'      => 'name', 'placeholder'   => 'No associated customer', 'query_builder' => fn (EntityRepository $er) => $er->createQueryBuilder('c')
                ->addSelect('partial billingAddress.{id}')
                ->leftJoin('c.billingAddress', 'billingAddress')
                ->orderBy('c.name', 'ASC')])
            ->add('newCustomer', TextType::class, ['mapped' => false, 'label'  => 'Customer']);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class'   => Contact::class,
            'withCustomer' => false,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'add_contact_form';
    }
}
