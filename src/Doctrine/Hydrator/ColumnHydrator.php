<?php

namespace App\Doctrine\Hydrator;

use Doctrine\ORM\Internal\Hydration\AbstractHydrator;

class ColumnHydrator extends AbstractHydrator
{
    protected function hydrateAllData(): array
    {
        return $this->_stmt->fetchAll(\PDO::FETCH_COLUMN);
    }
}
