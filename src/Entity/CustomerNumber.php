<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * App\Entity\CustomerNumber.
 *
 * @ORM\Table(name="customer_number")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\CustomerNumberRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
#[UniqueEntity(fields: ['letter', 'number'], message: 'The customer number that was auto-assigned is already in use. Please try again.', repositoryMethod: 'findByIncludingSoftDeleted')]
class CustomerNumber
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(name="letter", type="text", length=1)
     */
    protected ?string $letter = null;

    /**
     * @ORM\Column(name="number", type="integer")
     */
    protected ?int $number = null;

    /**
     * @ORM\Column(name="customerNumber", type="text", length=4)
     */
    protected ?string $customerNumber = null;

    /**
     * @ORM\Column(name="assigned_at", type="datetime")
     */
    protected ?\DateTimeInterface $assignedAt;

    /**
     * @ORM\Column(name="active", type="boolean")
     */
    protected ?bool $active = true;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    private ?\DateTimeInterface $deletedAt = null;

    public function __construct(/**
     * @ORM\ManyToOne(targetEntity="App\Entity\Customer", inversedBy="customerNumbers")
     */
        protected Customer $customer
    ) {
        $this->complete = false;
    }

    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    public function setCustomer(Customer $value): CustomerNumber
    {
        $this->customer = $value;

        return $this;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(?int $value): CustomerNumber
    {
        $this->number = $value;

        return $this;
    }

    public function getAssignedAt(): ?\DateTimeInterface
    {
        return $this->assignedAt;
    }

    public function setAssignedAt(?\DateTimeInterface $value): CustomerNumber
    {
        $this->assignedAt = $value;

        return $this;
    }

    public function getLetter(): ?string
    {
        return $this->letter;
    }

    public function setLetter(?string $value): CustomerNumber
    {
        $this->letter = $value;

        return $this;
    }

    public function getCustomerNumber(): ?string
    {
        return $this->customerNumber;
    }

    public function setCustomerNumber(?string $value): CustomerNumber
    {
        $this->customerNumber = $value;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }
}
