<?php

namespace App\Entity;

use App\Model\SearchableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * App\Entity\User.
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\UserRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
#[UniqueEntity(fields: ['username'], repositoryMethod: 'findByIncludingSoftDeleted')]
#[UniqueEntity(fields: ['email'], repositoryMethod: 'findByIncludingSoftDeleted')]
class User implements \Serializable, SearchableInterface, UserInterface
{
    final public const DEFAULT_RATE_MULTIPLIER = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @var string
     * @ORM\Column(name="username", type="string", length=50, unique=true)
     * @Gedmo\Versioned
     */
    #[Assert\NotNull(message: 'Required')]
    protected ?string $username = null;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=true)
     * @Gedmo\Versioned
     */
    #[Assert\NotNull(message: 'Required')]
    protected ?string $name = null;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=64)
     * @Gedmo\Versioned
     */
    protected ?string $password = null;

    /**
     * Plain password. Used for model validation. Must not be persisted.
     */
    #[Assert\Length(min: '6')]
    #[Assert\NotBlank(groups: ['NewUser'])]
    protected $plainPassword;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=60, unique=true, nullable=true)
     * @Gedmo\Versioned
     */
    #[Assert\NotNull(message: 'Required')]
    protected ?string $email = null;

    /**
     * @var bool
     *
     * @ORM\Column(name="isActive", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $isActive = true;

    /**
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users", cascade={"persist", "remove"})
     * @var Collection<Role>
     */
    protected Collection $roles;

    /**
     * @ORM\ManyToMany(targetEntity="Job", mappedBy="engineers", cascade={"persist"})
     * @var Collection<Job>
     */
    protected Collection $jobs;

    /**
     * @ORM\ManyToOne(targetEntity="Customer", inversedBy="users", cascade={"persist"}, )
     */
    protected ?Customer $customer = null;

    /**
     * @ORM\OneToMany(targetEntity="Lead", mappedBy="user", cascade={"persist"})
     * @var Collection<Lead>
     */
    protected Collection $leads;

    /**
     * @ORM\OneToMany(targetEntity="EstimateEvent", mappedBy="user", cascade={"persist"})
     * @var Collection<EstimateEvent>
     */
    protected Collection $estimateEvents;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\Column(name="password_reset_token", type="string", length=64, nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $passwordResetToken = null;

    /**
     * @ORM\Column(name="password_reset_token_expiry", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $passwordResetTokenExpiry = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\WorkLog", mappedBy="user", cascade={"persist"})
     * @var Collection<WorkLog>
     */
    protected Collection $workLogs;

    /**
     * @ORM\OneToMany(targetEntity="Reminder", mappedBy="user", cascade={"persist"})
     * @var Collection<Reminder>
     */
    protected Collection $reminders;

    /**
     * @ORM\OneToOne(targetEntity="Contact", inversedBy="user", cascade={"persist", "remove"}, )
     */
    protected ?Contact $contact = null;

    /**
     * @ORM\OneToMany(targetEntity="Customer", mappedBy="user", cascade={"persist"})
     * @var Collection<Customer>
     */
    protected Collection $customerAccounts;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Location", mappedBy="engineer", cascade={"persist", "remove"})
     */
    protected ?Location $van = null;

    /**
     * @ORM\Column(name="cost_multiplier", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    #[Assert\GreaterThanOrEqual(value: 0)]
    protected ?float $costMultiplier = 1;

    public function __construct()
    {
        $this->jobs = new ArrayCollection();
        $this->estimateEvents = new ArrayCollection();
        $this->roles            = new ArrayCollection();
        $this->leads            = new ArrayCollection();
        $this->workLogs         = new ArrayCollection();
        $this->reminders        = new ArrayCollection();
        $this->customerAccounts = new ArrayCollection();
    }

    public static function getSearchFields(): array
    {
        return ['id'       => 'ID', 'name'     => 'Name', 'username' => 'Username', 'email'    => 'Email'];
    }

    public static function getSearchCategory(): string
    {
        return 'user';
    }

    public function getDisplayName(): string
    {
        return $this->name . ' (' . $this->username . ')';
    }

    public function addWorkLog($value): User
    {
        $this->workLogs->add($value);

        return $this;
    }

    public function removeWorkLog($value): void
    {
        $this->workLogs->remove($value);
    }

    public function getWorkLogs()
    {
        return $this->worklogs->toArray();
    }

    public function addLead($value): User
    {
        $this->leads->add($value);

        return $this;
    }

    public function removeLead($value): void
    {
        $this->leads->remove($value);
    }

    public function getLeads()
    {
        return $this->leads->toArray();
    }

    public function getSalt(): ?string
    {
        return null;
    }

    // We use bcrypt

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setUsername(?string $value): User
    {
        $this->username = $value;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setName(?string $value): User
    {
        $this->name = $value;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setPassword(?string $value): User
    {
        $this->password = $value;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPlainPassword($value): User
    {
        $this->plainPassword = $value;

        return $this;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setEmail(?string $value): User
    {
        $this->email = $value;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setIsActive(?bool $value): User
    {
        $this->isActive = $value;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function addRole($value): User
    {
        $this->roles->add($value);

        return $this;
    }

    public function removeRole($value): void
    {
        $this->roles->removeElement($value);
    }

    public function addReminder($value): User
    {
        $this->reminders->add($value);

        return $this;
    }

    public function removeReminder($value): void
    {
        $this->reminders->removeElement($value);
    }

    public function getReminders(): Collection
    {
        return $this->reminders;
    }

    public function isAccountNonExpired(): bool
    {
        return true;
    }

    public function isAccountNonLocked(): bool
    {
        return true;
    }

    public function isCredentialsNonExpired(): bool
    {
        return true;
    }

    public function isEnabled(): ?bool
    {
        return $this->isActive;
    }

    public function eraseCredentials(): void
    {
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $value): User
    {
        $this->customer = $value;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getRoles(): array
    {
        return array_map(static fn (Role $role): string => (string) $role, array_merge([new Role('ROLE_USER', 'User')], $this->roles->toArray()));
    }

    /**
     * @return Collection<Role>
     */
    public function getRolesEntity(): Collection
    {
        return $this->roles;
    }

    public function getAssignedRoles(): Collection
    {
        return $this->roles;
    }

    public function setAssignedRoles(Collection $value): void
    {
        $this->roles = $value;
    }

    public function serialize(): ?string
    {
        return serialize([$this->id, $this->username, $this->password, $this->isActive]);
    }

    public function unserialize($serialized): void
    {
        [
            $this->id,
            $this->username,
            $this->password,
            $this->isActive,
        ] = unserialize($serialized);
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }

    public function getPasswordResetToken(): ?string
    {
        return $this->passwordResetToken;
    }

    public function setPasswordResetToken(?string $value): void
    {
        $this->passwordResetToken = $value;
    }

    public function getPasswordResetTokenExpiry(): ?\DateTimeInterface
    {
        return $this->passwordResetTokenExpiry;
    }

    public function setPasswordResetTokenExpiry(?\DateTimeInterface $value): void
    {
        $this->passwordResetTokenExpiry = $value;
    }

    public function getContact(): ?Contact
    {
        return $this->contact;
    }

    public function setContact(?Contact $value): User
    {
        $this->contact = $value;
        $value->setUser($this);

        return $this;
    }

    public function addCustomerAccount($value): User
    {
        $this->customerAccounts->add($value);

        return $this;
    }

    public function removeCustomerAccount($value): void
    {
        $this->customerAccounts->remove($value);
    }

    public function getCustomerAccounts()
    {
        return $this->customerAccounts();
    }

    public function getJobs(): Collection
    {
        return $this->jobs;
    }

    public function removeJob($value): User
    {
        $this->jobs->removeElement($value);

        return $this;
    }

    public function addJob($value): User
    {
        if (!$this->jobs->contains($value)) {
            $this->jobs->add($value);
        }

        return $this;
    }

    public function getVan(): ?Location
    {
        return $this->van;
    }

    public function setVan(?Location $value): User
    {
        $this->van = $value;

        return $this;
    }

    public function getCostMultiplier(): float|int
    {
        return $this->costMultiplier != null ? $this->costMultiplier : self::DEFAULT_RATE_MULTIPLIER;
    }

    public function setCostMultiplier(?string $value): User
    {
        $this->costMultiplier = $value;

        return $this;
    }
}
