<?php

namespace App\Entity;

use App\Model\ChargeMethod;
use App\Model\JobStage;
use App\Model\SearchableInterface;
use App\Model\Warranty;
use App\Util\StringHelpers;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Job.
 *
 * @ORM\Table(name="job", indexes={
 *      @ORM\Index(name="IDX_do_with_job_deleted", columns={"doWithJob_id", "deletedAt"})
 * })
 * @ORM\Entity(repositoryClass="App\Entity\Repository\JobRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
#[Assert\Callback(callback: 'validateForCompletion', groups: ['CompleteJob'])]
class Job implements SearchableInterface
{
    final public const PATH = '/job-images/%s';
    final public const DEFAULT_SALES_JOB = 'Dawson MHE Ltd';

    /**
     * Not persisted - form use only.
     */
    public $assetType;

    /**
     * @ORM\Column(name="uuid", type="text", nullable=true)
     */
    protected ?string $uuid = null;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(name="job_code", type="integer", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?int $jobCode = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Customer", cascade={"persist"}, inversedBy="jobs")
     * @Gedmo\Versioned
     */
    protected ?Customer $customer = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AssetLifecyclePeriod", cascade={"persist"}, inversedBy="jobs")
     * @ORM\JoinColumn(name="assetLifecyclePeriod_id", onDelete="SET NULL")
     * @Gedmo\Versioned
     */
    protected ?AssetLifecyclePeriod $assetLifecyclePeriod = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SiteAddress", cascade={"persist"}, inversedBy="job")
     * @ORM\JoinColumn(name="siteAddress_id")
     * @Gedmo\Versioned
     */
    protected ?SiteAddress $siteAddress = null;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", cascade={"persist"}, inversedBy="jobs")
     * @var Collection<User>
     */
    protected Collection $engineers;

    /**
     * @ORM\Column(name="detail", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $detail = null;

    /**
     * @ORM\Column(name="notes", type="string", length=255, nullable=true)
     * @Gedmo\Versioned
     * @ORM\OrderBy({"date" = "DESC"})
     */
    protected ?string $notes = null;

    /**
     * @ORM\Column(name="job_active_from", type="datetime")
     * @Gedmo\Versioned
     */
    #[Assert\NotNull(message: 'Required')]
    protected ?\DateTimeInterface $jobActiveFrom;

    /**
     * @var bool
     *
     * @ORM\Column(name="cancelled", type="boolean", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?bool $cancelled = null;

    /**
     * @var bool
     *
     * @ORM\Column(name="signed_off", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $signedOff = false;

    /**
     * @ORM\Column(name="cancellation_reason", type="string", length=255, nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $cancellationReason = null;

    /**
     * @ORM\Column(name="accepted", type="boolean", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?bool $accepted = null;

    /**
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    protected ?\DateTimeInterface $createdAt = null;

    /**
     * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    protected ?\DateTimeInterface $updatedAt = null;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", cascade={"persist"}, inversedBy="jobs")
     * @Gedmo\Versioned
     */
    protected ?User $deletedBy = null;

    /**
     * @ORM\Column(name="completion_stage", type="integer")
     */
    protected ?int $completionStage = 0;

    /**
     * @ORM\Column(name="completion_stage_overridden", type="boolean")
     */
    protected ?bool $completionStageOverridden = false;

    /**
     * @ORM\Column(name="completed", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $completed = false;

    /**
     * @ORM\Column(name="is_live", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $isLive = false;

    /**
     * @ORM\Column(name="urgent", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $urgent = false;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Estimate", mappedBy="job", cascade={"persist"})
     * @var Collection<Estimate>
     */
    protected Collection $estimates;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PurchaseOrder", mappedBy="job", cascade={"persist"})
     * @var Collection<PurchaseOrder>
     */
    protected Collection $purchaseOrders;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\WorkLog", mappedBy="job", cascade={"persist"})
     * @var Collection<WorkLog>
     */
    protected Collection $workLogs;

    /**
     * @ORM\Column(name="paid", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $paid = false;

    /**
     * @ORM\Column(name="paid_at", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $paidAt = null;

    /**
     * @ORM\Column(name="charge_method", type="integer", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?int $chargeMethod = 0;

    /**
     * @ORM\Column(name="client_charged_reason", type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $clientChargedReason = null;

    /**
     * @ORM\Column(name="invoiced", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $invoiced = false;

    /**
     * @ORM\Column(name="invoiced_at", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $invoicedAt = null;

    /**
     * @ORM\Column(name="completed_at", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $completedAt = null;

    /**
     * @ORM\Column(name="do_with_next_job", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $doWithNextJob = false;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Job", inversedBy="attachedJobs", cascade={"persist"})
     * @ORM\JoinColumn(name="doWithJob_id", onDelete="SET NULL")
     * @Gedmo\Versioned
     */
    protected ?Job $doWithJob = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Job", mappedBy="doWithJob", cascade={"persist"})
     * @var Collection<Job>
     */
    protected Collection $attachedJobs;

    /**
     * @ORM\Column(name="is_service_job", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $isServiceJob = false;

    /**
     * @ORM\Column(name="is_loler_job", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $isLolerJob = false;

    /**
     * @ORM\Column(name="is_routine_maintenance", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $isRoutineMaintenance = false;

    /**
     * @ORM\Column(name="is_general_repairs", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $isGeneralRepairs = false;

    /**
     * @ORM\Column(name="is_sales_job", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $isSalesJob = false;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\StockItem", mappedBy="job", cascade={"persist"})
     * @var Collection<StockItem>
     */
    protected Collection $attachedStockItems;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Contact", inversedBy="jobs", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @var Collection<Contact>
     */
    protected Collection $contacts;

    /**
     * @ORM\Column(name="parts_ordered", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $partsOrdered = false;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Invoice", mappedBy="job", cascade={"persist", "remove"})
     * @Gedmo\Versioned
     */
    protected ?Invoice $invoice = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Signature", inversedBy="job", cascade={"persist", "remove"})
     */
    protected ?Signature $signature = null;

    /**
     * @ORM\Column(name="signature_email_sent", type="boolean", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?bool $signatureEmailSent = false;

    /**
     * @ORM\Column(name="signature_email_send_at", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $signatureEmailSendAt = null;

    /**
     * @ORM\Column(name="signature_email_resent", type="boolean", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?bool $signatureEmailResent = false;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EstimateItem", mappedBy="job", cascade={"persist"})
     * @var Collection<int, EstimateItem>|EstimateItem[]
     */
    protected Collection $partsRequisition;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Image", mappedBy="job", cascade={"persist"})
     * @var Collection<Image>
     */
    protected Collection $images;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FutureWorkRecommendation", mappedBy="job", cascade={"persist"})
     * @var Collection<FutureWorkRecommendation>
     */
    protected Collection $futureWorkRecommendations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\StockTransaction", mappedBy="job", cascade={"persist"})
     * @var Collection<StockTransaction>
     */
    protected Collection $stockTransactions;

    /**
     * @ORM\Column(name="new_job_notification_email_sent", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $newJobNotificationEmailSent = false;

    /**
     * @ORM\Column(name="finalCost", type="decimal", nullable=true, precision=20, scale=4)
     * @Gedmo\Versioned
     */
    protected ?float $finalCost = null;

    /**
     * @ORM\Column(name="finalCostInternal", type="decimal", nullable=true, precision=20, scale=4)
     * @Gedmo\Versioned
     */
    protected ?float $finalCostInternal = null;

    /**
     * @ORM\Column(name="hour_reading", type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $hourReading = null;

    /**
     * @ORM\Column(name="is_recurring", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $isRecurring = false;

    /**
     * @ORM\Column(name="recurring_type", type="integer", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?int $recurringType = 0;

    /**
     * @ORM\Column(name="job_recurring_time", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $jobRecurringTime = null;

    /**
     * @ORM\Column(name="сall_out_charge", type="decimal")
     * @Gedmo\Versioned
     */
    protected ?float $callOutCharge = 0;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SalesJob", inversedBy="purchaseOrders")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected ?\App\Entity\SalesJob $salesJob = null;

    /**
     * @ORM\Column(name="archived", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $archived = false;

    /**
     * @ORM\Column(name="archived_at", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $archivedAt = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $reasonDeleted = null;

    public function __construct()
    {
        $this->images                    = new ArrayCollection();
        $this->workLogs                  = new ArrayCollection();
        $this->contacts                  = new ArrayCollection();
        $this->engineers                 = new ArrayCollection();
        $this->estimates                 = new ArrayCollection();
        $this->attachedJobs              = new ArrayCollection();
        $this->purchaseOrders            = new ArrayCollection();
        $this->partsRequisition          = new ArrayCollection();
        $this->stockTransactions         = new ArrayCollection();
        $this->attachedStockItems        = new ArrayCollection();
        $this->futureWorkRecommendations = new ArrayCollection();

        $this->uuid = str_replace("\n", '', shell_exec('uuidgen -r'));
    }

    public static function getSearchFields(): array
    {
        return ['id'      => 'ID', 'jobCode' => 'jobCode', 'detail'  => 'detail', 'notes'   => 'notes'];
    }

    public static function getSearchCategory(): string
    {
        return 'job';
    }

    public function getDisplayTitle(): string
    {
        $parts = [$this->jobCode];

        if ($this->customer) {
            $parts[] = $this->customer->getName();
        } else {
            $parts[] = 'No customer';
        }

        if ($asset = $this->getAsset()) {
            $parts[] = $asset->getAssetType();
            $parts[] = $asset->getNameString();
            if ($asset->getClientFleet()) {
                $parts[] =  'Client Fleet No. ' . $asset->getClientFleet();
            }
        } else {
            $parts[] = 'No asset';
        }

        return implode(' - ', $parts);
    }

    public function getCustomerName(): ?string
    {
        return $this->customer ? $this->customer->getName() : 'None';
    }

    public function getJobCategoryText(): string
    {
        if ($this->getUrgent()) {
            return 'Breakdown';
        }

        if ($this->getIsGeneralRepairs()) {
            return 'General repairs';
        }

        if ($this->getIsRoutineMaintenance()) {
            return 'Routine maintenance';
        }

        if ($this->getIsLolerJob()) {
            return 'LOLER Job';
        }

        if ($this->getIsServiceJob()) {
            return 'Service Job';
        }

        if ($this->getIsSalesJob()) {
            return 'Sales Job';
        }

        return 'Standard';
    }

    /**
     * @return mixed[]
     */
    public function getRequestedParts(): array
    {
        $requests = [];
        $workLogs = $this->getWorkLogs();

        foreach ($workLogs as $workLog) {
            if ($workLog->getRequestedParts()) {
                $requests[$workLog->getId()] = $workLog;
            }
        }

        return $requests;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function addEstimate($value): Job
    {
        $this->estimates->add($value);

        return $this;
    }

    public function removeEstimate($value): void
    {
        $this->estimates->removeElement($value);
    }

    public function getEstimates()
    {
        return $this->estimates->toArray();
    }

    public function addAttachedJob($value): Job
    {
        $this->attachedJobs->add($value);

        return $this;
    }

    public function removeAttachedJob($value): void
    {
        $this->attachedJobs->removeElement($value);
    }

    public function getAttachedJobs()
    {
        return $this->attachedJobs->toArray();
    }

    public function addPurchaseOrder($value): Job
    {
        $this->purchaseOrders->add($value);

        return $this;
    }

    public function removePurchaseOrder($value): void
    {
        $this->purchaseOrders->removeElement($value);
    }

    public function getPurchaseOrders()
    {
        return $this->purchaseOrders->toArray();
    }

    public function addWorkLog($value): Job
    {
        $this->workLogs->add($value);

        return $this;
    }

    public function removeWorkLog($value): void
    {
        $this->workLogs->removeElement($value);
    }

    public function getWorkLogs()
    {
        return $this->workLogs->toArray();
    }

    public function addContact($value): self
    {
        $this->contacts->add($value);

        return $this;
    }

    public function removeContact($value): void
    {
        $this->contacts->removeElement($value);
    }

    public function getContacts()
    {
        return $this->contacts->toArray();
    }

    public function addPartsRequisition($value): self
    {
        $this->partsRequisition->add($value);

        return $this;
    }

    public function removePartsRequisition($value): void
    {
        $this->partsRequisition->removeElement($value);
    }

    public function getPartsRequisitions()
    {
        return $this->partsRequisition->toArray();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setNotes(?string $notes): Job
    {
        $this->notes = $notes;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setDueDate($dueDate): Job
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    public function getDueDate()
    {
        return $this->dueDate;
    }

    public function setJobActiveFrom(?\DateTimeInterface $value): Job
    {
        $this->jobActiveFrom = $value;

        return $this;
    }

    public function getJobActiveFrom(): ?\DateTimeInterface
    {
        return $this->jobActiveFrom;
    }

    public function setCancelled(?bool $cancelled): Job
    {
        $this->cancelled = $cancelled;

        return $this;
    }

    public function getCancelled(): ?bool
    {
        return $this->cancelled;
    }

    public function setSignedOff($signedOff): Job
    {
        $this->signedOff = $signedOff;

        if ($signedOff == true) {
            $this->saveCost();
        }

        return $this;
    }

    public function getSignedOff(): ?bool
    {
        return $this->signedOff;
    }

    public function setCancellationReason(?string $cancellationReason): Job
    {
        $this->cancellationReason = $cancellationReason;

        return $this;
    }

    public function getCancellationReason(): ?string
    {
        return $this->cancellationReason;
    }

    public function setCustomer(?Customer $customer = null): Job
    {
        $this->customer = $customer;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function getSignature(): ?Signature
    {
        return $this->signature;
    }

    public function setSignature(?Signature $signature = null): Job
    {
        $this->signature = $signature;

        return $this;
    }

    public function setAssetLifecyclePeriod(?AssetLifecyclePeriod $value = null): Job
    {
        $this->assetLifecyclePeriod = $value;

        return $this;
    }

    public function getAssetLifecyclePeriod(): ?AssetLifecyclePeriod
    {
        return $this->assetLifecyclePeriod;
    }

    public function getTruck(): ?Truck
    {
        return $this->getAsset() instanceof Truck ? $this->getAsset() : null;
    }

    public function getAsset(): ?Asset
    {
        return $this->assetLifecyclePeriod ? $this->assetLifecyclePeriod->getAsset() : null;
    }

    public function getAccepted(): ?bool
    {
        return $this->accepted;
    }

    public function setAccepted(?bool $value): Job
    {
        $this->accepted = $value;

        return $this;
    }

    public function getJobCode(): ?int
    {
        return $this->jobCode;
    }

    public function setJobCode(?int $value): Job
    {
        $this->jobCode = $value;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $value): void
    {
        $this->deletedAt = $value;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $value): void
    {
        $this->createdAt = $value;
    }

    public function getCompletionStage(): ?int
    {
        return $this->completionStage;
    }

    public function setCompletionStage($value): void
    {
        $this->completionStage = $value;

        if ($value < JobStage::COMPLETE) {
            $this->setCompleted(false);
            $this->setCompletedAt(null);
        }
    }

    public function getCompletionStageOverridden(): ?bool
    {
        return $this->completionStageOverridden;
    }

    public function setCompletionStageOverridden(?bool $value): Job
    {
        $this->completionStageOverridden = $value;

        return $this;
    }

    public function getCompleted(): ?bool
    {
        return $this->completed;
    }

    public function setCompleted(?bool $value): void
    {
        $this->completed = $value;
    }

    public function isLive(): ?bool
    {
        return $this->isLive;
    }

    public function getIsLive(): ?bool
    {
        return $this->isLive;
    }

    public function setIsLive(?bool $value): void
    {
        $this->isLive = $value;

        if ($this->isLive == false) {
            $this->saveCost();
        }
    }

    public function isUrgent(): ?bool
    {
        return $this->urgent;
    }

    public function getUrgent(): ?bool
    {
        return $this->urgent;
    }

    public function setUrgent(?bool $value): void
    {
        $this->urgent = $value;
    }

    public function getEstimate()
    {
        return $this->estimate;
    }

    public function setEstimate($value): void
    {
        $this->estimate = $value;
    }

    public function getPaid(): ?bool
    {
        return $this->paid;
    }

    public function setPaid(?bool $value): void
    {
        $this->paid = $value;
    }

    public function getPaidAt(): ?\DateTimeInterface
    {
        return $this->paidAt;
    }

    public function setPaidAt(?\DateTimeInterface $value): void
    {
        $this->paidAt = $value;
    }

    public function getClientChargedReason(): ?string
    {
        return $this->clientChargedReason;
    }

    public function setClientChargedReason(?string $value): void
    {
        $this->clientChargedReason = $value;
    }

    public function getChargeMethod(): ?int
    {
        return $this->chargeMethod;
    }

    public function setChargeMethod(?int $value): void
    {
        $this->chargeMethod = $value;
    }

    public function getInvoiced(): ?bool
    {
        return $this->invoiced;
    }

    public function setInvoiced(?bool $value): void
    {
        $this->invoiced = $value;
    }

    public function getInvoicedAt(): ?\DateTimeInterface
    {
        return $this->invoicedAt;
    }

    public function setInvoicedAt(?\DateTimeInterface $value): void
    {
        $this->invoicedAt = $value;
    }

    public function getDoWithNextJob(): ?bool
    {
        return $this->doWithNextJob;
    }

    public function setDoWithNextJob(?bool $value): void
    {
        $this->doWithNextJob = $value;
    }

    public function getDoWithJob(): ?Job
    {
        return $this->doWithJob;
    }

    public function setDoWithJob(?Job $value): void
    {
        $this->doWithJob = $value;
    }

    public function getEngineers(): Collection
    {
        return $this->engineers;
    }

    public function removeEngineer($value): Job
    {
        $this->engineers->removeElement($value);

        return $this;
    }

    public function addEngineer($value): Job
    {
        if (!$this->engineers->contains($value)) {
            $this->engineers->add($value);
        }

        return $this;
    }

    public function removeAllEngineersFromJob(): Job
    {
        $this->engineers = new ArrayCollection();

        return $this;
    }

    public function getEngineer()
    {
        return $this->engineers ? ($this->engineers->first() ?: null) : null;
    }

    public function getIsServiceJob(): ?bool
    {
        return $this->isServiceJob;
    }

    public function setIsServiceJob(?bool $value): Job
    {
        $this->isServiceJob = $value;

        return $this;
    }

    public function getIsSalesJob(): ?bool
    {
        return $this->isSalesJob;
    }

    public function setIsSalesJob(?bool $value)
    {
        $this->isSalesJob = $value;

        return $this;
    }

    public function getIsLolerJob(): ?bool
    {
        return $this->isLolerJob;
    }

    public function setIsLolerJob(?bool $value): Job
    {
        $this->isLolerJob = $value;

        return $this;
    }

    public function getIsRoutineMaintenance(): ?bool
    {
        return $this->isRoutineMaintenance;
    }

    public function setIsRoutineMaintenance(?bool $value): Job
    {
        $this->isRoutineMaintenance = $value;

        return $this;
    }

    public function getIsGeneralRepairs(): ?bool
    {
        return $this->isGeneralRepairs;
    }

    public function setIsGeneralRepairs(?bool $value): Job
    {
        $this->isGeneralRepairs = $value;

        return $this;
    }

    public function getDetail(): ?string
    {
        return $this->detail;
    }

    public function setDetail(?string $value): Job
    {
        $this->detail = $value;

        return $this;
    }

    public function getPartsOrdered(): ?bool
    {
        return $this->partsOrdered;
    }

    public function setPartsOrdered(?bool $value): Job
    {
        $this->partsOrdered = $value;

        return $this;
    }

    public function getSiteAddress(): ?SiteAddress
    {
        return $this->siteAddress;
    }

    public function setSiteAddress(?SiteAddress $value): Job
    {
        $this->siteAddress = $value;

        return $this;
    }

    public function getCompletedAt(): ?\DateTimeInterface
    {
        return $this->completedAt;
    }

    public function setCompletedAt(?\DateTimeInterface $value): Job
    {
        $this->completedAt = $value;

        return $this;
    }

    public function addAttachedStockItem($value): Job
    {
        $this->attachedStockItems->add($value);

        return $this;
    }

    public function removeAttachedStockItem($value): Job
    {
        $this->attachedStockItems->removeElement($value);

        return $this;
    }

    public function getAttachedStockItems()
    {
        return $this->attachedStockItems->toArray();
    }

    public function addImage($value): Job
    {
        $this->images->add($value);
        $value->setJob($this);

        return $this;
    }

    public function removeImage($value): Job
    {
        $this->images->removeElement($value);

        return $this;
    }

    public function getImages(): Collection
    {
        return $this->images;
    }

    public function getAttachedStockCost($asString = true): string|float
    {
        $total = 0.0;

        foreach ($this->attachedStockItems as $stockItem) {
            $cost  = ($this->getChargeMethod() !== ChargeMethod::INVOICE_CUSTOMER || $this->customer->getHasCostPriceStock()) ? $stockItem->getCostPrice() : $stockItem->getIssuePrice();
            $total += $cost;
        }

        foreach ($this->purchaseOrders as $purchaseOrder) {
            foreach ($purchaseOrder->getEstimateItems() as $item) {
                if (!$item->getReceivedDate()) {
                    continue;
                }

                $total += (($this->getChargeMethod() !== ChargeMethod::INVOICE_CUSTOMER || $this->customer->getHasCostPriceStock()) ? $item->getPrice() : $item->getRetailPrice()) * $item->getQuantity();
            }
        }

        if ($this->getChargeMethod() !== ChargeMethod::CHARGE_AGAINST_MAINTENANCE
           && $this->getChargeMethod() !== ChargeMethod::WARRANTY
           && !$this->getSignedOff()) {
            $total = $total - ($total * ($this->customer->getPartsDiscount() / 100));
        }

        return $asString ? number_format($total, 2) : $total;
    }

    public function getTimeLogged($asString = true): int|float|string
    {
        $minutes = 0;

        foreach ($this->workLogs as $workLog) {
            $minutes += $workLog->getTimeLoggedRaw();
        }

        if (!$asString) {
            return $minutes;
        }

        return sprintf('%dh %dm', floor($minutes / 60), $minutes % 60);
    }

    public function getWorkLogTimeLabour(): string
    {
        $total = null;
        $totalOvertime = null;
        $totalExtremeOvertime = null;
        foreach ($this->workLogs as $workLog) {
            $time         = $workLog->getTimeLabour();
            $total += $time;

            $timeOvertime = $workLog->getTimeLabourOvertime();
            $totalOvertime += $timeOvertime;

            $timeExtremeOvertime = $workLog->getTimeLabourExtremeOvertime();
            $totalExtremeOvertime += $timeExtremeOvertime;
        }

        return StringHelpers::formatWorklogTimesAsString($total, $totalOvertime, $totalExtremeOvertime);
    }

    public function getWorkLogTimeTravel(): string
    {
        $total = null;
        $totalOvertime = null;
        $totalExtremeOvertime = null;
        foreach ($this->workLogs as $workLog) {
            $time         = $workLog->getTimeTravel();
            $total += $time;

            $timeOvertime = $workLog->getTimeTravelOvertime();
            $totalOvertime += $timeOvertime;

            $timeExtremeOvertime = $workLog->getTimeTravelExtremeOvertime();
            $totalExtremeOvertime += $timeExtremeOvertime;
        }

        return StringHelpers::formatWorklogTimesAsString($total, $totalOvertime, $totalExtremeOvertime);
    }

    public function getLabourCost($asString = true): string|float
    {
        $total = 0.0;

        foreach ($this->workLogs as $workLog) {
            $rate  = ($this->getChargeMethod() !== ChargeMethod::INVOICE_CUSTOMER || !$this->customer) ? $workLog->getLabourRateInternal() : $workLog->getLabourRate();
            $total += $this->getWorklogLabourCost($workLog, $rate);
        }

        return $asString ? number_format($total, 2) : $total;
    }

    public function getTravelCost($asString = true): string|float
    {
        $total = 0.0;

        foreach ($this->workLogs as $workLog) {
            $rate  = ($this->getChargeMethod() !== ChargeMethod::INVOICE_CUSTOMER || !$this->customer) ? $workLog->getLabourRateInternal() : $workLog->getLabourRate();
            $total += $this->getWorklogTravelCost($workLog, $rate);
        }

        return $asString ? number_format($total, 2) : $total;
    }

    public function getTotalCost(): int|float
    {
        return $this->getLabourCost(false) + $this->getTravelCost(false) + $this->getAttachedStockCost(false) + $this->getCallOutCharge();
    }

    public function getTotalJobCost(): string
    {
        $total = 0.0;

        $timeLabour = $this->getLabourCost(false);
        $timeTravel = $this->getTravelCost(false);
        $partsCost  = $this->getAttachedStockCost(false);
        $total      = $timeLabour + $timeTravel + $partsCost + $this->getCallOutCharge();

        return number_format($total, 2);
    }

    public function getInvoice(): ?Invoice
    {
        return $this->invoice;
    }

    public function setInvoice(?Invoice $value): Job
    {
        $this->invoice = $value;

        return $this;
    }

    public function addFutureWorkRecommendation($value): Job
    {
        $this->futureWorkRecommendations->add($value);
        $value->setJob($this);

        return $this;
    }

    public function removeFutureWorkRecommendation($value): void
    {
        $this->futureWorkRecommendations->removeElement($value);
    }

    public function getFutureWorkRecommendations(): Collection
    {
        return $this->futureWorkRecommendations;
    }

    public function getJobIdentifier(): string
    {
        return $this->id . ' - ' . $this->getCustomerName();
    }

    public function addStockTransaction($value): Job
    {
        $this->stockTransactions->add($value);

        return $this;
    }

    public function removeStockTransaction($value): void
    {
        $this->stockTransactions->removeElement($value);
    }

    public function getStockTransactions()
    {
        return $this->stockTransactions->toArray();
    }

    public function getNewJobNotificationEmailSent(): ?bool
    {
        return $this->newJobNotificationEmailSent;
    }

    public function setNewJobNotificationEmailSent(?bool $value): Job
    {
        $this->newJobNotificationEmailSent = $value;

        return $this;
    }

    public function getFinalCost(): ?string
    {
        return $this->finalCost;
    }

    public function setFinalCost(?string $value): Job
    {
        $this->finalCost = $value;

        return $this;
    }

    public function saveFinalCost(): Job
    {
        $finalCost = $this->getLabourCost() + $this->getTravelCost() + $this->getAttachedStockCost(false);

        return $this->setFinalCost($finalCost);
    }

    public function getFinalCostInternal(): ?string
    {
        return $this->finalCostInternal;
    }

    public function setFinalCostInternal(?string $value): Job
    {
        $this->finalCostInternal = $value;

        return $this;
    }

    public function saveCost(): Job
    {
        $totalPartCostPrice = 0.0;
        $totalPartIssuePrice = 0.0;

        foreach ($this->getAttachedStockItems() as $attachedStockItem) {
            $totalPartCostPrice += $attachedStockItem->getCostPrice();
            $totalPartIssuePrice += $attachedStockItem->getIssuePrice();
        }

        foreach ($this->purchaseOrders as $purchaseOrder) {
            foreach ($purchaseOrder->getEstimateItems() as $item) {
                if (!$item->getReceivedDate()) {
                    continue;
                }

                $totalPartCostPrice += $item->getPrice();
                $totalPartIssuePrice += $item->getRetailPrice();
            }
        }

        $finalCost = $this->getLabourCost(false) + $this->getTravelCost(false) + $totalPartIssuePrice + $this->getCallOutCharge();
        $finalCostInternal = $this->getInternalLabourCost(false) + $this->getInternalTravelCost(false) + $totalPartCostPrice;

        $this->setFinalCost($finalCost);
        $this->setFinalCostInternal($finalCostInternal);

        return $this;
    }

    public function getWarranties(): array
    {
        /** @var AssetLifecyclePeriod $assetLifecyclePeriod */
        $assetLifecyclePeriod = $this->getAssetLifecyclePeriod();
        /** @var Asset $asset */
        if ($assetLifecyclePeriod) {
            $asset = $assetLifecyclePeriod->getAsset();
        }

        if (!$assetLifecyclePeriod || !$asset) {
            return [];
        }

        $warranties = [];

        if ($asset->getManufacturersWarranty() && $asset->getManufacturersWarrantyDuration() && $asset->getPurchasedDate()) {
            $endDate = date('Y-m-d', strtotime('+' . $asset->getManufacturersWarrantyDuration() . ' MONTH', strtotime($asset->getPurchasedDate()->format('Y-m-d'))));
            $warranty = [
                'provider' => 'Manufacturer',
                'type' => Warranty::CHOICES[$asset->getManufacturersWarranty()],
                'endDate' => $endDate,
            ];
            array_push($warranties, $warranty);
        }
        if ($asset->getSecondLevelManufacturersWarranty() && $asset->getSecondLevelManufacturersWarrantyDuration() && $asset->getPurchasedDate()) {
            $endDate = date('Y-m-d', strtotime('+' . $asset->getSecondLevelManufacturersWarrantyDuration() . ' MONTH', strtotime($asset->getPurchasedDate()->format('Y-m-d'))));
            $warranty = [
                'provider' => 'Manufacturer',
                'type' => Warranty::CHOICES[$asset->getSecondLevelManufacturersWarranty()],
                'endDate' => $endDate,
            ];
            array_push($warranties, $warranty);
        }
        if ($assetLifecyclePeriod->getSaleWarranty() && $assetLifecyclePeriod->getSaleWarrantyDuration() && $assetLifecyclePeriod->getSaleWarrantyStartDate()) {
            $endDate = date('Y-m-d', strtotime('+' . $assetLifecyclePeriod->getSaleWarrantyDuration() . ' MONTH', strtotime($assetLifecyclePeriod->getSaleWarrantyStartDate()->format('Y-m-d'))));
            $warranty = [
                'provider' => 'Dawson',
                'type' => Warranty::CHOICES[$assetLifecyclePeriod->getSaleWarranty()],
                'endDate' => $endDate,
            ];
            array_push($warranties, $warranty);
        }

        return array_filter($warranties, fn ($warranty): bool => $warranty['endDate'] > date('Y-m-d'));
    }

    public function isWarranty(): bool
    {
        /** @var AssetLifecyclePeriod $assetLifecyclePeriod */
        $assetLifecyclePeriod = $this->getAssetLifecyclePeriod();
        /** @var Asset $asset */
        if ($assetLifecyclePeriod) {
            $asset = $assetLifecyclePeriod->getAsset();
        }

        if (!$assetLifecyclePeriod || !$asset) {
            return false;
        }

        $warranty = false;

        if ($asset->getManufacturersWarranty() && $asset->getManufacturersWarrantyDuration() && $asset->getPurchasedDate()) {
            $endDate = date('Y-m-d', strtotime('+' . $asset->getManufacturersWarrantyDuration() . ' MONTH', strtotime($asset->getPurchasedDate()->format('Y-m-d'))));
            if (!$warranty) {
                $warranty = $endDate > date('Y-m-d') ? true : false;
            }
        }
        if ($asset->getSecondLevelManufacturersWarranty() && $asset->getSecondLevelManufacturersWarrantyDuration() && $asset->getPurchasedDate()) {
            $endDate = date('Y-m-d', strtotime('+' . $asset->getSecondLevelManufacturersWarrantyDuration() . ' MONTH', strtotime($asset->getPurchasedDate()->format('Y-m-d'))));
            if (!$warranty) {
                $warranty = $endDate > date('Y-m-d') ? true : false;
            }
        }
        if ($assetLifecyclePeriod->getSaleWarranty() && $assetLifecyclePeriod->getSaleWarrantyDuration() && $assetLifecyclePeriod->getSaleWarrantyStartDate()) {
            $endDate = date('Y-m-d', strtotime('+' . $assetLifecyclePeriod->getSaleWarrantyDuration() . ' MONTH', strtotime($assetLifecyclePeriod->getSaleWarrantyStartDate()->format('Y-m-d'))));
            if (!$warranty) {
                $warranty = $endDate > date('Y-m-d') ? true : false;
            }
        }

        return $warranty;
    }

    public function setUuid(?string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getSignatureEmailSent(): ?bool
    {
        return $this->signatureEmailSent;
    }

    public function setSignatureEmailSent(?bool $signatureEmailSent): self
    {
        $this->signatureEmailSent = $signatureEmailSent;

        return $this;
    }

    /**
     * @return Collection|EstimateItem[]
     */
    public function getPartsRequisition(): Collection
    {
        return $this->partsRequisition;
    }

    public function getHourReading(): ?string
    {
        return $this->hourReading;
    }

    public function setHourReading(?string $hourReading): self
    {
        $this->hourReading = $hourReading;

        return $this;
    }

    public function getSignatureEmailSendAt(): ?\DateTimeInterface
    {
        return $this->signatureEmailSendAt;
    }

    public function setSignatureEmailSendAt(?\DateTimeInterface $signatureEmailSendAt): self
    {
        $this->signatureEmailSendAt = $signatureEmailSendAt;

        return $this;
    }

    public function getSignatureEmailResent(): ?bool
    {
        return $this->signatureEmailResent;
    }

    public function setSignatureEmailResent(?bool $signatureEmailResent): self
    {
        $this->signatureEmailResent = $signatureEmailResent;

        return $this;
    }

    public function getReasonDeleted(): ?string
    {
        return $this->reasonDeleted;
    }

    public function setReasonDeleted(?string $reasonDeleted): self
    {
        $this->reasonDeleted = $reasonDeleted;

        return $this;
    }

    public function getDeletedBy(): ?User
    {
        return $this->deletedBy;
    }

    public function setDeletedBy(?User $deletedBy): self
    {
        $this->deletedBy = $deletedBy;

        return $this;
    }

    public function getIsRecurring(): ?bool
    {
        return $this->isRecurring;
    }

    public function setIsRecurring(bool $isRecurring): self
    {
        $this->isRecurring = $isRecurring;

        return $this;
    }

    public function getRecurringType(): ?int
    {
        return $this->recurringType;
    }

    public function setRecurringType(?int $recurringType): self
    {
        $this->recurringType = $recurringType;

        return $this;
    }

    public function getJobRecurringTime(): ?\DateTimeInterface
    {
        return $this->jobRecurringTime;
    }

    public function setJobRecurringTime(\DateTimeInterface $jobRecurringTime): self
    {
        $this->jobRecurringTime = $jobRecurringTime;

        return $this;
    }

    public function getCallOutCharge(): ?float
    {
        return $this->callOutCharge;
    }

    public function setCallOutCharge(float $callOutCharge): self
    {
        $this->callOutCharge = $callOutCharge;

        return $this;
    }

    public function getSalesJob(): ?SalesJob
    {
        return $this->salesJob;
    }

    public function setSalesJob(?SalesJob $salesJob): self
    {
        $this->salesJob = $salesJob;

        return $this;
    }

    public function getArchived(): ?bool
    {
        return $this->archived;
    }

    public function setArchived(?bool $value): void
    {
        $this->archived = $value;
    }

    public function getArchivedAt(): ?\DateTimeInterface
    {
        return $this->archivedAt;
    }

    public function setArchivedAt(?\DateTimeInterface $value): void
    {
        $this->archivedAt = $value;
    }

    private function getWorklogLabourCost($workLog, $rate): string
    {
        $time         = $workLog->getAdjustedTotalLabourTime();
        $workLogCost  = ($rate / 60) * $time;

        if ($userMultiplier = $workLog->getUserCostMultiplier()) {
            $workLogCost = $workLogCost * $userMultiplier;
        }

        return $workLogCost;
    }

    private function getInternalLabourCost(bool $asString = true)
    {
        $total = 0.0;
        foreach ($this->workLogs as $workLog) {
            $rate  = $workLog->getLabourRateInternal();
            $total += $this->getWorklogLabourCost($workLog, $rate);
        }

        return $asString ? number_format($total, 2) : $total;
    }

    private function getWorklogTravelCost($workLog, $rate): int|float
    {
        $time         = $workLog->getAdjustedTotalTravelTime();
        $workLogCost  = ($rate / 60) * $time;

        if ($userMultiplier = $workLog->getUserCostMultiplier()) {
            $workLogCost = $workLogCost * $userMultiplier;
        }

        return $workLogCost;
    }

    private function getInternalTravelCost(bool $asString = true): string|float
    {
        $total = 0.0;

        foreach ($this->workLogs as $workLog) {
            $rate  = $workLog->getLabourRateInternal();
            $total += $this->getWorklogTravelCost($workLog, $rate);
        }

        return $asString ? number_format($total, 2) : $total;
    }
}
