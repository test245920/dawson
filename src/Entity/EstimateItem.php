<?php

namespace App\Entity;

use App\Model\SearchableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * EstimateItem.
 *
 * @ORM\Table(name="estimate_item")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\EstimateItemRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 */
class EstimateItem implements SearchableInterface
{
    final public const PATH = '/estimate-item-images/%s';

    /**
     * @ORM\Column(name="uuid", type="text", nullable=true)
     */
    protected ?string $uuid = null;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Gedmo\Versioned
     */
    protected ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Estimate", cascade={"persist"}, inversedBy="estimateItems")
     * @Gedmo\Versioned
     */
    protected ?Estimate $estimate = null;

    /**
     * @ORM\JoinColumn(name="purchaseOrder_id")
     * @ORM\ManyToOne(targetEntity="App\Entity\PurchaseOrder", cascade={"persist"}, inversedBy="estimateItems")
     * @Gedmo\Versioned
     */
    protected ?PurchaseOrder $purchaseOrder = null;

    /**
     * @ORM\Column(name="item", type="string", length=255)
     * @Gedmo\Versioned
     */
    #[Assert\NotBlank(message: 'Required')]
    protected ?string $item = null;

    /**
     * @ORM\Column(name="price", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    #[Assert\NotBlank(message: 'Required')]
    protected ?float $price = 0.00;

    /**
     * @ORM\Column(name="quantity", type="integer", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?int $quantity = null;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(name="received_date", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $receivedDate = null;

    /**
     * @ORM\Column(name="part_number", type="string", length=255, nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $partNumber = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\job", cascade={"persist"}, inversedBy="partsRequisition")
     * @Gedmo\Versioned
     */
    protected ?job $job = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Image", mappedBy="estimateItem", cascade={"persist"})
     * @var Collection<Image>
     */
    protected Collection $images;

    /**
     * @ORM\Column(name="retail_price", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    #[Assert\NotBlank(message: 'Required')]
    protected ?float $retailPrice = 0.00;

    /**
     * @ORM\Column(name="usedOnjob", type="integer", options={"default": 0})
     * @Gedmo\Versioned
     */
    protected ?int $usedOnJob = 0;

    /**
     * @ORM\Column(name="lead_time", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $leadTime = null;

    /**
     * @ORM\Column(name="eta", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $eta = null;

    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->uuid   = str_replace("\n", '', shell_exec('uuidgen -r'));
    }

    public static function getSearchFields(): array
    {
        return ['id'    => 'ID', 'item'  => 'Item', 'price' => 'Price'];
    }

    public static function getSearchCategory(): string
    {
        return 'estimate_item';
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setEstimate(?Estimate $estimate): EstimateItem
    {
        $this->estimate = $estimate;

        return $this;
    }

    public function getEstimate(): ?Estimate
    {
        return $this->estimate;
    }

    public function setItem(?string $item): EstimateItem
    {
        $this->item = $item;

        return $this;
    }

    public function getItem(): ?string
    {
        return $this->item;
    }

    public function setPrice(?string $price): EstimateItem
    {
        $this->price = $price;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $value): void
    {
        $this->deletedAt = $value;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(?int $value): void
    {
        $this->quantity = $value;
    }

    public function getPurchaseOrder(): ?PurchaseOrder
    {
        return $this->purchaseOrder;
    }

    public function setPurchaseOrder(?PurchaseOrder $value): void
    {
        $this->purchaseOrder = $value;
    }

    public function setReceivedDate(?\DateTimeInterface $value): EstimateItem
    {
        $this->receivedDate = $value;

        return $this;
    }

    public function getReceivedDate(): ?\DateTimeInterface
    {
        return $this->receivedDate;
    }

    public function setJob(?job $value): EstimateItem
    {
        $this->job = $value;

        return $this;
    }

    public function getJob(): ?job
    {
        return $this->job;
    }

    public function setPartNumber(?string $value): EstimateItem
    {
        $this->partNumber = $value;

        return $this;
    }

    public function getPartNumber(): ?string
    {
        return $this->partNumber;
    }

    public function addImage($value): EstimateItem
    {
        $this->images->add($value);
        $value->setEstimateItem($this);

        return $this;
    }

    public function removeImage($value): EstimateItem
    {
        $this->images->removeElement($value);

        return $this;
    }

    public function getImages(): Collection
    {
        return $this->images;
    }

    public function getRetailPrice(): ?string
    {
        return $this->retailPrice;
    }

    public function setRetailPrice(?string $value): EstimateItem
    {
        $this->retailPrice = $value;

        return $this;
    }

    public function getUsedOnJob(): ?int
    {
        return $this->usedOnJob;
    }

    public function setUsedOnJob(int $value): EstimateItem
    {
        $this->usedOnJob = $value;

        return $this;
    }

    public function setUuid(?string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getLeadTime(): ?string
    {
        return $this->leadTime;
    }

    public function setLeadTime(?string $leadTime): self
    {
        $this->leadTime = $leadTime;

        return $this;
    }

    public function getEta(): ?\DateTimeInterface
    {
        return $this->eta;
    }

    public function setEta(?\DateTimeInterface $eta): self
    {
        $this->eta = $eta;

        return $this;
    }

    public function getItemCost(): int|float
    {
        $price = $this->getPrice();
        $quantity = $this->getQuantity();
        $total    = $price * $quantity;

        return $total;
    }
}
