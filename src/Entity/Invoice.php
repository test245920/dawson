<?php

namespace App\Entity;

use App\Model\SearchableInterface;
use Doctrine\ORM\Mapping as ORM;
use Evence\Bundle\SoftDeleteableExtensionBundle\Mapping\Annotation as Evence;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Invoice.
 *
 * @ORM\Table(name="invoice")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\InvoiceRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Invoice implements SearchableInterface
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $status = null;

    /**
     * @ORM\Column(name="is_sent", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $isSent = false;

    /**
     * @ORM\Column(name="is_paid", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $isPaid = false;

    /**
     * @ORM\Column(name="is_queried", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $isQueried = false;

    /**
     * @ORM\Column(name="is_requested_order", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $isRequestedOrder = false;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Job", cascade={"persist"}, inversedBy="invoice")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @Evence\onSoftDelete(type="CASCADE")
     * @Gedmo\Versioned
     */
    protected ?Job $job = null;

    /**
     * @ORM\Column(name="completion_stage", type="integer")
     */
    protected ?int $completionStage = 1;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    protected ?\DateTimeInterface $createdAt = null;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $updatedAt = null;

    /**
     * @ORM\Column(name="sent_at", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $sentAt = null;

    /**
     * @ORM\Column(name="paid_at", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $paidAt = null;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\AttachedFile", cascade={"persist"}, mappedBy="invoice")
     * @ORM\JoinColumn(onDelete="SET NULL", name="attachedFile_id")
     * @Gedmo\Versioned
     */
    protected ?AttachedFile $attachedFile = null;

    public static function getSearchFields(): array
    {
        return ['status' => 'Status'];
    }

    public static function getSearchCategory(): string
    {
        return 'invoice';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?int
    {
        return $this->id;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $value): void
    {
        $this->deletedAt = $value;
    }

    public function setUpdatedAt(?\DateTimeInterface $value): Invoice
    {
        $this->updatedAt = $value;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setStatus(?string $status): Invoice
    {
        $this->status = $status;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setIsSent(?bool $isSent): Invoice
    {
        $this->isSent = $isSent;

        return $this;
    }

    public function getIsSent(): ?bool
    {
        return $this->isSent;
    }

    public function setIsPaid(?bool $isPaid): Invoice
    {
        $this->isPaid = $isPaid;

        return $this;
    }

    public function getIsPaid(): ?bool
    {
        return $this->isPaid;
    }

    public function setIsQueried(?bool $value): Invoice
    {
        $this->isQueried = $value;

        return $this;
    }

    public function getIsQueried(): ?bool
    {
        return $this->isQueried;
    }

    public function setJob(?Job $job): Invoice
    {
        $this->job = $job;

        return $this;
    }

    public function getJob(): ?Job
    {
        return $this->job;
    }

    public function setCompletionStage(?int $value): Invoice
    {
        $this->completionStage = $value;

        return $this;
    }

    public function getCompletionStage(): ?int
    {
        return $this->completionStage;
    }

    public function setSentAt(?\DateTimeInterface $value): Invoice
    {
        $this->sentAt = $value;

        return $this;
    }

    public function getSentAt(): ?\DateTimeInterface
    {
        return $this->sentAt;
    }

    public function setPaidAt(?\DateTimeInterface $value): Invoice
    {
        $this->paidAt = $value;

        return $this;
    }

    public function getPaidAt(): ?\DateTimeInterface
    {
        return $this->paidAt;
    }

    public function setAttachedFile(?AttachedFile $value): Invoice
    {
        $this->attachedFile = $value;

        return $this;
    }

    public function getAttachedFile(): ?AttachedFile
    {
        return $this->attachedFile;
    }

    public function getIsRequestedOrder(): ?bool
    {
        return $this->isRequestedOrder;
    }

    public function setIsRequestedOrder(bool $isRequestedOrder): self
    {
        $this->isRequestedOrder = $isRequestedOrder;

        return $this;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
