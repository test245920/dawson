<?php

namespace App\Entity;

use App\Model\SearchableInterface;
use Doctrine\ORM\Mapping as ORM;
use Evence\Bundle\SoftDeleteableExtensionBundle\Mapping\Annotation as Evence;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * PurchaseOrderItem.
 *
 * @ORM\Table(name="purchase_order_item")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\PurchaseOrderItemRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class PurchaseOrderItem implements SearchableInterface, \Stringable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @var string
     * @ORM\ManyToOne(targetEntity="App\Entity\SupplierStock", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Evence\onSoftDelete(type="SET NULL")
     */
    protected ?SupplierStock $item = null;

    /**
     * @var string
     *
     * @ORM\Column(name="unitCost", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $unitCost = 0.00;

    /**
     * @var string
     *
     * @ORM\Column(name="unitIssue", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $unitIssue = 0.00;

    /**
     * @var int
     *
     * @ORM\Column(name="quantity", type="integer")
     * @Gedmo\Versioned
     */
    protected ?int $quantity = 0;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(name="received_date", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $receivedDate = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PurchaseOrder", inversedBy="purchaseOrderItems")
     * @ORM\JoinColumn(name="purchaseOrder_id", onDelete="SET NULL", nullable=true)
     */
    protected ?PurchaseOrder $purchaseOrder = null;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    protected ?\DateTimeInterface $deletedAt = null;

    public function __construct()
    {
    }

    public static function getSearchFields(): array
    {
        return ['id'       => 'ID', 'unitCost' => 'unitCost'];
    }

    public static function getSearchCategory(): string
    {
        return 'purchase-order-item';
    }

    public function __toString(): string
    {
        return (string) ['item'     => $this->item, 'unitCost' => $this->unitCost, 'quantity' => $this->quantity];
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set item.
     *
     * @param string $item
     */
    public function setItem(?SupplierStock $item): PurchaseOrderItem
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item.
     *
     * @return string
     */
    public function getItem(): ?SupplierStock
    {
        return $this->item;
    }

    /**
     * Set unitCost.
     *
     * @param string $unitCost
     */
    public function setUnitCost(?string $unitCost): PurchaseOrderItem
    {
        $this->unitCost = $unitCost;

        return $this;
    }

    /**
     * Get unitCost.
     *
     * @return string
     */
    public function getUnitCost(): ?string
    {
        return $this->unitCost;
    }

    /**
     * Set unitIssue.
     *
     * @param string $unitIssue
     */
    public function setUnitIssue(?string $unitIssue): PurchaseOrderItem
    {
        $this->unitIssue = $unitIssue;

        return $this;
    }

    /**
     * Get unitIssue.
     *
     * @return string
     */
    public function getUnitIssue(): ?string
    {
        return $this->unitIssue;
    }

    /**
     * Set quantity.
     *
     * @param int $quantity
     */
    public function setQuantity(?int $quantity): PurchaseOrderItem
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity.
     *
     * @return int
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setPurchaseOrder(?PurchaseOrder $value): PurchaseOrderItem
    {
        $this->purchaseOrder = $value;

        return $this;
    }

    public function getPurchaseOrder(): ?PurchaseOrder
    {
        return $this->purchaseOrder;
    }

    public function setReceivedDate(?\DateTimeInterface $value): PurchaseOrderItem
    {
        $this->receivedDate = $value;

        return $this;
    }

    public function getReceivedDate(): ?\DateTimeInterface
    {
        return $this->receivedDate;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $value): void
    {
        $this->deletedAt = $value;
    }

    public function getItemCost(): int|float
    {
        $unitCost = $this->getUnitCost();
        $quantity = $this->getQuantity();
        $total    = $unitCost * $quantity;

        return $total;
    }
}
