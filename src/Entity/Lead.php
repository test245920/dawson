<?php

namespace App\Entity;

use App\Model\SearchableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="lead")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\LeadRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Lead implements SearchableInterface
{
    public $newSource;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Gedmo\Versioned
     */
    protected ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Customer", cascade={"persist"}, inversedBy="leads")
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Gedmo\Versioned
     */
    protected ?Customer $customer = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", cascade={"persist"}, inversedBy="leads")
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Gedmo\Versioned
     */
    protected ?User $user = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Source", cascade={"persist"}, inversedBy="leads")
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Gedmo\Versioned
     */
    protected ?Source $source = null;

    /**
     * @ORM\Column(name="type", type="string", length=255)
     * @Gedmo\Versioned
     */
    protected ?string $type = 'Not Yet Established';

    /**
     * @ORM\Column(name="status", type="string", length=255)
     * @Gedmo\Versioned
     */
    protected ?string $status = 'No contact';

    /**
     * @ORM\Column(name="value", type="string", length=255, nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $value = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Note", cascade={"persist"}, mappedBy="lead")
     * @ORM\OrderBy({"date" = "DESC"})
     * @var Collection<Note>
     */
    protected Collection $notes;

    /**
     * @ORM\Column(name="received_date", type="datetime", nullable=true)
     * @Gedmo\Versioned
     * @Gedmo\Timestampable(on="create")
     */
    protected ?\DateTimeInterface $receivedDate = null;

    /**
     * @ORM\Column(name="initial_contact", type="datetime", nullable=true)
     * @Gedmo\Versioned
     * @Gedmo\Timestampable(on="create")
     */
    protected ?\DateTimeInterface $initialContact = null;

    /**
     * @ORM\Column(name="last_contact", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $lastContact = null;

    /**
     * @ORM\Column(name="priority", type="string", length=255)
     * @Gedmo\Versioned
     */
    protected ?string $priority = null;

    /**
     * @ORM\Column(name="future_target", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $futureTarget = false;

    /**
     * @ORM\Column(name="primary_target", type="boolean", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?bool $primaryTarget = false;

    /**
     * @ORM\Column(name="unsuccessful", type="boolean", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?bool $unsuccessful = false;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Reminder", mappedBy="lead", cascade={"persist", "remove"})
     * @ORM\OrderBy({"date" = "DESC"})
     * @var Collection<Reminder>
     */
    protected Collection $reminders;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Contact", inversedBy="leads", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @var Collection<Contact>
     */
    protected Collection $contacts;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\Column(name="completion_stage", type="integer", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?int $completionStage = 1;

    /**
     * @ORM\Column(name="completion_date", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $completionDate = null;

    /**
     * @ORM\Column(name="unsuccessful_reason", type="string", length=255, nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $unsuccessfulReason = null;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @Gedmo\Versioned
     * @Gedmo\Timestampable(on="update")
     */
    protected ?\DateTimeInterface $updatedAt = null;

    /**
     * @ORM\Column(name="update_count", type="integer")
     * @Gedmo\Versioned
     */
    protected ?int $updateCount = 0;

    /**
     * @ORM\Column(name="is_delivered", type="boolean", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?bool $isDelivered = false;

    /**
     * @ORM\Column(name="delivery_date", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $deliveryDate = null;

    /**
     * @ORM\Column(name="contract_renewal_date", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $contractRenewalDate = null;

    /**
     * @ORM\Column(name="is_service_parts_lead", type="boolean", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?bool $isServicePartsLead = false;

    /**
     * @ORM\Column(name="visit_me", type="boolean", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?bool $visitMe = false;

    /**
     * @ORM\Column(name="archived_at", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $archivedAt = null;

    /**
     * @ORM\Column(name="archived", type="boolean", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?bool $archived = false;

    public function __construct()
    {
        $this->notes           = new ArrayCollection();
        $this->contacts        = new ArrayCollection();
        $this->reminders       = new ArrayCollection();
    }

    public static function getSearchFields(): array
    {
        return ['id'     => 'ID', 'type'   => 'Type', 'status' => 'Status', 'value'  => 'Value'];
    }

    public static function getSearchCategory(): string
    {
        return 'lead';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setCustomer(?Customer $value): Lead
    {
        $this->customer = $value;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setType(?string $value): Lead
    {
        $this->type = $value;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setSource(?Source $value): Lead
    {
        $this->source = $value;

        return $this;
    }

    public function getSource(): ?Source
    {
        return $this->source;
    }

    public function setStatus(?string $value): Lead
    {
        $this->status = $value;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setPriority(?string $value): Lead
    {
        $this->priority = $value;

        return $this;
    }

    public function getPriority(): ?string
    {
        return $this->priority;
    }

    public function setInitialContact(?\DateTimeInterface $value): Lead
    {
        $this->initialContact = $value;

        return $this;
    }

    public function getInitialContact(): ?\DateTimeInterface
    {
        return $this->initialContact;
    }

    public function setLastContact(?\DateTimeInterface $value): Lead
    {
        $this->lastContact = $value;

        return $this;
    }

    public function getLastContact(): ?\DateTimeInterface
    {
        return $this->lastContact;
    }

    public function setDeletedAt(?\DateTimeInterface $value): Lead
    {
        $this->deletedAt = $value;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setFutureTarget(?bool $value): Lead
    {
        $this->futureTarget = $value;

        return $this;
    }

    public function getFutureTarget(): ?bool
    {
        return $this->futureTarget;
    }

    public function setPrimaryTarget(?bool $value): Lead
    {
        $this->primaryTarget = $value;

        return $this;
    }

    public function getPrimaryTarget(): ?bool
    {
        return $this->primaryTarget;
    }

    public function setCompletionDate(?\DateTimeInterface $value): Lead
    {
        $this->completionDate = $value;

        return $this;
    }

    public function getCompletionDate(): ?\DateTimeInterface
    {
        return $this->completionDate;
    }

    public function setContractRenewalDate(?\DateTimeInterface $value): Lead
    {
        $this->contractRenewalDate = $value;

        return $this;
    }

    public function getContractRenewalDate(): ?\DateTimeInterface
    {
        return $this->contractRenewalDate;
    }

    public function getDescription(): string
    {
        $parts = [];

        if ($this->customer) {
            $parts[] = $this->getCustomer()->getName();
        }
        if ($this->type) {
            $parts[] = $this->getType();
        }

        $this->description = implode(' - ', $parts);

        return $this->description;
    }

    public function addNote($value): Lead
    {
        $this->notes->add($value);
        $value->setLead($this);

        return $this;
    }

    public function removeNote($value): void
    {
        $this->notes->removeElement($value);
    }

    public function getNotes(): Collection
    {
        return $this->notes;
    }

    public function addContact($value): Lead
    {
        $this->contacts->add($value);

        return $this;
    }

    public function removeContact($value): void
    {
        $this->contacts->removeElement($value);
    }

    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function getCompletionStage(): ?int
    {
        return $this->completionStage;
    }

    public function setCompletionStage(?int $value): void
    {
        $this->completionStage = $value;
    }

    public function addReminder($value): Lead
    {
        $this->reminders->add($value);
        $value->setLead($this);

        return $this;
    }

    public function removeReminder($value): void
    {
        $this->reminders->removeElement($value);
    }

    public function getReminders(): Collection
    {
        return $this->reminders;
    }

    public function setUser(?User $value): Lead
    {
        $this->user = $value;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setReceivedDate(?\DateTimeInterface $value): Lead
    {
        $this->receivedDate = $value;

        return $this;
    }

    public function getReceivedDate(): ?\DateTimeInterface
    {
        return $this->receivedDate;
    }

    public function setValue(?string $value): Lead
    {
        $this->value = $value;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function getUnsuccessful(): ?bool
    {
        return $this->unsuccessful;
    }

    public function setUnsuccessful(?bool $value): void
    {
        $this->unsuccessful = $value;
    }

    public function setUnsuccessfulReason(?string $value): Lead
    {
        $this->unsuccessfulReason = $value;

        return $this;
    }

    public function getUnsuccessfulReason(): ?string
    {
        return $this->unsuccessfulReason;
    }

    public function setUpdatedAt(?\DateTimeInterface $value): Lead
    {
        $this->updatedAt = $value;
        ++$this->updateCount;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function getUpdateCount(): ?int
    {
        return $this->updateCount;
    }

    public function setUpdateCount(?int $value): void
    {
        $this->updateCount = $value;
    }

    public function getIsDelivered(): ?bool
    {
        return $this->isDelivered;
    }

    public function setIsDelivered(?bool $value): void
    {
        $this->isDelivered = $value;
    }

    public function getDeliveryDate(): ?\DateTimeInterface
    {
        return $this->deliveryDate;
    }

    public function setDeliveryDate(?\DateTimeInterface $value): void
    {
        $this->deliveryDate = $value;
    }

    public function getIsServicePartsLead(): ?bool
    {
        return $this->isServicePartsLead;
    }

    public function setIsServicePartsLead(?bool $value): void
    {
        $this->isServicePartsLead = $value;
    }

    public function getVisitMe(): ?bool
    {
        return $this->visitMe;
    }

    public function setVisitMe(?bool $value): void
    {
        $this->visitMe = $value;
    }

    public function getArchived(): ?bool
    {
        return $this->archived;
    }

    public function setArchived(?bool $value): void
    {
        $this->archived = $value;
    }

    public function getArchivedAt(): ?\DateTimeInterface
    {
        return $this->archivedAt;
    }

    public function setArchivedAt(?\DateTimeInterface $value): void
    {
        $this->archivedAt = $value;
    }
}
