<?php

namespace App\Entity;

use App\Validator\UniqueBin;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * App\Entity\Stock.
 *
 * @ORM\Table(name="stock", indexes={
 *      @ORM\Index(name="stock_index", columns={"code"}),
 *      @ORM\Index(name="IDX_code_description", columns={"code", "description"}),
 *      @ORM\Index(name="import_id_idx", columns={"import_id"})
 * })
 * @ORM\Entity(repositoryClass="App\Entity\Repository\StockRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @UniqueBin
 */
class Stock implements \Stringable
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(name="code", type="string", length=50, nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $code = null;

    /**
     * @ORM\Column(name="description", type="string", length=255)
     * @Gedmo\Versioned
     */
    #[Assert\NotNull(message: 'Required description')]
    protected ?string $description = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SupplierStock", mappedBy="stock", cascade={"persist"})
     * @ORM\JoinColumn(name="supplierStock_id")
     * @var Collection<SupplierStock>
     */
    protected Collection $supplierStock;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\StockItem", mappedBy="stock", cascade={"persist"})
     * @var Collection<StockItem>
     */
    protected Collection $stockItems;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Stock", inversedBy="replacedBy", cascade={"persist"})
     * @ORM\JoinColumn(name="replacesPart_id", onDelete="SET NULL")
     */
    protected ?\App\Entity\Stock $replacesPart = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\StockType")
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Gedmo\Versioned
     */
    protected ?StockType $type = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Stock", mappedBy="replacesPart", cascade={"persist"})
     * @ORM\JoinColumn(name="replacedBy_id", onDelete="SET NULL")
     */
    protected ?\App\Entity\Stock $replacedBy = null;

    /**
     * @ORM\Column(name="last_ordered", type="datetime", nullable=true)
     */
    protected ?\DateTimeInterface $lastOrdered = null;

    /**
     * To be implemented.
     */
    protected $lastOrder;

    /**
     * @ORM\Column(name="min_stock", type="integer", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?int $minStock = null;

    /**
     * @ORM\Column(name="reorder_quantity", type="integer", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?int $reorderQuantity = null;

    /**
     * @ORM\Column(name="oem_price", type="decimal", nullable=true, precision=20, scale=10)
     * @Gedmo\Versioned
     */
    protected ?float $oemPrice = null;

    /**
     * @ORM\Column(name="non_stock", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $nonStock = false;

    /**
     * @ORM\Column(name="prev_code", type="string", length=50, nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $prevCode = null;

    /**
     * @ORM\Column(name="last_updated", type="datetime")
     * @Gedmo\Versioned
     */
    protected \DateTime $lastUpdated;

    /**
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    protected ?\DateTimeInterface $createdAt = null;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Supplier", inversedBy="preferredSupplierFor", cascade={"persist"})
     * @ORM\JoinColumn(name="preferredSupplier_id", onDelete="SET NULL")
     */
    protected ?Supplier $preferredSupplier = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\StockTransaction", cascade={"persist"}, mappedBy="stock")
     * @var Collection<StockTransaction>
     */
    protected Collection $stockTransactions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TruckModel", mappedBy="oilFilter", cascade={"persist"})
     * @var Collection<TruckModel>
     */
    protected Collection $oilFilterForTruckModel;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TruckModel", mappedBy="airFilter", cascade={"persist"})
     * @var Collection<TruckModel>
     */
    protected Collection $airFilterForTruckModel;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TruckModel", mappedBy="fuelFilter", cascade={"persist"})
     * @var Collection<TruckModel>
     */
    protected Collection $fuelFilterForTruckModel;

    /**
     * Format: YYYYmmddHHiiss-<row_number>, e.g. 20170530165000-123.
     * @ORM\Column(name="import_id", type="string", length=50, nullable=true)
     */
    protected ?string $importId = null;

    /**
     * @ORM\Column(name="bin", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $bin = null;

    public function __construct()
    {
        $this->stockItems = new ArrayCollection();
        $this->supplierStock           = new ArrayCollection();
        $this->stockTransactions       = new ArrayCollection();
        $this->purchaseOrdersForJob    = new ArrayCollection();
        $this->oilFilterForTruckModel  = new ArrayCollection();
        $this->airFilterForTruckModel  = new ArrayCollection();
        $this->fuelFilterForTruckModel = new ArrayCollection();

        $this->lastUpdated = new \DateTime();
    }

    public function addSupplierStock($value): Stock
    {
        $this->supplierStock->add($value);

        return $this;
    }

    public function removeSupplierStock($value): Stock
    {
        $this->supplierStock->removeElement($value);

        return $this;
    }

    public function getSupplierStock()
    {
        return $this->supplierStock->toArray();
    }

    public function getReplacedBy(): ?Stock
    {
        return $this->replacedBy;
    }

    public function setReplacedBy(?Stock $value): Stock
    {
        $this->replacedBy = $value;

        return $this;
    }

    public function getReplacesPart(): ?Stock
    {
        return $this->replacesPart;
    }

    public function setReplacesPart(?Stock $value): Stock
    {
        $this->replacesPart = $value;
        $value->setReplacedBy($this);

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $value): Stock
    {
        $this->id = $value;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $value): Stock
    {
        $this->code = $value;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $value): Stock
    {
        $this->description = $value;

        return $this;
    }

    public function getType(): ?StockType
    {
        return $this->type;
    }

    public function setType(?StockType $value): Stock
    {
        $this->type = $value;

        return $this;
    }

    public function getLastOrdered(): ?\DateTimeInterface
    {
        return $this->lastOrdered;
    }

    public function setLastOrdered(?\DateTimeInterface $value): Stock
    {
        $this->lastOrdered = $value;

        return $this;
    }

    public function getLastOrder()
    {
        return $this->lastOrder;
    }

    public function setLastOrder($value): Stock
    {
        $this->lastOrder = $value;

        return $this;
    }

    public function getMinStock(): ?int
    {
        return $this->minStock;
    }

    public function setMinStock(?int $value): Stock
    {
        $this->minStock = $value;

        return $this;
    }

    public function getReorderQuantity(): ?int
    {
        return $this->reorderQuantity;
    }

    public function setReorderQuantity(?int $value): Stock
    {
        $this->reorderQuantity = $value;

        return $this;
    }

    public function getOemPrice(): ?string
    {
        return $this->oemPrice;
    }

    public function setOemPrice(?string $value): Stock
    {
        $this->oemPrice = $value;

        return $this;
    }

    public function getNonStock(): ?bool
    {
        return $this->nonStock;
    }

    public function setNonStock(?bool $value): Stock
    {
        $this->nonStock = $value;

        return $this;
    }

    public function getPrevCode(): ?string
    {
        return $this->prevCode;
    }

    public function setPrevCode(?string $value): Stock
    {
        $this->prevCode = $value;

        return $this;
    }

    public function getLastUpdated(): \DateTime
    {
        return $this->lastUpdated;
    }

    public function setLastUpdated(\DateTime $value): Stock
    {
        $this->lastUpdated = $value;

        return $this;
    }

    public function getStockString(): string
    {
        $parts = [];

        if ($this->code) {
            $parts[] = $this->code;
        }
        if ($this->description) {
            $parts[] = $this->description;
        }

        return implode(' - ', $parts);
    }

    public function __toString(): string
    {
        return $this->getStockString();
    }

    public function getNewestReplacementPart()
    {
        if ($this->getReplacedBy()) {
            return;
        }

        $newPart  = $this->getReplacedBy()->first();
        $replaced = $newPart->getReplacedBy() ? true : false;

        while ($replaced) {
            $newPart  = $newPart->getReplacedBy();
            $replaced = $newPart->getReplacedBy() ? true : false;
        }

        return $newPart;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $value): Stock
    {
        $this->deletedAt = $value;

        return $this;
    }

    public function getPreferredSupplier(): ?Supplier
    {
        return $this->preferredSupplier;
    }

    public function setPreferredSupplier(?Supplier $value): Stock
    {
        $this->preferredSupplier = $value;

        return $this;
    }

    public function addStockTransaction($value): Stock
    {
        $this->stockTransactions->add($value);

        return $this;
    }

    public function removeStockTransaction($value): void
    {
        $this->stockTransactions->removeElement($value);
    }

    public function getStockTransactions(): Collection
    {
        return $this->stockTransactions;
    }

    public function addStockItem($value): Stock
    {
        $this->stockItems->add($value);

        return $this;
    }

    public function removeStockItem($value): void
    {
        $this->stockItems->removeElement($value);
    }

    public function getStockItems(): Collection
    {
        return $this->stockItems;
    }

    public function addOilFilterForTruckModel($value): Stock
    {
        $this->oilFilterForTruckModel->add($value);

        return $this;
    }

    public function removeOilFilterForTruckModel($value): Stock
    {
        $this->oilFilterForTruckModel->removeElement($value);

        return $this;
    }

    public function getOilFilterForTruckModel()
    {
        return $this->oilFilterForTruckModel->toArray();
    }

    public function addAirFilterForTruckModel($value): Stock
    {
        $this->airFilterForTruckModel->add($value);

        return $this;
    }

    public function removeAirFilterForTruckModel($value): Stock
    {
        $this->airFilterForTruckModel->removeElement($value);

        return $this;
    }

    public function getAirFilterForTruckModel()
    {
        return $this->airFilterForTruckModel->toArray();
    }

    public function addFuelFilterForTruckModel($value): Stock
    {
        $this->fuelFilterForTruckModel->add($value);

        return $this;
    }

    public function removeFuelFilterForTruckModel($value): Stock
    {
        $this->fuelFilterForTruckModel->removeElement($value);

        return $this;
    }

    public function getFuelFilterForTruckModel()
    {
        return $this->fuelFilterForTruckModel->toArray();
    }

    public function getLastPurchaseOrder()
    {
        if (!$this->stockTransactions->count()) {
            return null;
        }

        $criteria = Criteria::create()
            ->andWhere(Criteria::expr()->neq('purchaseOrder', null))
            ->orderBy(['id' => Criteria::DESC])
            ->setMaxResults(1);

        $transaction = $this->stockTransactions->matching($criteria)->first();

        return $transaction ? $transaction->getPurchaseOrder() : null;
    }

    public function getImportId(): ?string
    {
        return $this->importId;
    }

    public function setImportId(?string $value): Stock
    {
        $this->importId = $value;

        return $this;
    }

    public function getBin(): ?string
    {
        return $this->bin;
    }

    public function setBin(?string $value): Stock
    {
        $this->bin = $value;

        return $this;
    }
}
