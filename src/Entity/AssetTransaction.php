<?php

namespace App\Entity;

use App\Model\SearchableInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="asset_transaction")
 * @ORM\Entity()
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class AssetTransaction implements SearchableInterface
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(name="date", type="datetime")
     * @Gedmo\Versioned
     * @Gedmo\Timestampable(on="create")
     */
    protected ?\DateTimeInterface $date = null;

    /**
     * @ORM\Column(name="createdAt", type="datetime")
     * @Gedmo\Versioned
     * @Gedmo\Timestampable(on="create")
     */
    protected ?\DateTimeInterface $createdAt = null;

    /**
     * @ORM\Column(name="updatedAt", type="datetime")
     * @Gedmo\Versioned
     * @Gedmo\Timestampable(on="update")
     */
    protected ?\DateTimeInterface $updatedAt = null;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\Column(name="amount", type="integer")
     * @Gedmo\Versioned
     */
    protected ?int $amount = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AssetLifecyclePeriod", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL", name="assetLifecyclePeriod_id")
     */
    protected ?AssetLifecyclePeriod $assetLifecyclePeriod = null;

    /**
     * @ORM\Column(name="transaction_category", type="integer")
     */
    protected ?int $transactionCategory = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\AssetMaintenance", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected ?AssetMaintenance $maintenance = null;

    public static function getSearchFields(): array
    {
        return ['id'     => 'ID', 'amount' => 'amount', 'date'   => 'date'];
    }

    public static function getSearchCategory(): string
    {
        return 'stock_transaction';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setDate(?\DateTimeInterface $date): AssetTransaction
    {
        $this->date = $date;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): AssetTransaction
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): AssetTransaction
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): AssetTransaction
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setAmount(?int $quantity): AssetTransaction
    {
        $this->amount = $quantity;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAssetLifecyclePeriod(?AssetLifecyclePeriod $value = null): AssetTransaction
    {
        $this->assetLifecyclePeriod = $value;

        return $this;
    }

    public function getAssetLifecyclePeriod(): ?AssetLifecyclePeriod
    {
        return $this->assetLifecyclePeriod;
    }

    public function setTransactionCategory(?int $value): AssetTransaction
    {
        $this->transactionCategory = $value;

        return $this;
    }

    public function getTransactionCategory(): ?int
    {
        return $this->transactionCategory;
    }

    public function setMaintenance(?AssetMaintenance $value): AssetTransaction
    {
        $this->maintenance = $value;

        return $this;
    }

    public function getMaintenance(): ?AssetMaintenance
    {
        return $this->maintenance;
    }
}
