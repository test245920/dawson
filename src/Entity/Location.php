<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Location.
 *
 * @ORM\Table(name="location")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\LocationRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Location
{
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\StockItem", mappedBy="location", cascade={"persist"})
     * @var Collection<StockItem>
     */
    protected Collection $stockItems;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\SiteAddress", inversedBy="location", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="siteAddress_id", onDelete="SET NULL")
     * @Gedmo\Versioned
     */
    protected ?SiteAddress $siteAddress = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\BillingAddress", mappedBy="location", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="billingAddress_id", onDelete="SET NULL")
     * @Gedmo\Versioned
     */
    protected ?BillingAddress $billingAddress = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="location", cascade={"persist"})
     * @var Collection<User>
     */
    protected Collection $engineers;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="van", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Gedmo\Versioned
     */
    protected ?User $engineer = null;

    /**
     * @ORM\Column(name="isMainStock", type="boolean", nullable=true)
     */
    protected ?bool $isMainStock = null;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     * @Gedmo\Versioned
     */
    private ?string $type = null;

    public function __construct()
    {
        $this->stockItems = new ArrayCollection();
        $this->engineers  = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function addStockItem($value): Location
    {
        $this->stockItems->add($value);

        return $this;
    }

    public function removeStockItem($value): Location
    {
        $this->stockItems->removeElement($value);

        return $this;
    }

    public function getStockItems(): Collection
    {
        return $this->stockItems;
    }

    public function setType(?string $type): Location
    {
        $this->type = $type;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setSiteAddress(?SiteAddress $value): Location
    {
        $this->siteAddress = $value;
        $value->setLocation($this);

        return $this;
    }

    public function getSiteAddress(): ?SiteAddress
    {
        return $this->siteAddress;
    }

    public function addEngineer($value): Location
    {
        $this->engineers->add($value);
        $value->setLocation($this);

        return $this;
    }

    public function removeEngineer($value): Location
    {
        $this->engineers->removeElement($value);

        return $this;
    }

    public function getEngineers(): Collection
    {
        return $this->engineers;
    }

    public function setBillingAddress(?BillingAddress $value): Location
    {
        $this->billingAddress = $value;
        $value->setLocation($this);

        return $this;
    }

    public function getBillingAddress($value): ?BillingAddress
    {
        return $this->billingAddress;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $value): Location
    {
        $this->deletedAt = $value;

        return $this;
    }

    public function getLocationString(): ?string
    {
        if ($this->engineer) {
            return sprintf('Van (%s)', $this->engineer->getName());
        }

        $parts = [];

        if ($this->type) {
            $parts[] = $this->type;
        }

        if ($this->siteAddress) {
            $parts[] = $this->siteAddress->getName();
        }

        if ($this->billingAddress) {
            $parts[] = $this->billingAddress->getAddressString();
        }

        if (empty($parts)) {
            return null;
        }

        $locationString = implode(', ', $parts) . '.';

        return $locationString;
    }

    public function getEngineer(): ?User
    {
        return $this->engineer;
    }

    public function setEngineer(?User $value): Location
    {
        $this->engineer = $value;

        return $this;
    }

    public function getIsMainStock(): ?bool
    {
        return $this->isMainStock;
    }

    public function setIsMainStock(?bool $value): Location
    {
        $this->isMainStock = $value;

        return $this;
    }
}
