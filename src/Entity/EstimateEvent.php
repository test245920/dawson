<?php

namespace App\Entity;

use App\Model\SearchableInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * EstimateEvent.
 *
 * @ORM\Table(name="estimate_event")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\EstimateEventRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 */
class EstimateEvent implements SearchableInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Estimate", cascade={"persist"}, inversedBy="estimateEvents")
     * @Gedmo\Versioned
     */
    protected ?Estimate $estimate = null;

    /**
     * @var bool
     *
     * @ORM\Column(name="isCustomerAction", type="boolean", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?bool $isCustomerAction = false;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="estimateEvents", cascade={"persist"})
     * @Gedmo\Versioned
     */
    protected ?User $user = null;

    /**
     * @var bool
     *
     * @ORM\Column(name="isAdminResponse", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $isAdminResponse = false;

    /**
     * @var string
     *
     * @ORM\Column(name="adminResponse", type="string", length=255, nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $adminResponse = null;

    /**
     * @var bool
     *
     * @ORM\Column(name="accepted", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $accepted = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="rejected", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $rejected = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="isCustomerQuery", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $isCustomerQuery = false;

    /**
     * @var string
     *
     * @Gedmo\Versioned
     * @ORM\Column(name="customerQuery", type="string", length=255, nullable=true)
     */
    protected ?string $customerQuery = null;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\Column(name="date", type="datetime")
     * @Gedmo\Versioned
     */
    protected \DateTime $date;

    public function __construct()
    {
        $this->date = new \DateTime();
    }

    public static function getSearchFields(): array
    {
        return ['id'             => 'id', 'estimate'       => 'estimate', 'user'           => 'user', 'admin response' => 'adminResponse', 'customer query' => 'customerQuery'];
    }

    public static function getSearchCategory(): string
    {
        return 'estimate';
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set estimate.
     *
     * @param string $estimate
     */
    public function setEstimate(?Estimate $estimate): EstimateEvent
    {
        $this->estimate = $estimate;

        return $this;
    }

    /**
     * Get estimate.
     *
     * @return string
     */
    public function getEstimate(): ?Estimate
    {
        return $this->estimate;
    }

    /**
     * Set isCustomerAction.
     *
     * @param bool $isCustomerAction
     */
    public function setIsCustomerAction(?bool $isCustomerAction): EstimateEvent
    {
        $this->isCustomerAction = $isCustomerAction;

        return $this;
    }

    /**
     * Get isCustomerAction.
     *
     * @return bool
     */
    public function getIsCustomerAction(): ?bool
    {
        return $this->isCustomerAction;
    }

    /**
     * Set user.
     *
     * @param string $user
     */
    public function setUser(?User $user): EstimateEvent
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return string
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * Set isAdminResponse.
     *
     * @param bool $isAdminResponse
     */
    public function setIsAdminResponse(?bool $isAdminResponse): EstimateEvent
    {
        $this->isAdminResponse = $isAdminResponse;

        return $this;
    }

    /**
     * Get isAdminResponse.
     *
     * @return bool
     */
    public function getIsAdminResponse(): ?bool
    {
        return $this->isAdminResponse;
    }

    /**
     * Set adminResponse.
     *
     * @param string $adminResponse
     */
    public function setAdminResponse(?string $adminResponse): EstimateEvent
    {
        $this->adminResponse = $adminResponse;

        return $this;
    }

    /**
     * Get adminResponse.
     *
     * @return string
     */
    public function getAdminResponse(): ?string
    {
        return $this->adminResponse;
    }

    /**
     * Set accepted.
     *
     * @param bool $accepted
     */
    public function setAccepted(?bool $accepted): EstimateEvent
    {
        $this->accepted = $accepted;

        return $this;
    }

    /**
     * Get accepted.
     *
     * @return bool
     */
    public function getAccepted(): ?bool
    {
        return $this->accepted;
    }

    /**
     * Set rejected.
     *
     * @param bool $rejected
     */
    public function setRejected(?bool $rejected): EstimateEvent
    {
        $this->rejected = $rejected;

        return $this;
    }

    /**
     * Get rejected.
     *
     * @return bool
     */
    public function getRejected(): ?bool
    {
        return $this->rejected;
    }

    /**
     * Set isCustomerQuery.
     *
     * @param bool $isCustomerQuery
     */
    public function setIsCustomerQuery(?bool $isCustomerQuery): EstimateEvent
    {
        $this->isCustomerQuery = $isCustomerQuery;

        return $this;
    }

    /**
     * Get isCustomerQuery.
     *
     * @return bool
     */
    public function getIsCustomerQuery(): ?bool
    {
        return $this->isCustomerQuery;
    }

    /**
     * Set customerQuery.
     *
     * @param string $customerQuery
     */
    public function setCustomerQuery(?string $customerQuery): EstimateEvent
    {
        $this->customerQuery = $customerQuery;

        return $this;
    }

    /**
     * Get customerQuery.
     *
     * @return string
     */
    public function getCustomerQuery(): ?string
    {
        return $this->customerQuery;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $value): void
    {
        $this->deletedAt = $value;
    }
}
