<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SalesJob.
 *
 * @ORM\Table(name="sales_job")
 * })
 * @ORM\Entity(repositoryClass="App\Entity\Repository\SalesJobRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class SalesJob
{
    final public const STATUS_INCOMPLETE = 0;
    final public const STATUS_IN_PREP = 1;
    final public const STATUS_COMPLETE = 2;
    final public const CHOICES = [
        self::STATUS_INCOMPLETE => 'Incomplete',
        self::STATUS_IN_PREP => 'In Prep',
        self::STATUS_COMPLETE => 'Complete',
    ];

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Customer", cascade={"persist"}, inversedBy="salesJob")
     * @Gedmo\Versioned
     */
    #[Assert\NotBlank]
    protected ?\App\Entity\Customer $customer = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Truck", inversedBy="salesJob")
     * @ORM\JoinColumn(name="truck_id", referencedColumnName="id")
     */
    protected ?\App\Entity\Truck $truck = null;

    /**
     * @ORM\Column(name="job_code", type="integer", nullable=true))
     * @ORM\Column(type="integer")
     */
    protected ?int $jobCode = null;

    /**
     * @ORM\Column(name="make", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $make = null;

    /**
     * @ORM\Column(name="model", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $model = null;

    /**
     * @ORM\Column(name="serial", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $serial = null;

    /**
     * @ORM\Column(name="status", type="integer", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?int $status = null;

    /**
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    protected ?\DateTimeInterface $createdAt = null;

    /**
     * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    protected ?\DateTimeInterface $updatedAt = null;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", cascade={"persist"}, inversedBy="salesJob")
     * @Gedmo\Versioned
     */
    protected ?\App\Entity\User $deletedBy = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PurchaseOrder", mappedBy="salesJob", cascade={"persist"})
     * @var \Doctrine\Common\Collections\Collection<int, \App\Entity\PurchaseOrder>|\App\Entity\PurchaseOrder[]
     */
    protected \Doctrine\Common\Collections\Collection $purchaseOrders;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Job", mappedBy="salesJob", cascade={"persist"})
     * @var \Doctrine\Common\Collections\Collection<int, \App\Entity\Job>|\App\Entity\Job[]
     */
    protected \Doctrine\Common\Collections\Collection $jobs;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    public function __construct()
    {
        $this->purchaseOrders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getJobCode(): ?int
    {
        return $this->jobCode;
    }

    public function setJobCode(?int $jobCode): self
    {
        $this->jobCode = $jobCode;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getDeletedBy(): ?User
    {
        return $this->deletedBy;
    }

    public function setDeletedBy(?User $deletedBy): self
    {
        $this->deletedBy = $deletedBy;

        return $this;
    }

    /**
     * @return Collection|PurchaseOrder[]
     */
    public function getPurchaseOrders(): Collection
    {
        return $this->purchaseOrders;
    }

    public function addPurchaseOrder(PurchaseOrder $purchaseOrder): self
    {
        if (!$this->purchaseOrders->contains($purchaseOrder)) {
            $this->purchaseOrders[] = $purchaseOrder;
            $purchaseOrder->setSalesJob($this);
        }

        return $this;
    }

    public function removePurchaseOrder(PurchaseOrder $purchaseOrder): self
    {
        if ($this->purchaseOrders->removeElement($purchaseOrder)) {
            // set the owning side to null (unless already changed)
            if ($purchaseOrder->getSalesJob() === $this) {
                $purchaseOrder->setSalesJob(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Job[]
     */
    public function getJobs(): Collection
    {
        return $this->jobs;
    }

    public function addJob(Job $job): self
    {
        if (!$this->jobs->contains($job)) {
            $this->jobs[] = $job;
            $job->setSalesJob($this);
        }

        return $this;
    }

    public function removeJob(Job $job): self
    {
        if ($this->jobs->removeElement($job)) {
            if ($job->getSalesJob() === $this) {
                $job->setSalesJob(null);
            }
        }

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getMake(): ?string
    {
        return $this->make;
    }

    public function setMake(?string $make): self
    {
        $this->make = $make;

        return $this;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(?string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getSerial(): ?string
    {
        return $this->serial;
    }

    public function setSerial(?string $serial): self
    {
        $this->serial = $serial;

        return $this;
    }

    public function getTruck(): ?Truck
    {
        return $this->truck;
    }

    public function setTruck(?Truck $truck): self
    {
        $this->truck = $truck;

        return $this;
    }

    public function getDisplayTitle(): string
    {
        $parts = [$this->jobCode];

        if ($this->truck && $this->truck->getCustomer()) {
            $parts[] = $this->truck->getCustomer()->getName();
        } else {
            $parts[] = 'No customer';
        }

        return implode(' - ', $parts);
    }
}
