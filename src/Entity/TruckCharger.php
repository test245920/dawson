<?php

namespace App\Entity;

use App\Model\SearchableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * App\Entity\TruckCharger.
 *
 * @ORM\Table(name="truck_charger")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\TruckChargerRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 */
#[Assert\Callback(callback: 'validateMakeAndModel')]
class TruckCharger extends Asset implements SearchableInterface
{
    /**
     * @ORM\Column(name="make", type="string", length=255)
     * @Gedmo\Versioned
     */
    protected ?string $make = null;

    /**
     * @ORM\Column(name="model", type="string", length=255)
     * @Gedmo\Versioned
     */
    protected ?string $model = null;

    /**
     * @ORM\Column(name="phase", type="text")
     * @Gedmo\Versioned
     */
    #[Assert\NotBlank]
    protected ?string $phase = null;

    /**
     * @ORM\Column(name="output_voltage", type="integer", length=5, nullable=true)
     * @Gedmo\Versioned
     */
    protected ?int $outputVoltage = null;

    /**
     * @ORM\Column(name="output_current", type="integer", length=5, nullable=true)
     * @Gedmo\Versioned
     */
    protected ?int $outputCurrent = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Truck", mappedBy="charger", cascade={"persist"})
     * @var Collection<Truck>
     */
    protected Collection $truck;

    public function __construct()
    {
        parent::__construct();
        $this->truck = new ArrayCollection();
    }

    public static function getSearchFields(): array
    {
        return ['id'     => 'ID', 'make'   => 'Make', 'model'  => 'Model', 'serial' => 'Serial'];
    }

    public static function getSearchCategory(): string
    {
        return 'truck-charger';
    }

    public function getViewRoute(): string
    {
        return 'view_truck_charger';
    }

    public function getViewParams(): array
    {
        return ['id' => $this->id];
    }

    public function getNewRoute(): string
    {
        return 'new_truck_charger';
    }

    public function getEditRoute(): string
    {
        return 'edit_truck_charger';
    }

    public function getEditParams(): array
    {
        return ['id' => $this->id];
    }

    public function validateMakeAndModel(ExecutionContextInterface $context): void
    {
        if (!$this->make) {
            if ($this->newMake) {
                $this->make = $this->newMake;
            } else {
                $context->addViolation('Make Required.');
            }
        }

        if (!$this->model) {
            if ($this->newModel) {
                $this->model = $this->newModel;
            } else {
                $context->addViolation('Model Required.');
            }
        }
    }

    public function getMake(): ?string
    {
        return $this->make;
    }

    public function setMake(?string $value): TruckCharger
    {
        $this->make = $value;

        return $this;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(?string $value): TruckCharger
    {
        $this->model = $value;

        return $this;
    }

    public function getPhase(): ?string
    {
        return $this->phase;
    }

    public function setPhase(?string $value): TruckCharger
    {
        $this->phase = $value;

        return $this;
    }

    public function getOutputVoltage(): ?int
    {
        return $this->outputVoltage;
    }

    public function setOutputVoltage(?int $value): TruckCharger
    {
        $this->outputVoltage = $value;

        return $this;
    }

    public function getOutputCurrent(): ?int
    {
        return $this->outputCurrent;
    }

    public function setOutputCurrent(?int $value): TruckCharger
    {
        $this->outputCurrent = $value;

        return $this;
    }
}
