<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * FutureWorkRecommendation.
 *
 * @ORM\Table(name="future_work_recommendation")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\FutureWorkRecommendationRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class FutureWorkRecommendation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Gedmo\Versioned
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(name="task", type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $task = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Job", inversedBy="futureWorkRecommendations", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Gedmo\Versioned
     */
    protected ?Job $job = null;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    protected ?\DateTimeInterface $createdAt = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Estimate", inversedBy="futureWorkRecommendations", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Gedmo\Versioned
     */
    protected ?Estimate $estimate = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setTask(?string $value): FutureWorkRecommendation
    {
        $this->task = $value;

        return $this;
    }

    public function getTask(): ?string
    {
        return $this->task;
    }

    public function setJob(?Job $value): FutureWorkRecommendation
    {
        $this->job = $value;

        return $this;
    }

    public function getJob(): ?Job
    {
        return $this->job;
    }

    public function setCreatedAt(?\DateTimeInterface $value): FutureWorkRecommendation
    {
        $this->createdAt = $value;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setDeletedAt(?\DateTimeInterface $value): FutureWorkRecommendation
    {
        $this->deletedAt = $value;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setEstimate(?Estimate $value): FutureWorkRecommendation
    {
        $this->estimate = $value;

        return $this;
    }

    public function getEstimate(): ?Estimate
    {
        return $this->estimate;
    }
}
