<?php

namespace App\Entity;

use App\Model\SearchableInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * EstimateStock.
 *
 * @ORM\Table(name="estimate_stock")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\EstimateStockRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 */
class EstimateStock implements SearchableInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Gedmo\Versioned
     */
    protected ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Estimate", cascade={"persist"}, inversedBy="estimateStocks")
     * @Gedmo\Versioned
     */
    protected ?Estimate $estimate = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SupplierStock", cascade={"persist"})
     * @ORM\JoinColumn(name="supplierStock_id", onDelete="CASCADE")
     * @Gedmo\Versioned
     */
    protected ?SupplierStock $supplierStock = null;

    /**
     * @ORM\Column(name="quantity", type="integer", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?int $quantity = null;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $deletedAt = null;

    public static function getSearchFields(): array
    {
        return ['id' => 'id'];
    }

    public static function getSearchCategory(): string
    {
        return 'estimate_stock';
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set estimate.
     *
     * @param string $estimate
     */
    public function setEstimate(?Estimate $estimate): EstimateStock
    {
        $this->estimate = $estimate;

        return $this;
    }

    /**
     * Get estimate.
     *
     * @return string
     */
    public function getEstimate(): ?Estimate
    {
        return $this->estimate;
    }

    /**
     * Set supplierStock.
     *
     * @param string $supplierStock
     */
    public function setSupplierStock(?SupplierStock $supplierStock): EstimateStock
    {
        $this->supplierStock = $supplierStock;

        return $this;
    }

    /**
     * Get supplierStock.
     *
     * @return string
     */
    public function getSupplierStock(): ?SupplierStock
    {
        return $this->supplierStock;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $value): void
    {
        $this->deletedAt = $value;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(?int $value): void
    {
        $this->quantity = $value;
    }
}
