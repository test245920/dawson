<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="note")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\NoteRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Note
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(name="reason", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $reason = null;

    /**
     * @ORM\Column(name="detail", type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $detail = null;

    /**
     * @ORM\Column(name="date", type="datetime")
     * @Gedmo\Versioned
     * @Gedmo\Timestampable(on="create")
     */
    #[Assert\NotNull(message: 'Required')]
    protected ?\DateTimeInterface $date = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Lead", cascade={"persist"}, inversedBy="notes")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected ?Lead $lead = null;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AssetLifecyclePeriod", cascade={"persist"}, inversedBy="notes")
     * @ORM\JoinColumn(onDelete="SET NULL", name="assetLifecyclePeriod_id")
     * @Gedmo\Versioned
     */
    protected ?AssetLifecyclePeriod $assetLifecyclePeriod = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Asset", cascade={"persist"}, inversedBy="notes")
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Gedmo\Versioned
     */
    protected ?Asset $asset = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $value): void
    {
        $this->deletedAt = $value;
    }

    public function setReason(?string $reason): Note
    {
        $this->reason = $reason;

        return $this;
    }

    public function getReason(): ?string
    {
        return $this->reason;
    }

    public function setDetail(?string $detail): Note
    {
        $this->detail = $detail;

        return $this;
    }

    public function getDetail(): ?string
    {
        return $this->detail;
    }

    public function setDate(?\DateTimeInterface $date): Note
    {
        $this->date = $date;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setLead(?Lead $lead): Note
    {
        $this->lead = $lead;

        return $this;
    }

    public function getLead(): ?Lead
    {
        return $this->lead;
    }

    public function setAssetLifecyclePeriod(?AssetLifecyclePeriod $value = null): Note
    {
        $value->addNote($this);
        $this->assetLifecyclePeriod = $value;

        return $this;
    }

    public function getAsset(): ?Asset
    {
        return $this->asset;
    }

    public function setAsset(?Asset $value = null): Note
    {
        $value->addNote($this);
        $this->asset = $value;

        return $this;
    }

    public function getAssetLifecyclePeriod(): ?AssetLifecyclePeriod
    {
        return $this->assetLifecyclePeriod;
    }

    public function getDescription(): string
    {
        $parts = [];

        if ($this->reason) {
            $parts[] = $this->getReason();
        }
        if ($this->detail) {
            $parts[] = $this->getDetail();
        }
        if ($this->date) {
            $parts[] = date_format($this->getDate(), 'd-m-Y');
        }

        $this->description = implode(' - ', $parts);

        return $this->description;
    }
}
