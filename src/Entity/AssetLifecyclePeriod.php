<?php

namespace App\Entity;

use App\Model\ChargeMethod;
use App\Model\Warranty;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Table(name="asset_lifecycle_period")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\AssetLifecyclePeriodRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
#[Assert\Callback(callback: 'checkDates')]
class AssetLifecyclePeriod
{
    final public const PERIOD_TYPE_NONE = 0;
    final public const PERIOD_TYPE_CONTRACT = 1;
    final public const PERIOD_TYPE_CASUAL = 2;
    final public const PERIOD_TYPE_PURCHASE = 3;
    final public const PERIOD_TYPE_CUSTOMER_OWNED = 4;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Customer", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected ?Customer $customer = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AssetLifecyclePeriod", cascade={"persist"})
     * @ORM\JoinColumn(name="parentPeriod_id", onDelete="SET NULL")
     */
    protected ?\App\Entity\AssetLifecyclePeriod $parentPeriod = null;

    /**
     * @ORM\Column(name="start", type="datetime")
     */
    #[Assert\NotBlank]
    protected ?\DateTimeInterface $start;

    /**
     * @ORM\Column(name="end", type="datetime", nullable=true)
     */
    protected ?\DateTimeInterface $end = null;

    /**
     * @ORM\Column(name="estimated_hire_duration", type="string", length=50, nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $estimatedHireDuration = null;

    /**
     * @ORM\Column(name="contract_hire_duration", type="string", length=50, nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $contractHireDuration = null;

    /**
     * @ORM\Column(name="hire_rate_monthly", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $hireRateMonthly = null;

    /**
     * @ORM\Column(name="hire_rate_weekly", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $hireRateWeekly = null;

    /**
     * @ORM\Column(name="client_fleet", type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $clientFleet = null;

    /**
     * @ORM\OneToMany(targetEntity="Job", mappedBy="assetLifecyclePeriod")
     * @var Collection<Job>
     */
    protected Collection $jobs;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Contact", mappedBy="assetLifecyclePeriods", cascade={"persist"})
     * @var Collection<Contact>
     */
    protected Collection $contacts;

    /**
     * @ORM\Column(name="type", type="integer")
     * @Gedmo\Versioned
     */
    protected ?int $type = self::PERIOD_TYPE_NONE;

    /**
     * @ORM\Column(name="underwriter", type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $underwriter = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SiteAddress", inversedBy="assetLifecyclePeriods", cascade={"persist"})
     * @ORM\JoinColumn(name="siteAddress_id", onDelete="SET NULL")
     * @Gedmo\Versioned
     */
    protected ?SiteAddress $siteAddress = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AssetMaintenance", mappedBy="assetLifecyclePeriod", cascade={"persist", "remove"})
     * @var Collection<AssetMaintenance>
     */
    protected Collection $maintenances;

    /**
     * @ORM\Column(name="maintenance_contract", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $maintenanceContract = false;

    /**
     * @ORM\Column(name="scheduled_lifecycle_period_cleanup", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $scheduledLifecyclePeriodCleanup = false;

    /**
     * @ORM\Column(name="maintenance_expenditure", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $maintenanceExpenditure = null;

    /**
     * @ORM\Column(name="maintenance_revenue", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $maintenanceRevenue = null;

    /**
     * @ORM\Column(name="maintenance_rate_weekly", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $maintenanceRateWeekly = null;

    /**
     * @ORM\Column(name="maintenance_rate_monthly", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $maintenanceRateMonthly = null;

    /**
     * @ORM\Column(name="maintenance_start_date", type="datetime", nullable=true)
     */
    protected ?\DateTimeInterface $maintenanceStartDate = null;

    /**
     * @ORM\Column(name="maintenance_end_date", type="datetime", nullable=true)
     */
    protected ?\DateTimeInterface $maintenanceEndDate = null;

    /**
     * @ORM\Column(name="maintenance_duration", type="integer", nullable=true)
     */
    protected ?int $maintenanceDuration = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AssetTransaction", mappedBy="assetLifecyclePeriod", cascade={"persist", "remove"})
     * @var Collection<AssetTransaction>
     */
    protected Collection $transactions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Note", mappedBy="assetLifecyclePeriod", cascade={"persist", "remove"})
     * @var Collection<Note>
     */
    protected Collection $notes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Image", mappedBy="assetLifecyclePeriod", cascade={"persist", "remove"})
     * @var Collection<Image>
     */
    protected Collection $images;

    /**
     * @ORM\Column(name="sale_price", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $salePrice = null;

    /**
     * @ORM\Column(name="sale_warranty", type="integer", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?int $saleWarranty = Warranty::WARRANTY_NONE;

    /**
     * @ORM\Column(name="sale_warranty_duration", type="integer", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?int $saleWarrantyDuration = 0;

    /**
     * @ORM\Column(name="sale_warranty_start_date", type="datetime", nullable=true)
     */
    protected ?\DateTimeInterface $saleWarrantyStartDate = null;

    /**
     * @ORM\Column(name="residual_value", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $residualValue = null;

    /**
     * @ORM\Column(name="trade_in_value", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $tradeInValue = null;

    /**
     * @ORM\Column(name="hours_per_annum", type="integer", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?int $hoursPerAnnum = null;

    /**
     * @ORM\Column(name="loler_interval", type="integer", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?int $lolerInterval = 12;

    /**
     * @ORM\Column(name="service_interval", type="integer", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?int $serviceInterval = 12;

    public function __construct(/**
     * @ORM\ManyToOne(targetEntity="App\Entity\Asset", inversedBy="lifecyclePeriods", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
        protected ?Asset $asset = null
    ) {
        $this->contacts     = new ArrayCollection();
        $this->maintenances = new ArrayCollection();
        $this->notes        = new ArrayCollection();
        $this->images       = new ArrayCollection();
        $this->jobs         = new ArrayCollection();
        $this->transactions = new ArrayCollection();
    }

    // Used to remove any configuration that is not relevant to the period type
    public function cleanup(): void
    {
        switch ($this->type) {
            case self::PERIOD_TYPE_NONE:
                $this->estimatedHireDuration    = null;
                $this->contractHireDuration     = null;
                $this->hireRateMonthly          = null;
                $this->hireRateWeekly           = null;
                $this->underwriter              = null;
                $this->maintenanceRateWeekly    = null;
                $this->maintenanceRateMonthly   = null;
                $this->maintenanceStartDate     = null;
                $this->maintenanceEndDate       = null;
                $this->maintenanceDuration      = null;
                $this->salePrice                = null;
                $this->saleWarranty             = null;
                $this->saleWarrantyDuration     = null;
                $this->saleWarrantyStartDate    = null;
                $this->hoursPerAnnum            = null;
                break;
            case self::PERIOD_TYPE_CASUAL:
                $this->contractHireDuration     = null;
                $this->maintenanceRateWeekly    = null;
                $this->maintenanceRateMonthly   = null;
                $this->maintenanceStartDate     = null;
                $this->maintenanceEndDate       = null;
                $this->maintenanceDuration      = null;
                $this->salePrice                = null;
                $this->saleWarranty             = null;
                $this->saleWarrantyDuration     = null;
                $this->saleWarrantyStartDate    = null;
                $this->hoursPerAnnum            = null;
                break;
            case self::PERIOD_TYPE_CONTRACT:
                $this->estimatedHireDuration    = null;
                $this->saleWarranty             = null;
                $this->saleWarrantyDuration     = null;
                $this->saleWarrantyStartDate    = null;
                $this->maintenanceStartDate     = $this->start;
                $this->maintenanceEndDate       = $this->end;
                $this->maintenanceDuration      = null;
                $this->maintenanceContract      = true;
                break;
            case self::PERIOD_TYPE_PURCHASE:
                $this->contractHireDuration     = null;
                $this->estimatedHireDuration    = null;
                $this->hireRateMonthly          = null;
                $this->hireRateWeekly           = null;
                $this->underwriter              = null;

                if ($this->saleWarranty) {
                    $this->saleWarrantyStartDate = $this->start;
                }

                if ($this->maintenanceContract) {
                    $this->maintenanceStartDate = $this->start;

                    $this->maintenanceEndDate = clone $this->maintenanceStartDate;
                    $this->maintenanceEndDate->modify('+' . $this->maintenanceDuration . ' months');
                }

                break;
            case self::PERIOD_TYPE_CUSTOMER_OWNED:
                $this->estimatedHireDuration = null;
                $this->hireRate              = null;
                $this->underwriter           = null;

                if ($this->saleWarranty) {
                    $this->saleWarrantyStartDate = $this->start;
                }

                if ($this->maintenanceContract) {
                    $this->maintenanceStartDate = $this->start;

                    $this->maintenanceEndDate = clone $this->maintenanceStartDate;
                    $this->maintenanceEndDate->modify('+' . $this->maintenanceDuration . ' months');
                }

                break;
        }
    }

    public function checkDates(ExecutionContextInterface $context): void
    {
        if ($this->start && $this->end && $this->end < $this->start) {
            $context->addViolation('End date cannot be before start date.');
        }
    }

    public function hasActiveMaintenance(): bool
    {
        $maintenanceStartDate = $this->getMaintenanceStartDate();
        $maintenanceDuration  = $this->getMaintenanceDuration();
        $maintenanceEndDate   = $this->getMaintenanceEndDate();

        if (!$maintenanceStartDate || (!$maintenanceDuration && !$maintenanceEndDate)) {
            return false;
        }

        if (!$maintenanceEndDate) {
            $maintenanceEndDate = new \DateTime($maintenanceStartDate->format('Y-m-d H:i:s'));
            $maintenanceEndDate->modify('+' . $maintenanceDuration . ' months');
        }

        $now = new \DateTime();

        return $maintenanceStartDate <= $now && $maintenanceEndDate >= $now;
    }

    public function isContractHire(): bool
    {
        return $this->type == self::PERIOD_TYPE_CONTRACT;
    }

    public function isCasualHire(): bool
    {
        return $this->type == self::PERIOD_TYPE_CASUAL;
    }

    public function isPurchase(): bool
    {
        return $this->type == self::PERIOD_TYPE_PURCHASE;
    }

    public function getDescription(): string
    {
        return sprintf(
            '%s - %s (%s)',
            $this->getCustomerName(),
            $this->getTypeString(),
            $this->getDateString(),
        );
    }

    public function getCustomerName(): ?string
    {
        return $this->customer ? $this->customer->getName() : 'None (Asset Register)';
    }

    public function getDateString(): string
    {
        if (!$this->start) {
            return '-';
        }

        $parts[] = $this->start->format('Y-m-d');

        if ($this->end) {
            $parts[] = $this->end->format('Y-m-d');
        }

        return implode(' - ', $parts);
    }

    public function getTypeString()
    {
        switch ($this->type) {
            case self::PERIOD_TYPE_NONE:
                return 'None (Asset Register)';
            case self::PERIOD_TYPE_CASUAL:
                return 'Casual Hire';
            case self::PERIOD_TYPE_CONTRACT:
                return 'Contract Hire';
            case self::PERIOD_TYPE_PURCHASE:
                return 'Sold';
        }
    }

    public function getLastJobHoursRecorded()
    {
        $jobs = $this->jobs->toArray();
        $jobsHours = [];
        foreach ($jobs as $job) {
            $jobsHours[] = $job->getHourReading();
        }

        return $jobsHours ? end($jobsHours) : '';
    }

    public function getLastDateServiceJob()
    {
        $jobs = $this->jobs->toArray();
        $jobsDate = [];
        foreach ($jobs as $job) {
            if ($job->getIsServiceJob() === true && $job->getCompleted() === true) {
                $jobsDate[] = $job->getCompletedAt();
            }
        }

        return $jobsDate ? max($jobsDate) : null;
    }

    public function getLastDateLolerJob()
    {
        $jobs = $this->jobs->toArray();
        $jobsDate = [];
        foreach ($jobs as $job) {
            if ($job->getIsLolerJob() === true && $job->getCompleted() === true) {
                $jobsDate[] = $job->getCompletedAt();
            }
        }

        return $jobsDate ? max($jobsDate) : null;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function addContact($value): AssetLifecyclePeriod
    {
        $this->contacts->add($value);

        return $this;
    }

    public function removeContact($value): void
    {
        $this->contacts->removeElement($value);
    }

    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function addJob($value): AssetLifecyclePeriod
    {
        $this->jobs->add($value);

        return $this;
    }

    public function removeJob($value): void
    {
        $this->jobs->removeElement($value);
    }

    public function getJobs(): Collection
    {
        return $this->jobs;
    }

    public function addNote($value): AssetLifecyclePeriod
    {
        $this->notes->add($value);

        return $this;
    }

    public function removeNote($value): void
    {
        $this->notes->removeElement($value);
    }

    public function getNotes(): Collection
    {
        return $this->notes;
    }

    public function getSiteAddressString(): ?string
    {
        return $this->siteAddress ? $this->siteAddress->getAddressString() : $this->customer->getBillingAddress()->getAddressString();
    }

    public function addMaintenance($value): AssetLifecyclePeriod
    {
        $this->maintenances->add($value);

        return $this;
    }

    public function removeMaintenance($value): void
    {
        $this->maintenances->removeElement($value);
    }

    public function getMaintenances(): Collection
    {
        return $this->maintenances;
    }

    public function addTransaction($value): AssetLifecyclePeriod
    {
        $this->transactions->add($value);

        return $this;
    }

    public function removeTransaction($value): void
    {
        $this->transactions->removeElement($value);
    }

    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function incrementMaintenanceExpenditure($value): AssetLifecyclePeriod
    {
        $this->maintenanceExpenditure += $value;

        return $this;
    }

    public function addImage($value): AssetLifecyclePeriod
    {
        $this->images->add($value);
        $value->setTruck($this);

        return $this;
    }

    public function removeImage($value): AssetLifecyclePeriod
    {
        $this->images->removeElement($value);

        return $this;
    }

    public function getImages(): Collection
    {
        return $this->images;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $value): AssetLifecyclePeriod
    {
        $this->deletedAt = $value;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $value): AssetLifecyclePeriod
    {
        $this->customer = $value;

        return $this;
    }

    public function getAsset(): ?Asset
    {
        return $this->asset;
    }

    public function setAsset(?Asset $value): AssetLifecyclePeriod
    {
        $this->asset = $value;

        return $this;
    }

    public function getParentPeriod(): ?AssetLifecyclePeriod
    {
        return $this->parentPeriod;
    }

    public function setParentPeriod(?AssetLifecyclePeriod $value): AssetLifecyclePeriod
    {
        $this->parentPeriod = $value;

        return $this;
    }

    public function getStart(): ?\DateTimeInterface
    {
        return $this->start;
    }

    public function setStart(?\DateTimeInterface $value): AssetLifecyclePeriod
    {
        $this->start = $value;

        return $this;
    }

    public function getEnd(): ?\DateTimeInterface
    {
        return $this->end;
    }

    public function setEnd(?\DateTimeInterface $value): AssetLifecyclePeriod
    {
        $this->end = $value;

        return $this;
    }

    public function getHireRateMonthly(): ?string
    {
        return $this->hireRateMonthly;
    }

    public function setHireRateMonthly(?string $value): AssetLifecyclePeriod
    {
        $this->hireRateMonthly = $value;

        return $this;
    }

    public function getHireRateWeekly(): ?string
    {
        return $this->hireRateWeekly;
    }

    public function setHireRateWeekly(?string $value): AssetLifecyclePeriod
    {
        $this->hireRateWeekly = $value;

        return $this;
    }

    public function getClientFleet(): ?string
    {
        return $this->clientFleet;
    }

    public function setClientFleet(?string $value): AssetLifecyclePeriod
    {
        $this->clientFleet = $value;

        return $this;
    }

    /**
     * @param (Collection & \iterable<Job>) $value
     */
    public function setJobs(Collection $value): AssetLifecyclePeriod
    {
        $this->jobs = $value;

        return $this;
    }

    public function setContacts(Collection $value): AssetLifecyclePeriod
    {
        $this->contacts = $value;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(?int $value): AssetLifecyclePeriod
    {
        $this->type = $value;

        return $this;
    }

    public function getUnderwriter(): ?string
    {
        return $this->underwriter;
    }

    public function setUnderwriter(?string $value): AssetLifecyclePeriod
    {
        $this->underwriter = $value;

        return $this;
    }

    public function getSiteAddress(): ?SiteAddress
    {
        return $this->siteAddress;
    }

    public function setSiteAddress(?SiteAddress $value): AssetLifecyclePeriod
    {
        $this->siteAddress = $value;

        return $this;
    }

    /**
     * @param (Collection & \iterable<AssetMaintenance>) $value
     */
    public function setMaintenances(Collection $value): AssetLifecyclePeriod
    {
        $this->maintenances = $value;

        return $this;
    }

    public function getMaintenanceExpenditure(): ?string
    {
        return $this->maintenanceExpenditure;
    }

    public function setMaintenanceExpenditure(?string $value): AssetLifecyclePeriod
    {
        $this->maintenanceExpenditure = $value;

        return $this;
    }

    public function getMaintenanceRevenue(): ?string
    {
        return $this->maintenanceRevenue;
    }

    public function setMaintenanceRevenue(?string $value): AssetLifecyclePeriod
    {
        $this->maintenanceRevenue = $value;

        return $this;
    }

    public function getMaintenanceStartDate(): ?\DateTimeInterface
    {
        return $this->maintenanceStartDate;
    }

    public function setMaintenanceStartDate(?\DateTimeInterface $value): AssetLifecyclePeriod
    {
        $this->maintenanceStartDate = $value;

        return $this;
    }

    public function getMaintenanceEndDate(): ?\DateTimeInterface
    {
        return $this->maintenanceEndDate;
    }

    public function setMaintenanceEndDate(?\DateTimeInterface $value): AssetLifecyclePeriod
    {
        $this->maintenanceEndDate = $value;

        return $this;
    }

    public function getSalePrice(): ?string
    {
        return $this->salePrice;
    }

    public function setSalePrice(?string $value): AssetLifecyclePeriod
    {
        $this->salePrice = $value;

        return $this;
    }

    public function getSaleWarranty(): ?int
    {
        return $this->saleWarranty;
    }

    public function setSaleWarranty(?int $value): AssetLifecyclePeriod
    {
        $this->saleWarranty = $value;

        return $this;
    }

    public function getSaleWarrantyDuration(): ?int
    {
        return $this->saleWarrantyDuration;
    }

    public function setSaleWarrantyDuration(?int $value): AssetLifecyclePeriod
    {
        $this->saleWarrantyDuration = $value;

        return $this;
    }

    public function getSaleWarrantyStartDate(): ?\DateTimeInterface
    {
        return $this->saleWarrantyStartDate;
    }

    public function setSaleWarrantyStartDate(?\DateTimeInterface $value): AssetLifecyclePeriod
    {
        $this->saleWarrantyStartDate = $value;

        return $this;
    }

    public function getMaintenanceRateWeekly(): ?string
    {
        return $this->maintenanceRateWeekly;
    }

    public function setMaintenanceRateWeekly(?string $value): AssetLifecyclePeriod
    {
        $this->maintenanceRateWeekly = $value;

        return $this;
    }

    public function getMaintenanceRateMonthly(): ?string
    {
        return $this->maintenanceRateMonthly;
    }

    public function setMaintenanceRateMonthly(?string $value): AssetLifecyclePeriod
    {
        $this->maintenanceRateMonthly = $value;

        return $this;
    }

    public function getEstimatedHireDuration(): ?string
    {
        return $this->estimatedHireDuration;
    }

    public function setEstimatedHireDuration(?string $value): AssetLifecyclePeriod
    {
        $this->estimatedHireDuration = $value;

        return $this;
    }

    public function getContractHireDuration(): ?string
    {
        return $this->contractHireDuration;
    }

    public function setContractHireDuration(?string $value): AssetLifecyclePeriod
    {
        $this->contractHireDuration = $value;

        return $this;
    }

    public function getResidualValue(): ?string
    {
        return $this->residualValue;
    }

    public function setResidualValue(?string $value): AssetLifecyclePeriod
    {
        $this->residualValue = $value;

        return $this;
    }

    public function getHoursPerAnnum(): ?int
    {
        return $this->hoursPerAnnum;
    }

    public function setHoursPerAnnum(?int $value): AssetLifecyclePeriod
    {
        $this->hoursPerAnnum = $value;

        return $this;
    }

    public function getMaintenanceContract(): ?bool
    {
        return $this->maintenanceContract;
    }

    public function setScheduledLifecyclePeriodCleanup(?bool $scheduledLifecyclePeriodCleanup): AssetLifecyclePeriod
    {
        $this->scheduledLifecyclePeriodCleanup = $scheduledLifecyclePeriodCleanup;

        return $this;
    }

    public function getScheduledLifecyclePeriodCleanup(): ?bool
    {
        return $this->scheduledLifecyclePeriodCleanup;
    }

    public function setMaintenanceContract(?bool $value): AssetLifecyclePeriod
    {
        $this->maintenanceContract = $value;

        return $this;
    }

    public function getTradeInValue(): ?string
    {
        return $this->tradeInValue;
    }

    public function setTradeInValue(?string $value): AssetLifecyclePeriod
    {
        $this->tradeInValue = $value;

        return $this;
    }

    public function getMaintenanceDuration(): ?int
    {
        return $this->maintenanceDuration;
    }

    public function setMaintenanceDuration(?int $value): AssetLifecyclePeriod
    {
        $this->maintenanceDuration = $value;

        return $this;
    }

    public function getLolerInterval(): ?int
    {
        return $this->lolerInterval;
    }

    public function setLolerInterval(?int $value): AssetLifecyclePeriod
    {
        $this->lolerInterval = $value;

        return $this;
    }

    public function getServiceInterval(): ?int
    {
        return $this->serviceInterval;
    }

    public function setServiceInterval(?int $value): AssetLifecyclePeriod
    {
        $this->serviceInterval = $value;

        return $this;
    }

    public function getChargeableJobsRevenue(): int|float
    {
        $total = 0;

        foreach ($this->jobs as $job) {
            if ($job->getCompleted() && $job->getChargeMethod() === ChargeMethod::INVOICE_CUSTOMER) {
                $total += $job->getFinalCost();
            }
        }

        return $total;
    }

    public function getNotChargeableJobsRevenue(): int|float
    {
        $total = 0;

        foreach ($this->jobs as $job) {
            if ($job->getCompleted() && $job->getChargeMethod() === ChargeMethod::CHARGE_AGAINST_MAINTENANCE) {
                $total += $job->getFinalCostInternal();
            }
        }

        return $total;
    }

    public function getChargeableJobsProfit(): int|float
    {
        $total = 0;

        foreach ($this->jobs as $job) {
            if ($job->getCompleted() && $job->getChargeMethod() === ChargeMethod::INVOICE_CUSTOMER) {
                $total += $job->getFinalCostInternal();
            }
        }

        return $this->getChargeableJobsRevenue() - $total;
    }

    public function getMaintenanceContractBalance(): int|float
    {
        return $this->getMaintenanceContractRevenue() - $this->getNotChargeableJobsRevenue();
    }

    public function getOverallMaintenanceProfit(): float|int|array
    {
        $total = $this->getMaintenanceContractBalance() + $this->getChargeableJobsProfit();

        return $total;
    }

    public function getMaintenanceContractRevenue(): int|float
    {
        $firstDate = $this->maintenanceStartDate;

        if (!$firstDate) {
            return 0;
        }

        $interval = $firstDate->diff(new \DateTime());

        $moth = 0;

        if ($interval->y) {
            $moth = $moth + $interval->format('%y') * 12;
        }
        if ($interval->m) {
            $moth = $moth + $interval->format('%m');
        }

        return $moth * $this->maintenanceRateMonthly;
    }
}
