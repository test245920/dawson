<?php

namespace App\Entity;

use App\Model\ChargeMethod;
use App\Model\SearchableInterface;
use App\Model\WorklogRate;
use App\Util\StringHelpers;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * WorkLog.
 *
 * @ORM\Table(name="work_log")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\WorkLogRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
#[Assert\Callback(callback: 'validate')]
class WorkLog implements SearchableInterface
{
    final public const DEFAULT_RATE_MULTIPLIER = 1;
    final public const INTERNAL_LABOUR_RATE = 55;
    final public const DEFAULT_LABOUR_RATE  = 65;

    /*
     * Non-persisted fields, for form use only
     */
    public $requiresParts;

    public $unassignJob;

    public $completeJob;

    public $postponeJob;

    public $assetLifecyclePeriod;

    public bool $allowNoTimeLogged = false;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Gedmo\Versioned
     */
    protected ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="workLogs", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    #[Assert\NotNull(message: 'Engineer required')]
    protected ?User $user = null;

    /**
     * @ORM\Column(name="description", type="text")
     * @Gedmo\Versioned
     */
    protected ?string $description = null;

    /**
     * @ORM\Column(name="time_labour", type="integer")
     * @Gedmo\Versioned
     */
    protected ?int $timeLabour = null;

    /**
     * @ORM\Column(name="time_labour_ot", type="integer")
     * @Gedmo\Versioned
     */
    protected ?int $timeLabourOvertime = null;

    /**
     * @ORM\Column(name="time_labour_eot", type="integer")
     * @Gedmo\Versioned
     */
    protected ?int $timeLabourExtremeOvertime = null;

    /**
     * @ORM\Column(name="time_travel", type="integer")
     * @Gedmo\Versioned
     */
    protected ?int $timeTravel = null;

    /**
     * @ORM\Column(name="time_travel_ot", type="integer")
     * @Gedmo\Versioned
     */
    protected ?int $timeTravelOvertime = null;

    /**
     * @ORM\Column(name="time_travel_eot", type="integer")
     * @Gedmo\Versioned
     */
    protected ?int $timeTravelExtremeOvertime = null;

    /**
     * @ORM\Column(name="startDate", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $startDate = null;

    /**
     * @ORM\Column(name="created", type="datetime")
     * @Gedmo\Versioned
     */
    protected \DateTime $created;

    /**
     * @ORM\Column(name="updated", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected \DateTime $updated;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\Column(name="requested_parts", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $requestedParts = null;

    /**
     * @ORM\Column(name="requested_parts_complete", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $requestedPartsComplete = false;

    /**
     * @ORM\Column(name="hour_reading", type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $hourReading = null;

    /**
     * @ORM\Column(name="user_cost_multiplier", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $userCostMultiplier = 1;

    /**
     * @ORM\Column(name="labour_rate", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $labourRate = null;

    /**
     * @ORM\Column(name="labour_rate_internal", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $labourRateInternal = null;

    public function __construct(/**
     * @ORM\ManyToOne(targetEntity="App\Entity\Job", inversedBy="workLogs", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
        protected ?Job $job,
        private $engineer = false
    ) {
        $this->created = new \DateTime();
        $this->updated = new \DateTime();
    }

    public static function getSearchFields(): array
    {
        return ['id'            => 'ID', 'description'   => 'Description', 'timeLabour'    => 'Time Labour', 'timeTravel'    => 'Time Travel', 'startDate'     => 'Start Date', 'created'       => 'Created', 'updated'       => 'Updated'];
    }

    public static function getSearchCategory(): string
    {
        return 'workLog';
    }

    public function hasLabourTimeLogged(): bool
    {
        return $this->timeLabour || $this->timeLabourOvertime || $this->timeTravelExtremeOvertime;
    }

    public function hasTravelTimeLogged(): bool
    {
        return $this->timeTravel || $this->timeTravelOvertime || $this->timeTravelExtremeOvertime;
    }

    public function validate(ExecutionContextInterface $executionContext): void
    {
        $logComplete = $this->description
            && ($this->hasLabourTimeLogged() || $this->allowNoTimeLogged)
            && !$this->completeJob;

        if ($logComplete) {
            return;
        }

        $logRequired = true;
        $job         = $this->job;
        $workLogs    = $job->getWorklogs();
        $user        = $this->user;

        $userWorkLogs = array_filter($workLogs, function ($workLog) use ($user): ?bool {
            if ($user) {
                return $workLog->getUser()->getId() === $user->getId();
            }

            return null;
        });

        if ($this->completeJob) {
            if (!$this->assetLifecyclePeriod && !$job->getAssetLifecyclePeriod()) {
                $executionContext->addViolation('Please choose an asset.', []);
            }

            if (count((array) $userWorkLogs)) {
                $logRequired = false;
            }
        }

        $descMissing = false;
        $timeLabourMissing = false;

        if (($logRequired || $this->hasLabourTimeLogged()) && !$this->description) {
            $descMissing = true;
        }

        if (($logRequired || $this->description) && !$this->hasLabourTimeLogged()) {
            $timeLabourMissing = true;
        }

        if ($descMissing) {
            $executionContext->addViolation('Required.', []);
        }

        if ($timeLabourMissing && $this->engineer) {
            $executionContext->addViolation('Required.', []);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setUser(?User $user): WorkLog
    {
        $this->user = $user;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setJob(?Job $job): WorkLog
    {
        $this->job = $job;

        return $this;
    }

    public function getJob(): ?Job
    {
        return $this->job;
    }

    public function setDescription(?string $description): WorkLog
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getAdjustedTotalLabourTime(): float
    {
        return $this->timeLabour + ($this->timeLabourOvertime * WorklogRate::RATE_OVERTIME) + ($this->timeLabourExtremeOvertime * WorklogRate::RATE_EXTREME_OVERTIME);
    }

    public function setTimeLabour(?int $time): WorkLog
    {
        $this->timeLabour = $time;

        return $this;
    }

    public function getTimeLabour($asString = false): int|null|string
    {
        if (!$asString) {
            return $this->timeLabour;
        }

        return $this->getTimeAsString($this->timeLabour);
    }

    public function getTimeLabourOvertime($asString = false): int|null|string
    {
        if (!$asString) {
            return $this->timeLabourOvertime;
        }

        return $this->getTimeAsString($this->timeLabourOvertime);
    }

    public function setTimeLabourOvertime(?int $value): WorkLog
    {
        $this->timeLabourOvertime = $value;

        return $this;
    }

    public function getTimeLabourExtremeOvertime($asString = false): int|null|string
    {
        if (!$asString) {
            return $this->timeLabourExtremeOvertime;
        }

        return $this->getTimeAsString($this->timeLabourExtremeOvertime);
    }

    public function setTimeLabourExtremeOvertime(?int $value): WorkLog
    {
        $this->timeLabourExtremeOvertime = $value;

        return $this;
    }

    public function getAdjustedTotalTravelTime(): float
    {
        return $this->timeTravel + ($this->timeTravelOvertime * WorklogRate::RATE_OVERTIME) + ($this->timeTravelExtremeOvertime * WorklogRate::RATE_EXTREME_OVERTIME);
    }

    public function setTimeTravel(?int $time): WorkLog
    {
        $this->timeTravel = $time;

        return $this;
    }

    public function getTimeTravel($asString = false): int|null|string
    {
        if (!$asString) {
            return $this->timeTravel;
        }

        return $this->getTimeAsString($this->timeTravel);
    }

    public function getTimeTravelOvertime($asString = false): int|null|string
    {
        if (!$asString) {
            return $this->timeTravelOvertime;
        }

        return $this->getTimeAsString($this->timeTravelOvertime);
    }

    public function setTimeTravelOvertime(?int $value): WorkLog
    {
        $this->timeTravelOvertime = $value;

        return $this;
    }

    public function getTimeTravelExtremeOvertime($asString = false): int|null|string
    {
        if (!$asString) {
            return $this->timeTravelExtremeOvertime;
        }

        return $this->getTimeAsString($this->timeTravelExtremeOvertime);
    }

    public function setTimeTravelExtremeOvertime(?int $value): WorkLog
    {
        $this->timeTravelExtremeOvertime = $value;

        return $this;
    }

    public function getTimeLoggedRaw(): int
    {
        $totalTime = $this->timeLabour +
                    $this->timeTravel +
                    $this->timeLabourOvertime +
                    $this->timeLabourExtremeOvertime +
                    $this->timeTravelOvertime +
                    $this->timeTravelExtremeOvertime;

        return $totalTime;
    }

    public function getAllTimeLabourString(): string
    {
        return StringHelpers::formatWorklogTimesAsString($this->timeLabour, $this->timeLabourOvertime, $this->timeLabourExtremeOvertime);
    }

    public function getAllTimeTravelString(): string
    {
        return StringHelpers::formatWorklogTimesAsString($this->timeTravel, $this->timeTravelOvertime, $this->timeTravelExtremeOvertime);
    }

    public function setStartDate(?\DateTimeInterface $startDate): WorkLog
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setCreated(\DateTime $created): WorkLog
    {
        $this->created = $created;

        return $this;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function setUpdated(\DateTime $updated): WorkLog
    {
        $this->updated = $updated;

        return $this;
    }

    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): WorkLog
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function getRequestedParts(): ?string
    {
        return $this->requestedParts;
    }

    public function setRequestedParts(?string $value): WorkLog
    {
        $this->requestedParts = $value;

        return $this;
    }

    public function getRequestedPartsComplete(): ?bool
    {
        return $this->requestedPartsComplete;
    }

    public function setRequestedPartsComplete(?bool $value): WorkLog
    {
        $this->requestedPartsComplete = $value;

        return $this;
    }

    public function getHourReading(): ?string
    {
        return $this->hourReading;
    }

    public function setHourReading(?string $value): WorkLog
    {
        $this->hourReading = $value;

        return $this;
    }

    public function setAssetLifecyclePeriod(?AssetLifecyclePeriod $value = null): WorkLog
    {
        $this->assetLifecyclePeriod = $value;

        return $this;
    }

    public function getAssetLifecyclePeriod()
    {
        return $this->assetLifecyclePeriod;
    }

    public function getUserCostMultiplier(): float|int
    {
        return $this->userCostMultiplier != null ? $this->userCostMultiplier : self::DEFAULT_RATE_MULTIPLIER;
    }

    public function setUserCostMultiplier(?string $value): WorkLog
    {
        $this->userCostMultiplier = $value;

        return $this;
    }

    public function calculateAndSetLabourRates(): void
    {
        $this->labourRateInternal = self::INTERNAL_LABOUR_RATE;

        if (!$this->job) {
            $this->labourRate = self::DEFAULT_LABOUR_RATE;

            return;
        }

        $this->labourRate = ($this->job->getChargeMethod() !== ChargeMethod::INVOICE_CUSTOMER || !$this->job->getCustomer())
            ? self::INTERNAL_LABOUR_RATE
            : ($this->job->getCustomer()->getLabourRate() ?? self::DEFAULT_LABOUR_RATE);
    }

    public function getLabourRate(): ?string
    {
        return $this->labourRate;
    }

    public function setLabourRate(?string $value): WorkLog
    {
        $this->labourRate = $value;

        return $this;
    }

    public function getLabourRateInternal(): ?string
    {
        return $this->labourRateInternal;
    }

    public function setLabourRateInternal(?string $value): WorkLog
    {
        $this->labourRateInternal = $value;

        return $this;
    }

    private function getTimeAsString(?int $time): string
    {
        return StringHelpers::formatTimeAsString($time);
    }
}
