<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * App\Entity\SentEmail.
 *
 * @ORM\Table(name="sent_email")
 * @ORM\Entity()
 */
class SentEmail
{
    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected \DateTime $createdAt;

    /**
     * @ORM\Column(name="errorMessage", type="string", nullable=true)
     */
    protected ?string $errorMessage = null;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="array", nullable=false)
     */
    #[Assert\NotNull]
    private $recipients;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    #[Assert\NotNull]
    private string $subject;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    #[Assert\NotNull]
    private string $body;

    /**
     * @ORM\Column(name="sesMessageId", type="string", nullable=true)
     */
    private ?string $sesMessageId = null;

    /**
     * @ORM\Column(name="sendStatusOk", type="boolean", nullable=false)
     */
    #[Assert\NotNull]
    private bool $sendStatusOk;

    /**
     * @ORM\Column(name="sendStatusCode", type="integer", nullable=false)
     */
    #[Assert\NotNull]
    private int $sendStatusCode;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setRecipients($value): void
    {
        $this->recipients = $value;
    }

    public function getRecipients()
    {
        return $this->recipients;
    }

    public function setSubject(string $value): void
    {
        $this->subject = $value;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function setBody(string $value): void
    {
        $this->body = $value;
    }

    public function getBody(): string
    {
        return $this->body;
    }

    public function setSesMessageId(?string $value): void
    {
        $this->sesMessageId = $value;
    }

    public function getSesMessageId(): ?string
    {
        return $this->sesMessageId;
    }

    public function setSendStatusOk(bool $value): void
    {
        $this->sendStatusOk = $value;
    }

    public function getSendStatusOk(): bool
    {
        return $this->sendStatusOk;
    }

    public function setSendStatusCode(int $value): void
    {
        $this->sendStatusCode = $value;
    }

    public function getSendStatusCode(): int
    {
        return $this->sendStatusCode;
    }

    public function setErrorMessage(?string $value): void
    {
        $this->errorMessage = $value;
    }

    public function getErrorMessage(): ?string
    {
        return $this->errorMessage;
    }

    public function setCreatedAt(\DateTime $value): void
    {
        $this->createdAt = $value;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }
}
