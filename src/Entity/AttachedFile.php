<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Busy\BusyCoreBundle\Entity\File.
 *
 * @ORM\Table(name="attached_files")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\AttachedFileRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class AttachedFile
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(name="path", type="string")
     * @Gedmo\Versioned
     */
    protected ?string $path = null;

    /**
     * @ORM\Column(name="name", type="string")
     * @Gedmo\Versioned
     */
    protected ?string $name = null;

    #[Assert\Valid]
    protected $file;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Invoice", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Gedmo\Versioned
     */
    protected ?Invoice $invoice = null;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected ?\DateTimeInterface $createdAt;

    /**
     * @ORM\Column(name="updated", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    protected ?\DateTimeInterface $updated;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\Column(name="description", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $description = null;

    /*
    * @ORM\Column(name="customer_viewed_at", type="datetime", nullable=true)
    * @Gedmo\Versioned
    */
    protected $customerViewedAt;

    /*
    * @ORM\Column(name="customer_downloaded_at", type="datetime", nullable=true)
    * @Gedmo\Versioned
    */
    protected $customerDownloadedAt;

    /**
     * @ORM\Column(name="uuid", type="string")
     * @Gedmo\Versioned
     */
    protected ?string $uuid = null;

    public function __construct()
    {
        $this->uuid = str_replace("\n", '', shell_exec('uuidgen -r'));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(?string $value): AttachedFile
    {
        $this->uuid = $value;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(?string $value): AttachedFile
    {
        $this->path = $value;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $value): AttachedFile
    {
        $this->name = $value;

        return $this;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFile($value): AttachedFile
    {
        $this->file = $value;

        return $this;
    }

    public function getInvoice(): ?Invoice
    {
        return $this->invoice;
    }

    public function setInvoice(?Invoice $value): AttachedFile
    {
        $this->invoice = $value;
        $value->setAttachedFile($this);

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(?\DateTimeInterface $value): AttachedFile
    {
        $this->updated = $value;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $value): void
    {
        $this->deletedAt = $value;
    }

    public function getChangeRecord()
    {
        return $this->changeRecord;
    }

    public function setChangeRecord($value): void
    {
        $this->changeRecord = $value;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $value): AttachedFile
    {
        $this->description = $value;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $value): void
    {
        $this->createdAt = $value;
    }

    public function getCustomerViewedAt()
    {
        return $this->customerViewedAt;
    }

    public function setCustomerViewedAt($value): void
    {
        $this->customerViewedAt = $value;
    }

    public function getCustomerDownloadedAt()
    {
        return $this->customerDownloadedAt;
    }

    public function setCustomerDownloadedAt($value): void
    {
        $this->customerDownloadedAt = $value;
    }
}
