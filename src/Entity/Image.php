<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * App\Entity\Image.
 *
 * @ORM\Table(name="image")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\ImageRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Image
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(name="path", type="string")
     * @Gedmo\Versioned
     */
    protected ?string $path = null;

    /**
     * @ORM\Column(name="file_name", type="string")
     * @Gedmo\Versioned
     */
    protected ?string $filename = null;

    #[Assert\Valid]
    protected $image;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AssetLifecyclePeriod", cascade={"persist"}, inversedBy="images")
     * @ORM\JoinColumn(onDelete="SET NULL", name="assetlifecycleperiod_id")
     * @Gedmo\Versioned
     */
    protected ?AssetLifecyclePeriod $assetLifecyclePeriod = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Asset", cascade={"persist"}, inversedBy="images")
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Gedmo\Versioned
     */
    protected ?Asset $asset = null;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected ?\DateTimeInterface $createdAt = null;

    /**
     * @ORM\Column(name="updated", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    protected ?\DateTimeInterface $updated = null;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Job", cascade={"persist"}, inversedBy="images")
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Gedmo\Versioned
     */
    protected ?Job $job = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\EstimateItem", cascade={"persist"}, inversedBy="images")
     * @ORM\JoinColumn(name="estimateitem_id", onDelete="SET NULL")
     * @Gedmo\Versioned
     */
    protected ?EstimateItem $estimateItem = null;

    /**
     * @ORM\Column(name="description", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $description = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(?string $value): Image
    {
        $this->path = $value;

        return $this;
    }

    public function getFileName(): ?string
    {
        return $this->filename;
    }

    public function setFileName(?string $value): Image
    {
        $this->filename = $value;

        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($value): Image
    {
        $this->image = $value;

        return $this;
    }

    public function getAssetLifecyclePeriod(): ?AssetLifecyclePeriod
    {
        return $this->assetLifecyclePeriod;
    }

    public function setAssetLifecyclePeriod(?AssetLifecyclePeriod $value): Image
    {
        $this->assetLifecyclePeriod = $value;

        return $this;
    }

    public function getAsset(): ?Asset
    {
        return $this->asset;
    }

    public function setAsset(?Asset $value = null): Image
    {
        $this->asset = $value;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(?\DateTimeInterface $value): Image
    {
        $this->updated = $value;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $value): void
    {
        $this->deletedAt = $value;
    }

    public function getcreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getJob(): ?Job
    {
        return $this->job;
    }

    public function setJob(?Job $value): Image
    {
        $this->job = $value;

        return $this;
    }

    public function getEstimateItem(): ?EstimateItem
    {
        return $this->estimateItem;
    }

    public function setEstimateItem(?EstimateItem $value): Image
    {
        $this->estimateItem = $value;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $value): Image
    {
        $this->description = $value;

        return $this;
    }
}
