<?php

namespace App\Entity;

use App\Model\SearchableInterface;
use App\Model\TransactionCategory;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * StockTransaction.
 *
 * @ORM\Table(name="stock_transaction")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\StockTransactionRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class StockTransaction implements SearchableInterface
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(name="date", type="datetime")
     * @Gedmo\Versioned
     * @Gedmo\Timestampable(on="create")
     */
    protected ?\DateTimeInterface $date = null;

    /**
     * @ORM\Column(name="createdAt", type="datetime")
     * @Gedmo\Versioned
     * @Gedmo\Timestampable(on="create")
     */
    protected ?\DateTimeInterface $createdAt = null;

    /**
     * @ORM\Column(name="updatedAt", type="datetime")
     * @Gedmo\Versioned
     * @Gedmo\Timestampable(on="update")
     */
    protected ?\DateTimeInterface $updatedAt = null;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\Column(name="quantity", type="integer")
     * @Gedmo\Versioned
     */
    protected ?int $quantity = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Job", inversedBy="stockTransactions", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected ?Job $job = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PurchaseOrder", inversedBy="stockTransactions", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL", name="purchaseOrder_id")
     */
    protected ?PurchaseOrder $purchaseOrder = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Stock", cascade={"persist"}, inversedBy="stockTransactions")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected ?Stock $stock = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SupplierStock", cascade={"persist"})
     * @ORM\JoinColumn(name="supplierStock_id", onDelete="SET NULL")
     */
    protected ?SupplierStock $supplierStock = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Location", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected ?Location $location = null;

    /**
     * @ORM\Column(name="transaction_category", type="integer")
     */
    protected ?int $transactionCategory = null;

    public static function getSearchFields(): array
    {
        return ['id'       => 'ID', 'quantity' => 'quantity', 'date'     => 'date'];
    }

    public static function getSearchCategory(): string
    {
        return 'stock_transaction';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setTransactionCategory(?int $transaction): StockTransaction
    {
        $this->transactionCategory = $transaction;

        return $this;
    }

    public function getTransactionCategory(): ?int
    {
        return $this->transactionCategory;
    }

    public function getTransactionCategoryString()
    {
        return TransactionCategory::getCategoryString($this->transactionCategory) ?? $this->transactionCategory;
    }

    public function setDate(?\DateTimeInterface $date): StockTransaction
    {
        $this->date = $date;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): StockTransaction
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): StockTransaction
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): StockTransaction
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setQuantity(?int $quantity): StockTransaction
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setJob(?Job $job): StockTransaction
    {
        $this->job = $job;

        return $this;
    }

    public function getJob(): ?Job
    {
        return $this->job;
    }

    public function setPurchaseOrder(?PurchaseOrder $purchaseOrder): StockTransaction
    {
        $this->purchaseOrder = $purchaseOrder;

        return $this;
    }

    public function getPurchaseOrder(): ?PurchaseOrder
    {
        return $this->purchaseOrder;
    }

    public function setStock(?Stock $value): StockTransaction
    {
        $value->addStockTransaction($this);
        $this->stock = $value;

        return $this;
    }

    public function getStock(): ?Stock
    {
        return $this->stock;
    }

    public function setSupplierStock(?SupplierStock $value): StockTransaction
    {
        $this->supplierStock = $value;

        return $this;
    }

    public function getSupplierStock(): ?SupplierStock
    {
        return $this->supplierStock;
    }

    public function setLocation(?Location $value): StockTransaction
    {
        $this->location = $value;

        return $this;
    }

    public function getLocation(): ?Location
    {
        return $this->location;
    }
}
