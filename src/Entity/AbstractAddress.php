<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Gedmo\Mapping\Annotation as Gedmo;

/** @MappedSuperclass */
class AbstractAddress
{
    /**
     * @ORM\Column(name="address_1", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $address1 = null;

    /**
     * @ORM\Column(name="address_2", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $address2 = null;

    /**
     * @ORM\Column(name="city", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $city = null;

    /**
     * @ORM\Column(name="postcode", type="string", nullable=true, length=8)
     * @Gedmo\Versioned
     */
    protected ?string $postcode = null;

    public function getAddress1(): ?string
    {
        return $this->address1;
    }

    public function setAddress1(?string $value): AbstractAddress
    {
        $this->address1 = $value;

        return $this;
    }

    public function getAddress2(): ?string
    {
        return $this->address2;
    }

    public function setAddress2(?string $value): AbstractAddress
    {
        $this->address2 = $value;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $value): AbstractAddress
    {
        $this->city = $value;

        return $this;
    }

    public function getPostcode(): ?string
    {
        return $this->postcode;
    }

    public function setPostcode(?string $value): AbstractAddress
    {
        $this->postcode = $value;

        return $this;
    }

    public function getAddressString(): ?string
    {
        $parts = [];

        if ($this->address1) {
            $parts[] = $this->address1;
        }
        if ($this->address2) {
            $parts[] = $this->address2;
        }
        if ($this->city) {
            $parts[] = $this->city;
        }
        if ($this->postcode) {
            $parts[] = $this->postcode;
        }

        if (empty($parts)) {
            return null;
        }

        $addressString = implode(', ', $parts) . '.';

        return $addressString;
    }
}
