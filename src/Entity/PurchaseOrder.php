<?php

namespace App\Entity;

use App\Model\SearchableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * App\Entity\PurchaseOrder.
 *
 * @ORM\Table(name="purchase_order")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\PurchaseOrderRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
#[Assert\Callback(callback: 'isFormEmpty')]
class PurchaseOrder implements SearchableInterface
{
    final public const TYPE_DMHE = 'DMHE';
    final public const TYPE_DFS = 'DFS';
    final public const ORDER_TYPE_NEW = 'New';
    final public const ORDER_TYPE_DFS = 'Used';

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(name="code", type="string", nullable=true))
     */
    protected ?string $code = null;

    /**
     * @ORM\Column(name="created", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected ?\DateTimeInterface $created;

    /**
     * @ORM\Column(name="order_date", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $orderDate = null;

    /**
     * @ORM\Column(name="completed", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $completed = false;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Job", inversedBy="purchaseOrders")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected ?Job $job = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SalesJob", inversedBy="purchaseOrders")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected ?\App\Entity\SalesJob $salesJob = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Customer", inversedBy="purchaseOrders")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected ?Customer $customer = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Supplier", inversedBy="purchaseOrders")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected ?Supplier $supplier = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PurchaseOrderItem", mappedBy="purchaseOrder", cascade={"persist", "remove"})
     * @var Collection<PurchaseOrderItem>
     */
    protected Collection $purchaseOrderItems;

    /**
     * @ORM\Column(name="order_instructions", type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $orderInstructions = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EstimateItem", mappedBy="purchaseOrder", cascade={"persist", "remove"} )
     * @var Collection<EstimateItem>
     */
    protected Collection $estimateItems;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\StockItem", mappedBy="purchaseOrder", cascade={"persist"})
     * @var Collection<StockItem>
     */
    protected Collection $stockItems;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\StockTransaction", mappedBy="purchaseOrder", cascade={"persist"})
     * @var Collection<StockTransaction>
     */
    protected Collection $stockTransactions;

    /**
     * @ORM\Column(name="completed_at", type="datetime", nullable=true)
     */
    protected ?\DateTimeInterface $completedAt = null;

    /**
     * @ORM\Column(name="archived", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $archived = false;

    /**
     * @ORM\Column(name="reinstated", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $reinstated = false;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", cascade={"persist"}, inversedBy="leads")
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Gedmo\Versioned
     */
    protected ?User $owner = null;

    /**
     * @ORM\Column(name="engineer_collection", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $engineerCollection = false;

    /**
     * @ORM\Column(type="string", length=16)
     */
    protected ?string $type = self::TYPE_DFS;

    /**
     * @ORM\Column(name="order_type", type="string", length=16, nullable=true)
     */
    protected ?string $orderType = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Truck", inversedBy="purchaseOrder")
     * @ORM\JoinColumn(name="truck_id", referencedColumnName="id")
     */
    protected ?\App\Entity\Truck $truck = null;

    public function __construct()
    {
        $this->stockItems         = new ArrayCollection();
        $this->estimateItems      = new ArrayCollection();
        $this->stockTransactions  = new ArrayCollection();
        $this->purchaseOrderItems = new ArrayCollection();
    }

    public static function getSearchFields(): array
    {
        return ['id'          => 'ID'];
    }

    public static function getSearchCategory(): string
    {
        return 'purchase-order';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(?\DateTimeInterface $value): PurchaseOrder
    {
        $this->created = $value;

        return $this;
    }

    public function getOrderDate(): ?\DateTimeInterface
    {
        return $this->orderDate;
    }

    public function setOrderDate(?\DateTimeInterface $value): PurchaseOrder
    {
        $this->orderDate = $value;

        return $this;
    }

    public function getCompleted(): ?bool
    {
        return $this->completed;
    }

    public function setCompleted(?bool $value): PurchaseOrder
    {
        $this->completed = $value;

        return $this;
    }

    public function getJob(): ?Job
    {
        return $this->job;
    }

    public function setJob(?Job $value): PurchaseOrder
    {
        $this->job = $value;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $value): PurchaseOrder
    {
        $this->customer = $value;

        return $this;
    }

    public function getSupplier(): ?Supplier
    {
        return $this->supplier;
    }

    public function setSupplier(?Supplier $value): PurchaseOrder
    {
        $this->supplier = $value;

        return $this;
    }

    public function getOrderInstructions(): ?string
    {
        return $this->orderInstructions;
    }

    public function setOrderInstructions(?string $value): PurchaseOrder
    {
        $this->orderInstructions = $value;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $value): void
    {
        $this->deletedAt = $value;
    }

    public function addPurchaseOrderItem(PurchaseOrderItem $value): PurchaseOrder
    {
        $this->purchaseOrderItems->add($value);

        return $this;
    }

    public function removePurchaseOrderItem(PurchaseOrderItem $value): void
    {
        $this->purchaseOrderItems->removeElement($value);
    }

    public function getPurchaseOrderItems(): Collection
    {
        return $this->purchaseOrderItems;
    }

    public function addStockItem(StockItem $value): PurchaseOrder
    {
        $this->stockItems->add($value);
        $value->setPurchaseOrder($this);

        return $this;
    }

    public function removeStockItem(StockItem $value): void
    {
        $this->stockItems->removeElement($value);
    }

    public function getIsStockItems(): Collection
    {
        return $this->stockItems;
    }

    public function addEstimateItem(EstimateItem $value): PurchaseOrder
    {
        $this->estimateItems->add($value);
        $value->setPurchaseOrder($this);

        return $this;
    }

    public function removeEstimateItem(EstimateItem $value): void
    {
        $this->estimateItems->removeElement($value);
    }

    public function getEstimateItems(): Collection
    {
        return $this->estimateItems;
    }

    public function getItemCost(): string
    {
        $total = 0.0;
        foreach ($this->getPurchaseOrderItems() as $purchaseOrderItem) {
            $unitCost               = $purchaseOrderItem->getUnitCost();
            $quantity               = $purchaseOrderItem->getQuantity();
            $purchaseOrderItemTotal = $unitCost * $quantity;
            $total                  += $purchaseOrderItemTotal;
        }

        foreach ($this->getEstimateItems() as $estimateItem) {
            $unitCost = $estimateItem->getPrice();
            $quantity = $estimateItem->getQuantity();
            $estimateItemTotal = $unitCost * $quantity;
            $total += $estimateItemTotal;
        }

        return number_format($total, 2);
    }

    public function getItemIssueCost(): string
    {
        $total = 0.0;
        foreach ($this->getPurchaseOrderItems() as $purchaseOrderItem) {
            $unitIssue               = $purchaseOrderItem->getUnitIssue();
            $quantity               = $purchaseOrderItem->getQuantity();
            $purchaseOrderItemTotal = $unitIssue * $quantity;
            $total                  += $purchaseOrderItemTotal;
        }

        foreach ($this->getEstimateItems() as $estimateItem) {
            $unitIssue          = $estimateItem->getRetailPrice();
            $quantity          = $estimateItem->getQuantity();
            $estimateItemTotal = $unitIssue * $quantity;
            $total             += $estimateItemTotal;
        }

        return number_format($total, 2);
    }

    public function getItemQuantity(): int|float
    {
        $quantity = 0;
        foreach ($this->getPurchaseOrderItems() as $purchaseOrderItem) {
            $itemQuantity   = $purchaseOrderItem->getQuantity();
            $quantity       += $itemQuantity;
        }
        foreach ($this->getEstimateItems() as $estimateItem) {
            $itemQuantity   = $estimateItem->getQuantity();
            $quantity       += $itemQuantity;
        }

        return $quantity;
    }

    public function isFormEmpty(ExecutionContextInterface $context): void
    {
        if ($this->getPurchaseOrderItems()->isEmpty() && $this->getEstimateItems()->isEmpty()) {
            $context->addViolation('You cannot create an purchase order with no items, either select items or close the form.');
        }
    }

    public function hasCheckInStarted(): bool
    {
        $purchaseOrderItems = $this->getPurchaseOrderItems();
        foreach ($purchaseOrderItems as $item) {
            if ($item->getReceivedDate()) {
                return true;
            }
        }

        $estimateItems = $this->getEstimateItems();
        foreach ($estimateItems as $item) {
            if ($item->getReceivedDate()) {
                return true;
            }
        }

        return false;
    }

    public function getOrderComplete(): bool
    {
        $purchaseOrderItems = $this->getPurchaseOrderItems();
        foreach ($purchaseOrderItems as $item) {
            if (!$item->getReceivedDate()) {
                return false;
            }
        }

        $estimateItems = $this->getEstimateItems();
        foreach ($estimateItems as $item) {
            if (!$item->getReceivedDate()) {
                return false;
            }
        }

        return true;
    }

    public function addStockTransaction($value): PurchaseOrder
    {
        $this->stockTransactions->add($value);

        return $this;
    }

    public function removeStockTransaction($value): void
    {
        $this->stockTransactions->removeElement($value);
    }

    public function getStockTransactions()
    {
        return $this->stockTransactions->toArray();
    }

    public function getCompletedAt(): ?\DateTimeInterface
    {
        return $this->completedAt;
    }

    public function setCompletedAt(?\DateTimeInterface $value): void
    {
        $this->completedAt = $value;
    }

    public function getArchived(): ?bool
    {
        return $this->archived;
    }

    public function setArchived(?bool $value): PurchaseOrder
    {
        $this->archived = $value;

        return $this;
    }

    public function getReinstated(): ?bool
    {
        return $this->reinstated;
    }

    public function setReinstated(?bool $value): PurchaseOrder
    {
        $this->reinstated = $value;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $value): PurchaseOrder
    {
        $this->owner = $value;

        return $this;
    }

    public function getEngineerCollection(): ?bool
    {
        return $this->engineerCollection;
    }

    public function setEngineerCollection(?bool $value): PurchaseOrder
    {
        $this->engineerCollection = $value;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getOrderType(): ?string
    {
        return $this->orderType;
    }

    public function setOrderType(string $orderType): self
    {
        $this->orderType = $orderType;

        return $this;
    }

    /**
     * @return Collection|StockItem[]
     */
    public function getStockItems(): Collection
    {
        return $this->stockItems;
    }

    public function getSalesJob(): ?SalesJob
    {
        return $this->salesJob;
    }

    public function setSalesJob(?SalesJob $salesJob): self
    {
        $this->salesJob = $salesJob;

        return $this;
    }

    public function getDisplayName(): string
    {
        $name = '';
        $supplier = $this->getSupplier();
        if ($supplier) {
            $name = $supplier->getName();
        }

        return $this->getId() . ' - ' . $name;
    }

    public function getCode(): ?string
    {
        if ($this->type === self::TYPE_DMHE) {
            return $this->code ?? self::TYPE_DMHE . '-' . $this->id;
        }

        return $this->code ?? self::TYPE_DFS . '-' . $this->id;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getTruck(): ?Truck
    {
        return $this->truck;
    }

    public function setTruck(?Truck $truck): self
    {
        $this->truck = $truck;

        return $this;
    }

    public function isDMHETruck(): bool
    {
        return $this->getTruck() !== null;
    }

    public function isDMHEType(): bool
    {
        return $this->getTruck() !== null && $this->getType() == self::TYPE_DMHE;
    }

    public function isOtherType(): bool
    {
        return $this->getTruck() === null && $this->getType() == self::TYPE_DMHE;
    }
}
