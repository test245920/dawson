<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Evence\Bundle\SoftDeleteableExtensionBundle\Mapping\Annotation as Evence;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * App\Entity\SupplierStock.
 *
 * @ORM\Table(name="supplier_stock", uniqueConstraints={@UniqueConstraint(name="stock_supplier_idx", columns={"stock_id", "supplier_id"})})
 * @ORM\Entity(repositoryClass="App\Entity\Repository\SupplierStockRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class SupplierStock
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Gedmo\Versioned
     */
    protected ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Stock", inversedBy="supplierStock", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Evence\onSoftDelete(type="SET NULL")
     */
    protected ?Stock $stock = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\StockItem", mappedBy="supplierStock")
     * @var Collection<StockItem>
     */
    protected Collection $stockItems;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Supplier", inversedBy="stockItems", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    #[Assert\NotNull(message: 'Required')]
    protected ?Supplier $supplier = null;

    /**
     * @ORM\Column(name="quantity_on_order", type="integer")
     */
    protected ?int $quantityOnOrder = 1;

    /**
     * @ORM\Column(name="units", type="integer")
     * @Gedmo\Versioned
     */
    protected ?int $units = 1;

    /**
     * @ORM\Column(name="cost_price", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    #[Assert\NotNull(message: 'Required')]
    protected ?float $costPrice = null;

    /**
     * @ORM\Column(name="issue_price", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $issuePrice = null;

    /**
     * @ORM\Column(name="carriage_price", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $carriagePrice = null;

    /**
     * @ORM\Column(name="supplier_notes", type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $supplierNotes = null;

    /**
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    protected ?\DateTimeInterface $createdAt = null;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\Column(name="code", type="string", length=50, nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $code = null;

    /**
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $description = null;

    public function __construct()
    {
        $this->stockItems = new ArrayCollection();
    }

    public static function getSearchFields(): array
    {
        return ['id'            => 'id', 'costPrice'     => 'CostPrice', 'carriagePrice' => 'CarriagePrice', 'supplierNotes' => 'SupplierNotes', 'code'          => 'Code'];
    }

    public static function getSearchCategory(): string
    {
        return 'supplier-stock';
    }

    public function getDisplayString(): string
    {
        $code = $this->code ?: $this->stock->getCode();
        $description = $this->description ?: $this->stock->getDescription();

        return sprintf('%s - %s', $code, $description);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $value): SupplierStock
    {
        $this->id = $value;

        return $this;
    }

    public function getStock(): ?Stock
    {
        return $this->stock;
    }

    public function setStock(?Stock $value): SupplierStock
    {
        $this->stock = $value;

        return $this;
    }

    public function getStockItems(): Collection
    {
        return $this->stockItems;
    }

    /**
     * @param (Collection & \iterable<StockItem>) $value
     */
    public function setStockItems(Collection $value): SupplierStock
    {
        $this->stockItems = $value;

        return $this;
    }

    public function getSupplier(): ?Supplier
    {
        return $this->supplier;
    }

    public function setSupplier(?Supplier $value): SupplierStock
    {
        $this->supplier = $value;

        return $this;
    }

    public function getQuantityOnOrder(): ?int
    {
        return $this->quantityOnOrder;
    }

    public function setQuantityOnOrder(?int $value): SupplierStock
    {
        $this->quantityOnOrder = $value;

        return $this;
    }

    public function getUnits(): ?int
    {
        return $this->units;
    }

    public function setUnits(?int $value): SupplierStock
    {
        $this->units = $value;

        return $this;
    }

    public function getCostPrice(): ?string
    {
        return $this->costPrice;
    }

    public function setCostPrice(?string $value): SupplierStock
    {
        $this->costPrice = $value;

        return $this;
    }

    public function getIssuePrice(): ?string
    {
        return $this->issuePrice;
    }

    public function setIssuePrice(?string $value): SupplierStock
    {
        $this->issuePrice = $value;

        return $this;
    }

    public function getCarriagePrice(): ?string
    {
        return $this->carriagePrice;
    }

    public function setCarriagePrice(?string $value): SupplierStock
    {
        $this->carriagePrice = $value;

        return $this;
    }

    public function getSupplierNotes(): ?string
    {
        return $this->supplierNotes;
    }

    public function setSupplierNotes(?string $value): SupplierStock
    {
        $this->supplierNotes = $value;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $value): SupplierStock
    {
        $this->deletedAt = $value;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $value): SupplierStock
    {
        $this->code = $value;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $value): SupplierStock
    {
        $this->description = $value;

        return $this;
    }
}
