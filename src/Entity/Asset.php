<?php

namespace App\Entity;

use App\Model\Warranty;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Entity\Repository\AssetRepository")
 * @ORM\Table(name="asset")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="assetType", type="string")
 * @ORM\DiscriminatorMap({
 *      "truck"      = "Truck",
 *      "attachment" = "TruckAttachment",
 *      "charger"    = "TruckCharger",
 * })
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
abstract class Asset
{
    /*
     * Not persisted = only used for forms
     */
    public $newMake;

    public $newModel;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(name="uuid", type="text", nullable=true)
     */
    protected ?string $uuid = null;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    protected ?\DateTimeInterface $createdAt = null;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    protected ?\DateTimeInterface $updatedAt = null;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\AssetLifecyclePeriod", cascade={"persist"})
     * @ORM\JoinColumn(name="currentLifecyclePeriod_id", onDelete="SET NULL")
     */
    protected ?AssetLifecyclePeriod $currentLifecyclePeriod = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AssetLifecyclePeriod", mappedBy="asset")
     * @ORM\OrderBy({"start" = "DESC", "id" = "DESC"})
     * @var Collection<AssetLifecyclePeriod>
     */
    protected Collection $lifecyclePeriods;

    /**
     * @ORM\Column(name="serial", type="text")
     * @Gedmo\Versioned
     */
    #[Assert\NotNull(message: 'Required')]
    protected ?string $serial = null;

    /**
     * @ORM\Column(name="year_of_manufacture", type="integer", length=4, nullable=true)
     * @Gedmo\Versioned
     */
    protected ?int $yearOfManufacture = null;

    /**
     * @ORM\Column(name="purchase_price", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $purchasePrice = null;

    /**
     * @ORM\Column(name="purchase_date", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $purchasedDate = null;

    /**
     * @ORM\Column(name="manufacturers_warranty", type="integer")
     * @Gedmo\Versioned
     */
    protected ?int $manufacturersWarranty = Warranty::WARRANTY_NONE;

    /**
     * @ORM\Column(name="manufacturers_warranty_duration", type="integer")
     * @Gedmo\Versioned
     */
    protected ?int $manufacturersWarrantyDuration = 0;

    /**
     * @ORM\Column(name="second_level_manufacturers_warranty", type="integer")
     * @Gedmo\Versioned
     */
    protected ?int $secondLevelManufacturersWarranty = Warranty::WARRANTY_NONE;

    /**
     * @ORM\Column(name="second_level_manufacturers_warranty_duration", type="integer")
     * @Gedmo\Versioned
     */
    protected ?int $secondLevelManufacturersWarrantyDuration = 0;

    /**
     * @ORM\Column(name="hire_rate", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $hireRate = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Note", mappedBy="asset", cascade={"persist", "remove"})
     * @var Collection<Note>
     */
    protected Collection $notes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Image", mappedBy="asset", cascade={"persist", "remove"})
     * @var Collection<Image>
     */
    protected Collection $images;

    /**
     * @ORM\Column(name="stock_value", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $stockValue = null;

    public function __construct()
    {
        $this->uuid = str_replace("\n", '', shell_exec('uuidgen -r'));
        $this->lifecyclePeriods = new ArrayCollection();
        $this->notes = new ArrayCollection();
        $this->images = new ArrayCollection();
    }

    public static function getSearchFields(): array
    {
        return ['id'     => 'ID', 'serial' => 'Serial'];
    }

    public function isCasualHire(): bool
    {
        if (!$this->currentLifecyclePeriod) {
            return false;
        }

        return $this->currentLifecyclePeriod->isCasualHire();
    }

    public function __call($name, $args)
    {
        $period = new AssetLifecyclePeriod();

        $name = preg_replace('/^get/', '', (string) $name);

        $getter = 'get' . ucfirst($name);

        if (method_exists($period, $getter)) {
            if (!$this->currentLifecyclePeriod) {
                return null;
            }

            return call_user_func([$this->currentLifecyclePeriod, $getter]);
        }

        throw new \BadMethodCallException('Call to undefined method Asset::' . $name);
    }

    public function getAssetType($className = false)
    {
        $classParts = explode('\\', static::class);
        $type = end($classParts);

        if ($className) {
            return $type;
        }

        return match ($type) {
            'Truck' => 'Truck',
            'TruckAttachment' => 'Attachment',
            'TruckCharger' => 'Charger',
            default => $type,
        };
    }

    public function ensureCurrentLifecyclePeriod(): bool
    {
        if ($this->currentLifecyclePeriod) {
            return false;
        }

        $period = new AssetLifecyclePeriod();
        $period->setAsset($this);
        $period->setStart(new \DateTime());

        $this->currentLifecyclePeriod = $period;

        return true;
    }

    public function getNameString($serial = true): string
    {
        $name = [];

        $name[] = $this->make;
        $name[] = $this->model;
        if ($serial) {
            $name[] = '(' . $this->serial . ')';
        }

        return implode(' ', $name);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid()
    {
        return $this->uuid;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $value): Asset
    {
        $this->createdAt = $value;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $value): Asset
    {
        $this->updatedAt = $value;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $value): Asset
    {
        $this->deletedAt = $value;

        return $this;
    }

    public function getCurrentLifecyclePeriod(): ?AssetLifecyclePeriod
    {
        return $this->currentLifecyclePeriod;
    }

    public function setCurrentLifecyclePeriod(?AssetLifecyclePeriod $value): Asset
    {
        $this->currentLifecyclePeriod = $value;

        return $this;
    }

    public function getSerial(): ?string
    {
        return $this->serial;
    }

    public function setSerial(?string $value): Asset
    {
        $this->serial = $value;

        return $this;
    }

    public function getYearOfManufacture(): ?int
    {
        return $this->yearOfManufacture;
    }

    public function setYearOfManufacture(?int $value): Asset
    {
        $this->yearOfManufacture = $value;

        return $this;
    }

    public function getPurchasePrice(): ?float
    {
        return $this->purchasePrice;
    }

    public function setPurchasePrice(?string $value): Asset
    {
        $this->purchasePrice = $value;

        return $this;
    }

    public function getPurchasedDate(): ?\DateTimeInterface
    {
        return $this->purchasedDate;
    }

    public function setPurchasedDate(?\DateTimeInterface $value): Asset
    {
        $this->purchasedDate = $value;

        return $this;
    }

    public function getManufacturersWarranty(): ?int
    {
        return $this->manufacturersWarranty;
    }

    public function setManufacturersWarranty(?int $value): Asset
    {
        $this->manufacturersWarranty = $value;

        return $this;
    }

    public function getManufacturersWarrantyDuration(): ?int
    {
        return $this->manufacturersWarrantyDuration;
    }

    public function setManufacturersWarrantyDuration(?int $value): Asset
    {
        $this->manufacturersWarrantyDuration = $value;

        return $this;
    }

    public function getSecondLevelManufacturersWarranty(): ?int
    {
        return $this->secondLevelManufacturersWarranty;
    }

    public function setSecondLevelManufacturersWarranty(?int $value): Asset
    {
        $this->secondLevelManufacturersWarranty = $value;

        return $this;
    }

    public function getSecondLevelManufacturersWarrantyDuration(): ?int
    {
        return $this->secondLevelManufacturersWarrantyDuration;
    }

    public function setSecondLevelManufacturersWarrantyDuration(?int $value): Asset
    {
        $this->secondLevelManufacturersWarrantyDuration = $value;

        return $this;
    }

    public function getHireRate(): ?float
    {
        return $this->hireRate;
    }

    public function setHireRate(?string $value): Asset
    {
        $this->hireRate = $value;

        return $this;
    }

    public function getLifecyclePeriods()
    {
        return $this->lifecyclePeriods;
    }

    public function addNote($value): Asset
    {
        $this->notes->add($value);

        return $this;
    }

    public function removeNote($value): void
    {
        $this->notes->removeElement($value);
    }

    public function getNotes()
    {
        return $this->notes;
    }

    public function getAllNotes()
    {
        $notes = $this->notes->toArray();

        if ($this->currentLifecyclePeriod) {
            $notes = array_merge($notes, $this->currentLifecyclePeriod->getNotes()->toArray());

            usort($notes, fn ($a, $b): int => $a->getDate() < $b->getDate() ? -1 : 1);
        }

        return $notes;
    }

    public function addImage($value): Asset
    {
        $this->images->add($value);
        $value->setTruck($this);

        return $this;
    }

    public function removeImage($value): Asset
    {
        $this->images->removeElement($value);

        return $this;
    }

    public function getImages()
    {
        return $this->images;
    }

    public function getStockValue(): ?float
    {
        return $this->stockValue;
    }

    public function setStockValue(?string $stockValue): void
    {
        $this->stockValue = $stockValue;
    }
}
