<?php

namespace App\Entity;

use App\Model\SearchableInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Reminder.
 *
 * @ORM\Table(name="reminder")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\ReminderRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Reminder implements SearchableInterface
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(name="detail", type="string", length=255, nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $detail = null;

    /**
     * @ORM\Column(name="date", type="datetime")
     * @Gedmo\Versioned
     */
    #[Assert\NotBlank(message: 'Date cannot be blank')]
    protected ?\DateTimeInterface $date = null;

    /**
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $type = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="reminders", cascade={"persist"})
     * @Gedmo\Versioned
     */
    protected ?User $user = null;

    /**
     * @ORM\ManyToOne(targetEntity="Lead", inversedBy="reminders", cascade={"persist"})
     * @Gedmo\Versioned
     */
    protected ?Lead $lead = null;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\Column(name="notified", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $notified = false;

    /**
     * @ORM\Column(name="is_completed", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $isCompleted = false;

    /**
     * @ORM\Column(name="is_completed_at", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $isCompletedAt = null;

    public static function getSearchFields(): array
    {
        return ['id'       => 'ID', 'detail'   => 'detail', 'datetime' => 'datetime', 'type'     => 'type'];
    }

    public static function getSearchCategory(): string
    {
        return 'reminder';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setDetail(?string $detail): Reminder
    {
        $this->detail = $detail;

        return $this;
    }

    public function getDetail(): ?string
    {
        return $this->detail;
    }

    public function setDate(?\DateTimeInterface $date): Reminder
    {
        $this->date = $date;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setType(?string $value): Reminder
    {
        $this->type = $value;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setLead(?Lead $value): Reminder
    {
        $this->lead = $value;

        return $this;
    }

    public function getLead(): ?Lead
    {
        return $this->lead;
    }

    public function setDeletedAt(?\DateTimeInterface $value): Reminder
    {
        $this->deletedAt = $value;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setNotified(?bool $value): Reminder
    {
        $this->notified = $value;

        return $this;
    }

    public function getNotified(): ?bool
    {
        return $this->notified;
    }

    public function setUser(?User $value): Reminder
    {
        $this->user = $value;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setIsCompleted(?bool $value): Reminder
    {
        $this->isCompleted = $value;

        return $this;
    }

    public function getIsCompleted(): ?bool
    {
        return $this->isCompleted;
    }

    public function setIsCompletedAt(?\DateTimeInterface $value): Reminder
    {
        $this->isCompletedAt = $value;

        return $this;
    }

    public function getIsCompletedAt(): ?\DateTimeInterface
    {
        return $this->isCompletedAt;
    }
}
