<?php

namespace App\Entity;

use App\Model\SearchableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * App\Entity\Customer.
 *
 * @ORM\Table(name="customer")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\CustomerRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
#[UniqueEntity(fields: ['name'], message: 'A customer with this name already exists', repositoryMethod: 'findByIncludingSoftDeleted')]
class Customer implements SearchableInterface
{
    final public const PATH = '/customer-files/%s';

    /**
     * @ORM\Column(name="uuid", type="text", nullable=true)
     */
    protected ?string $uuid = null;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(name="name", type="string", length=50)
     * @Gedmo\Versioned
     */
    #[Assert\NotNull(message: 'Required')]
    protected ?string $name = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SiteAddress", mappedBy="customer", cascade={"persist", "remove"})
     * @var Collection<SiteAddress>
     */
    protected Collection $siteAddresses;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\BillingAddress", mappedBy="customer", cascade={"persist", "remove"})
     * @Gedmo\Versioned
     */
    protected ?BillingAddress $billingAddress = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Lead", mappedBy="customer", cascade={"persist", "remove"})
     * @var Collection<Lead>
     */
    protected Collection $leads;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Contact", mappedBy="customer", cascade={"persist", "remove"})
     * Assert\Valid
     *
     * @var Collection<Contact>
     */
    protected Collection $contacts;

    /**
     * @ORM\Column(name="telephone", type="text", nullable=true, length=30)
     * @Gedmo\Versioned
     */
    protected ?string $telephone = null;

    /**
     * @ORM\Column(name="fax", type="text", nullable=true, length=50)
     * @Gedmo\Versioned
     */
    protected ?string $fax = null;

    /**
     * @ORM\Column(name="email", type="string", nullable=true, length=50)
     * @Gedmo\Versioned
     */
    #[Assert\Email(message: 'Please enter a valid email')]
    protected ?string $email = null;

    /**
     * @ORM\Column(name="email_invoice", type="boolean", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?bool $emailInvoice = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CustomerNumber", mappedBy="customer", cascade={"persist", "remove"})
     * @ORM\OrderBy({"assignedAt" = "DESC"})
     * @var Collection<CustomerNumber>
     */
    protected Collection $customerNumbers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PurchaseOrder", mappedBy="customer", cascade={"persist"})
     * @var Collection<PurchaseOrder>
     */
    protected Collection $purchaseOrders;

    /**
     * @ORM\Column(name="payment_term", type="integer", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?int $paymentTerm = 30;

    /**
     * @ORM\Column(name="account_limit", type="integer", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?int $accountLimit = null;

    /**
     * @ORM\Column(name="labour_rate", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $labourRate = null;

    /**
     * @ORM\Column(name="parts_discount", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $partsDiscount = 0.0;

    /**
     * @ORM\Column(name="complete", type="boolean")
     */
    protected ?bool $complete = false;

    /**
     * @ORM\OneToMany(targetEntity="Job", mappedBy="customer", cascade={"persist", "remove"})
     * @var Collection<Job>
     */
    protected Collection $jobs;

    /**
     * @ORM\OneToMany(targetEntity="Estimate", mappedBy="customer", cascade={"persist", "remove"})
     * @var Collection<Estimate>
     */
    protected Collection $estimates;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\Column(name="trucks_reassignable", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $trucksReassignable = false;

    /**
     *@ORM\OneToMany(targetEntity="User", mappedBy="customer", cascade={"persist", "remove"})
     * @var Collection<User>
     */
    protected Collection $users;

    /**
     * @ORM\Column(name="requires_order_number_on_invoices", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $requiresOrderNumberOnInvoices = false;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $isActive = true;

    /**
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     * @Gedmo\Versioned
     * @Gedmo\Timestampable(on="create")
     */
    protected ?\DateTimeInterface $createdAt = null;

    /**
     * @ORM\Column(name="is_internal_customer", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $isInternalCustomer = false;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", cascade={"persist"}, inversedBy="customerAccounts")
     * @ORM\JoinColumn(name="accountManager_id")
     * @Gedmo\Versioned
     */
    protected ?User $accountManager = null;

    /**
     * @ORM\Column(name="has_cost_price_stock", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $hasCostPriceStock = false;

    /**
     * @ORM\Column(name="is_default", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $isDefault = false;

    /**
     * @ORM\Column(name="activeAt", type="datetime", nullable=true)
     * @Gedmo\Versioned
     * @Gedmo\Timestampable(on="create")
     */
    private ?\DateTimeInterface $activeAt = null;

    public function __construct()
    {
        $this->customerNumbers = new ArrayCollection();
        $this->purchaseOrders  = new ArrayCollection();
        $this->jobs            = new ArrayCollection();
        $this->estimates       = new ArrayCollection();
        $this->users           = new ArrayCollection();
        $this->leads           = new ArrayCollection();
        $this->siteAddresses   = new ArrayCollection();
        $this->contacts        = new ArrayCollection();
        $this->uuid            = str_replace("\n", '', shell_exec('uuidgen -r'));
    }

    public static function getSearchFields(): array
    {
        return ['id'        => 'ID', 'name'      => 'Name', 'email'     => 'Email', 'telephone' => 'Telephone'];
    }

    public static function getSearchCategory(): string
    {
        return 'customer';
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $value): Customer
    {
        $this->name = $value;

        return $this;
    }

    public function getBillingAddress(): ?BillingAddress
    {
        return $this->billingAddress;
    }

    public function setBillingAddress(?BillingAddress $value): Customer
    {
        $this->billingAddress = $value;

        return $this;
    }

    public function getContactName()
    {
        return $this->contactName;
    }

    public function setContactName($value): Customer
    {
        $this->contactName = $value;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $value): Customer
    {
        $this->telephone = $value;

        return $this;
    }

    public function getFax(): ?string
    {
        return $this->fax;
    }

    public function setFax(?string $value): Customer
    {
        $this->fax = $value;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $value): Customer
    {
        $this->email = $value;

        return $this;
    }

    public function getEmailInvoice(): ?bool
    {
        return $this->emailInvoice;
    }

    public function setEmailInvoice(?bool $value): Customer
    {
        $this->emailInvoice = $value;

        return $this;
    }

    public function getComplete(): ?bool
    {
        return $this->complete;
    }

    public function setComplete(?bool $value): Customer
    {
        $this->complete = $value;

        return $this;
    }

    public function getPaymentTerm(): ?int
    {
        return $this->paymentTerm;
    }

    public function setPaymentTerm(?int $value): Customer
    {
        $this->paymentTerm = $value;

        return $this;
    }

    public function getAccountLimit(): ?int
    {
        return $this->accountLimit;
    }

    public function setAccountLimit(?int $value): Customer
    {
        $this->accountLimit = $value;

        return $this;
    }

    public function getLabourRateOrDefault(): float|int
    {
        return $this->labourRate ?: WorkLog::DEFAULT_LABOUR_RATE;
    }

    public function getLabourRate(): ?string
    {
        return $this->labourRate;
    }

    public function setLabourRate(?string $value): Customer
    {
        $this->labourRate = $value;

        return $this;
    }

    public function getPartsDiscount(): ?string
    {
        return $this->partsDiscount;
    }

    public function setPartsDiscount(?string $value): Customer
    {
        $this->partsDiscount = $value;

        return $this;
    }

    public function getCustomerNumber()
    {
        return $this->customerNumbers->first();
    }

    public function setTrucksReassignable(?bool $value): Customer
    {
        $this->trucksReassignable = $value;

        return $this;
    }

    public function getTrucksReassignable(): ?bool
    {
        return $this->trucksReassignable;
    }

    public function setRequiresOrderNumberOnInvoices(?bool $value): Customer
    {
        $this->requiresOrderNumberOnInvoices = $value;

        return $this;
    }

    public function getRequiresOrderNumberOnInvoices(): ?bool
    {
        return $this->requiresOrderNumberOnInvoices;
    }

    public function addPurchaseOrder($value): void
    {
        $this->purchaseOrders->add($value);
    }

    public function removePurchaseOrder($value): void
    {
        $this->purchaseOrders->removeElement($value);
    }

    public function getPurchaseOrders(): Collection
    {
        return $this->purchaseOrders;
    }

    public function setIsActive(?bool $value): void
    {
        $this->isActive = $value;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function getCustomerNumberString()
    {
        $number = $this->customerNumbers->first();

        return $number ? $number->getCustomerNumber() : null;
    }

    public function getCustomerNumbers(): Collection
    {
        return $this->customerNumbers;
    }

    public function setCustomerNumbers($value): void
    {
        $this->customerNumbers->add($value);
    }

    public function getJobs(): Collection
    {
        return $this->jobs;
    }

    /**
     * @param (Collection & \iterable<Job>) $value
     */
    public function setJobs(Collection $value): void
    {
        $this->jobs = $value;
    }

    public function getEstimates(): Collection
    {
        return $this->estimates;
    }

    /**
     * @param (Collection & \iterable<Estimate>) $value
     */
    public function setEstimates(Collection $value): void
    {
        $this->estimates = $value;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $value): void
    {
        $this->deletedAt = $value;
    }

    public function getIsInternal()
    {
        return $this->isInternal;
    }

    public function setIsInternal($value): void
    {
        $this->isInternal = $value;
    }

    public function addUsers($value): Customer
    {
        $this->users->add($value);

        return $this;
    }

    public function removeUsers($value): void
    {
        $this->users->removeElement($value);
    }

    public function getUsers()
    {
        return $this->users->toArray();
    }

    public function addSiteAddress($value): Customer
    {
        $this->siteAddresses->add($value);
        $value->setCustomer($this);

        return $this;
    }

    public function removeSiteAddress($value): void
    {
        $this->siteAddresses->removeElement($value);
    }

    public function getSiteAddresses(): Collection
    {
        return $this->siteAddresses;
    }

    public function addContact($value): Customer
    {
        $this->contacts->add($value);
        $value->setCustomer($this);

        return $this;
    }

    public function removeContact($value): void
    {
        $this->contacts->removeElement($value);
    }

    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function addLead($value): Customer
    {
        $this->leads->add($value);
        $value->setCustomer($this);

        return $this;
    }

    public function removeLead($value): void
    {
        $this->leads->removeElement($value);
    }

    public function getLeads(): Collection
    {
        return $this->leads;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $value): void
    {
        $this->createdAt = $value;
    }

    public function getActiveAt(): ?\DateTimeInterface
    {
        return $this->activeAt;
    }

    public function setActiveAt(?\DateTimeInterface $value): void
    {
        $this->activeAt = $value;
    }

    public function getAccountManager(): ?User
    {
        return $this->accountManager;
    }

    public function setAccountManager(?User $value): Customer
    {
        $this->accountManager = $value;

        return $this;
    }

    public function getHasCostPriceStock(): ?bool
    {
        return $this->hasCostPriceStock;
    }

    public function setHasCostPriceStock(?bool $value): Customer
    {
        $this->hasCostPriceStock = $value;

        return $this;
    }

    public function getIsDefault(): ?bool
    {
        return $this->isDefault;
    }

    public function setIsDefault(?bool $value): Customer
    {
        $this->isDefault = $value;

        return $this;
    }
}
