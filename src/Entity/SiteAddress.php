<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * App\Entity\SiteAddress.
 *
 * @ORM\Table(name="site_address")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\SiteAddressRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class SiteAddress extends AbstractAddress
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(name="name", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $name = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Customer", inversedBy="siteAddresses", cascade={"persist"})
     * @ORM\JoinColumn(name="customer")
     */
    protected ?Customer $customer = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AssetLifecyclePeriod", mappedBy="siteAddress", cascade={"persist"})
     * @var Collection<AssetLifecyclePeriod>
     */
    protected Collection $assetLifecyclePeriods;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Job", mappedBy="siteAddress", cascade={"persist"})
     * @var Collection<Job>
     */
    protected Collection $job;

    /**
     * @ORM\OneToOne(targetEntity="Location", mappedBy="siteAddress", cascade={"persist"})
     */
    protected ?Location $location = null;

    /**
     * @ORM\Column(name="is_stock_location", type="boolean", nullable=true)
     */
    protected ?bool $isStockLocation = false;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    private ?\DateTimeInterface $deletedAt = null;

    public function __construct()
    {
        $this->assetLifecyclePeriods = new ArrayCollection();
        $this->job                   = new ArrayCollection();
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $value): SiteAddress
    {
        $this->deletedAt = $value;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $value): SiteAddress
    {
        $this->customer = $value;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $value): SiteAddress
    {
        $this->name = $value;

        return $this;
    }

    public function addAssetLifecyclePeriod($value): SiteAddress
    {
        $this->assetLifecyclePeriods->add($value);

        return $this;
    }

    public function removeAssetLifecyclePeriod($value): SiteAddress
    {
        $this->assetLifecyclePeriods->removeElement($value);

        return $this;
    }

    public function getAssetLifecyclePeriods(): Collection
    {
        return $this->assetLifecyclePeriods;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setLocation(?Location $value): SiteAddress
    {
        $this->location = $value;

        return $this;
    }

    public function getLocation(): ?Location
    {
        return $this->location;
    }

    public function getIsStockLocation(): ?bool
    {
        return $this->isStockLocation;
    }

    public function setIsStockLocation(?bool $value): SiteAddress
    {
        $this->isStockLocation = $value;

        return $this;
    }

    public function getAddressString(): ?string
    {
        $parts = [];

        if ($this->name) {
            $parts[] = $this->name;
        }
        if ($this->address1) {
            $parts[] = $this->address1;
        }
        if ($this->address2) {
            $parts[] = $this->address2;
        }
        if ($this->city) {
            $parts[] = $this->city;
        }
        if ($this->postcode) {
            $parts[] = $this->postcode;
        }

        if (empty($parts)) {
            return null;
        }

        $addressString = implode(', ', $parts) . '.';

        return $addressString;
    }
}
