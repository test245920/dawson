<?php

namespace App\Entity;

use App\Model\SearchableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="source")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\SourceRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 */
#[UniqueEntity(fields: ['name'], message: 'A source with this name already exists', repositoryMethod: 'findByIncludingSoftDeleted')]
class Source implements SearchableInterface
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     * @Gedmo\Versioned
     */
    #[Assert\NotNull(message: 'Required')]
    protected ?string $name = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Lead", mappedBy="source", cascade={"persist"})
     * @var Collection<Lead>
     */
    protected Collection $leads;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    protected ?\DateTimeInterface $deletedAt = null;

    public function __construct()
    {
        $this->leads = new ArrayCollection();
    }

    public static function getSearchFields(): array
    {
        return ['name' => 'Name'];
    }

    public static function getSearchCategory(): string
    {
        return 'source';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $value): void
    {
        $this->deletedAt = $value;
    }

    public function setName(?string $name): Source
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function addLead($value): Source
    {
        $this->leads->add($value);
        $value->setSource($this);

        return $this;
    }

    public function removeLead($value): void
    {
        $this->leads->removeElement($value);
    }

    public function getLeads(): Collection
    {
        return $this->leads;
    }
}
