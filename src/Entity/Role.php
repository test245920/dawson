<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\Role as BaseRole;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * App\Entity\Role.
 *
 * @ORM\Table(name="role")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\RoleRepository")
 */
class Role extends BaseRole implements RoleHierarchyInterface, \Stringable
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="roles")
     * @var Collection<User>
     */
    protected Collection $users;

    public function __construct(/**
     * @ORM\Column(name="role", type="string", length=20, unique=true)
     */
        #[Assert\NotNull(message: 'Required')]
        protected ?string $role = null, /**
     * @ORM\Column(name="name", type="string", length=50, unique=true)
     */
        #[Assert\NotNull(message: 'Required')]
        protected ?string $name = null
    ) {
        $this->users = new ArrayCollection();
    }

    public function __toString(): string
    {
        return (string) $this->role;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setName(?string $value): Role
    {
        $this->name = $value;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setRole(?string $value): Role
    {
        $this->role = $value;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function removeUser($value): Role
    {
        $this->users->removeElement($value);

        return $this;
    }

    public function addUser($value): Role
    {
        if (!$this->users->contains($value)) {
            $this->users->add($value);
        }

        return $this;
    }

    /**
     * @param array $roles *
     */
    public function getReachableRoleNames(array $roles): array
    {
        return [];
    }
}
