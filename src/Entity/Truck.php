<?php

namespace App\Entity;

use App\Model\AssetInterface;
use App\Model\SearchableInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * App\Entity\Truck.
 *
 * @ORM\Table(name="truck")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\TruckRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 */
#[Assert\Callback(callback: 'makeModelSerialRequired')]
class Truck extends Asset implements SearchableInterface, AssetInterface
{
    final public const PATH = '/truck-images/%s';

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TruckModel", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected ?TruckModel $model = null;

    /**
     * @ORM\Column(name="hour_reading", type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $hourReading = null;

    /**
     * @ORM\Column(name="tyre_type", type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $tyreType = null;

    /**
     * @ORM\Column(name="mast", type="text", length=4, nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $mast = null;

    /**
     * @ORM\Column(name="mastHeight", type="integer", length=4, nullable=true)
     * @Gedmo\Versioned
     */
    protected ?int $mastHeight = null;

    /**
     * @ORM\Column(name="mast_code", type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $mastCode = null;

    /**
     * @ORM\Column(name="hydraulics", type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $hydraulics = null;

    /**
     * @ORM\Column(name="sideshift", type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $sideshift = null;

    /**
     * @ORM\Column(name="fork_dimensions", type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $forkDimensions = null;

    /**
     * @ORM\Column(name="cabin", type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $cabin = null;

    /**
     * @ORM\Column(name="battery_spec", type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $batterySpec = null;

    /**
     * @ORM\Column(name="truck_type", type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $truckType = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TruckAttachment", inversedBy="truck", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected ?TruckAttachment $attachment = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TruckCharger", inversedBy="truck", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected ?TruckCharger $charger = null;

    /*
     * Not persisted = only used for forms
     */
    protected ?string $attachmentType = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\SalesJob", mappedBy="truck", cascade={"persist"})
     */
    protected ?\App\Entity\SalesJob $salesJob = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\PurchaseOrder", mappedBy="truck", cascade={"persist"})
     */
    protected ?\App\Entity\PurchaseOrder $purchaseOrder = null;

    public static function getSearchCategory(): string
    {
        return 'truck';
    }

    public function getNameString($serial = true): string
    {
        $nameString = '';

        $nameString .= $this->model && $this->model->getMake() ? $this->model->getMake()->getName() . ' ' : '';
        $nameString .= $this->model ? $this->model->getName() : '';
        if ($serial) {
            $nameString .= ' (' . $this->serial . ')';
        }

        return $nameString;
    }

    public function getViewRoute(): string
    {
        return 'view_asset';
    }

    public function getViewParams(): array
    {
        return ['id' => $this->id];
    }

    public function getNewRoute(): string
    {
        return 'new_truck';
    }

    public function getEditRoute(): string
    {
        return 'edit_truck';
    }

    public function getEditParams(): array
    {
        return ['id' => $this->id];
    }

    public function makeModelSerialRequired(ExecutionContextInterface $context): void
    {
        $modelExist = $this->getModel() || $this->newModel;
        $noSerial = !$this->getSerial();
        $formInValid = !$modelExist || $noSerial;

        if ($formInValid) {
            $context->addViolation('A truck must have a "Make", "Model" and "Serial" to be created.');
        }
    }

    public function getAttachmentType(): ?string
    {
        return $this->attachment ? $this->attachment->getType() : null;
    }

    public function setAttachmentType(?string $value): void
    {
        $this->attachmentType = $value;
    }

    public function getModel(): ?TruckModel
    {
        return $this->model;
    }

    public function setModel(?TruckModel $value): Truck
    {
        $this->model = $value;

        return $this;
    }

    public function getHourReading(): ?string
    {
        return $this->hourReading;
    }

    public function setHourReading(?string $value): Truck
    {
        $this->hourReading = $value;

        return $this;
    }

    public function getNewMake()
    {
        return $this->newMake;
    }

    public function setNewMake($value): Truck
    {
        $this->newMake = $value;

        return $this;
    }

    public function getNewModel()
    {
        return $this->newModel;
    }

    public function setNewModel($value): Truck
    {
        $this->newModel = $value;

        return $this;
    }

    public function getMast(): ?string
    {
        return $this->mast;
    }

    public function setMast(?string $value): Truck
    {
        $this->mast = $value;

        return $this;
    }

    public function getMastHeight(): ?int
    {
        return $this->mastHeight;
    }

    public function setMastHeight(?int $value): Truck
    {
        $this->mastHeight = $value;

        return $this;
    }

    public function getMastCode(): ?string
    {
        return $this->mastCode;
    }

    public function setMastCode(?string $value): Truck
    {
        $this->mastCode = $value;

        return $this;
    }

    public function getHydraulics(): ?string
    {
        return $this->hydraulics;
    }

    public function setHydraulics(?string $value): Truck
    {
        $this->hydraulics = $value;

        return $this;
    }

    public function getSideshift(): ?string
    {
        return $this->sideshift;
    }

    public function setSideshift(?string $value): Truck
    {
        $this->sideshift = $value;

        return $this;
    }

    public function getForkDimensions(): ?string
    {
        return $this->forkDimensions;
    }

    public function setForkDimensions(?string $value): Truck
    {
        $this->forkDimensions = $value;

        return $this;
    }

    public function getCabin(): ?string
    {
        return $this->cabin;
    }

    public function setCabin(?string $value): Truck
    {
        $this->cabin = $value;

        return $this;
    }

    public function getTruckType(): ?string
    {
        return $this->truckType;
    }

    public function setTruckType(?string $value): Truck
    {
        $this->truckType = $value;

        return $this;
    }

    public function getBatterySpec(): ?string
    {
        return $this->batterySpec;
    }

    public function setBatterySpec(?string $value): Truck
    {
        $this->batterySpec = $value;

        return $this;
    }

    public function getTyreType(): ?string
    {
        return $this->tyreType;
    }

    public function setTyreType(?string $value): void
    {
        $this->tyreType = $value;
    }

    public function getAttachment(): ?TruckAttachment
    {
        return $this->attachment;
    }

    public function setAttachment(?TruckAttachment $value): Truck
    {
        $this->attachment = $value;

        return $this;
    }

    public function getCharger(): ?TruckCharger
    {
        return $this->charger;
    }

    public function setCharger(?TruckCharger $value): Truck
    {
        $this->charger = $value;

        return $this;
    }

    public function getType(): ?string
    {
        if ($this->getModel()) {
            return $this->getModel()->getFuel();
        }

        return null;
    }

    public function getSalesJob(): ?SalesJob
    {
        return $this->salesJob;
    }

    public function setSalesJob(?SalesJob $salesJob): self
    {
        // unset the owning side of the relation if necessary
        if ($salesJob === null && $this->salesJob !== null) {
            $this->salesJob->setTruck(null);
        }

        // set the owning side of the relation if necessary
        if ($salesJob !== null && $salesJob->getTruck() !== $this) {
            $salesJob->setTruck($this);
        }

        $this->salesJob = $salesJob;

        return $this;
    }
}
