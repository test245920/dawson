<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * App\Entity\StockType.
 *
 * @ORM\Table(name="stock_type")
 * @ORM\Entity()
 */
class StockType
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(name="type", type="string", length=50)
     */
    #[Assert\NotNull(message: 'Required')]
    protected ?string $type = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $value): StockType
    {
        $this->type = $value;

        return $this;
    }
}
