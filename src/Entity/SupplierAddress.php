<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * App\Entity\SupplierAddress.
 *
 * @ORM\Table(name="supplier_address")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\SupplierAddressRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class SupplierAddress extends AbstractAddress
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(name="address_1", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $address1 = null;

    /**
     * @ORM\Column(name="address_2", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $address2 = null;

    /**
     * @ORM\Column(name="city", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $city = null;

    /**
     * @ORM\Column(name="county", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $county = null;

    /**
     * @ORM\Column(name="country", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $country = null;

    /**
     * @ORM\Column(name="postcode", type="string", nullable=true, length=8)
     * @Gedmo\Versioned
     */
    protected ?string $postcode = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Supplier", inversedBy="supplierAddress", cascade={"persist"})
     */
    protected ?Supplier $supplier = null;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    private ?\DateTimeInterface $deletedAt = null;

    public function setSupplier(?Supplier $value): SupplierAddress
    {
        $this->supplier = $value;

        return $this;
    }

    public function getSupplier(): ?Supplier
    {
        return $this->supplier;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $value): SupplierAddress
    {
        $this->deletedAt = $value;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $value): SupplierAddress
    {
        $this->country = $value;

        return $this;
    }

    public function getCounty(): ?string
    {
        return $this->county;
    }

    public function setCounty(?string $value): SupplierAddress
    {
        $this->county = $value;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAddressString(): string
    {
        $parts   = [];

        if ($this->address1) {
            $parts[] = $this->address1;
        }
        if ($this->address2) {
            $parts[] = $this->address2;
        }
        if ($this->city) {
            $parts[] = $this->city;
        }
        if ($this->postcode) {
            $parts[] = $this->postcode;
        }
        if ($this->county) {
            $parts[] = $this->county;
        }
        if ($this->country) {
            $parts[] = $this->country;
        }

        $this->addressString = implode(', ', $parts);

        return $this->addressString;
    }
}
