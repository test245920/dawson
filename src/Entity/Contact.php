<?php

namespace App\Entity;

use App\Model\SearchableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * App\Entity\Contact.
 *
 * @ORM\Table(name="contact")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\ContactRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Contact implements SearchableInterface
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     * @Gedmo\Versioned
     */
    #[Assert\NotBlank(message: 'Required')]
    protected ?string $name = null;

    /**
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $email = null;

    /**
     * @ORM\Column(name="telephone", type="string", length=255, nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $telephone = null;

    /**
     * @ORM\Column(name="mobile", type="string", length=255, nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $mobile = null;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\AssetLifecyclePeriod", inversedBy="contacts", cascade={"persist"})
     * @ORM\JoinTable(name="contact_assetlifecycleperiod",
     *      inverseJoinColumns={@ORM\JoinColumn(name="assetlifecycleperiod_id", onDelete="CASCADE")}
     * )
     * @var Collection<AssetLifecyclePeriod>
     */
    protected Collection $assetLifecyclePeriods;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Customer", inversedBy="contacts", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Gedmo\Versioned
     */
    protected ?Customer $customer = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Supplier", inversedBy="contacts", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Gedmo\Versioned
     */
    protected ?Supplier $supplier = null;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\ManyToMany(targetEntity="Lead", mappedBy="contacts", cascade={"persist"})
     * @var Collection<Lead>
     */
    protected Collection $leads;

    /**
     * @ORM\ManyToMany(targetEntity="Estimate", mappedBy="contacts", cascade={"persist"})
     * @var Collection<Estimate>
     */
    protected Collection $estimates;

    /**
     * @ORM\ManyToMany(targetEntity="Job", mappedBy="contacts", cascade={"persist"})
     * @var Collection<Job>
     */
    protected Collection $jobs;

    /**
     * @ORM\Column(name="job_title", type="string", length=255, nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $jobTitle = null;

    /**
     * @ORM\OneToOne(targetEntity="User", mappedBy="contact", cascade={"persist"})
     * @Gedmo\Versioned
     */
    protected ?User $user = null;

    public function __construct()
    {
        $this->leads                 = new ArrayCollection();
        $this->assetLifecyclePeriods = new ArrayCollection();
        $this->estimates             = new ArrayCollection();
        $this->jobs                  = new ArrayCollection();
    }

    public static function getSearchFields(): array
    {
        return ['id'        => 'ID', 'name'      => 'Name', 'email'     => 'Email', 'mobile'    => 'Mobile', 'telephone' => 'Telephone'];
    }

    public static function getSearchCategory(): string
    {
        return 'contact';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setName(?string $name): Contact
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setEmail(?string $email): Contact
    {
        $this->email = $email;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $value): Contact
    {
        $this->deletedAt = $value;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $value): Contact
    {
        $this->telephone = $value;

        return $this;
    }

    public function getMobile(): ?string
    {
        return $this->mobile;
    }

    public function setMobile(?string $value): Contact
    {
        $this->mobile = $value;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $value): Contact
    {
        $this->customer = $value;

        return $this;
    }

    public function getSupplier(): ?Supplier
    {
        return $this->supplier;
    }

    public function setSupplier(?Supplier $value): Contact
    {
        $this->supplier = $value;

        return $this;
    }

    public function getContactString(): string
    {
        $parts = [];

        if ($this->name) {
            $parts[] = $this->name;
        }
        if ($this->jobTitle) {
            $parts[] = $this->jobTitle;
        }
        if ($this->telephone) {
            $parts[] = $this->telephone;
        }
        if ($this->email) {
            $parts[] = $this->email;
        }

        $this->contactString = implode(', ', $parts);

        return $this->contactString;
    }

    public function getAssetLifecyclePeriods(): Collection
    {
        return $this->assetLifecyclePeriods;
    }

    public function addAssetLifecyclePeriod($value): Contact
    {
        $this->assetLifecyclePeriods->add($value);
        $value->addContact($this);

        return $this;
    }

    public function removeAssetLifecyclePeriod($value): void
    {
        $this->assetLifecyclePeriods->removeElement($value);
    }

    public function getLeads(): Collection
    {
        return $this->leads;
    }

    public function addLead($value): Contact
    {
        $this->leads->add($value);

        return $this;
    }

    public function removeLead($value): void
    {
        $this->leads->removeElement($value);
    }

    public function getEstimates(): Collection
    {
        return $this->estimates;
    }

    public function addEstimate($value): Contact
    {
        $this->estimates->add($value);

        return $this;
    }

    public function removeEstimate($value): void
    {
        $this->estimates->removeElement($value);
    }

    public function getJobs(): Collection
    {
        return $this->jobs;
    }

    public function addJob($value): Contact
    {
        $this->jobs->add($value);

        return $this;
    }

    public function removeJob($value): void
    {
        $this->jobs->removeElement($value);
    }

    public function getJobTitle(): ?string
    {
        return $this->jobTitle;
    }

    public function setJobTitle(?string $value): Contact
    {
        $this->jobTitle = $value;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $value): Contact
    {
        $this->user = $value;

        return $this;
    }
}
