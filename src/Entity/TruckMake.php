<?php

namespace App\Entity;

use App\Model\SearchableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * App\Entity\Truck.
 *
 * @ORM\Table(name="truck_make")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\TruckMakeRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
#[UniqueEntity(fields: ['name'], message: 'A truck make with this name already exists', repositoryMethod: 'findByIncludingSoftDeleted')]
class TruckMake implements SearchableInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * @Gedmo\Versioned
     */
    protected ?string $name = null;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TruckModel", mappedBy="make")
     * @var Collection<TruckModel>
     */
    protected Collection $models;

    public function __construct()
    {
        $this->models = new ArrayCollection();
    }

    public static function getSearchFields(): array
    {
        return ['id'   => 'ID', 'name' => 'Name'];
    }

    public static function getSearchCategory(): string
    {
        return 'truck-make';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setName(?string $name): TruckMake
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $value): TruckMake
    {
        $this->deletedAt = $value;

        return $this;
    }
}
