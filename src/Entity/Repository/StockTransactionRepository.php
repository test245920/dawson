<?php

namespace App\Entity\Repository;

use App\Entity\StockTransaction;
use App\Model\TransactionCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * StockTransactionRepository.
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class StockTransactionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StockTransaction::class);
    }

    public function getStockReturnedFromJob($job)
    {
        $qb = $this->createQueryBuilder('t');

        $qb->select('supplierStock.id as supplierStockId, COALESCE(siteAddress.name, CONCAT(engineer.name, :string)) as stockLocation, count(supplierStock.id) as quantity, COALESCE(supplierStock.code, stock.code) code, COALESCE(supplierStock.description, stock.description) as description, supplier.name supplierName, supplierStock.costPrice as costPrice')
            ->leftJoin('t.stock', 'stock')
            ->leftJoin('t.supplierStock', 'supplierStock')
            ->leftJoin('supplierStock.supplier', 'supplier')
            ->leftJoin('t.location', 'location')
            ->leftJoin('location.siteAddress', 'siteAddress')
            ->leftJoin('location.engineer', 'engineer')
            ->andWhere('t.job = :job')
            ->andWhere('t.transactionCategory = :category')
            ->setParameter('job', $job)
            ->setParameter('category', TransactionCategory::IN_JOB)
            ->setParameter('string', "'s Van")
            ->groupBy('supplierStockId', 'stockLocation');

        return $qb->getQuery()->getResult();
    }
}
