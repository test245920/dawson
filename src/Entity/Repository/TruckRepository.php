<?php

namespace App\Entity\Repository;

use App\Entity\Truck;
use App\Model\SearchRepositoryInterface;
use Doctrine\Persistence\ManagerRegistry;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;

class TruckRepository extends AssetRepository implements SearchRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Truck::class);
    }

    public function getTrucksPaged($currentPage, int $maxPerPage = 25): Pagerfanta
    {
        $qb = $this->createQueryBuilder('t');

        $adapter = new QueryAdapter($qb);
        $pager   = new Pagerfanta($adapter);
        $pager->setMaxPerPage($maxPerPage)->setCurrentPage((int) $currentPage);

        return $pager;
    }

    public function getItemsForSearch($user, $isFullAdmin)
    {
        $qb = $this->createQueryBuilder('t')
            ->leftJoin('t.model', 'mod')
            ->leftJoin('mod.make', 'mak');

        $fields = Truck::getSearchFields();
        $select = [];

        foreach ($fields as $field => $displayName) {
            $select[] = 't.' . $field;
        }

        $select[] = 'mak.name as make';
        $select[] = 'mod.name as model';

        $qb->select(implode(', ', $select));
        $qb->addSelect($qb->expr()->literal(Truck::getSearchCategory()) . ' as category');
        $qb->addSelect('t.id as linkIdentifier');

        return $qb->getQuery()->getArrayResult();
    }

    public function getActiveListings()
    {
        $qb = $this->createQueryBuilder('t');

        $qb->select('t.id, t.serial as serial, tma.name as make, tm.name as model, lp.clientFleet as flt, lp.type as periodType, cus.name as customer, parentCus.name as parentCustomer')
           ->leftJoin('t.model', 'tm')
           ->leftJoin('tm.make', 'tma')
           ->leftJoin('t.currentLifecyclePeriod', 'lp')
           ->leftJoin('lp.parentPeriod', 'parent')
           ->leftJoin('lp.customer', 'cus')
           ->leftJoin('parent.customer', 'parentCus')
           ->andWhere('lp.customer is not null')
           ->addOrderBy('cus.name', 'ASC')
           ->addOrderBy('lp.clientFleet', 'ASC')
           ->addOrderBy('tma.name', 'ASC')
           ->addOrderBy('tm.name', 'ASC')
           ->addOrderBy('t.serial', 'ASC');

        $trucks = $qb->getQuery()->getArrayResult();

        foreach ($trucks as $index => $truck) {
            if ($truck['flt'] == null) {
                $trucks[$index]['flt'] = 'None';
            }
            if ($truck['parentCustomer'] == null) {
                $trucks[$index]['parentCustomer'] = 'None';
            }
        }

        return $trucks;
    }

    public function getTrucksForService()
    {
        $qb = $this->createQueryBuilder('t');

        $qb->leftJoin('t.model', 'tm')
            ->leftJoin('t.currentLifecyclePeriod', 'p')
            ->leftJoin('lp.jobs', 'j', 'WITH', 'j.isServiceJob = true AND j.completed = false')
            ->groupBy('t.id')
            ->having('COUNT(j) < 1');

        return $qb->getQuery()->getResult();
    }

    public function getTrucksForLoler()
    {
        $qb = $this->createQueryBuilder('t');

        $qb->leftJoin('t.model', 'tm')
            ->leftJoin('t.currentLifecyclePeriod', 'p')
            ->leftJoin('lp.jobs', 'j', 'WITH', 'j.isLolerJob = true AND j.completed = false')
            ->groupBy('t.id')
            ->having('COUNT(j) < 1');

        return $qb->getQuery()->getResult();
    }

    public function getMakesForCustomerQb($id, $siteAddress, $assetLifecyclePeriod = null): \Doctrine\ORM\QueryBuilder
    {
        $qb = $this->createQueryBuilder('t')
            ->select('t.id, tma.name as make, tma.id as makeId, tm.id as modelId, tm.name as model, t.serial')
            ->leftJoin('t.model', 'tm')
            ->leftJoin('tm.make', 'tma')

            ->setParameter('id', $id)
            ->addOrderBy('make', 'ASC');

        if ($siteAddress) {
            $qb->leftJoin('p.siteAddress', 's')
                ->andWhere('s.id = :site')
                ->setParameter('site', $siteAddress);
        }

        if ($assetLifecyclePeriod) {
            $qb->leftJoin('t.lifecyclePeriods', 'p')
                ->andWhere('p = :period')
                ->setParameter('period', $assetLifecyclePeriod);
        } else {
            $qb->leftJoin('t.currentLifecyclePeriod', 'p')
                ->andWhere('p IS NOT NULL AND p.customer = :id');
        }

        return $qb;
    }

    public function getLinkedFuelType($id)
    {
        $qb = $this->createQueryBuilder('t')
            ->select('m.fuel')
            ->leftJoin('t.model', 'm')
            ->where('t.id = :id')
            ->setParameter('id', $id);
        if ($result = $qb->getQuery()->getOneOrNullResult()) {
            return $result['fuel'];
        }

        return null;
    }

    public function getTrucksNotLeasedOrSold()
    {
        $qb = $this->createQueryBuilder('t')
            ->select('t.id, t.serial as serial, tma.name as make, tm.name as model')
            ->leftJoin('t.model', 'tm')
            ->leftJoin('tm.make', 'tma')
            ->leftJoin('t.currentLifecyclePeriod', 'lp')
            ->andWhere('lp IS NULL OR lp.customer IS NULL');

        return $qb->getQuery()->getArrayResult();
    }

    public function getTrucksServicesDue(?int $status = null): array
    {
        $trucks = $this->createQueryBuilder('t')
            ->select('t.id, t.serial as serial, tma.name as make, tm.name as model, COALESCE(lp.serviceInterval, 0) serviceInterval, MAX(j.completedAt) as completedAt, j.isServiceJob, c.name as customer, lp.start')
            ->addSelect("CASE
                            WHEN lp.type = 1 THEN 'Contract Hire'
                            WHEN lp.type = 2 THEN 'Casual Hire'
                            WHEN lp.type = 3 THEN 'Purchase'
                            WHEN lp.type = 4 THEN 'Customer Owned'
                            ELSE 'None'
                            END AS type")
            ->leftJoin('t.model', 'tm')
            ->leftJoin('tm.make', 'tma')
            ->leftJoin('t.currentLifecyclePeriod', 'lp')
            ->leftJoin('lp.jobs', 'j', 'WITH', 'j.isServiceJob = true AND j.isServiceJob IS NOT NULL')
            ->leftJoin('lp.customer', 'c')
            ->andWhere('lp.serviceInterval >= 0 OR lp.serviceInterval IS NULL')
            ->andWhere('lp.type = :status')
            ->setParameter('status', $status)
            ->groupBy('t.id')
            ->orderBy('c.name')
            ->getQuery()->getArrayResult();

        return array_filter($trucks, static function (array $truck) {
            $date = $truck['completedAt'] ? new \DateTime($truck['completedAt']) : $truck['start'];

            $diff = $date ? $date->diff(new \DateTime()) : null;
            if ($diff) {
                return $truck['serviceInterval'] === 0 ? $truck : ($truck['serviceInterval'] * 30) < $diff->format('%a');
            }
        });
    }

    public function findItemsForAutocomplete($searchTerm, $customer)
    {
        $qb = $this->createQueryBuilder('t');

        $qb->select('t, tm, tma, lp')
            ->innerJoin('t.currentLifecyclePeriod', 'lp')
            ->leftJoin('t.model', 'tm')
            ->leftJoin('tm.make', 'tma')
            ->andWhere('tma.name LIKE :searchTerm OR tm.name LIKE :searchTerm OR t.serial LIKE :searchTerm OR lp.clientFleet LIKE :searchTerm')
            ->setParameter('searchTerm', '%' . $searchTerm . '%');

        if ($customer) {
            $qb->andWhere('lp.customer = :customer')
                ->setParameter('customer', $customer);
        } else {
            $qb->andWhere('lp.customer IS NULL');
        }

        return $qb->getQuery()->getResult();
    }

    private function truckQuery(string $status)
    {
        return $this->createQueryBuilder('t')
            ->select('t.id, t.serial as serial, tma.name as make, tm.name as model, COALESCE(lp.serviceInterval, 0) serviceInterval, MAX(j.completedAt) as completedAt, j.isServiceJob, c.name as customer, lp.start')
            ->addSelect("CASE
                            WHEN lp.type = 1 THEN 'Contract Hire'
                            WHEN lp.type = 2 THEN 'Casual Hire'
                            WHEN lp.type = 3 THEN 'Purchase'
                            WHEN lp.type = 4 THEN 'Customer Owned'
                            ELSE 'None'
                            END AS type")
            ->leftJoin('t.model', 'tm')
            ->leftJoin('tm.make', 'tma')
            ->leftJoin('t.currentLifecyclePeriod', 'lp')
            ->leftJoin('lp.jobs', 'j', 'WITH', 'j.isServiceJob = true AND j.isServiceJob IS NOT NULL')
            ->leftJoin('lp.customer', 'c')
            ->andWhere('lp.serviceInterval >= 0 OR lp.serviceInterval IS NULL')
            ->andWhere('lp.type = :status')
            ->setParameter('status', $status)
            ->groupBy('t.id')
            ->orderBy('c.name')
            ->getQuery()->getArrayResult();
    }
}
