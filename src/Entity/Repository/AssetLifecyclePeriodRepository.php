<?php

namespace App\Entity\Repository;

use App\Entity\AssetLifecyclePeriod;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AssetLifecyclePeriodRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AssetLifecyclePeriod::class);
    }

    public function getExpiredPeriods()
    {
        $qb = $this->createQueryBuilder('a')
            ->Where('a.end > :now')
            ->setParameter('now', new \DateTime('now'))
            ->andWhere('a.scheduledLifecyclePeriodCleanup = 1');

        return $qb->getQuery()->getResult();
    }

    public function getAssetStatus()
    {
        $qb = $this->createQueryBuilder('a')
            ->addSelect("CASE
                            WHEN a.type = 1 THEN 'Contract Hire'
                            WHEN a.type = 2 THEN 'Casual Hire'
                            WHEN a.type = 3 THEN 'Purchase'
                            WHEN a.type = 4 THEN 'Customer Owned'
                            ELSE 'None'
                            END AS type")
            ->groupBy('a.type');

        return $qb->getQuery()->getArrayResult();
    }
}
