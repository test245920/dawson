<?php

namespace App\Entity\Repository;

use App\Entity\TruckAttachment;
use App\Model\SearchRepositoryInterface;
use Doctrine\Persistence\ManagerRegistry;

class TruckAttachmentRepository extends AssetRepository implements SearchRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TruckAttachment::class);
    }

    public function getItemsForSearch($user, $isFullAdmin)
    {
        $qb = $this->createQueryBuilder('t');

        $fields = TruckAttachment::getSearchFields();
        $select = [];

        foreach ($fields as $field => $displayName) {
            $select[] = 't.' . $field;
        }

        $qb->select(implode(', ', $select));
        $qb->addSelect($qb->expr()->literal(TruckAttachment::getSearchCategory()) . ' as category');
        $qb->addSelect('t.id as linkIdentifier');

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * @return \non-empty-array<(\int | \string), \mixed>[]
     */
    public function getMakesAndModels(): array
    {
        $qb = $this->createQueryBuilder('t');

        $qb->select('t.model, t.make')
            ->addOrderBy('t.make')
            ->addOrderBy('t.model')
            ->groupBy('t.model, t.make');

        $result = $qb->getQuery()->getResult();
        $choices = [];

        foreach ($result as $model) {
            if (!array_key_exists($model['make'], $choices)) {
                $choices[$model['make']] = [];
            }

            $choices[$model['make']][$model['model']] = $model['model'];
        }

        return $choices;
    }

    public function getAttachmentsForLinkingQb($truckId = null, $customer = null): \Doctrine\ORM\QueryBuilder
    {
        $qb = $this->createQueryBuilder('att');

        $qb->select('att')
            ->leftJoin('att.currentLifecyclePeriod', 'period')
            ->leftJoin('att.truck', 'truck')
            ->addOrderBy('att.make')
            ->addOrderBy('att.model');

        if ($customer) {
            $qb->andWhere('period IS NULL OR period.customer IS NULL OR (period.customer = :customer AND truck IS NULL)')
                ->setParameter('customer', $customer);
        } else {
            $qb->andWhere('period IS NULL OR period.customer IS NULL');
        }

        if (!$truckId) {
            $qb->andWhere('truck is null');
        } else {
            $qb->andWhere('truck is null or truck.id = :truckId')
                ->setParameter('truckId', $truckId);
        }

        if (!$truckId) {
            $qb->andWhere('truck IS NULL');
        } else {
            $qb->andWhere('truck IS NULL or truck.id = :truckId')
                ->setParameter('truckId', $truckId);
        }

        return $qb;
    }
}
