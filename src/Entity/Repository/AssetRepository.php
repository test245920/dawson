<?php

namespace App\Entity\Repository;

use App\Entity\Asset;
use App\Entity\AssetLifecyclePeriod;
use App\Model\TransactionCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AssetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, string $entityClass = Asset::class)
    {
        parent::__construct($registry, $entityClass);
    }

    public function getUnassigned()
    {
        $qb = $this->createQueryBuilder('a')
            ->leftJoin('a.currentLifecyclePeriod', 'p')
            ->leftJoin('p.customer', 'currentCustomer')
            ->leftJoin('p.parentPeriod', 'parent')
            ->leftJoin('parent.customer', 'parentCustomer')
            ->andWhere('p IS NULL OR p.customer IS NULL OR currentCustomer.isDefault = true OR (p.type = :period_type AND (p.parentPeriod IS NULL OR parent.customer IS NULL OR parentCustomer.isDefault = true))')
            ->setParameter('period_type', AssetLifecyclePeriod::PERIOD_TYPE_CASUAL);

        return $qb->getQuery()->getResult();
    }

    public function getActiveListings()
    {
        $qb = $this->createQueryBuilder('a');

        $qb->select('a.id, a.serial as serial, a.make, a.model, lp.clientFleet as flt, cus.name as customer, parentCus.name as parentCustomer')
           ->leftJoin('a.currentLifecyclePeriod', 'lp')
           ->leftJoin('lp.parentPeriod', 'parent')
           ->leftJoin('lp.customer', 'cus')
           ->leftJoin('parent.customer', 'parentCus')
           ->andWhere('lp.customer is not null')
           ->addOrderBy('cus.name', 'ASC')
           ->addOrderBy('lp.clientFleet', 'ASC')
           ->addOrderBy('a.make', 'ASC')
           ->addOrderBy('a.model', 'ASC')
           ->addOrderBy('a.serial', 'ASC');

        $assets = $qb->getQuery()->getArrayResult();

        foreach ($assets as &$asset) {
            if ($asset['flt'] == null) {
                $asset['flt'] = 'None';
            }
            if ($asset['parentCustomer'] == null) {
                $asset['parentCustomer'] = 'None';
            }
        }

        return $assets;
    }

    public function getMaintenanceInfo($id)
    {
        $qb = $this->createQueryBuilder('a')
            ->select('a.id as truckId, tm.amount, tm.date, tm.detail')
            ->leftJoin('a.currentLifecyclePeriod', 'lp')
            ->leftJoin('lp.maintenances', 'tm')
            ->leftJoin('tm.transaction', 'tt')
            ->andWhere('tt.transactionCategory = :out')
            ->setParameter('out', TransactionCategory::OUT_TRUCK_MAINTENANCE_EXPENDITURE);

        return $qb->getQuery()->getArrayResult();
    }

    public function getMaintenanceRevenueInfo($id)
    {
        $qb = $this->createQueryBuilder('a')
            ->select('a.id as truckId, tt.amount, tt.date')
            ->leftJoin('a.currentLifecyclePeriod', 'lp')
            ->leftJoin('lp.transactions', 'tt')
            ->andWhere('tt.transactionCategory = :in')
            ->setParameter('in', TransactionCategory::IN_TRUCK_MAINTENANCE_REVENUE);

        return $qb->getQuery()->getArrayResult();
    }

    public function findItemsForAutocomplete(string $searchTerm, $customer)
    {
        $qb = $this->createQueryBuilder('a');

        $qb->select('a, lp')
            ->innerJoin('a.currentLifecyclePeriod', 'lp')
            ->andWhere('a.make LIKE :searchTerm OR a.model LIKE :searchTerm OR a.serial LIKE :searchTerm OR lp.clientFleet LIKE :searchTerm')
            ->setParameter('searchTerm', '%' . $searchTerm . '%');

        if ($customer) {
            $qb->andWhere('lp.customer = :customer')
                ->setParameter('customer', $customer);
        } else {
            $qb->andWhere('lp.customer IS NULL');
        }

        return $qb->getQuery()->getResult();
    }

    public function getAssetsWithHireDate(string $day)
    {
        $qb = $this->createQueryBuilder('a');

        $qb
            ->leftJoin('a.currentLifecyclePeriod', 'lp')
            ->andWhere('lp.type IN (:hireTypes)')
            ->andWhere('lp.start LIKE :day')
            ->setParameter('day', '%-%-' . $day . ' %')
            ->setParameter('hireTypes', [AssetLifecyclePeriod::PERIOD_TYPE_CONTRACT, AssetLifecyclePeriod::PERIOD_TYPE_CASUAL]);

        return $qb->getQuery()->getResult();
    }

    public function getAssetWithMaintenanceContractOnly(?string $customer = null)
    {
        $qb = $this->createQueryBuilder('a')
            ->select('a.id, a.id as truckId, cus.name as customer')
            ->leftJoin('a.currentLifecyclePeriod', 'lp')
            ->leftJoin('lp.maintenances', 'tm')
            ->leftJoin('lp.customer', 'cus')
            ->andWhere('lp.maintenanceContract = true');

        if ($customer) {
            $qb->andWhere('cus.name LIKE :name')
                ->setParameter('name', $customer);
        }

        $qb->orderBy('truckId');

        return $qb->getQuery()->getResult();
    }

    public function getCustomerWithMaintenanceContractOnly()
    {
        $qb = $this->createQueryBuilder('a')
            ->select('cus.name as customer')
            ->distinct()
            ->leftJoin('a.currentLifecyclePeriod', 'lp')
            ->leftJoin('lp.maintenances', 'tm')
            ->leftJoin('lp.customer', 'cus')
            ->andWhere('lp.maintenanceContract = true');

        return $qb->getQuery()->getArrayResult();
    }

    public function findByIdAndDateInBetween($id, $start = null, $end = null)
    {
        $qb = $this->createQueryBuilder('a')
            ->leftJoin('a.currentLifecyclePeriod', 'lp')
            ->andWhere('a.id = :id')
            ->setParameter('id', $id);

        if ($start and $start != 'null') {
            $qb->andWhere('lp.start >= :start')
                ->setParameter('start', $start);
        }

        if ($end and $end != 'null') {
            $isDateTimeObject = $end instanceof \DateTime;
            if ($isDateTimeObject == false) {
                $end = $end . ' 23:59:59';
            }

            $qb->andwhere('lp.end <= :end')
                ->setParameter('end', $end);
        }

        return $qb->getQuery()->getOneOrNullResult();
    }
}
