<?php

namespace App\Entity\Repository;

use App\Entity\LogEntry;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Gedmo\Exception\RuntimeException;
use Gedmo\Loggable\Entity\Repository\LogEntryRepository as BaseRepository;
use Gedmo\Loggable\LoggableListener;
use Gedmo\Tool\Wrapper\EntityWrapper;

class LogEntryRepository extends BaseRepository
{
    /**
     * Currently used loggable listener.
     */
    private ?LoggableListener $listener = null;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em, $em->getClassMetadata(LogEntry::class));
    }

    public function getLogEntries($entity, $hydrate = false): array
    {
        $q = $hydrate ? $this->getLogEntriesQuery($entity) : $this->getLogEntryDataQuery($entity);

        return $q->getResult();
    }

    private function getLogEntryDataQuery($entity): Query
    {
        $wrapped = new EntityWrapper($entity, $this->_em);
        $objectClass = $wrapped->getMetadata()->name;
        $meta = $this->getClassMetadata();
        $dql = "SELECT log.id, log.action, log.loggedAt, log.version, log.username FROM {$meta->name} log";
        $dql .= ' WHERE log.objectId = :objectId';
        $dql .= ' AND log.objectClass = :objectClass';
        $dql .= ' ORDER BY log.version DESC';

        $objectId = $wrapped->getIdentifier();
        $q = $this->_em->createQuery($dql);
        $q->setParameters(compact('objectId', 'objectClass'));

        return $q;
    }

    /**
     * Get the currently used LoggableListener.
     *
     * @return LoggableListener
     * @throws RuntimeException - if listener is not found
     */
    private function getLoggableListener(): ?LoggableListener
    {
        if (is_null($this->listener)) {
            foreach ($this->_em->getEventManager()->getListeners() as $event => $listeners) {
                foreach ($listeners as $hash => $listener) {
                    if ($listener instanceof LoggableListener) {
                        $this->listener = $listener;
                        break;
                    }
                }
                if ($this->listener) {
                    break;
                }
            }

            if (is_null($this->listener)) {
                throw new RuntimeException('The loggable listener could not be found');
            }
        }

        return $this->listener;
    }
}
