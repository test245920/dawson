<?php

namespace App\Entity\Repository;

use App\Entity\SalesJob;
use Doctrine\ORM\EntityRepository;

/**
 * @method SalesJob|null find($id, $lockMode = null, $lockVersion = null)
 * @method SalesJob|null findOneBy(array $criteria, array $orderBy = null)
 * @method SalesJob[]    findAll()
 * @method SalesJob[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SalesJobRepository extends EntityRepository
{
    public function getGroupByStatus(): array
    {
        $jobs = $this->createQueryBuilder('s')
            ->select('s', 't', 'lp', 'c')
            ->leftJoin('s.truck', 't')
            ->leftJoin('t.currentLifecyclePeriod', 'lp')
            ->leftJoin('lp.customer', 'c')
            ->getQuery()
            ->getArrayResult();

        return array_reduce($jobs, function (array $toilets, array $element): array {
            $toilets[SalesJob::CHOICES[$element['status']]][] = $element;

            return $toilets;
        }, []);
    }

    public function findOneById($id)
    {
        return $this->createQueryBuilder('s')
            ->select('lp.id as id, lp.clientFleet as clientFleet, m.name as model, ma.name as make, t.serial as serial')
            ->leftJoin('s.truck', 't')
            ->leftJoin('t.currentLifecyclePeriod', 'lp')
            ->leftJoin('t.model', 'm')
            ->leftJoin('m.make', 'ma')
            ->andWhere('s.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
