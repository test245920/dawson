<?php

namespace App\Entity\Repository;

use App\Entity\Stock;
use App\Model\SearchRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * StockRepository.
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class StockRepository extends ServiceEntityRepository implements SearchRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Stock::class);
    }

    public function findWithSupplierStock($id)
    {
        $qb = $this->createQueryBuilder('s')
            ->select('s, ss, sup')
            ->leftJoin('s.supplierStock', 'ss')
            ->leftJoin('ss.supplier', 'sup')
            ->andWhere('s.id = :id')
            ->setParameter('id', $id);

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function getItemsForSearch($user, $isFullAdmin)
    {
        $qb = $this->createQueryBuilder('s');

        $fields = Stock::getSearchFields();
        $select = [];

        foreach ($fields as $field => $displayName) {
            $select[] = 's.' . $field;
        }

        $qb->select(implode(', ', $select));
        $qb->addSelect($qb->expr()->literal(Stock::getSearchCategory()) . ' as category');
        $qb->addSelect('s.id as linkIdentifier');
        $qb->orderBy('s.code');

        return $qb->getQuery()->getArrayResult();
    }

    public function findByIncludingSoftDeleted(array $criteria): array
    {
        $em = $this->getEntityManager();

        $em->getFilters()->disable('softdeleteable');
        $result = $this->findBy($criteria);
        $em->getFilters()->enable('softdeleteable');

        return $result;
    }

    public function getItemsNotSuppliedBySupplier($id = null)
    {
        $qb = $this->createQueryBuilder('s');

        $qb->select('s.id, s.code, s.description')
            ->andWhere('s.id NOT IN (SELECT stock.id FROM Stock stock LEFT JOIN stock.supplierStock supplierStock LEFT JOIN supplierStock.supplier supplier WHERE supplier.id = :id)')
            ->setParameter('id', $id);

        return $qb->getQuery()->getArrayResult();
    }

    public function getStockItemsContaining($searchTerm, $currentOnly = false)
    {
        $sql = '
            SELECT DISTINCT stock.id stockId, stock.code, stock.description
            FROM stock
            WHERE stock.deletedAt IS NULL
            ' . ($currentOnly ? 'AND stock.replacedBy_id IS NULL' : '') . "
            AND (stock.description REGEXP :searchTerm
                OR stock.code REGEXP :searchTerm
                OR CONCAT(stock.code, ' - ', stock.description) REGEXP :searchTerm
            )
            GROUP BY stock.id
        ";

        $stmt             = $this->getEntityManager()->getConnection()->prepare($sql);
        $regexdSearchTerm = '^' . preg_quote((string) $searchTerm) . '.*';

        $stmt->bindParam(':searchTerm', $regexdSearchTerm);
        $stmt->execute();

        return $stmt->fetchAll();
    }
}
