<?php

namespace App\Entity\Repository;

use App\Entity\SupplierStockIssuePriceRule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class SupplierStockIssuePriceRuleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SupplierStockIssuePriceRule::class);
    }

    public function getMargin(string $supplierId, string $price)
    {
        $qb = $this->createQueryBuilder('s');

        $qb->select('s.profitMargin')
            ->andWhere('s.startPrice IS NULL OR s.startPrice <= ' . $price)
            ->andWhere('s.endPrice IS NULL OR s.endPrice > ' . $price)
            ->andWhere('s.supplier = ' . $supplierId);

        $result = $qb->getQuery()->getOneOrNullResult();

        return $result ? $result['profitMargin'] : null;
    }
}
