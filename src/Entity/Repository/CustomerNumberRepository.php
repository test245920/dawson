<?php

namespace App\Entity\Repository;

use App\Entity\CustomerNumber;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * CustomerNumberRepository.
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CustomerNumberRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CustomerNumber::class);
    }

    public function getNextNumberForLetter($letter): int|float
    {
        $qb = $this->createQueryBuilder('n')
            ->select('n.number')
            ->andWhere('n.letter = :letter')
            ->orderBy('n.number', 'DESC')
            ->setMaxResults(1)
            ->setParameter('letter', $letter);

        $result = $qb->getQuery()->getOneOrNullResult();
        $number = $result ? $result['number'] : 0;
        ++$number;

        return $number;
    }

    public function checkForExistingLetter($letter, $id)
    {
        $qb = $this->createQueryBuilder('n')
            ->andWhere('n.customer = :id')
            ->andWhere('n.letter = :letter')
            ->setParameter('letter', $letter)
            ->setParameter('id', $id);

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function findByIncludingSoftDeleted(array $criteria): array
    {
        $em = $this->getEntityManager();

        $em->getFilters()->disable('softdeleteable');
        $result = $this->findBy($criteria);
        $em->getFilters()->enable('softdeleteable');

        return $result;
    }
}
