<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * App\Entity\SupplierStockIssuePriceRule.
 *
 * @ORM\Table(name="supplier_stock_issue_price_rule")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\SupplierStockIssuePriceRuleRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 */
#[Assert\Callback(callback: 'validatePricesNotEmpty')]
#[Assert\Callback(callback: 'validateStartPriceLessThanEndPrice')]
#[Assert\Callback(callback: 'checkOverlappingRules')]
#[Assert\Callback(callback: 'validateProfitMarginGreaterThanZero')]
class SupplierStockIssuePriceRule
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Gedmo\Versioned
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(name="start_price", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $startPrice = null;

    /**
     * @ORM\Column(name="end_price", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $endPrice = null;

    /**
     * @ORM\Column(name="profit_margin", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $profitMargin = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Supplier", cascade={"persist"}, inversedBy="stockPriceRule")
     */
    protected ?Supplier $supplier = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getStartPrice(): ?string
    {
        return $this->startPrice;
    }

    /**
     * @param mixed $startPrice
     */
    public function setStartPrice(?string $startPrice): void
    {
        $this->startPrice = $startPrice;
    }

    /**
     * @return mixed
     */
    public function getEndPrice(): ?string
    {
        return $this->endPrice;
    }

    /**
     * @param mixed $endPrice
     */
    public function setEndPrice(?string $endPrice): void
    {
        $this->endPrice = $endPrice;
    }

    /**
     * @return mixed
     */
    public function getProfitMargin(): ?string
    {
        return $this->profitMargin;
    }

    /**
     * @param mixed $profitMargin
     */
    public function setProfitMargin(?string $profitMargin): void
    {
        $this->profitMargin = $profitMargin;
    }

    /**
     * @return Supplier
     */
    public function getSupplier(): ?Supplier
    {
        return $this->supplier;
    }

    /**
     * @param mixed $supplier
     */
    public function setSupplier(Supplier $supplier): void
    {
        $this->supplier = $supplier;
    }

    public function validatePricesNotEmpty(ExecutionContextInterface $context): void
    {
        if (!$this->getStartPrice() && !$this->getEndPrice()) {
            $context->addViolation('At least one of the price fields must be filled');
        }
    }

    public function validateStartPriceLessThanEndPrice(ExecutionContextInterface $context): void
    {
        if ($this->getStartPrice() && $this->getEndPrice()) {
            if ($this->getStartPrice() >= $this->getEndPrice()) {
                $context->addViolation('Start price must be less than end price.');
            }
        }
    }

    public function checkOverlappingRules(ExecutionContextInterface $context): void
    {
        $currentStart = $this->getStartPrice();
        $currentEnd   = $this->getEndPrice();

        $rules = $this->getSupplier()->getStockPriceRules();

        foreach ($rules as $rule) {
            if ($rule == $this) {
                continue;
            }

            $otherStart = $rule->getStartPrice();
            $otherEnd   = $rule->getEndPrice();

            if ($currentStart >= $otherStart && $currentStart < $otherEnd) {
                $context->addViolation(sprintf('This rule\'s start price overlaps another (%d - %d = %d%%)', $otherStart, $otherEnd, $rule->getProfitMargin()));

                return;
            }

            if ($currentEnd && (($currentEnd > $otherStart && $currentEnd < $otherEnd) or ($currentStart < $otherStart && ($otherEnd != 0 && $currentEnd >= $otherEnd)))) {
                $context->addViolation(sprintf('This rule\'s end price overlaps another (%d - %d = %d%%)', $otherStart, $otherEnd, $rule->getProfitMargin()));

                return;
            }
        }
    }

    public function validateProfitMarginGreaterThanZero(ExecutionContextInterface $context): void
    {
        if ($this->getProfitMargin() <= 0) {
            $context->addViolation('Profit margin must be greater than zero.');
        }
    }
}
