<?php

namespace App\Entity;

use App\Model\SearchableInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="asset_maintenance")
 * @ORM\Entity()
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class AssetMaintenance implements SearchableInterface
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(name="detail", type="string")
     * @Gedmo\Versioned
     */
    protected ?string $detail = null;

    /**
     * @ORM\Column(name="date", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $date = null;

    /**
     * @ORM\Column(name="createdAt", type="datetime")
     * @Gedmo\Versioned
     * @Gedmo\Timestampable(on="create")
     */
    protected ?\DateTimeInterface $createdAt = null;

    /**
     * @ORM\Column(name="updatedAt", type="datetime")
     * @Gedmo\Versioned
     * @Gedmo\Timestampable(on="update")
     */
    protected ?\DateTimeInterface $updatedAt = null;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\Column(name="amount", type="integer")
     * @Gedmo\Versioned
     */
    protected ?int $amount = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AssetLifecyclePeriod", cascade={"persist"}, inversedBy="maintenances")
     * @ORM\JoinColumn(onDelete="SET NULL", name="assetLifecyclePeriod_id")
     * @Gedmo\Versioned
     */
    protected ?AssetLifecyclePeriod $assetLifecyclePeriod = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\AssetTransaction", cascade={"persist"})
     */
    protected ?AssetTransaction $transaction = null;

    public static function getSearchFields(): array
    {
        return ['id'     => 'ID', 'amount' => 'amount', 'date'   => 'date'];
    }

    public static function getSearchCategory(): string
    {
        return 'stock_transaction';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setDate(?\DateTimeInterface $date): AssetMaintenance
    {
        $this->date = $date;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): AssetMaintenance
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): AssetMaintenance
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): AssetMaintenance
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setAmount(?int $quantity): AssetMaintenance
    {
        $this->amount = $quantity;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAssetLifecyclePeriod(?AssetLifecyclePeriod $value): AssetMaintenance
    {
        $this->assetLifecyclePeriod = $value;
        $value->addMaintenance($this);

        return $this;
    }

    public function getAssetLifecyclePeriod(): ?AssetLifecyclePeriod
    {
        return $this->assetLifecyclePeriod;
    }

    public function setDetail(?string $value): AssetMaintenance
    {
        $this->detail = $value;

        return $this;
    }

    public function getDetail(): ?string
    {
        return $this->detail;
    }

    public function setTransaction(?AssetTransaction $value): AssetMaintenance
    {
        $this->transaction = $value;
        $value->setMaintenance($this);

        return $this;
    }

    public function getTransaction(): ?AssetTransaction
    {
        return $this->transaction;
    }
}
