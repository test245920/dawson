<?php

namespace App\Entity;

use App\Model\SearchableInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Model.
 * @ORM\Table(name="truck_model")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\TruckModelRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class TruckModel implements SearchableInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TruckMake", inversedBy="models")
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Gedmo\Versioned
     */
    protected ?TruckMake $make = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Stock", inversedBy="oilFilterForTruckModel")
     * @ORM\JoinColumn(name="oilFilter_id", onDelete="SET NULL")
     * @Gedmo\Versioned
     */
    protected ?Stock $oilFilter = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Stock", inversedBy="airFilterForTruckModel")
     * @ORM\JoinColumn(name="airFilter_id" ,onDelete="SET NULL")
     * @Gedmo\Versioned
     */
    protected ?Stock $airFilter = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Stock", inversedBy="fuelFilterForTruckModel")
     * @ORM\JoinColumn(name="fuelFilter_id", onDelete="SET NULL")
     * @Gedmo\Versioned
     */
    protected ?Stock $fuelFilter = null;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     * @Gedmo\Versioned
     */
    #[Assert\NotBlank(message: 'Model name cannot be blank')]
    protected ?string $name = null;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    protected ?\DateTimeInterface $createdAt = null;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    protected ?\DateTimeInterface $updatedAt = null;

    /**
     * @ORM\Column(name="front_tyres", type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $frontTyres = null;

    /**
     * @ORM\Column(name="rear_tyres", type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $rearTyres = null;

    /**
     * @ORM\Column(name="fuel", type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $fuel = null;

    public static function getSearchFields(): array
    {
        return ['id'   => 'ID', 'name' => 'Name'];
    }

    public static function getSearchCategory(): string
    {
        return 'truck-model';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $value): void
    {
        $this->name = $value;
    }

    public function getMake(): ?TruckMake
    {
        return $this->make;
    }

    public function setMake(?TruckMake $value): TruckModel
    {
        $this->make = $value;

        return $this;
    }

    public function getOilFilter(): ?Stock
    {
        return $this->oilFilter;
    }

    public function setOilFilter(?Stock $value): TruckModel
    {
        $this->oilFilter = $value;

        return $this;
    }

    public function getAirFilter(): ?Stock
    {
        return $this->airFilter;
    }

    public function setAirFilter(?Stock $value): TruckModel
    {
        $this->airFilter = $value;

        return $this;
    }

    public function getFuelFilter(): ?Stock
    {
        return $this->fuelFilter;
    }

    public function setFuelFilter(?Stock $value): TruckModel
    {
        $this->fuelFilter = $value;

        return $this;
    }

    public function setFrontTyres(?string $value): TruckModel
    {
        $this->frontTyres = $value;

        return $this;
    }

    public function getFrontTyres(): ?string
    {
        return $this->frontTyres;
    }

    public function setRearTyres(?string $value): TruckModel
    {
        $this->rearTyres = $value;

        return $this;
    }

    public function getRearTyres(): ?string
    {
        return $this->rearTyres;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $value): void
    {
        $this->updatedAt = $value;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $value): void
    {
        $this->createdAt = $value;
    }

    public function getFuel(): ?string
    {
        return $this->fuel;
    }

    public function setFuel(?string $value): void
    {
        $this->fuel = $value;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $value): TruckModel
    {
        $this->deletedAt = $value;

        return $this;
    }
}
