<?php

namespace App\Entity;

use App\Model\AssetInterface;
use App\Model\SearchableInterface;
use App\Model\TruckAttachmentType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * App\Entity\TruckAttachment.
 *
 * @ORM\Table(name="truck_attachment")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\TruckAttachmentRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 */
#[Assert\Callback(callback: 'validateMakeAndModel')]
class TruckAttachment extends Asset implements SearchableInterface, AssetInterface
{
    /**
     * @ORM\Column(name="make", type="string", length=255)
     * @Gedmo\Versioned
     */
    protected ?string $make = null;

    /**
     * @ORM\Column(name="model", type="string", length=255)
     * @Gedmo\Versioned
     */
    protected ?string $model = null;

    /**
     * @ORM\Column(name="type", type="text")
     * @Gedmo\Versioned
     */
    #[Assert\NotBlank]
    protected ?string $type = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Truck", mappedBy="attachment", cascade={"persist"})
     * @var Collection<Truck>
     */
    protected Collection $truck;

    public function __construct()
    {
        parent::__construct();
        $this->truck = new ArrayCollection();
    }

    public static function getSearchFields(): array
    {
        return ['id'     => 'ID', 'make'   => 'Make', 'model'  => 'Model', 'serial' => 'Serial'];
    }

    public static function getSearchCategory(): string
    {
        return 'truck-attachment';
    }

    public function getViewRoute(): string
    {
        return 'view_truck_attachment';
    }

    public function getViewParams(): array
    {
        return ['id' => $this->id];
    }

    public function getNewRoute(): string
    {
        return 'new_truck_attachment';
    }

    public function getEditRoute(): string
    {
        return 'edit_truck_attachment';
    }

    public function getEditParams(): array
    {
        return ['id' => $this->id];
    }

    public function validateMakeAndModel(ExecutionContextInterface $context): void
    {
        if (!$this->make) {
            if ($this->newMake) {
                $this->make = $this->newMake;
            } else {
                $context->addViolation('Make Required.');
            }
        }

        if (!$this->model) {
            if ($this->newModel) {
                $this->model = $this->newModel;
            } else {
                $context->addViolation('Model Required.');
            }
        }
    }

    public function getMake(): ?string
    {
        return $this->make;
    }

    public function setMake(?string $value): TruckAttachment
    {
        $this->make = $value;

        return $this;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(?string $value): TruckAttachment
    {
        $this->model = $value;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType($value): TruckAttachment
    {
        if (!in_array($value, TruckAttachmentType::TYPES)) {
            throw new \InvalidArgumentException(sprintf('Invalid truck attachment type "%s" given', $value));
        }

        $this->type = $value;

        return $this;
    }
}
