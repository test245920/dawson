<?php

namespace App\Entity;

use App\Model\SearchableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * App\Entity\Supplier.
 *
 * @ORM\Table(name="supplier")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\SupplierRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
#[UniqueEntity(fields: ['code', 'name'], message: 'A supplier with this name already exists', repositoryMethod: 'findByIncludingSoftDeleted')]
class Supplier implements SearchableInterface
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * Seems to be the Access DB table ID - legacy.
     *
     * @ORM\Column(name="code", type="string", length=50, nullable=true, unique=true)
     */
    protected ?string $code = null;

    /**
     * @ORM\Column(name="name", type="string", length=50)
     * @Gedmo\Versioned
     */
    #[Assert\NotNull(message: 'Required')]
    protected ?string $name = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\SupplierAddress", mappedBy="supplier", cascade={"persist", "remove"})
     * @Gedmo\Versioned
     */
    protected ?SupplierAddress $supplierAddress = null;

    /**
     * @ORM\Column(name="telephone", type="text",nullable=true,  length=30)
     * @Gedmo\Versioned
     */
    protected ?string $telephone = null;

    /**
     * @ORM\Column(name="fax", type="text",nullable=true,  length=50)
     * @Gedmo\Versioned
     */
    protected ?string $fax = null;

    /**
     * @ORM\Column(name="email", type="text",nullable=true,  length=50)
     * @Gedmo\Versioned
     */
    protected ?string $email = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SupplierStock", mappedBy="supplier", cascade={"persist"})
     * @var Collection<SupplierStock>
     */
    protected Collection $stockItems;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Contact", mappedBy="supplier", cascade={"persist"})
     * @var Collection<Contact>
     */
    protected Collection $contacts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PurchaseOrder", mappedBy="supplier", cascade={"persist"})
     * @var Collection<PurchaseOrder>
     */
    protected Collection $purchaseOrders;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    protected ?\DateTimeInterface $createdAt = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Stock", mappedBy="preferredSupplier", cascade={"persist"})
     * @var Collection<Stock>
     */
    protected Collection $preferredSupplierFor;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SupplierStockIssuePriceRule", mappedBy="supplier", cascade={"persist", "remove"})
     * @var Collection<SupplierStockIssuePriceRule>
     */
    protected Collection $stockPriceRules;

    public function __construct()
    {
        $this->contacts             = new ArrayCollection();
        $this->stockItems           = new ArrayCollection();
        $this->purchaseOrders       = new ArrayCollection();
        $this->preferredSupplierFor = new ArrayCollection();
        $this->stockPriceRules      = new ArrayCollection();
    }

    public static function getSearchFields(): array
    {
        return ['id'        => 'ID', 'name'      => 'Name', 'email'     => 'Email', 'telephone' => 'Telephone', 'fax'       => 'Fax'];
    }

    public static function getSearchCategory(): string
    {
        return 'supplier';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $value): Supplier
    {
        $this->code = $value;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $value): Supplier
    {
        $this->name = $value;

        return $this;
    }

    public function getSupplierAddress(): ?SupplierAddress
    {
        return $this->supplierAddress;
    }

    public function setSupplierAddress(?SupplierAddress $value): Supplier
    {
        $this->supplierAddress = $value;
        $value->setSupplier($this);

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $value): Supplier
    {
        $this->telephone = $value;

        return $this;
    }

    public function getFax(): ?string
    {
        return $this->fax;
    }

    public function setFax(?string $value): Supplier
    {
        $this->fax = $value;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $value): Supplier
    {
        $this->email = $value;

        return $this;
    }

    public function addContact($value): Supplier
    {
        $this->contacts->add($value);
        $value->setSupplier($this);

        return $this;
    }

    public function removeContact($value): void
    {
        $this->contacts->removeElement($value);
    }

    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function addStockItem($value): Supplier
    {
        $this->stockItems->add($value);
        $value->setSupplier($this);

        return $this;
    }

    public function removeStockItem($value): void
    {
        $this->stockItems->removeElement($value);
    }

    public function getStockItems(): Collection
    {
        return $this->stockItems;
    }

    public function addPurchaseOrder($value): Supplier
    {
        $this->purchaseOrders->add($value);
        $value->setSupplier($this);

        return $this;
    }

    public function removePurchaseOrder($value): void
    {
        $this->purchaseOrders->removeElement($value);
    }

    public function getPurchaseOrders(): Collection
    {
        return $this->purchaseOrders;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $value): Supplier
    {
        $this->deletedAt = $value;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $value): Supplier
    {
        $this->createdAt = $value;

        return $this;
    }

    public function addPreferredSupplierFor($value): Supplier
    {
        $this->preferredSupplierFor->add($value);
        $value->setPreferredSupplier($this);

        return $this;
    }

    public function removePreferredSupplierFor($value): void
    {
        $this->preferredSupplierFor->removeElement($value);
    }

    public function getPreferredSupplierFor(): Collection
    {
        return $this->preferredSupplierFor;
    }

    public function addStockPriceRule($value): Supplier
    {
        $this->stockPriceRules->add($value);
        $value->setSupplier($this);

        return $this;
    }

    public function removeStockPriceRule($value): void
    {
        $this->stockPriceRules->removeElement($value);
    }

    public function getStockPriceRules(): Collection
    {
        return $this->stockPriceRules;
    }
}
