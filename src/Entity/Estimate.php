<?php

namespace App\Entity;

use App\Model\SearchableInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Estimate.
 *
 * @ORM\Table(name="estimate")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\EstimateRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Estimate implements SearchableInterface
{
    /*
     * Non-persisted fields, for form use only
     */
    public bool $issueImmediately = false;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(name="code", type="string", length=255, nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $code = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Customer", cascade={"persist"}, inversedBy="estimates")
     * @Gedmo\Versioned
     */
    protected ?Customer $customer = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Job", cascade={"persist"}, inversedBy="estimates")
     * @Gedmo\Versioned
     */
    protected ?Job $job = null;

    /**
     * @ORM\Column(name="details", type="string", length=255, nullable=true)
     * @Gedmo\Versioned
     */
    #[Assert\NotNull(message: 'Required')]
    protected ?string $details = null;

    /**
     * @ORM\Column(name="total_price", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $totalPrice = null;

    /**
     * @ORM\Column(name="labour_price", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $labourPrice = null;

    /**
     * @ORM\Column(name="parts_price", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $partsPrice = null;

    /**
     * @var bool
     *
     * @ORM\Column(name="awaiting_response", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $awaitingResponse = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="pending", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $pending = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="issued", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $issued = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="accepted", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $accepted = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="rejected", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $rejected = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="queried", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $queried = false;

    /**
     * @var int
     *
     * @ORM\Column(name="completion_stage", type="integer", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?int $completionStage = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="queryText", type="string", length=255, nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $queryText = null;

    /**
     * @ORM\OneToMany(targetEntity="EstimateEvent", mappedBy="estimate", cascade={"persist", "remove"} )
     * @var Collection<EstimateEvent>
     */
    protected Collection $estimateEvents;

    /**
     * @ORM\OneToMany(targetEntity="EstimateStock", mappedBy="estimate", cascade={"persist", "remove"} )
     * @var Collection<EstimateStock>
     */
    protected Collection $estimateStocks;

    /**
     * @ORM\OneToMany(targetEntity="EstimateItem", mappedBy="estimate", cascade={"persist", "remove"} )
     * @ORM\OneToMany(targetEntity="EstimateItem", mappedBy="estimate", cascade={"persist", "remove"} )
     * @var Collection<EstimateItem>
     */
    protected Collection $estimateItems;

    /**
     * @ORM\Column(name="labour_time", type="integer", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?int $labourTime = null;

    /**
     * @ORM\Column(name="travel_time", type="integer", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?int $travelTime = null;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_lead", type="boolean")
     * @Gedmo\Versioned
     */
    protected ?bool $isLead = false;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Contact", inversedBy="estimates", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @var Collection<Contact>
     */
    protected Collection $contacts;

    /**
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    protected ?\DateTimeInterface $createdAt = null;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\Column(name="unsuccessful_reason", type="string", length=255, nullable=true)
     * @Gedmo\Versioned
     */
    protected ?string $unsuccessfulReason = null;

    /**
     * @ORM\OneToMany(targetEntity="FutureWorkRecommendation", mappedBy="estimate", cascade={"persist"} )
     * @var Collection<FutureWorkRecommendation>
     */
    protected Collection $futureWorkRecommendations;

    public function __construct()
    {
        $this->estimateEvents            = new ArrayCollection();
        $this->estimateStocks            = new ArrayCollection();
        $this->estimateItems             = new ArrayCollection();
        $this->contacts                  = new ArrayCollection();
        $this->futureWorkRecommendations = new ArrayCollection();
    }

    public static function getSearchFields(): array
    {
        return ['id'      => 'id', 'details' => 'details'];
    }

    public static function getSearchCategory(): string
    {
        return 'estimate';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setCustomer(?Customer $customer): Estimate
    {
        $this->customer = $customer;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setDetails(?string $details): Estimate
    {
        $this->details = $details;

        return $this;
    }

    public function getDetails(): ?string
    {
        return $this->details;
    }

    public function setAwaitingResponse(?bool $awaitingResponse): Estimate
    {
        $this->awaitingResponse = $awaitingResponse;

        return $this;
    }

    public function getAwaitingResponse(): ?bool
    {
        return $this->awaitingResponse;
    }

    public function setPending(?bool $value): Estimate
    {
        $this->pending = $value;

        return $this;
    }

    public function getPending(): ?bool
    {
        return $this->pending;
    }

    public function setAccepted(?bool $accepted): Estimate
    {
        $this->accepted = $accepted;

        return $this;
    }

    public function getAccepted(): ?bool
    {
        return $this->accepted;
    }

    public function setRejected(?bool $rejected): Estimate
    {
        $this->rejected = $rejected;

        return $this;
    }

    public function getRejected(): ?bool
    {
        return $this->rejected;
    }

    public function setQueried(?bool $queried): Estimate
    {
        $this->queried = $queried;

        return $this;
    }

    public function getQueried(): ?bool
    {
        return $this->queried;
    }

    public function setQueryText(?string $queryText): Estimate
    {
        $this->queryText = $queryText;

        return $this;
    }

    public function getQueryText(): ?string
    {
        return $this->queryText;
    }

    public function getTotalPrice(): ?string
    {
        return $this->totalPrice;
    }

    public function setTotalPrice(?string $value): Estimate
    {
        $this->totalPrice = $value;

        return $this;
    }

    public function getLabourPrice(): ?string
    {
        return $this->labourPrice;
    }

    public function setLabourPrice(?string $value): Estimate
    {
        $this->labourPrice = $value;

        return $this;
    }

    public function getPartsPrice(): ?string
    {
        return $this->partsPrice;
    }

    public function setPartsPrice(?string $value): Estimate
    {
        $this->partsPrice = $value;

        return $this;
    }

    public function getJob(): ?Job
    {
        return $this->job;
    }

    public function setJob(?Job $value): Estimate
    {
        $this->job = $value;

        return $this;
    }

    public function addEstimateEvent($value): Estimate
    {
        $this->estimateEvents->add($value);
        $value->setEstimate($this);

        return $this;
    }

    public function removeEstimateEvent($value): Estimate
    {
        $this->estimateEvents->removeElement($value);

        return $this;
    }

    public function getEstimateEvents()
    {
        return $this->estimateEvents->toArray();
    }

    public function addEstimateStock($value): Estimate
    {
        $this->estimateStocks->add($value);
        $value->setEstimate($this);

        return $this;
    }

    public function removeEstimateStock($value): Estimate
    {
        $this->estimateStocks->removeElement($value);

        return $this;
    }

    public function getEstimateStocks(): Collection
    {
        return $this->estimateStocks;
    }

    public function addEstimateItem($value): Estimate
    {
        $this->estimateItems->add($value);
        $value->setEstimate($this);

        return $this;
    }

    public function removeEstimateItem($value): Estimate
    {
        $this->estimateItems->removeElement($value);

        return $this;
    }

    public function getEstimateItems(): Collection
    {
        return $this->estimateItems;
    }

    public function getTravelTime(): ?int
    {
        return $this->travelTime;
    }

    public function setTravelTime(?int $value): Estimate
    {
        $this->travelTime = $value;

        return $this;
    }

    public function getIssued(): ?bool
    {
        return $this->issued;
    }

    public function setIssued(?bool $value): Estimate
    {
        $this->issued = $value;

        return $this;
    }

    public function getLabourTime(): ?int
    {
        return $this->labourTime;
    }

    public function setLabourTime(?int $value): Estimate
    {
        $this->labourTime = $value;

        return $this;
    }

    public function getIsLead(): ?bool
    {
        return $this->isLead;
    }

    public function setIsLead(?bool $value): Estimate
    {
        $this->isLead = $value;

        return $this;
    }

    public function getCompletionStage(): ?int
    {
        return $this->completionStage;
    }

    public function setCompletionStage(?int $value): Estimate
    {
        $this->completionStage = $value;

        return $this;
    }

    public function addContact($value): Estimate
    {
        $this->contacts->add($value);
        $value->addEstimate($this);

        return $this;
    }

    public function removeContact($value): Estimate
    {
        $this->contacts->removeElement($value);

        return $this;
    }

    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $value): void
    {
        $this->createdAt = $value;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $value): Estimate
    {
        $this->code = $value;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $value): void
    {
        $this->deletedAt = $value;
    }

    public function setUnsuccessfulReason(?string $value): Estimate
    {
        $this->unsuccessfulReason = $value;

        return $this;
    }

    public function getUnsuccessfulReason(): ?string
    {
        return $this->unsuccessfulReason;
    }

    public function addFutureWorkRecommendation($value): Estimate
    {
        $this->futureWorkRecommendations->add($value);
        $value->setEstimate($this);

        return $this;
    }

    public function removeFutureWorkRecommendation($value): Estimate
    {
        $this->futureWorkRecommendations->removeElement($value);

        return $this;
    }

    public function getFutureWorkRecommendations(): Collection
    {
        return $this->futureWorkRecommendations;
    }
}
