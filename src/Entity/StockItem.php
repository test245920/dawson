<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Evence\Bundle\SoftDeleteableExtensionBundle\Mapping\Annotation as Evence;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * StockItem.
 *
 * @ORM\Table(name="stock_item")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\StockItemRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 */
class StockItem
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Gedmo\Versioned
     */
    protected ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Stock", cascade={"persist"}, inversedBy="stockItems")
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Evence\onSoftDelete(type="SET NULL")
     * @Gedmo\Versioned
     */
    protected ?Stock $stock = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SupplierStock", cascade={"persist"}, inversedBy="stockItems")
     * @ORM\JoinColumn(name="supplierStock_id", onDelete="SET NULL")
     * @Evence\onSoftDelete(type="SET NULL")
     * @Gedmo\Versioned
     */
    protected ?SupplierStock $supplierStock = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Job", inversedBy="attachedStockItems", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Evence\onSoftDelete(type="SET NULL")
     * @Gedmo\Versioned
     */
    protected ?Job $job = null;

    /**
     * @ORM\Column(name="sold", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $sold = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PurchaseOrder", inversedBy="stockItems")
     * @ORM\JoinColumn(name="purchaseOrder_id")
     * @Gedmo\Versioned
     */
    protected ?PurchaseOrder $purchaseOrder = null;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    protected ?\DateTimeInterface $createdAt = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Location", inversedBy="stockItems", cascade={"persist"})
     * @Gedmo\Versioned
     */
    protected ?Location $location = null;

    /**
     * @ORM\Column(name="used_on_job", type="boolean", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?bool $usedOnJob = null;

    /**
     * @ORM\Column(name="returned_from_job_id", type="integer", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?int $returnedFromJobId = null;

    /**
     * @ORM\Column(name="cost_price", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $costPrice = null;

    /**
     * @ORM\Column(name="issue_price", type="decimal", nullable=true, precision=8, scale=2)
     * @Gedmo\Versioned
     */
    protected ?float $issuePrice = null;

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $value): void
    {
        $this->deletedAt = $value;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setStock(?Stock $stock): StockItem
    {
        $this->stock = $stock;

        return $this;
    }

    public function getStock(): ?Stock
    {
        return $this->stock;
    }

    public function setSupplierStock(?SupplierStock $value): StockItem
    {
        $this->supplierStock = $value;

        return $this;
    }

    public function getSupplierStock(): ?SupplierStock
    {
        return $this->supplierStock;
    }

    public function setJob($job): StockItem
    {
        if ($job) {
            $this->issuePrice = $this->supplierStock->getIssuePrice();
            $this->costPrice = $this->supplierStock->getCostPrice();

            $job->addAttachedStockItem($this);
        } else {
            if ($this->job) {
                $this->job->removeAttachedStockItem($this);
            }

            $this->issuePrice = null;
            $this->costPrice = null;
        }

        $this->job = $job;

        return $this;
    }

    public function getJob(): ?Job
    {
        return $this->job;
    }

    public function setSold(?\DateTimeInterface $sold): StockItem
    {
        $this->sold = $sold;

        return $this;
    }

    public function getSold(): ?\DateTimeInterface
    {
        return $this->sold;
    }

    public function setPurchaseOrder(?PurchaseOrder $purchaseOrder): StockItem
    {
        $this->purchaseOrder = $purchaseOrder;
        $purchaseOrder->addStockItem($this);

        return $this;
    }

    public function getPurchaseOrder(): ?PurchaseOrder
    {
        return $this->purchaseOrder;
    }

    public function setLocation(?Location $value): StockItem
    {
        $this->location = $value;

        return $this;
    }

    public function getLocation(): ?Location
    {
        return $this->location;
    }

    public function getUsedOnJob(): ?bool
    {
        return $this->usedOnJob;
    }

    public function setUsedOnJob(?bool $value): void
    {
        $this->usedOnJob = $value;
    }

    public function getReturnedFromJobId(): ?int
    {
        return $this->returnedFromJobId;
    }

    public function setReturnedFromJobId(?int $value): void
    {
        $this->returnedFromJobId = $value;
    }

    public function getCostPrice(): ?string
    {
        return $this->costPrice;
    }

    public function setCostPrice(?string $value): StockItem
    {
        $this->costPrice = $value;

        return $this;
    }

    public function getIssuePrice(): ?string
    {
        return $this->issuePrice;
    }

    public function setIssuePrice(?string $value): StockItem
    {
        $this->issuePrice = $value;

        return $this;
    }
}
