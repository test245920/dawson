<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * App\Entity\BillingAddress.
 *
 * @ORM\Table(name="billing_address")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\BillingAddressRepository")
 * @Gedmo\Loggable(logEntryClass="App\Entity\LogEntry")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class BillingAddress extends AbstractAddress
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Gedmo\Versioned
     */
    protected ?int $id = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Customer", inversedBy="billingAddress", cascade={"persist"})
     * @ORM\JoinColumn(name="customer")
     */
    protected ?Customer $customer = null;

    /**
     * @ORM\Column(name="postcode", type="string", nullable=true, length=8)
     * @Gedmo\Versioned
     */
    #[Assert\Length(max: '8', maxMessage: 'Please enter a valid postcode')]
    protected ?string $postcode = null;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected ?\DateTimeInterface $deletedAt = null;

    /**
     * @ORM\OneToOne(targetEntity="Location", inversedBy="billingAddress", cascade={"persist"})
     */
    protected ?Location $location = null;

    public function setCustomer(?Customer $value): BillingAddress
    {
        $this->customer = $value;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setPostcode($value): BillingAddress
    {
        $this->postcode = $value;

        return $this;
    }

    public function getPostcode(): ?string
    {
        return $this->postcode;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $value): BillingAddress
    {
        $this->deletedAt = $value;

        return $this;
    }

    public function setLocation(?Location $value): BillingAddress
    {
        $this->location = $value;

        return $this;
    }

    public function getLocation(): ?Location
    {
        return $this->location;
    }

    public function getId(): ?int
    {
        return $this->id;
    }
}
