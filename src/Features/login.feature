Feature: Login is as a user (megaadmin)
    To test a megaadmin login
    As a registered site user
    "Dawson Forklift Services" should appear

    Scenario: Login as a megaadmin (megaadmin)
        Given I am on "/login"
        When I fill in "_username" with "megaadmin"
        And I fill in "_password" with "megaadmin"
        And I press "Sign In"
        Then I should be on "/"

