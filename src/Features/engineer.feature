Feature: Engineer feature
    Add, edit, delete engineers and their data, contacts and sites.

    @javascript
    Scenario: View an engineer's jobs
        Given I am authenticated as "megaadmin"
            And I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | engineer |
                | testcustomer | test details | 2     | engineer |
        When I go to "/engineer/list-jobs/1"
            And I select "engineer" from "user_selector"
        Then I should see "testcustomer"

    @javascript
    Scenario: View a job's particulars
        Given I am authenticated as "engineer"
            And I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | engineer |
                | testcustomer | test details | 2     | engineer |
        When I go to "/engineer/list-jobs/3"
            And I click on "testcustomer" in the "Jobs" column
        Then I should see "test details"

    @javascript
    Scenario: Add photos to a job
        Given I am authenticated as "engineer"
            And I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | labourRate |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | 20.00      |
            And I have the following jobs:
                | customer     | detail       | stage | engineer |
                | testcustomer | test details | 2     | engineer |
            When I go to "engineer/list-jobs/3"
            And I click on "testcustomer" in the "Jobs" column
            And I click the "Add Image" button in the "job-particulars" section
            And I click the "Add new" button in the "images" section
            And I attach the file "causeway.jpg" to "job_images_form_images_0_image"
            And I press "Save"
            And I wait 1 second
        Then I should see "Images"
