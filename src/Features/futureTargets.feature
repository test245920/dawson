Feature: Testing out lead future target functionality

    Background:
        Given I am authenticated as "megaadmin"

    @javascript
    Scenario: Check future target appears in this month column
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city | isLead |
                | testcustomer | 123 Walker Avenue       | Atlanta             | 1      |
            And I have the following reminders:
                | type          | detail        | date     | user      |
                | Future Target | test reminder | ten days | megaadmin |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | reminder      | future target | type         |
                | testcustomer | Rick   | No contact | Normal   | test reminder | true          | New Purchase |
        When I go to "/leads/futureTargets/list"
            And I should see "testcustomer" in the "This month" column
            And I go to "/reminders/list"
        Then I should see "test reminder" in the "This month" column

    @javascript
    Scenario: Check future target appears in Next 6 months column
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city | isLead |
                | testcustomer | 123 Walker Avenue       | Atlanta             | 1      |
            And I have the following reminders:
                | type          | detail        | date     | user      |
                | Future Target | test reminder | 3 months | megaadmin |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | reminder      | future target | type         |
                | testcustomer | Rick   | No contact | Normal   | test reminder | true          | New Purchase |
        When I go to "/leads/futureTargets/list"
            And I should see "testcustomer" in the "Next 6 months" column
            And I go to "/reminders/list"
        Then I should see "test reminder" in the "Long Range" column

    @javascript
    Scenario: Check future target appears in This Year column
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city | isLead |
                | testcustomer | 123 Walker Avenue       | Atlanta             | 1      |
            And I have the following reminders:
                | type          | detail        | date     | user      |
                | Future Target | test reminder | 9 months | megaadmin |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | reminder      | future target | type         |
                | testcustomer | Rick   | No contact | Normal   | test reminder | true          | New Purchase |
        When I go to "/leads/futureTargets/list"
            And I should see "testcustomer" in the "This Year" column
            And I go to "/reminders/list"
        Then I should see "test reminder" in the "Long Range" column

    @javascript
    Scenario: Check future target appears in Longer Range column
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city | isLead |
                | testcustomer | 123 Walker Avenue       | Atlanta             | 1      |
            And I have the following reminders:
                | type          | detail        | date      | user      |
                | Future Target | test reminder | 18 months | megaadmin |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | reminder      | future target | type         |
                | testcustomer | Rick   | No contact | Normal   | test reminder | true          | New Purchase |
        When I go to "/leads/futureTargets/list"
            And I should see "testcustomer" in the "Longer Range" column
            And I go to "/reminders/list"
        Then I should see "test reminder" in the "Long Range" column

    @javascript
    Scenario: Check reminder appears in This week column
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city | isLead |
                | testcustomer | 123 Walker Avenue       | Atlanta             | 1      |
            And I have the following reminders:
                | type          | detail        | date     | user      |
                | Future Target | test reminder | tomorrow | megaadmin |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | reminder      | future target | type         |
                | testcustomer | Rick   | No contact | Normal   | test reminder | true          | New Purchase |
        When I go to "/leads/futureTargets/list"
            And I should see "testcustomer" in the "This month" column
            And I go to "/reminders/list"
        Then I should see "test reminder" in the "This week" column

    @javascript
    Scenario: Check reminder appears in This Month column
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
            And I have the following reminders:
                | type          | detail        | date     | user      |
                | Future Target | test reminder | ten days | megaadmin |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | reminder      | future target | type         |
                | testcustomer | Rick   | No contact | Normal   | test reminder | true          | New Purchase |
        When I go to "/leads/futureTargets/list"
            And I should see "testcustomer" in the "This month" column
            And I go to "/reminders/list"
        Then I should see "test reminder" in the "This month" column
