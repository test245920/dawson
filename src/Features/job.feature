Feature: Testing out job functionality
    Add a job, assign an engineer, create a purchase order, complete a job, log work, sign off a job, raise an estimate, assign stock

    Background:
        Given I am authenticated as "megaadmin"

    @javascript
    Scenario: Add a job
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
        When I go to "/jobs"
            And I follow "new-item"
            And I select "testcustomer" from "job_form_customer"
            And I fill in the following:
                |  job_form_detail  |  test job  |
            And I press "Save"
        Then the column "Jobs to assign" should contain "testcustomer"
            And I should see "has been successfully added"

    @javascript
    Scenario: Assign and engineer to a job
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | breakdown |
                | testcustomer | test details | 1     | yes       |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Jobs to assign" column
            And I click the "Engineer" button in the "job-particulars" section
            And I check "First Engineer"
            And I press "Save"
        Then I should see "testcustomer" in the "Live Jobs" column as the colour "red"

    @javascript
    Scenario: Click button to raise a purchase order but then close the form without creating one
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage |
                | testcustomer | test details | 1     |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Jobs to assign" column
            And I follow "Create PO"
        Then I should see "testcustomer" in the "Jobs to assign" column

    @javascript
    Scenario: Click button to assign an engineer but then close the form without doing so
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage |
                | testcustomer | test details | 1     |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Jobs to assign" column
            And I click the "Engineer" button in the "job-particulars" section
        Then the column "Jobs to assign" should contain "testcustomer"

    @javascript
    Scenario: Click button to assign stock but then close the form without doing so
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | engineer |
                | testcustomer | test details | 2     | engineer |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Live Jobs" column
            And I click the "Assign Stock" button in the "job-particulars" section
        Then the column "Live Jobs" should contain "testcustomer"

    @javascript
    Scenario: Click button to log work but then close the form without doing so
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | engineer |
                | testcustomer | test details | 2     | engineer |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Live Jobs" column
            And I click the "Log work" button in the "job-particulars" section
        Then the column "Jobs to assign" should contain "testcustomer"

    @javascript
    Scenario: Click button to complete a job but then close the form without doing so
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | engineer |
                | testcustomer | test details | 2     | engineer |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Live Jobs" column
            And I click the "Complete" button in the "job-particulars" section
            And I press "Close" on the current modal
        Then the column "Jobs to assign" should contain "testcustomer"

    @javascript
    Scenario: Click button to postpone a job but then close the form without doing so
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | engineer |
                | testcustomer | test details | 2     | engineer |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Live Jobs" column
            And I click the "Postpone" button in the "job-particulars" section
            And I press "Cancel" on the current modal
        Then the column "Jobs to assign" should contain "testcustomer"

    @javascript
    Scenario: Create an Purchase order which is not ordered on creation for an unassigned job
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following trucks:
                | make        | model  | serial  | yom  | customer     | siteAddress |
                | Caterpillar | CAT123 | SER2015 | 2015 | testcustomer | Main site   |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | truck   |
                | testcustomer | test details | 2     | engineer | SER2015 |
            And I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     | costPrice | carriagePrice |
                | stock item  | SI1  | 7   | 14    | ABC supplies | 5.00      | 1.00          |
            And I have the following stockItems:
                | stock      | count |
                | stock item | 1     |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Live Jobs" column
            And I click the "Create PO" button in the "job-particulars" section
            And I replace "_search" with "stock"
            And I wait 1 second
            And I click the "Add Special Part" button in the "stockItems" section
            And I fill in the following:
                | purchase_order_form_estimateItems_0_item        | special item |
                | purchase_order_form_estimateItems_0_price       | 50           |
                | purchase_order_form_estimateItems_0_retailPrice | 50           |
                | purchase_order_form_estimateItems_0_quantity    | 1            |
            And I press "Save"
            And I wait for the "job" particulars section to appear
        Then I should see "Purchase Orders" in the "job-particulars" section

    @javascript
    Scenario: Create a purchase order which is ordered on creation for a unassigned job
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following trucks:
                | make        | model  | serial  | yom  | customer     | siteAddress |
                | Caterpillar | CAT123 | SER2015 | 2015 | testcustomer | Main site   |
            And I have the following jobs:
                | customer     | detail       | stage | truck   |
                | testcustomer | test details | 1     | SER2015 |
            And I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     | costPrice | carriagePrice |
                | stock item  | SI1  | 7   | 14    | ABC supplies | 5.00      | 1.00          |
            And I have the following stockItems:
                | stock      | count |
                | stock item | 1     |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Jobs to assign" column
            And I click the "Create PO" button in the "job-particulars" section
            And I replace "_search" with "stock"
            And I wait 1 second
            And I click the "Add Special Part" button in the "stockItems" section
            And I fill in the following:
                |  purchase_order_form_estimateItems_0_item      |  special item  |
                |  purchase_order_form_estimateItems_0_price     |  50            |
                |  purchase_order_form_estimateItems_0_quantity  |  1             |
            And I press "Save & Order"
            And I should see "testcustomer" in the "Part on Order" column
            And I wait for the "job" particulars section to appear
        Then I should see "Purchase Orders" in the "job-particulars" section

    @javascript
    Scenario: Create a purchase order which is not ordered on creation for a live job
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following trucks:
                | make        | model  | serial  | yom  | customer     | siteAddress |
                | Caterpillar | CAT123 | SER2015 | 2015 | testcustomer | Main site   |
            And I have the following jobs:
                | customer     | detail       | stage | truck   |
                | testcustomer | test details | 2     | SER2015 |
            And I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     | costPrice | carriagePrice |
                | stock item  | SI1  | 7   | 14    | ABC supplies | 5.00      | 1.00          |
            And I have the following stockItems:
                | stock      | count |
                | stock item | 1     |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Live Jobs" column
            And I click the "Create PO" button in the "job-particulars" section
            And I replace "_search" with "stock"
            And I wait 1 second
            And I click the "Add Special Part" button in the "stockItems" section
            And I fill in the following:
                |  purchase_order_form_estimateItems_0_item      |  special item  |
                |  purchase_order_form_estimateItems_0_price     |  50            |
                |  purchase_order_form_estimateItems_0_quantity  |  1             |
            And I press "Save"
            And I should see "testcustomer" in the "Parts Required" column
        Then I should see "Purchase Orders" in the "job-particulars" section

    @javascript
    Scenario: Create a purchase order which is ordered on creation for a live job
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following trucks:
                | make        | model  | serial  | yom  | customer     | siteAddress |
                | Caterpillar | CAT123 | SER2015 | 2015 | testcustomer | Main site   |
            And I have the following jobs:
                | customer     | detail       | stage | orderDate | truck   |
                | testcustomer | test details | 2     | now       | SER2015 |
            And I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     | costPrice | carriagePrice |
                | stock item  | SI1  | 7   | 14    | ABC supplies | 5.00      | 1.00          |
            And I have the following stockItems:
                | stock      | count |
                | stock item | 1     |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Live Jobs" column
            And I click the "Create PO" button in the "job-particulars" section
            And I replace "_search" with "stock"
            And I wait 1 second
            And I click the "Add Special Part" button in the "stockItems" section
            And I fill in the following:
                |  purchase_order_form_estimateItems_0_item      |  special item  |
                |  purchase_order_form_estimateItems_0_price     |  50            |
                |  purchase_order_form_estimateItems_0_quantity  |  1             |
            And I press "Save & Order"
            And I wait for the "job" particulars section to appear
        Then I should see "Purchase Orders" in the "job-particulars" section

    @javascript
    Scenario: Postpone a job
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage |
                | testcustomer | test details | 2     |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Live Jobs" column
            And I click the "Postpone" button in the "job-particulars" section
            And I follow "Confirm"
        Then I should see "testcustomer" in the "Jobs to assign" column

    @javascript
    Scenario: Complete a job
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following trucks:
                | make        | model  | serial  | yom  | customer     | siteAddress |
                | Caterpillar | CAT123 | SER2015 | 2015 | testcustomer | Main site   |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | truck   | site      |
                | testcustomer | test details | 2     | engineer | SER2015 | Main site |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Live Jobs" column
            And I click the "Complete" button in the "job-particulars" section
            And I fill in the following:
                | add_worklog_form_description | Was broken |
            And I select "1" from "add_worklog_form_timeLogged_hour"
            And I press "Save"
            And I click the "Add Detail" button in the "possibleFutureWorkForm" section
            And I fill in the following:
                | Task | Blah joints need replaced |
            And I press "Save"
        Then I should see "testcustomer" in the "completed" column as the colour "red"
            And I should see "Blah joints need replaced"

    @javascript
    Scenario: Complete a job after logging time on a job
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following trucks:
                | make        | model  | serial  | yom  | customer     | siteAddress |
                | Caterpillar | CAT123 | SER2015 | 2015 | testcustomer | Main site   |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | truck   | site      |
                | testcustomer | test details | 2     | engineer | SER2015 | Main site |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Live Jobs" column
            And I click the "Log work" button in the "job-particulars" section
            And I fill in the following:
                |  add_worklog_form_description |  Lots of work  |
            And I select "1" from "add_worklog_form_timeLogged_hour"
            And I select "30" from "add_worklog_form_timeLogged_minute"
            And I check "add_worklog_form_completeJob"
            And I press "Save"
            And I click the "Add Detail" button in the "possibleFutureWorkForm" section
            And I fill in the following:
                | Task | Blah joints need replaced |
            And I press "Save"
        Then I should see "testcustomer" in the "completed" column as the colour "red"
            And I should see "Blah joints need replaced"

    @javascript
    Scenario: Log work on a job
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | worklog_desc | worklog_time | engineer |
                | testcustomer | test details | 2     | Lots of work | 60           | engineer |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Live Jobs" column
            And I click the "Log work" button in the "job-particulars" section
            And I fill in the following:
                | add_worklog_form_description | Lots of work |
            And I select "30" from "add_worklog_form_timeLogged_minute"
            And I press "Save"
            And I should see "testcustomer" in the "Live Jobs" column
        Then I should see "1h 30m" in the "job-particulars" section
            And I should see "£75.00"

    @javascript
    Scenario: Signoff on a job
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following trucks:
                | make        | model  | serial  | yom  | customer     | siteAddress |
                | Caterpillar | CAT123 | SER2015 | 2015 | testcustomer | Main site   |
            And I have the following jobs:
                | customer     | detail       | stage | truck   |
                | testcustomer | test details | 6     | SER2015 |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Completed" column
            And I click the "Sign off" button in the "job-particulars" section
            And I follow "Confirm"
        Then I should not see "testcustomer" in the "completed" column

    @javascript
    Scenario: Raise and estimate on a job
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage |
                | testcustomer | test details | 2     |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Live Jobs" column
            And I click the "Estimate Req" button in the "job-particulars" section
            And I fill in the following:
                |  estimate_form_details  |  test details  |
            And I press "Save"
            And I should see "testcustomer" in the "Estimate Pending" column
        Then I should see "test details" in the "job-particulars" section

    @javascript
    Scenario: Assign stock to a job
        Given I have the following customers:
                | name        |siteAddress_name|siteAddress_address1 |siteAddress_address2|siteAddress_city|siteAddress_postcode|stockLocation |
                | testcustomer|Main site       |100 Bell's Theorm Way|ECIT                |Belfast         |BT3 9DT             |true          |
            And I have the following jobs:
                | customer     | detail       | stage | engineer |
                | testcustomer | test details | 2     | engineer |
            And I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     | costPrice | carriagePrice |
                | stock item  | SI1  | 7   | 14    | ABC supplies | 5.00      | 1.00          |
            And I have the following locations:
                | siteAddress_name  |
                | Main    |
            And I have the following stockItems:
                | stock      | count | location |
                | stock item | 1     | Main     |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Live Jobs" column
            And I click the "Assign Stock" button in the "job-particulars" section
            And I replace "_search" with "stock"
            And I wait 1 second
            And I click the "Add" button in the "search-results" section
            And I select "Main" from "Location"
            And I press "Save"
            And I wait for the "job" particulars section to appear
        Then I should see "stock item" in the "job-particulars" section
            And I should see "testcustomer" in the "Live Jobs" column
            And I go to the view page for the "stock item" stock item
            And I should see "1" and "OUT - Job" in a transaction row

    @javascript
    Scenario: Order a Purchase order which has already been created
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | engineer |
                | testcustomer | test details | 2     | engineer |
            And I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following purchaseOrders:
                | customer     | description     | specialItem_item | specialItem_price | specialItem_qty | job   |
                | testcustomer | Purchase Orders | special item     | 1.00              | 1               | 80001 |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Live Jobs" column
            And I click the "Purchase Orders" button in the "job-particulars" section
            And I click the "Order" button in the "top" section
            And I press "Save"
            And I go to "/jobs"
        Then I should see "testcustomer" in the "Part on Order" column

    @javascript
    Scenario: Check in a Purchase order which has already been ordered
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | engineer |
                | testcustomer | test details | 2     | engineer |
            And I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following purchaseOrders:
                | customer     | description     | specialItem_item | specialItem_price | specialItem_qty | job   | orderDate | specialItem_partNumber |
                | testcustomer | Purchase Orders | special item     | 1.00              | 1               | 80001 | now       | PN-1                   |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Live Jobs" column
            And I click the "Purchase Orders" button in the "job-particulars" section
            And I press "Check in item"
            And I focus "estimate_item_form_receivedDate"
            And I press "Save"
            And I go to "/jobs"
        Then I should see "testcustomer" in the "Live jobs" column

    @javascript
    Scenario: Delete a job
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | engineer |
                | testcustomer | test details | 2     | engineer |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Live Jobs" column
            And I click the "View" button in the "job-particulars" section
            And I follow "Delete"
            And I press "Delete" on the current modal
        And I should not see "testcustomer"

    @javascript
    Scenario: Create a new customer on the new job form
        Given I go to "/jobs"
            And I follow "New"
            And I click "createNewCustomer"
            And I fill in the following:
                | customer_form_name                    | testcustomer   |
                | customer_form_billingAddress_address1 | 1 American Way |
                | customer_form_billingAddress_address2 | Brooklyn       |
                | customer_form_billingAddress_city     | New York       |
                | customer_form_billingAddress_postcode | NY01 1NY       |
            And I press "Save" on the top modal
            And I fill in the following:
                | job_form_detail | test job |
            And I press "Save" on the top modal
        Then the column "Jobs to assign" should contain "testcustomer"

    @javascript
    Scenario: Check a job edit form has the truck information
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following trucks:
                | make        | model  | serial  | yom  | customer     |
                | Caterpillar | CAT123 | SER2015 | 2015 | testcustomer |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | truck   |
                | testcustomer | test details | 2     | engineer | SER2015 |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Live Jobs" column
            And I click the "Edit" button in the "job-particulars" section
        Then I should see "SER2015" in the "jobInfo" section

    @javascript
    Scenario: Check all trucks show on customer selection
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | billingeAddress_address1  | billingAddress_address2 | billingAddress_city | billingAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | 5 Sydenham Road               | Harbour Estate          | Belfast             | BT14 1HS                |
                | Customer A   | | | | | | | | | |
            And I have the following trucks:
                | customer     | make        | model  | serial  | yom  | siteAddress |
                | testcustomer | Caterpillar | CAT123 | SER2015 | 2015 | Main site   |
                | testcustomer | Combilift   | CMB789 | SER2017 | 2015 |             |
        When I go to "/jobs"
            And I follow "New"
            And I select "testcustomer" from "job_form_customer"
            And I fill in "Combilift" for "job_form_asset"
            And I wait for the autocomplete results to appear
        Then I should see "Combilift CMB789 (SER2017)"

    @javascript
    Scenario: Check trucks filter on site address change
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |billingeAddress_address1  | billingAddress_address2 | billingAddress_city | billingAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT | 5 Sydenham Road | Harbour Estate | Belfast | BT14 1HS |
                | Customer A   | | | | | | | | | |
            And I have the following trucks:
                | customer     | make        | model  | serial  | yom  | siteAddress |
                | testcustomer | Caterpillar | CAT123 | SER2015 | 2015 | Main site   |
                | testcustomer | Combilift   | CMB789 | SER2017 | 2015 |             |
        When I go to "/jobs"
            And I follow "New"
            And I select "testcustomer" from "job_form_customer"
            And I select "Main site, 100 Bell's Theorm Way, ECIT, Belfast, BT3 9DT." from "job_form_siteAddress"
        Then I should not see option "Combilift" in selector "job_form_make"

    @javascript
    Scenario: Assign stock to a completed job
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following trucks:
                | make        | model  | serial  | yom  | customer     | siteAddress |
                | Caterpillar | CAT123 | SER2015 | 2015 | testcustomer | Main site   |
            And I have the following jobs:
                | customer     | detail       | stage | signedOff | truck   |
                | testcustomer | test details | 6     | 0         | SER2015 |
            And I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     | costPrice | carriagePrice |
                | stock item  | SI1  | 7   | 14    | ABC supplies | 5.00      | 1.00          |
            And I have the following locations:
                | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | Main warehouse   | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following stockItems:
                | stock      | count | location       |
                | stock item | 1     | Main warehouse |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Completed" column
            And I click the "Assign Stock" button in the "job-particulars" section
            And I replace "_search" with "stock"
            And I wait 1 second
            And I click the "Add" button in the "search-results" section
            And I select "Main warehouse" from "Location"
            And I press "Save"
            And I wait for the "job" particulars section to appear
        Then I should see "SI1" in the "stockTable" section

    @javascript
    Scenario: Check trucks are loading correctly on the new job form
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following trucks:
                | make        | model  | serial  | yom  | customer     |
                | Caterpillar | CAT123 | SER2015 | 2015 | testcustomer |
        When I go to "/jobs"
            And I follow "new-item"
            And I select "testcustomer" from "job_form_customer"
            And I fill in the following:
                |  job_form_detail  |  test job  |
            And I fill in "SER2015" for "job_form_asset"
            And I wait for the autocomplete results to appear
            And I click "Caterpillar CAT123 (SER2015)"
            And I press "Save"
        Then the column "Jobs to assign" should contain "testcustomer"
            And I should see "has been successfully added"

    @javascript
    Scenario: Check trucks are loading correctly on the edit job form
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following trucks:
                | make        | model  | serial  | yom  | customer     | siteAddress |
                | Caterpillar | CAT123 | SER2015 | 2015 | testcustomer | Main site   |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | truck   | site      |
                | testcustomer | test details | 2     | engineer | SER2015 | Main site |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Live Jobs" column
            And I click the "Edit" button in the "job-particulars" section
            And I click "Change"
            And I fill in "CAT123" for "job_form_asset"
            And I wait for the autocomplete results to appear
        Then I should see "Caterpillar CAT123 (SER2015)"

    @javascript
    Scenario: Test complete job form validation works
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | worklog_desc | worklog_time | engineer |
                | testcustomer | test details | 2     | Lots of work | 60           | engineer |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Live Jobs" column
            And I click the "Complete" button in the "job-particulars" section
            # First modal
            And I press "Save"
            # Second modal
            And I press "Save"

    @javascript
    Scenario: Complete a job
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | site      | worklog_desc | worklog_time |
                | testcustomer | test details | 2     | engineer | Main site | Lots of work | 60           |
            And I have the following trucks:
                | make        | model  | serial  | yom  | customer     | siteAddress |
                | Caterpillar | CAT123 | SER2015 | 2015 | testcustomer | Main site   |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Live Jobs" column
            And I click the "Complete" button in the "job-particulars" section
            And I press "Save"
            And I fill in "SER2015" for "complete_job_form_asset"
            And I wait for the autocomplete results to appear
            And I click "Caterpillar CAT123 (SER2015)"
            And I press "Save"
            And I click the "Add Detail" button in the "possibleFutureWorkForm" section
            And I fill in the following:
                | Task | Blah joints need replaced |
            And I press "Save"
        Then I should see "testcustomer" in the "completed" column as the colour "red"
            And I should see "Blah joints need replaced"

    @javascript
    Scenario: Add photos to a job
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | site      |
                | testcustomer | test details | 2     | engineer | Main site |
            And I have the following trucks:
                | make        | model  | serial  | yom  | customer     | siteAddress |
                | Caterpillar | CAT123 | SER2015 | 2015 | testcustomer | Main site   |
            When I go to "/jobs"
            And I click on "testcustomer" in the "Live Jobs" column
            And I click the "Add Image" button in the "job-particulars" section
            And I click the "Add new" button in the "images" section
            And I attach the file "causeway.jpg" to "job_images_form_images_0_image"
            And I press "Save"
        Then I should see "Images"

    @javascript
    Scenario: Assign stock to a live job and check location is the engineer
        Given I have the following customers:
                | name         |
                | testcustomer |
            And I have the following trucks:
                | make        | model  | serial  | yom  | customer     |
                | Caterpillar | CAT123 | SER2015 | 2015 | testcustomer |
            And I have the following jobs:
                | customer     | detail       | stage | signedOff | engineer |
                | testcustomer | test details | 2     | 0         | engineer |
            And I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     | costPrice | carriagePrice |
                | stock item  | SI1  | 7   | 14    | ABC supplies | 5.00      | 1.00          |
            And I have the following locations:
                | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | Main warehouse   | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following stockItems:
                | stock      | count | location       |
                | stock item | 1     | Main warehouse |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Live Jobs" column
            And I click the "Assign Stock" button in the "job-particulars" section
            And I replace "_search" with "stock"
            And I wait for the search results to appear
            And I click the "Add" button in the "search-results" section
            And I select "Main warehouse" from "assign_stock_to_job_form_attachedStock_0_location"
            And I press "Save"
            And I wait for the "job" particulars section to appear
        Then I should see "SI1" in the "stockTable" section

    @javascript
    Scenario: Add a new contact to a job
        Given I have the following customers:
                | name         | billingAddress_name | billingAddress_address1  | billingAddress_address2 | billingAddress_city | billingAddress_postcode |
                | testcustomer | Main site           | 100 Bell's Theorm Way     | ECIT                   | Belfast             | BT3 9DT                 |
            And I have the following trucks:
                | customer     | make        | model  | serial  | yom  |
                | testcustomer | Caterpillar | CAT123 | SER2015 | 2015 |
           And I have the following contacts:
                | name           | telephone | email            | customer     |
                | first contact  | 123456789 | test@contact.com | testcustomer |
                | second contact | 123456789 | test@contact.com | testcustomer |
        When I go to "/jobs"
            And I follow "new-item"
            And I select "testcustomer" from "Customer"
            And I fill in "SER2015" for "job_form_asset"
            And I wait for the autocomplete results to appear
            And I click "Caterpillar CAT123 (SER2015)"
            And I follow "Add a new contact"
            And I fill in the following:
                | job_form_newContacts_0_name      |  Rick Grimes     |
                | job_form_newContacts_0_telephone |  123456789       |
                | job_form_newContacts_0_email     |  rick@grimes.com |
            And I press "Save"
            And I go to the view page for the "80000" job
        Then I should see "Rick Grimes"

    @javascript
    Scenario: Select an existing contact for a job
        Given I have the following customers:
                | name         | billingAddress_name | billingAddress_address1  | billingAddress_address2 | billingAddress_city | billingAddress_postcode |
                | testcustomer | Main site           | 100 Bell's Theorm Way     | ECIT                   | Belfast             | BT3 9DT                 |
            And I have the following trucks:
                | customer     | make        | model  | serial  | yom  |
                | testcustomer | Caterpillar | CAT123 | SER2015 | 2015 |
           And I have the following contacts:
                | name           | telephone | email            | customer     |
                | first contact  | 123456789 | test@contact.com | testcustomer |
                | second contact | 123456789 | test@contact.com | testcustomer |
        When I go to "/jobs"
            And I follow "new-item"
            And I select "testcustomer" from "Customer"
            And I fill in "SER2015" for "job_form_asset"
            And I wait for the autocomplete results to appear
            And I click "Caterpillar CAT123 (SER2015)"
            And I check "first contact"
            And I press "Save"
            And I go to the view page for the "80000" job
        Then I should see "first contact"

    @javascript
    Scenario: Select an existing customer contact for a job when one is already connected to a truck
        Given I have the following customers:
                | name         | billingAddress_name | billingAddress_address1  | billingAddress_address2 | billingAddress_city | billingAddress_postcode |
                | testcustomer | Main site           | 100 Bell's Theorm Way     | ECIT                   | Belfast             | BT3 9DT                 |
            And I have the following trucks:
                | customer     | make        | model  | serial  | yom  |
                | testcustomer | Caterpillar | CAT123 | SER2015 | 2015 |
           And I have the following contacts:
                | name           | telephone | email            | truck   | customer     |
                | first contact  | 123456789 | test@contact.com | SER2015 | testcustomer |
                | second contact | 123456789 | test@contact.com |         | testcustomer |
        When I go to "/jobs"
            And I follow "new-item"
            And I select "testcustomer" from "Customer"
            And I fill in "SER2015" for "job_form_asset"
            And I wait for the autocomplete results to appear
            And I click "Caterpillar CAT123 (SER2015)"
            And I check "second contact"
            And I press "Save"
            And I go to the view page for the "80000" job
        Then I should see "first contact"
            And I should see "second contact"

    @javascript
    Scenario: Find a job using the search by truck page
    Given I have the following customers:
                | name         |
                | testcustomer |
            And I have the following trucks:
                | customer     | make        | model  | serial  | yom  |
                | testcustomer | Caterpillar | CAT123 | SER2015 | 2015 |
            And I have the following jobs:
                | customer     | detail       | stage | signedOff | engineer | truck   |
                | testcustomer | test details | 2     | 0         | engineer | SER2015 |
        When I go to "/jobs/trucks"
            And I click on "SER2015" in the "Serial Number" column
            And I wait for the "job" particulars section to appear
        Then I should see "test details"

    @javascript
    Scenario: Find a job using the search by truck page
        Given I have the following customers:
            | name         |
            | testcustomer |
        And I have the following trucks:
            | customer     | make        | model  | serial  | yom  |
            | testcustomer | Caterpillar | CAT123 | SER2015 | 2015 |
            | testcustomer | Totyota     | TOY345 | SER1985 | 1985 |
        And I have the following jobs:
            | customer     | detail       | stage | signedOff | engineer | truck   |
            | testcustomer | test details | 2     | 0         | engineer | SER2015 |
        When I go to "/jobs/trucks"
            And I click on "SER1985" in the "Serial Number" column
        Then I should see "No Job History"

    @javascript
    Scenario: Check that selecting breakdown changes form text
        Given I have the following customers:
            | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
            | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
        When I go to "/jobs"
            And I follow "new-item"
            And I check "Breakdown"
        Then I should see "Fault reported"

    @javascript
    Scenario: Check that selecting LOLER sets the job detail
        Given I have the following customers:
            | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
            | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
        When I go to "/jobs"
            And I follow "new-item"
            And I check "LOLER"
        Then I should see "LOLER inspection"

    @javascript
    Scenario: Check that selecting Service sets the job detail
        Given I have the following customers:
            | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
            | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
        When I go to "/jobs"
            And I follow "new-item"
            And I check "Service"
        Then I should see "Routine Service"

    @javascript
    Scenario: Check job diagnostic on work log appears on complete job form
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | worklog_desc | worklog_time | engineer |
                | testcustomer | test details | 2     | Lots of work | 60           | engineer |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Live Jobs" column
            And I click the "Log work" button in the "job-particulars" section
            And I fill in the following:
                |  add_worklog_form_description  |  Lots of work  |
            And I select "1" from "add_worklog_form_timeLogged_hour"
            And I select "30" from "add_worklog_form_timeLogged_minute"
            And I press "Save"
            And I click on "testcustomer" in the "Live Jobs" column
            And I wait 1 second
            And I click the "Complete" button in the "job-particulars" section
        Then I should see "Lots of work"

    @javascript
    Scenario: Complete a job and choose which parts have been used on job
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following trucks:
                | make        | model  | serial  | yom  | customer     | siteAddress |
                | Caterpillar | CAT123 | SER2015 | 2015 | testcustomer | Main site   |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | truck   | site      | worklog_desc | worklog_time |
                | testcustomer | test details | 2     | engineer | SER2015 | Main site | Lots of work | 60           |
            And I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     | costPrice | carriagePrice |
                | stock item  | SI1  | 7   | 14    | ABC supplies | 5.00      | 1.00          |
            And I have the following stockItems:
                | stock      | count | job          |
                | stock item | 2     | test details |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Live Jobs" column
            And I click the "Complete" button in the "job-particulars" section
            And I press "Save"
            And I uncheck "set_stock_used_on_job_form_attachedStockItems_1_usedOnJob"
            And I press "Save"
            And I press "Save"
            #First modal
            And I press "Save"
            #Second modal
            And I press "Save"
            And I go to the view page for the "stock item" stock item
        Then I should see "1" beside "Stock Quantity" in the "description-section" section

    @javascript
    Scenario: Request parts for a job
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail  | stage || next_site_visit | do_with_job |
                | testcustomer | job one | 2     || false           |             |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Live Jobs" column
            And I click the "Log work" button in the "job-particulars" section
            And I fill in "Did work" for "Diagnostics"
            And I select "1" from "Hours"
            And I check "Requires parts"
            And I fill in "Required part for the job" for "Requested Parts"
            And I press "Save"
            And I wait for the "job" particulars section to appear
        Then I should see "Required part for the job"

    @javascript
    Scenario: Create a purchase order which will be collected by an engineer
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following trucks:
                | make        | model  | serial  | yom  | customer     | siteAddress |
                | Caterpillar | CAT123 | SER2015 | 2015 | testcustomer | Main site   |
            And I have the following jobs:
                | customer     | detail       | stage | truck   |
                | testcustomer | test details | 2     | SER2015 |
            And I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     | costPrice | carriagePrice |
                | stock item  | SI1  | 7   | 14    | ABC supplies | 5.00      | 1.00          |
            And I have the following stockItems:
                | stock      | count |
                | stock item | 1     |
        When I go to "/jobs"
            And I click on "testcustomer" in the "Live Jobs" column
            And I click the "Create PO" button in the "job-particulars" section
            And I select "Engineer" from "purchase_order_form_engineerCollection"
            And I click the "Add Special Part" button in the "stockItems" section
            And I fill in the following:
                |  purchase_order_form_estimateItems_0_item      |  special item  |
                |  purchase_order_form_estimateItems_0_price     |  50            |
                |  purchase_order_form_estimateItems_0_quantity  |  1             |
            And I press "Save"
            And I click "Confirm"
            And I should see "testcustomer" in the "Live Jobs" column
        Then I should see "Purchase Orders" in the "job-particulars" section
