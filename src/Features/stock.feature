Feature: Testing out stock functionality
    Add an item of stock, add a supplier to an item of stock, add item(s) of stock to the system from a supplier, assign stock to a job

    Background:
        Given I am authenticated as "megaadmin"

    @javascript
    Scenario: Add an item of stock
        Given I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I go to "/stock/list"
            And I follow "new-item"
            And I fill in the following:
                | stock_form_description | stock item |
                | stock_form_code        | SI1        |
                | stock_form_oemPrice    | 4          |
            And I press "Save"
            And I fill in the following:
                | filter | stock item |
            And I wait for the "stock" particulars section to appear
        Then I should see "SI1 - stock item"

    @javascript
    Scenario: Add item(s) of stock to the system from a supplier
        Given I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     | costPrice | carriagePrice |
                | stock item  | SI1  | 7   | 14    | ABC supplies | 5.00      | 1.00          |
           And I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | internal     | test site        | test address 1        | test address 2       | test city        | BT3 9DT              |
            And I have the following locations:
                | siteAddress_name  |
                | Main warehouse    |
            And I have the following stockItems:
                | stock      | count  | location       |
                | stock item | 5      | Main warehouse |
        When I go to "/stock/list?selectedItem=1"
            And I click the "Add stock" button in the "stock-particulars" section
            And I select "ABC supplies" from "add_stock_form_supplierStock"
            And I fill in "15" for "Quantity"
            And I select "Main warehouse" from "Location"
            And I press "Save"
            And I fill in "Bin 1" for "Bin"
            And I press "Save"
            And I should see "20 in Main warehouse" in the "stock-particulars" section
            And I go to the view page for the "stock item" stock item
        Then I should see "15" and "IN - Manual Entry" in a transaction row

    @javascript
    Scenario: Assign stock to a job
        Given I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     | costPrice | carriagePrice |
                | stock item  | SI1  | 7   | 14    | ABC supplies | 5.00      | 1.00          |
            And I have the following customers:
                | name         |
                | testcustomer |
           And I have the following jobs:
                | customer     | detail       | stage | jobCode |
                | testcustomer | test details | 1     | 80000   |
            And I have the following locations:
                | siteAddress_name  |
                | Main warehouse    |
            And I have the following stockItems:
                | stock      | count  | location       |
                | stock item | 5      | Main warehouse |
        When I go to "/stock/list?selectedItem=1"
            And I follow "Issue Stock"
            And I select "Main warehouse." from "issue_stock_to_job_form_location"
            And I select "80000 - testcustomer" from "issue_stock_to_job_form_job"
            And I select "3" from "issue_stock_to_job_form_quantity"
            And I press "Save"
            And I should see "5" beside "Quantity in stock" in the "stock-particulars" section
            And I should see "2 in Main warehouse" in the "stock-particulars" section
            And I should see "3 on 80000" in the "stock-particulars" section
            And I go to the view page for the "stock item" stock item
        Then I should see "3" and "OUT - Job" in a transaction row

    @javascript
    Scenario: Check filter results return for a 3 character string on the listing page
        Given I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     | costPrice | carriagePrice |
                | stock item  | SI1  | 7   | 14    | ABC supplies | 5.00      | 1.00          |
        When I go to "/stock/list"
            And I fill in the following:
                | filter | sto |
            And I wait for the "stock" particulars section to appear
        Then I should see "SI1 - stock item"

    @javascript
    Scenario: Check filter results return for a 3 character string on the listing page
        Given I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     | costPrice | carriagePrice |
                | stock item  | SI1  | 7   | 14    | ABC supplies | 5.00      | 1.00          |
            And I have the following locations:
                | siteAddress_name  |
                | Main warehouse    |
            And I have the following stockItems:
                | stock      | count  | location          |
                | stock item | 5      | Main warehouse    |
        When I go to "/stock/list"
            And I fill in the following:
                | filter | sto |
            And I wait for the "stock" particulars section to appear
        Then I should see "SI1 - stock item"

    @javascript
    Scenario: Check job locations are showing correctly
        Given I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     | costPrice | carriagePrice |
                | stock item  | SI1  | 7   | 14    | ABC supplies | 5.00      | 1.00          |
            And I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage |
                | testcustomer | test details | 1     |
            And I have the following locations:
                | siteAddress_name  |
                | Main warehouse    |
                | Secondary storage |
            And I have the following stockItems:
                | stock      | count  | location          | job          |
                | stock item | 5      | Main warehouse    |              |
                | stock item | 1      | Secondary storage |              |
                | stock item | 1      |                   | test details |
        When I go to "/stock/list?selectedItem=1"
        Then I should see "5 in Main warehouse"
            And I should see "1 in Secondary Storage"
            And I should see "1 on 80001 (testcustomer)"

    @javascript
    Scenario: Check a deleted item of stock doesn't show up in search results
        Given I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     | costPrice | carriagePrice | deletedAt |
                | stock item  | SI1  | 7   | 14    | ABC supplies | 5.00      | 1.00          | yes      |
            And I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage |
                | testcustomer | test details | 1     |
        When I go to "/stock/list"
            And I fill in the following:
                | filter | sto |
        Then I should not see "SI1 - stock item"

    @javascript
    Scenario: Add a supplier for an item of stock
        Given I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
                | XYZ supplies | XYZ1 | 123456789 | 987654321 | xyz@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     |
                | stock item  | SI1  | 7   | 14    | ABC supplies |
            And I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage |
                | testcustomer | test details | 1     |
       When I go to "/stock/list?selectedItem=1"
            And I click the "Add a Supplier" button in the "stock-particulars" section
            And I fill in the following:
                | Code        | ALT1 |
                | Cost price  | 20   |
                | Issue price | 30   |
            And I press "Save"
            # Select prefered supplier on subsequent modal
            And I press "Save"
            And I go to the view page for the "stock item" stock item
        Then I should see "ALT1"

    @javascript
    Scenario: Issue stock to a job
        Given I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     |
                | stock item  | SI1  | 7   | 14    | ABC supplies |
            And I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | jobCode | engineer |
                | testcustomer | test details | 2     | 80000   | engineer |
            And I have the following locations:
                | siteAddress_name  |
                | Main warehouse    |
                | Secondary storage |
            And I have the following stockItems:
                | stock      | count  | location          |
                | stock item | 5      | Main warehouse    |
                | stock item | 1      | Secondary storage |
       When I go to "/stock/list?selectedItem=1"
            And I click the "Issue Stock" button in the "stock-particulars" section
            And I select "Secondary storage" from "Location"
            And I select "1" from "Quantity"
            And I select "80000 - testcustomer" from "Job"
            And I press "Save"
            And I go to the view page for the "stock item" stock item
        Then I should see "1" and "OUT - Job" in a transaction row

    @javascript
    Scenario: Issue stock to a job
        Given I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     |
                | stock item  | SI1  | 7   | 14    | ABC supplies |
            And I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | jobCode |
                | testcustomer | test details | 1     | 80000   |
            And I have the following locations:
                | siteAddress_name  |
                | Main warehouse    |
                | Secondary storage |
            And I have the following stockItems:
                | stock      | count  | location          |
                | stock item | 5      | Main warehouse    |
                | stock item | 1      | Secondary storage |
       When I go to "/stock/list?selectedItem=1"
            And I click the "Manually Set Stock Level" button in the "stock-particulars" section
            And I select "Secondary storage" from "Location"
            And I select "0" from "Quantity"
            And I press "Save"
            And I go to the view page for the "stock item" stock item
        Then I should not see "1 in Secondary storage" in the "stockLevels" section

    @javascript
    Scenario: Manually set stock level
        Given I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     |
                | stock item  | SI1  | 7   | 14    | ABC supplies |
            And I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | jobCode |
                | testcustomer | test details | 1     | 80000   |
            And I have the following locations:
                | siteAddress_name  |
                | Main warehouse    |
                | Secondary storage |
            And I have the following stockItems:
                | stock      | count  | location          |
                | stock item | 5      | Main warehouse    |
                | stock item | 1      | Secondary storage |
       When I go to "/stock/list?selectedItem=1"
            And I click the "Manually Set Stock Level" button in the "stock-particulars" section
            And I select "Secondary storage" from "Location"
            And I select "10" from "Quantity"
            And I press "Save"
            And I go to the view page for the "stock item" stock item
        Then I should see "9" and "IN - Manual Entry" in a transaction row

    @javascript
    Scenario: Manually set the stock level so it does not change
        Given I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     |
                | stock item  | SI1  | 7   | 14    | ABC supplies |
            And I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | jobCode |
                | testcustomer | test details | 1     | 80000   |
            And I have the following locations:
                | siteAddress_name  |
                | Main warehouse    |
                | Secondary storage |
            And I have the following stockItems:
                | stock      | count  | location          |
                | stock item | 5      | Main warehouse    |
                | stock item | 1      | Secondary storage |
       When I go to "/stock/list?selectedItem=1"
            And I click the "Manually Set Stock Level" button in the "stock-particulars" section
            And I select "Secondary storage" from "Location"
            And I select "1" from "Quantity"
            And I press "Save"
        Then I should see "1 in Secondary storage" in the "stock-particulars" section


    @javascript
    Scenario: Supercede a stock item
        Given I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     | costPrice | carriagePrice |
                | stock item  | SI1  | 7   | 14    | ABC supplies | 5.00      | 1.00          |
            And I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | jobCode |
                | testcustomer | test details | 1     | 80000   |
       When I go to "/stock/list?selectedItem=1"
            And I click the "Supercede" button in the "stock-particulars" section
            And I fill in the following:
                | Code | blah |
            And I press "Save"
        Then I should see "Part Number has been superceded by blah - stock item"

    @javascript
    Scenario: Issue stock to a job and return it from job view page
        Given I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     |
                | stock item  | SI1  | 7   | 14    | ABC supplies |
            And I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | jobCode | engineer |
                | testcustomer | test details | 2     | 80000   | engineer |
            And I have the following locations:
                | siteAddress_name  |
                | Main warehouse    |
            And I have the following stockItems:
                | stock      | count  | location          | job          |
                | stock item | 1      | Main warehouse    | test details |
       When I go to the view page for the "80000" job
            And I click "Return stock"
        Then I should not see "Return stock"
            And I should see "has been successfully returned from Job 80000 - testcustomer"

    @javascript
    Scenario: Edit stock
        Given I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     | costPrice | carriagePrice |
                | stock item  | SI1  | 7   | 14    | ABC supplies | 5.00      | 1.00          |
        When I go to "/stock/list?selectedItem=1"
            And I click the "Edit" button in the "stock-particulars" section
            And I fill in the following:
                | OEM price | 2.42 |
            And I press "Save"
            And I go to the view page for the "stock item" stock item
        Then I should see "£2.42"

    @javascript
    Scenario: Add a second supplier for an item of stock and set preferred supplier
        Given I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
                | 123 supplier | OTT1 | 123123123 | 321321321 | 123@supplier.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     | costPrice | carriagePrice |
                | stock item  | SI1  | 7   | 14    | ABC supplies | 50        | 0             |
            And I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage |
                | testcustomer | test details | 1     |
       When I go to "/stock/list?selectedItem=1"
            And I click the "Add a Supplier" button in the "stock-particulars" section
            And I select "123 supplier" from "supplier_stock_form_supplier"
            And I fill in the following:
                | Code        | ALT1 |
                | Cost price  | 20   |
                | Issue price | 30   |
            And I press "Save"
            And I select "123 supplier" from "stock_preferred_supplier_form_preferredSupplier"
            And I press "Save"
            And I go to the view page for the "stock item" stock item
        Then I should see "Preferred supplier is 123 supplier"

    @javascript
    Scenario: Check search term persists
        Given I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     | costPrice | carriagePrice |
                | stock item  | SI1  | 7   | 14    | ABC supplies | 5.00      | 1.00          |
            And I have the following customers:
                | name         |
                | testcustomer |
           And I have the following jobs:
                | customer     | detail       | stage | jobCode |
                | testcustomer | test details | 1     | 80000   |
            And I have the following locations:
                | siteAddress_name  |
                | Main warehouse    |
            And I have the following stockItems:
                | stock      | count  | location       |
                | stock item | 5      | Main warehouse |
        When I go to "/stock/list?selectedItem=1"
            And I click the "Add stock" button in the "stock-particulars" section
            And I fill in the following:
                | add_stock_form_quantity | 5 |
            And I press "Save"
            And I press "Close" on the current modal
        Then I should see "stock item"

    @javascript
    Scenario: Add an item of stock with a preferred supplier (bugfix)
        Given I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I go to "/stock/list"
            And I follow "new-item"
            And I fill in the following:
                | stock_form_description | stock item |
                | stock_form_code        | SI1        |
                | stock_form_oemPrice    | 4          |
            And I select "ABC supplies" from "Supplier"
            And I press "Save"
            And I fill in the following:
                | Code        | ALT1 |
                | Issue price | 30   |
                | Cost price  | 20   |
            And I press "Save"
            And I go to "/stock/list?selectedItem=1"
            And I click the "Edit" button in the "stock-particulars" section
        Then I should see "ABC supplies"
