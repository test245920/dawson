Feature: Testing out purchase order functionality
    Add a PO, delete a PO.

    Background:
        Given I am authenticated as "megaadmin"

    @javascript
    Scenario: Create a purchase order which is not ordered on creation
        Given I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     | costPrice | carriagePrice |
                | stock item  | SI1  | 7   | 14    | ABC supplies | 5.00      | 1.00          |
            And I have the following locations:
                | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | Main warehouse   | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following stockItems:
                | stock      | count | location       |
                | stock item | 1     | Main warehouse |
        When I go to "/purchaseorders"
            And I follow "New Stock Order"
            And I click the "Add Special Part" button in the "stockItems" section
            And I fill in the following:
                | purchase_order_form_estimateItems_0_item     | special item |
                | purchase_order_form_estimateItems_0_price    | 1.00         |
                | purchase_order_form_estimateItems_0_quantity | 1            |
            And I fill in the following:
                | _search | stock |
            And I press "Save"
        Then the column "Incomplete" should contain "Stock Order"

    @javascript
    Scenario: Create a purchase order which is ordered on creation
        Given I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     | costPrice | carriagePrice |
                | stock item  | SI1  | 7   | 14    | ABC supplies | 5.00      | 1.00          |
            And I have the following locations:
                | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | Main warehouse   | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following stockItems:
                | stock      | count | location       |
                | stock item | 1     | Main warehouse |
        When I go to "/purchaseorders"
            And I follow "New Stock Order"
            And I press "Order"
            And I focus "purchase_order_form_orderDate"
            And I fill in "Order" for "Order Description"
            And I click the "Add Special Part" button in the "stockItems" section
            And I fill in the following:
                | purchase_order_form_estimateItems_0_item     | special item |
                | purchase_order_form_estimateItems_0_price    | 1.00         |
                | purchase_order_form_estimateItems_0_quantity | 1            |
            And I fill in the following:
                | _search | stock |
            And I press "Save"
        Then the column "Ordered" should contain "Stock Order"

    @javascript
    Scenario: Check in an item on an ordered purchase order from the view page
        Given I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     | costPrice | carriagePrice |
                | stock item  | SI1  | 7   | 14    | ABC supplies | 5.00      | 1.00          |
            And I have the following locations:
                | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | Main warehouse   | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following stockItems:
                | stock      | count | location       |
                | stock item | 1     | Main warehouse |
            And I have the following purchaseOrders:
                | description | instructions | orderDate | stockItem  | supplier     | stockItem_qty |
                | test PO     | order this   | now       | stock item | ABC supplies | 1             |
        When I go to "/purchaseorders"
            And I click on "Stock Order" in the "Ordered" column
            And I follow "View"
            And I press "Check in item"
            And I focus "purchase_order_item_form_receivedDate"
            And I press "Save"
            And I go to "/purchaseorders"
            And I click on "Stock Order" in the "Received" column
        Then I should see "Received" in the "purchaseOrder-particulars" section
            And I go to the view page for the "stock item" stock item
            And I should see "1" and "IN - Purchase Order" in a transaction row

    @javascript
    Scenario: Check in an item on an ordered purchase order from the view page
        Given I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     | costPrice | carriagePrice |
                | stock item  | SI1  | 7   | 14    | ABC supplies | 5.00      | 1.00          |
            And I have the following locations:
                | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | Main warehouse   | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following stockItems:
                | stock      | count | location       |
                | stock item | 1     | Main warehouse |
            And I have the following purchaseOrders:
                | description | instructions | orderDate | stockItem  | supplier     | stockItem_qty |
                | test PO     | order this   | now       | stock item | ABC supplies | 1             |
        When I go to "/purchaseorders"
            And I click on "Stock Order" in the "Ordered" column
            And I press "Check in item"
            And I focus "purchase_order_item_form_receivedDate"
            And I press "Save"
            And I fill in "Bin 1" for "Bin"
            And I press "Save"
            And I click on "Stock Order" in the "Received" column
        Then I should see "Received on" in the "purchaseOrder-particulars" section

    @javascript
    Scenario: Delete an PO
        Given I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     | costPrice | carriagePrice |
                | stock item  | SI1  | 7   | 14    | ABC supplies | 5.00      | 1.00          |
            And I have the following locations:
                | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | Main warehouse   | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following stockItems:
                | stock      | count | location       |
                | stock item | 1     | Main warehouse |
            And I have the following purchaseOrders:
                | description | instructions | orderDate | stockItem  | supplier    | stockItem_qty |
                | test PO     | order this   | now       | stock item | ABC supplies| 1             |
        When I go to "/purchaseorders"
            And I click the ".panel-link" containing "Stock Order"
            And I follow "View"
            And I follow "Delete"
            And I press "Delete" on the current modal
            And I go to "/purchaseorders"
        Then I should not see "ABC supplies"

    @javascript
    Scenario: Check print page appears
        Given I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     | costPrice | carriagePrice |
                | stock item  | SI1  | 7   | 14    | ABC supplies | 5.00      | 1.00          |
            And I have the following locations:
                | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | Main warehouse   | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following stockItems:
                | stock      | count | location       |
                | stock item | 1     | Main warehouse |
            And I have the following purchaseOrders:
                | description | instructions | orderDate | stockItem  | supplier    | stockItem_qty |
                | test PO     | order this   | now       | stock item | ABC supplies| 1             |
        When I go to "/purchaseorders"
            And I click the ".panel-link" containing "Stock Order"
            And I follow "Print"
        Then I should see "stock item"

    @javascript
    Scenario: Add a stock item by searching for code
        Given I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code    | oem | issue | supplier     | costPrice | carriagePrice |
                | stock item  | 8080808 | 7   | 14    | ABC supplies | 5.00      | 1.00          |
            And I have the following locations:
                | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | Main warehouse   | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following stockItems:
                | stock      | count | location       |
                | stock item | 1     | Main warehouse |
        When I go to "/purchaseorders"
            And I follow "New Stock Order"
            And I click the "Add Special Part" button in the "stockItems" section
            And I fill in the following:
                | purchase_order_form_estimateItems_0_item     | special item |
                | purchase_order_form_estimateItems_0_price    | 1.00         |
                | purchase_order_form_estimateItems_0_quantity | 1            |
            And I fill in the following:
                | _search | 8080 |
            And I wait for the search results to appear
            And I press "Save"
        Then the column "Incomplete" should contain "Stock Order"

    @javascript
    Scenario: Add a new stock item in a purchase order
        Given I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
        When I go to "/purchaseorders"
            And I follow "New Stock Order"
            And I click the "Add New Part" button in the "stockItems" section
            And I fill in the following:
                | stock_form_description | special item |
                | stock_form_code        | 12345        |
                | stock_form_oemPrice    | 1.00         |
            And I press "Save" on the top modal
            And I wait for the search results to appear
            And I press "Save" on the top modal
        Then there should be a "stock" with the "description" "special item"

    @javascript
    Scenario: Manually archive a completed PO
        Given I have the following suppliers:
                | name         | code | telephone | fax       | email            |
                | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following stock:
                | description | code | oem | issue | supplier     | costPrice | carriagePrice |
                | stock item  | SI1  | 7   | 14    | ABC supplies | 5.00      | 1.00          |
            And I have the following locations:
                | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | Main warehouse   | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following stockItems:
                | stock      | count | location       |
                | stock item | 1     | Main warehouse |
            And I have the following purchaseOrders:
                | description | instructions | orderDate | stockItem  | supplier    | stockItem_qty |
                | test PO     | order this   | now       | stock item | ABC supplies| 1             |
        When I go to "/purchaseorders"
            And I click the ".panel-link" containing "Stock Order"
            And I follow "View"
            And I press "Check in item"
            And I press "Save"
            And I fill in "Bin 1" for "Bin"
            And I press "Save"
            And I dismiss any flash messages
            And I follow "setAsArchived"
        Then I should see "Re-Instate"
