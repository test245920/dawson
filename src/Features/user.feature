Feature: Testing out user functionality
    Add a user, delete a user.


    @javascript
    Scenario: Add a user
        Given I am authenticated as "megaadmin"
            And I go to "/users"
            And I follow "new-item"
            And I fill in the following:
                | user_form_username               | testuser      |
                | user_form_name                   | test user     |
                | user_form_email                  | test@user.com |
                | user_form_plainPassword_Password | testuser      |
                | user_form_plainPassword_Confirm  | testuser      |
            And I press "Save"
        Then I should see "testuser" in the "users" section

    @javascript
    Scenario: Edit a user
        Given I am authenticated as "megaadmin"
            And I have the following users:
                | username | email         | password |
                | testuser | test@user.com | testuser |
            And I go to "/users"
            And I follow "testuser (testuser)"
            And I follow "Edit"
            And I fill in the following:
                | user_form_email | new@email.com |
            And I press "Save"
        Then I should see "new@email.com" in the "details" section

    @javascript
    Scenario: Delete a user
        Given I am authenticated as "megaadmin"
            And I have the following users:
                | username | password |
                | testuser | testuser |
        When I go to "/users"
            And I follow "testuser (testuser)"
            And I follow "Delete"
            And I press "Delete" on the current modal
        Then I should not see "testuser" in the "users" section

    @javascript
    Scenario: Set password for new account
    Given I have the following contacts:
            | name       | telephone | email              | customer     |
            | test user  | 123456789 | first@contact.com  | testcustomer |
        And I have the following users:
            | username               | token | contact   |
            | test.user-testcustomer | abcde | test user |
    When I go to "/users/password-set/abcde"
        And I fill in the following:
            | password_reset_form_plainPassword_Password | password |
            | password_reset_form_plainPassword_Confirm | password |
        And I press "Submit"
    Then I should see "Dawson Fleetmanager"

    @javascript
    Scenario: Reset password for an account
    Given I have the following contacts:
            | name       | telephone | email              | customer     |
            | test user  | 123456789 | first@contact.com  | testcustomer |
        And I have the following users:
            | username               | token | contact   |
            | test.user-testcustomer | abcde | test user |
    When I go to "/users/password-reset/abcde"
        And I fill in the following:
            | password_reset_form_plainPassword_Password | password |
            | password_reset_form_plainPassword_Confirm | password |
        And I press "Submit"
    Then I should see "Dawson Fleetmanager"
