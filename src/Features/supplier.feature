Feature: Testing out supplier functionality
    Add a supplier, edit a supplier by adding a supplier address, delete a supplier

    Background:
        Given I am authenticated as "megaadmin"

    @javascript
    Scenario: Add a supplier
        Given I go to "/suppliers"
            And I follow "New"
            And I fill in the following:
                | supplier_form_name      | ABC supplies |
                | supplier_form_email     | ABC1 |
                | supplier_form_telephone | 123456789 |
                | supplier_form_fax       | 987654321 |
                | supplier_form_email     | abc@supplies.com |
            And I press "Save"
        Then I should see "ABC supplies" in the "suppliers" section

    @javascript
    Scenario: Add an address to a supplier
        Given I have the following suppliers:
               | name         | code | telephone | fax       | email            |
               | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
        When I go to "/suppliers"
            And I follow "ABC supplies"
            And I follow "Edit"
            And I fill in the following:
                | supplier_form_supplierAddress_address1 | ECIT             |
                | supplier_form_supplierAddress_address2 | Queen's Road     |
                | supplier_form_supplierAddress_city     | Belfast          |
                | supplier_form_supplierAddress_postcode | BT3 9DT          |
                | supplier_form_supplierAddress_county   | Antrim           |
                | supplier_form_supplierAddress_country  | Northern Ireland |
            And I press "Save"
        Then I should see "ECIT" in the "details" section

    @javascript
    Scenario: Delete a supplier
        Given I have the following suppliers:
               | name         | code | telephone | fax       | email            |
               | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
        When I go to "/suppliers"
            And I follow "ABC supplies"
            And I follow "Delete"
            And I press "Delete" on the current modal
        Then I should not see "ABC supplies" in the "suppliers" section

    @javascript
    Scenario: Check customer and supplier fields are not being shown in contact form
        Given I have the following suppliers:
               | name         | code | telephone | fax       | email            |
               | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I have the following contacts:
                | name           | telephone | email            | supplier     |
                | test contact   | 123456789 | test@contact.com | ABC supplies |
                | second contact | 123456789 | test@contact.com | ABC supplies |
                | third contact  | 123456789 | test@contact.com | ABC supplies |
        When I go to "/suppliers"
            And I follow "ABC supplies"
            And I follow "Edit"
        Then I should not see an "supplier_form_contacts_0_customer" element
            And I should not see an "supplier_form_contacts_0_supplier" element
            And I should not see an "supplier_form_contacts_1_customer" element
            And I should not see an "supplier_form_contacts_1_supplier" element
            And I should not see an "supplier_form_contacts_2_customer" element
            And I should not see an "supplier_form_contacts_2_supplier" element
