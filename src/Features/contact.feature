Feature: Testing out contact functionality
    Add a contact, delete a contact.

    Background:
        Given I am authenticated as "megaadmin"

    @javascript
    Scenario: Add a contact for a customer
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
        When I go to "/contacts"
            And I follow "New"
            And I select "testcustomer" from "add_contact_form_customer"
            And I fill in the following:
                | add_contact_form_name      | test contact     |
                | add_contact_form_telephone | 123456789        |
                | add_contact_form_mobile    | 987654321        |
                | add_contact_form_email     | test@contact.com |
            And I press "Save"
        Then I should see "test contact" in the "Contacts" column

    @javascript
    Scenario: Add a contact with no customer
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
        When I go to "/contacts"
            And I follow "New"
            And I fill in the following:
                | add_contact_form_name      | test contact     |
                | add_contact_form_telephone | 12345679         |
                | add_contact_form_mobile    | 987654321        |
                | add_contact_form_email     | test@contact.com |
            And I press "Save"
        Then I should see "test contact" in the "Contacts" column

    @javascript
    Scenario: Check a contact with a supplier appears in the correct column
        Given I have the following suppliers:
                | name         | code |
                | testsupplier | SUP1 |
            And I have the following contacts:
                | name         | telephone | email            | supplier     |
                | test contact | 123456789 | test@contact.com | testsupplier |
        When I go to "/contacts"
            And I click on "testsupplier" in the "Supplier Name" column
        Then I should see "test contact" in the "Contacts" column

    @javascript
    Scenario: Edit a contact's name
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
            And I have the following contacts:
                | name         | telephone | email            | customer     |
                | test contact | 123456789 | test@contact.com | testcustomer |
        When I go to "/contacts"
            And I click on "test contact" in the "Contacts" column
            And I follow "Edit"
            And I fill in the following:
                | add_contact_form_name | edited contact |
            And I press "Save"
        Then I should see "edited contact" in the "Contacts" column

    @javascript
    Scenario: Edit a contact's customer
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
            And I have the following contacts:
                | name         | telephone | email            | customer     |
                | test contact | 123456789 | test@contact.com | testcustomer |
        When I go to "/contacts"
            And I click on "test contact" in the "Contacts" column
            And I follow "Edit"
            And I select "testcustomer" from "add_contact_form_customer"
            And I press "Save"
        Then I should see "test contact" in the "Contacts" column

    @javascript
    Scenario: Find a contact by using the filter in the contact column
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
            And I have the following contacts:
                | name           | telephone | email              | customer     |
                | first contact  | 123456789 | first@contact.com  | testcustomer |
                | second contact | 987654321 | second@contact.com | testcustomer |
        When I go to "/contacts"
            And I fill in the following:
                | contact_filter | first |
            And the column "Contact" should contain "first contact"
        Then I should not see "second contact"

    @javascript
    Scenario: Find a contact by using the filter in the customer column
        Given I have the following customers:
                | name            | billingAddress_address1 | billingAddress_city |
                | first customer  | 123 Walker Avenue       | Atlanta             |
                | second customer | 456 Walker Avenue       | Atlanta             |
            And I have the following contacts:
                | name           | telephone | email              | customer        |
                | first contact  | 123456789 | first@contact.com  | first customer  |
                | second contact | 987654321 | second@contact.com | second customer |
        When I go to "/contacts"
            And I fill in the following:
                | customer_filter | first |
            And I click on "first customer" in the "Customer Name" column
            And I should see "first contact" in the "Contacts" column
        Then I should not see "second contact" in the "Contacts" column

    @javascript
    Scenario: Delete an contact
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
            And I have the following contacts:
                | name         | telephone | email            | customer     |
                | test contact | 123456789 | test@contact.com | testcustomer |
        When I go to "/contacts"
            And I click the ".panel-link" containing "test contact"
            And I follow "View"
            And I follow "Delete"
            And I press "Delete" on the current modal
        Then I should not see "test contact" in the "Contacts" column

    @javascript
    Scenario: Add a contact with a job title
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
        When I go to "/contacts"
            And I follow "New"
            And I select "testcustomer" from "add_contact_form_customer"
            And I fill in the following:
                | add_contact_form_name      | test contact     |
                | add_contact_form_telephone | 12345679         |
                | add_contact_form_mobile    | 987654321        |
                | add_contact_form_email     | test@contact.com |
                | add_contact_form_jobTitle  | Sheriff          |
            And I press "Save"
            And I click the ".panel-link" containing "test contact"
        Then I should see "Sheriff"

    @javascript
    Scenario: Add a contact and a new customer
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
        When I go to "/contacts"
            And I follow "New"
            And I fill in the following:
                | add_contact_form_name      | test contact     |
                | add_contact_form_telephone | 123456789        |
                | add_contact_form_mobile    | 987654321        |
                | add_contact_form_email     | test@contact.com |
                | add_contact_form_jobTitle  | Sales person     |
            And I click "add-new-customer"
            And I fill in the following:
                | add_contact_form_newCustomer | newCustomer |
            And I press "Save"
        Then I wait for "test contact" to appear in the "contacts" section
            And I should see "newCustomer" in the "Customer Name" column

    @javascript
    Scenario: Find a contact by using the filter in the contact column
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
            And I have the following contacts:
                | name           | telephone | email              | customer     |
                | first contact  | 123456789 | first@contact.com  | testcustomer |
                | second contact | 987654321 | second@contact.com | testcustomer |
        When I go to "/contacts"
            And I fill in the following:
                | contact_filter | 12345 |
            And the column "Contact" should contain "first contact"
        Then I should not see "second contact"

    @javascript
    Scenario: Create a user account for a contact
        Given I have the following customers:
                | name            | billingAddress_address1 | billingAddress_city |
                | first customer  | 123 Walker Avenue       | Atlanta             |
            And I have the following contacts:
                | name           | telephone | email                            | customer        |
                | first contact  | 123456789 | first.contact@firstcustomer.com  | first customer  |
        When I go to the view page for the "first contact" contact
            And I click "Create user account"
            And I fill in the following:
                | contact_account_form_username | first.contact-first.customer    |
                | contact_account_form_name     | first contact                   |
                | contact_account_form_email    | first.contact@firstcustomer.com |
            And I press "Save" on the current modal
        Then there should be a "user" with the "username" "first.contact-first.customer"
            And there should be 1 email with the subject "Welcome to your new Dawson Forklift Services account."
