Feature: Testing out lead functionality
    Add a lead, delete a lead.

    Background:
       Given I am authenticated as "megaadmin"

#    @javascript
#    Scenario: Add a lead for the current user
#        Given I have the following customers:
#                | name         | billingAddress_address1 | billingAddress_city | isLead |
#                | testcustomer | 123 Walker Avenue       | Atlanta             | 1      |
#            And I have the following sources:
#                | name |
#                | Rick |
#        When I go to "/leads"
#            And I follow "new-item"
#            And I select "testcustomer" from "lead_form_customer"
#            And I select "Rick" from "lead_form_source"
#            And I select "Normal" from "lead_form_priority"
#            And I press "Save"
#        Then I should see "testcustomer" in the "No contact" column
#
#    @javascript
#    Scenario: Assign a lead to another user on the system
#        Given I have the following customers:
#                | name         | billingAddress_address1 | billingAddress_city | isLead |
#                | testcustomer | 123 Walker Avenue       | Atlanta             | 1      |
#            And I have the following sources:
#                | name |
#                | Rick |
#        When I go to "/leads"
#            And I follow "new-item"
#            And I select "testcustomer" from "lead_form_customer"
#            And I select "Rick" from "lead_form_source"
#            And I select "Normal" from "lead_form_priority"
#            And I select "Service Admin" from "lead_form_user"
#            And I press "Save"
#            And I wait for the "user_selectoor" element to contain a "serviceadmin" option
#            And I select "serviceadmin" from "user_selector"
#        Then I should see "testcustomer" in the "No contact" column
#            And there should be 1 email with the subject "You have been assigned to a new Lead."
#
#    @javascript
#    Scenario: Check a user cannot assign an admin to a lead
#        Given I am authenticated as "serviceadmin"
#            And I have the following customers:
#                | name         | billingAddress_address1 | billingAddress_city | isLead |
#                | testcustomer | 123 Walker Avenue       | Atlanta             | 1      |
#            And I have the following sources:
#                | name |
#                | Rick |
#        When I go to "/leads"
#        Then I should not see "megaadmin" in the "lead_form_user" section
#
#    @javascript
#    Scenario: Change a lead status to Contact made
#        Given I have the following customers:
#                | name         |
#                | testcustomer |
#            And I have the following sources:
#                | name |
#                | Rick |
#            And I have the following leads:
#                | customer     | source | status     | user          | type         |
#                | testcustomer | Rick   | No contact | megaadmin     | New Purchase |
#        When I go to the view page for the "testcustomer" lead
#            And I select "Contact made" from "status_selector"
#            And I go to "/leads"
#        Then I should see "testcustomer" in the "Contact made" column
#
#    @javascript
#    Scenario: Change a lead status to No contact
#        Given I have the following customers:
#                | name         |
#                | testcustomer |
#            And I have the following sources:
#                | name |
#                | Rick |
#            And I have the following leads:
#                | customer     | source | status       | user          | type         |
#                | testcustomer | Rick   | Contact made | megaadmin     | New Purchase |
#        When I go to the view page for the "testcustomer" lead
#            And I select "No contact" from "status_selector"
#            And I go to "/leads"
#        Then I should see "testcustomer" in the "No contact" column
#
#    @javascript
#    Scenario: Change a lead status to Action required
#        Given I have the following customers:
#                | name         |
#                | testcustomer |
#            And I have the following sources:
#                | name |
#                | Rick |
#            And I have the following leads:
#                | customer     | source | status     | user          | type         |
#                | testcustomer | Rick   | No contact | megaadmin     | New Purchase |
#        When I go to the view page for the "testcustomer" lead
#            And I select "Action required" from "status_selector"
#            And I go to "/leads"
#        Then I should see "testcustomer" in the "Action required" column
#
#    @javascript
#    Scenario: Change a lead status to Quoting
#        Given I have the following customers:
#                | name         |
#                | testcustomer |
#            And I have the following sources:
#                | name |
#                | Rick |
#            And I have the following leads:
#                | customer     | source | status     | user          | type         |
#                | testcustomer | Rick   | No contact | megaadmin     | New Purchase |
#        When I go to the view page for the "testcustomer" lead
#            And I select "Quoting" from "status_selector"
#            And I go to "/leads"
#        Then I should see "testcustomer" in the "Quoting" column
#
#    @javascript
#    Scenario: Add a contact to a lead from particulars section
#        Given I have the following customers:
#                | name         |
#                | testcustomer |
#            And I have the following sources:
#                | name |
#                | Rick |
#            And I have the following leads:
#                | customer     | source | status     | type         |
#                | testcustomer | Rick   | No contact | New Purchase |
#        When I go to the view page for the "testcustomer" lead
#            And I click the "New" button in the "contacts" section
#            And I fill in the following:
#                | add_contact_form_name      | Rick Grimes     |
#                | add_contact_form_telephone | 1543587         |
#                | add_contact_form_email     | rick@grimes.com |
#                | add_contact_form_jobTitle  | Sheriff         |
#            And I press "Save" on the current modal
#        Then I should see "Rick Grimes"
#
#    @javascript
#    Scenario: Select a contact on a lead
#        Given I have the following customers:
#                | name         | contact_name | contact_telephone | contact_email   |
#                | testcustomer | Rick Grimes  | 1234567899        | rick@grimes.com |
#            And I have the following sources:
#                | name |
#                | Rick |
#            And I have the following leads:
#                | customer     | source | status     | priority | user      | type         |
#                | testcustomer | Rick   | No contact | Normal   | megaadmin | New Purchase |
#        When I go to the view page for the "testcustomer" lead
#            And I follow "Edit"
#            And I check "Rick Grimes"
#            And I press "Save" on the current modal
#        Then I should see "Rick Grimes"
#
#    @javascript
#    Scenario: Add a note to a lead from the particulars
#        Given I have the following customers:
#                | name         |
#                | testcustomer |
#            And I have the following sources:
#                | name |
#                | Rick |
#            And I have the following leads:
#                | customer     | source | status     | priority | user      | type         |
#                | testcustomer | Rick   | No contact | Normal   | megaadmin | New Purchase |
#        When I go to "/leads"
#            And I click on "testcustomer" in the "No contact" column
#            And I click the "New" button in the "notes" section
#            And I select "Telephone call" from "note_form_reason"
#            And I fill in the following:
#                | note_form_detail | tel call |
#            And I check "note_form_date"
#            And I press "Save" on the current modal
#        Then I should see "Telephone call"
#
#    @javascript
#    Scenario: Check notes appear with newest at top of list
#        Given I have the following customers:
#                | name         |
#                | testcustomer |
#            And I have the following sources:
#                | name |
#                | Rick |
#            And I have the following leads:
#                | customer     | source | status     | priority | type         |
#                | testcustomer | Rick   | No contact | normal   | New Purchase |
#            And I have the following notes:
#                | reason         | detail     | date       | lead         |
#                | Telephone call | today note | now        | testcustomer |
#                | Telephone call | yday note  | yesterday  | testcustomer |
#        When I go to the view page for the "testcustomer" lead
#        Then I should see "today note" in row 1 and "yday note" in row 2 in the "notes" section
#
#    @javascript
#    Scenario: Add a future target reminder to a lead
#        Given I have the following customers:
#                | name         |
#                | testcustomer |
#            And I have the following sources:
#                | name |
#                | Rick |
#            And I have the following leads:
#                | customer     | source | status     | priority | user          | type         |
#                | testcustomer | Rick   | No contact | Normal   | megaadmin     | New Purchase |
#        When I go to the view page for the "testcustomer" lead
#            And I click the "New" button in the "reminders" section
#            And I select "Future Target" from "reminder_form_type"
#            And I fill in the following:
#                | reminder_form_detail | test reminder |
#            And I check "reminder_form_date"
#            And I press "Save" on the current modal
#        Then I should see "test reminder"
#
#    @javascript
#    Scenario: Delete a lead
#        Given I have the following customers:
#                | name         | billingAddress_address1 | billingAddress_city | isLead |
#                | testcustomer | 123 Walker Avenue       | Atlanta             | 1      |
#            And I have the following sources:
#                | name |
#                | Rick |
#            And I have the following leads:
#                | status     | priority | customer     | source | user          | type         |
#                | No contact | Normal   | testcustomer | Rick   | megaadmin     | New Purchase |
#        When I go to "/leads"
#            And I click on "testcustomer" in the "No contact" column
#            And I follow "Delete"
#            And I press "Delete" on the current modal
#        Then I should not see "testcustomer"
#
#    @javascript
#    Scenario: Add a service call reminder to a lead
#        Given I have the following customers:
#                | name         | billingAddress_address1 | billingAddress_city | isLead |
#                | testcustomer | 123 Walker Avenue       | Atlanta             | 1      |
#            And I have the following sources:
#                | name |
#                | Rick |
#            And I have the following leads:
#                | status     | priority | customer     | source | user      | type         |
#                | No contact | Normal   | testcustomer | Rick   | megaadmin | New Purchase |
#        When I go to the view page for the "testcustomer" lead
#            And I click the "New" button in the "reminders" section
#            And I select "Service Call" from "reminder_form_type"
#            And I fill in the following:
#                | reminder_form_detail |  test reminder |
#            And I focus "reminder_form_date"
#            And I press "Save" on the current modal
#        Then I should see "megaadmin" in the "reminders" section
#
#    @javascript
#    Scenario: View a different user's leads
#        Given I have the following customers:
#                | name            |
#                | testcustomer    |
#                | second customer |
#            And I have the following sources:
#                | name |
#                | Rick |
#            And I have the following leads:
#                | customer        | source   | status       | user          | type         |
#                | testcustomer    | Rick     | No contact   | megaadmin     | New Purchase |
#                | second customer | Rick   2 | Contact made | serviceadmin | New Purchase |
#        When I go to "/leads"
#            And I select "serviceadmin" from "user_selector"
#        Then I should see "second customer" in the "Contact made" column
#            And I should not see "testcustomer" in the "No contact" column
#
#    @javascript
#    Scenario: Check a non SUPER_ADMIN can't see the user_selector
#        Given I am authenticated as "serviceadmin"
#            And I have the following customers:
#                | name            |
#                | testcustomer    |
#                | second customer |
#            And I have the following sources:
#                | name |
#                | Rick |
#            And I have the following leads:
#                | customer        | source   | status       | user          | type         |
#                | testcustomer    | Rick     | No contact   | serviceadmin | New Purchase |
#        When I go to "/leads"
#        Then I should not see "user_selector"
#
#    @javascript
#    Scenario: Change a lead to priority urgent
#        Given I have the following customers:
#                | name         |
#                | testcustomer |
#            And I have the following sources:
#                | name |
#                | Rick |
#            And I have the following leads:
#                | customer     | source | status       | user      | type         | priority  |
#                | testcustomer | Rick   | Contact made | megaadmin | New Purchase | Important |
#        When I go to the view page for the "testcustomer" lead
#            And I select "Urgent" from "priority_selector"
#            And I go to "/leads"
#        Then I should see "testcustomer" in the "Contact made" column as the colour "red"
#
#    @javascript
#    Scenario: Change a lead to priority normal
#        Given I have the following customers:
#                | name         |
#                | testcustomer |
#            And I have the following sources:
#                | name |
#                | Rick |
#            And I have the following leads:
#                | customer     | source | status       | user      | type         | priority  |
#                | testcustomer | Rick   | Contact made | megaadmin | New Purchase | Important |
#        When I go to the view page for the "testcustomer" lead
#            And I select "Normal" from "priority_selector"
#            And I go to "/leads"
#        Then I should see "testcustomer" in the "Contact made" column as the colour "white"
#
#    @javascript
#    Scenario: Change a lead to priority important
#        Given I have the following customers:
#                | name         |
#                | testcustomer |
#            And I have the following sources:
#                | name |
#                | Rick |
#            And I have the following leads:
#                | customer     | source | status       | user      | type         | priority  |
#                | testcustomer | Rick   | Contact made | megaadmin | New Purchase | Urgent    |
#        When I go to the view page for the "testcustomer" lead
#            And I select "Important" from "priority_selector"
#            And I go to "/leads"
#        Then I should see "testcustomer" in the "Contact made" column as the colour "amber"
#
#    @javascript
#    Scenario: Convert a successful lead
#        Given I have the following customers:
#                | name         | billingAddress_address1 | billingAddress_city | isLead |
#                | testcustomer | 123 Walker Avenue       | Atlanta             | 1      |
#            And I have the following sources:
#                | name |
#                | Rick |
#            And I have the following leads:
#                | status     | priority | customer     | source | type         |
#                | No contact | Normal   | testcustomer | Rick   | New Purchase |
#        When I go to the view page for the "testcustomer" lead
#            And I click the "Successful lead" button in the "top" section
#            And I click the "Confirm" button in the "confirm-modal" section
#        Then the url should match "/customers/view"
#            And I go to "/customers"
#            And I should see "T1"
#
#    @javascript
#    Scenario: Add a casual hire lead for the current user
#    Given I have the following customers:
#                | name         | billingAddress_address1 | billingAddress_city | isLead |
#                | testcustomer | 123 Walker Avenue       | Atlanta             | 1      |
#            And I have the following sources:
#                | name |
#                | Rick |
#            When I go to "/leads/casual-hire-leads"
#                And I follow "new-item"
#                And I select "testcustomer" from "lead_form_customer"
#                And I select "Rick" from "lead_form_source"
#                And I press "Save" on the current modal
#        Then I should see "testcustomer" in the "No contact" column
#
#    @javascript
#    Scenario: Add a primary target lead
#        Given I have the following customers:
#                | name         | billingAddress_address1 | billingAddress_city | isLead |
#                | testcustomer | 123 Walker Avenue       | Atlanta             | 1      |
#            And I have the following sources:
#                | name |
#                | Rick |
#        When I go to "/leads"
#            And I follow "new-item"
#            And I select "testcustomer" from "lead_form_customer"
#            And I select "Rick" from "lead_form_source"
#            And I press "Save"
#            And I go to the view page for the "testcustomer" lead
#            And I select "Primary" from "target_selector"
#            And I go to "/leads/primary-targets"
#        Then I should see "testcustomer" in the "No contact" column
#
#    @javascript
#    Scenario: Convert a lead to unsuccessful
#        Given I have the following customers:
#                | name         | isLead |
#                | testcustomer | true   |
#            And I have the following sources:
#                | name |
#                | Rick |
#            And I have the following leads:
#                | customer     | source | status       | type         |
#                | testcustomer | Rick   | Contact made | New Purchase |
#        When I go to "/leads"
#            And I click on "testcustomer" in the "Contact made" column
#            And I click the "Unsuccessful lead" button in the "top" section
#            And I fill in the following:
#                | Reason | test reason |
#            And I press "Save" on the current modal
#            And I go to "/leads/list-unsuccessful"
#        Then I should see "testcustomer" in the "Leads" column
#
#    @javascript
#    Scenario: Edit a Lead's address
#        Given I have the following customers:
#                | name         | isLead |
#                | testcustomer | true   |
#            And I have the following sources:
#                | name |
#                | Rick |
#            And I have the following leads:
#                | customer     | source | status     | user          | type         |
#                | testcustomer | Rick   | No contact | megaadmin     | New Purchase |
#        When I go to the view page for the "testcustomer" lead
#            And I click the "button" containing "Edit Address"
#            And I fill in the following:
#               | billing_address_form_address1 | 14 Test  Street |
#               | billing_address_form_address2 | Wonderland      |
#               | billing_address_form_city     | Belfast         |
#               | billing_address_form_postcode | BT3 9DT         |
#            And I press "Save" on the current modal
#        Then I should see "14 test Street, Wonderland, Belfast, BT3 9DT"
#
#    @javascript
#    Scenario: Add a contact to a lead in the form
#        Given I am authenticated as "megaadmin"
#            And I have the following customers:
#                | name         |
#                | testcustomer |
#            And I have the following sources:
#                | name |
#                | Rick |
#            And I have the following leads:
#                | customer     | source | summary   | status     | user          | type         |
#                | testcustomer | Rick   | test lead | No contact | megaadmin     | New Purchase |
#        When I go to "/leads"
#            And I click on "testcustomer" in the "No contact" column
#            And I follow "Edit"
#            And I follow "addNewContact"
#            And I fill in the following:
#                | lead_form_newContacts_0_name      | Rick Grimes     |
#                | lead_form_newContacts_0_telephone | 1543587         |
#                | lead_form_newContacts_0_email     | rick@grimes.com |
#            And I press "Save" on the current modal
#        Then I should see "Rick Grimes"
#
#    @javascript
#    Scenario: Add a contact to a lead without an email address
#        Given I am authenticated as "megaadmin"
#            And I have the following customers:
#                | name         |
#                | testcustomer |
#            And I have the following sources:
#                | name |
#                | Rick |
#            And I have the following leads:
#                | customer     | source | summary   | status     | user          | type         |
#                | testcustomer | Rick   | test lead | No contact | megaadmin     | New Purchase |
#        When I go to "/leads"
#            And I click on "testcustomer" in the "No contact" column
#            And I follow "Edit"
#            And I follow "addNewContact"
#            And I fill in the following:
#                | lead_form_newContacts_0_name      | Rick Grimes     |
#                | lead_form_newContacts_0_telephone | 1543587         |
#            And I press "Save" on the current modal
#        Then I should see "Rick Grimes"
#
#    @javascript
#    Scenario: Select a contact on a lead
#        Given I am authenticated as "megaadmin"
#            And I have the following customers:
#                | name         | contact_name | contact_telephone | contact_email   |
#                | testcustomer | Rick Grimes  | 1234567899        | rick@grimes.com |
#            And I have the following sources:
#                | name |
#                | Rick |
#            And I have the following leads:
#                | customer     | source | summary   | status     | priority | user          | type         |
#                | testcustomer | Rick   | test lead | No contact | Normal   | megaadmin     | New Purchase |
#        When I go to "/leads"
#            And I click on "testcustomer" in the "No contact" column
#            And I follow "Edit"
#            And I check "Rick Grimes"
#            And I press "Save" on the current modal
#        Then I should see "Rick Grimes"
#
#    @javascript
#    Scenario: Check an unsuccessful lead appears in the listings page
#        Given I have the following customers:
#                | name         | isLead |
#                | testcustomer | true   |
#            And I have the following sources:
#                | name |
#                | Rick |
#            And I have the following leads:
#                | customer     | source | status       | type         | unsuccessful | unsuccessfulReason |
#                | testcustomer | Rick   | Contact made | New Purchase | true         | test reason        |
#        When I go to "/leads/list-unsuccessful"
#            And I click on "testcustomer" in the "Leads" column
#        Then I should see "Unsuccessful" beside "Outcome" in the "middle-description-section" section
#            And I should see "test reason" beside "Reason" in the "middle-description-section" section
#
#    @javascript
#    Scenario: View a different user's primary targets
#        Given I have the following customers:
#                | name            |
#                | testcustomer    |
#                | second customer |
#            And I have the following sources:
#                | name |
#                | Rick |
#            And I have the following leads:
#                | customer        | source   | status       | user          | type         | primaryTarget |
#                | testcustomer    | Rick     | No contact   | megaadmin     | New Purchase | yes           |
#                | second customer | Rick   2 | Contact made | serviceadmin | New Purchase | yes           |
#        When I go to "/leads/primary-targets"
#            And I select "serviceadmin" from "user_selector"
#        Then I should see "second customer" in the "Contact made" column
#            And I should not see "testcustomer" in the "No contact" column
#
#    @javascript
#    Scenario: Check a non SUPER_ADMIN can't see the user_selector in primary targets
#        Given I am authenticated as "serviceadmin"
#            And I have the following customers:
#                | name            |
#                | testcustomer    |
#                | second customer |
#            And I have the following sources:
#                | name |
#                | Rick |
#            And I have the following leads:
#                | customer     | source | status     | user          | type         | primaryTarget |
#                | testcustomer | Rick   | No contact | serviceadmin | New Purchase | yes           |
#        When I go to "/leads/primary-targets"
#        Then I should not see "user_selector"

    @javascript
    Scenario: View a different user's future targets
        Given I have the following customers:
                | name            |
                | testcustomer    |
                | second customer |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer        | source | user          | type         | future target |
                | testcustomer    | Rick   | megaadmin     | New Purchase | yes           |
                | second customer | Rick 2 | serviceadmin | New Purchase | yes           |
        When I go to "/leads/futureTargets/list"
            And I select "serviceadmin" from "user_selector"
        Then I should see "second customer" in the "No Reminder" column
            And I should not see "testcustomer" in the "No Reminder" column

    @javascript
    Scenario: Check a non SUPER_ADMIN can't see the user_selector in future targets
        Given I am authenticated as "serviceadmin"
            And I have the following customers:
                | name            |
                | testcustomer    |
                | second customer |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer        | source   | user          | type         | future target |
                | testcustomer    | Rick     | serviceadmin | New Purchase | yes           |
        When I go to "/leads/futureTargets/list"
        Then I should not see "user_selector"

    @javascript
    Scenario: Change the user on an existing lead and check reminder changes ownership
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
            And I have the following reminders:
                | type          | detail         | date     | user      |
                | Future Target | test reminder  | ten days | megaadmin |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | reminder      | future target | type         |
                | testcustomer | Rick   | No contact | Normal   | test reminder | true          | New Purchase |
        When I go to the view page for the "testcustomer" lead
            And I follow "Edit"
            And I select "Service Admin" from "lead_form_user"
            And I press "Save" on the current modal
        Then there should be 1 email with the subject "You have been assigned to a new Lead."
            And I should see "serviceadmin" in the "reminders" section

    @javascript
    Scenario: Check a future target does not appear in leads section
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city | active |
                | testcustomer | 123 Walker Avenue       | Atlanta             | no     |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | future target | type         |
                | testcustomer | Rick   | No contact | Normal   | true          | New Purchase |
        When I go to "/leads"
            And I should not see "testcustomer"
            And I go to "/leads/futureTargets/list"
        Then I should see "testcustomer"

    @javascript
    Scenario: Check a reminder is deleted when a lead is deleted
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
            And I have the following reminders:
                | type          | detail         | date     | user      |
                | Future Target | test reminder  | ten days | megaadmin |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | reminder      | future target | type         |
                | testcustomer | Rick   | No contact | Normal   | test reminder | true          | New Purchase |
        When I go to the view page for the "testcustomer" lead
            And I follow "Delete"
            And I press "Delete" on the current modal
        Then I should not see "testcustomer"
            And I go to "/reminders/list"
            And I should not see "testcustomer"

    @javascript
    Scenario: Check future targets are listed for all reminder types
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | future target | type         |
                | testcustomer | Rick   | No contact | Normal   | true          | New Purchase |
            And I have the following reminders:
                | type          | detail         | date      | user      | lead         |
                | Future Target | test reminder  | ten days  | megaadmin | testcustomer |
                | Phone Call    | test call      | 18 months | megaadmin | testcustomer |
        When I go to "/leads/futureTargets/list/1"
        Then I should see "testcustomer" in the "This month" column
            And I should see "testcustomer" in the "Longer Range" column

    @javascript
    Scenario: Check when setting a reminder for a lead it gets assigned to the lead owner
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | future target | type         | user          |
                | testcustomer | Rick   | No contact | Normal   | true          | New Purchase | serviceadmin |
        When I go to the view page for the "testcustomer" lead
            And I click the "New" button in the "reminders" section
            And I select "Phone Call" from "reminder_form_type"
            And I fill in the following:
                | reminder_form_date   | 2000-01-01 |
                | reminder_form_detail | test reminder |
            And I press "Save" on the current modal
        Then I should see "megaadmin" in the "reminders" section

    @javascript
    Scenario: Change a lead status to Contact made
        Given I have the following customers:
                | name         |
                | testcustomer |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | user          | type         | createdAt           |
                | testcustomer | Rick   | No contact | megaadmin     | New Purchase | 2000-01-01 00:00:01 |
        When I go to "/leads"
        Then I should see "Current Month: Leads In 1, Leads contacted 0.00% (0)"

    @javascript
    Scenario: Check future targets with no reminder is in the correct column
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | future target | type         | user      |
                | testcustomer | Rick   | No contact | Normal   | true          | New Purchase | megaadmin |
        When I go to "/leads/futureTargets/list/1"
        Then I should see "testcustomer" in the "No Reminder" column

    @javascript
    Scenario: Check future targets with reminder in the past shows up in primary targets
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | future target | type         | user      |
                | testcustomer | Rick   | No contact | Normal   | true          | New Purchase | megaadmin |
            And I have the following reminders:
                | type          | detail         | date      | user      | lead         |
                | Future Target | test reminder  | yesterday | megaadmin | testcustomer |
        When I go to "/leads/futureTargets/list/1"
            And I should not see "testcustomer" in the "No Reminder" column
            And I go to "/leads/primary-targets/1"
            And I should see "testcustomer" in the "Action required" column

    @javascript
    Scenario: Add a contact to a primary target in the form
        Given I am authenticated as "megaadmin"
            And I have the following customers:
                | name         |
                | testcustomer |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | summary   | status     | user          | type         | primaryTarget |
                | testcustomer | Rick   | test lead | No contact | megaadmin     | New Purchase | true           |
        When I go to "/leads/primary-targets/1"
            And I click on "testcustomer" in the "No contact" column
            And I follow "Edit"
            And I follow "addNewContact"
            And I fill in the following:
                | lead_form_newContacts_0_name      | Rick Grimes     |
                | lead_form_newContacts_0_telephone | 1543587         |
                | lead_form_newContacts_0_email     | rick@grimes.com |
            And I press "Save" on the current modal
            And I go to the view page for the "testcustomer" lead
        Then I should see "Rick Grimes"

    @javascript
    Scenario: Check all contact details are saving correctly
        Given I have the following customers:
                | name         |
                | testcustomer |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | type         |
                | testcustomer | Rick   | No contact | New Purchase |
        When I go to the view page for the "testcustomer" lead
            And I click the "New" button in the "contacts" section
            And I fill in the following:
                | add_contact_form_name      | Rick Grimes     |
                | add_contact_form_telephone | 1543587         |
                | add_contact_form_email     | rick@grimes.com |
                | add_contact_form_jobTitle  | Sheriff         |
            And I press "Save" on the current modal
            And I go to the view page for the "Rick Grimes" contact
        Then I should see "Rick Grimes"
            And I should see "1543587"
            And I should see "rick@grimes.com"
            And I should see "Sheriff"

    @javascript
    Scenario: Check a reminder can be assigned to another user
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | future target | type         | user          |
                | testcustomer | Rick   | No contact | Normal   | true          | New Purchase | serviceadmin |
        When I go to the view page for the "testcustomer" lead
            And I click the "New" button in the "reminders" section
            And I select "Phone Call" from "reminder_form_type"
            And I select "Service Admin" from "reminder_form_user"
            And I fill in the following:
                | reminder_form_date   | 2000-01-01 |
                | reminder_form_detail | test reminder |
            And I press "Save" on the current modal
            And I wait 1 second
        Then there should be 1 email with the subject "You have been assigned a reminder for a Lead."

    @javascript
    Scenario: Check an successful lead awaiting delivery appears in the listings page
        Given I have the following customers:
                | name         | isLead |
                | testcustomer | false  |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status    | type         | delivered | successful |
                | testcustomer | Rick   | Completed | New Purchase | false     | yes        |
        When I go to "/leads/list-successful-awaiting-delivery"
            And I click on "testcustomer" in the "Leads" column
        Then I should see "Delivered to Customer"

    @javascript
    Scenario: Check an successful lead which is delivered does not appear in the listings page
        Given I have the following customers:
                | name         | isLead |
                | testcustomer | false  |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status       | type         | delivered |
                | testcustomer | Rick   | Contact made | New Purchase | true      |
        When I go to the view page for the "testcustomer" lead
        Then I should not see "Delivered to Customer"

    @javascript
    Scenario: Check delivery button on lead view page
        Given I have the following customers:
                | name         | isLead |
                | testcustomer | false  |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status    | type         | delivered |
                | testcustomer | Rick   | Completed | New Purchase | false     |
        When I go to the view page for the "testcustomer" lead
            And I click the "Delivered to customer" button in the "top" section
            And I click the "Confirm" button in the "confirm-modal" section
        Then I should not see "Delivered to customer" in the "top" section

    @javascript
    Scenario: Add expected delivery date to successful lead
        Given I have the following customers:
                | name         | isLead |
                | testcustomer | false  |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status    | type         | delivered |
                | testcustomer | Rick   | Completed | New Purchase | false     |
        When I go to the view page for the "testcustomer" lead
            And I fill in the following:
                | lead_delivery_date | 02-01-2000 |
            And I click "save_lead_delivery_date"
        Then the "lead_delivery_date" input box should have the value "02-01-2000"

    @javascript
    Scenario: Add contract renewal date to lead
        Given I have the following customers:
                | name         | isLead |
                | testcustomer | true   |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status       | type         | delivered |
                | testcustomer | Rick   | Contact made | New Purchase | false     |
        When I go to the view page for the "testcustomer" lead
            And I fill in the following:
                | lead_contract_renewal_date | 02-01-2000 |
            And I click the "button" containing "Save Date"
        Then the "lead_contract_renewal_date" input box should have the value "02-01-2000"

    @javascript
    Scenario: Add a service/parts target lead
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city | isLead |
                | testcustomer | 123 Walker Avenue       | Atlanta             | 1      |
            And I have the following sources:
                | name |
                | Rick |
        When I go to "/leads"
            And I follow "new-item"
            And I select "testcustomer" from "lead_form_customer"
            And I select "Rick" from "lead_form_source"
            And I press "Save"
            And I go to the view page for the "testcustomer" lead
            And I select "Service/Parts" from "target_selector"
            And I go to "/leads/list-service-parts-leads"
        Then I should see "testcustomer" in the "No contact" column

    @javascript
    Scenario: Mark a reminder as completed
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
            And I have the following reminders:
                | type          | detail         | date     | user      |
                | Future Target | test reminder  | tomorrow | megaadmin |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | reminder      | future target | type         |
                | testcustomer | Rick   | No contact | Normal   | test reminder | true          | New Purchase |
        When I go to "/reminders/list"
            And I click on "test reminder" in the "This week" column
            And I follow "Mark as completed"
        Then I should not see "testcustomer - test reminder"

    @javascript
    Scenario: Reactivate a completed reminder
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
            And I have the following reminders:
                | type          | detail         | date     | user      | completed |
                | Future Target | test reminder  | tomorrow | megaadmin | true      |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | reminder      | future target | type         |
                | testcustomer | Rick   | No contact | Normal   | test reminder | true          | New Purchase |
        When I go to "/reminders/view/1"
            And I follow "Reactivate reminder"
        Then I should see "No" beside "Completed:" in the "description-section" section

    @javascript
    Scenario: Add a lead to 'Requires Visit' listing page
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | type         |
                | testcustomer | Rick   | No contact | Normal   | New Purchase |
        When I go to the view page for the "testcustomer" lead
            And I follow "lead_set_visit_me"
            And I go to "/leads/visit-me/list"
        Then I should see "testcustomer"

    @javascript
    Scenario: Remove a lead to 'Visit me' listing page
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | type         | visitMe |
                | testcustomer | Rick   | No contact | Normal   | New Purchase | true    |
        When I go to the view page for the "testcustomer" lead
            And I follow "Cancel Visit"
            And I go to "/leads/visit-me/list"
        Then I should not see "testcustomer"

    @javascript
    Scenario: Archive a lead
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | type         |
                | testcustomer | Rick   | No contact | Normal   | New Purchase |
        When I go to the view page for the "testcustomer" lead
            And I follow "archive_lead"
            And I go to "/leads/archived/list"
        Then I should see "testcustomer"

    @javascript
    Scenario: Un-archive a lead
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | type         | archived |
                | testcustomer | Rick   | No contact | Normal   | New Purchase | true     |
        When I go to the view page for the "testcustomer" lead
            And I follow "reinstate_lead"
            And I go to "/leads/archived/list"
        Then I should not see "testcustomer"

    @javascript
    Scenario: Add lead with a new customer
        Given I go to "/leads"
        When I follow "new-item"
            And I follow "Add New"
            And I fill in the following:
                | lead_form_newCustomer | Lori Grimes     |
            And I press "Save" on the current modal
        Then I should see "Lori Grimes"

    @javascript
    Scenario: Check a new customer cannot be added if there is an existing customer with that name
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
        When I go to "/leads"
            And I follow "new-item"
            And I follow "Add New"
            And I fill in the following:
                | lead_form_newCustomer | testcustomer |
            And I press "Save" on the current modal
        Then I should see "A customer with the name \"testcustomer\" already exists in the database."
