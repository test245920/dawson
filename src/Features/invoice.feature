Feature: Testing out invoicing functionality

    Background:
        Given I am authenticated as "megaadmin"

    @javascript
    Scenario: Click email invoice button appears on particulars section
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | emailInvoice |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | yes          |
            And I have the following trucks:
               | make        | model  | serial  | yom  | customer     |
               | Caterpillar | CAT123 | SER2015 | 2015 | testcustomer |
            And I have the following jobs:
                | customer     | detail       | stage | truck   |
                | testcustomer | test details | 8     | SER2015 |
            And I have the following attachedFiles:
                | uuid |
                | 1234 |
            And I have the following invoices:
                | job          | stage | attachedFile |
                | test details | 1     | 1234         |
        When I go to "/invoices"
            And I click on "testcustomer" in the "Not sent" column
            And I click the "Email Invoice" button in the "invoice-particulars" section
        Then there should be 1 email with the subject "Dawson Forklift Services invoice for completed work."

    @javascript
    Scenario: Click email invoice button appears on the view page
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | emailInvoice |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | yes          |
            And I have the following trucks:
               | make        | model  | serial  | yom  | customer     |
               | Caterpillar | CAT123 | SER2015 | 2015 | testcustomer |
            And I have the following jobs:
                | customer     | detail       | stage | truck   |
                | testcustomer | test details | 8     | SER2015 |
            And I have the following attachedFiles:
                | uuid |
                | 1234 |
            And I have the following invoices:
                | job          | stage | attachedFile |
                | test details | 1     | 1234         |
        When I go to the view page for the invoice for job "test details"
            And I click "sendEmail"
        Then there should be 1 email with the subject "Dawson Forklift Services invoice for completed work."

    @javascript
    Scenario: Check email invoice button does not appear on particulars section
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | emailInvoice |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | no          |
            And I have the following trucks:
               | make        | model  | serial  | yom  | customer     |
               | Caterpillar | CAT123 | SER2015 | 2015 | testcustomer |
            And I have the following jobs:
                | customer     | detail       | stage | truck   |
                | testcustomer | test details | 8     | SER2015 |
            And I have the following invoices:
                | job          | stage |
                | test details | 1     |
        When I go to "/invoices"
            And I click on "testcustomer" in the "Not sent" column
        Then I should not see "Email Invoice"

    @javascript
    Scenario: Click email invoice button does not appear on the view page
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | emailInvoice |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | no          |
            And I have the following trucks:
               | make        | model  | serial  | yom  | customer     |
               | Caterpillar | CAT123 | SER2015 | 2015 | testcustomer |
            And I have the following jobs:
                | customer     | detail       | stage | truck   |
                | testcustomer | test details | 8     | SER2015 |
            And I have the following attachedFiles:
                | uuid |
                | 1234 |
            And I have the following invoices:
                | job          | stage | attachedFile |
                | test details | 2     | 1234         |
        When I go to the view page for the invoice for job "test details"
        Then I should not see "Email Invoice"

    @javascript
    Scenario: Set as sent on particulars
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | emailInvoice |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | no           |
            And I have the following trucks:
               | make        | model  | serial  | yom  | customer     |
               | Caterpillar | CAT123 | SER2015 | 2015 | testcustomer |
            And I have the following jobs:
                | customer     | detail       | stage | truck   |
                | testcustomer | test details | 8     | SER2015 |
            And I have the following attachedFiles:
                | uuid |
                | 1234 |
            And I have the following invoices:
                | job          | stage | attachedFile |
                | test details | 1     | 1234         |
        When I go to "/invoices"
            And I click on "testcustomer" in the "Not sent" column
            And I click the "Set as sent" button in the "invoice-particulars" section
        Then I should see "testcustomer" in the "Sent" column

    @javascript
    Scenario: Set as queried on particulars
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | emailInvoice |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | no           |
            And I have the following trucks:
               | make        | model  | serial  | yom  | customer     |
               | Caterpillar | CAT123 | SER2015 | 2015 | testcustomer |
            And I have the following jobs:
                | customer     | detail       | stage | truck   |
                | testcustomer | test details | 8     | SER2015 |
            And I have the following attachedFiles:
                | uuid |
                | 1234 |
            And I have the following invoices:
                | job          | stage | attachedFile |
                | test details | 2     | 1234         |
        When I go to "/invoices"
            And I click on "testcustomer" in the "Sent" column
            And I click the "Set as queried" button in the "invoice-particulars" section
        Then I should see "testcustomer" in the "Queried" column

    @javascript
    Scenario: Set as paid on particulars
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | emailInvoice |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | no           |
            And I have the following trucks:
               | make        | model  | serial  | yom  | customer     |
               | Caterpillar | CAT123 | SER2015 | 2015 | testcustomer |
            And I have the following jobs:
                | customer     | detail       | stage | truck   |
                | testcustomer | test details | 8     | SER2015 |
            And I have the following attachedFiles:
                | uuid |
                | 1234 |
            And I have the following invoices:
                | job          | stage | attachedFile |
                | test details | 2     | 1234         |
        When I go to "/invoices"
            And I click on "testcustomer" in the "Sent" column
            And I click the "Set as paid" button in the "invoice-particulars" section
        Then I should see "testcustomer" in the "Paid" column

    @javascript
    Scenario: Set as sent on view page
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | emailInvoice |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | no           |
            And I have the following trucks:
               | make        | model  | serial  | yom  | customer     |
               | Caterpillar | CAT123 | SER2015 | 2015 | testcustomer |
            And I have the following jobs:
                | customer     | detail       | stage | truck   |
                | testcustomer | test details | 8     | SER2015 |
            And I have the following attachedFiles:
                | uuid |
                | 1234 |
            And I have the following invoices:
                | job          | stage | attachedFile |
                | test details | 1     | 1234         |
        When I go to "/invoices"
            And I click on "testcustomer" in the "Not sent" column
            And I follow "View"
            And I follow "Set as sent"
        Then I should see "Sent" beside "Status:" in the "main-information" section

    @javascript
    Scenario: Set as queried on view page
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | emailInvoice |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | no           |
            And I have the following trucks:
               | make        | model  | serial  | yom  | customer     |
               | Caterpillar | CAT123 | SER2015 | 2015 | testcustomer |
            And I have the following jobs:
                | customer     | detail       | stage | truck   |
                | testcustomer | test details | 8     | SER2015 |
            And I have the following attachedFiles:
                | uuid |
                | 1234 |
            And I have the following invoices:
                | job          | stage | attachedFile |
                | test details | 2     | 1234         |
        When I go to "/invoices"
            And I click on "testcustomer" in the "Sent" column
            And I follow "View"
            And I follow "Set as queried"
        Then I should see "Querying" beside "Status:" in the "main-information" section

    @javascript
    Scenario: Set as paid on view page
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | emailInvoice |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | no          |
            And I have the following trucks:
               | make        | model  | serial  | yom  | customer     |
               | Caterpillar | CAT123 | SER2015 | 2015 | testcustomer |
            And I have the following jobs:
                | customer     | detail       | stage | truck   |
                | testcustomer | test details | 8     | SER2015 |
            And I have the following attachedFiles:
                | uuid |
                | 1234 |
            And I have the following invoices:
                | job          | stage | attachedFile |
                | test details | 2     | 1234         |
        When I go to "/invoices"
            And I click on "testcustomer" in the "Sent" column
            And I follow "View"
            And I follow "Set as paid"
        Then I should see "Paid" beside "Status:" in the "main-information" section
