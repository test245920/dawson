Feature: Customer feature
    Add, edit, delete customers and their data, contacts and sites.

    @javascript
    Scenario: Add a Customer using the button
        Given I am authenticated as "megaadmin"
        When I go to "/customers"
            And I follow "New"
            And I fill in the following:
                | customer_form_name                    | testcustomer      |
                | customer_form_billingAddress_address1 | 14 Happy Street   |
                | customer_form_billingAddress_address2 | Wonderland        |
                | customer_form_billingAddress_city     | Belfast           |
                | customer_form_telephone               | 132456789         |
                | customer_form_email                   | test@customer.com |
                | customer_form_fax                     | 987654321         |
            And I follow "Billing Information"
            And I check "customer_form_emailInvoice"
            And I press "Save"
            And I press "Close" on the current modal
        Then I should see "testcustomer" in the "customers" section

    @javascript
    Scenario: Edit a customer's details from the listing page
        Given I am authenticated as "megaadmin"
            And I have the following customers:
                | name         | billingAddress_address1 | billingAddress_address2 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       |                         | Atlanta             |
        When I go to "/customers"
            And I click the "Edit" button in the "customers" section
            And I replace "customer_form_name" with "edit"
            And I press "Save"
        Then I should see "edit" in the "customers" section

    @javascript
    Scenario: Add a contact to a customer
        Given I am authenticated as "megaadmin"
            And I have the following customers:
                | name         |
                | testcustomer |
        When I go to "/customers"
            And I click the "Edit" button in the "customers" section
            And I follow "Add a contact"
            And I fill in the following:
                | customer_form_contacts_0_name      | Rick Grimes     |
                | customer_form_contacts_0_email     | rick@grimes.com |
                | customer_form_contacts_0_telephone | 1234567899      |
            And I press "Save"
            And I follow "testcustomer"
        Then I should see "Rick Grimes" in the "mainInformation" section

    @javascript
    Scenario: Edit a customer's contact
        Given I am authenticated as "megaadmin"
            And I have the following customers:
                | name         | contact_name | contact_telephone | contact_email   |
                | testcustomer | Rick Grimes  | 1234567899        | rick@grimes.com |
        When I go to "/customers"
            And I click the "Edit" button in the "customers" section
            And I click the "Unlock" button in the "contacts" section
            And I fill in the following:
                | customer_form_contacts_0_name      | Carl Grimes     |
                | customer_form_contacts_0_telephone | 1543587         |
                | customer_form_contacts_0_email     | carl@grimes.com |
            And I press "Save"
            And I follow "testcustomer"
        Then I should see "Carl Grimes" in the "mainInformation" section

    @javascript
    Scenario: Delete a customer's contact
        Given I am authenticated as "megaadmin"
            And I have the following customers:
                | name         | contact_name | contact_telephone | contact_email   |
                | testcustomer | Rick Grimes  | 1234567899        | rick@grimes.com |
        When I go to the view page for the "testcustomer" customer
            And I follow "Edit"
            And I should see the modal "Edit customer"
            And I click the "Remove" button in the "contacts" section
            And I press "Save"
        Then I should not see "Rick Grimes" in the "mainInformation" section

    @javascript
    Scenario: Add a site address to a customer
        Given I am authenticated as "megaadmin"
            And I have the following customers:
                | name         |
                | testcustomer |
        When I go to the view page for the "testcustomer" customer
            And I follow "Edit"
            And I click the "Add a site address" button in the "siteAddresses" section
            And I fill in the following:
                | customer_form_siteAddresses_0_name     | test site         |
                | customer_form_siteAddresses_0_address1 | 14 Happy Street   |
                | customer_form_siteAddresses_0_address2 | Wonderland        |
                | customer_form_siteAddresses_0_city     | Belfast           |
                | customer_form_siteAddresses_0_postcode | BT3 9DT           |
            And I press "Save"
        Then I should see "test site" in the "details" section

    @javascript
    Scenario: Edit a customer's site address
        Given I am authenticated as "megaadmin"
            And I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
        When I go to the view page for the "testcustomer" customer
            And I follow "Edit"
            And I click the "Unlock" button in the "siteAddresses" section
            And I fill in the following:
                | customer_form_siteAddresses_0_name | Secondary Site |
            And I press "Save"
        Then I should see "Secondary Site" in the "details" section

    @javascript
    Scenario: Delete a customer's site address
        Given I am authenticated as "megaadmin"
            And I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
        When I go to the view page for the "testcustomer" customer
            And I follow "Edit"
            And I should see the modal "Edit customer"
            And I click the "Remove" button in the "siteAddresses" section
            And I press "Save"
        Then I should not see "Main Site" in the "details" section

    @javascript
    Scenario: Delete a customer
        Given I am authenticated as "megaadmin"
            And I have the following customers:
                | name         | billingAddress_address1 | billingAddress_address2 | billingAddress_city | billingAddress_postcode |
                | testcustomer | 123 Walker Avenue       |                         | Atlanta             | AT17 3DE                |
        When I go to the view page for the "testcustomer" customer
            And I follow "Delete"
            And I press "Delete" on the current modal
            And I go to "/customers"
        Then I should not see "testcustomer" in the "customers" section

    @javascript
    Scenario: Edit a customer's billing address
        Given I am authenticated as "megaadmin"
            And I have the following customers:
                | name         | contact_name | contact_telephone | contact_email   |
                | testcustomer | Rick Grimes  | 1234567899        | rick@grimes.com |
        When I go to "/customers"
            And I click the "Edit" button in the "customers" section
            And I fill in the following:
                | customer_form_billingAddress_address1 | 1 Street Name |
                | customer_form_billingAddress_address2 | Test area     |
                | customer_form_billingAddress_city     | Test city     |
                | customer_form_billingAddress_postcode | TS37 4PQ      |
            And I press "Save"
            And I follow "testcustomer"
        Then I should see "1 Street Name, Test area, Test city, TS37 4PQ." in the "details" section

    @javascript
    Scenario: Edit a customer's site address and lock it
        Given I am authenticated as "megaadmin"
            And I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
        When I go to the view page for the "testcustomer" customer
            And I click the "View Map" link in the "site_address_1" section
        Then I should see a map popover

    @javascript
    Scenario: Check adding a second customer from the form works
        Given I am authenticated as "megaadmin"
        When I go to "/customers"
            And I follow "New"
            And I fill in the following:
                | customer_form_name                    | testcustomer      |
                | customer_form_billingAddress_address1 | 14 Happy Street   |
                | customer_form_billingAddress_address2 | Wonderland        |
                | customer_form_billingAddress_city     | Belfast           |
                | customer_form_telephone               | 132456789         |
                | customer_form_email                   | test@customer.com |
                | customer_form_fax                     | 987654321         |
            And I follow "Billing Information"
            And I check "customer_form_emailInvoice"
            And I press "Save" on the current modal
            And I follow "Create another new customer"
        Then I should see "Add another customer"

    @javascript
    Scenario: Creating a new job after adding a customer
        Given I am authenticated as "megaadmin"
        When I go to "/customers"
            And I follow "New"
            And I fill in the following:
                | customer_form_name                    | testcustomer      |
                | customer_form_billingAddress_address1 | 14 Happy Street   |
                | customer_form_billingAddress_address2 | Wonderland        |
                | customer_form_billingAddress_city     | Belfast           |
                | customer_form_telephone               | 132456789         |
                | customer_form_email                   | test@customer.com |
                | customer_form_fax                     | 987654321         |
            And I follow "Billing Information"
            And I check "customer_form_emailInvoice"
            And I press "Save"
            And I follow "Create a new job"
        Then I should see "Add Job"

    @javascript
    Scenario: Check adding a second customer from the form works
        Given I am authenticated as "megaadmin"
        When I go to "/customers"
            And I follow "New"
            And I fill in the following:
                | customer_form_name                    | testcustomer      |
                | customer_form_billingAddress_address1 | 14 Happy Street   |
                | customer_form_billingAddress_address2 | Wonderland        |
                | customer_form_billingAddress_city     | Belfast           |
                | customer_form_telephone               | 132456789         |
                | customer_form_email                   | test@customer.com |
                | customer_form_fax                     | 987654321         |
            And I follow "Billing Information"
            And I check "customer_form_emailInvoice"
            And I press "Save"
            And I follow "Create a new truck"
        Then I should see "Add Truck"
