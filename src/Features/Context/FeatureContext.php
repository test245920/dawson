<?php

namespace App\Features\Context;

use App\Command\ScheduledEmailSendingCommand;
use App\DataFixtures\ORM\LoadRoles;
use App\DataFixtures\ORM\LoadUsers;
use App\DataFixtures\ORM\SetLoggableUsername;
use App\Entity\AssetLifecyclePeriod;
use App\Entity\AssetMaintenance;
use App\Entity\AssetTransaction;
use App\Entity\AttachedFile;
use App\Entity\BillingAddress;
use App\Entity\Contact;
use App\Entity\Customer;
use App\Entity\CustomerNumber;
use App\Entity\Estimate;
use App\Entity\EstimateItem;
use App\Entity\Invoice;
use App\Entity\Job;
use App\Entity\Lead;
use App\Entity\Location;
use App\Entity\Note;
use App\Entity\PurchaseOrder;
use App\Entity\PurchaseOrderItem;
use App\Entity\Reminder;
use App\Entity\SiteAddress;
use App\Entity\Source;
use App\Entity\Stock;
use App\Entity\StockItem;
use App\Entity\Supplier;
use App\Entity\SupplierStock;
use App\Entity\Truck;
use App\Entity\TruckAttachment;
use App\Entity\TruckMake;
use App\Entity\TruckModel;
use App\Entity\User;
use App\Entity\WorkLog;
use App\Model\TransactionCategory;
use Behat\Behat\Event\StepEvent;
use Behat\CommonContexts\DoctrineFixturesContext;
use Behat\Gherkin\Node\TableNode;
use Behat\Mink\Driver\Selenium2Driver;
use Behat\Mink\Exception\ElementNotFoundException;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareInterface;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Bridge\Doctrine\DataFixtures\ContainerAwareLoader as DataFixturesLoader;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

require_once 'PHPUnit/Autoload.php';
require_once 'PHPUnit/Framework/Assert/Functions.php';

/**
 * Feature context.
 */
class FeatureContext extends MinkContext implements KernelAwareInterface
{
    private $em;

    private $driver;

    private ?KernelInterface $kernel = null;

    private ?object $timeService = null;

    private $currentSession; // this is the session that is set when "authenticated as 'username'" is called

    /**
     * Initializes context with parameters from behat.yml.
     */
    public function __construct(private readonly array $parameters)
    {
        $this->useContext('doctrine_fixtures_context', new DoctrineFixturesContext());
    }

    /**
     * Example of using DoctrineFixturesContext in BeforeScenario hook.
     *
     * @BeforeScenario
     */
    public function beforeScen(): void
    {
        $container         = $this->kernel->getContainer();
        $this->timeService = $this->getFromContainer(App\Time\TimeService::class);

        $loader = class_exists(DataFixturesLoader::class)
            ? new DataFixturesLoader($container)
            : (class_exists('Doctrine\Bundle\FixturesBundle\Common\DataFixtures\Loader')
                ? new DoctrineFixturesLoader($container)
                : new SymfonyFixturesLoader($container));

        $this->em = $this->kernel->getContainer()->get('doctrine.orm.entity_manager');
        $this->em->getConnection()->exec('SET FOREIGN_KEY_CHECKS=0;');

        $this->getMainContext()
            ->getSubcontext('doctrine_fixtures_context')
            ->loadFixtureClasses($loader, [SetLoggableUsername::class, LoadUsers::class, LoadRoles::class]);

        $this->currentSession = $this->getSession();

        if ($this->currentSession->getDriver() instanceof Selenium2Driver) {
            $this->currentSession->resizeWindow(1440, 900, 'current');
        }

        $purger   = new ORMPurger();
        $executor = new ORMExecutor($this->em, $purger);

        $executor->purge();

        $this->em->getConnection()->exec('
            ALTER TABLE `purchase_order` AUTO_INCREMENT = 1;
            ALTER TABLE `job` AUTO_INCREMENT = 80000;
            ALTER TABLE `estimate` AUTO_INCREMENT = 1;
            ALTER TABLE `user` AUTO_INCREMENT = 1;
            ALTER TABLE `invoice` AUTO_INCREMENT = 1;
            ALTER TABLE `attached_files` AUTO_INCREMENT = 1;
            ALTER TABLE `location` AUTO_INCREMENT = 1;
            ALTER TABLE `reminder` AUTO_INCREMENT = 1;
            ALTER TABLE `stock` AUTO_INCREMENT = 1;
        ');

        $executor->execute($loader->getFixtures(), true);
    }

    /**
     * @AfterScenario
     */
    public function afterScen(): void
    {
        if ($this->currentSession->getDriver() instanceof Selenium2Driver) {
            $javascript = <<<'JS'
                                (function(){
                                    formChanged = false;
                                })()
                JS;
            $this->currentSession->executeScript($javascript);
        }
    }

    /**
     * Sets HttpKernel instance.
     * This method will be automatically called by Symfony2Extension ContextInitializer.
     */
    public function setKernel(KernelInterface $kernel): void
    {
        if ($kernel === null) {
            exit('null kernel');
        }
        $this->kernel = $kernel;
    }

    /**
     * @Given /^I am authenticated as "([^\']*)"$/
     */
    public function iAmAuthenticatedAs($username)
    {
        $user = $this->em->getRepository(User::class)->findByUsername($username);

        if (!$user) {
            throw CreateNotFoundException('User not found');
        }

        $this->visit('/login');
        $session = $this->getFromContainer('session');

        $firewall = 'secured_area';
        $token = new UsernamePasswordToken($user[0], null, $firewall, $user[0]->getRoles());
        $session->set('_security_' . $firewall, serialize($token));
        $session->save();

        $this->driver = $this->currentSession->getDriver();
        $this->driver->setCookie($session->getName(), $session->getId());
    }

    /**
     * @Then /^I should see the modal "([^"]*)"$/
     */
    public function iShouldSeeTheModal($title): void
    {
        $this->assertElementContainsText('.modal.in .modal-header h2', $title);
    }

    /**
     * @Then /^I should see "([^"]*)" in the modal$/
     */
    public function iShouldSeeInTheModal($text): void
    {
        $this->assertElementContainsText('.modal.in', $text);
    }

    /**
     * @Then /^I should not see the modal "([^"]*)"$/
     */
    public function iShouldNotSeeTheModal($title): void
    {
        $this->assertElementNotContainsText('.modal .modal-header h2', $title);
    }

    /**
     * Take screenshot when step fails. Works only with Selenium2Driver.
     */
    public function takeScreenshotAfterFailedStep(StepEvent $event): void
    {
        if ($event->getResult() === StepEvent::FAILED) {
            $driver = $this->currentSession->getDriver();
            $step = $event->getStep();
            $path = [
                'date' => date('Ymd-Hi'),
                // 'feature' => $step->getParent()->getFeature()->getTitle(),
                // 'scenario' => $step->getParent()->getTitle(),
                // 'step' => $step->getType() . ' ' . $step->getText(),
                'file' => basename((string) $step->getParent()->getFile()),
                'line' => $step->getParent()->getLine(),
            ];
            $path = preg_replace('/[^\-\.\w]/', '_', $path);
            $imgBase = '/tmp/behat';
            $htmlBase = '/tmp/behat';
            $filename = implode('--', $path);
            // printf('Saving response: "%s"', $filename);
            // Create directories if needed
            if (!@is_dir($htmlBase)) {
                @mkdir($htmlBase, 0775, true);
            }
            if (!@is_dir($imgBase)) {
                @mkdir($imgBase, 0775, true);
            }
            file_put_contents($htmlBase . '/' . $filename . '.html', $this->currentSession->getPage()->getContent());
            if ($driver instanceof Selenium2Driver) {
                try {
                    file_put_contents($imgBase . '/' . $filename . '.png', $driver->getScreenshot());
                } catch (\Exception) {
                    printf('Unable to save image');
                }
            }
        }
    }

    /**
     * @When /^I click the "([^"]*)" containing "([^"]*)"$/
     */
    public function iClickTheContaining(string $elementType, string $contents)
    {
        $element = $this->currentSession->getPage()->find('css', sprintf('%s:contains("%s")', $elementType, $contents));

        if (!$element) {
            throw new \Exception('Cannot find a "' . $elementType . '" containing "' . $contents . '".');
        }

        $element->click();
    }

    /**
     * @Then /^I press "([^"]*)" on the top modal$/
     */
    public function iPressOnTheTopModal(string $buttonText)
    {
        $modals = $this->currentSession->getPage()->findAll('css', '.modal.in:contains("' . $buttonText . '")');

        if (!$modals) {
            throw new \Exception('Cannot find a visible modal.');
        }

        $largestZIndex = 0;
        foreach ($modals as $modal) {
            $style  = $modal->getAttribute('style');
            $pieces = explode(' ', (string) $style);
            $zIndex = intval(chop($pieces[3], ';'));

            if ($zIndex > $largestZIndex) {
                $largestZIndex = $zIndex;
                $topModal      = $modal;
            }
        }
        $button = $topModal->find('css', '.modal-footer .btn:contains("' . $buttonText . '")');
        $button->getTagName() === 'button' ? $button->click() : $topModal->clickLink($buttonText);
    }

    /**
     * @Then /^I press "([^"]*)" on the current modal$/
     */
    public function iPressOnTheCurrentModal(string $buttonText)
    {
        $modal = $this->currentSession->getPage()->find('css', '.modal.in:contains("' . $buttonText . '")');

        if (!$modal) {
            throw new \Exception('Cannot find a visible modal.');
        }

        $button = $modal->find('css', '.modal-footer .btn:contains("' . $buttonText . '")');
        $button->getTagName() === 'button' ? $button->click() : $modal->clickLink($buttonText);
    }

    /**
     * @override When /^(?:|I )check "(?P<option>(?:[^"]|\\")*)"$/
     */
    public function checkOption($option)
    {
        $field = $this->currentSession->getPage()->findField($option);

        if (!$field) {
            throw new ElementNotFoundException(
                $this->getSession(),
                'form field',
                'id|name|label|value',
                $option,
            );
        }

        if ($field->isChecked()) {
            return;
        }

        $field->click();
    }

    /**
     * override When /^(?:|I )uncheck "(?P<option>(?:[^"]|\\")*)"$/.
     */
    public function uncheckOption($option)
    {
        $field = $this->currentSession->getPage()->findField($option);

        if (!$field) {
            throw new ElementNotFoundException(
                $this->getSession(),
                'form field',
                'id|name|label|value',
                $option,
            );
        }

        if (!$field->isChecked()) {
            return;
        }

        $field->click();
    }

    /**
     * @Given I have the following customers:
     */
    public function pushCustomers(TableNode $customersTable): void
    {
        foreach ($customersTable->getHash() as $customerHash) {
            $customer = new Customer();

            if (isset($customerHash['billingAddress_address1'])) {
                $billingAddress = new BillingAddress();
                $billingAddress->setAddress1($customerHash['billingAddress_address1']);

                if (isset($customerHash['billingAddress_address2'])) {
                    $billingAddress->setAddress2($customerHash['billingAddress_address2']);
                }
                if (isset($customerHash['billingAddress_postcode'])) {
                    $billingAddress->setPostcode($customerHash['billingAddress_postcode']);
                }
                if (isset($customerHash['billingAddress_city'])) {
                    $billingAddress->setCity($customerHash['billingAddress_city']);
                }

                $billingAddress->setCustomer($customer);
                $this->em->persist($billingAddress);
            }

            if (isset($customerHash['siteAddress_name']) || isset($customerHash['siteAddress_address1'])) {
                $siteAddress = new SiteAddress();

                if (isset($customerHash['siteAddress_name'])) {
                    $siteAddress->setName($customerHash['siteAddress_name']);
                }

                if (isset($customerHash['siteAddress_address1'])) {
                    $siteAddress->setAddress1($customerHash['siteAddress_address1']);
                }

                if (isset($customerHash['siteAddress_address2'])) {
                    $siteAddress->setAddress2($customerHash['siteAddress_address2']);
                }

                if (isset($customerHash['siteAddress_postcode'])) {
                    $siteAddress->setPostcode($customerHash['siteAddress_postcode']);
                }

                if (isset($customerHash['siteAddress_city'])) {
                    $siteAddress->setCity($customerHash['siteAddress_city']);
                }

                if (isset($customerHash['stockLocation'])) {
                    $setToTrue = $this->isValueSetToTrue($customerHash['stockLocation'], 'Site address');
                    $setToTrue ? $siteAddress->setIsStockLocation(true) : $siteAddress->setIsStockLocation(false);

                    $location = new Location();
                    $location->setSiteAddress($siteAddress);
                    $this->em->persist($location);
                }

                $this->em->persist($siteAddress);
                $customer->addSiteAddress($siteAddress);
            }

            if (isset($customerHash['contact_name'])) {
                $contact = new Contact();
                $contact->setName($customerHash['contact_name']);

                if (isset($customerHash['contact_telephone'])) {
                    $contact->setTelephone($customerHash['contact_telephone']);
                }

                if (isset($customerHash['contact_mobile'])) {
                    $contact->setMobile($customerHash['contact_mobile']);
                }

                if (isset($customerHash['contact_email'])) {
                    $contact->setEmail($customerHash['contact_email']);
                }

                $contact->setCustomer($customer);
                $this->em->persist($contact);
            }

            $customer->setName($customerHash['name']);

            if (isset($customerHash['telephone'])) {
                $customer->setTelephone($customerHash['telephone']);
            }

            if (isset($customerHash['email'])) {
                $customer->setEmail($customerHash['email']);
            }

            if (isset($customerHash['fax'])) {
                $customer->setFax($customerHash['fax']);
            }

            if (isset($customerHash['isLead'])) {
                $setToTrue = $this->isValueSetToTrue($customerHash['isLead'], 'Lead');
                $setToTrue ? $customer->setIsActive(false) : $customer->setIsActive(true);
            }

            if (isset($customerHash['emailInvoice'])) {
                $setToTrue = $this->isValueSetToTrue($customerHash['emailInvoice']);
                $setToTrue ? $customer->setEmailInvoice(1) : $customer->setEmailInvoice(0);
            }

            if (isset($customerHash['labourRate'])) {
                $customer->setLabourRate($customerHash['labourRate']);
            } else {
                $customer->setLabourRate(50.00);
            }

            $this->em->persist($customer);

            $custNum = new CustomerNumber($customer);
            $letter  = strtoupper((string) $customerHash['name'][0]);

            $custNum->setLetter($letter);
            $custNum->setNumber(1);
            $custNum->setCustomerNumber($letter . '1');
            $custNum->setAssignedAt($this->timeService->getNewDateTime());

            $this->em->persist($custNum);

            $customer->getCustomerNumbers()->add($custNum);

            $this->em->persist($customer);
            $this->em->flush();
        }
    }

    /**
     * @Given /^there should be a "([^"]*)" with the "([^"]*)" "([^"]*)"$/
     */
    public function thereShouldBeAWithThe(string $entity, string $property, string $value)
    {
        $object = $this->em->getRepository('' . $entity)->findBy([$property => $value]);
        if (!$object) {
            throw new \Exception('The "' . $entity . '" with property "' . $property . '" and value "' . $value . '" was not found in the database.');
        }
    }

    /**
     * @Given /^there should not be a "([^"]*)" with the "([^"]*)" "([^"]*)"$/
     */
    public function thereShouldNotBeAWithThe(string $entity, string $property, string $value)
    {
        $object = $this->em->getRepository('' . $entity)->findBy([$property => $value]);

        if (!$object) {
            throw new \Exception('The "' . $entity . '" with property "' . $property . '" and value "' . $value . '" should not exist but it was found in the database.');
        }
    }

    /**
     * @Given I have the following attachments:
     */
    public function pushAttachments(TableNode $table): void
    {
        foreach ($table->getHash() as $hash) {
            $attachment = new TruckAttachment();

            if (isset($hash['make'])) {
                $attachment->setMake($hash['make']);
            }

            if (isset($hash['model'])) {
                $attachment->setModel($hash['model']);
            }

            if (isset($hash['serial'])) {
                $attachment->setSerial($hash['serial']);
            } else {
                $attachment->setSerial(uniqid());
            }

            if (isset($hash['yom'])) {
                $attachment->setYearOfManufacture($hash['yom']);
            }

            if (isset($hash['type'])) {
                $attachment->setType($hash['type']);
            } else {
                $attachment->setType('Rotator');
            }

            $this->em->persist($attachment);
            $this->em->flush();
        }
    }

    /**
     * @Given I have the following trucks:
     */
    public function pushTrucks(TableNode $trucksTable): void
    {
        foreach ($trucksTable->getHash() as $truckHash) {
            $truck = new Truck();
            $alp = new AssetLifecyclePeriod($truck);
            $alp->setStart($this->timeService->getNewDateTime());

            $truck->setCurrentLifecyclePeriod($alp);

            if (isset($truckHash['make'])) {
                $truckMake = $this->em->getRepository('TruckMake')->findOneBy(['name' => $truckHash['make']]);
                if (!$truckMake) {
                    $truckMake = new TruckMake();
                    $truckMake->setName($truckHash['make']);
                    $this->em->persist($truckMake);
                }
            }

            if (isset($truckHash['model'])) {
                $truckModel = new TruckModel();
                $truckModel->setName($truckHash['model']);
                $truckModel->setMake($truckMake);
                $this->em->persist($truckModel);
                $truck->setModel($truckModel);
            }

            if (isset($truckHash['customer'])) {
                $customer = $this->em->getRepository('Customer')->findOneByName($truckHash['customer']);
                $alp->setCustomer($customer);
            }

            if (isset($truckHash['siteAddress'])) {
                $site = $this->em->getRepository('SiteAddress')->findOneBy(['name' => $truckHash['siteAddress']]);
                $alp->setSiteAddress($site);
            }

            if (isset($truckHash['serial'])) {
                $truck->setSerial($truckHash['serial']);
            } else {
                $truck->setSerial(uniqid());
            }

            if (isset($truckHash['clientFleet'])) {
                $alp->setClientFleet($truckHash['clientFleet']);
            } else {
                $alp->setClientFleet(random_int(0, mt_getrandmax()));
            }

            if (isset($truckHash['yom'])) {
                $truck->setYearOfManufacture($truckHash['yom']);
            }

            if (isset($truckHash['casualHire'])) {
                $setToTrue = $this->isValueSetToTrue($truckHash['casualHire'], 'truck');
                $hireType = $setToTrue ? AssetLifecyclePeriod::PERIOD_TYPE_CASUAL : AssetLifecyclePeriod::PERIOD_TYPE_CONTRACT;
                $alp->setType($hireType);
            } else {
                $alp->setType(AssetLifecyclePeriod::PERIOD_TYPE_CONTRACT);
            }

            $this->em->persist($alp);
            $this->em->persist($truck);
            $this->em->flush();

            if (isset($truckHash['contact_name'])) {
                $contact = new Contact();
                $contact->setName($truckHash['contact_name']);

                if (isset($truckHash['contact_telephone'])) {
                    $contact->setTelephone($truckHash['contact_telephone']);
                }

                if (isset($customerHash['contact_mobile'])) {
                    $contact->setMobile($customerHash['contact_mobile']);
                }

                if (isset($truckHash['contact_email'])) {
                    $contact->setEmail($truckHash['contact_email']);
                }

                $contact->addAssetLifecyclePeriod($alp);
                $this->em->persist($contact);
                $this->em->flush();
            }
        }
    }

    /**
     * @Given I have the following jobs:
     */
    public function pushJobs(TableNode $jobsTable)
    {
        foreach ($jobsTable->getHash() as $jobHash) {
            $job = new Job();

            if (isset($jobHash['customer'])) {
                $customer = $this->em->getRepository('Customer')->findOneBy(['name' => $jobHash['customer']]);
                $job->setCustomer($customer);

                $hasSiteAddress = $customer->getSiteAddresses()->first();
                if ($hasSiteAddress) {
                    $job->setSiteAddress($customer->getSiteAddresses()->first());
                }
            }

            $user = null;

            if (isset($jobHash['engineer'])) {
                $user = $this->em->getRepository('User')->findOneByUsername($jobHash['engineer']);
                $job->addEngineer($user);
            }

            if (isset($jobHash['worklog_time'])) {
                $workLog = new WorkLog($job);
                if (isset($jobHash['worklog_user'])) {
                    $workLog->setUser($jobHash['worklog_user']);
                } else {
                    if ($user) {
                        $workLog->setUser($user);
                    }
                }

                if (isset($jobHash['worklog_desc'])) {
                    $workLog->setDescription($jobHash['worklog_desc']);
                }

                if (isset($jobHash['worklog_time'])) {
                    $workLog->setTimeLogged($jobHash['worklog_time']);
                }

                if (isset($jobHash['worklog_startDate'])) {
                    $workLog->setStartDate($jobHash['worklog_startDate']);
                }

                $this->em->persist($workLog);
                $job->addWorkLog($workLog);
            }

            if (isset($jobHash['truck'])) {
                $truck = $this->em->getRepository('Truck')->findOneBySerial($jobHash['truck']);
                $job->setAssetLifecyclePeriod($truck->getCurrentLifecyclePeriod());
            }

            if (isset($jobHash['contact'])) {
                $contact = $this->em->getRepository('Contact')->findOneByName($jobHash['contact']);
                $job->addContact($contact);
            }

            if (isset($jobHash['site'])) {
                $site = $this->em->getRepository('SiteAddress')->findOneByName($jobHash['site']);
                $job->setSiteAddress($site);
            }

            if (isset($jobHash['stage'])) {
                $job->setCompletionStage($jobHash['stage']);
                switch ($jobHash['stage']) {
                    case 1:
                        $job->setCompletionStage(1);

                        break;
                    case 2:
                        $job->setCompletionStage(2);
                        $job->setIsLive(true);
                        $engineer = $this->em->getRepository('User')->findOneByUsername('engineer');
                        $job->addEngineer($engineer);

                        break;
                    case 3:
                        $job->setCompletionStage(3);
                        // code...
                        break;
                    case 4:
                        $job->setCompletionStage(4);
                        // code...
                        break;
                    case 5:
                        $job->setCompletionStage(5);
                        $job->setCompleted(true);
                        $job->setCompletedAt($this->timeService->getNewDateTime());

                        break;
                    case 6:
                        $job->setCompletionStage(6);
                        $job->setInvoiced(true);
                        $job->setInvoicedAt($this->timeService->getNewDateTime());

                        break;
                    case 7:
                        $job->setCompletionStage(7);

                        break;
                    case 8:
                        $job->setCompletionStage(8);

                        break;

                    default:
                        throw new \Exception('The job stage "' . $jobHash['stage'] . '" does not exist in this system, choose from 1-8.');
                        break;
                }
            }

            if (isset($jobHash['detail'])) {
                $job->setDetail($jobHash['detail']);
            }

            if (isset($jobHash['completedAt'])) {
                switch (strtolower((string) $jobHash['completedAt'])) {
                    case 'now':
                        $job->setCompletedAt($this->timeService->getNewDateTime());

                        break;
                    case 'tomorrow':
                        $date = $this->timeService->getNewDateTime();
                        $date->modify('+1 day');
                        $job->setCompletedAt($date);

                        break;
                    case 'ten days':
                        $date = $this->timeService->getNewDateTime();
                        $date->modify('+10 days');
                        $job->setCompletedAt($date);

                        break;
                    case '3 months':
                        $date = $this->timeService->getNewDateTime();
                        $date->modify('+3 months');
                        $job->setCompletedAt($date);

                        break;
                    case '9 months':
                        $date = $this->timeService->getNewDateTime();
                        $date->modify('+9 months');
                        $job->setCompletedAt($date);

                        break;
                    case '18 months':
                        $date = $this->timeService->getNewDateTime();
                        $date->modify('+18 months');
                        $job->setCompletedAt($date);

                        break;
                    case '':
                        $job->setCompletedAt(null);

                        break;

                    default:
                        throw new \Exception('The completedAt time "' . $jobHash['completedAt'] . '" does not exist in the current system.');
                        break;
                }
            }

            if (isset($jobHash['jobCode'])) {
                $job->setJobCode($jobHash['jobCode']);
            } else {
                $job->setJobCode(80001);
            }

            if (isset($jobHash['breakdown'])) {
                $setToTrue = $this->isValueSetToTrue($jobHash['breakdown']);
                if ($setToTrue) {
                    $job->setUrgent(true);
                }
            }

            if (isset($jobHash['urgent'])) {
                $setToTrue = $this->isValueSetToTrue($jobHash['urgent']);
                if ($setToTrue) {
                    $job->setUrgent(true);
                }
            }

            if (isset($jobHash['loler'])) {
                $setToTrue = $this->isValueSetToTrue($jobHash['loler']);
                if ($setToTrue) {
                    $job->setisLolerJob(true);
                }
            }

            if (isset($jobHash['service'])) {
                $setToTrue = $this->isValueSetToTrue($jobHash['service']);
                if ($setToTrue) {
                    $job->setisServiceJob(true);
                }
            }

            if (isset($jobHash['general repairs'])) {
                $setToTrue = $this->isValueSetToTrue($jobHash['general repairs']);
                if ($setToTrue) {
                    $job->setUrgent(true);
                }
            }

            if (isset($jobHash['next_site_visit'])) {
                $setToTrue = $this->isValueSetToTrue($jobHash['next_site_visit']);
                if ($setToTrue) {
                    $job->setDoWithNextJob(true);
                }
            }

            if (isset($jobHash['do_with_job'])) {
                $doWithThisJob = $this->em->getRepository('Job')->findOneByDetail($jobHash['do_with_job']);
                if ($doWithThisJob) {
                    $job->setDoWithJob($doWithThisJob);
                }
            }

            $job->setJobActiveFrom($this->timeService->getNewDateTime());
            $job->setCreatedAt($this->timeService->getNewDateTime());

            $this->em->persist($job);
            $this->em->flush();
        }
    }

    /**
     * @Given I have the following users:
     */
    public function pushUsers(TableNode $usersTable): void
    {
        foreach ($usersTable->getHash() as $userHash) {
            $user = new User();

            if (isset($userHash['username'])) {
                $username = $userHash['username'];

                $user->setUsername($username);
                $user->setName($username);
                $user->setPassword($username);
                $user->setEmail($username . '@' . $username . '.com');

                if (isset($userHash['contact'])) {
                    $contact = $this->em->getRepository('Contact')->findOneBy(['name' => $userHash['contact']]);
                    $user->setContact($contact);
                }

                if (isset($userHash['token'])) {
                    $user->setpasswordResetToken($userHash['token']);
                    $user->setpasswordResetTokenExpiry($this->timeService->getNewDateTime('tomorrow'));
                }
            }

            if (isset($userHash['role'])) {
                if (strtolower((string) $userHash['role']) == 'engineer') {
                    $role = $this->em->getRepository('Role')->findOneByRole('ROLE_ENGINEER');
                    $user->addRole($role);
                }
            }

            $this->em->persist($user);

            if (isset($userHash['role'])) {
                if (strtolower((string) $userHash['role']) === 'engineer') {
                    $location = new Location();
                    $location->addEngineer($user);
                    $this->em->persist($location);
                }
            }

            $this->em->flush();
        }
    }

    /**
     * @Given I have the following estimates:
     */
    public function pushEstimates(TableNode $estimatesTable)
    {
        foreach ($estimatesTable->getHash() as $estimateHash) {
            $estimate   = new Estimate();
            $labourTime = 0;
            $travelTime = 0;

            if (isset($estimateHash['customer'])) {
                $customer = $this->em->getRepository('Customer')->findOneBy(['name' => $estimateHash['customer']]);
                $estimate->setCustomer($customer);
            }

            if (isset($estimateHash['job'])) {
                $job = $this->em->getRepository('Job')->findOneBy(['detail' => $estimateHash['job']]);
                $estimate->setJob($job);

                $estimate->setCode('EJ' . $job->getId());
            } else {
                $estimate->setCode('E1');
            }

            if (isset($estimateHash['details'])) {
                $estimate->setDetails($estimateHash['details']);
            }

            if (isset($estimateHash['totalPrice'])) {
                $estimate->setTotalPrice($estimateHash['totalPrice']);
            }

            if (isset($estimateHash['labour_hour'])) {
                $labourTime += ($estimateHash['labour_hour'] * 60);
            }

            if (isset($estimateHash['labour_minute'])) {
                $labourTime += $estimateHash['labour_minute'];
            }

            if (isset($estimateHash['travel_hour'])) {
                $travelTime += ($estimateHash['travel_hour'] * 60);
            }

            if (isset($estimateHash['travel_minute'])) {
                $travelTime += $estimateHash['travel_minute'];
            }

            if (isset($estimateHash['stage'])) {
                switch (strtolower((string) $estimateHash['stage'])) {
                    case 'pending':
                        $estimate->setCompletionStage(1);
                        $estimate->setPending(1);

                        break;
                    case 'issued':
                        $estimate->setCompletionStage(2);
                        $estimate->setIssued(1);

                        break;
                    case 'awaiting reply':
                        $estimate->setCompletionStage(3);
                        $estimate->setAwaitingReply(1);

                        break;
                    case 'rejected':
                        $estimate->setCompletionStage(4);
                        $estimate->setRejected(1);

                        break;
                    case 'accepted':
                        $estimate->setCompletionStage(5);
                        $estimate->setAccepted(1);

                        break;

                    default:
                        throw new \Exception('The status "' . $estimateHash['stage'] . '" does not exist in the current system.');
                        break;
                }
            }

            if (isset($estimateHash['specialItem'])) {
                $estimateItem = $this->em->getRepository('EstimateItem')->findOneByItem(['item' => $estimateHash['specialItem']]);

                $estimate->addEstimateItem($estimateItem);
            }

            $estimate->setCreatedAt($this->timeService->getNewDateTime());
            $estimate->setLabourTime($labourTime);
            $estimate->setTravelTime($travelTime);
            $this->em->persist($estimate);
            $this->em->flush();
        }
    }

    /**
     * @Given I have the following stock:
     */
    public function pushStock(TableNode $stocksTable): void
    {
        foreach ($stocksTable->getHash() as $stockHash) {
            $stock = new Stock();

            if (isset($stockHash['description'])) {
                $stock->setDescription($stockHash['description']);
            }

            if (isset($stockHash['code'])) {
                $stock->setCode($stockHash['code']);
            }

            if (isset($stockHash['oem'])) {
                $stock->setOemPrice($stockHash['oem']);
            }

            if (isset($stockHash['deletedAt'])) {
                $setToTrue = $this->isValueSetToTrue($stockHash['deletedAt'], 'stock');
                if ($setToTrue) {
                    $now = $this->timeService->getNewDateTime();
                    $stock->setDeletedAt($now);
                }
            }

            $this->em->persist($stock);
            $this->em->flush();

            if (isset($stockHash['supplier'])) {
                $supplier      = $this->em->getRepository('Supplier')->findOneByName($stockHash['supplier']);
                $supplierStock = new SupplierStock();

                $supplierStock->setSupplier($supplier);
                $supplierStock->setStock($stock);
                isset($stockHash['costPrice']) ? $supplierStock->setCostPrice($stockHash['costPrice']) : $supplierStock->setCostPrice(1);
                isset($stockHash['carriagePrice']) ? $supplierStock->setCarriagePrice($stockHash['carriagePrice']) : $supplierStock->setCarriagePrice(1);

                if (isset($stockHash['description'])) {
                    $supplierStock->setDescription($stockHash['description']);
                }

                if (isset($stockHash['code'])) {
                    $supplierStock->setCode($stockHash['code']);
                }

                if (isset($stockHash['issue'])) {
                    $supplierStock->setIssuePrice($stockHash['issue']);
                }

                $this->em->persist($supplierStock);
                $this->em->flush();
            }
        }
    }

    /**
     * @Given I have the following suppliers:
     */
    public function pushSuppliers(TableNode $suppliersTable): void
    {
        foreach ($suppliersTable->getHash() as $supplierHash) {
            $supplier = new Supplier();

            if (isset($supplierHash['code'])) {
                $supplier->setCode($supplierHash['code']);
            }

            if (isset($supplierHash['name'])) {
                $supplier->setName($supplierHash['name']);
            }

            if (isset($supplierHash['telephone'])) {
                $supplier->setTelephone($supplierHash['telephone']);
            }

            if (isset($supplierHash['fax'])) {
                $supplier->setFax($supplierHash['fax']);
            }

            if (isset($supplierHash['email'])) {
                $supplier->setEmail($supplierHash['email']);
            }

            $this->em->persist($supplier);
            $this->em->flush();
        }
    }

    /**
     * @Given I have the following stockItems:
     */
    public function pushStockItems(TableNode $stockItemsTable)
    {
        foreach ($stockItemsTable->getHash() as $stockItemHash) {
            $valid = isset($stockItemHash['stock'], $stockItemHash['count']);

            if (!$valid) {
                throw new \Exception('Must provide stock and count values');
            }

            $supplierStock = $this->em->getRepository('SupplierStock')->findOneByDescription($stockItemHash['stock']);

            if (!$supplierStock) {
                throw new \Exception('Supplier stock "' . $stockItemHash['stock'] . '" not found.');
            }

            $count = $stockItemHash['count'];

            if (isset($stockItemHash['location'])) {
                $siteAddress = $this->em->getRepository('SiteAddress')->findOneByName($stockItemHash['location']);
                $location    = $this->em->getRepository('Location')->findOneBySiteAddress($siteAddress);
            } else {
                $location = null;
            }

            if (isset($stockItemHash['job'])) {
                $job = $this->em->getRepository('Job')->findOneByDetail($stockItemHash['job']);
            } else {
                $job = null;
            }

            for ($i = 0; $i < $count; ++$i) {
                $stockItem = new StockItem();

                if ($location) {
                    $stockItem->setLocation($location);
                    $this->em->persist($location);
                }

                if ($job) {
                    $stockItem->setJob($job);
                }

                $stockItem->setSupplierStock($supplierStock);
                $stockItem->setStock($supplierStock->getStock());

                $this->em->persist($stockItem);
                $this->em->flush();
            }
        }
    }

    /**
     * @Given I have the following purchaseOrders:
     */
    public function pushPurchaseOrders(TableNode $purchaseOrdersTable): void
    {
        foreach ($purchaseOrdersTable->getHash() as $purchaseOrderHash) {
            $purchaseOrder = new PurchaseOrder();

            if (isset($purchaseOrderHash['customer'])) {
                $customer = $this->em->getRepository('Customer')->findOneBy(['name' => $purchaseOrderHash['customer']]);
                $purchaseOrder->setCustomer($customer);
            }

            if (isset($purchaseOrderHash['job'])) {
                $job = $this->em->getRepository('Job')->findOneBy(['jobCode' => $purchaseOrderHash['job']]);
                $purchaseOrder->setJob($job);
            }

            if (isset($purchaseOrderHash['supplier'])) {
                $supplier = $this->em->getRepository('Supplier')->findOneBy(['name' => $purchaseOrderHash['supplier']]);
                $purchaseOrder->setSupplier($supplier);
            }

            if (isset($purchaseOrderHash['description'])) {
                $purchaseOrder->setDescription($purchaseOrderHash['description']);
            }

            if (isset($purchaseOrderHash['instructions'])) {
                $purchaseOrder->setOrderInstructions($purchaseOrderHash['instructions']);
            }

            if (isset($purchaseOrderHash['orderDate']) && $purchaseOrderHash['orderDate'] == 'now') {
                $purchaseOrder->setOrderDate($this->timeService->getNewDateTime());
            }

            if (isset($purchaseOrderHash['specialItem_item'])) {
                $specialItem = new EstimateItem();

                if (isset($purchaseOrderHash['specialItem_item'])) {
                    $specialItem->setItem($purchaseOrderHash['specialItem_item']);
                }

                if (isset($purchaseOrderHash['specialItem_price'])) {
                    $specialItem->setPrice($purchaseOrderHash['specialItem_price']);
                }

                if (isset($purchaseOrderHash['specialItem_qty'])) {
                    $specialItem->setQuantity($purchaseOrderHash['specialItem_qty']);
                }

                if (isset($purchaseOrderHash['specialItem_partNumber'])) {
                    $specialItem->setPartNumber($purchaseOrderHash['specialItem_partNumber']);
                }

                $this->em->persist($specialItem);
                $this->em->flush();
                $purchaseOrder->addEstimateItem($specialItem);
            }

            $this->em->persist($purchaseOrder);
            $this->em->flush();

            if (isset($purchaseOrderHash['stockItem'])) {
                $purchaseOrderItem = new PurchaseOrderItem();

                $stock         = $this->em->getRepository('Stock')->findOneBy(['description' => $purchaseOrderHash['stockItem']]);
                $stockItem     = $this->em->getRepository('StockItem')->findOneBy(['stock' => $stock]);
                $supplier      = $this->em->getRepository('Supplier')->findOneBy(['name' => $purchaseOrderHash['supplier']]);
                $supplierStock = $this->em->getRepository('SupplierStock')->findOneBy(['stock' => $stock]);
                $price         = $supplierStock->getCostPrice();

                $purchaseOrderItem->setItem($supplierStock);
                $purchaseOrderItem->setUnitCost($price);

                if (isset($purchaseOrderHash['stockItem_qty'])) {
                    $purchaseOrderItem->setQuantity($purchaseOrderHash['stockItem_qty']);
                }

                $purchaseOrderItem->setPurchaseOrder($purchaseOrder);

                $this->em->persist($purchaseOrderItem);
                $this->em->flush();
            }
        }
    }

    /**
     * @Given I have the following sources:
     */
    public function pushSources(TableNode $sourcesTable): void
    {
        foreach ($sourcesTable->getHash() as $sourceHash) {
            $source = new Source();
            if (isset($sourceHash['name'])) {
                $source->setName($sourceHash['name']);
            }
            $this->em->persist($source);
            $this->em->flush();
        }
    }

    /**
     * @Given I have the following notes:
     */
    public function pushNotes(TableNode $notesTable)
    {
        foreach ($notesTable->getHash() as $noteHash) {
            $note = new Note();

            if (isset($noteHash['reason'])) {
                $note->setReason($noteHash['reason']);
            }

            if (isset($noteHash['detail'])) {
                $note->setDetail($noteHash['detail']);
            }

            switch (strtolower((string) $noteHash['date'])) {
                case 'now':
                    $note->setDate($this->timeService->getNewDateTime());

                    break;
                case 'yesterday':
                    $date = $this->timeService->getNewDateTime();
                    $date->modify('-1 days');
                    $note->setDate($date);

                    break;

                default:
                    throw new \Exception('The time "' . $noteHash['date'] . '" does not exist for this test.');
                    break;
            }

            if (isset($noteHash['lead'])) {
                $customer = $this->em->getRepository('Customer')->findOneByName($noteHash['lead']);
                $lead     = $this->em->getRepository('Lead')->findOneByCustomer($customer);
                if (!$lead) {
                    throw new \Exception('Could not find lead with customer "' . $noteHash['lead'] . '" in the database.');
                }
                $note->setLead($lead);
            }

            $this->em->persist($note);
            $this->em->flush();
        }
    }

    /**
     * @Given I have the following reminders:
     */
    public function pushReminders(TableNode $remindersTable): void
    {
        foreach ($remindersTable->getHash() as $reminderHash) {
            $reminder = new Reminder();

            if (isset($reminderHash['type'])) {
                $reminder->setType($reminderHash['type']);
            }

            if (isset($reminderHash['detail'])) {
                $reminder->setDetail($reminderHash['detail']);
            }

            if (isset($reminderHash['user'])) {
                $user = $this->em->getRepository('User')->findOneByUsername($reminderHash['user']);
                $reminder->setUser($user);
            } else {
                $user = $this->em->getRepository('User')->findOneByUsername('megaadmin');
                $reminder->setUser($user);
            }

            if (isset($reminderHash['notified'])) {
                $setToTrue = $this->isValueSetToTrue($reminderHash['notified']);
                $setToTrue ? $reminder->setNotified(1) : $reminder->setNotified(0);
            }

            if (isset($reminderHash['lead'])) {
                $customer = $this->em->getRepository('Customer')->findOneByName($reminderHash['lead']);
                $lead     = $this->em->getRepository('Lead')->findOneByCustomer($customer);
                $reminder->setLead($lead);
            }

            switch ($reminderHash['date']) {
                case 'now':
                    $reminder->setDate($this->timeService->getNewDateTime());

                    break;
                case 'yesterday':
                    $date = $this->timeService->getNewDateTime();
                    $date->modify('-1 day');
                    $reminder->setDate($date);

                    break;
                case 'tomorrow':
                    $date = $this->timeService->getNewDateTime();
                    $date->modify('+1 day');
                    $reminder->setDate($date);

                    break;
                case 'ten days':
                    $date = $this->timeService->getNewDateTime();
                    $date->modify('+10 days');
                    $reminder->setDate($date);

                    break;
                case '3 months':
                    $date = $this->timeService->getNewDateTime();
                    $date->modify('+3 months');
                    $reminder->setDate($date);

                    break;
                case '9 months':
                    $date = $this->timeService->getNewDateTime();
                    $date->modify('+9 months');
                    $reminder->setDate($date);

                    break;
                case '18 months':
                    $date = $this->timeService->getNewDateTime();
                    $date->modify('+18 months');
                    $reminder->setDate($date);

                    break;

                default:
                    $reminder->setDate($this->timeService->getNewDateTime());

                    break;
            }

            if (isset($reminderHash['completed'])) {
                $setToTrue = $this->isValueSetToTrue($reminderHash['completed']);
                $setToTrue ? $reminder->setIsCompleted(1) : $reminder->setIsCompleted(0);

                if ($setToTrue) {
                    $now = $this->timeService->getNewDateTime();
                    $reminder->setIsCompletedAt($now);
                }
            }

            $this->em->persist($reminder);
            $this->em->flush();
        }
    }

    /**
     * @Given I have the following leads:
     */
    public function pushLeads(TableNode $leadsTable): void
    {
        foreach ($leadsTable->getHash() as $leadHash) {
            $lead = new Lead();

            if (isset($leadHash['status'])) {
                match ($leadHash['status']) {
                    'No contact' => $lead->setCompletionStage(1),
                    'Contact made' => $lead->setCompletionStage(2),
                    'Action Required' => $lead->setCompletionStage(3),
                    'Quoting' => $lead->setCompletionStage(5),
                    'Completed' => $lead->setCompletionStage(null),
                    default => throw new \Exception('The status "' . $leadHash['status'] . '" does not exist in the current system.'),
                };
                $lead->setStatus($leadHash['status']);
            }

            if (isset($leadHash['type'])) {
                match ($leadHash['type']) {
                    'New Purchase' => $lead->setType($leadHash['type']),
                    'Used Hire' => $lead->setType($leadHash['type']),
                    'Casual Hire' => $lead->setType($leadHash['type']),
                    'Other' => $lead->setType($leadHash['type']),
                    default => throw new \Exception('The type "' . $leadHash['type'] . '" does not exist in the current system.'),
                };
            }

            if (isset($leadHash['priority'])) {
                match (strtolower((string) $leadHash['priority'])) {
                    'normal' => $lead->setPriority('Normal'),
                    'important' => $lead->setPriority('Important'),
                    'urgent' => $lead->setPriority('Urgent'),
                    'other' => $lead->setPriority('Other'),
                    default => throw new \Exception('The priority "' . $leadHash['priority'] . '" does not exist in the current system.'),
                };
            }

            if (isset($leadHash['primaryTarget'])) {
                $setToTrue = $this->isValueSetToTrue($leadHash['primaryTarget']);
                $setToTrue ? $lead->setPrimaryTarget(true) : $lead->setPrimaryTarget(false);
            }

            if (isset($leadHash['unsuccessful'])) {
                $setToTrue = $this->isValueSetToTrue($leadHash['unsuccessful']);
                $setToTrue ? $lead->setUnsuccessful(true) : $lead->setUnsuccessful(false);
            }

            if (isset($leadHash['successful'])) {
                match (strtolower((string) $leadHash['successful'])) {
                    'yes' => $lead->setUnsuccessful(false),
                    'no' => $lead->setUnsuccessful(true),
                    'pending' => $lead->setUnsuccessful(false),
                    default => throw new \Exception('Choose from options yes/no/pending.'),
                };
            }

            if (isset($leadHash['unsuccessfulReason'])) {
                $lead->setUnsuccessful(true);
                $lead->setUnsuccessfulReason($leadHash['unsuccessfulReason']);
            }

            if (isset($leadHash['source'])) {
                $source = $this->em->getRepository('Source')->findOneBy(['name' => $leadHash['source']]);
                $lead->setSource($source);
            }

            if (isset($leadHash['note'])) {
                $note = $this->em->getRepository('Note')->findOneBy(['detail' => $leadHash['note']]);
                $lead->addNote($note);
            }

            if (isset($leadHash['reminder'])) {
                $reminder = $this->em->getRepository('Reminder')->findOneBy(['detail' => $leadHash['reminder']]);
                $lead->addReminder($reminder);
            }

            if (isset($leadHash['customer'])) {
                $customer = $this->em->getRepository('Customer')->findOneBy(['name' => $leadHash['customer']]);
                $lead->setCustomer($customer);
            }

            if (isset($leadHash['user'])) {
                $user = $this->em->getRepository('User')->findOneByUsername($leadHash['user']);
                $lead->setUser($user);
            }

            if (isset($leadHash['contact_name'])) {
                $contact = new Contact();
                $contact->setName($leadHash['contact_name']);

                if (isset($leadHash['contact_telephone'])) {
                    $contact->setTelephone($leadHash['contact_telephone']);
                }

                if (isset($customerHash['contact_mobile'])) {
                    $contact->setMobile($customerHash['contact_mobile']);
                }

                if (isset($leadHash['contact_email'])) {
                    $contact->setEmail($leadHash['contact_email']);
                }

                $contact->addLead($lead);
                $contact->setCustomer($lead->getCustomer());
                $this->em->persist($contact);
            }

            if (isset($leadHash['value'])) {
                $lead->setValue($leadHash['value']);
            }

            isset($leadHash['future target']) ? $lead->setFutureTarget(true) : $lead->setFutureTarget(false);
            isset($leadHash['priority']) ? $lead->setPriority($leadHash['priority']) : $lead->setPriority('normal');

            if (isset($leadHash['delivered'])) {
                $setToTrue = $this->isValueSetToTrue($leadHash['delivered']);
                $setToTrue ? $lead->setisDelivered(true) : $lead->setisDelivered(false);
            }

            $now = $this->timeService->getNewDateTime();

            if (isset($leadHash['deliveryDate']) && $leadHash['deliveryDate'] != '') {
                $date = $this->timeService->getNewDateTime($leadHash['deliveryDate']);
                $lead->setDeliveryDate($date);
            }

            if (isset($leadHash['completionDate']) && $leadHash['completionDate'] != '') {
                $date = $this->timeService->getNewDateTime($leadHash['completionDate']);
                $lead->setCompletionDate($date);
            }

            $this->em->persist($lead);

            if (isset($leadHash['createdAt'])) {
                $then = $this->timeService->getNewDateTime($leadHash['createdAt']);
                $lead->setReceivedDate($then);
                $lead->setInitialContact($then);
            } else {
                $lead->setReceivedDate($now);
                $lead->setInitialContact($now);
            }

            if (isset($leadHash['updatedAt'])) {
                $lead->setUpdatedAt($this->timeService->getNewDateTime($leadHash['updatedAt']));
            } else {
                $lead->setUpdatedAt($now);
            }

            if (isset($leadHash['visitMe'])) {
                $setToTrue = $this->isValueSetToTrue($leadHash['visitMe']);
                if ($setToTrue) {
                    $lead->setVisitMe(true);
                }
            }

            if (isset($leadHash['archived'])) {
                $setToTrue = $this->isValueSetToTrue($leadHash['archived']);
                if ($setToTrue) {
                    $lead->setArchived(true);
                    $lead->setArchivedAt($now);
                }
            }

            $this->em->flush();
        }
    }

    /**
     * @Given I have the following contacts:
     */
    public function pushContacts(TableNode $contactsTable): void
    {
        foreach ($contactsTable->getHash() as $contactHash) {
            $contact = new Contact();

            $contact->setName($contactHash['name']);
            $contact->setTelephone($contactHash['telephone']);
            $contact->setEmail($contactHash['email']);

            if (isset($contactHash['jobTitle'])) {
                $contact->setJobTitle($jobTitle);
            }

            if (isset($contactHash['customer'])) {
                $customer = $this->em->getRepository('Customer')->findOneBy(['name' => $contactHash['customer']]);
                $contact->setCustomer($customer);
            }

            if (isset($contactHash['truck']) && $contactHash['truck'] != '') {
                $truck = $this->em->getRepository('Truck')->findOneBySerial($contactHash['truck']);
                $contact->addAssetLifecyclePeriod($truck->getCurrentLifecyclePeriod());
            }

            if (isset($contactHash['supplier'])) {
                $supplier = $this->em->getRepository('Supplier')->findOneBy(['name' => $contactHash['supplier']]);
                $contact->setSupplier($supplier);
            }

            $this->em->persist($contact);
            $this->em->flush();
        }
    }

    /**
     * @Given I have the following special items:
     */
    public function pushEstimateItems(TableNode $estimateItemsTable)
    {
        foreach ($estimateItemsTable->getHash() as $estimateItemHash) {
            $estimateItem = new EstimateItem();

            if (isset($estimateItemHash['item'])) {
                $estimateItem->setItem($estimateItemHash['item']);
            } else {
                throw new \Exception('There must be an item description');
            }

            if (isset($estimateItemHash['price'])) {
                $estimateItem->setPrice($estimateItemHash['price']);
            } else {
                throw new \Exception('There must be an item price');
            }

            if (isset($estimateItemHash['quantity'])) {
                $estimateItem->setQuantity($estimateItemHash['quantity']);
            } else {
                throw new \Exception('There must be an item quantity');
            }

            if (isset($estimateItemHash['partNum'])) {
                $estimateItem->setPartNumber($estimateItemHash['partNum']);
            }

            if (isset($estimateItemHash['job'])) {
                $job = $this->em->getRepository('Job')->findOneByDetail($estimateItemHash['job']);
                $estimateItem->setJob($job);
            }

            $this->em->persist($estimateItem);
            $this->em->flush();
        }
    }

    /**
     * @Given I have the following site addresses:
     */
    public function pushSiteAddresss(TableNode $siteAddresssTable): void
    {
        $em = $this->getFromContainer('doctrine')->getManager();
        foreach ($siteAddresssTable->getHash() as $siteAddressHash) {
            $siteAddress = new SiteAddress();

            if (isset($siteAddressHash['name'])) {
                $siteAddress->setName($siteAddressHash['name']);
            }

            if (isset($siteAddressHash['address1'])) {
                $siteAddress->setAddress1($siteAddressHash['address1']);
            }

            if (isset($siteAddressHash['address2'])) {
                $siteAddress->setAddress2($siteAddressHash['address2']);
            }

            if (isset($siteAddressHash['city'])) {
                $siteAddress->setCity($siteAddressHash['city']);
            }

            if (isset($siteAddressHash['postcode'])) {
                $siteAddress->setPostcode($siteAddressHash['postcode']);
            }

            if (isset($siteAddressHash['stockLocation'])) {
                $setToTrue = $this->isValueSetToTrue($siteAddressHash['stockLocation'], 'Site address');
                $setToTrue ? $siteAddress->setIsStockLocation(true) : $siteAddress->setIsStockLocation(false);

                $location = new Location();
                $location->setSiteAddress($siteAddress);
                $em->persist($location);
            }

            $em->persist($siteAddress);
            $em->flush();
        }
    }

    /**
     * @AfterStep
     */
    public function afterStep(StepEvent $event)
    {
        try {
            if ($this->driver instanceof Selenium2Driver) {
                $this->currentSession->wait(50);
                $this->currentSession->wait(4000, '(window.jQuery == undefined || (0 === jQuery.active && 0 === jQuery(":animated").length))');
            }

            $this->takeScreenshotAfterFailedStep($event);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @When /^(?:|I )confirm the popup$/
     */
    public function confirmPopup(): void
    {
        if ($this->driver instanceof Selenium2Driver) {
            $this->currentSession->getDriver()->getWebDriverSession()->accept_alert();
        }
    }

    /**
     * @When /^(?:|I )cancel the popup$/
     */
    public function cancelPopup(): void
    {
        if ($this->driver instanceof Selenium2Driver) {
            $this->currentSession->getDriver()->getWebDriverSession()->dismiss_alert();
        }
    }

    /**
     * @Given /^I wait for the "([^"]*)" element to contain a "([^"]*)" option$/
     */
    public function iWaitForOptionToAppear(string $container, string $text): void
    {
        $this->currentSession->wait(4000, "$('#" . $container . "').has('option:contains(" . $text . ")').length > 0");
    }

    /**
     * @Given /^I wait for the "([^"]*)" particulars section to appear$/
     */
    public function iWaitForTheSectionToAppear(string $sectionName): void
    {
        $this->currentSession->wait(4000, "$('#" . $sectionName . "-particulars').children().length > 0");
    }

    /**
     * @Given /^I wait for the particulars section to appear$/
     */
    public function iWaitForTheParticularsSectionToAppear(): void
    {
        $this->currentSession->wait(4000, "$('#particulars-holder').children().length > 0");
    }

    /**
     * @Given /^the column "([^"]*)" should contain "([^"]*)"$/
     */
    public function theColumnShouldContain(string $columnName, string $contents)
    {
        $panelLink        = $this->currentSession->getPage()->find('css', sprintf(".panel-link:contains('%s')", $contents));
        if (!$panelLink) {
            throw new \Exception('No panel-link element with contents "' . $contents . '" was found.');
        }

        $colHeading = $panelLink->getParent()->getParent()->getParent()->find('css', '.panel-heading')->getText();
        if (!$colHeading) {
            throw new \Exception('Column "' . $columnName . '" does not contain "' . $contents . '".');
        }
    }

    /**
     * @Given /^the element "([^"]*)" containing "([^"]*)" should have the priority "([^"]*)"$/
     */
    public function theElementContainingShouldHaveThePriority($element, string $content, string $class)
    {
        $element = $this->currentSession->getPage()->find('css', sprintf('%s.%s:contains("%s")', $element, $class, $content));
        if (!$element->hasClass($class)) {
            throw new \Exception('No element "' . $element . '" with contents "' . $content . '" and priority "' . $class . '" was found.');
        }
    }

    /**
     * @Given /^I replace "([^"]*)" with "([^"]*)"$/
     */
    public function iReplaceWith($element, string $content)
    {
        $element = $this->currentSession->getPage()->find('css', '#' . $element);

        if (!$element) {
            throw new \Exception('Cannot find a "' . $element . '" containing "' . $content . '".');
        }

        $element->setValue('');
        $element->setValue($content);
    }

    /**
     * @Given /^I click on "([^"]*)" in the "([^"]*)" column$/
     */
    public function iClickOnInTheColumn(string $elementDataName, $panelBodyId)
    {
        $panelBodyId = str_replace(' ', '_', strtolower((string) $panelBodyId));
        $element     = $this->currentSession->getPage()->find('css', sprintf("#%s .panel-link[data-name='%s']", $panelBodyId, $elementDataName));

        if (!$element) {
            throw new \Exception('No column with heading "' . $panelBodyId . '" with a panel-link containing "' . $elementDataName . '" was found.');
        }

        $element->click();
    }

    /**
     * @Given /^I click on the "([^"]*)" item$/
     */
    public function iClickOnTheItem(string $elementDataName)
    {
        $element = $this->currentSession->getPage()->find('css', sprintf(".panel-link[data-name='%s']", $elementDataName));

        if (!$element) {
            throw new \Exception('No panel-link containing "' . $elementDataName . '" was found.');
        }

        $element->click();
    }

    /**
     * @Then /^I should see "([^"]*)" in the "([^"]*)" column$/
     */
    public function iShouldSeeInTheColumn(string $elementDataName, $panelBodyId)
    {
        $panelBodyId = str_replace(' ', '_', strtolower((string) $panelBodyId));
        $element = $this->currentSession->getPage()->find('css', sprintf("#%s .panel-link[data-name='%s']", $panelBodyId, $elementDataName));

        if (!$element) {
            throw new \Exception('No column with heading "' . $panelBodyId . '" with a panel-link containing "' . $elementDataName . '" was found. Check for case sensitive typos.');
        }
    }

    /**
     * @Then /^I should not see "([^"]*)" in the "([^"]*)" column$/
     */
    public function iShouldNotSeeInTheColumn(string $elementText, $panelBodyId)
    {
        $panelBodyId = str_replace(' ', '_', strtolower((string) $panelBodyId));
        $element = $this->currentSession->getPage()->find('css', sprintf("#%s .panel-link[data-name='%s']", $panelBodyId, $elementText));

        if ($element) {
            throw new \Exception('Element with text "' . $elementText . '" should not be visible but it is.');
        }
    }

    /**
     * @Given /^I click the "([^"]*)" button in the section with class name "([^"]*)"$/
     */
    public function iClickTheButtonInTheSectionWithClassName(string $elementText, string $sectionName)
    {
        $element = $this->currentSession->getPage()->find('css', sprintf('.%s a:contains("%s")', $sectionName, $elementText));

        if (!$element) {
            throw new \Exception('No section with class name "' . $sectionName . '" with a button containing "' . $elementText . '" was found.');
        }

        $element->click();
    }

    /**
     * @Then /^I should see "([^"]*)" in the "([^"]*)" section$/
     */
    public function iShouldSeeInTheSection(string $elementText, string $sectionName)
    {
        $element = $this->currentSession->getPage()->find('css', sprintf('#%s:contains("%s")', $sectionName, $elementText));

        if (!$element) {
            throw new \Exception('No section with ID "' . $sectionName . '" containing the text "' . $elementText . '" was found.');
        }
    }

    /**
     * @Then /^I should not see "([^"]*)" in the "([^"]*)" section$/
     */
    public function iShouldNotSeeInTheSection(string $elementText, $sectionName)
    {
        $element = $this->currentSession->getPage()->find('css', sprintf("#%s .panel-link[data-name='%s']", $sectionName, $elementText));

        if ($element) {
            throw new \Exception('Element with text "' . $elementText . '" should not be visible but it is.');
        }
    }

    /**
     * @Given /^I click "([^"]*)"$/
     */
    public function iClick($elementText): void
    {
        $element = $this->getLinkOrButtonInContainer($this->currentSession->getPage(), $elementText);
        $element->click();
    }

    /**
     * @Then /^I should see "([^"]*)" in the "([^"]*)" column as the colour "([^"]*)"$/
     */
    public function iShouldSeeInTheColumnAsTheColour(string $elementText, $panelBodyId, string $colour)
    {
        $priority = match (strtolower($colour)) {
            'green' => 'priority-other',
            'white' => 'priority-normal',
            'red' => 'priority-urgent',
            'amber' => 'priority-important',
            default => throw new \Exception('The colour "' . $colour . '" is not used in this system.'),
        };

        $panelBodyId = str_replace(' ', '_', strtolower((string) $panelBodyId));
        $element     = $this->currentSession->getPage()->find('css', sprintf("#%s .panel-link[data-name='%s'].%s", $panelBodyId, $elementText, $priority));

        if (!$element) {
            throw new \Exception('No element with text "' . $elementText . '" and colour "' . $colour . '" was found.');
        }
    }

    /**
     * @Given /^I wait (\d+) seconds?$/
     */
    public function iWaitSecond($seconds): void
    {
        $duration = 1000 * $seconds;
        $this->currentSession->wait($duration);
    }

    /**
     * @Then /^I should see "([^"]*)" in row (\d+) and "([^"]*)" in row (\d+) in the "([^"]*)" section$/
     */
    public function iShouldSeeInRowAndInRowInTheSection(string $text1, $row1, string $text2, $row2, string $sectionName)
    {
        $section = $this->currentSession->getPage()->find('css', '#' . $sectionName);
        if (!$section) {
            throw new \Exception('Cannot find an section with ID "' . $sectionName . '" ');
        }

        $element1 = $this->currentSession->getPage()->find('css', sprintf('#%s tbody tr:nth-child(%d):contains("%s")', $sectionName, $row1, $text1));
        if (!$element1) {
            throw new \Exception('Cannot find td element with text "' . $text1 . '" ');
        }

        $element2 = $this->currentSession->getPage()->find('css', sprintf('#%s tbody tr:nth-child(%d):contains("%s")', $sectionName, $row2, $text2));
        if (!$element2) {
            throw new \Exception('Cannot find td element with text "' . $text2 . '" ');
        }
    }

    /**
     * @When /^I go to the view page for the "([^"]*)" lead$/
     */
    public function iGoToTheViewPageForTheLead(string $name)
    {
        $customer = $this->em->getRepository('Customer')->findOneByName($name);
        $lead     = $this->em->getRepository('Lead')->findOneByCustomer($customer);

        if (!$lead) {
            throw new \Exception('No lead with customer "' . $name . '" found in the database.');
        }

        $router = $this->getFromContainer('router');
        $path   = $router->generate('view_lead', ['id' => $lead->getId()]);
        $this->visit($path);
    }

    /**
     * @When /^I go to the view page for the incomplete "([^"]*)" lead$/
     */
    public function iGoToTheViewPageForIncompleteTheLead(string $name)
    {
        $customer = $this->em->getRepository('Customer')->findOneByName($name);
        $lead     = $this->em->getRepository('Lead')->findIncompleteLeadForCustomer($customer->getId());

        if (!$lead) {
            throw new \Exception('No lead with customer "' . $name . '" found in the database.');
        }

        $router = $this->getFromContainer('router');
        $path   = $router->generate('view_lead', ['id' => $lead['id']]);
        $this->visit($path);
    }

    /**
     * @When /^I go to the view page for the invoice for job "([^"]*)"$/
     */
    public function iGoToTheViewPageForTheInvoiceForJob($detail)
    {
        $job = $this->em->getRepository('Job')->findOneByDetail($detail);
        if (!$job) {
            throw new \Exception('No job with detail "' . $name . '" found in the database.');
        }

        $invoice = $this->em->getRepository('Invoice')->findOneByJob($job->getId());
        if (!$invoice) {
            throw new \Exception('No invoice for job "' . $job->getId() . '" found in the database.');
        }

        $invoiceId = $invoice->getId();
        $router    = $this->getFromContainer('router');
        $path      = $router->generate('view_invoice', ['id' => $invoiceId]);

        $this->visit($path);
    }

    /**
     * @When /^I run the "([^"]*)" command I should see "([^"]*)" in the output$/
     */
    public function iRunTheCommand(string $name, string $output): void
    {
        $application = new Application($this->kernel);
        $application->add(new ScheduledEmailSendingCommand());

        $command       = $application->find($name);
        $commandTester = new CommandTester($command);
        $commandTester->execute(['command' => $command->getName()]);

        assertRegExp('/' . $output . '/', $commandTester->getDisplay());
    }

    /**
     * @Then /^there should be (\d+) emails? with the subject "([^"]*)"$/
     */
    public function thereShouldBeEmailsWithTheSubject($numEmails, $subject): void
    {
        $emails = $this->em->getRepository('SentEmail')->findBySubject($subject);
        assertEquals($numEmails, is_countable($emails) ? count($emails) : 0);
    }

    /**
     * @Given /^there should be a "([^"]*)" entity with the property "([^"]*)" containing "([^"]*)"$/
     */
    public function iShouldBeAEntityWithThePropertyContaining(string $entity, $property, string $value)
    {
        $object = $this->em->getRepository('' . ucfirst($entity))->findOneBy([$property => $value]);

        if (!$object) {
            throw new \Exception('No instance of type "' . $entity . '" with field "' . $field . '" with value "' . $value . '" was found in the database.');
        }
    }

    /**
     * @Given /^I should see "([^"]*)" beside "([^"]*)" in the "([^"]*)" section$/
     */
    public function iShouldSeeBesideInTheSection(string $text, string $neighbourText, string $sectionName)
    {
        $dtElement = $this->currentSession->getPage()->find('css', sprintf('#%s dt:contains("%s") + dd:contains("%s")', $sectionName, $neighbourText, $text));
        $thElement = $this->currentSession->getPage()->find('css', sprintf('#%s th:contains("%s") + td:contains("%s")', $sectionName, $neighbourText, $text));

        if (!$dtElement && !$thElement) {
            throw new \Exception('No section with ID "' . $sectionName . '" containing the text "' . $neighbourText . ' ' . $text . '" was found.');
        }
    }

    /**
     * @When /^I go to the view page for the "([^"]*)" customer$/
     */
    public function iGoToTheViewPageForTheCustomer(string $name)
    {
        $customer = $this->em->getRepository('Customer')->findOneByName($name);

        if (!$customer) {
            throw new \Exception('No customer with name "' . $name . '" found in the database.');
        }

        $router = $this->getFromContainer('router');
        $path   = $router->generate('view_customer', ['id' => $customer->getId()]);
        $this->visit($path);
    }

    /**
     * @Given /^I click the "([^"]*)" link in the "([^"]*)" section$/
     */
    public function iClickTheLinkInTheSection($elementText, string $sectionName)
    {
        $section = $this->currentSession->getPage()->find('css', sprintf('#%s', $sectionName));

        if (!$section) {
            throw new \Exception('No section with ID "' . $sectionName . '" was found.');
        }

        $element = $this->getLinkOrButtonInContainer($section, $elementText);

        $element->click();
    }

    /**
     * @Given /^I click the "([^"]*)" button in the "([^"]*)" section$/
     */
    public function iClickTheButtonInTheSection($elementText, string $sectionName): void
    {
        $this->iClickTheLinkInTheSection($elementText, $sectionName);
    }

    /**
     * @Then /^I should see a map popover$/
     */
    public function iShouldSeeAMapPopover()
    {
        $element = $this->currentSession->getPage()->find('css', '#map-popover');

        if (!$element) {
            throw new \Exception('No map popover was found');
        }
    }

    /**
     * @When /^I go to the view page for the "([^"]*)" job$/
     */
    public function iGoToTheViewPageForTheJob(string $id)
    {
        $job = $this->em->getRepository('Job')->find($id);

        if (!$job) {
            throw new \Exception('No job with job id/code "' . $id . '" found in the database.');
        }

        $router = $this->getFromContainer('router');
        $path   = $router->generate('view_job', ['id' => $job->getId()]);
        $this->visit($path);
    }

    /**
     * @Then /^I should not see option "([^"]*)" in selector "([^"]*)"$/
     */
    public function iShouldNotSeeOptionInSelector(string $option, string $selector)
    {
        $element = $this->currentSession->getPage()->find('css', sprintf('#%s option:contains("%s")', $selector, $option));

        if ($element) {
            throw new \Exception('The option "' . $option . '" was found in selector "' . $selector . '" when it should not exist');
        }
    }

    /**
     * @Then /^the "([^"]*)" input box should have the value "([^"]*)"$/
     */
    public function theInputBoxShouldHaveTheValue(string $elementId, string $value)
    {
        $element = $this->currentSession->getPage()->find('css', sprintf('#%s', $elementId));

        if (!$element) {
            throw new \Exception('No section with ID "' . $elementId . '" was found.');
        }

        $elementValue = $element->getValue();
        $valueMatches = $elementValue == $value ? true : false;

        if (!$valueMatches) {
            throw new \Exception('The value "' . $value . '" does not match the value "' . $elementValue . '" in the element "' . $elementId . '"');
        }
    }

    /**
     * @Given /^I set the date on "([^"]*)" to "([^"]*)"$/
     */
    public function iSetTheDateOnTo(string $elementId, $date)
    {
        $element = $this->currentSession->getPage()->find('css', sprintf('#%s', $elementId));

        if (!$element) {
            throw new \Exception('No section with ID "' . $elementId . '" was found.');
        }

        $element->setValue($date);
        $datePicker = $this->currentSession->getPage()->find('css', '.datepicker');
        $datePicker->blur();
    }

    /**
     * @Given /^I manually press "([^"]*)"$/
     */
    public function manuallyPress($key): void
    {
        switch (strtolower((string) $key)) {
            case 'tab':
                $key    = 9;
                $script = "jQuery.event.trigger({ type : 'keypress', keyCode : '" . $key . "' });";

                break;
            case 'enter':
                $key    = 13;
                $script = "jQuery.event.trigger({ type : 'keypress', keyCode : '" . $key . "' });";

                break;
            case 'return':
                $key    = 13;
                $script = "jQuery.event.trigger({ type : 'keypress', keyCode : '" . $key . "' });";

                break;

            default:
                $script = "jQuery.event.trigger({ type : 'keypress', which : '" . $key . "' });";

                break;
        }

        try {
            $this->currentSession->executeScript($script);
        } catch (Exception $e) {
            var_dump('[Exception] => ' . $e);
        }
    }

    /**
     * @When /^I go to the view page for the "([^"]*)" contact$/
     */
    public function iGoToTheViewPageForTheContact(string $name)
    {
        $contact = $this->em->getRepository('Contact')->findOneByName($name);

        if (!$contact) {
            throw new \Exception('No contact with name "' . $name . '" found in the database.');
        }

        $router = $this->getFromContainer('router');
        $path   = $router->generate('view_contact', ['id' => $contact->getId()]);
        $this->visit($path);
    }

    /**
     * @Given I have the following images:
     */
    public function pushImages(TableNode $imagesTable)
    {
        foreach ($imagesTable->getHash() as $imageHash) {
            $image = new Image();

            if (isset($imageHash['filename'])) {
                $image->setFileName($imageHash['filename']);
            } else {
                throw new \Exception('You must supply a filename for an image.');
            }

            if (isset($imageHash['path'])) {
                $image->setPath($imageHash['path']);
            } else {
                throw new \Exception('You must supply a path for an image.');
            }

            if (isset($imageHash['truck'])) {
                $truck = $this->em->getRepository('Truck')->findOneByName($imageHash['truck']);

                if (!$truck) {
                    throw new \Exception('No truck with name "' . $imageHash['truck'] . '" found in the database.');
                }

                $image->setTruck($truck);
            }

            $this->em->persist($image);
            $this->em->flush();
        }
    }

    /**
     * @Given /^I click the "([^"]*)" panel header$/
     */
    public function iClickThePanelHeader(string $title)
    {
        $element = $this->currentSession->getPage()->find('css', sprintf('.panel-title a:contains("%s")', $title));

        if (!$element) {
            throw new \Exception('A panel with title "' . $title . '" was not found.');
        }

        $element->click();
    }

    /**
     * @Then /^I should see an element with ID "([^"]*)"$/
     */
    public function iShouldSeeAnElementWithId(string $id)
    {
        $element = $this->currentSession->getPage()->find('css', sprintf('#"%s"', $id));
        if (!$element) {
            throw new \Exception('No element with ID "' . $id . '" was found.');
        }
    }

    /**
     * @Then /^element "([^"]*)" should contain the placeholder "([^"]*)"$/
     */
    public function elementShouldContainThePlaceholder(string $elementId, string $text)
    {
        $element = $this->currentSession->getPage()->find('css', sprintf('#%s', $elementId));

        if (!$element) {
            throw new \Exception('No element with ID "' . $elementId . '" was found.');
        }

        $placeholder = $element->getAttribute('placeholder');

        if (!$placeholder) {
            throw new \Exception('No placeholder with text "' . $text . '" was found.');
        }

        $matchingPlaceholder = $placeholder === $text;

        if (!$matchingPlaceholder) {
            throw new \Exception('The placeholder text was "' . $placeholder . '", not "' . $text . '"');
        }
    }

    /**
     * @Given I have the following invoices:
     */
    public function pushInvoices(TableNode $invoicesTable): void
    {
        $em = $this->getFromContainer('doctrine')->getManager();
        foreach ($invoicesTable->getHash() as $invoiceHash) {
            $invoice = new Invoice();

            if (isset($invoiceHash['job'])) {
                $job = $this->em->getRepository('Job')->findOneByDetail($invoiceHash['job']);
                $invoice->setJob($job);
            }

            if (isset($invoiceHash['attachedFile'])) {
                $file = $this->em->getRepository('AttachedFile')->findOneBy(['uuid' => $invoiceHash['attachedFile']]);
                $invoice->setAttachedFile($file);
            }

            if (isset($invoiceHash['stage'])) {
                $invoice->setCompletionStage($invoiceHash['stage']);
            }

            $em->persist($invoice);
            $em->flush();
        }
    }

    /**
     * @Given I have the following attachedFiles:
     */
    public function pushAttachedFiles(TableNode $attachedFilesTable): void
    {
        foreach ($attachedFilesTable->getHash() as $attachedFileHash) {
            $attachedFile = new AttachedFile();

            $attachedFile->setPath('test-data/');
            $attachedFile->setname('test.pdf');

            if (isset($attachedFileHash['uuid'])) {
                $attachedFile->setUuid($attachedFileHash['uuid']);
            } else {
                $attachedFile->setUuid(1234);
            }

            $this->em->persist($attachedFile);
            $this->em->flush();
        }
    }

    /**
     * @When /^I go to the view page for the "([^"]*)" parts requisition$/
     */
    public function iGoToTheViewPageForThePartRequisistion(string $item)
    {
        $part = $this->em->getRepository('EstimateItem')->findOneByItem($item);

        if (!$part) {
            throw new \Exception('No part requisition with item "' . $item . '" found in the database.');
        }

        $router = $this->getFromContainer('router');
        $path   = $router->generate('view_part_requisition', ['id' => $part->getId()]);
        $this->visit($path);
    }

    /**
     * @Given /^I should see an image$/
     */
    public function iShouldSeeAnImage()
    {
        $image = $this->currentSession->getPage()->find('css', 'img');

        if (!$image) {
            throw new \Exception('No img element was found.');
        }
    }

    /**
     * @When /^I go to the view page for the "([^"]*)" stock item$/
     */
    public function iGoToTheViewPageForTheStockItem(string $item)
    {
        $stock = $this->em->getRepository('Stock')->findOneByDescription($item);

        if (!$stock) {
            throw new \Exception('No stock item with description "' . $item . '" found in the database.');
        }

        $router = $this->getFromContainer('router');
        $path   = $router->generate('view_stock', ['id' => $stock->getId()]);
        $this->visit($path);
    }

    /**
     * @When /^I go to the view page for the "([^"]*)" stock location$/
     */
    public function iGoToTheViewPageForTheStockLocation(string $name)
    {
        $site = $this->em->getRepository('SiteAddress')->findOneByName($name);

        if (!$site) {
            throw new \Exception('No stock location with name "' . $name . '" was found in the database.');
        }

        $router = $this->getFromContainer('router');
        $path   = $router->generate('view_stock_location', ['id' => $site->getId()]);
        $this->visit($path);
    }

    /**
     * @Given I have the following locations:
     */
    public function pushLocations(TableNode $locationsTable)
    {
        $em = $this->getFromContainer('doctrine')->getManager();

        foreach ($locationsTable->getHash() as $locationHash) {
            $location = new Location();

            if (isset($locationHash['siteAddress_name'])) {
                $siteAddress = new SiteAddress();
                $siteAddress->setIsStockLocation(true);

                if (isset($locationHash['siteAddress_name'])) {
                    $siteAddress->setName($locationHash['siteAddress_name']);
                }

                if (isset($locationHash['siteAddress_address1'])) {
                    $siteAddress->setAddress1($locationHash['siteAddress_address1']);
                }

                if (isset($locationHash['siteAddress_address2'])) {
                    $siteAddress->setAddress2($locationHash['siteAddress_address2']);
                }

                if (isset($locationHash['siteAddress_postcode'])) {
                    $siteAddress->setPostcode($locationHash['siteAddress_postcode']);
                }

                if (isset($locationHash['siteAddress_city'])) {
                    $siteAddress->setCity($locationHash['siteAddress_city']);
                }

                $em->persist($siteAddress);

                $location->setSiteAddress($siteAddress);

                $em->persist($location);
                $em->flush();
            } else {
                throw new \Exception('Must provide a siteAddress_name parameter');
            }
        }
    }

    /**
     * @Given /^I should see a checkbox with the label "([^"]*)"$/
     */
    public function iShouldSeeACheckboxWithTheLabel(string $label)
    {
        $standard         = $this->currentSession->getPage()->find('css', sprintf('.checkbox label:contains("%s")', $label));
        $truckFormContact = $this->currentSession->getPage()->find('css', sprintf('.checkbox:contains("%s")', $label));
        $valid            = $standard || $truckFormContact;

        if (!$valid) {
            throw new \Exception('Cannot find a checkbox with label "' . $label . '".');
        }
    }

    /**
     * @Then /^I should see "([^"]*)" and "([^"]*)" in a transaction row$/
     */
    public function iShouldSeeAndInATransactionRow(string $left, string $right)
    {
        $leftElement = $this->currentSession->getPage()->find('css', sprintf('#transactions tr td:contains("%s")', $left));
        $rightElement = $this->currentSession->getPage()->find('css', sprintf('#transactions tr td:contains("%s")', $right));

        if (!$leftElement || !$rightElement) {
            throw new \Exception('No transaction row with text "' . $left . '" and text "' . $right . '" was found.');
        }
    }

    /**
     * @Given /^I should not see the "([^"]*)" element$/
     */
    public function iShouldNotSeeTheElement(string $elementId)
    {
        $element = $this->currentSession->getPage()->find('css', sprintf('#%s', $elementId));

        if (!$element) {
            throw new \Exception('An element with ID "' . $elementId . '" was found on the page');
        }

        $isVisible = $element->isVisible();

        if ($isVisible) {
            throw new \Exception('An element with ID "' . $elementId . '" is visible when it should not be');
        }
    }

    /**
     * @Given /^I should see the "([^"]*)" element$/
     */
    public function iShouldSeeTheElement(string $elementId)
    {
        $element = $this->currentSession->getPage()->find('css', sprintf('#%s', $elementId));

        if (!$element) {
            throw new \Exception('No element with ID "' . $elementId . '" was found on the page');
        }

        $isVisible = $element->isVisible();

        if (!$isVisible) {
            throw new \Exception('An element with ID "' . $elementId . '" is visible when it should not be');
        }
    }

    /**
     * @Given /^I wait for "([^"]*)" to appear in the "([^"]*)" section$/
     */
    public function iWaitForToAppearInTheSection(string $text, string $elementId): void
    {
        $this->currentSession->wait(4000, "$('#" . $elementId . ' :contains(' . $text . ")').length > 0");
        // $element = $this->currentSession->getPage()->find('css', sprintf('#%s :contains("%s")', $elementId, $text));
    }

    /**
     * @Given /^I have the following maintenance tasks:$/
     */
    public function iHaveTheFollowingMaintenanceTasks(TableNode $table)
    {
        $em = $this->getFromContainer('doctrine')->getManager();

        foreach ($table->getHash() as $maintanenceHash) {
            $maintanence = new AssetMaintenance();
            $transaction = new AssetTransaction();

            $transaction->setTransactionCategory(TransactionCategory::OUT_TRUCK_MAINTENANCE_EXPENDITURE);

            if (isset($maintanenceHash['detail'])) {
                $maintanence->setDetail($maintanenceHash['detail']);
            } else {
                $maintanence->setDetail('default');
                $transaction->setDetail('default');
            }

            if (isset($maintanenceHash['amount'])) {
                $maintanence->setAmount($maintanenceHash['amount']);
                $transaction->setAmount($maintanenceHash['amount']);
            } else {
                $maintanence->setAmount(1);
                $transaction->setAmount(1);
            }

            if (isset($maintanenceHash['date'])) {
                $maintanence->setDate($maintanenceHash['date']);
                $transaction->setDate($maintanenceHash['date']);
            }

            if (isset($maintanenceHash['truck'])) {
                $truck = $this->em->getRepository('Truck')->findOneBySerial($maintanenceHash['truck']);

                if (!$truck) {
                    throw new \Exception('No truck with Serial "' . $maintanenceHash['truck'] . '" was found in the database.');
                }

                $maintanence->setAssetLifecyclePeriod($truck->getCurrentLifecyclePeriod());
            }

            $em->persist($maintanence);

            $transaction->setMaintenance($maintanence);
            $em->persist($transaction);
            $maintanence->setTransaction($transaction);
        }

        $em->flush();
    }

    /**
     * @When /^I go to the view page for the "([^"]*)" truck maintenance$/
     */
    public function iGoToTheViewPageForTheAssetMaintenance(string $detail)
    {
        $maintenance = $this->em->getRepository('AssetMaintenance')->findOneByDetail($detail);

        if (!$maintenance) {
            throw new \Exception('No maintenance with detail "' . $detail . '" found in the database.');
        }

        $router = $this->getFromContainer('router');
        $path   = $router->generate('view_maintenance', ['id' => $maintenance->getId()]);
        $this->visit($path);
    }

    /**
     * @Given /^I go to the view page for the "([^"]*)" source$/
     */
    public function iGoToTheViewPageForTheSource(string $sourceName)
    {
        $source = $this->em->getRepository('Source')->findOneByName($sourceName);

        if (!$source) {
            throw new \Exception('No source with name "' . $sourceName . '" was found in the database.');
        }

        $router = $this->getFromContainer('router');
        $path   = $router->generate('view_source', ['id' => $source->getId()]);
        $this->visit($path);
    }

    /**
     * @Given /^I wait for the search results to appear$/
     */
    public function iWaitForTheSearchResultsToAppear(): void
    {
        if ($this->currentSession->getDriver() instanceof Selenium2Driver) {
            $this->currentSession->wait(4000, '$("#search-result-details tr").length');
        }
    }

    /**
     * @Given /^I wait for the autocomplete results to appear$/
     */
    public function iWaitForTheAutocompleteResultsToAppear(): void
    {
        if ($this->currentSession->getDriver() instanceof Selenium2Driver) {
            $this->currentSession->wait(4000, '$(".typeahead.dropdown-menu").length');
        }
    }

    /**
     * @Given /^I focus "([^"]*)"$/
     */
    public function iFocus($element)
    {
        $element = $this->currentSession->getPage()->findField($element);

        if (!$element) {
            throw new \Exception('Cannot find element "' . $element . '" ');
        }

        $element->focus();
    }

    /**
     * @Given /^I (?:unfocus|blur) "([^"]*)"$/
     */
    public function iUnfocus($element)
    {
        $element = $this->currentSession->getPage()->findField($element);

        if (!$element) {
            throw new \Exception('Cannot find element "' . $element . '" ');
        }

        $element->blur();
    }

    /**
     * @When /^I dismiss (any|the) flash messages?$/
     */
    public function iDismissFlashMessages(): void
    {
        if (!$this->currentSession->getDriver() instanceof Selenium2Driver) {
            return;
        }

        $javascript = <<<'JS'
                        (function(){
                            $('.growl-animated.alert button.close').each(function() {
                                $(this).click();
                            });
                        })()
            JS;
        $this->currentSession->executeScript($javascript);
    }

    protected function getFromContainer(string $key): ?object
    {
        return $this->kernel->getContainer()->get($key);
    }

    protected function getParam(string $key)
    {
        return $this->kernel->getContainer()->getParameter($key);
    }

    protected function getLinkOrButtonInContainer($container, $linkText)
    {
        $els       = $container->findAll('named', ['link_or_button', $this->getSession()->getSelectorsHandler()->xpathLiteral($linkText)]);
        $matchedEl = null;

        foreach ($els as $el) {
            if (!($this->driver instanceof Selenium2Driver) || $el->isVisible()) {
                $matchedEl = $el;

                break;
            }
        }

        assertNotNull($matchedEl, sprintf('Link or button "%s" not found', $linkText));

        return $matchedEl;
    }

    private function isValueSetToTrue($value, string $type = 'Options'): bool
    {
        return match (strtolower((string) $value)) {
            '1' => true,
            'yes' => true,
            'true' => true,
            '0' => false,
            'no' => false,
            'false' => false,
            default => throw new \Exception($type . ' can only be one of the following values, 1/0, yes/no, true/false.'),
        };
    }
}
