Feature: Testing out estimate functionality
    Add an estimate, respond to a customer query, accept and reject an estimate, delete an estimate

    Background:
        Given I am authenticated as "megaadmin"

    @javascript
    Scenario: Add an estimate with travel time and check correct price
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | labourRate |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | 20.00      |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | jobCode |
                | testcustomer | test details | 2     | engineer | 80000   |
        When I go to "/estimates"
            And I follow "New"
            And I fill in the following:
               | estimate_form_details  | test estimate |
            And I select "testcustomer" from "estimate_form_customer"
            And I select "80000 - testcustomer" from "estimate_form_job"
            And I select "15" from "estimate_form_travelTime_minute"
            And I press "Save"
            And I click on "testcustomer" in the "Pending" column
            And I should see "15m" in the "estimate-particulars" section
            And I should see "£5.00" in the "estimate-particulars" section
            And I go to "/jobs"
        Then I should see "testcustomer" in the "Estimate Pending" column

    @javascript
    Scenario: Add an estimate with labour time and check correct price
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | labourRate |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | 20.00      |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | jobCode |
                | testcustomer | test details | 2     | engineer | 80000   |
        When I go to "/estimates"
            And I follow "new-item"
            And I fill in the following:
               | estimate_form_details  | test estimate |
            And I select "testcustomer" from "estimate_form_customer"
            And I select "80000 - testcustomer" from "estimate_form_job"
            And I select "1" from "estimate_form_labourTime_hour"
            And I press "Save"
            And I click on "testcustomer" in the "Pending" column
        Then I should see "1hr" in the "estimate-particulars" section
            And I should see "£20.00" in the "estimate-particulars" section

    @javascript
    Scenario: Issue an estimate
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | jobCode |
                | testcustomer | test details | 1     | engineer | 80000   |
            And I have the following estimates:
                | customer     | job          | details          | labour_hour | labour_minute | totalPrice | stage   |
                | testcustomer | test details | estimate details | 1           | 15            | 500        | Pending |
        When I go to "/estimates"
            And I click on "testcustomer" in the "Pending" column
            And I click the "Issue estimate" button in the "estimate-particulars" section
            And I follow "Confirm"
        Then I should see "testcustomer" in the "Issued" column

    @javascript
    Scenario: Respond to a customer query
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | jobCode |
                | testcustomer | test details | 2     | engineer | 80000   |
            And I have the following estimates:
                | customer     | job          | details          | labour_hour | labour_minute | totalPrice | stage  |
                | testcustomer | test details | estimate details | 1           | 15            | 500        | Issued |
        When I go to "/estimates"
            And I click on "testcustomer" in the "Issued" column
            And I follow "View"
            And I fill in the following:
                | responseText | this is a response |
            And I press "Respond"
            And I wait 1 second
        Then I should see "this is a response" in the "negotiations" section

    @javascript
    Scenario: Accept an estimate
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | jobCode |
                | testcustomer | test details | 2     | engineer | 80000   |
            And I have the following estimates:
                | customer     | job          | details          | labour_hour | labour_minute | totalPrice | stage  |
                | testcustomer | test details | estimate details | 1           | 15            | 500        | Issued |
        When I go to "/estimates"
            And I click on "testcustomer" in the "Issued" column
            And I follow "View"
            And I click the "Accept" button in the "top" section
            And I follow "Confirm"
        Then the column "Approved" should contain "testcustomer"

    @javascript
    Scenario: Reject an estimate
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | jobCode |
                | testcustomer | test details | 2     | engineer | 80000   |
            And I have the following estimates:
                | customer     | job          | details          | labour_hour | labour_minute | totalPrice | stage  |
                | testcustomer | test details | estimate details | 1           | 15            | 500        | Issued |
        When I go to "/estimates"
            And I click on "testcustomer" in the "Issued" column
            And I follow "View"
            And I click the "Reject" button in the "top" section
            And I fill in the following:
                | Reason | expensive |
            And I press "Save"
        Then I should see "Yes, expensive"

    @javascript
    Scenario: Delete an estimate
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | jobCode |
                | testcustomer | test details | 2     | engineer | 80000   |
            And I have the following estimates:
                | customer     | job          | details          | labour_hour | labour_minute | totalPrice | stage  |
                | testcustomer | test details | estimate details | 1           | 15            | 500        | Issued |
        When I go to "/estimates"
            And I click on "testcustomer" in the "Issued" column
            And I follow "View"
            And I follow "Delete"
            And I press "Delete" on the current modal
        Then I should not see "testcustomer"

    @javascript
    Scenario: Check labour time calculations work for hours and minutes
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | labourRate |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | 20.00      |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | jobCode |
                | testcustomer | test details | 2     | engineer | 80000   |
        When I go to "/estimates"
            And I follow "new-item"
            And I fill in the following:
               | estimate_form_details  | test estimate |
            And I select "testcustomer" from "estimate_form_customer"
            And I select "80000 - testcustomer" from "estimate_form_job"
            And I select "1" from "estimate_form_labourTime_hour"
            And I select "45" from "estimate_form_labourTime_minute"
            And the "estimate_form_labourPrice" input box should have the value "35.00"
            And I press "Save"
            And I click on "testcustomer" in the "Pending" column
        Then I should see "1hr 45m" in the "estimate-particulars" section
            And I should see "£35.00" in the "estimate-particulars" section

    @javascript
    Scenario: Check labour time and cost calculations work for hours
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | labourRate |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | 20.00      |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | jobCode |
                | testcustomer | test details | 2     | engineer | 80000   |
        When I go to "/estimates"
            And I follow "new-item"
            And I fill in the following:
               | estimate_form_details  | test estimate |
            And I select "testcustomer" from "estimate_form_customer"
            And I select "80000 - testcustomer" from "estimate_form_job"
            And I select "4" from "estimate_form_labourTime_hour"
            And the "estimate_form_labourPrice" input box should have the value "80.00"
            And I press "Save"
            And I click on "testcustomer" in the "Pending" column
        Then I should see "4hr 0m" in the "estimate-particulars" section
            And I should see "£80.00" in the "estimate-particulars" section

    @javascript
    Scenario: Check labour time and cost calculations work for minutes
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | labourRate |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | 20.00      |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | jobCode |
                | testcustomer | test details | 2     | engineer | 80000   |
        When I go to "/estimates"
            And I follow "new-item"
            And I fill in the following:
               | estimate_form_details  | test estimate |
            And I select "testcustomer" from "estimate_form_customer"
            And I select "80000 - testcustomer" from "estimate_form_job"
            And I select "30" from "estimate_form_labourTime_minute"
            And the "estimate_form_labourPrice" input box should have the value "10.00"
            And I press "Save"
            And I click on "testcustomer" in the "Pending" column
        Then I should see "0hr 30m" in the "estimate-particulars" section
            And I should see "£10.00" in the "estimate-particulars" section

    @javascript
    Scenario: Check travel time and cost calculations work for hours and minutes
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | labourRate |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | 20.00      |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | jobCode |
                | testcustomer | test details | 2     | engineer | 80000   |
        When I go to "/estimates"
            And I follow "new-item"
            And I fill in the following:
               | estimate_form_details  | test estimate |
            And I select "testcustomer" from "estimate_form_customer"
            And I select "80000 - testcustomer" from "estimate_form_job"
            And I select "1" from "estimate_form_travelTime_hour"
            And I select "45" from "estimate_form_travelTime_minute"
            And the "estimate_form_labourPrice" input box should have the value "35.00"
            And I press "Save"
            And I click on "testcustomer" in the "Pending" column
            And I wait for the "estimate" particulars section to appear
        Then I should see "1hr 45m" in the "estimate-particulars" section
            And I should see "£35.00" in the "estimate-particulars" section

    @javascript
    Scenario: Check travel time and cost calculations work for hours
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | labourRate |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | 20.00      |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | jobCode |
                | testcustomer | test details | 2     | engineer | 80000   |
        When I go to "/estimates"
            And I follow "new-item"
            And I fill in the following:
               | estimate_form_details  | test estimate |
            And I select "testcustomer" from "estimate_form_customer"
            And I select "80000 - testcustomer" from "estimate_form_job"
            And I select "4" from "estimate_form_travelTime_hour"
            And the "estimate_form_labourPrice" input box should have the value "80.00"
            And I press "Save"
            And I click on "testcustomer" in the "Pending" column
        Then I should see "4hr 0m" in the "estimate-particulars" section
            And I should see "£80.00" in the "estimate-particulars" section

    @javascript
    Scenario: Check travel time and cost calculations work for minutes
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | labourRate |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | 20.00      |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | jobCode |
                | testcustomer | test details | 2     | engineer | 80000   |
        When I go to "/estimates"
            And I follow "new-item"
            And I fill in the following:
               | estimate_form_details  | test estimate |
            And I select "testcustomer" from "estimate_form_customer"
            And I select "80000 - testcustomer" from "estimate_form_job"
            And I select "45" from "estimate_form_travelTime_minute"
            And the "estimate_form_labourPrice" input box should have the value "15.00"
            And I press "Save"
            And I click on "testcustomer" in the "Pending" column
        Then I should see "0hr 45m" in the "estimate-particulars" section
            And I should see "£15.00" in the "estimate-particulars" section

    @javascript
    Scenario: Check time and cost calculations work for labour hours and travel hours
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | labourRate |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | 20.00      |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | jobCode |
                | testcustomer | test details | 2     | engineer | 80000   |
        When I go to "/estimates"
            And I follow "new-item"
            And I fill in the following:
               | estimate_form_details  | test estimate |
            And I select "testcustomer" from "estimate_form_customer"
            And I select "80000 - testcustomer" from "estimate_form_job"
            And I select "1" from "estimate_form_labourTime_hour"
            And I select "2" from "estimate_form_travelTime_hour"
            And the "estimate_form_labourPrice" input box should have the value "60.00"
            And I press "Save"
            And I click on "testcustomer" in the "Pending" column
        Then I should see "2hr 0m" in the "estimate-particulars" section
            And I should see "2hr 0m" in the "estimate-particulars" section
            And I should see "£60.00" in the "estimate-particulars" section

    @javascript
    Scenario: Check time and cost calculations work for labour hours and travel minutes
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | labourRate |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | 20.00      |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | jobCode |
                | testcustomer | test details | 2     | engineer | 80000   |
        When I go to "/estimates"
            And I follow "new-item"
            And I fill in the following:
               | estimate_form_details  | test estimate |
            And I select "testcustomer" from "estimate_form_customer"
            And I select "80000 - testcustomer" from "estimate_form_job"
            And I select "15" from "estimate_form_labourTime_minute"
            And I select "45" from "estimate_form_travelTime_minute"
            And the "estimate_form_labourPrice" input box should have the value "20.00"
            And I press "Save"
            And I click on "testcustomer" in the "Pending" column
        Then I should see "0hr 15m" in the "estimate-particulars" section
            And I should see "0hr 45m" in the "estimate-particulars" section
            And I should see "£20.00" in the "estimate-particulars" section

    @javascript
    Scenario: Check time and cost calculations work for labour hours and travel minutes
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | labourRate |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | 20.00      |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | jobCode |
                | testcustomer | test details | 2     | engineer | 80000   |
        When I go to "/estimates"
            And I follow "new-item"
            And I fill in the following:
               | estimate_form_details  | test estimate |
            And I select "testcustomer" from "estimate_form_customer"
            And I select "80000 - testcustomer" from "estimate_form_job"
            And I select "1" from "estimate_form_travelTime_hour"
            And I select "45" from "estimate_form_travelTime_minute"
            And I select "2" from "estimate_form_labourTime_hour"
            And I select "15" from "estimate_form_labourTime_minute"
            And the "estimate_form_labourPrice" input box should have the value "80.00"
            And I press "Save"
            And I click on "testcustomer" in the "Pending" column
        Then I should see "1hr 45m" in the "estimate-particulars" section
            And I should see "2hr 15m" in the "estimate-particulars" section
            And I should see "£80.00" in the "estimate-particulars" section

    @javascript
    Scenario: Check price persists on an estimate edit form
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | labourRate |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | 20.00      |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | jobCode |
                | testcustomer | test details | 2     | engineer | 80000   |
            And I have the following estimates:
                | customer     | job          | details          | labour_hour | labour_minute | totalPrice | stage   |
                | testcustomer | test details | estimate details | 1           | 15            | 25         | Pending |
        When I go to "/estimates"
            And I click on "testcustomer" in the "Pending" column
            And I follow "Edit"
        Then the "estimate_form_totalPrice" input box should have the value "25.00"

    @javascript
    Scenario: Check price updates on an edit form
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | labourRate |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | 20.00      |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | jobCode |
                | testcustomer | test details | 2     | engineer | 80000   |
            And I have the following estimates:
                | customer     | job          | details          | labour_hour | labour_minute | totalPrice | stage   |
                | testcustomer | test details | estimate details | 1           | 15            | 25         | Pending |
        When I go to "/estimates"
            And I click on "testcustomer" in the "Pending" column
            And I follow "Edit"
            And I select "30" from "estimate_form_labourTime_minute"
        Then the "estimate_form_totalPrice" input box should have the value "30.00"

    @javascript
    Scenario: Check price updates on an edit form when adding a specialist item of stock
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | labourRate |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | 20.00      |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | jobCode |
                | testcustomer | test details | 2     | engineer | 80000   |
            And I have the following estimates:
                | customer     | job          | details          | labour_hour | labour_minute | totalPrice | stage   |
                | testcustomer | test details | estimate details | 1           | 15            | 25         | Pending |
        When I go to "/estimates"
            And I click on "testcustomer" in the "Pending" column
            And I follow "Edit"
            And I click the "Add new" button in the "specialistItems" section
            And I fill in the following:
                | estimate_form_estimateItems_0_item       | Special item |
                | estimate_form_estimateItems_0_price      | 50.00        |
                | estimate_form_estimateItems_0_quantity   | 1            |
                | estimate_form_estimateItems_0_partNumber | XX-1         |
            And I click the "Lock" button in the "specialistItems" section
        Then the "estimate_form_totalPrice" input box should have the value "75.00"
            And the "estimate_form_labourPrice" input box should have the value "25.00"
            And the "estimate_form_partsPrice" input box should have the value "50.00"

    @javascript
    Scenario: Check price updates on an edit form when a specialist item of stock is removed
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | labourRate |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | 20.00      |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | jobCode |
                | testcustomer | test details | 2     | engineer | 80000   |
            And I have the following special items:
                | item         | price | quantity |
                | special item | 50.00 | 1        |
            And I have the following estimates:
                | customer     | job          | details          | labour_hour | labour_minute | totalPrice | stage   | specialItem  |
                | testcustomer | test details | estimate details | 1           | 15            | 75         | Pending | special item |
        When I go to "/estimates"
            And I click on "testcustomer" in the "Pending" column
            And I follow "Edit"
            And I click the "Remove" button in the "specialistItems" section
        Then the "estimate_form_totalPrice" input box should have the value "25.00"
            And the "estimate_form_labourPrice" input box should have the value "25.00"
            And the "estimate_form_partsPrice" input box should have the value "00.00"

    @javascript
    Scenario: Check a specialist item of stock is removed from estimate
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | labourRate |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | 20.00      |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | jobCode |
                | testcustomer | test details | 2     | engineer | 80000   |
            And I have the following special items:
                | item         | price | quantity |
                | special item | 50.00 | 1        |
            And I have the following estimates:
                | customer     | job          | details          | labour_hour | labour_minute | totalPrice | stage   | specialItem  |
                | testcustomer | test details | estimate details | 1           | 15            | 75         | Pending | special item |
        When I go to "/estimates"
            And I click on "testcustomer" in the "Pending" column
            And I follow "Edit"
            And I click the "Remove" button in the "specialistItems" section
            And I press "Save"
            And I wait for the "estimate" particulars section to appear
            And I wait 1 second
        Then I should see "25.00"

    @javascript
    Scenario: Check contacts filter on job selection
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following contacts:
                | name  | telephone | email             | customer     |
                | Alice | 123456789 | alice@contact.com | testcustomer |
                | Bob   | 987654321 | bob@contact.com   | testcustomer |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | contact | jobCode |
                | testcustomer | test details | 2     | engineer | Alice   | 80000   |
            And I have the following estimates:
                | customer     | job          | details          | labour_hour | labour_minute | totalPrice | stage  |
                | testcustomer | test details | estimate details | 1           | 15            | 500        | Issued |
        When I go to "/estimates"
            And I follow "New"
            And I wait 1 second
            And I should see "Alice"
            And I should see "Bob"
            And I select "80000 - testcustomer" from "estimate_form_job"
        Then I should see "Alice"
            And I should not see "Bob"

    @javascript
    Scenario: Accept an estimate in the particulars section
        Given I have the following customers:
                | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
                | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | jobCode |
                | testcustomer | test details | 2     | engineer | 80000   |
            And I have the following estimates:
                | customer     | job          | details          | labour_hour | labour_minute | price | stage  |
                | testcustomer | test details | estimate details | 1           | 15            | 500   | Issued |
        When I go to "/estimates"
            And I click on "testcustomer" in the "Issued" column
            And I click the "Accept" button in the "estimate-particulars" section
            And I follow "Confirm"
        Then the column "Approved" should contain "testcustomer"

    @javascript
    Scenario: Reject an estimate in the particulars section
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | jobCode |
                | testcustomer | test details | 2     | engineer | 80000   |
            And I have the following estimates:
                | customer     | job          | details          | labour_hour | labour_minute | price | stage  |
                | testcustomer | test details | estimate details | 1           | 15            | 500   | Issued |
        When I go to "/estimates"
            And I click on "testcustomer" in the "Issued" column
            And I click the "Reject" button in the "estimate-particulars" section
            And I fill in the following:
                | Reason | expensive |
            And I press "Save"
        Then the column "Rejected" should contain "testcustomer"
            And I should see "Yes, expensive"

    @javascript
    Scenario: Check part number gets saved on estimate item form
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode | labourRate |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              | 20.00      |
            And I have the following jobs:
                | customer     | detail       | stage | engineer |
                | testcustomer | test details | 2     | engineer |
            And I have the following estimates:
                | customer     | job          | details          | labour_hour | labour_minute | totalPrice | stage   |
                | testcustomer | test details | estimate details | 1           | 15            | 25         | Pending |
        When I go to "/estimates"
            And I click on "testcustomer" in the "Pending" column
            And I follow "Edit"
            And I click the "Add new" button in the "specialistItems" section
            And I fill in the following:
                | estimate_form_estimateItems_0_item       | Special item |
                | estimate_form_estimateItems_0_price      | 50.00        |
                | estimate_form_estimateItems_0_quantity   | 1            |
                | estimate_form_estimateItems_0_partNumber | XX-1         |
            And I press "Save"
            And I click on "testcustomer" in the "Pending" column
        Then I should see "XX-1"
