Feature: Test that stock locations can be created, edited, and deleted

    Background:
        Given I am authenticated as "megaadmin"

    @javascript
    Scenario: Create a new stock location
        Given I go to "/stock/list-locations"
        When I click the "New" button in the "top" section
            And I fill in the following:
                | site_address_form_name     | test location |
                | site_address_form_address1 | street 1      |
                | site_address_form_address2 | area 2        |
                | site_address_form_city     | foobar        |
                | site_address_form_postcode | FB10 S32      |
            And I press "Save"
        Then I should see "test location, street 1, area 2, foobar, FB10 S32"

    @javascript
    Scenario: Edit a stock location's details on the view page
        Given I have the following site addresses:
                | name          | address1 | address2 | city   | postcode | stockLocation |
                | test location | street 1 | area 2   | foobar | FB10 S32 | true          |
        When I go to the view page for the "test location" stock location
            And I follow "Edit"
            And I fill in the following:
                | site_address_form_name | edited name |
            And I press "Save"
        Then I should see "edited name" in the "description-section" section

    @javascript
    Scenario: Edit a stock location's details from the particulars section
        Given I have the following site addresses:
                | name          | address1 | address2 | city   | postcode | stockLocation |
                | test location | street 1 | area 2   | foobar | FB10 S32 | true          |
        When I go to "/stock/list-locations"
            And I click on "test location" in the "Stock location" column
            And I follow "Edit"
            And I fill in the following:
                | site_address_form_name | edited name |
            And I press "Save"
            And I wait 1 second
        Then I should see "edited name" in the "location-particulars" section
