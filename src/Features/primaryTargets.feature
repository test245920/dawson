Feature: Testing out lead functionality

    Background:
        Given I am authenticated as "megaadmin"

    @javascript
    Scenario: Add a lead for the current user
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city | isLead |
                | testcustomer | 123 Walker Avenue       | Atlanta             | 1      |
            And I have the following sources:
                | name |
                | Rick |
        When I go to "/leads/primary-targets"
            And I follow "new-item"
            And I select "testcustomer" from "lead_form_customer"
            And I select "Rick" from "lead_form_source"
            And I select "Normal" from "lead_form_priority"
            And I press "Save"
        Then I should see "testcustomer" in the "No contact" column

    @javascript
    Scenario: Assign a lead to another user on the system
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city | isLead |
                | testcustomer | 123 Walker Avenue       | Atlanta             | 1      |
            And I have the following sources:
                | name |
                | Rick |
        When I go to "/leads/primary-targets"
            And I follow "new-item"
            And I select "testcustomer" from "lead_form_customer"
            And I select "Rick" from "lead_form_source"
            And I select "Normal" from "lead_form_priority"
            And I select "Service Admin" from "lead_form_user"
            And I press "Save"
            And I wait 1 second
            And I go to the view page for the "testcustomer" lead
        Then I should see "Service Admin" beside "Owner" in the "lead-details" section

    @javascript
    Scenario: Check selecting a primary target takes you to the lead view page
        Given I have the following customers:
                | name         |
                | testcustomer |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | summary   | status     | user          | type         | primaryTarget |
                | testcustomer | Rick   | test lead | No contact | megaadmin     | New Purchase | yes           |
        When I go to "/leads/primary-targets"
            And I click on "testcustomer" in the "No contact" column
        Then the url should match "/leads/view"


