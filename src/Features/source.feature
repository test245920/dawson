Feature: Testing out Source functionality
    Add a Source, delete a Source.

    Background:
       Given I am authenticated as "megaadmin"

    @javascript
    Scenario: Add a source
        Given I go to "/sources"
            And I follow "new-item"
            And I fill in the following:
            | Name | Bob |
            And I press "Save"
        Then I should see "Bob"

    @javascript
    Scenario: Edit a source
        Given I have the following sources:
            | name |
            | Bob  |
            And I go to the view page for the "Bob" source
            And I follow "Edit"
            And I fill in the following:
                | Name | Claire |
            And I press "Save"
        Then I should see "Claire"

    @javascript
    Scenario: Delete a source
        Given I have the following sources:
            | name |
            | Bob  |
            And I go to the view page for the "Bob" source
            And I follow "Delete"
            And I press "Delete" on the current modal
        Then I should not see "Bob" in the "source_name" section
