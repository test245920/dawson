Feature: Check that the reporting functionality

    Background:
        Given I am authenticated as "megaadmin"

    @javascript
    Scenario: Check job history over given timeframe
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | engineer | completedAt |
                | testcustomer | all done     | 5     | engineer | now         |
                | testcustomer | not finished | 2     | engineer |             |
        When I go to "/reports/jobs/in-vs-out/2000-01-01/2000-01-02"
        Then I should see "Job history from"
            And I should see "not finished" in the "jobs-created" section
            And I should see "all done" in the "jobs-completed" section

    @javascript
    Scenario: Check lead history over given timeframe
        Given I go to "/reports/list/lead"
        When I fill in the following:
            | r1_start_date | 2000-01-01 |
            | r1_end_date   | 2000-01-02 |
            And I unfocus "r1_end_date"
            And I click "leadsInVsOut"
        Then I should see "Lead history from"

    @javascript
    Scenario: Check report appears for current month
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage |
                | testcustomer | test details | 1     |
        When I go to "/jobs"
            And I click the "Current Month Job History" button in the "top" section
        Then I should see "Job history for"

    @javascript
    Scenario: Check estimate history over given timeframe
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage | engineer |
                | testcustomer | happy days   | 2     | engineer |
                | testcustomer | denied       | 2     | engineer |
                | testcustomer | not resolved | 2     | engineer |
            And I have the following estimates:
                | customer     | job          | details       | labour_hour | labour_minute | price | stage    |
                | testcustomer | happy days   | happy days    | 1           | 15            | 500   | Accepted |
                | testcustomer | denied       | denied        | 1           | 15            | 500   | Rejected |
                | testcustomer | not resolved | not resolved  | 1           | 15            | 500   | Issued   |
        When I go to "/reports/estimates/in-vs-out/2000-01-01/2000-01-02"
        Then I should see "Estimate history from"
            And I should see "not resolved" in the "estimates-created" section
            And I should see "happy days" in the "estimates-completed" section
            And I should see "denied" in the "estimates-completed" section

    @javascript
    Scenario: Check Leads value at "Quoting" stage report
        Given I go to "/reports/list/lead"
        When I click "leadsValueAtQuoting"
        Then I should see "No leads in quoting stage."

    @javascript
    Scenario: Check Leads with reminders set report for empty report
        Given I go to "/reports/list/lead"
        When I click "leadWithReminders"
        Then I should see "No leads with reminders."

    @javascript
    Scenario: Check Leads with reminders set report
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
            And I have the following reminders:
                | type          | detail         | date     | user      |
                | Future Target | test reminder  | ten days | megaadmin |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | reminder      | future target | type         | user      |
                | testcustomer | Rick   | No contact | Normal   | test reminder | true          | New Purchase | megaadmin |
        When I go to "/reports/list/lead"
            And I click "leadWithReminders"
        Then I should see "Mega Admin"

    @javascript
    Scenario: Check leads in vs leads out per user
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
                | Bob Scott    | 456 Running Street      | Bangor              |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | type         | user          |
                | testcustomer | Rick   | No contact | Normal   | New Purchase | megaadmin     |
                | Bob Scott    | Rick   | No contact | Normal   | New Purchase | serviceadmin  |
        When I go to "/reports/list/lead"
        When I fill in the following:
            | r1_start_date | 2000-01-01 |
            | r1_end_date   | 2000-01-02 |
            And I unfocus "r1_end_date"
            And I select "megaadmin" from "r1_user_selector"
            And I click "leadsInVsOut"
        Then I should see "testcustomer"
            And I should not see "Bob Scott"

    @javascript
    Scenario: Check leads created or updated
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
                | Bob Scott    | 456 Running Street      | Bangor              |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | type         | user          |
                | testcustomer | Rick   | No contact | Normal   | New Purchase | megaadmin     |
                | Bob Scott    | Rick   | No contact | Normal   | New Purchase | serviceadmin  |
        When I go to "/reports/list/lead"
        When I fill in the following:
                | r4_start_date | 2000-01-01 |
            And I unfocus "r4_start_date"
            And I click "leadsCreatedOrUpdated"
        Then I should see "testcustomer"
            And I should see "Bob Scott"

    @javascript
    Scenario: Check leads created or updated per user
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
                | Bob Scott    | 456 Running Street      | Bangor              |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | type         | user          |
                | testcustomer | Rick   | No contact | Normal   | New Purchase | megaadmin     |
                | Bob Scott    | Rick   | No contact | Normal   | New Purchase | serviceadmin  |
        When I go to "/reports/list/lead"
        When I fill in the following:
            | r4_start_date | 2000-01-01 |
            And I unfocus "r4_start_date"
            And I select "megaadmin" from "r4_user_selector"
            And I click "leadsCreatedOrUpdated"
        Then I should see "testcustomer"
            And I should not see "Bob Scott"

    @javascript
    Scenario: Check Leads updated between dates
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
                | Bob Scott    | 456 Running Street      | Bangor              |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | type         | user          | updatedAt |
                | testcustomer | Rick   | No contact | Normal   | New Purchase | megaadmin     | +1 hour   |
                | Bob Scott    | Rick   | No contact | Normal   | New Purchase | serviceadmin  | +1 hour   |
        When I go to "/reports/list/lead"
        When I fill in the following:
            | r3_start_date | 2000-01-01 |
            | r3_end_date   | 2000-01-02 |
            And I unfocus "r3_end_date"
            And I select "megaadmin" from "r3_user_selector"
            And I click "leadsUpdatedBetween"
        Then I should see "testcustomer"
            And I should not see "Bob Scott"

    @javascript
    Scenario: Check Leads updated between dates per user
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
                | Bob Scott    | 456 Running Street      | Bangor              |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | type         | user          | updatedAt |
                | testcustomer | Rick   | No contact | Normal   | New Purchase | megaadmin     | +1 hour   |
                | Bob Scott    | Rick   | No contact | Normal   | New Purchase | serviceadmin  | +1 hour   |
        When I go to "/reports/list/lead"
        When I fill in the following:
            | r3_start_date | 2000-01-01 |
            | r3_end_date   | 2000-01-02 |
            And I unfocus "r3_end_date"
            And I select "megaadmin" from "r3_user_selector"
            And I click "leadsUpdatedBetween"
        Then I should see "testcustomer"
            And I should not see "Bob Scott"

    @javascript
    Scenario: Check Inactive leads over time
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
                | Bob Scott    | 456 Running Street      | Bangor              |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | type         | user          | createdAt | updatedAt |
                | testcustomer | Rick   | No contact | Normal   | New Purchase | megaadmin     | -10 days  | -10 days  |
                | Bob Scott    | Rick   | No contact | Normal   | New Purchase | serviceadmin  | -10 days  | -10 days  |
        When I go to "/reports/list/lead"
        When I fill in the following:
            | r7_start_date | 1999-12-25 |
            And I unfocus "r7_start_date"
            And I select "No contact" from "r7_status_selector"
            And I click "leadsInactiveSince"
        Then I should see "testcustomer"
            And I should see "Bob Scott"

    @javascript
    Scenario: Check Inactive leads over time per user
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
                | Bob Scott    | 456 Running Street      | Bangor              |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | type         | user          | createdAt | updatedAt |
                | testcustomer | Rick   | No contact | Normal   | New Purchase | megaadmin     | -10 days  | -10 days  |
                | Bob Scott    | Rick   | No contact | Normal   | New Purchase | serviceadmin  | -10 days  | -10 days  |
        When I go to "/reports/list/lead"
        When I fill in the following:
            | r7_start_date | 1999-12-25 |
            And I unfocus "r7_start_date"
            And I select "No contact" from "r7_status_selector"
            And I select "megaadmin" from "r7_user_selector"
            And I click "leadsInactiveSince"
        Then I should see "testcustomer"
            And I should not see "Bob Scott"

    @javascript
    Scenario: Check Leads updated between dates per user
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
                | Bob Scott    | 456 Running Street      | Bangor              |
                | Rick Grimes  | 789 52nd Street         | Bangor              |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status       | priority | type         | user      | updatedAt |
                | testcustomer | Rick   | No contact   | Normal   | New Purchase | megaadmin | +1 hour   |
                | Bob Scott    | Rick   | Contact made | Normal   | New Purchase | megaadmin | +1 hour   |
                | Rick Grimes  | Rick   | Quoting      | Normal   | New Purchase | megaadmin | +1 hour   |
        When I go to "/reports/list/lead"
        When I fill in the following:
            | r2_start_date | 2000-01-01 |
            | r2_end_date   | 2000-01-02 |
            And I unfocus "r2_end_date"
            And I select "megaadmin" from "r2_user_selector"
            And I click "leadsInVsLeadsInWithNoContact"
        Then I should see "testcustomer" in the "no-contact-leads" section
            And I should not see "Bob Scott" in the "no-contact-leads" section
            And I should not see "Rick Grimes" in the "no-contact-leads" section
            And I should see "Bob Scott" in the "contacted-leads" section
            And I should see "Rick Grimes" in the "contacted-leads" section

    @javascript
    Scenario: Check leads with reminders in the past works
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
                | Adam         | 1 Camp David            | LA                  |
            And I have the following reminders:
                | type          | detail          | date      | user      |
                | Future Target | future reminder | ten days  | megaadmin |
                | Future Target | past reminder   | yesterday | megaadmin |
            And I have the following sources:
                | name |
                | Rick |
                | Lori |
            And I have the following leads:
                | customer     | source | status     | priority | reminder        | future target | type         | user      |
                | testcustomer | Rick   | No contact | Normal   | future reminder | true          | New Purchase | megaadmin |
                | Adam         | Lori   | No contact | Normal   | past reminder   | false         | New Purchase | megaadmin |
        When I go to "/reports/list/lead"
            And I click "leadWithReminders"
            And I should not see "Adam"
            And I select "All" from "reminder_selector"
        Then I should see "Adam"

    @javascript
    Scenario: Check Leads which are quoting do not show successful ones
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city | isLead |
                | testcustomer | 123 Walker Avenue       | Atlanta             | no     |
                | Bob Scott    | 456 Running Street      | Bangor              | yes    |
                | Rick Grimes  | 789 52nd Street         | Bangor              | yes    |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status       | priority | type         | user      | updatedAt | successful | value |
                | testcustomer | Rick   | No contact   | Normal   | New Purchase | megaadmin | +1 hour   | yes        | 500   |
                | Bob Scott    | Rick   | Contact made | Normal   | New Purchase | megaadmin | +1 hour   | no         | 500   |
                | Rick Grimes  | Rick   | Quoting      | Normal   | New Purchase | megaadmin | +1 hour   | pending    | 500   |
        When I go to "/reports/list/lead"
        When I select "megaadmin" from "r5_user_selector"
            And I click "leadsValueAtQuoting"
        Then I should see "Rick Grimes"
            And I should not see "Bob Scott"
            And I should not see "testcustomer"
