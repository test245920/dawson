Feature: Add a truck with all form fields filled in
    Add a truck with new make and model, add a new model to existing make, add, edit, select, delete contacts from truck.

    Background:
        Given I am authenticated as "megaadmin"

    @javascript
    Scenario: Add a truck with a new make and model
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_address2 | billingAddress_city | billingAddress_postcode |
                | testcustomer | 100 Bell's Theorm Way   | ECIT                    | Belfast             | BT3 9DT                 |
            And I go to "/assets/active"
            And I click "New"
            And I follow "new-truck"
            And I follow "add-new-make"
            And I fill in the following:
                | truck_form_newMake | Caterpillar |
            And I follow "add-new-model"
            And I fill in the following:
                | truck_form_newModel | CAT-1 |
            And I fill in the following:
                | truck_form_serial            | SER2015 |
                | truck_form_yearOfManufacture | 2015 |
            And I press "Save & Transfer"
            And I should see "Oil filter"
            And I press "Save" on the current modal
            And I select "testcustomer" from "transfer_asset_customer"
            And I focus "transfer_asset_start"
            And I select "Contract Hire" from "transfer_asset_type"
            And I press "Save"
        Then I should see "Truck Particulars - Caterpillar CAT-1 (SER2015)"

    @javascript
    Scenario: Delete a truck
        Given I have the following customers:
               | name         | billingAddress_name | billingAddress_address1  | billingAddress_address2 | billingAddress_city | billingAddress_postcode |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following trucks:
               | customer     | make        | model  | serial  | yom  |
               | testcustomer | Caterpillar | CAT123 | SER2015 | 2015 |
        When I go to "/assets/active"
            And I click on "testcustomer" in the "Customer Name" column
            And I follow "View"
            And I follow "Delete"
            And I press "Delete" on the current modal
        Then I should not see "testcustomer"

    @javascript
    Scenario: Create a job from truck particulars
        Given I am authenticated as "megaadmin"
            And I have the following customers:
                | name         | billingAddress_address1 | billingAddress_address2 | billingAddress_city | billingAddress_postcode |
                | testcustomer | 100 Bell's Theorm Way   | ECIT                    | Belfast             | BT3 9DT                 |
            And I have the following jobs:
                | customer     | detail       | stage | engineer |
                | testcustomer | test details | 2     | engineer |
            And I have the following trucks:
               | customer     | make        | model  | serial  | yom  |
               | testcustomer | Caterpillar | CAT123 | SER2015 | 2015 |
        When I go to "/assets/active"
            And I click on "testcustomer" in the "Customer Name" column
            And I click the "Create Job" button in the "particulars-holder" section
            And I wait 1 second
            And I press "Save"
        And I go to the view page for the "80001" job
        Then I should see "Caterpillar CAT123 (SER2015)" beside "Truck" in the "description-section" section

    @javascript
    Scenario: Find truck by filtering make then model
        Given I have the following customers:
               | name         | billingAddress_name | billingAddress_address1  | billingAddress_address2 | billingAddress_city | billingAddress_postcode |
               | testcustomer | Main site           | 100 Bell's Theorm Way     | ECIT                   | Belfast             | BT3 9DT                 |
            And I have the following trucks:
               | customer     | make        | model  | serial  | yom  |
               | testcustomer | Caterpillar | CAT123 | SER2015 | 2015 |
               | testcustomer | Rolatruc    | ROL111 | SHB4037 | 2011 |
               | testcustomer | Caterpillar | CAT456 | DJW3216 | 2013 |
               | testcustomer | Servol      | SV-1X  | AEN1987 | 2011 |
        When I go to "/assets/active"
            And I click on "testcustomer" in the "Customer Name" column
            And I click on "Caterpillar" in the "Make" column
            And I click on "CAT123" in the "Model" column
        Then I should see "Truck Particulars"

    @javascript
    Scenario: Add a used truck to the system
        Given I go to "/assets/register"
            And I have the following trucks:
               | customer     | make        | model  | serial  | yom  |
               | testcustomer | Caterpillar | CAT123 | SER2015 | 2015 |
            And I click "New"
            And I follow "new-truck"
            And I select "Caterpillar" from "truck_form_make"
            And I select "CAT123" from "truck_form_model"
            And I fill in the following:
                | truck_form_serial            | SER2015 |
                | truck_form_yearOfManufacture | 2015    |
            And I press "Save"
            And I go to "/assets/register"
        Then I should see "Caterpillar CAT123 (SER2015)"

    @javascript
    Scenario: Edit a used truck
        Given I have the following trucks:
               | make        | model  | serial  | yom  |
               | Caterpillar | CAT123 | SER2015 | 2015 |
            And I go to "/assets/register"
            And I click on "Caterpillar CAT123 (SER2015)" in the "truck" column
            And I follow "Edit"
            And I select "Caterpillar" from "truck_form_make"
            And I select "CAT123" from "truck_form_model"
            And I fill in the following:
                | truck_form_serial | SER5555 |
            And I press "Save"
        Then I should see "Caterpillar CAT123 (SER5555)"

    @javascript
    Scenario: Change a truck from casual hire to contract hire
        Given I have the following customers:
               | name         | billingAddress_name | billingAddress_address1  | billingAddress_address2 | billingAddress_city | billingAddress_postcode |
               | testcustomer | Main site           | 100 Bell's Theorm Way     | ECIT                   | Belfast             | BT3 9DT                 |
            And I have the following trucks:
               | customer     | make        | model  | serial  | yom  | casualHire |
               | testcustomer | Caterpillar | CAT123 | SER2015 | 2015 | yes        |
        When I go to "/assets/active"
            And I click on "testcustomer" in the "Customer Name" column
            And I click "editStatus"
            And I select "Contract Hire" from "Type"
            And I press "Save"
        Then I should see "Maintenance start date"
            And I should see "Contract Hire"

    @javascript
    Scenario: Check truck form appears after filter reset
        Given I have the following customers:
               | name         | billingAddress_name | billingAddress_address1  | billingAddress_address2 | billingAddress_city | billingAddress_postcode |
               | testcustomer | Main site           | 100 Bell's Theorm Way     | ECIT                   | Belfast             | BT3 9DT                 |
            And I have the following trucks:
               | customer     | make        | model  | serial  | yom  |
               | testcustomer | Caterpillar | CAT123 | SER2015 | 2015 |
       When I go to "/assets/active"
            And I click "New"
            And I follow "new-truck"
            And I press "Close" on the current modal
            And I click "reset-filter"
            And I click "New"
            And I follow "new-truck"
        Then I should see "Add truck"

    @javascript
    Scenario: Check trucks without customers do not get listed
        Given I have the following trucks:
           | make        | model  | serial  | yom  |
           | Caterpillar | CAT123 | SER2015 | 2015 |
        When I go to "/assets/active"
        Then I should not see "Caterpillar"

    @javascript
    Scenario: Add a mast and attachment to truck
        Given I have the following customers:
                | name         | billingAddress_name | billingAddress_address1  | billingAddress_address2 | billingAddress_city | billingAddress_postcode |
                | testcustomer | Main site           | 100 Bell's Theorm Way     | ECIT                   | Belfast             | BT3 9DT                 |
            And I have the following trucks:
                | make        | model  | serial  | yom  |
                | Caterpillar | CAT123 | SER2015 | 2015 |
            And I have the following attachments:
                | make | model   | serial | yom  | type    |
                | Att  | Achment | ATT101 | 2018 | Rotator |
        When I go to "/assets/register"
            And I click on the "Caterpillar CAT123 (SER2015)" item
            And I wait for the "asset" particulars section to appear
            And I click "edit-asset"
            And I follow "Truck specifics"
            And I select "Boom lift" from "Mast type"
            And I fill in the following:
                | Mast height | 1000   |
            And I select "Rotator" from "Attachment type"
            And I select "Att Achment (ATT101)" from "truck_form_attachment"
            And I press "Save"
            And I click on the "Caterpillar CAT123 (SER2015)" item
            And I wait for the "asset" particulars section to appear
        Then I should see "Boom Lift"
            And I should see "1000mm"
            And I should see "Att Achment"

    @javascript
    Scenario: Edit a truck's model information
        Given I have the following customers:
               | name         | billingAddress_name | billingAddress_address1  | billingAddress_address2 | billingAddress_city | billingAddress_postcode |
               | testcustomer | Main site           | 100 Bell's Theorm Way     | ECIT                   | Belfast             | BT3 9DT                 |
            And I have the following trucks:
               | customer     | make        | model  | serial  | yom  |
               | testcustomer | Caterpillar | CAT123 | SER2015 | 2015 |
       When I go to "/assets/active"
            And I click on "testcustomer" in the "Customer Name" column
            And I follow "Edit Truck Model"
            And I fill in the following:
                | Name | CAT456 |
            And I press "Save"
            And I wait for the "truck" particulars section to appear
        Then I should see "CAT456"

    @javascript
    Scenario: Assign a used truck to a customer
        Given I have the following trucks:
               | make        | model  | serial  | yom  |
               | Caterpillar | CAT123 | SER2015 | 2015 |
            And I have the following customers:
               | name         |billingAddress_name |billingAddress_address1  |billingAddress_address2 |billingAddress_city |billingAddress_postcode |
               | testcustomer |Main site           |100 Bell's Theorm Way    |ECIT                    |Belfast             |BT3 9DT                 |
            And I go to "/assets/register"
            And I click on the "Caterpillar CAT123 (SER2015)" item
            And I wait for the "asset" particulars section to appear
            And I follow "Transfer Asset"
            And I select "testcustomer" from "transfer_asset_customer"
            And I focus "transfer_asset_start"
            And I select "Contract Hire" from "transfer_asset_type"
            And I press "Save"
            And I should see "testcustomer"
        When I go to "/assets/register"
        Then I should not see "Caterpillar"

    @javascript
    Scenario: Add a new maintenance task
        Given I have the following customers:
               | name         | billingAddress_address1  | billingAddress_address2 | billingAddress_city | billingAddress_postcode |
               | testcustomer | 100 Bell's Theorm Way    | ECIT                    | Belfast             | BT3 9DT                 |
            And I have the following trucks:
               | customer     | make        | model  | serial  | yom  |
               | testcustomer | Caterpillar | CAT123 | SER2015 | 2015 |
            And I go to "/assets/active"
        When I click on "testcustomer" in the "Customer Name" column
            And I click the "New" button in the "maintenance-section" section
            And I fill in the following:
                | Detail | big fix |
                | Amount | 100 |
            And I focus "asset_maintenance_date"
            And I unfocus "asset_maintenance_date"
            And I press "Save"
        Then I should see "big fix"

    @javascript
    Scenario: Edit a maintenance task
        Given I have the following customers:
               | name         | billingAddress_address1  | billingAddress_address2 | billingAddress_city | billingAddress_postcode |
               | testcustomer | 100 Bell's Theorm Way    | ECIT                    | Belfast             | BT3 9DT                 |
            And I have the following trucks:
               | customer     | make        | model  | serial  | yom  |
               | testcustomer | Caterpillar | CAT123 | SER2015 | 2015 |
            And I have the following maintenance tasks:
               | truck   | amount | detail  |
               | SER2015 | 500    | big fix |
            And I go to the view page for the "big fix" truck maintenance
            And I follow "Edit"
            And I fill in the following:
                | Amount | 111 |
            And I press "Save"
        Then I should see "111"
            And there should be a "AssetTransaction" with the "amount" "111"

    @javascript
    Scenario: Delete a maintenance task
        Given I have the following customers:
               | name         | billingAddress_address1  | billingAddress_address2 | billingAddress_city | billingAddress_postcode |
               | testcustomer | 100 Bell's Theorm Way    | ECIT                    | Belfast             | BT3 9DT                 |
            And I have the following trucks:
               | customer     | make        | model  | serial  | yom  |
               | testcustomer | Caterpillar | CAT123 | SER2015 | 2015 |
            And I have the following maintenance tasks:
               | truck   | amount | detail  |
               | SER2015 | 500    | big fix |
            And I go to the view page for the "big fix" truck maintenance
            And I follow "Delete"
            And I press "Delete" on the current modal
        Then I should not see "big fix" in the "maintenance-section" section

