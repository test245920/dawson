Feature: Check that the /find page can search for all different items across the system

    Background:
        Given I am authenticated as "megaadmin"

    @javascript
    Scenario: Search for a contact using their name
        Given I have the following contacts:
            | name         | telephone | email            | supplier     |
            | test contact | 123456789 | test@contact.com | testsupplier |
            And I am on "/find"
        When I fill in the following:
            | _search | test |
        Then I should see "test@contact.com - 123456789"

    @javascript
    Scenario: Search for a lead by the value
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following leads:
                | customer     | source | status     | user      | type         | value |
                | testcustomer | Rick   | No contact | megaadmin | New Purchase | 5000  |
            And I am on "/find"
        When I fill in the following:
            | _search | 5000 |
        Then I should see "No contact - 5000 - testcustomer"

    @javascript
    Scenario: Search for a job by the details
        Given I have the following customers:
               | name         | siteAddress_name | siteAddress_address1  | siteAddress_address2 | siteAddress_city | siteAddress_postcode |
               | testcustomer | Main site        | 100 Bell's Theorm Way | ECIT                 | Belfast          | BT3 9DT              |
            And I have the following jobs:
                | customer     | detail       | stage |
                | testcustomer | test details | 1     |
            And I am on "/find"
        When I fill in the following:
            | _search | deta |
        Then I should see "test details - testcustomer - 80000"

    @javascript
    Scenario: Search for a truck by the make
        Given I have the following trucks:
               | customer     | make        | model  | serial  | yom  |
               | testcustomer | Caterpillar | CAT123 | SER2015 | 2015 |
            And I am on "/find"
        When I fill in the following:
            | _search | 123 |
        Then I should see "Caterpillar - CAT123"

    @javascript
    Scenario: Search for a supplier by the name
        Given I have the following suppliers:
               | name         | code | telephone | fax       | email            |
               | ABC supplies | ABC1 | 123456789 | 987654321 | abc@supplies.com |
            And I am on "/find"
        When I fill in the following:
            | _search | ABC |
        Then I should see "abc@supplies.com - 123456789 - 987654321"

    @javascript
    Scenario: Search for a user by the name
        Given I am on "/find"
        When I fill in the following:
            | _search | mega |
        Then I should see "megaadmin - megaadmin@megaadmin.com"

    @javascript
    Scenario: Search for a customer by the name
        Given I have the following customers:
               | name         | active |
               | testcustomer | yes    |
            And I am on "/find"
        When I fill in the following:
            | _search | test |
        Then I should see "testcustomer"
