Feature: test the console commands for the system

    Background:
        Given I am authenticated as "megaadmin"

    Scenario: Check emails get sent for reminders
        Given I have the following customers:
                | name         |
                | testcustomer |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following reminders:
                | type          | detail        | date |
                | Future Target | test reminder | now  |
            And I have the following leads:
                | customer     | source | status     | priority | reminder      | user      | type         |
                | testcustomer | Rick   | No contact | Normal   | test reminder | megaadmin | New Purchase |
        When I run the "v42:email:schedule" command I should see "Sending reminder email" in the output
        Then there should be 1 email with the subject "Reminder: Future Target"

    Scenario: Check email does not get sent for reminder which has already had email sent
        Given I have the following customers:
                | name         |
                | testcustomer |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following reminders:
                | type          | detail        | date | notified |
                | Future Target | test reminder | now  | yes      |
            And I have the following leads:
                | customer     | source | status     | priority | reminder      | user      | type         |
                | testcustomer | Rick   | No contact | Normal   | test reminder | megaadmin | New Purchase |
        When I run the "v42:email:schedule" command I should see "No reminder emails need to be sent" in the output
        Then there should be 0 emails with the subject "Reminder: Future Target"

    Scenario: Check emails do not get sent for days other than today
        Given I have the following customers:
                | name         |
                | testcustomer |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following reminders:
                | type          | detail                 | date     |
                | Future Target | test today reminder    | now      |
                | Future Target | test tomorrow reminder | tomorrow |
            And I have the following leads:
                | customer     | source | status     | priority | reminder               | user      | type         |
                | testcustomer | Rick   | No contact | Normal   | test today reminder    | megaadmin | New Purchase |
                | testcustomer | Rick   | No contact | Normal   | test tomorrow reminder | megaadmin | New Purchase |
        When I run the "v42:email:schedule" command I should see "Sending reminder email" in the output
        Then there should be 1 email with the subject "Reminder: Future Target"

    Scenario: Check Future targets with reminders in the past get moved to primary targets
        Given I have the following customers:
                | name         | billingAddress_address1 | billingAddress_city |
                | testcustomer | 123 Walker Avenue       | Atlanta             |
            And I have the following reminders:
                | type          | detail         | date      | user      |
                | Future Target | test reminder  | yesterday | megaadmin |
            And I have the following sources:
                | name |
                | Rick |
            And I have the following leads:
                | customer     | source | status     | priority | reminder      | future target | type         |
                | testcustomer | Rick   | No contact | Normal   | test reminder | true          | New Purchase |
        When I run the "v42:leads:assessFutureTargets" command I should see "Changing Future Target" in the output
        Then there should be a "lead" entity with the property "primaryTarget" containing "1"
