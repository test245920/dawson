<?php

namespace App\EventListeners;

use App\Entity\Image;
use Aws\S3\S3ClientInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

class ImageListener
{
    public function __construct(private readonly S3ClientInterface $s3, private $bucket)
    {
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        if (!$args->getObject() instanceof Image) {
            return;
        }

        $image = $args->getObject();

        if ($image->getImage() == null) {
            return;
        }

        $em   = $args->getObjectManager();
        $file = $image->getImage();
        $path = $image->getPath() . '/';
        $this->uploadImage($file, $path);
        $image->setFilename($file->getClientOriginalName());
    }

    public function preUpdate(PreUpdateEventArgs $eventArgs): void
    {
        if ($eventArgs->getEntity() instanceof Image) {
            if ($eventArgs->hasChangedField('filename')) {
                $eventArgs->setNewValue('filename', $eventArgs->getNewValue('filename'));
            }

            $image = $eventArgs->getEntity();
            if ($image->getImage() == null) {
                return;
            }

            $file = $image->getImage();
            $path = $image->getPath() . '/';
            $this->uploadImage($file, $path);
            $image->setFilename($file->getClientOriginalName());
        }
    }

    private function uploadImage($image, string $path)
    {
        if ($image == null) {
            return;
        }

        $filename     = $path . $image->getClientOriginalName();
        $fileLocation = $image->getRealPath() . $image->getClientOriginalName();

        try {
            $this->s3->putObject(['Bucket'     => $this->bucket, 'Key'        => ltrim($filename, '/'), 'SourceFile' => $image->getRealPath(), 'ACL'        => 'public-read']);
        } catch (Aws\S3\Exception\S3Exception $e) {
            throw new S3Exception('[Error] -> ' . $e->getMessage());
        }
    }
}
