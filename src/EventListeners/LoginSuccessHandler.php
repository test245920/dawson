<?php

namespace App\EventListeners;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface
{
    public function __construct(protected AuthorizationCheckerInterface $authorizationChecker, protected RouterInterface $router)
    {
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token): RedirectResponse
    {
        if ($this->authorizationChecker->isGranted('ROLE_CUSTOMER')) {
            $userId = $token->getUser()->getId();
            $url = $this->router->generate('view_account', ['id' => $userId]);
        } else {
            $url = $this->router->generate('index');
        }

        return new RedirectResponse($url);
    }
}
