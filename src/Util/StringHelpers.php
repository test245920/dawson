<?php

namespace App\Util;

class StringHelpers
{
    public static function formatTimeAsString($time): string
    {
        $hours     = floor($time / 60);
        $minutes   = $time % 60;

        return $hours . 'h ' . $minutes . 'm';
    }

    public static function formatWorklogTimesAsString($time, $overtime, $extremeOvertime, $absenceTime = null): string
    {
        $string = static::formatTimeAsString($time);

        if ($absenceTime) {
            $string = static::formatTimeAsString($time + $absenceTime);
        }

        if ($overtime) {
            $string .= ' (+ ' . static::formatTimeAsString($overtime) . ' OT)';
        }

        if ($extremeOvertime) {
            $string .= ' (+ ' . static::formatTimeAsString($extremeOvertime) . ' EOT)';
        }

        return $string;
    }
}
