<?php

namespace App\DataFixtures\ORM;

use App\Entity\Job;
use App\Manager\JobManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadJobs extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private ?ContainerInterface $container = null;

    private ?ObjectManager $manager = null;

    private int $counter = 0;

    public function __construct(JobManager $jobManager)
    {
        $this->jobManager = $jobManager;
    }

    public function setContainer(?ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
        $this->resetAutoIncrementId();

        $jobOptions = ['accepted'             => false, 'cancelled'            => false, 'reason'               => null, 'signedOff'            => false, 'estimate'             => false, 'oldJob'               => false, 'futureJob'            => false, 'live'                 => false, 'urgent'               => false];

        // Unestimated jobs
        $jobOptions['oldJob'] = true;
        $this->createJob('customer-1', 'truck-1', null, 'Broken handbrake', 'replace cable', $jobOptions);

        $jobOptions['oldJob'] = false;
        $this->createJob('customer-2', 'truck-2', null, 'Drivers side front suspension spring snapped', 'replace spring', $jobOptions);

        // Unassigned jobs

        $jobOptions['estimate'] = true;
        $jobOptions['urgent']   = true;
        $this->createJob('customer-3', 'truck-9', null, 'Change oil and oil filters', 'Regular service', $jobOptions);

        $jobOptions['urgent']    = false;
        $jobOptions['futureJob'] = true;
        $this->createJob('customer-1', 'truck-6', null, 'Brakes sticking', 'Check brake fluid levels', $jobOptions);

        $jobOptions['futureJob'] = false;
        $this->createJob('customer-4', 'truck-2', null, 'sticking brakes', 'replace brake pads', $jobOptions);
        $this->createJob('customer-5', 'truck-3', null, 'Cracked windscreen', 'Call autoglass', $jobOptions);

        // Live Jobs

        $jobOptions['accepted'] = true;
        $jobOptions['live']     = true;

        $this->createJob('customer-4', 'truck-4', 'user-engineer2', 'Leaking fuel', 'Take truck off road until resolved', $jobOptions);
        $this->createJob('customer-5', 'truck-5', 'user-engineer', 'Losing power when driving', 'Check connection hoses for cracks', $jobOptions);

        // Parts Required

        // Parts On Order

        // Complete Invoice

        $jobOptions['live'] = false;

        $this->createJob('customer-2', 'truck-7', 'user-engineer2', 'Truck pulling to left when driving', 'Reset wheel alignment', $jobOptions);
        $this->createJob('customer-5', 'truck-7', 'user-engineer2', 'Truck still pulling to left', 'Check passenger side front tyre for slow puncture', $jobOptions);
        $this->createJob('customer-3', 'truck-8', 'user-engineer', 'Removing key does not turn ignition off', 'Check electrics for possible short', $jobOptions);

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 7; // the order in which fixtures will be loaded
    }

    private function createJob(string $customer, string $truck, ?string $engineer, string $detail, string $notes, array $jobOptions): Job
    {
        $job = new Job();

        if ($customer && $this->hasReference($customer)) {
            $customer = $this->getReference($customer);
            $job->setCustomer($customer);
            $customer->getJobs()->add($job);
        }

        if ($truck && $this->hasReference($truck)) {
            $period = $this->getReference($truck)->getCurrentLifeCyclePeriod();
            $job->setAssetLifecyclePeriod($period);
        }

        if ($engineer && $this->hasReference($engineer)) {
            $job->addEngineer($this->getReference($engineer));
        }

        if ($jobOptions['estimate']) {
            $jobOptions['estimate'] = (random_int(100, 99999) / 100);
            $job->setEstimate($jobOptions['estimate']);
        }

        if ($jobOptions['oldJob']) {
            $job->setCreatedAt(new \DateTime('-3 hours'));
        }

        if ($jobOptions['futureJob']) {
            $job->setJobActiveFrom(new \DateTime('+1 day'));
        } else {
            $job->setJobActiveFrom(new \DateTime());
        }

        $job->setDetail($detail);
        $job->setNotes($notes);
        $job->setAccepted($jobOptions['accepted']);
        $job->setCancelled($jobOptions['cancelled']);
        $job->setCancellationReason($jobOptions['reason']);
        $job->setSignedOff($jobOptions['signedOff']);
        $job->setIsLive($jobOptions['live']);
        $job->setUrgent($jobOptions['urgent']);
        $job->setJobcode($this->counter + 1);

        $this->jobManager->saveJob($job, true, false);
        $this->addReference('job-' . $this->counter, $job);
        ++$this->counter;

        return $job;
    }

    private function resetAutoIncrementId(): void
    {
        $metaData  = $this->manager->getMetadataFactory()->getMetadataFor(Job::class);
        $tableName = $metaData->getTableName();
        $conn      = $this->manager->getConnection();
        $conn->prepare('ALTER TABLE `' . $tableName . '` AUTO_INCREMENT = 1')->execute();
    }
}
