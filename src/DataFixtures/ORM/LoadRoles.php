<?php

namespace App\DataFixtures\ORM;

use App\Entity\Role;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class LoadRoles extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;

        $role = $this->createRole('Management', 'ROLE_SUPER_ADMIN');
        $role = $this->createRole('Service Admin', 'ROLE_SERVICE_ADMIN');
        $role = $this->createRole('Sales', 'ROLE_SALES_ADMIN');
        $role = $this->createRole('Engineer', 'ROLE_ENGINEER');
        $role = $this->createRole('Customer User', 'ROLE_CUSTOMER');

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 2;
    }

    private function createRole(string $name, string $roleString): Role
    {
        $role = new Role();
        $role->setName($name);
        $role->setRole($roleString);

        $this->addReference($roleString, $role);
        $this->manager->persist($role);

        return $role;
    }
}
