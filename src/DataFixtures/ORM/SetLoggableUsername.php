<?php

namespace App\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SetLoggableUsername extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private ?ContainerInterface $container = null;

    public function setContainer(?ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager): void
    {
        $this->container->get('v42.stof_doctrine_extensions.listener.loggable')->setUsername('megaadmin');
    }

    public function getOrder(): int
    {
        return 1; // the order in which fixtures will be loaded
    }
}
