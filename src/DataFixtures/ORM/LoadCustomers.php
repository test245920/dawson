<?php

namespace App\DataFixtures\ORM;

use App\Entity\BillingAddress;
use App\Entity\Customer;
use App\Entity\CustomerNumber;
use App\Entity\SiteAddress;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadCustomers extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private ?ContainerInterface $container = null;

    private ?ObjectManager $manager = null;

    private int $counter = 1;

    public function setContainer(?ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
        $this->resetAutoIncrementId();

        $customer = $this->createCustomer('Short Term Rental Fleet', 'Internal', '54-72 Tamar Street', 'Belfast', 'BT4 1HS', '02890452088', '02890731318', 'info@dawsonmhe.com', '0', '0', '0', '0', '0', 'R', 1, 'R1', 'WD Depot');
        $customer = $this->createCustomer('Jess Stockdale', 'Jess', '1 Talbot Street', 'Belfast', 'BT1 1AA', '02890456123', '02890123457', 'jes@j.com', '1', '12', '2000', '200', '0.05', 'J', 1, 'J1', 'Stockdale yard');
        $customer = $this->createCustomer('Aaron Aaronson', 'Zach', '54 Scotch Street', 'Armagh', 'BT61 7BW', '02845645255', '02845789616', 'a@ron.com', '0', '24', '5000', '500', '0.10', 'A', 1, 'A1', 'Jetson Park');
        $customer = $this->createCustomer('Max Power', 'Homer', '5 Westland Drive', 'Magherafelt', 'BT45 5BA', '02875757577', '02875461389', 'max@power.com', '1', '6', '10000', '2000', '0.10', 'P', 1, 'P1', 'Lambeau Industrial Park');
        $customer = $this->createCustomer('Buzz Armstrong', 'Buzz', '77 Cleland Park N', 'Bangor', 'BT20 3EB', '0289145678', '02891648719', 'buzz@moon.com', '1', '12', '2500', '225', '0.05', 'A', 2, 'A2', 'Lunar Drive');

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 3; // the order in which fixtures will be loaded
    }

    private function createCustomer(string $name, string $contactName, string $address, string $city, string $postcode, string $telephone, string $fax, string $email, string $acceptEmailInvoice, string $paymentTerm, string $accountLimit, string $labourRate, string $partsDiscount, string $custNumLetter, int $custNumNum, string $custNum, string $siteName): Customer
    {
        $customer       = new Customer();
        $customerNumber = new CustomerNumber($customer);

        $customerNumber->setLetter($custNumLetter);
        $customerNumber->setNumber($custNumNum);
        $customerNumber->setCustomerNumber($custNum);
        $customerNumber->setAssignedAt(new \DateTime());

        $customer->setName($name);
        $customer->setContactName($contactName);

        $billingAddress = new BillingAddress();
        $billingAddress->setAddress1($address);
        $billingAddress->setCity($city);
        $billingAddress->setPostcode($postcode);
        $billingAddress->setCustomer($customer);
        $this->manager->persist($billingAddress);

        $customer->setBillingAddress($billingAddress);

        $siteAddress = new SiteAddress();
        $siteAddress->setName($siteName);
        $siteAddress->setAddress1($address);
        $siteAddress->setCity($city);
        $siteAddress->setPostcode($postcode);
        $siteAddress->setCustomer($customer);
        $this->manager->persist($siteAddress);

        $customer->addSiteAddress($siteAddress);

        $customer->setTelephone($telephone);
        $customer->setFax($fax);
        $customer->setEmail($email);
        $customer->setEmailInvoice($acceptEmailInvoice);
        $customer->setPaymentTerm($paymentTerm);
        $customer->setAccountLimit($accountLimit);
        $customer->setLabourRate($labourRate);
        $customer->setPartsDiscount($partsDiscount);
        $customer->setCustomerNumbers($customerNumber);

        $this->manager->persist($customer);

        $this->addReference('customer-' . $this->counter, $customer);
        ++$this->counter;

        return $customer;
    }

    private function resetAutoIncrementId(): void
    {
        $metaData  = $this->manager->getMetadataFactory()->getMetadataFor(Customer::class);
        $tableName = $metaData->getTableName();
        $conn      = $this->manager->getConnection();
        $conn->prepare('ALTER TABLE `' . $tableName . '` AUTO_INCREMENT = 1')->execute();
    }
}
