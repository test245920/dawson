<?php

namespace App\DataFixtures\ORM;

use App\Entity\Location;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUsers extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private ?ContainerInterface $container = null;

    public function setContainer(?ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
        $this->resetAutoIncrementId();

        $user = $this->createUser('megaadmin', 'megaadmin', 'Mega Admin', 'ROLE_SUPER_ADMIN');
        $user = $this->createUser('serviceadmin', 'megaadmin', 'Service Admin', 'ROLE_SERVICE_ADMIN');
        $user = $this->createUser('salesadmin', 'megaadmin', 'Sales Admin', 'ROLE_SALES_ADMIN');
        $user = $this->createUser('engineer', 'megaadmin', 'First Engineer', 'ROLE_ENGINEER');
        $user = $this->createUser('customeruser', 'megaadmin', 'Customer User', 'ROLE_CUSTOMER');
        $user = $this->createUser('Karl', 'megaadmin', 'Mega Admin', 'ROLE_SUPER_ADMIN');
        $user = $this->createUser('Graham', 'megaadmin', 'Mega Admin', 'ROLE_SUPER_ADMIN');

        $manager->persist($user);

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 4; // the order in which fixtures will be loaded
    }

    private function createUser(string $username, string $password, string $name, $role = null): User
    {
        $user = new User();
        $user->setUsername($username);
        $user->setName($name);
        $user->setEmail($username . '@' . $username . '.com');

        $passwordHash = '';

        $passwordHash = match ($password) {
            'megaadmin' => '$2y$13$aZKZwr28nXKS2LlhKOqRduMlyakgtf8tkWxR90g6gcSkMipXMOtnW',
            default => throw new Exception('Password hash lookup not available for "' . $password . '"'),
        };

        $user->setPassword($passwordHash);

        if ($role && $this->hasReference($role)) {
            $user->addRole($this->getReference($role));
        }

        if ($role == 'ROLE_ENGINEER') {
            $this->createVan($user);
        }

        $this->addReference('user-' . $username, $user);
        $this->manager->persist($user);

        return $user;
    }

    private function createVan(User $user): void
    {
        $van = new Location();
        $van->setEngineer($user);

        $this->manager->persist($van);

        $user->setVan($van);
    }

    private function resetAutoIncrementId(): void
    {
        $metaData  = $this->manager->getMetadataFactory()->getMetadataFor(User::class);
        $tableName = $metaData->getTableName();
        $conn      = $this->manager->getConnection();
        $conn->prepare('ALTER TABLE `' . $tableName . '` AUTO_INCREMENT = 1')->execute();
    }
}
