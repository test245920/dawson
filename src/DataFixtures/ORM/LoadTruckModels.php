<?php

namespace App\DataFixtures\ORM;

use App\Entity\TruckMake;
use App\Entity\TruckModel;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadTruckModels extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private ?ContainerInterface $container = null;

    private ?ObjectManager $manager = null;

    private int $counter = 1;

    public function setContainer(?ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
        $this->resetAutoIncrementId();

        $this->createTruckModel('4FD25', 'Toyota', 'Diesel', '200x50x10', '140x55x90');
        $this->createTruckModel('R 16 P', 'Lansing', 'Electric', '200x50x10', '140x55x90');
        $this->createTruckModel('FG15K', 'Mitsubishi', 'LPG', '200x50x10', '140x55x90');
        $this->createTruckModel('RRB7 15', 'Rolatruc', 'Diesel', '200x50x10', '140x55x90');
        $this->createTruckModel('Fork Truck Fleet', 'Servol', 'LPG', '200x50x10', '140x55x90');
        $this->createTruckModel('NR1355', 'Caterpillar', 'LPG', '200x50x10', '140x55x90');
        $this->createTruckModel('NR1350', 'Caterpillar', 'Diesel', '200x50x10', '140x55x90');
        $this->createTruckModel('NR1600', 'Caterpillar', 'Electric', '200x50x10', '140x55x90');
        $this->createTruckModel('Miscellaenous', 'M.F.I.', 'Electric', '200x50x10', '140x55x90');
        $this->createTruckModel('RT2000E/5', 'Rolatruc', 'Diesel', '200x50x10', '140x55x90');
        $this->createTruckModel('T2 650kg', 'Bradshaw', 'Diesel', '200x50x10', '140x55x90');
        $this->createTruckModel('PB18P', 'Mitsubishi', 'Electric', '200x50x10', '140x55x90');

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 5; // the order in which fixtures will be loaded
    }

    private function createTruckModel(string $model, string $make, string $fuel, string $frontTyres, string $rearTyres): TruckModel
    {
        $truckModel = new TruckModel();

        if ($this->hasReference('truck-make-' . strtolower($make))) {
            $truckModel->setMake($this->getReference('truck-make-' . strtolower($make)));
        } else {
            $truckMake = new TruckMake();
            $truckMake->setName($make);
            $this->manager->persist($truckMake);
            $this->addReference('truck-make-' . strtolower($make), $truckMake);
            $truckModel->setMake($truckMake);
            $truckModel->setFuel($fuel);
            $truckModel->setRearTyres($rearTyres);
            $truckModel->setFrontTyres($frontTyres);
        }

        $truckModel->setName($model);

        $this->manager->persist($truckModel);

        $this->addReference('truck-model-' . $this->counter, $truckModel);
        ++$this->counter;

        return $truckModel;
    }

    private function resetAutoIncrementId(): void
    {
        $metaData  = $this->manager->getMetadataFactory()->getMetadataFor(TruckModel::class);
        $tableName = $metaData->getTableName();
        $conn      = $this->manager->getConnection();
        $conn->prepare('ALTER TABLE `' . $tableName . '` AUTO_INCREMENT = 1')->execute();
    }
}
