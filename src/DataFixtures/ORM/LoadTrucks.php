<?php

namespace App\DataFixtures\ORM;

use App\Entity\AssetLifecyclePeriod;
use App\Entity\Truck;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadTrucks extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private ?ContainerInterface $container = null;

    private ?ObjectManager $manager = null;

    private int $counter = 1;

    public function setContainer(?ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
        $this->resetAutoIncrementId();

        $this->createTruck('truck-model-1', 99999.00, 100.00, 'customer-1', '1', 12, 12, 2005, 'HSTD', 'Off-Road', 'None');
        $this->createTruck('truck-model-2', 99999.00, 100.00, 'customer-2', '2', 12, 6, 2006, 'HSTD', 'Construction', 'PVC cab cover');
        $this->createTruck('truck-model-3', 99999.00, 100.00, 'customer-3', '3', 10, 10, 2007, 'HSTD', 'Off-Road', 'PVC cab cover');
        $this->createTruck('truck-model-4', 99999.00, 100.00, 'customer-4', '4', 12, 12, 2010, 'HDPX', 'Off-Road', 'PVC cab cover');
        $this->createTruck('truck-model-5', 99999.00, 100.00, 'customer-5', '5', 6, 6, 2011, 'HDPX', 'Construction', 'Enclosed Cab');
        $this->createTruck('truck-model-6', 99999.00, 100.00, 'customer-1', '6', 6, 6, 2011, 'HDPX', 'Extreme duty', 'PVC cab cover');
        $this->createTruck('truck-model-7', 99999.00, 100.00, 'customer-5', '7', 12, 6, 2011, 'HDPX', 'Extreme duty', 'PVC cab cover');
        $this->createTruck('truck-model-8', 99999.00, 100.00, 'customer-3', '8', 6, 6, 2011, 'HDPX', 'Snow', 'Enclosed Cab');
        $this->createTruck('truck-model-9', 99999.00, 100.00, 'customer-4', '9', 24, 24, 2005, 'HTPX', 'Construction', 'Enclosed Cab');
        $this->createTruck('truck-model-10', 99999.00, 100.00, 'customer-2', '10', 20, 18, 2005, 'HTSD', 'Extreme duty', 'Enclosed Cab');
        $this->createTruck('truck-model-11', 99999.00, 100.00, 'customer-4', '11', 12, 12, 2006, 'HTPX', 'Construction', 'Enclosed Cab');
        $this->createTruck('truck-model-12', 99999.00, 100.00, 'customer-5', '12', 12, 12, 2012, 'HTSD', 'Smooth tread', 'Enclosed Cab');

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 6; // the order in which fixtures will be l
    }

    private function createTruck(string $model, float $purchasePrice, float $hirePriceMonthly, string $customer, string $flt, int $loler, int $service, int $yom, string $mast, string $tyre, string $cabin): Truck
    {
        $truck = new Truck();
        $truck->setSerial(uniqid());
        $truck->setPurchasePrice($purchasePrice);
        $truck->setHireRate($hirePriceMonthly);
        $truck->setYearOfManufacture($yom);
        $truck->setMast($mast);
        $truck->setMastHeight(random_int(500, 1000));
        $truck->setCabin($cabin);
        $truck->setTyreType($tyre);
        $truck->setHourReading(random_int(0, 100000));

        $this->manager->persist($truck);

        if ($model && $this->hasReference($model)) {
            $truckModel = $this->getReference($model);
            $truck->setModel($truckModel);
        }

        if ($customer && $this->hasReference($customer)) {
            $period = new AssetLifecyclePeriod();

            $period->setAsset($truck);
            $period->setClientFleet($flt);
            $period->setServiceInterval($service);
            $period->setLolerInterval($loler);
            $period->setCustomer($this->getReference($customer));
            $period->setSiteAddress($this->getReference($customer)->getSiteAddresses()->first());
            $period->setStart(new \DateTime());
            $period->setHireRateMonthly($hirePriceMonthly);
            $period->setType(AssetLifecyclePeriod::PERIOD_TYPE_CONTRACT);

            $this->manager->persist($period);

            $truck->setCurrentLifecyclePeriod($period);
        }

        $this->addReference('truck-' . $this->counter, $truck);
        ++$this->counter;

        return $truck;
    }

    private function resetAutoIncrementId(): void
    {
        $conn = $this->manager->getConnection();

        $metaData  = $this->manager->getMetadataFactory()->getMetadataFor(Truck::class);
        $tableName = $metaData->getTableName();
        $conn->prepare('ALTER TABLE `' . $tableName . '` AUTO_INCREMENT = 1')->execute();

        $metaData  = $this->manager->getMetadataFactory()->getMetadataFor(AssetLifecyclePeriod::class);
        $tableName = $metaData->getTableName();
        $conn->prepare('ALTER TABLE `' . $tableName . '` AUTO_INCREMENT = 1')->execute();
    }
}
