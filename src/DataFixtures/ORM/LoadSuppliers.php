<?php

namespace App\DataFixtures\ORM;

use App\Entity\Supplier;
use App\Entity\SupplierAddress;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadSuppliers extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private ?ContainerInterface $container = null;

    private ?ObjectManager $manager = null;

    private int $counter = 0;

    public function setContainer(?ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
        $this->resetAutoIncrementId();

        $supplier = $this->createSupplier('ABC Supplies', 'Sydenham Road', '02890123456', '02890456123', 'Belfast', 'Antrim', 'NI', 'BT3 9DH', 'S-A-1');
        $supplier = $this->createSupplier('Toyota Parts Ltd', 'Frosses Road', '0287548612', '02875894613', 'Ballymoney', 'Antrim', 'NI', 'BT53 7EJ', 'S-T-1');
        $supplier = $this->createSupplier('Carlton Metal Works', 'Cambourne Park', '02891451235', '', 'Newtownards', 'Down', 'NI', 'BT23 4WE', 'S-C-1');
        $supplier = $this->createSupplier('Transit Specalists', 'Main Street', '02044576511', '0202447896421', 'Prestwick', 'Ayrshire', 'Scotland', 'KA9 1SN', 'S-T-2');
        $supplier = $this->createSupplier('Stephens Tyres', 'Hill View Street', '003538659412335', '', 'Mullaghmore', 'Sligo', 'Ireland', '', 'S-S-1');
        $supplier = $this->createSupplier('Lowry Lorry Mechanics', 'Univeristy Way', '02892865312', '02892481926', 'Jordanstown', 'Antrim', 'NI', ' BT3 0QF', 'S-L-1');

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 8; // the order in which fixtures will be loaded
    }

    private function createSupplier(string $name, string $address1, string $tel, string $fax, string $city, string $county, string $country, string $postcode, string $code): Supplier
    {
        $supplier        = new Supplier();
        $supplierAddress = new SupplierAddress();

        $supplier->setName($name);
        $supplier->setTelephone($tel);
        $supplier->setfax($fax);
        $lowerNameWithNoWhiteSpace = strtolower(preg_replace('/\s+/', '', $name));
        $supplier->setEmail($lowerNameWithNoWhiteSpace . '@' . $lowerNameWithNoWhiteSpace . '.com');
        $supplier->setCode($code);

        $supplierAddress->setAddress1($address1);
        $supplierAddress->setCity($city);
        $supplierAddress->setCountry($country);
        $supplierAddress->setCounty($county);
        $supplierAddress->setPostcode($postcode);
        $supplierAddress->setSupplier($supplier);

        $this->manager->persist($supplierAddress);
        $supplier->setSupplierAddress($supplierAddress);
        $this->manager->persist($supplier);

        $this->addReference('supplier-' . $this->counter, $supplier);
        ++$this->counter;

        return $supplier;
    }

    private function resetAutoIncrementId(): void
    {
        $metaData  = $this->manager->getMetadataFactory()->getMetadataFor(Supplier::class);
        $tableName = $metaData->getTableName();
        $conn      = $this->manager->getConnection();
        $conn->prepare('ALTER TABLE `' . $tableName . '` AUTO_INCREMENT = 1')->execute();
    }
}
