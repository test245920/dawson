<?php

namespace App\DataFixtures\ORM;

use App\Entity\PurchaseOrder;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadPurchaseOrders extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private ?ContainerInterface $container = null;

    private ?ObjectManager $manager = null;

    private int $counter = 0;

    public function setContainer(?ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
        $this->resetAutoIncrementId();

        $purchaseOrder = $this->createPurchaseOrder(null, 'Handbrake cable for Toyota 4FD25', null, new \DateTime(), true);
        $purchaseOrder = $this->createPurchaseOrder(null, 'Suspension spring for Toyota 4FD25', null, new \DateTime(), true);
        $purchaseOrder = $this->createPurchaseOrder('customer-2', 'Brake pads for Lansing R 16 P', 'job-3', new \DateTime(), null);
        $purchaseOrder = $this->createPurchaseOrder(null, 'brake fluid for M.F.I', null, new \DateTime(), true);

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 10; // the order in which fixtures will be loaded
    }

    private function createPurchaseOrder(?string $customer, string $orderInstructions, ?string $job, \DateTime $orderDate, ?bool $stockOrder): ?string
    {
        $purchaseOrder = new PurchaseOrder();
        $purchaseOrder->setCreated(new \DateTime());
        $purchaseOrder->setOrderDate($orderDate);
        $purchaseOrder->setSupplier($this->getReference('supplier-1'));

        if ($job && $this->hasReference($job)) {
            $purchaseOrder->setJob($this->getReference($job));
        }
        if ($customer && $this->hasReference($customer)) {
            $purchaseOrder->setCustomer($this->getReference($customer));
        }

        $purchaseOrder->setOrderInstructions($orderInstructions);

        $this->manager->persist($purchaseOrder);

        $this->addReference('purchaseOrder-' . $this->counter, $purchaseOrder);
        ++$this->counter;

        return $customer;
    }

    private function resetAutoIncrementId(): void
    {
        $metaData  = $this->manager->getMetadataFactory()->getMetadataFor(PurchaseOrder::class);
        $tableName = $metaData->getTableName();
        $conn      = $this->manager->getConnection();
        $conn->prepare('ALTER TABLE `' . $tableName . '` AUTO_INCREMENT = 1')->execute();
    }
}
