<?php

namespace App\DataFixtures\ORM;

use App\Entity\PurchaseOrderItem;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadPurchaseOrderItems extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private ?ContainerInterface $container = null;

    private ?ObjectManager $manager = null;

    private int $counter = 0;

    public function setContainer(?ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
        $this->resetAutoIncrementId();

        $purchaseOrderItem = $this->createPurchaseOrderItem('supplierStock-1', 'purchaseOrder-1', 1);
        $purchaseOrderItem = $this->createPurchaseOrderItem('supplierStock-2', 'purchaseOrder-1', 1);
        $purchaseOrderItem = $this->createPurchaseOrderItem('supplierStock-3', 'purchaseOrder-2', null);
        $purchaseOrderItem = $this->createPurchaseOrderItem('supplierStock-4', 'purchaseOrder-3', null);
        $purchaseOrderItem = $this->createPurchaseOrderItem('supplierStock-5', 'purchaseOrder-4', null);

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 11; // the order in which fixtures will be loaded
    }

    private function createPurchaseOrderItem(string $supplierStock, string $purchaseOrder, ?int $receivedDate): PurchaseOrderItem
    {
        $purchaseOrderItem = new PurchaseOrderItem();

        if ($receivedDate) {
            $purchaseOrderItem->setReceivedDate(new \DateTime());
        }

        if ($supplierStock && $this->hasReference($supplierStock)) {
            $purchaseOrderItem->setItem($this->getReference($supplierStock));
            $purchaseOrderItem->setUnitCost($this->getReference($supplierStock)->getCostPrice());
        }

        if ($purchaseOrder && $this->hasReference($purchaseOrder)) {
            $purchaseOrderItem->setPurchaseOrder($this->getReference($purchaseOrder));
        }

        $purchaseOrderItem->setQuantity(random_int(1, 10));

        $this->manager->persist($purchaseOrderItem);
        $this->addReference('purchaseOrderItem-' . $this->counter, $purchaseOrderItem);
        ++$this->counter;

        return $purchaseOrderItem;
    }

    private function resetAutoIncrementId(): void
    {
        $metaData  = $this->manager->getMetadataFactory()->getMetadataFor(PurchaseOrderItem::class);
        $tableName = $metaData->getTableName();
        $conn      = $this->manager->getConnection();
        $conn->prepare('ALTER TABLE `' . $tableName . '` AUTO_INCREMENT = 1')->execute();
    }
}
