<?php

namespace App\DataFixtures\ORM;

use App\Entity\Location;
use App\Entity\SiteAddress;
use App\Entity\Stock;
use App\Entity\StockItem;
use App\Entity\SupplierStock;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadStock extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private ?ContainerInterface $container = null;

    private ?ObjectManager $manager = null;

    private int $counter              = 0;

    private int $supplierStockCounter = 0;

    public function setContainer(?ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
        $this->resetAutoIncrementId();

        $address   = new SiteAddress();
        $address->setName('Main warehouse');
        $address->setIsStockLocation(true);
        $this->manager->persist($address);

        $warehouse = new Location();
        $warehouse->setSiteAddress($address);
        $warehouse->setIsMainStock(true);
        $this->manager->persist($warehouse);
        $this->manager->flush();

        $stock = $this->createStock('T1', 'Handbrake cable for Toyota 4FD25', 2, 1, 1, 50.00, 70.00, false, 'H1', $warehouse);
        $stock = $this->createStock('T2', 'Suspension spring for Toyota 4FD25', 4, 1, 1, 10.00, 20.00, false, 'S1', $warehouse);
        $stock = $this->createStock('C1', 'Brake fluid for Caterpillar NR1350', 0, 0, 0, 20.00, 30.00, false, null, $warehouse);
        $stock = $this->createStock('L1', 'Brake pads for Lansing R16 P', 3, 2, 2, 40.00, 50.00, false, null, $warehouse);
        $stock = $this->createStock('M1', 'Engine oil for M.F.I trucks', 10, 3, 3, 15.00, 25.00, false, null, $warehouse);
        $stock = $this->createStock('M2', 'Oil filters for M.F.I trucks', 6, 2, 2, 10.00, 20.00, false, null, $warehouse);
        $stock = $this->createStock('S1', 'Air intake hose for Servol Truck', 0, 0, 0, 15.00, 20.00, false, 'H2', $warehouse);

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 9; // the order in which fixtures will be loaded
    }

    private function createStock(string $code, string $description, int $stockQuantity, int $min, int $reorder, float $issue, float $list, bool $nonStock, ?string $prevcode, Location $warehouse): Stock
    {
        $stock = new Stock();

        $stock->setCode($code);
        $stock->setDescription($description);
        $stock->setLastOrdered(new \DateTime());
        $stock->setReorderQuantity($reorder);
        $stock->setMinStock($min);
        $stock->setNonStock($nonStock);
        $stock->setPrevCode($prevcode);

        $supplierStock = $this->createSupplierStock($stock, 'supplier-' . $this->counter, $issue, $list);

        $stock->addSupplierStock($supplierStock);

        if ($stockQuantity > 0) {
            for ($i = 0; $i < $stockQuantity; ++$i) {
                $stockItem = new StockItem();
                $stockItem->setStock($stock);
                $stockItem->setSupplierStock($supplierStock);
                $stockItem->setLocation($warehouse);
                $this->manager->persist($stockItem);
            }
        }

        $this->manager->persist($stock);
        $this->addReference('stock-' . $this->counter, $stock);
        ++$this->counter;

        return $stock;
    }

    private function createSupplierStock(Stock $stockItem, string $supplier, float $issue, float $list): SupplierStock
    {
        $supplierStock = new SupplierStock();
        $costPrice     = (random_int(1000, 20000) / 100);
        $carriagePrice = ($costPrice * 1.1);

        $supplierStock->setStock($stockItem);

        if ($supplier && $this->hasReference($supplier)) {
            $supplierStock->setSupplier($this->getReference($supplier));
        } else {
            $supplierStock->setSupplier($this->getReference('supplier-1'));
        }

        $supplierStock->setQuantityOnOrder(random_int(0, 9));
        $supplierStock->setUnits(random_int(0, 20));
        $supplierStock->setCostPrice($costPrice);
        $supplierStock->setCarriagePrice($carriagePrice);
        $supplierStock->setIssuePrice($issue);
        $supplierStock->setCostPrice($list);

        $this->manager->persist($supplierStock);

        $this->addReference('supplierStock-' . $this->supplierStockCounter, $supplierStock);
        ++$this->supplierStockCounter;

        return $supplierStock;
    }

    private function resetAutoIncrementId(): void
    {
        $metaData  = $this->manager->getMetadataFactory()->getMetadataFor(Stock::class);
        $tableName = $metaData->getTableName();
        $conn      = $this->manager->getConnection();
        $conn->prepare('ALTER TABLE `' . $tableName . '` AUTO_INCREMENT = 1')->execute();
    }
}
