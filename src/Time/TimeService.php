<?php

namespace App\Time;

class TimeService
{
    public function getTime(): int
    {
        return time();
    }

    public function getNewDateTime($time = 'now', $timezone = null): \DateTime
    {
        return new \DateTime($time, $timezone);
    }

    public function sleep($seconds): void
    {
        sleep($seconds);
    }

    public function usleep($microseconds): void
    {
        usleep($microseconds);
    }
}
