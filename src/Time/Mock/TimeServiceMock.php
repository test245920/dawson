<?php

namespace App\Time\Mock;

class TimeServiceMock
{
    private string $datetime = '2000-01-01 00:00:00';

    public function getTime(): int|bool
    {
        return strtotime($this->datetime);
    }

    public function getNewDateTime($time = 'now', $timezone = null): \DateTime
    {
        $dt = new \DateTime($this->datetime, $timezone);
        if ($time == '') {
            $time = 'now';
        }

        if ($time !== 'now') {
            $dt->modify($time);
        }

        return $dt;
    }

    public function sleep($seconds): void
    {
        sleep(0);
    }

    public function usleep($microseconds): void
    {
        usleep(0);
    }
}
