<?php

namespace App\Twig\Extension;

use App\Entity\Image;
use App\Util\StringHelpers;
use Symfony\Bundle\TwigBundle\DependencyInjection\TwigExtension;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Twig\Extension\ExtensionInterface;
use Twig\TwigFilter;
use Twig\TwigFunction;

class MainExtension extends TwigExtension implements ExtensionInterface
{
    protected $currentUser;

    public function __construct(protected ContainerInterface $container)
    {
    }

    public function getName(): string
    {
        return 'velocity42.main.extension';
    }

    public function getFilters(): array
    {
        return [new TwigFilter('json_decode', $this->jsonDecode(...)), new TwigFilter('ucwords', $this->ucwords(...)), new TwigFilter('display_date_format', $this->displayDateFormat(...)), new TwigFilter('get_image_url', $this->getImageUrl(...))];
    }

    public function getFunctions(): array
    {
        return [new TwigFunction('is_engineer', $this->isEngineer(...)), new TwigFunction('worklog_time_string', $this->getWorklogTimeString(...))];
    }

    public function getGlobals(): array
    {
        return ['user' => $this->currentUser];
    }

    public function onKernelRequest(\Symfony\Component\HttpKernel\Event\RequestEvent $event): void
    {
        $this->request  = $event->getRequest();
        $exception      = $this->request->get('exception');
        $isException    = $exception ? in_array($exception->getStatusCode(), [403, 404], true) : false;

        if ($event->getRequestType() !== HttpKernelInterface::MAIN_REQUEST && !$isException) {
            return;
        }

        $token             = $this->container->get('security.authorization_checker')->getToken();
        $this->currentUser = $token ? ($token->getUser() !== 'anon.' ? $token->getUser() : null) : null;
    }

    public function isEngineer(): bool
    {
        $context = $this->container->get('security.authorization_checker');

        return $context->isGranted('ROLE_ENGINEER') && !$context->isGranted('ROLE_SUPER_ADMIN');
    }

    public function getWorklogTimeString($time, $overtime = null, $extremeOvertime = null, $absenceTime = null): string
    {
        return StringHelpers::formatWorklogTimesAsString($time, $overtime, $extremeOvertime, $absenceTime);
    }

    public function jsonDecode($string)
    {
        return json_decode((string) $string, true, 512, JSON_THROW_ON_ERROR);
    }

    public function ucwords($string): string
    {
        return ucwords((string) $string);
    }

    public function displayDateFormat($date): string
    {
        $str = date_format($date, 'd/m/Y');

        return $str;
    }

    public function getImageUrl($image): string
    {
        $em       = $this->container->get('doctrine')->getManager();
        $image    = $em->getRepository(Image::class)->find($image);
        $path     = $image->getPath();
        $filename = $image->getFilename();
        $url      = $this->container->getParameter('media_base_url') . $path . '/' . $filename;

        return $url;
    }

    public function getTokenParsers(): array
    {
        return [];
    }

    public function getNodeVisitors(): array
    {
        return [];
    }

    public function getTests(): array
    {
        return [];
    }

    public function getOperators(): array
    {
        return [];
    }
}
