<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UniqueBin extends Constraint
{
    public string $service = 'v42.validator.unique_bin';

    public string $message = 'This bin number is already used by {{existingItem}}.';

    public function validatedBy(): string
    {
        return $this->service;
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
