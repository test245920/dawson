<?php

namespace App\Validator;

use App\Entity\Stock;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UniqueBinValidator extends ConstraintValidator
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function validate($entity, Constraint $constraint): void
    {
        if (!$entity->getBin()) {
            return;
        }

        $repo = $this->em->getRepository(Stock::class);
        $existingForBin = $repo->findByBin($entity->getBin());

        if ((is_countable($existingForBin) ? count($existingForBin) : 0) > 0) {
            foreach ($existingForBin as $existingItem) {
                if ($existingItem->getId() === $entity->getId()) {
                    continue;
                }

                $this->context->addViolation($constraint->message, ['{{existingItem}}' => $existingItem->getStockString()], $entity->getBin());
            }
        }
    }
}
