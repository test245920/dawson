<?php

namespace App\Controller;

use App\Entity\Source;
use App\Form\Type\SourceType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SourceController extends Controller
{
    public function list(): Response
    {
        $this->securityCheck('ROLE_SUPER_ADMIN');
        $sources = $this->getRepo(Source::class)->findAll();

        return $this->render('Source/list.html.twig', ['sources' => $sources]);
    }

    public function view($id = null): Response
    {
        $this->securityCheck('ROLE_SUPER_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No Source ID was supplied.');
        }

        $source = $this->getRepo(Source::class)->find($id);

        if (!$source) {
            throw $this->createNotFoundException('Source with ID "' . $id . '" not found.');
        }

        $versions = $this->getVersionRepo()->getLogEntries($source);

        return $this->render('Source/view.html.twig', ['source'   => $source, 'versions' => $versions]);
    }

    public function new(Request $request, EntityManagerInterface $em): JsonResponse
    {
        $this->securityCheck('ROLE_SUPER_ADMIN');

        $response = new JsonResponse();
        $source   = new Source();

        $form = $this->createForm(SourceType::class, $source);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $source = $form->getData();

            $em->persist($source);
            $em->flush();

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The source "' . $source->getId() . '" was successfully created.');

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Source/_form.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('new_source'), 'isEdit'    => false, 'source'      => $source])]);

        return $response;
    }

    public function edit(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SUPER_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No Source ID was supplied.');
        }

        $response = new JsonResponse();
        $source = $this->getRepo(Source::class)->find($id);

        if (!$source) {
            throw $this->createNotFoundException('Source with ID "' . $id . '" not found.');
        }

        $form = $this->createForm(SourceType::class, $source);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $source = $form->getData();

            $em->persist($source);
            $em->flush();

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The Source "' . $source->getId() . '" was successfully edited.');

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Source/_form.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('edit_source', ['id' => $id]), 'isEdit'    => true, 'source'    => $source])]);

        return $response;
    }

    public function delete(EntityManagerInterface $em, $id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SUPER_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No Source ID was supplied.');
        }

        $source = $this->getRepo(Source::class)->find($id);

        if (!$source) {
            throw $this->createNotFoundException('Source with ID "' . $id . '" not found.');
        }

        foreach ($source->getLeads() as $lead) {
            $lead->setSource(null);
            $em->persist($lead);
        }

        $em->remove($source);
        $em->flush();

        $this->setFlash('success', 'The Source "' . $source->getName() . '" was successfully deleted.');

        return $this->redirect($this->generateUrl('list_sources'));
    }
}
