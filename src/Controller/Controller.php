<?php

namespace App\Controller;

use App\Entity\LogEntry;
use App\Manager\AssetManager;
use App\Manager\EmailManager;
use App\Manager\EstimateManager;
use App\Manager\InvoiceManager;
use App\Manager\JobManager;
use App\Manager\LeadManager;
use App\Manager\PdfManager;
use App\Manager\SearchManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class Controller extends AbstractController
{
    public function __construct(private readonly LeadManager $leadManager, private readonly EmailManager $emailManager, private readonly InvoiceManager $invoiceManager, private readonly PdfManager $pdfManager, private readonly SearchManager $searchManager, private readonly AssetManager $assetManager, private readonly JobManager $jobManager, private readonly EstimateManager $estimateManager, private readonly EntityManagerInterface $entityManager, private readonly ManagerRegistry $managerRegistry)
    {
    }

    public function generateAbsoluteUrl(string $route, array $parameters = []): string
    {
        return $this->generateUrl($route, $parameters, UrlGeneratorInterface::ABSOLUTE_URL);
    }

    protected function getRepo(string $key): \Doctrine\Persistence\ObjectRepository
    {
        return $this->managerRegistry->getRepository($key);
    }

    protected function isGranted($attribute, $object = null): bool
    {
        return $this->getAuthorizationChecker()->isGranted($attribute, $object);
    }

    protected function securityCheck($attributes, $object = null)
    {
        if (is_array($attributes)) {
            foreach ($attributes as $attribute) {
                if ($this->isGranted($attribute, $object)) {
                    return;
                }
            }

            throw new AccessDeniedException();
        }

        if (!$this->isGranted($attributes, $object)) {
            throw new AccessDeniedException();
        }
    }

    protected function createAccessDeniedException($message = null, ?\Throwable $previous = null): AccessDeniedException
    {
        throw new AccessDeniedException($message);
    }

    protected function setFlash($type, $message)
    {
        $this->get('session')->getFlashBag()->add($type, $message);
    }

    protected function getCurrentUser(): ?\Symfony\Component\Security\Core\User\UserInterface
    {
        $token = $this->getTokenStorage()->getToken();
        $user  = $token === null ? null : $token->getUser();

        if ($user === 'anon.') {
            return null;
        }

        return $user;
    }

    protected function getSearchManager(): SearchManager
    {
        return $this->searchManager;
    }

    protected function getAssetManager(): AssetManager
    {
        return $this->assetManager;
    }

    protected function getJobManager(): JobManager
    {
        return $this->jobManager;
    }

    protected function getEstimateManager(): EstimateManager
    {
        return $this->estimateManager;
    }

    protected function getVersionRepo(): \Doctrine\Persistence\ObjectRepository
    {
        return $this->getRepo(LogEntry::class);
    }

    protected function getEmailManager(): EmailManager
    {
        return $this->emailManager;
    }

    protected function getLeadManager(): LeadManager
    {
        return $this->leadManager;
    }

    protected function getInvoiceManager(): InvoiceManager
    {
        return $this->invoiceManager;
    }

    protected function getPdfManager(): PdfManager
    {
        return $this->pdfManager;
    }

    protected function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    protected function getAuthorizationChecker(): AuthorizationCheckerInterface
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->get('security.authorization_checker');
    }

    protected function getTokenStorage(): TokenStorageInterface
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->get('security.token_storage');
    }
}
