<?php

namespace App\Controller;

class SearchController extends Controller
{
    public function find(?string $category = null): \Symfony\Component\HttpKernel\Exception\NotFoundHttpException|\Symfony\Component\HttpFoundation\Response
    {
        $searchManager = $this->getSearchManager();

        if (!$searchManager->isCategoryValid($category)) {
            return $this->createNotFoundException('Invalid search category "' . $category . '".');
        }

        if ($category) {
            $requiredRole = $searchManager->getRequiredRoleForCategory($category);
            $this->securityCheck($requiredRole);
        }

        $repos = $searchManager->getRepositories($category);
        $items = [];

        $fullAdmin = $this->isGranted('ROLE_SUPER_ADMIN');

        foreach ($repos as $repo) {
            $items = array_merge($repo->getItemsForSearch($this->getCurrentUser(), $fullAdmin), $items);
        }

        return $this->render('Search/find.html.twig', ['category' => $category, 'items'    => json_encode($items, JSON_THROW_ON_ERROR)]);
    }
}
