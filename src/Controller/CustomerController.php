<?php

namespace App\Controller;

use App\Entity\BillingAddress;
use App\Entity\Contact;
use App\Entity\Customer;
use App\Entity\CustomerNumber;
use App\Entity\Job;
use App\Entity\SiteAddress;
use App\Form\Type\CustomerType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CustomerController extends Controller
{
    public function list($page = 1): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $pager     = $this->getRepo(Customer::class)->getCustomersPaged($page);
        $customers = $pager->getCurrentPageResults();

        return $this->render('Customer/list.html.twig', ['pager'     => $pager, 'customers' => $customers]);
    }

    public function new(Request $request, EntityManagerInterface $em, $hideExtraForm = null): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $response = new JsonResponse();
        $customer = new Customer();
        $form     = $this->createForm(CustomerType::class, $customer);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $customer = $form->getData();

            $customerLetter = $request->request->get('customer-number');

            $customerNumber = new CustomerNumber($customer);
            $customerNumber->setLetter($customerLetter);

            $em->getFilters()->disable('softdeleteable');
            $newNumber = $this->getRepo(CustomerNumber::class)->getNextNumberForLetter($customerLetter);
            $em->getFilters()->enable('softdeleteable');

            $customerNumber->setNumber($newNumber);
            $customerNumber->setCustomerNumber($customerLetter . $newNumber);
            $customerNumber->setAssignedAt(new \DateTime());

            $customer->getCustomerNumbers()->add($customerNumber);

            $billingAddress = $customer->getBillingAddress();
            if (!$customer->getBillingAddress()) {
                $billingAddress = new BillingAddress();
            }

            $billingAddress->setCustomer($customer);
            $customer->setBillingAddress($billingAddress);

            $siteAddresses = $customer->getSiteAddresses();
            foreach ($siteAddresses as $siteAddress) {
                $siteAddress->setCustomer($customer);
            }

            $em->persist($customer);
            $em->flush();

            if ($hideExtraForm) {
                $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true, 'customer'    => ['name' => $customer->getName(), 'id'   => $customer->getId()]]);
            } else {
                $response->setData(['success'     => true, 'displayForm' => true, 'refresh'     => true, 'customer'    => ['name' => $customer->getName(), 'id'   => $customer->getId()], 'html' => $this->renderView('Customer/_customerSuccess.html.twig', ['customer' => $customer->getName()])]);
            }

            $this->setFlash('success', 'The customer "' . $customer->getName() . '" has been successfully added');

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Customer/_form.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('new_customer'), 'customer'  => $customer, 'isEdit'    => false])]);

        return $response;
    }

    public function edit(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $response = new JsonResponse();

        $customer = $this->getRepo(Customer::class)->find($id);
        $jobs = $this->getRepo(Job::class)->findBy(['customer' => $customer]);

        if (!$customer) {
            $response->setData(['success'     => false, 'displayForm' => false, 'refresh'     => false]);

            $this->setFlash('error', 'The customer with ID "' . $id . '" was not found.');

            return $response;
        }

        $storedSiteAddresses = new ArrayCollection();
        foreach ($customer->getSiteAddresses() as $siteAddress) {
            $storedSiteAddresses->add($siteAddress);
        }

        $storedContacts = new ArrayCollection();
        foreach ($customer->getContacts() as $contact) {
            $storedContacts->add($contact);
        }

        $form = $this->createForm(CustomerType::class, $customer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $customer           = $form->getData();
            $newLetter          = strtoupper($request->request->get('customer-number'));
            $formSiteAddresses  = $customer->getSiteAddresses();

            foreach ($storedSiteAddresses as $storedSiteAddress) {
                if ($customer->getSiteAddresses()->contains($storedSiteAddress) === false) {
                    $storedSiteAddress->setCustomer(null);
                    foreach ($storedSiteAddress->getAssetLifecyclePeriods() as $assetPeriod) {
                        $assetPeriod->setSiteAddress(null);
                        $storedSiteAddress->removeAssetLifecyclePeriod($assetPeriod);
                    }

                    $em->persist($storedSiteAddress);
                }
            }

            foreach ($storedContacts as $storedContact) {
                if ($customer->getContacts()->contains($storedContact) === false) {
                    $storedContact->setCustomer(null);
                    $em->persist($storedContact);
                }
                foreach ($jobs as $job) {
                    if (in_array($storedContact, $job->getContacts())) {
                        $job->removeContact($storedContact);
                        $em->persist($job);
                    }
                }
            }

            if (strlen($newLetter) === 1) {
                $letterExistsForCustomer = $this->getRepo(CustomerNumber::class)->checkForExistingLetter($newLetter, $id);

                if (!$letterExistsForCustomer) {
                    $customerNumber = new CustomerNumber($customer);
                    $customerNumber->setLetter($newLetter);

                    $newNumber = $this->getRepo(CustomerNumber::class)->getNextNumberForLetter($newLetter);

                    $customerNumber->setNumber($newNumber);
                    $customerNumber->setCustomerNumber($newLetter . $newNumber);
                    $customerNumber->setAssignedAt(new \DateTime());

                    $customer->getCustomerNumbers()->add($customerNumber);
                } else {
                    $letterExistsForCustomer->setAssignedAt(new \DateTime());
                }
            }

            $billingAddress = $customer->getBillingAddress();
            if (!$customer->getBillingAddress()) {
                $billingAddress = new BillingAddress();
            }
            $billingAddress->setCustomer($customer);
            $customer->setBillingAddress($billingAddress);

            $em->persist($customer);
            $em->flush();

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The customer "' . $customer->getName() . '" has been successfully edited');

            return $response;
        }

        $response->setData(['success'     => true, 'displayForm' => true, 'isEdit'      => true, 'refresh'     => false, 'html'        => $this->renderView('Customer/_form.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('edit_customer', ['id' => $id]), 'customer'  => $customer, 'isEdit'    => true])]);

        return $response;
    }

    public function view(?string $id): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if (!$id) {
            throw $this->createNotFoundException('No customer ID supplied.');
        }

        $customer = $this->getRepo(Customer::class)->find($id);

        if (!$customer) {
            throw $this->createNotFoundException('Customer with ID ' . $id . ' not found.');
        }

        $versions = $this->getVersionRepo()->getLogEntries($customer);

        return $this->render('Customer/view.html.twig', ['customer' => $customer, 'versions' => $versions]);
    }

    public function delete(EntityManagerInterface $em, $id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $customer = $this->getRepo(Customer::class)->find($id);

        if (!$customer) {
            throw $this->createNotFoundException('The customer with ID"' . $id . '" was not found.');
        }

        $customer->setIsActive(0);
        $em->persist($customer);
        $em->remove($customer);
        $em->flush();

        $this->setFlash('success', 'Customer "' . $customer->getName() . '" was successfully deleted');

        return $this->redirect($this->generateUrl('list_customers'));
    }

    public function getSites($id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No customer ID supplied.');
        }

        $customer       = $this->getRepo(Customer::class)->find($id);
        $siteAddresses  = $customer->getSiteAddresses();

        $sites = [];

        foreach ($siteAddresses as $siteAddress) {
            $sites[] = ['id' => $siteAddress->getId(), 'addressString' => $siteAddress->getAddressString()];
        }

        $response = new JsonResponse();

        return $response->setData(['sites' => $sites]);
    }

    public function getContacts($id = null): JsonResponse
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_SALES_ADMIN']);

        if ($id === null) {
            throw $this->createNotFoundException('No customer ID supplied');
        }

        $contacts = $this->getRepo(Contact::class)->getContactsForCustomer($id);

        $response = new JsonResponse();

        return $response->setData(['contacts' => $contacts]);
    }

    public function getJobHistoryReport($id = null): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if (!$id) {
            throw $this->createNotFoundException('No customer ID supplied.');
        }

        $customer = $this->getRepo(Customer::class)->find($id);

        if (!$customer) {
            throw $this->createNotFoundException('Customer with ID ' . $id . ' not found.');
        }

        return $this->render('Customer/jobHistoryPerCustomer.html.twig', ['customer' => $customer]);
    }

    public function getJobHistoryReportForAddress($id = null, $siteAddress = null): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if (!$id) {
            throw $this->createNotFoundException('No customer ID supplied.');
        }

        $customer = $this->getRepo(Customer::class)->find($id);
        if (!$customer) {
            throw $this->createNotFoundException('Customer with ID ' . $id . ' not found.');
        }

        $address = null;
        if (!$siteAddress) {
            $jobs = $this->getRepo(Job::class)->findBy(['customer' => $customer, 'siteAddress' => null]);
        } else {
            $address = $this->getRepo(SiteAddress::class)->find($siteAddress);
            $jobs    = $this->getRepo(Job::class)->findBy(['customer' => $customer, 'siteAddress' => $address]);
        }
        $address = $address ? $address->getAddressString() : $customer->getBillingAddress()->getAddressString();

        return $this->render('Customer/jobHistoryPerAddress.html.twig', ['customer' => $customer, 'address'  => $address, 'jobs'     => $jobs]);
    }

    public function search(Request $request): JsonResponse
    {
        $content    = $request->getContent();
        $searchName = json_decode($content, true, 512, JSON_THROW_ON_ERROR);

        $customers = $this->getRepo(Customer::class)->getCustomerByName($searchName);

        $response   = new JsonResponse();

        $response->setData([
            'customers'       => $customers,
        ]);

        return $response;
    }
}
