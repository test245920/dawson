<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Zenstruck\ScheduleBundle\Schedule\ScheduleRunner;

#[Route(path: '/run-schedule')]
class RunScheduleController
{
    public function __invoke(ScheduleRunner $scheduleRunner): Response
    {
        $result = $scheduleRunner();

        return new Response('', $result->isSuccessful() ? 200 : 500);
    }
}
