<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Entity\PurchaseOrder;
use App\Entity\Supplier;
use App\Entity\SupplierStock;
use App\Entity\SupplierStockIssuePriceRule;
use App\Form\Type\SupplierStockIssuePriceRuleType;
use App\Form\Type\SupplierType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SupplierController extends Controller
{
    public function list($page = 1): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $pager     = $this->getRepo(Supplier::class)->getSuppliersPaged($page);
        $suppliers = $pager->getCurrentPageResults();

        return $this->render('Supplier/list.html.twig', ['pager'     => $pager, 'suppliers' => $suppliers]);
    }

    public function view(?string $id): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if (!$id) {
            throw $this->createNotFoundException('No supplier ID supplied.');
        }

        $supplier = $this->getRepo(Supplier::class)->find($id);

        if (!$supplier) {
            throw $this->createNotFoundException('Supplier with ID ' . $id . ' not found.');
        }

        $outstandingOrders = $this->getRepo(PurchaseOrder::class)->getOutstandingOrdersForSupplier($supplier);
        $showPrintButton   = $outstandingOrders ? true : false;

        $versions = $this->getVersionRepo()->getLogEntries($supplier);

        return $this->render('Supplier/view.html.twig', ['supplier'        => $supplier, 'versions'        => $versions, 'showPrintButton' => $showPrintButton]);
    }

    public function new(Request $request, EntityManagerInterface $em): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $response = new JsonResponse();

        $supplier = new Supplier();
        $form     = $this->createForm(SupplierType::class, $supplier);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $supplier = $form->getData();

            $em->persist($supplier);
            $em->flush();

            $response->setContent(json_encode(['success'     => true, 'displayForm' => false, 'refresh'     => true]));

            $this->setFlash('success', 'The supplier "' . $supplier->getName() . '" has been successfully added');

            return $response;
        }

        $response->setData(['success'     => true, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Supplier/_form.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('new_supplier'), 'supplier'  => $supplier, 'isEdit'      => false])]);

        return $response;
    }

    public function edit(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $response = new JsonResponse();

        $supplier = $this->getRepo(Supplier::class)->find($id);

        if (!$supplier) {
            $response->setData(['success'     => false, 'displayForm' => false, 'refresh'     => false]);

            $this->setFlash('error', 'The supplier with ID "' . $id . '" was not found.');

            return $response;
        }

        $storedContacts = new ArrayCollection();
        foreach ($supplier->getContacts() as $contact) {
            $storedContacts->add($contact);
        }

        $supplierId = $supplier->getId();
        $form       = $this->createForm(SupplierType::class, $supplier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $supplier = $form->getData();

            foreach ($storedContacts as $storedContact) {
                if ($supplier->getContacts()->contains($storedContact) === false) {
                    $storedContact->setSupplier(null);
                    $em->persist($storedContact);
                }
            }

            $em->persist($supplier);
            $em->flush();

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The supplier "' . $supplier->getName() . '" has been successfully edited');

            return $response;
        }

        $response->setData(['success'     => true, 'displayForm' => true, 'isEdit'      => true, 'refresh'     => false, 'html'        => $this->renderView('Supplier/_form.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('edit_supplier', ['id' => $id]), 'supplier'  => $supplier, 'isEdit'    => true])]);

        return $response;
    }

    public function delete(EntityManagerInterface $em, $id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $supplier = $this->getRepo(Supplier::class)->find($id);

        if (!$supplier) {
            throw $this->createNotFoundException('The supplier with ID"' . $id . '" was not found.');
        }

        $em->remove($supplier);
        $em->flush();

        $this->setFlash('success', 'Supplier "' . $supplier->getName() . '" was successfully deleted');

        return $this->redirect($this->generateUrl('list_suppliers'));
    }

    public function getContacts($id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No supplier ID supplied');
        }

        $contacts = $this->getRepo(Contact::class)->getContactsForSupplier($id);

        $response = new JsonResponse();

        return $response->setData(['contacts' => $contacts]);
    }

    public function printOutstandingOrders(?string $id): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if (!$id) {
            throw $this->createNotFoundException('No supplier ID supplied.');
        }

        $supplier = $this->getRepo(Supplier::class)->find($id);

        if (!$supplier) {
            throw $this->createNotFoundException('Supplier with ID ' . $id . ' not found.');
        }

        $purchaseOrders = $this->getRepo(PurchaseOrder::class)->getOutstandingOrdersForSupplier($supplier);
        $versions       = $this->getVersionRepo()->getLogEntries($supplier);

        return $this->render('Supplier/printOutstandingOrders.html.twig', ['supplier'       => $supplier, 'purchaseOrders' => $purchaseOrders, 'versions'       => $versions]);
    }

    public function newStockIssuePriceRule(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No supplier ID was supplied');
        }

        $supplier = $this->getRepo(Supplier::class)->find($id);

        if (!$supplier) {
            throw $this->createNotFoundException('The supplier with ID "' . $id . '" was not found.');
        }

        $response = new JsonResponse();

        $supplierStockIssuePriceRule    = new SupplierStockIssuePriceRule();
        $form                           = $this->createForm(SupplierStockIssuePriceRuleType::class, $supplierStockIssuePriceRule);

        $supplierStockIssuePriceRule->setSupplier($supplier);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $supplierStockIssuePriceRule = $form->getData();

            $em->persist($supplierStockIssuePriceRule);
            $em->flush();

            $supplier->addStockPriceRule($supplierStockIssuePriceRule);
            $em->persist($supplier);
            $this->refreshIssuePriceRule($supplier, $supplierStockIssuePriceRule);
            $em->flush();

            $response->setContent(json_encode(['success'     => true, 'displayForm' => false, 'refresh'     => true]));

            $this->setFlash('success', 'The supplier stock issue price rule has been successfully added');

            return $response;
        }

        $response->setData(['success'     => true, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Supplier/_newStockIssuePriceRuleForm.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('new_stock_issue_price_rule', ['id' => $id])])]);

        return $response;
    }

    public function editStockIssuePriceRule(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No supplier stock issue price rule supplied.');
        }

        $supplierStockIssuePriceRule    = $this->getRepo(SupplierStockIssuePriceRule::class)->find($id);
        $supplier                       = $supplierStockIssuePriceRule ? $supplierStockIssuePriceRule->getSupplier() : null;
        $response                       = new JsonResponse();
        $form                           = $this->createForm(SupplierStockIssuePriceRuleType::class, $supplierStockIssuePriceRule);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $supplierStockIssuePriceRule = $form->getData();

            $em->persist($supplierStockIssuePriceRule);
            $this->refreshIssuePriceRule($supplier, $supplierStockIssuePriceRule);

            $em->flush();

            $response->setContent(json_encode(['success'     => true, 'displayForm' => false, 'refresh'     => true]));

            $this->setFlash('success', 'The supplier stock issue price rule has been successfully updated');

            return $response;
        }

        $response->setData(['success'     => true, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Supplier/_newStockIssuePriceRuleForm.html.twig', ['form'        => $form->createView(), 'formRoute'   => $this->generateUrl('edit_stock_issue_price_rule', ['id' => $id]), 'supplierStockIssuePriceRule' => $supplierStockIssuePriceRule])]);

        return $response;
    }

    public function deleteStockIssuePriceRule(EntityManagerInterface $em, $id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $supplierStockIssuePriceRule = $this->getRepo(SupplierStockIssuePriceRule::class)->find($id);

        if (!$supplierStockIssuePriceRule) {
            throw $this->createNotFoundException('The supplier stock issue price rule with ID"' . $id . '" was not found.');
        }

        $supplier = $supplierStockIssuePriceRule->getSupplier();
        $supplier->removeStockPriceRule($supplierStockIssuePriceRule);

        $em->persist($supplier);
        $em->remove($supplierStockIssuePriceRule);
        $em->flush();

        $this->setFlash('success', 'supplier stock issue price rule was successfully deleted');

        return $this->redirect($this->generateUrl('view_supplier', ['id' => $supplier->getId()]));
    }

    private function refreshIssuePriceRule(Supplier $supplier, SupplierStockIssuePriceRule $supplierStockIssuePriceRule): void
    {
        $this->getRepo(SupplierStock::class)->updateIssuePricesFromRule($supplier, $supplierStockIssuePriceRule);
    }
}
