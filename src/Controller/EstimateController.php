<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Entity\Estimate;
use App\Entity\EstimateEvent;
use App\Entity\Job;
use App\Form\Type\EstimateType;
use App\Form\Type\RejectedEstimateType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EstimateController extends Controller
{
    public function view(?string $id): Response
    {
        $this->securityCheck(['ROLE_CUSTOMER', 'ROLE_SERVICE_ADMIN']);

        if (!$id) {
            throw $this->createNotFoundException('No estimate ID supplied');
        }

        $user     = $this->getCurrentUser();
        $estimate = $this->getRepo(Estimate::class)->find($id);

        if (!$estimate) {
            throw $this->createNotFoundException('Estimate with ID "' . $id . '" not found.');
        }

        if ($this->isGranted('ROLE_CUSTOMER')) {
            $estimateCustomer = $estimate->getCustomer();
            $userCustomer     = $user->getCustomer() ?: ($user->getContact() ? $user->getContact()->getCustomer() : null);

            if (!$userCustomer || $userCustomer !== $estimateCustomer) {
                throw $this->createAccessDeniedException('Access denied to estimate.');
            }

            if (!$estimate->getIssued()) {
                throw $this->createNotFoundException('Estimate not issued.');
            }
        }

        $versions = $this->getVersionRepo()->getLogEntries($estimate);

        return $this->render('Estimate/view.html.twig', ['user'     => $user, 'estimate' => $estimate, 'versions' => $versions]);
    }

    public function list(): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');
        $estimates  = $this->getRepo(Estimate::class)->getEstimatesForListing();
        $priorities = $this->getEstimateManager()->getEstimatePriorityForListingsEstimates($estimates);

        return $this->render('Estimate/list.html.twig', ['estimates'  => $estimates, 'priorities' => $priorities]);
    }

    public function new(Request $request, EntityManagerInterface $em, $jobId = null): JsonResponse
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);

        $response = new JsonResponse();
        $estimate = new Estimate();

        $estimate->setPending(1);
        $formRouteParams = [];
        if ($jobId) {
            $job = $this->getRepo(Job::class)->find($jobId);

            if (!$job) {
                throw $this->createNotFoundException('The Job with ID "' . $jobId . '" was not found.');
            }
            $formRouteParams = ['jobId' => $job->getId()];
            $estimate->setJob($job);
            $estimate->setCustomer($job->getCustomer());
        } else {
            if (!$this->isGranted('ROLE_SERVICE_ADMIN')) {
                throw $this->createAccessDeniedException('Only estimate admins can create an estimate without specifying a job.');
            }
        }

        $isEngineer = $this->isGranted('ROLE_ENGINEER') && !$this->isGranted('ROLE_SUPER_ADMIN');

        $form = $this->createForm(EstimateType::class, $estimate, ['isEngineer' => $isEngineer]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->has('newContacts')) {
                $newContacts = $form['newContacts']->getData();

                foreach ($newContacts as $newContact) {
                    $contact = new Contact();
                    $contact->setName($newContact->getName());
                    $contact->setTelephone($newContact->getTelephone());
                    $contact->setEmail($newContact->getEmail());
                    $contact->setCustomer($estimate->getCustomer());
                    $contact->setJobTitle($newContact->getJobTitle());
                    $contact->setMobile($newContact->getMobile());

                    $em->persist($contact);
                    $em->flush();
                    $estimate->addContact($contact);
                }
            }

            $this->saveEstimate($estimate);

            if ($estimate->issueImmediately) {
                $this->issueEstimate($estimate);
            }

            $estimateIdentifier = $estimate->getDetails() ?: $estimate->getId() . ' - ' . $estimate->getCustomer()->getName();

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The estimate "' . $estimateIdentifier . '" was successfully added.');

            return $response;
        }

        $response->setData(['success'     => true, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Estimate/_form.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('new_estimate', $formRouteParams), 'isEdit'    => false, 'estimate'  => $estimate])]);

        return $response;
    }

    public function estimateParticulars($id = null): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $estimate = $this->getRepo(Estimate::class)->find($id);

        return $this->render('Estimate/particulars.html.twig', ['estimate' => $estimate]);
    }

    public function edit(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $response = new JsonResponse();
        $estimate = $this->getRepo(Estimate::class)->find($id);

        if (!$estimate) {
            $response->setData(['success'     => false, 'displayForm' => false, 'message'     => 'The estimate was not found.', 'refresh'     => false]);

            return $response;
        }

        $storedEstimateStocks = new ArrayCollection();
        foreach ($estimate->getEstimateStocks() as $estimateStock) {
            $storedEstimateStocks->add($estimateStock);
        }

        $storedEstimateItems = new ArrayCollection();
        foreach ($estimate->getEstimateItems() as $estimateItem) {
            $storedEstimateItems->add($estimateItem);
        }

        $form = $this->createForm(EstimateType::class, $estimate, ['isEngineer' => false]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $estimate = $form->getData();

            $newContacts = $form['newContacts']->getData();
            foreach ($newContacts as $newContact) {
                $contact = new Contact();
                $contact->setName($newContact->getName());
                $contact->setTelephone($newContact->getTelephone());
                $contact->setEmail($newContact->getEmail());
                $contact->setCustomer($estimate->getCustomer());
                $contact->setJobTitle($newContact->getJobTitle());
                $contact->setMobile($newContact->getMobile());

                $estimate->addContact($contact);
                $em->persist($contact);
                $em->flush();
            }

            foreach ($storedEstimateStocks as $storedEstimateStock) {
                if ($estimate->getEstimateStocks()->contains($storedEstimateStock) === false) {
                    $estimate->removeEstimateStock($storedEstimateStock);
                    $em->remove($storedEstimateStock);
                    $em->persist($estimate);
                }
            }

            foreach ($storedEstimateItems as $storedEstimateItem) {
                if ($estimate->getEstimateItems()->contains($storedEstimateItem) === false) {
                    $estimate->removeEstimateItem($storedEstimateItem);
                    $em->remove($storedEstimateItem);
                    $em->persist($estimate);
                }
            }

            $this->saveEstimate($estimate);

            if ($estimate->getIssued() == 1) {
                $url      = $this->generateAbsoluteUrl('view_estimate', ['id' => $id]);
                $customer = $estimate->getCustomer();

                $body = $this->renderView('Estimate/_estimateEditEmail.html.twig', ['username'        => $customer->getName(), 'estimateDetails' => $estimate->getDetails(), 'url'             => $url]);

                $currentUser = $this->getCurrentUser();
                $email       = $this->getEmailManager()->sendEmail('Dawson Forklift Services altered your estimation.', $body, $currentUser->getEmail(), true);
            }

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The estimate "' . $estimate->getId() . '" has been successfully edited');

            return $response;
        }

        $response->setData(['success'     => true, 'displayForm' => true, 'refresh'     => false, 'isEdit'      => true, 'html'        => $this->renderView('Estimate/_form.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('edit_estimate', ['id' => $id]), 'estimate'  => $estimate])]);

        return $response;
    }

    public function delete(EntityManagerInterface $em, $id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $estimate = $this->getRepo(Estimate::class)->find($id);

        if (!$estimate) {
            throw $this->createNotFoundException('The estimate with ID"' . $id . '" was not found.');
        }

        $em->remove($estimate);
        $em->flush();

        $this->setFlash('success', 'Estimate "' . $estimate->getId() . '" was successfully deleted');

        return $this->redirect($this->generateUrl('list_estimates'));
    }

    public function negotiate(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck(['ROLE_CUSTOMER', 'ROLE_SERVICE_ADMIN']);

        $content = $request->getContent();

        if (!$content) {
            throw $this->createNotFoundException('No estimate data supplied');
        }

        $user          = $this->getCurrentUser();
        $content       = json_decode($content, true, 512, JSON_THROW_ON_ERROR);
        $inputText     = $content['inputText'];
        $estimate      = $this->getRepo(Estimate::class)->find($id);
        $estimateEvent = new EstimateEvent();

        if ($this->container->get('security.authorization_checker')->isGranted('ROLE_SERVICE_ADMIN')) {
            $estimateEvent->setIsAdminResponse(true);
            $estimateEvent->setAdminResponse($inputText);
            $estimate->setAwaitingResponse(0);

            $url      = $this->generateAbsoluteUrl('view_estimate', ['id' => $id]);
            $customer = $estimate->getCustomer();
            $body     = $this->renderView('Estimate/_estimateNegotiationEmail.html.twig', ['username'   => $customer->getName(), 'jobDetails' => $estimate->getDetails(), 'accepted'   => $estimateEvent->getAccepted(), 'url'        => $url]);
            $currentUser = $this->getCurrentUser();
            $email = $this->getEmailManager()->sendEmail('Dawson Forklift Services estimate negotiation message.', $body, $currentUser->getEmail(), true);
        } elseif ($this->container->get('security.authorization_checker')->isGranted('ROLE_CUSTOMER')) {
            $estimateEvent->setIsCustomerQuery(1);
            $estimateEvent->setCustomerQuery($inputText);
            $estimate->setQueryText($inputText);
            $estimate->setAwaitingResponse(1);
        } else {
            throw $this->createNotFoundException('Not a valid customer or admin action');
        }

        $estimateEvent->setEstimate($estimate);
        $estimateEvent->setUser($user);

        $estimate->setQueried(1);

        $em->persist($estimate);
        $em->persist($estimateEvent);
        $em->flush();
        $this->saveEstimate($estimate);

        $response = new JsonResponse();
        $response->setData(['text' => $inputText]);

        return $response;
    }

    public function decision(EntityManagerInterface $em, $id = null, $accepted = null): RedirectResponse
    {
        $this->securityCheck(['ROLE_CUSTOMER', 'ROLE_SERVICE_ADMIN']);

        if (!$id) {
            throw $this->createNotFoundException('No estimate ID was supplied.');
        }

        if ($accepted === null) {
            throw $this->createNotFoundException('No decision parameter was supplied.');
        }

        $estimate      = $this->getRepo(Estimate::class)->find($id);
        $user          = $this->getCurrentUser();

        if ($estimate->getAccepted() || $estimate->getRejected()) {
            $this->setFlash('error', 'The estimate"' . $estimate->getDetails() . '" has already been settled. Contact help@dawson.com for more help.');

            return $this->redirect($this->generateUrl('view_account'));
        }

        $estimateEvent = new EstimateEvent();

        if ($accepted == true) {
            $estimateEvent->setAccepted(true);
        } elseif ($accepted == false) {
            $estimateEvent->setRejected(true);
        } else {
            throw $this->createNotFoundException('Not a valid customer or admin action');
        }

        $estimateEvent->setEstimate($estimate);
        $estimateEvent->setUser($user);

        $em->persist($estimateEvent);
        $em->flush();

        if ($accepted == true) {
            $estimate->setAccepted(1);
            $this->saveEstimate($estimate);
            $this->setFlash('success', 'The estimate "' . $estimate->getId() . '" was successfully accepted.');
        } elseif ($accepted == false) {
            $estimate->setRejected(1);
            $this->saveEstimate($estimate);
            $this->setFlash('error', 'The estimate "' . $estimate->getDetails() . '" was successfully declined.');
        }

        $url      = $this->generateAbsoluteUrl('view_estimate', ['id' => $id]);
        $customer = $estimate->getCustomer();

        $body     = $this->renderView('Estimate/_estimateDecisionEmail.html.twig', ['username'   => $customer->getName(), 'jobDetails' => $estimate->getDetails(), 'accepted'   => $estimateEvent->getAccepted(), 'url'        => $url]);

        $currentUser = $this->getCurrentUser();
        $email       = $this->getEmailManager()->sendEmail('Dawson Forklift Services estimate decision.', $body, $currentUser->getEmail(), true);

        return $this->redirect($this->isGranted('ROLE_CUSTOMER') ? $this->generateUrl('view_account') : $this->generateUrl('list_estimates'));
    }

    public function createJob(EntityManagerInterface $em, $id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $estimate = $this->getRepo(Estimate::class)->find($id);

        if (!$estimate) {
            throw $this->createNotFoundException('No estimate ID supplied.');
        }

        $response       = new JsonResponse();
        $job            = new Job();
        $estimateStocks = $estimate->getEstimateStocks();
        $estimateItems  = $estimate->getEstimateItems();
        $jobNotes       = '';

        $job->setCustomer($estimate->getCustomer());
        $job->setDetail($estimate->getDetails());
        $job->setIsGeneralRepairs(1);
        $job->setJobActiveFrom(new \DateTime());
        $job->addEstimate($estimate);

        foreach ($estimateStocks as $estimateStock) {
            $parts = $estimateStock->getSupplierStock()->getDisplayString();

            if ($estimateStock->getQuantity()) {
                $parts .= ' x ' . $estimateStock->getQuantity();
            }

            $jobNotes = $jobNotes . $parts . "\n";
        }

        foreach ($estimateItems as $estimateItem) {
            $parts = [];

            if ($estimateItem->getItem()) {
                $parts[] = $estimateItem->getItem();
            }

            if ($estimateItem->getPrice()) {
                $parts[] = '£' . $estimateItem->getPrice();
            }

            if ($estimateItem->getQuantity()) {
                $parts[] = ' x ' . $estimateItem->getQuantity();
            }

            $stockString = implode(' - ', $parts);
            $jobNotes    = $jobNotes . $stockString . "\n";
        }

        $job->setNotes($jobNotes);

        $this->getJobManager()->saveJob($job);

        $estimate->setJob($job);

        $this->getEstimateManager()->saveEstimate($estimate);

        $this->setFlash('success', 'The job "' . $job->getJobCode() . '" was successfully created using estimate "' . $estimate->getId() . '".');

        return $this->redirect($this->generateUrl('list_estimates'));
    }

    public function issueEstimateEmail($id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $estimate = $this->getRepo(Estimate::class)->find($id);

        if (!$estimate) {
            throw $this->createNotFoundException('No estimate ID supplied.');
        }

        $this->issueEstimate($estimate);
        $this->setFlash('success', 'Estimate "' . $estimate->getId() . '" was successfully emailed to the client');

        $job = $estimate->getJob();
        if ($job) {
            $this->getJobManager()->saveJob($job);
        }

        return $this->redirect($this->generateUrl('list_estimates'));
    }

    public function rejectedEstimateReason(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No Estimate ID was supplied.');
        }

        $estimate = $this->getRepo(Estimate::class)->find($id);

        if (!$estimate) {
            throw $this->createNotFoundException('No estimate with ID "' . $id . '" was found.');
        }

        $response = new JsonResponse();
        $form     = $this->createForm(RejectedEstimateType::class, $estimate);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $estimate = $form->getData();

            $estimate->setRejected(1);

            $this->saveEstimate($estimate);
            $this->setFlash('error', 'The estimate "' . $estimate->getDetails() . '" was successfully declined.');

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Estimate/_rejectedEstimateReasonForm.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('estimate_rejection_reason', ['id' => $id]), 'estimate'  => $estimate])]);

        return $response;
    }

    private function issueEstimate(Estimate $estimate): void
    {
        $url      = $this->generateAbsoluteUrl('view_estimate', ['id' => $estimate->getId()]);
        $customer = $estimate->getCustomer();
        $to       = '';
        $contactsOnEstimate = $estimate->getContacts();
        if (!$contactsOnEstimate) {
            $to = $customer->getEmail();
        } else {
            foreach ($contactsOnEstimate as $contact) {
                $contactEmail = $contact->getEmail();
                if ($contactEmail) {
                    $to .= $contactEmail . '; ';
                }
            }
        }

        $body = $this->renderView('Estimate/_estimateNewEmail.html.twig', ['username'   => $customer->getName(), 'jobDetails' => $estimate->getDetails(), 'url'        => $url]);
        $currentUser = $this->getCurrentUser();
        $email       = $this->getEmailManager()->sendEmail('Dawson Forklift Services estimate for your job.', $body, $currentUser->getEmail(), true);

        $estimate->setPending(0);
        $estimate->setIssued(1);

        $this->saveEstimate($estimate);
    }

    private function saveEstimate(Estimate $estimate): Estimate
    {
        $this->getEstimateManager()->saveEstimate($estimate);

        return $estimate;
    }
}
