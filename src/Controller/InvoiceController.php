<?php

namespace App\Controller;

use App\Controller\Aws\S3\Exception\S3Exception;
use App\Entity\AttachedFile;
use App\Entity\Invoice;
use App\Form\Type\InvoiceType;
use App\Time\TimeService;
use Aws\S3\S3Client;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class InvoiceController extends Controller
{
    public function list(): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');
        $invoices       = $this->getRepo(Invoice::class)->getInvoicesForListing();
        $invoiceManager = $this->getInvoiceManager();
        $priorities     = $invoiceManager->getInvoicePriorityForListingsInvoices($invoices);

        return $this->render('Invoice/list.html.twig', ['invoices'   => $invoices, 'priorities' => $priorities]);
    }

    public function new(Request $request, EntityManagerInterface $em, $hideExtraForm = null): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $response = new JsonResponse();
        $invoice  = new Invoice();
        $form     = $this->createForm(InvoiceType::class, $invoice);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $invoice = $form->getData();

            $this->getInvoiceManager()->saveInvoice($invoice);

            $this->setFlash('success', 'The invoice "' . $invoice->getId() . '" has been successfully created.');

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Invoice/_form.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('new_invoice'), 'invoice'   => $invoice, 'isEdit'    => false])]);

        return $response;
    }

    public function edit(Request $request, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $response = new JsonResponse();
        $invoice  = $this->getRepo(Invoice::class)->find($id);

        if (!$invoice) {
            $response->setData(['success'     => false, 'displayForm' => false, 'refresh'     => false]);

            $this->setFlash('error', 'The invoice with ID "' . $id . '" was not found.');

            return $response;
        }

        $form = $this->createForm(InvoiceType::class, $invoice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $invoice = $form->getData();

            $this->getInvoiceManager()->saveInvoice($invoice);

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The invoice "' . $invoice->getId() . '" has been successfully edited');

            return $response;
        }

        $response->setData(['success'     => true, 'displayForm' => true, 'isEdit'      => true, 'refresh'     => false, 'html'        => $this->renderView('Invoice/_form.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('edit_invoice', ['id' => $id]), 'invoice'   => $invoice, 'isEdit'    => true])]);

        return $response;
    }

    public function view(?string $id): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No invoice ID supplied.');
        }

        $invoice = $this->getRepo(Invoice::class)->find($id);

        if (!$invoice) {
            throw $this->createNotFoundException('No invoice with ID "' . $id . '" was found in the database.');
        }

        $versions = $this->getVersionRepo()->getLogEntries($invoice);

        return $this->render('Invoice/view.html.twig', ['invoice'  => $invoice, 'versions' => $versions]);
    }

    public function delete($id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $invoice = $this->getRepo(Invoice::class)->find($id);

        if (!$invoice) {
            throw $this->createNotFoundException('The invoice with ID"' . $id . '" was not found.');
        }

        $this->getInvoiceManager()->saveInvoice($invoice);
        $this->setFlash('success', 'Invoice was successfully deleted');

        return $this->redirect($this->generateUrl('list_invoices'));
    }

    public function getParticulars($id = null): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No invoice ID supplied');
        }

        $invoice = $this->getRepo(Invoice::class)->find($id);

        if (!$invoice) {
            throw $this->createNotFoundException('Invoice  with ID "' . $id . '" not found.');
        }

        return $this->render('Invoice/particulars.html.twig', ['invoice' => $invoice]);
    }

    public function emailInvoiceToCustomer(TimeService $timeService, S3Client $s3, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No invoice ID supplied');
        }

        $invoice = $this->getRepo(Invoice::class)->find($id);

        if (!$invoice) {
            throw $this->createNotFoundException('Invoice  with ID "' . $id . '" not found.');
        }

        $fileId = (int) $invoice->getAttachedFile();

        $file   = $this->getRepo(AttachedFile::class)->find($fileId);

        if (!$file) {
            throw $this->createNotFoundException('No invoice file for Invoice with ID "' . $id . '" was found in the database.');
        }

        $tmpFile = $this->getFileFromS3($file, $s3);

        $invoice->setIsSent(true);
        $now         = $timeService->getNewDateTime();
        $invoice->setSentAt($now);

        $this->getInvoiceManager()->saveInvoice($invoice);

        $token         = $file->getUuid();
        $customerEmail = $invoice->getJob()->getCustomer()->getEmail();
        $previewUrl    = $this->generateAbsoluteUrl('preview_invoice', ['id' => $fileId, 'token' => $token]);
        $downloadUrl   = $this->generateAbsoluteUrl('download_invoice', ['id' => $fileId, 'token' => $token]);

        $body = $this->renderView('Invoice/emailInvoice.html.twig', ['invoice'     => $invoice, 'previewUrl'  => $previewUrl, 'downloadUrl' => $downloadUrl]);

        $currentUser = $this->getCurrentUser();
        $email       = $this->getEmailManager()->sendEmail('Dawson Forklift Services invoice for completed work.', $body, $currentUser->getEmail(), true);

        $response = new JsonResponse();
        $response->setData(['success' => true]);

        return $response;
    }

    public function previewInvoice(EntityManagerInterface $em, TimeService $timeService, $id = null, $token = null): Response
    {
        if ($id === null) {
            throw $this->createNotFoundException('No file ID supplied.');
        }

        $file = $this->getRepo(AttachedFile::class)->find($id);

        if (!$file) {
            throw $this->createNotFoundException('No file with ID "' . $id . '" was found in the database.');
        }

        $currentUser = $this->getCurrentUser();
        $storedToken = $file->getUuid();
        $tokenMatch  = $storedToken === $token;

        if (!$this->isGranted('ROLE_SERVICE_ADMIN', $currentUser) && !$tokenMatch) {
            throw new AccessDeniedException('You do not have access to thie file');
        }

        $name       = $file->getName();
        $pdfContent = $this->getFileFromS3($file);

        if (!$pdfContent) {
            throw $this->createNotFoundException('The invoice file does not exist');
        }

        if ($this->isGranted('ROLE_CUSTOMER', $currentUser)) {
            $now         = $timeService->getNewDateTime();

            $file->setCustomerViewedAt($now);

            $em->persist($file);
            $em->flush();
        }

        $fileContents = (string) $pdfContent['Body'];

        $response = new Response($fileContents);
        $response->headers->set('Content-Type', 'application/pdf');
        $response->headers->set('Content-Disposition', "inline; filename*=UTF-8''" . urlencode((string) $name));
        $response->headers->set('Content-Length', strlen($fileContents));

        return $response;
    }

    public function downloadInvoice(EntityManagerInterface $em, TimeService $timeService, S3Client $s3, $id = null, $token = null): Response
    {
        if ($id === null) {
            throw $this->createNotFoundException('No file ID supplied.');
        }

        $file = $this->getRepo(AttachedFile::class)->find($id);

        if (!$file) {
            throw $this->createNotFoundException('No file with ID "' . $id . '" was found in the database.');
        }

        $currentUser = $this->getCurrentUser();
        $storedToken = $file->getUuid();
        $tokenMatch  = $storedToken === $token;

        if (!$this->isGranted('ROLE_SERVICE_ADMIN', $currentUser) && !$tokenMatch) {
            throw new AccessDeniedException('You do not have access to thie file');
        }

        $name       = $file->getName();
        $pdfContent = $this->getFileFromS3($file, $s3);

        if (!$pdfContent) {
            throw $this->createNotFoundException('The invoice file does not exist');
        }

        if ($this->isGranted('ROLE_CUSTOMER', $currentUser)) {
            $now         = $timeService->getNewDateTime();

            $file->setCustomerDownloadedAt($now);

            $em->persist($file);
            $em->flush();
        }

        $fileContents = (string) $pdfContent['Body'];

        $response = new Response($fileContents);
        $response->headers->set('Content-Type', 'application/pdf');
        $response->headers->set('Content-Disposition', "attachement; filename*=UTF-8''" . urlencode((string) $name));
        $response->headers->set('Content-Length', strlen($fileContents));

        return $response;
    }

    public function setAsQueried(?string $id): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No invoice ID supplied.');
        }

        $invoice = $this->getRepo(Invoice::class)->find($id);

        if (!$invoice) {
            throw $this->createNotFoundException('No invoice with ID "' . $id . '" was found in the database.');
        }

        $invoice->setIsQueried(1);
        $invoiceManager = $this->getInvoiceManager();
        $invoiceManager->saveInvoice($invoice);

        $response = new JsonResponse();
        $response->setData(['success' => true]);

        $this->setFlash('success', 'The invoice "' . $invoice->getId() . '" has been successfully updated.');

        return $response;
    }

    public function setAsPaid(?string $id, TimeService $timeService): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No invoice ID supplied.');
        }

        $invoice = $this->getRepo(Invoice::class)->find($id);

        if (!$invoice) {
            throw $this->createNotFoundException('No invoice with ID "' . $id . '" was found in the database.');
        }

        $now         = $timeService->getNewDateTime();

        $invoice->setIsPaid(1);
        $invoice->setPaidAt($now);
        $invoiceManager = $this->getInvoiceManager();
        $invoiceManager->saveInvoice($invoice);

        $response = new JsonResponse();
        $response->setData(['success' => true]);

        $this->setFlash('success', 'The invoice "' . $invoice->getId() . '" has been successfully updated.');

        return $response;
    }

    public function setAsSent(?string $id, TimeService $timeService): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No invoice ID supplied.');
        }

        $invoice = $this->getRepo(Invoice::class)->find($id);

        if (!$invoice) {
            throw $this->createNotFoundException('No invoice with ID "' . $id . '" was found in the database.');
        }

        $now         = $timeService->getNewDateTime();

        $invoice->setIsSent(1);
        $invoice->setSentAt($now);

        $this->getJobManager()->archivedJob($invoice, $now);

        $invoiceManager = $this->getInvoiceManager();
        $invoiceManager->saveInvoice($invoice);

        $response = new JsonResponse();
        $response->setData(['success' => true]);

        $this->setFlash('success', 'The invoice "' . $invoice->getId() . '" has been successfully updated.');

        return $response;
    }

    public function setAsRequestedOrder(?string $id): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No invoice ID supplied.');
        }

        $invoice = $this->getRepo(Invoice::class)->find($id);

        if (!$invoice) {
            throw $this->createNotFoundException('No invoice with ID "' . $id . '" was found in the database.');
        }

        $invoice->setIsRequestedOrder(1);

        $invoiceManager = $this->getInvoiceManager();
        $invoiceManager->saveInvoice($invoice);

        $response = new JsonResponse();
        $response->setData(['success' => true]);

        $this->setFlash('success', 'The invoice "' . $invoice->getId() . '" has been successfully updated.');

        return $response;
    }

    public function setAsArchive(?string $id): JsonResponse
    {
        if ($id === null) {
            throw $this->createNotFoundException('No invoice ID supplied.');
        }

        $invoice = $this->getRepo(Invoice::class)->find($id);

        if (!$invoice) {
            throw $this->createNotFoundException('No invoice with ID "' . $id . '" was found in the database.');
        }

        $timeService = $this->container->get('v42.time_service');
        $now         = $timeService->getNewDateTime();

        $this->getJobManager()->archivedJob($invoice, $now);

        $response = new JsonResponse();
        $response->setData(['success' => true]);

        $this->setFlash('success', 'The invoice "' . $invoice->getId() . '" has been successfully updated.');

        return $response;
    }

    private function getFileFromS3($file, S3Client $s3)
    {
        $name       = $file->getName();
        $path       = $file->getPath();
        $filename   = $path . '/' . $name;
        $bucket     = $this->container->getParameter('bucket');

        try {
            $client = $s3;
            $result = $client->getObject(['Bucket' => $bucket, 'Key'    => $filename]);
        } catch (S3Exception) {
            return false;
        }

        return $result;
    }
}
