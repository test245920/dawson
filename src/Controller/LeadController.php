<?php

namespace App\Controller;

use App\Entity\BillingAddress;
use App\Entity\Contact;
use App\Entity\Customer;
use App\Entity\CustomerNumber;
use App\Entity\Lead;
use App\Entity\Note;
use App\Entity\Reminder;
use App\Entity\Source;
use App\Entity\User;
use App\Form\Type\BillingAddressType;
use App\Form\Type\ContactType;
use App\Form\Type\LeadType;
use App\Form\Type\NoteType;
use App\Form\Type\ReminderType;
use App\Form\Type\UnsuccessfulLeadType;
use App\Time\TimeService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class LeadController extends Controller
{
    public function list(TimeService $timeService, $id = null): Response
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        $currentUser = $this->getCurrentUser();
        $superAdmin  = $this->isGranted('ROLE_SUPER_ADMIN');
        $leadOwners  = null;
        $start       = $timeService->getNewDateTime('first day of this month midnight');
        $end         = $timeService->getNewDateTime('first day of next month midnight');
        $end->modify('-1 second');

        if (!$superAdmin) {
            $leads                = $this->getRepo(Lead::class)->getLeadsForListing($currentUser->getId());
            $leadsIn              = $this->getRepo(Lead::class)->getLeadsInBetween($start, $end, $currentUser->getId());
            $leadsInWithNoContact = $this->getRepo(Lead::class)->getLeadsInWhichChangedFromNoContact($start, $end, $currentUser->getId());
        } elseif ($id) {
            $leads                = $this->getRepo(Lead::class)->getLeadsForListing($id);
            $leadOwners           = $this->getRepo(User::class)->getUsersWithLeads();
            $leadsIn              = $this->getRepo(Lead::class)->getLeadsInBetween($start, $end, $id);
            $leadsInWithNoContact = $this->getRepo(Lead::class)->getLeadsInWhichChangedFromNoContact($start, $end, $id);
        } else {
            $leads                = $this->getRepo(Lead::class)->getLeadsForListing();
            $leadOwners           = $this->getRepo(User::class)->getUsersWithLeads();
            $leadsIn              = $this->getRepo(Lead::class)->getLeadsInBetween($start, $end);
            $leadsInWithNoContact = $this->getRepo(Lead::class)->getLeadsInWhichChangedFromNoContact($start, $end);
        }

        $leadManager          = $this->getLeadManager();
        $priorities           = $leadManager->getLeadPriorityForListingsLeads($leads);
        $leadsIn              = sizeof($leadsIn);
        $leadsInWithNoContact = sizeof($leadsInWithNoContact);

        if ($leadsIn > 0) {
            $leadsPercentage = $leadsInWithNoContact / $leadsIn * 100;
            $leadsPercentage = number_format($leadsPercentage, 2);
        } else {
            $leadsPercentage = 0;
        }

        return $this->render('Lead/list.html.twig', ['leads'                => $leads, 'leadOwners'           => $leadOwners, 'priorities'           => $priorities, 'leadsIn'              => $leadsIn, 'leadsInWithNoContact' => $leadsInWithNoContact, 'leadsPercentage'      => $leadsPercentage, 'userId'               => $id]);
    }

    public function view($id = null): Response
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No Lead ID was supplied.');
        }

        $lead = $this->getRepo(Lead::class)->find($id);

        if (!$lead) {
            throw $this->createNotFoundException('Lead with ID "' . $id . '" not found.');
        }

        if ($lead->getUser() != $this->getCurrentUser() && !$this->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException('You do not own this lead.');
        }

        $versions = $this->getVersionRepo()->getLogEntries($lead);

        return $this->render('Lead/view.html.twig', ['lead'     => $lead, 'versions' => $versions]);
    }

    public function new(Request $request, EntityManagerInterface $em): JsonResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        $response    = new JsonResponse();
        $lead        = new Lead();
        $superAdmin  = $this->isGranted('ROLE_SUPER_ADMIN');
        $currentUser = $this->getCurrentUser();

        if ($superAdmin) {
            $lead->setUser($this->getCurrentUser());
        }

        $form     = $this->createForm(LeadType::class, $lead, [
            'securityContext' => $this->get('security.token_storage'),
            'authorizationChecker' => $this->get('security.authorization_checker'),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $lead      = $form->getData();
            $newSource = $form['newSource']->getData();

            if ($newSource['name'] !== null) {
                $sourceAlreadyExists = $this->getRepo(Source::class)->findOneByName($newSource['name']);

                if ($sourceAlreadyExists) {
                    $sourceAlreadyExists->addLead($lead);
                    $em->persist($sourceAlreadyExists);
                } else {
                    $source = new Source();
                    $source->setName($newSource['name']);
                    $source->addLead($lead);
                    $em->persist($source);
                }
            }

            if (!$lead->getCustomer()) {
                $customer    = new Customer();
                $newCustName = $form['newCustomer']->getData();

                $customerAlreadyExists = $this->getRepo(Customer::class)->findByIncludingSoftDeleted(['name' => $newCustName]);
                if ($customerAlreadyExists) {
                    $error = new FormError('A customer with the name "' . $newCustName . '" already exists in the database.');
                    $form->addError($error);

                    $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Lead/_form.html.twig', ['form'                  => $form->createView(), 'formRoute'             => $this->generateUrl('new_lead'), 'isEdit'                => false, 'customerAlreadyExists' => true])]);

                    return $response;
                }

                $customer->setName($newCustName);
                $customer->setIsActive(0);

                $em->persist($customer);
                $customer->setActiveAt(null);

                $em->flush();
                $lead->setCustomer($customer);
            }

            $leadAssignedToOtherUser = $lead->getUser() != $currentUser;

            $manager = $this->getLeadManager();
            $manager->saveLead($lead);

            $newContacts = $form['newContacts']->getData();
            foreach ($newContacts as $newContact) {
                $contact = new Contact();
                $contact->setName($newContact->getName());
                $contact->setTelephone($newContact->getTelephone());
                $contact->setEmail($newContact->getEmail());
                $contact->setJobTitle($newContact->getJobTitle());
                $contact->setMobile($newContact->getMobile());
                $contact->setCustomer($lead->getCustomer());

                $em->persist($contact);
                $em->flush();

                $lead->addContact($contact);
                $manager->saveLead($lead);
            }

            if ($leadAssignedToOtherUser) {
                $url          = $this->generateAbsoluteUrl('view_lead', ['id' => $lead->getId()]);
                $emailAddress = $lead->getUser()->getEmail();

                $body = $this->renderView('Lead/_assignedLeadEmail.html.twig', ['assigner' => $currentUser->getUsername(), 'user'     => $lead->getUser()->getUsername(), 'lead'     => $lead, 'url'      => $url]);

                $email = $this->getEmailManager()->sendEmail('You have been assigned to a new Lead.', $body, $emailAddress, true);

                $reminders = $this->getRepo(Reminder::class)->getRemindersForLead($lead->getId());

                foreach ($reminders as $reminder) {
                    $reminder->setUser($lead->getUser());
                    $em->persist($reminder);
                    $em->flush();
                }
            }

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The Lead "' . $lead->getId() . '" was successfully created.');

            return $response;
        }

        $response->setData(['success' => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Lead/_form.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('new_lead'), 'isEdit'    => false])]);

        return $response;
    }

    public function edit(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No Lead ID was supplied.');
        }

        $lead = $this->getRepo(Lead::class)->find($id);

        if (!$lead) {
            throw $this->createNotFoundException('Lead with ID "' . $id . '" not found.');
        }

        if ($lead->getUser() != $this->getCurrentUser() && !$this->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException('You do not own this lead.');
        }

        $response              = new JsonResponse();
        $currentlyAssignedUser = $lead->getUser();
        $currentUser           = $this->getCurrentUser();

        $form     = $this->createForm(LeadType::class, $lead, [
            'securityContext' => $this->get('security.token_storage'),
            'authorizationChecker' => $this->get('security.authorization_checker'),
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $lead      = $form->getData();
            $newSource = $form['newSource']->getData();

            if ($newSource['name'] !== null) {
                $lead->setSource(null);
                $source = new Source();
                $source->setName($newSource['name']);
                $source->addLead($lead);
                $em->persist($source);
            }

            $manager = $this->getLeadManager();
            $manager->saveLead($lead);

            $newContacts = $form['newContacts']->getData();
            foreach ($newContacts as $newContact) {
                $contact = new Contact();
                $contact->setName($newContact->getName());
                $contact->setTelephone($newContact->getTelephone());
                $contact->setEmail($newContact->getEmail());
                $contact->setJobTitle($newContact->getJobTitle());
                $contact->setMobile($newContact->getMobile());
                $contact->setCustomer($lead->getCustomer());

                $em->persist($contact);
                $em->flush();

                $lead->addContact($contact);
                $manager->saveLead($lead);
            }

            $leadOwner               = $lead->getUser();
            $leadAssignedToOtherUser = $leadOwner != $currentlyAssignedUser;

            if ($leadAssignedToOtherUser) {
                $url          = $this->generateAbsoluteUrl('view_lead', ['id' => $lead->getId()]);
                $emailAddress = $leadOwner->getEmail();

                $body = $this->renderView('Lead/_assignedLeadEmail.html.twig', ['assigner' => $currentUser->getUsername(), 'user'     => $leadOwner->getUsername(), 'lead'     => $lead, 'url'      => $url]);

                foreach ($lead->getReminders() as $reminder) {
                    $reminder->setUser($leadOwner);
                    $em->persist($reminder);
                }
                $em->flush();

                $email = $this->getEmailManager()->sendEmail('You have been assigned to a new Lead.', $body, $emailAddress, true);
            }

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The Lead "' . $lead->getId() . '" was successfully edited.');

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Lead/_form.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('edit_lead', ['id' => $id]), 'isEdit'    => true])]);

        return $response;
    }

    public function delete(EntityManagerInterface $em, $id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SUPER_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No Lead ID was supplied.');
        }

        $lead = $this->getRepo(Lead::class)->find($id);

        if (!$lead) {
            throw $this->createNotFoundException('Lead with ID "' . $id . '" not found.');
        }

        $em->remove($lead);
        $em->flush();

        $this->setFlash('success', 'The Lead "' . $lead->getId() . '" was successfully deleted.');

        return $this->redirect($this->generateUrl('list_leads'));
    }

    public function newNote(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No lead ID was supplied');
        }

        $lead = $this->getRepo(Lead::class)->find($id);

        if (!$lead) {
            throw $this->createNotFoundException('The Lead with ID "' . $id . '" was not found.');
        }

        if ($lead->getUser() != $this->getCurrentUser() && !$this->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException('You do not own this lead.');
        }

        $response = new JsonResponse();
        $note     = new Note();
        $note->setLead($lead);
        $form = $this->createForm(NoteType::class, $note);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $note = $form->getData();

            $hr  = date('H');
            $min = date('i');
            $sec = date('s');
            $date = $note->getDate();
            $date->setTime($hr, $min, $sec);
            $note->setDate($date);

            $em->persist($note);
            $em->flush();

            $this->getLeadManager()->saveLead($lead);

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The note "' . $note->getId() . '" has been successfully created for lead "' . $lead->getDescription() . '".');

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'isEdit'      => false, 'html'        => $this->renderView('Lead/_newNoteForm.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('new_note_for_lead', ['id' => $id]), 'lead'      => $lead, 'note'      => $note, 'isEdit'    => false])]);

        return $response;
    }

    public function newContact(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No lead ID was supplied');
        }

        $lead     = $this->getRepo(Lead::class)->find($id);
        $customer = $lead->getCustomer();

        if (!$lead) {
            throw $this->createNotFoundException('The Lead with ID "' . $id . '" was not found.');
        }

        if ($lead->getUser() != $this->getCurrentUser() && !$this->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException('You do not own this lead.');
        }

        $response = new JsonResponse();

        $contact  = new Contact();
        $contact->setCustomer($customer);

        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contact = $form->getData();

            $em->persist($contact);
            $em->flush();

            $lead->addContact($contact);
            $this->getLeadManager()->saveLead($lead);

            $response->setData(['success'     => true, 'displayForm' => false, 'message'     => 'The contact "' . $contact->getName() . '" has been successfully created for lead "' . $lead->getDescription() . '".', 'refresh'     => true]);

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'message'     => null, 'refresh'     => false, 'isEdit'      => false, 'html'        => $this->renderView('Lead/_newContactForm.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('new_contact_for_lead', ['id' => $id]), 'lead'      => $lead, 'contact'   => $contact, 'isEdit'    => false])]);

        return $response;
    }

    public function newReminder(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No lead ID was supplied');
        }

        $lead = $this->getRepo(Lead::class)->find($id);

        if (!$lead) {
            throw $this->createNotFoundException('The Lead with ID "' . $id . '" was not found.');
        }

        if ($lead->getUser() != $this->getCurrentUser() && !$this->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException('You do not own this lead.');
        }

        $response    = new JsonResponse();
        $currentUser = $this->getCurrentUser();

        $reminder = new Reminder();
        $reminder->setLead($lead);
        $reminder->setUser($currentUser);

        $form = $this->createForm(
            ReminderType::class,
            $reminder,
            [
                'securityContext' => $this->get('security.token_storage'),
                'authorizationChecker' => $this->get('security.authorization_checker'),
            ],
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $reminder = $form->getData();

            $reminderOnwer               = $reminder->getUser();
            $reminderAssignedToOtherUser = $reminderOnwer != $currentUser;

            $em->persist($reminder);
            $em->flush();

            if ($reminderAssignedToOtherUser) {
                $url          = $this->generateAbsoluteUrl('view_reminder', ['id' => $reminder->getId()]);
                $emailAddress = $reminderOnwer->getEmail();

                $body = $this->renderView('Lead/_assignedReminderEmail.html.twig', ['assigner' => $currentUser->getUsername(), 'assignee' => $reminderOnwer->getName(), 'reminder' => $reminder, 'lead'     => $lead, 'url'      => $url]);

                $email = $this->getEmailManager()->sendEmail('You have been assigned a reminder for a Lead.', $body, $emailAddress, true);
            }

            $this->getLeadManager()->saveLead($lead);

            $response->setData(['success'     => true, 'displayForm' => false, 'message'     => 'The reminder "' . $reminder->getType() . '" has been successfully created for lead "' . $lead->getDescription() . '".', 'refresh'     => true]);

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'message'     => null, 'refresh'     => false, 'isEdit'      => false, 'html'        => $this->renderView('Lead/_newReminderForm.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('new_reminder_for_lead', ['id' => $id]), 'lead'      => $lead, 'reminder'  => $reminder, 'isEdit'    => false, 'userId'    => $currentUser->getId()])]);

        return $response;
    }

    public function listFutureTargets(TimeService $timeService, EntityManagerInterface $em, $id = null): Response
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        $currentUser = $this->getCurrentUser();
        $superAdmin  = $this->isGranted('ROLE_SUPER_ADMIN');
        $leadOwners  = null;
        $now         = $timeService->getNewDateTime();

        if (!$superAdmin) {
            $reminders = $this->getRepo(Reminder::class)->getFutureTargets($currentUser->getId(), $now);
        } elseif ($id) {
            $leadOwners = $this->getRepo(User::class)->getUsersWithFutureTargets();
            $reminders  = $this->getRepo(Reminder::class)->getFutureTargets($id, $now);
        } else {
            $leadOwners = $this->getRepo(User::class)->getUsersWithFutureTargets();
            $reminders  = $this->getRepo(Reminder::class)->getFutureTargets(null, $now);
        }

        $thisMonth     = $timeService->getNewDateTime('last day of this month', null);
        $nextSixMonths = $timeService->getNewDateTime('+6 months', null);
        $thisYear      = $timeService->getNewDateTime('+1 year', null);

        $sortedFutureTargets = ['thisMonth'     => [], 'nextSixMonths' => [], 'thisYear'      => [], 'longRange'     => [], 'noReminder'    => []];

        $leads = [];

        foreach ($reminders as $reminder) {
            $reminderDate = $reminder['date'];
            if ($reminderDate <= $thisMonth) {
                $sortedFutureTargets['thisMonth'][] = $reminder;
            } elseif ($reminderDate <= $nextSixMonths) {
                $sortedFutureTargets['nextSixMonths'][] = $reminder;
            } elseif ($reminderDate <= $thisYear) {
                $sortedFutureTargets['thisYear'][] = $reminder;
            } else {
                $sortedFutureTargets['longRange'][] = $reminder;
            }

            $leads[$reminder['lead']['id']] = true;
        }

        $leadIdsAlreadyDisplayed = array_keys($leads);

        if ($id) {
            $reminderlessFutureTargets         = $this->getRepo(Lead::class)->getLeadsNotInListWithoutReminders($id, $leadIdsAlreadyDisplayed);
            $futureTargetsWithElapsedReminders = $this->getRepo(Lead::class)->getLeadsNotInListWithReminders($id, $leadIdsAlreadyDisplayed);
        } else {
            $reminderlessFutureTargets         = $this->getRepo(Lead::class)->getLeadsNotInListWithoutReminders(null, $leadIdsAlreadyDisplayed);
            $futureTargetsWithElapsedReminders = $this->getRepo(Lead::class)->getLeadsNotInListWithReminders(null, $leadIdsAlreadyDisplayed);
        }

        foreach ($reminderlessFutureTargets as $target) {
            $sortedFutureTargets['noReminder'][] = $target;
        }

        foreach ($futureTargetsWithElapsedReminders as $futureTargetsWithElapsedReminder) {
            $lead = $this->getRepo(Lead::class)->find($futureTargetsWithElapsedReminder['id']);
            $lead->setFutureTarget(false);
            $lead->setPrimaryTarget(true);
            $lead->setCompletionStage(3);
            $lead->setStatus('Action required');

            $em->persist($lead);
            $em->flush();
        }

        return $this->render('FutureTarget/list.html.twig', ['leadOwners'    => $leadOwners, 'futureTargets' => $sortedFutureTargets, 'userId'        => $id]);
    }

    public function updateStatus($id = null, $status = null): JsonResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No lead ID was supplied');
        }

        if ($status === null) {
            throw $this->createNotFoundException('No lead Status was supplied');
        }

        $lead = $this->getRepo(Lead::class)->find($id);

        if (!$lead) {
            throw $this->createNotFoundException('No lead with ID "' . $id . '" was found.');
        }

        if ($lead->getUser() != $this->getCurrentUser() && !$this->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException('You do not own this lead.');
        }

        switch ($status) {
            case 1:
                $lead->setCompletionStage(1);
                $lead->setStatus('No contact');

                break;
            case 2:
                $lead->setCompletionStage(2);
                $lead->setStatus('Contact made');

                break;
            case 3:
                $lead->setCompletionStage(3);
                $lead->setStatus('Action required');

                break;
            case 5:
                $lead->setCompletionStage(5);
                $lead->setStatus('Quoting');

                break;

            default:
                throw new Exception('Status "' . $status . '" is not valid.');
                break;
        }

        $manager = $this->getLeadManager();
        $manager->saveLead($lead);

        $response = new JsonResponse();

        return $response->setData(['success' => true, 'refresh' => true]);
    }

    public function convertToCustomer(TimeService $timeService, EntityManagerInterface $em, $id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if (!$id) {
            throw $this->createNotFoundException('No Lead ID supplied.');
        }

        $lead = $this->getRepo(Lead::class)->find($id);

        if (!$lead) {
            throw $this->createNotFoundException('No Lead with ID "' . $id . '" was found.');
        }

        if ($lead->getUser() != $this->getCurrentUser() && !$this->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException('You do not own this lead.');
        }

        $customer                = $lead->getCustomer();
        $customerIsAlreadyActive = $customer->getIsActive();

        if ($customerIsAlreadyActive) {
            $lead->setCompletionStage(null);

            $em->persist($lead);
            $em->flush();

            return $this->redirect($this->generateUrl('view_lead', ['id' => $id]));
        }

        $customer->setIsActive(true);
        $now         = $timeService->getNewDateTime();
        $customer->setActiveAt($now);

        $name   = $customer->getName();
        $letter = strtoupper((string) $name[0]);

        $letterExistsForCustomer = $this->getRepo(CustomerNumber::class)->checkForExistingLetter($letter, $customer->getId());

        if (!$letterExistsForCustomer) {
            $customerNumber = new CustomerNumber($customer);
            $customerNumber->setLetter($letter);

            $number = $this->getRepo(CustomerNumber::class)->getNextNumberForLetter($letter);

            $customerNumber->setNumber($number);
            $customerNumber->setCustomerNumber($letter . $number);
            $customerNumber->setAssignedAt($now);

            $customer->getCustomerNumbers()->add($customerNumber);
        } else {
            $letterExistsForCustomer->setAssignedAt($now);
        }

        $em->persist($customer);

        $lead->setCompletionStage(null);

        $manager = $this->getLeadManager();
        $manager->saveLead($lead);

        return $this->redirect($this->generateUrl('view_customer', ['id' => $customer->getId()]));
    }

    public function listCasualHire($id = null): Response
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        $currentUser = $this->getCurrentUser();
        $superAdmin  = $this->isGranted('ROLE_SUPER_ADMIN');
        $leadOwners  = null;

        if (!$superAdmin) {
            $leads = $this->getRepo(Lead::class)->getCasualHireLeadsForListing($currentUser->getId());
        } elseif ($id) {
            $leads      = $this->getRepo(Lead::class)->getCasualHireLeadsForListing($id);
            $leadOwners = $this->getRepo(User::class)->getUsersWithCasualHireLeads();
        } else {
            $leads      = $this->getRepo(Lead::class)->getCasualHireLeadsForListing(null);
            $leadOwners = $this->getRepo(User::class)->getUsersWithCasualHireLeads();
        }

        $leadManager = $this->getLeadManager();
        $priorities  = $leadManager->getLeadPriorityForListingsLeads($leads);

        return $this->render('CasualHire/list.html.twig', ['leads'       => $leads, 'leadOwners'  => $leadOwners, 'priorities'  => $priorities, 'userId'      => $id, 'currentUser' => $currentUser]);
    }

    public function listPrimaryTargets(TimeService $timeService, $id = null): Response
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        $currentUser = $this->getCurrentUser();
        $superAdmin  = $this->isGranted('ROLE_SUPER_ADMIN');
        $leadOwners  = null;
        $start       = $timeService->getNewDateTime('first day of this month midnight');
        $end         = $timeService->getNewDateTime('first day of next month midnight');
        $end->modify('-1 second');

        if (!$superAdmin) {
            $leads                = $this->getRepo(Lead::class)->getPrimaryTargets($currentUser->getId());
            $leadsIn              = $this->getRepo(Lead::class)->getLeadsInBetween($start, $end, $currentUser->getId(), 'primary');
            $leadsInWithNoContact = $this->getRepo(Lead::class)->getLeadsInWhichChangedFromNoContact($start, $end, $currentUser->getId(), 'primary');
        } elseif ($id) {
            $leads                = $this->getRepo(Lead::class)->getPrimaryTargets($id);
            $leadOwners           = $this->getRepo(User::class)->getUsersWithPrimaryTargets();
            $leadsIn              = $this->getRepo(Lead::class)->getLeadsInBetween($start, $end, $id, 'primary');
            $leadsInWithNoContact = $this->getRepo(Lead::class)->getLeadsInWhichChangedFromNoContact($start, $end, $id, 'primary');
        } else {
            $leads                = $this->getRepo(Lead::class)->getPrimaryTargets();
            $leadOwners           = $this->getRepo(User::class)->getUsersWithPrimaryTargets();
            $leadsIn              = $this->getRepo(Lead::class)->getLeadsInBetween($start, $end, 'primary');
            $leadsInWithNoContact = $this->getRepo(Lead::class)->getLeadsInWhichChangedFromNoContact($start, $end, 'primary');
        }

        $leadManager          = $this->getLeadManager();
        $priorities           = $leadManager->getLeadPriorityForListingsLeads($leads);
        $leadsIn              = sizeof($leadsIn);
        $leadsInWithNoContact = sizeof($leadsInWithNoContact);

        if ($leadsIn > 0) {
            $leadsPercentage = $leadsInWithNoContact / $leadsIn * 100;
            $leadsPercentage = number_format($leadsPercentage, 2);
        } else {
            $leadsPercentage = 0;
        }

        return $this->render('PrimaryTarget/list.html.twig', ['leads'                => $leads, 'leadOwners'           => $leadOwners, 'priorities'           => $priorities, 'leadsIn'              => $leadsIn, 'leadsInWithNoContact' => $leadsInWithNoContact, 'leadsPercentage'      => $leadsPercentage, 'userId'               => $id]);
    }

    public function newCasualHire(Request $request, EntityManagerInterface $em): JsonResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        $response = new JsonResponse();
        $lead     = new Lead();

        $lead->setType('Casual Hire');

        $form     = $this->createForm(LeadType::class, $lead, [
            'securityContext' => $this->get('security.token_storage'),
            'authorizationChecker' => $this->get('security.authorization_checker'),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $lead      = $form->getData();
            $newSource = $form['newSource']->getData();

            if ($newSource['name'] !== null) {
                $lead->setSource(null);
                $source = new Source();
                $source->setName($newSource['name']);
                $source->addLead($lead);
                $em->persist($source);
            }

            if (!$lead->getCustomer()) {
                $customer    = new Customer();
                $newCustName = $form['newCustomer']->getData();
                $customer->setName($newCustName);
                $customer->setIsActive(0);
                $em->persist($customer);
                $em->flush();
                $lead->setCustomer($customer);
            }

            $manager = $this->getLeadManager();
            $manager->saveLead($lead);

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The Lead "' . $lead->getId() . '" was successfully created.');

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Lead/_form.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('new_casual_hire_lead'), 'isEdit'    => false])]);

        return $response;
    }

    public function newPrimaryTarget(Request $request, EntityManagerInterface $em): JsonResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        $response    = new JsonResponse();
        $lead        = new Lead();
        $currentUser = $this->getCurrentUser();

        $lead->setPrimaryTarget(true);
        $lead->setUser($currentUser);

        $form     = $this->createForm(LeadType::class, $lead, [
            'securityContext' => $this->get('security.token_storage'),
            'authorizationChecker' => $this->get('security.authorization_checker'),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $lead      = $form->getData();
            $newSource = $form['newSource']->getData();

            if ($newSource['name'] !== null) {
                $lead->setSource(null);
                $source = new Source();
                $source->setName($newSource['name']);
                $source->addLead($lead);
                $em->persist($source);
            }

            if (!$lead->getCustomer()) {
                $customer    = new Customer();
                $newCustName = $form['newCustomer']->getData();
                $customer->setName($newCustName);
                $customer->setIsActive(0);
                $em->persist($customer);
                $em->flush();
                $lead->setCustomer($customer);
            }

            $leadAssignedToOtherUser = $lead->getUser() != $currentUser;

            $manager = $this->getLeadManager();
            $manager->saveLead($lead);

            $newContacts = $form['newContacts']->getData();
            foreach ($newContacts as $newContact) {
                $contact = new Contact();
                $contact->setName($newContact->getName());
                $contact->setTelephone($newContact->getTelephone());
                $contact->setEmail($newContact->getEmail());
                $contact->setJobTitle($newContact->getJobTitle());
                $contact->setMobile($newContact->getMobile());
                $contact->setCustomer($lead->getCustomer());

                $em->persist($contact);
                $em->flush();

                $lead->addContact($contact);
                $manager->saveLead($lead);
            }

            if ($leadAssignedToOtherUser) {
                $url          = $this->generateAbsoluteUrl('view_lead', ['id' => $lead->getId()]);
                $emailAddress = $lead->getUser()->getEmail();

                $body = $this->renderView('Lead/_assignedLeadEmail.html.twig', ['assigner' => $currentUser->getUsername(), 'user'     => $lead->getUser()->getUsername(), 'lead'     => $lead, 'url'      => $url]);

                $email = $this->getEmailManager()->sendEmail('You have been assigned to a new Lead.', $body, $emailAddress, true);

                $reminders = $this->getRepo(Reminder::class)->getRemindersForLead($lead->getId());

                foreach ($reminders as $reminder) {
                    $reminder->setUser($lead->getUser());
                    $em->persist($reminder);
                    $em->flush();
                }
            }

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The Lead "' . $lead->getId() . '" was successfully created.');

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Lead/_form.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('new_primary_target'), 'isEdit'    => false])]);

        return $response;
    }

    public function convertToUnsuccessful(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if (!$id) {
            throw $this->createNotFoundException('No Lead ID supplied.');
        }

        $lead = $this->getRepo(Lead::class)->find($id);

        if (!$lead) {
            throw $this->createNotFoundException('No Lead with ID "' . $id . '" was found.');
        }

        if ($lead->getUser() != $this->getCurrentUser() && !$this->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException('You do not own this lead.');
        }

        $response = new JsonResponse();
        $form     = $this->createForm(UnsuccessfulLeadType::class, $lead);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $lead = $form->getData();

            $lead->setUnsuccessful(true);
            $lead->setCompletionDate(new \DateTime());

            $manager = $this->getLeadManager();
            $manager->saveLead($lead);

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The Lead "' . $lead->getId() . '" was assigned unsuccessful status.');

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Lead/_unsuccessfulLeadForm.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('convert_unsuccessful_lead', ['id' => $id]), 'isEdit'    => false, 'lead'      => $lead])]);

        return $response;
    }

    public function listUnsuccessful($page = 1): Response
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        $params = ['unsuccessful' => true, 'archived' => false];

        if (!$this->isGranted('ROLE_SUPER_ADMIN')) {
            $params['user'] = $this->getCurrentUser();
        }

        $leads = $this->getRepo(Lead::class)->findBy($params);

        return $this->render('UnsuccessfulLeads/list.html.twig', ['leads' => $leads]);
    }

    public function updatePriority($id = null, $priority = null): JsonResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No lead ID was supplied');
        }

        if ($priority === null) {
            throw $this->createNotFoundException('No lead Priority was supplied');
        }

        $lead = $this->getRepo(Lead::class)->find($id);

        if (!$lead) {
            throw $this->createNotFoundException('No lead with ID "' . $id . '" was found.');
        }

        if ($lead->getUser() != $this->getCurrentUser() && !$this->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException('You do not own this lead.');
        }

        match ($priority) {
            1 => $lead->setPriority('Normal'),
            2 => $lead->setPriority('Important'),
            3 => $lead->setPriority('Urgent'),
            default => throw new Exception('Priority "' . $priority . '" is not valid.'),
        };

        $manager = $this->getLeadManager();
        $manager->saveLead($lead);

        $response = new JsonResponse();

        return $response->setData(['success' => true]);
    }

    public function updateType($id = null, $type = null): JsonResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No lead ID was supplied');
        }

        if ($type === null) {
            throw $this->createNotFoundException('No lead type was supplied');
        }

        $lead = $this->getRepo(Lead::class)->find($id);

        if (!$lead) {
            throw $this->createNotFoundException('No lead with ID "' . $id . '" was found.');
        }

        if ($lead->getUser() != $this->getCurrentUser() && !$this->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException('You do not own this lead.');
        }

        match ($type) {
            1 => $lead->setType('New Purchase'),
            2 => $lead->setType('New Hire'),
            3 => $lead->setType('Used Hire'),
            4 => $lead->setType('Casual Hire'),
            5 => $lead->setType('Not Yet Established'),
            6 => $lead->setType('Other'),
            7 => $lead->setType('Used Purchase'),
            8 => $lead->setType('Service Contract'),
            default => throw new \Exception('Type "' . $type . '" is not valid.'),
        };

        $manager = $this->getLeadManager();
        $manager->saveLead($lead);

        $response = new JsonResponse();

        return $response->setData(['success' => true]);
    }

    public function updateTargetType($id = null, $type = null): JsonResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No lead ID was supplied');
        }

        if ($type === null) {
            throw $this->createNotFoundException('No lead target type was supplied');
        }

        $lead = $this->getRepo(Lead::class)->find($id);

        if (!$lead) {
            throw $this->createNotFoundException('No lead with ID "' . $id . '" was found.');
        }

        if ($lead->getUser() != $this->getCurrentUser() && !$this->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException('You do not own this lead.');
        }

        switch ($type) {
            case 1:
                $lead->setPrimaryTarget(false);
                $lead->setFutureTarget(false);
                $lead->setIsServicePartsLead(false);

                break;
            case 2:
                $lead->setPrimaryTarget(false);
                $lead->setFutureTarget(true);
                $lead->setIsServicePartsLead(false);

                break;
            case 3:
                $lead->setPrimaryTarget(true);
                $lead->setFutureTarget(false);
                $lead->setIsServicePartsLead(false);

                break;
            case 4:
                $lead->setPrimaryTarget(false);
                $lead->setFutureTarget(false);
                $lead->setIsServicePartsLead(true);

                break;

            default:
                throw new \Exception('Target Type "' . $type . '" is not valid.');
                break;
        }

        $manager = $this->getLeadManager();
        $manager->saveLead($lead);

        $response = new JsonResponse();

        return $response->setData(['success' => true]);
    }

    public function updateValue($id = null, $value = null): JsonResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No lead ID was supplied');
        }

        if ($value === null) {
            throw $this->createNotFoundException('No lead value was supplied');
        }

        $lead = $this->getRepo(Lead::class)->find($id);

        if (!$lead) {
            throw $this->createNotFoundException('No lead with ID "' . $id . '" was found.');
        }

        if ($lead->getUser() != $this->getCurrentUser() && !$this->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException('You do not own this lead.');
        }

        $lead->setValue($value);

        $manager = $this->getLeadManager();
        $manager->saveLead($lead);

        $response = new JsonResponse();

        return $response->setData(['success' => true]);
    }

    public function updateAddress(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No lead ID was supplied');
        }

        $lead = $this->getRepo(Lead::class)->find($id);

        if (!$lead) {
            throw $this->createNotFoundException('No lead with ID "' . $id . '" was found.');
        }

        if ($lead->getUser() != $this->getCurrentUser() && !$this->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException('You do not own this lead.');
        }

        $response       = new JsonResponse();
        $customer       = $lead->getCustomer();
        $billingAddress = $customer->getBillingAddress() ?: new BillingAddress();

        $form = $this->createForm(BillingAddressType::class, $billingAddress);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $billingAddress = $form->getData();

            $billingAddress->setCustomer($customer);

            $em->persist($billingAddress);
            $em->persist($customer);
            $em->flush();

            $manager = $this->getLeadManager();
            $manager->saveLead($lead);

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The billing address for Lead "' . $lead->getDescription() . '" was successfully created.');

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Lead/_editAddressForm.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('update_lead_address', ['id' => $id]), 'isEdit'    => false])]);

        return $response;
    }

    public function listSuccessfulAwaitingDelivery(): Response
    {
        $this->securityCheck('ROLE_SALES_ADMIN');
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $leads = $this->getRepo(Lead::class)->getSuccessfulLeadsAwaitingDelivery();
        } else {
            $leads = $this->getRepo(Lead::class)->getSuccessfulLeadsAwaitingDelivery($this->getCurrentUser()->getId());
        }

        return $this->render('SuccessfulLeads/list.html.twig', ['leads' => $leads]);
    }

    public function deliveredToCustomer(TimeService $timeService, EntityManagerInterface $em, $leadId = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if (!$leadId) {
            throw $this->createNotFoundException('No lead Id supplied');
        }

        $lead = $this->getRepo(Lead::class)->find($leadId);

        if (!$lead) {
            throw $this->createNotFoundException('No lead with ID "' . $id . '" was found.');
        }

        if ($lead->getUser() != $this->getCurrentUser() && !$this->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException('You do not own this lead.');
        }

        $now         = $timeService->getNewDateTime();

        $lead->setIsDelivered(true);
        $lead->setCompletionDate($now);

        $em->persist($lead);
        $em->flush();

        $this->setFlash('success', 'The Lead "' . $lead->getId() . '" was delivered to customer.');

        return $this->redirect($this->generateUrl('view_lead', ['id' => $leadId]));
    }

    public function updateDeliveryDate(TimeService $timeService, $id = null, $date = null): JsonResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No lead ID was supplied');
        }

        if ($date === null) {
            throw $this->createNotFoundException('No date was supplied');
        }

        $dateTime    = $timeService->getNewDateTime($date);

        $lead = $this->getRepo(Lead::class)->find($id);

        if (!$lead) {
            throw $this->createNotFoundException('No lead with ID "' . $id . '" was found.');
        }

        if ($lead->getUser() != $this->getCurrentUser() && !$this->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException('You do not own this lead.');
        }

        $lead->setDeliveryDate($dateTime);

        $manager = $this->getLeadManager();
        $manager->saveLead($lead);

        $response = new JsonResponse();

        return $response->setData(['success' => true]);
    }

    public function updateContractRenewalDate(TimeService $timeService, $id = null, $date = null): JsonResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No lead ID was supplied');
        }

        if ($date === null) {
            throw $this->createNotFoundException('No date was supplied');
        }

        $dateTime    = $timeService->getNewDateTime($date);

        $lead = $this->getRepo(Lead::class)->find($id);

        if (!$lead) {
            throw $this->createNotFoundException('No lead with ID "' . $id . '" was found.');
        }

        if ($lead->getUser() != $this->getCurrentUser() && !$this->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException('You do not own this lead.');
        }

        $lead->setContractRenewalDate($dateTime);

        $manager = $this->getLeadManager();
        $manager->saveLead($lead);

        $response = new JsonResponse();

        return $response->setData(['success' => true]);
    }

    public function listServicePartsLeads(TimeService $timeService, $id = null): Response
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        $currentUser = $this->getCurrentUser();
        $superAdmin  = $this->isGranted('ROLE_SUPER_ADMIN');
        $leadOwners  = null;
        $start       = $timeService->getNewDateTime('first day of this month midnight');
        $end         = $timeService->getNewDateTime('first day of next month midnight');
        $end->modify('-1 second');

        if (!$superAdmin) {
            $leads                = $this->getRepo(Lead::class)->getServicePartsLeads($currentUser->getId());
            $leadsIn              = $this->getRepo(Lead::class)->getLeadsInBetween($start, $end, $currentUser->getId(), 'service/parts');
            $leadsInWithNoContact = $this->getRepo(Lead::class)->getLeadsInWhichChangedFromNoContact($start, $end, $currentUser->getId(), 'service/parts');
        } elseif ($id) {
            $leads                = $this->getRepo(Lead::class)->getServicePartsLeads($id);
            $leadOwners           = $this->getRepo(User::class)->getUsersWithServicePartsLeads();
            $leadsIn              = $this->getRepo(Lead::class)->getLeadsInBetween($start, $end, $id, 'service/parts');
            $leadsInWithNoContact = $this->getRepo(Lead::class)->getLeadsInWhichChangedFromNoContact($start, $end, $id, 'service/parts');
        } else {
            $leads                = $this->getRepo(Lead::class)->getServicePartsLeads();
            $leadOwners           = $this->getRepo(User::class)->getUsersWithServicePartsLeads();
            $leadsIn              = $this->getRepo(Lead::class)->getLeadsInBetween($start, $end, null, 'service/parts');
            $leadsInWithNoContact = $this->getRepo(Lead::class)->getLeadsInWhichChangedFromNoContact($start, $end, null, 'service/parts');
        }

        $leadManager          = $this->getLeadManager();
        $priorities           = $leadManager->getLeadPriorityForListingsLeads($leads);
        $leadsIn              = sizeof($leadsIn);
        $leadsInWithNoContact = sizeof($leadsInWithNoContact);

        if ($leadsIn > 0) {
            $leadsPercentage = $leadsInWithNoContact / $leadsIn * 100;
            $leadsPercentage = number_format($leadsPercentage, 2);
        } else {
            $leadsPercentage = 0;
        }

        return $this->render('ServiceParts/list.html.twig', ['leads'                => $leads, 'leadOwners'           => $leadOwners, 'priorities'           => $priorities, 'leadsIn'              => $leadsIn, 'leadsInWithNoContact' => $leadsInWithNoContact, 'leadsPercentage'      => $leadsPercentage, 'userId'               => $id]);
    }

    public function listVisitMe($page = 1): Response
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        $leads = $this->getRepo(Lead::class)->getVisitMeLeads();

        return $this->render('VisitMe/list.html.twig', ['leads' => $leads]);
    }

    public function setVisitMe($id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No lead ID was supplied');
        }

        $lead = $this->getRepo(Lead::class)->find($id);

        if (!$lead) {
            throw $this->createNotFoundException('No lead with ID "' . $id . '" was found.');
        }

        if ($lead->getUser() != $this->getCurrentUser() && !$this->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException('You do not own this lead.');
        }

        $lead->setVisitMe(true);

        $manager = $this->getLeadManager();
        $manager->saveLead($lead);

        $response = new JsonResponse();

        return $response->setData(['success' => true]);
    }

    public function removeVisitMe($id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No lead ID was supplied');
        }

        $lead = $this->getRepo(Lead::class)->find($id);

        if (!$lead) {
            throw $this->createNotFoundException('No lead with ID "' . $id . '" was found.');
        }

        if ($lead->getUser() != $this->getCurrentUser() && !$this->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException('You do not own this lead.');
        }

        $lead->setVisitMe(false);

        $manager = $this->getLeadManager();
        $manager->saveLead($lead);

        $response = new JsonResponse();

        return $response->setData(['success' => true]);
    }

    public function listArchived($page = 1): Response
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $leads = $this->getRepo(Lead::class)->getArchivedLeads();
        } else {
            $leads = $this->getRepo(Lead::class)->getArchivedLeads($this->getCurrentUser()->getId());
        }

        return $this->render('Archived/list.html.twig', ['leads' => $leads]);
    }

    public function archive(TimeService $timeService, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No lead ID was supplied');
        }

        $lead = $this->getRepo(Lead::class)->find($id);

        if (!$lead) {
            throw $this->createNotFoundException('No lead with ID "' . $id . '" was found.');
        }

        if ($lead->getUser() != $this->getCurrentUser() && !$this->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException('You do not own this lead.');
        }

        $now         = $timeService->getNewDateTime();

        $lead->setArchived(true);
        $lead->setArchivedAt($now);

        $manager = $this->getLeadManager();
        $manager->saveLead($lead);

        $response = new JsonResponse();

        return $response->setData(['success' => true]);
    }

    public function reinstate($id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No lead ID was supplied');
        }

        $lead = $this->getRepo(Lead::class)->find($id);

        if (!$lead) {
            throw $this->createNotFoundException('No lead with ID "' . $id . '" was found.');
        }

        if ($lead->getUser() != $this->getCurrentUser() && !$this->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException('You do not own this lead.');
        }

        $lead->setArchived(false);
        $lead->setArchivedAt(null);

        $manager = $this->getLeadManager();
        $manager->saveLead($lead);

        $response = new JsonResponse();

        return $response->setData(['success' => true]);
    }
}
