<?php

namespace App\Controller;

use App\Entity\Reminder;
use App\Form\Type\ReminderType;
use App\Time\TimeService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ReminderController extends Controller
{
    public function list(TimeService $timeService): Response
    {
        $this->securityCheck('ROLE_SALES_ADMIN');
        $currentUser = $this->getCurrentUser();
        $reminders   = $this->getRepo(Reminder::class)->getRemindersForCurrentUser($currentUser);
        $today       = $timeService->getNewDateTime('midnight', null);
        $thisWeek    = $timeService->getNewDateTime('next monday', null);
        $thisMonth   = $timeService->getNewDateTime('last day of this month', null);
        $sortedReminders = ['today'     => [], 'thisWeek'  => [], 'thisMonth' => [], 'longRange' => []];
        foreach ($reminders as $reminder) {
            $reminderDate = $reminder->getDate();
            if ($reminderDate <= $today) {
                $sortedReminders['today'][] = $reminder;
            } elseif ($reminderDate <= $thisWeek) {
                $sortedReminders['thisWeek'][] = $reminder;
            } elseif ($reminderDate <= $thisMonth) {
                $sortedReminders['thisMonth'][] = $reminder;
            } else {
                $sortedReminders['longRange'][] = $reminder;
            }
        }

        return $this->render('Reminder/list.html.twig', ['reminders' => $sortedReminders]);
    }

    public function view($id = null): Response
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No Reminder ID was supplied.');
        }

        $reminder = $this->getRepo(Reminder::class)->find($id);

        if (!$reminder) {
            throw $this->createNotFoundException('Reminder with ID "' . $id . '" not found.');
        }

        $versions = $this->getVersionRepo()->getLogEntries($reminder);

        return $this->render('Reminder/view.html.twig', ['reminder' => $reminder, 'versions' => $versions]);
    }

    public function new(Request $request, EntityManagerInterface $em): JsonResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        $response = new JsonResponse();
        $reminder = new Reminder();

        $currentUser = $this->getCurrentUser();
        $reminder->setUser($currentUser);

        $form = $this->createForm(
            ReminderType::class,
            $reminder,
            [
                'securityContext' => $this->get('security.token_storage'),
                'authorizationChecker' => $this->get('security.authorization_checker'),
            ],
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $reminder = $form->getData();

            $em->persist($reminder);
            $em->flush();

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The reminder "' . $reminder->getId() . '" was successfully created.');

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Reminder/_form.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('new_reminder'), 'isEdit'    => false, 'reminder'  => $reminder, 'userId'    => $currentUser->getId()])]);

        return $response;
    }

    public function edit(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No Reminder ID was supplied.');
        }

        $response    = new JsonResponse();
        $currentUser = $this->getCurrentUser();
        $reminder    = $this->getRepo(Reminder::class)->find($id);

        if (!$reminder) {
            throw $this->createNotFoundException('Reminder with ID "' . $id . '" not found.');
        }

        if ($reminder->getUser() != $currentUser) {
            throw $this->createAccessDeniedException('You do not own this reminder.');
        }

        $form = $this->createForm(
            ReminderType::class,
            $reminder,
            [
                'securityContext' => $this->get('security.token_storage'),
                'authorizationChecker' => $this->get('security.authorization_checker'),
            ],
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $reminder = $form->getData();

            $em->persist($reminder);
            $em->flush();

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The Reminder "' . $reminder->getId() . '" was successfully edited.');

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Reminder/_form.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('edit_reminder', ['id' => $id]), 'isEdit'    => true, 'reminder'  => $reminder, 'userId'    => $currentUser->getId()])]);

        return $response;
    }

    public function delete(EntityManagerInterface $em, $id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No Reminder ID was supplied.');
        }

        $reminder = $this->getRepo(Reminder::class)->find($id);

        if (!$reminder) {
            throw $this->createNotFoundException('Reminder with ID "' . $id . '" not found.');
        }

        if ($reminder->getUser() != $this->getCurrentUser()) {
            throw $this->createAccessDeniedException('You do not own this reminder.');
        }

        $em->remove($reminder);
        $em->flush();

        $this->setFlash('success', 'The Reminder "' . $reminder->getId() . '" was successfully deleted.');

        return $this->redirect($this->generateUrl('index'));
    }

    public function particulars($id = null): Response
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No reminder ID supplied.');
        }

        $reminder = $this->getRepo(Reminder::class)->find($id);

        if (!$reminder) {
            throw $this->createNotFoundException('No reminder with ID "' . $id . '" was found.');
        }

        if ($reminder->getUser() != $this->getCurrentUser()) {
            throw $this->createAccessDeniedException('You do not own this reminder.');
        }

        return $this->render('Reminder/particulars.html.twig', ['reminder' => $reminder]);
    }

    public function markAsCompleted(Request $request, EntityManagerInterface $em, TimeService $timeService, $id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No reminder ID supplied.');
        }

        $reminder = $this->getRepo(Reminder::class)->find($id);

        if (!$reminder) {
            throw $this->createNotFoundException('No reminder with ID "' . $id . '" was found.');
        }

        if ($reminder->getUser() != $this->getCurrentUser()) {
            throw $this->createAccessDeniedException('You do not own this reminder.');
        }

        $now         = $timeService->getNewDateTime();

        $reminder->setIsCompleted(true);
        $reminder->setIsCompletedAt($now);

        $em->persist($reminder);
        $em->flush();

        return $this->redirect($request->headers->get('referer'));
    }

    public function reactivate(EntityManagerInterface $em, Request $request, $id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No reminder ID supplied.');
        }

        $reminder = $this->getRepo(Reminder::class)->find($id);

        if (!$reminder) {
            throw $this->createNotFoundException('No reminder with ID "' . $id . '" was found.');
        }

        if ($reminder->getUser() != $this->getCurrentUser()) {
            throw $this->createAccessDeniedException('You do not own this reminder.');
        }

        $reminder->setIsCompleted(false);
        $reminder->setIsCompletedAt(null);

        $em->persist($reminder);
        $em->flush();

        return $this->redirect($request->headers->get('referer'));
    }
}
