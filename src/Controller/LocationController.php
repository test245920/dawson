<?php

namespace App\Controller;

use App\Entity\Job;
use App\Entity\Location;
use App\Entity\SiteAddress;
use App\Entity\StockItem;
use App\Entity\SupplierStock;
use App\Form\Type\SiteAddressType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class LocationController extends Controller
{
    public function listStockLocations(): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');
        $locations = $this->getRepo(Location::class)->getStockLocations();

        return $this->render('Stock/listStockLocations.html.twig', ['locations' => $locations]);
    }

    public function newStockLocation(Request $request, EntityManagerInterface $em): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $response = new JsonResponse();
        $site     = new SiteAddress();
        $form     = $this->createForm(SiteAddressType::class, $site);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var SiteAddress $site */
            $site = $form->getData();
            $isMainStock = $form['isMainStock']->getData();

            $site->setIsStockLocation(1);

            $em->persist($site);

            $location = new Location();
            $location->setSiteAddress($site);

            if ($isMainStock === true) {
                $currentMainStocks = $this->getRepo(Location::class)->findBy(['isMainStock' => true]);

                if (sizeof($currentMainStocks)) {
                    /** @var Location $currentMainStock */
                    foreach ($currentMainStocks as $currentMainStock) {
                        $currentMainStock->setIsMainStock(null);
                        $em->persist($currentMainStock);
                    }
                    $em->flush();
                }

                $location->setIsMainStock(true);
            }

            $em->persist($location);

            $em->flush();

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The stock location item "' . $site->getAddressString() . '" has been successfully added');

            return $response;
        }

        $response->setData(['success'     => true, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Stock/_stockLocationForm.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('new_stock_location'), 'site'      => $site, 'isEdit'    => false])]);

        return $response;
    }

    public function editStockLocation(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $response = new JsonResponse();
        $site     = $this->getRepo(SiteAddress::class)->find($id);

        if (!$site) {
            $response->setData(['success'     => false, 'displayForm' => false, 'message'     => 'The stock location was not found', 'refresh'     => false]);

            return $response;
        }

        $form = $this->createForm(SiteAddressType::class, $site);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var SiteAddress $site */
            $site = $form->getData();
            $isMainStock = $form['isMainStock']->getData();

            if ($isMainStock === true) {
                $currentMainStocks = $this->getRepo(Location::class)->findBy(['isMainStock' => true]);

                if (sizeof($currentMainStocks)) {
                    /** @var Location $currentMainStock */
                    foreach ($currentMainStocks as $currentMainStock) {
                        $currentMainStock->setIsMainStock(null);
                        $em->persist($currentMainStock);
                    }
                    $em->flush();
                }

                /** @var Location $location */
                $location = $site->getLocation();
                $location->setIsMainStock(true);
                $em->persist($location);
            }

            $em->persist($site);
            $em->flush();

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The stock location "' . $site->getAddressString() . '" has been successfully edited');

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'isEdit'      => true, 'refresh'     => false, 'html'        => $this->renderView('Stock/_stockLocationForm.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('edit_stock_location', ['id' => $id]), 'site'     => $site, 'isEdit'    => true])]);

        return $response;
    }

    public function deleteStockLocation(EntityManagerInterface $em, $id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        /** @var SiteAddress $site */
        $site = $this->getRepo(SiteAddress::class)->find($id);
        /** @var Location $location */
        $location = $site->getLocation();

        if (!$site) {
            throw $this->createNotFoundException('The Stock item with ID"' . $id . '" was not found.');
        }

        if ($location->getIsMainStock() === true) {
            $this->setFlash('error', 'Can not delete Main Stock');
        } else {
            $em->remove($site);
            $em->remove($location);
            $em->flush();

            $this->setFlash('success', 'The site address "' . $site->getAddressString() . '" was successfully deleted');
        }

        return $this->redirect($this->generateUrl('list_stock_locations'));
    }

    public function getStockLocationParticulars($id = null): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $location = $this->getRepo(Location::class)->find($id);

        $mainStockLocation = $this->getRepo(Location::class)->findOneBy(['isMainStock' => true]);
        $mainStockName = $mainStockLocation ? $mainStockLocation->getSiteAddress()->getName() : null;

        $stockItems = $location->getStockItems();
        $stock = [];
        $count = [];

        /** @var StockItem $stockItem */
        foreach ($stockItems as $stockItem) {
            if (!$stockItem->getJob() && !$stockItem->getUsedOnJob()) {
                if (in_array($stockItem->getStock()->getId(), array_map(fn ($o): ?int => $o->getId(), $stock))) {
                    ++$count[$stockItem->getStock()->getId()];
                } else {
                    $stock[] = $stockItem->getStock();
                    $count[$stockItem->getStock()->getId()] = 1;
                }
            }
        }

        return $this->render('Stock/_stockLocationParticulars.html.twig', ['location' => $location, 'stock' => $stock, 'count' => $count, 'mainStockName' => $mainStockName]);
    }

    public function viewStockLocation(?string $id): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if (!$id) {
            throw $this->createNotFoundException('No stock ID supplied.');
        }

        /** @var SiteAddress $site */
        $site = $this->getRepo(SiteAddress::class)->find($id);

        if (!$site) {
            throw $this->createNotFoundException('Stock location with ID ' . $id . ' not found.');
        }
        // dd($site->getLocation());
        $location = $site->getLocation();
        $stockItems = $location && sizeof($location->getStockItems()) ? true : false;

        $versions = $this->getVersionRepo()->getLogEntries($site);

        return $this->render('Stock/viewStockLocation.html.twig', ['site'     => $site, 'versions' => $versions, 'stockItems' => $stockItems]);
    }

    public function getStockQuantity($supplierStockId = null, $locationId = null): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if (!$supplierStockId) {
            throw $this->createNotFoundException('No stock ID supplied.');
        }

        if (!$locationId) {
            throw $this->createNotFoundException('No location ID supplied.');
        }

        $quantity = $this->getRepo(StockItem::class)->getCountItemsAtLocation($supplierStockId, $locationId);

        $response = new JsonResponse();
        $response->setData(['quantity' => $quantity]);

        return $response;
    }

    public function getLocationsForSupplierStock($jobId, $workLog, $supplierStockId): JsonResponse
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);

        if (!$supplierStockId) {
            throw $this->createNotFoundException('No stock ID supplied.');
        }

        $engineer = null;

        if ($this->isGranted('ROLE_ENGINEER') && !$this->isGranted('ROLE_SUPER_ADMIN')) {
            $engineer =  $this->getCurrentUser();
        }

        $workLog = $workLog && $workLog !== 'false' ? true : false;

        $job = $this->getRepo(Job::class)->find($jobId);

        $locations = $this->getRepo(SupplierStock::class)->getLocationsWithStock($supplierStockId, $engineer, $job, $workLog);

        $response = new JsonResponse();

        $response->setData(['locations' => $locations]);

        return $response;
    }
}
