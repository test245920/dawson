<?php

namespace App\Controller;

use App\Entity\Job;
use App\Entity\Location;
use App\Entity\PurchaseOrder;
use App\Entity\PurchaseOrderItem;
use App\Entity\SiteAddress;
use App\Entity\Stock;
use App\Entity\StockItem;
use App\Entity\StockTransaction;
use App\Entity\Supplier;
use App\Entity\SupplierStock;
use App\Entity\SupplierStockIssuePriceRule;
use App\Entity\User;
use App\Form\Type\AddStockType;
use App\Form\Type\IssueStockToJobType;
use App\Form\Type\ManualStockLevelAdjustmentType;
use App\Form\Type\MoveStockType;
use App\Form\Type\PreferredSupplierType;
use App\Form\Type\ReplacePartType;
use App\Form\Type\ReturnPartCountType;
use App\Form\Type\ReturnStockFromJobType;
use App\Form\Type\SelectStockLocationType;
use App\Form\Type\SetStockBinType;
use App\Form\Type\StockType;
use App\Form\Type\SupplierStockType;
use App\Model\AddStock;
use App\Model\StockAddition;
use App\Model\StockAdjustment;
use App\Model\StockAssignment;
use App\Model\StockReturn;
use App\Model\TransactionCategory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class StockController extends Controller
{
    public function list($page = 1): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        return $this->render('Stock/list.html.twig');
    }

    public function new(Request $request, EntityManagerInterface $em): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $response = new JsonResponse();
        $stock    = new AddStock();
        $form     = $this->createForm(AddStockType::class, $stock);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var AddStock $addStock */
            $addStock = $form->getData();
            $stock = new Stock();
            $stock->setPreferredSupplier($addStock->getPreferredSupplier());
            $stock->setCode($addStock->getCode());
            $stock->setPrevCode($addStock->getPrevCode());
            $stock->setDescription($addStock->getDescription());
            $stock->setOemPrice($addStock->getOemPrice());
            $stock->setMinStock($addStock->getMinStock());
            $stock->setReorderQuantity($addStock->getReorderQuantity());
            $stock->setBin($addStock->getBin());

            $em->persist($stock);
            $em->flush();

            $supplierStock = new SupplierStock();

            $supplierStock->setCode($stock->getCode());
            $supplierStock->setDescription($stock->getDescription());
            $supplierStock->setStock($stock);
            $supplierStock->setSupplier($stock->getPreferredSupplier());
            $supplierStock->setCostPrice($addStock->getCostPrice());
            $supplierStock->setCarriagePrice($addStock->getCarriagePrice());
            $supplierStock->setSupplierNotes($addStock->getSupplierNotes());

            $costPrice = $supplierStock->getCostPrice();
            $supplierId = $supplierStock->getSupplier()->getId();
            $issuePrice = $addStock->getIssuePrice();
            $profitMargin = $this->getRepo(SupplierStockIssuePriceRule::class)->getMargin($supplierId, $costPrice);

            if ($profitMargin && !$issuePrice) {
                $issuePrice =  $costPrice + ($costPrice * $profitMargin / 100);
            }

            $supplierStock->setIssuePrice($issuePrice);

            $em->persist($supplierStock);
            $em->flush();

            if ($addStock->getLocation() && $addStock->getQuantity() > 0) {
                for ($i = 0; $i < $addStock->getQuantity(); ++$i) {
                    $stockItem = new StockItem();

                    $stockItem->setLocation($addStock->getLocation());
                    $stockItem->setStock($stock);
                    $stockItem->setSupplierStock($supplierStock);
                    $em->persist($stockItem);
                }
                $em->flush();
            }

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true, 'stock'       => ['id'          => $stock->getId(), 'description' => $stock->getDescription()]]);

            $this->setFlash('success', 'The stock item "' . $stock->getDescription()
                . '" has been successfully added<br/>The supplier ' . $supplierStock->getSupplier()->getName()
                . ' now supplies stock item "' . $supplierStock->getStock()->getDescription() . '".');

            return $response;
        }

        $response->setData(['success'     => true, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Stock/_form.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('new_stock'), 'stock'     => $stock, 'isEdit'    => false])]);

        return $response;
    }

    public function edit(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $response = new JsonResponse();

        $stock = $this->getRepo(Stock::class)->find($id);

        if (!$stock) {
            $response->setData([
                'success'     => false,
                'displayForm' => false,
                'message'     => 'The stock item was not found',
                'refresh'     => false,
            ]);

            return $response;
        }

        $form = $this->createForm(StockType::class, $stock, ['stockId' => $id]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $stock = $form->getData();

            $em->persist($stock);
            $em->flush();

            $response->setData([
                'success'     => true,
                'displayForm' => false,
                'refresh'     => true,
                'selected'    => $id,
            ]);

            $this->setFlash('success', 'The stock item "' . $stock->getCode() . '" has been successfully edited');

            return $response;
        }

        $response->setData([
            'success'     => true,
            'displayForm' => true,
            'isEdit'      => true,
            'refresh'     => false,
            'html'        => $this->renderView('Stock/_form.html.twig', [
                'form'      => $form->createView(),
                'formRoute' => $this->generateUrl('edit_stock', ['id' => $id]),
                'stock'     => $stock,
                'isEdit'    => true,
            ]),
        ]);

        return $response;
    }

    public function view(?string $id): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if (!$id) {
            throw $this->createNotFoundException('No stock ID supplied.');
        }

        $stock           = $this->getRepo(Stock::class)->find($id);
        $quantityInStock = $this->getRepo(StockItem::class)->getQuantity($id);
        $stockOnJobs     = $this->getRepo(StockItem::class)->getStockOnJobs($id);

        if (!$stock) {
            throw $this->createNotFoundException('Stock item with ID ' . $id . ' not found.');
        }

        $versions = $this->getVersionRepo()->getLogEntries($stock);

        return $this->render('Stock/view.html.twig', ['stock'           => $stock, 'versions'        => $versions, 'stockOnJobs'     => $stockOnJobs, 'quantityInStock' => $quantityInStock]);
    }

    public function viewSupplierStock(Request $request, ?string $id): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if (!$id) {
            throw $this->createNotFoundException('No supplier stock ID supplied.');
        }

        $supplierStock = $this->getRepo(SupplierStock::class)->find($id);

        if (!$supplierStock) {
            throw $this->createNotFoundException('Supplier stock item with ID ' . $id . ' not found.');
        }

        $versions = $this->getVersionRepo()->getLogEntries($supplierStock);

        $showEdit = $request->query->has('edit');

        return $this->render('SupplierStock/view.html.twig', ['supplierStock' => $supplierStock, 'versions'      => $versions, 'showEdit'      => $showEdit]);
    }

    public function delete(EntityManagerInterface $em, $id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        /** @var Stock $stock */
        $stock = $this->getRepo(Stock::class)->find($id);

        if (!$stock) {
            throw $this->createNotFoundException('The Stock item with ID"' . $id . '" was not found.');
        }

        if ($stockItems = $stock->getStockItems()) {
            /** @var StockItem $stockItem */
            foreach ($stockItems as $stockItem) {
                /** @var Job $job */
                if ($job = $stockItem->getJob()) {
                    if ($purchaseOrders = $job->getPurchaseOrders()) {
                        /** @var PurchaseOrder $purchaseOrder */
                        foreach ($purchaseOrders as $purchaseOrder) {
                            if ($stockTransactions = $purchaseOrder->getStockTransactions()) {
                                /** @var StockTransaction $stockTransaction */
                                foreach ($stockTransactions as $stockTransaction) {
                                    if ($stockTransaction->getStock() === $stock) {
                                        $em->remove($stockTransaction);
                                    }
                                }
                            }
                            if ($purchaseOrderItems = $purchaseOrder->getPurchaseOrderItems()) {
                                /** @var PurchaseOrderItem $purchaseOrderItem */
                                foreach ($purchaseOrderItems as $purchaseOrderItem) {
                                    /** @var SupplierStock $item */
                                    $item = $purchaseOrderItem->getItem();
                                    if ($item->getStock() === $stock) {
                                        $em->remove($purchaseOrderItem);
                                    }
                                }
                                $em->flush();

                                if (!sizeof($purchaseOrder->getPurchaseOrderItems())) {
                                    $job->removePurchaseOrder($purchaseOrder);
                                    $em->remove($purchaseOrder);
                                }
                            }
                        }
                    }
                }
                if ($supplierStock = $stockItem->getSupplierStock()) {
                    $em->remove($supplierStock);
                }
                $em->remove($stockItem);
            }
        }

        $em->remove($stock);
        $em->flush();

        $this->setFlash('success', 'The stock item "' . $stock->getCode() . '" was successfully deleted');

        return $this->redirect($this->generateUrl('list_stock'));
    }

    public function stockParticulars($id = null, $isStock = null): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if ($isStock !== 'true') {
            $item = $this->getRepo(SupplierStock::class)->find($id);

            if (!$item) {
                throw $this->createNotFoundException('Supplier stock  with ID "' . $id . '" not found.');
            }

            $id = $item->getStock()->getId();
            $code = $item->getCode();
            $description = $item->getDescription();
        }

        $stockItemRepo = $this->getRepo(StockItem::class);
        $poiRepo = $this->getRepo(PurchaseOrderItem::class);

        $stock = $this->getRepo(Stock::class)->findWithSupplierStock($id);

        if (!$stock) {
            throw $this->createNotFoundException('Stock  with ID "' . $id . '" not found.');
        }

        $quantityInStock = $stockItemRepo->getQuantity($id);
        $qtyOnLocations  = $stockItemRepo->getLocations($id);
        $stockOnJobs     = $stockItemRepo->getStockOnJobs($id);
        $qtyOnJobs       = $stockItemRepo->getJobs($id);
        $stockOnOrder    = $poiRepo->getStockOnOrder($id);
        $qtyOnPos        = $poiRepo->getPos($id);
        $availableStock  = $quantityInStock - $stockOnJobs;

        if ($availableStock <= 0) {
            $availableStock = 0;
        }

        $supplierCounts = [];

        foreach ($stock->getSupplierStock() as $supplierStock) {
            $supplierCounts[$supplierStock->getId()] = $stockItemRepo->getSupplierQuantity($supplierStock->getId());
        }

        return $this->render('Stock/particulars.html.twig', ['stock'           => $stock, 'qtyOnPos'        => $qtyOnPos, 'qtyOnJobs'       => $qtyOnJobs, 'stockOnJobs'     => $stockOnJobs, 'stockOnOrder'    => $stockOnOrder, 'qtyOnLocations'  => $qtyOnLocations, 'availableStock'  => $availableStock, 'quantityInStock' => $quantityInStock, 'supplierCounts'  => $supplierCounts, 'code'            => $code ?? $stock->getCode(), 'description'     => $description ?? $stock->getDescription()]);
    }

    /*
     * Adds additional supplier stock items to the stock list
     */
    public function move(Request $request, EntityManagerInterface $em, $id = null): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No stock ID supplied.');
        }

        $stock = $this->getRepo(Stock::class)->find($id);

        if (!$stock) {
            throw $this->createNotFoundException('The stock item with ID "' . $id . '" was not found.');
        }

        $addition = new StockAddition($stock);

        $response = new JsonResponse();
        $form     = $this->createForm(MoveStockType::class, $addition, ['id' => $id]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $addition = $form->getData();
            $quantity = $addition->getQuantity();
            $fromLocation = $addition->getFromLocation();

            $stockItems = $this->getRepo(StockItem::class)->findBy(['location' => $fromLocation->getId(), 'stock' => $stock->getId(), 'job' => null, 'usedOnJob' => null]);

            /** @var StockItem $stockItem */
            foreach ($stockItems as $key => $stockItem) {
                if ($key < $quantity) {
                    $stockItem->setLocation($addition->getToLocation());
                    $em->persist($stockItem);
                }
            }

            $em->flush();

            $this->setFlash('success', 'The stock levels for item "' . $stock->getDescription() . '" have been successfully updated');

            if (!$stock->getBin()) {
                return $this->redirect($this->generateUrl('stock_set_bin', ['id' => $stock->getId()]));
            }

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'isEdit'      => true, 'refresh'     => false, 'html'        => $this->renderView('Stock/_moveStockForm.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('move_stock', ['id' => $id]), 'stock'     => $stock, 'isEdit'    => true])]);

        return $response;
    }

    public function returnStockFromJob(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if ($id === null) {
            throw $this->CreateNotFoundException('No stock ID supplied');
        }

        $stock = $this->getRepo(Stock::class)->find($id);

        if (!$stock) {
            throw $this->CreateNotFoundException('The stock item with ID "' . $id . '" was not found.');
        }

        $return = new StockReturn($stock);

        $form = $this->createForm(ReturnStockFromJobType::class, $return);
        $form->handleRequest($request);

        $response = new JsonResponse();

        if ($form->isSubmitted() && $form->isValid()) {
            $return = $form->getData();

            $job           = $return->getJob();
            $quantity      = $return->getQuantity();
            $supplierStock = $return->getSupplierStock();

            if ($job && ($quantity > 0) && $supplierStock) {
                $stockItems = $this->getRepo(StockItem::class)->getSupplierStockItemsOnJob($supplierStock, $job, $quantity);

                /** @var StockItem $stockItem */
                foreach ($stockItems as $stockItem) {
                    $job->removeAttachedStockItem($stockItem);

                    $stockItem->setJob(null);
                    $stockItem->setReturnedFromJobId($job->getId());

                    $em->persist($stockItem);
                    $em->persist($job);
                }

                $updated = is_countable($stockItems) ? count($stockItems) : 0;

                $stockTransaction = new StockTransaction();
                $stockTransaction->setTransactionCategory(TransactionCategory::IN_JOB);
                $stockTransaction->setQuantity($updated);
                $stockTransaction->setSupplierStock($supplierStock);
                $stockTransaction->setStock($stock);
                $stockTransaction->setJob($job);

                $em->persist($stockTransaction);
                $em->flush();

                $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

                $this->setFlash('success', $updated . ' x "' . $stock->getDescription() . '" successfully returned from job "' . $job->getJobCode() . '".');

                return $response;
            }
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'isEdit'      => false, 'refresh'     => false, 'html'        => $this->renderView('Stock/_returnStockForm.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('return_stock_from_job', ['id' => $id]), 'id'        => $id])]);

        return $response;
    }

    public function returnPart(Request $request, EntityManagerInterface $em, $locationId = null, $stockId = null): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if ($locationId === null) {
            throw $this->CreateNotFoundException('No location ID');
        }

        if ($stockId === null) {
            throw $this->CreateNotFoundException('No stock ID');
        }

        $maxCount = $this->getRepo(StockItem::class)->getCountAtLocation($locationId, $stockId);

        if (!$maxCount) {
            throw $this->CreateNotFoundException('This item was not found.');
        }

        $form = $this->createForm(ReturnPartCountType::class, [], ['maxCount' => $maxCount]);
        $form->handleRequest($request);

        $response = new JsonResponse();

        $location = $this->getRepo(Location::class)->findOneBy(['isMainStock' => true]);

        if (!$location) {
            throw $this->CreateNotFoundException('Main stock was not found.');
        }

        $mainStockName = $location->getSiteAddress()->getName();

        if ($form->isSubmitted() && $form->isValid()) {
            $count = $form['count']->getData() > $maxCount ? $maxCount : $form['count']->getData();
            $stockItems = $this->getRepo(StockItem::class)->getUnusedAtLocation($locationId, $stockId);

            /** @var StockItem $stockItem */
            foreach ($stockItems as $key => $stockItem) {
                if ($key < $count) {
                    $stockItem->setLocation($location);
                    $em->persist($stockItem);
                }
            }

            $em->flush();

            $response->setData([
                'success'     => true,
                'displayForm' => false,
                'refresh'     => true,
            ]);

            $this->setFlash('success', 'Successfully returned to "' . $mainStockName . '"');

            return $response;
        }

        $response->setData([
            'success'     => false,
            'displayForm' => true,
            'isEdit'      => false,
            'refresh'     => false,
            'html'        => $this->renderView('Stock/_returnPartForm.html.twig', [
                'form'      => $form->createView(),
                'formRoute' => $this->generateUrl('return_part', ['locationId' => $locationId, 'stockId' => $stockId]),
                'mainStockName' => $mainStockName,
            ]),
        ]);

        return $response;
    }

    public function addSupplierStock(Request $request, EntityManagerInterface $em, $stockId = null, $supplierId = null): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if (!$stockId || $stockId == 'null') {
            throw $this->CreateNotFoundException('No stock ID provided.');
        }

        $stock = $this->getRepo(Stock::class)->find($stockId);

        if (!$stock) {
            throw $this->CreateNotFoundException('The stock item with ID "' . $stockId . '" was not found.');
        }

        if ($supplierId && $supplierId != 'null') {
            $supplier = $this->getRepo(Supplier::class)->find($supplierId);

            if (!$supplier) {
                throw $this->CreateNotFoundException('The supplier with ID "' . $supplierId . '" was not found.');
            }
        } else {
            $supplier = null;
        }

        $response      = new JsonResponse();
        $supplierStock = new SupplierStock();

        $supplierStock->setCode($stock->getCode());
        $supplierStock->setDescription($stock->getDescription());
        $supplierStock->setStock($stock);
        $supplierStock->setSupplier($supplier);

        $form = $this->createForm(SupplierStockType::class, $supplierStock);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $supplierStock = $form->getData();

            $costPrice = $supplierStock->getCostPrice();
            $supplierId = $supplierStock->getSupplier()->getId();
            $issuePrice = $supplierStock->getIssuePrice();
            $profitMargin = $this->getRepo(SupplierStockIssuePriceRule::class)->getMargin($supplierId, $costPrice);

            if ($profitMargin && !$issuePrice) {
                $issuePrice =  $costPrice + ($costPrice * $profitMargin / 100);
            }

            $supplierStock->setIssuePrice($issuePrice);

            if ($supplier) {
                $supplierStock->addSupplier($supplier);
            }

            $em->persist($supplierStock);
            $em->flush();

            if (sizeof($stock->getSupplierStock()) > 1) {
                $preferredSupplierForm = $this->createForm(PreferredSupplierType::class, $stock, ['id' => $stockId]);
                $response->setData(['success'     => false, 'displayForm' => true, 'isEdit'      => false, 'refresh'     => false, 'html'        => $this->renderView('Stock/_choosePreferredSupplierForm.html.twig', ['form'      => $preferredSupplierForm->createView(), 'formRoute' => $this->generateUrl('stock_choose_preferred_supplier', ['id' => $stockId]), 'stock'     => $stock])]);
            } else {
                $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);
            }

            $this->setFlash('success', 'The supplier ' . $supplierStock->getSupplier()->getName() . ' now supplies stock item "' . $supplierStock->getStock()->getDescription() . '".');

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'isEdit'      => false, 'refresh'     => false, 'html'        => $this->renderView('Stock/_supplierStockForm.html.twig', ['form'            => $form->createView(), 'formRoute'       => $this->generateUrl('add_supplier_stock', ['stockId' => $stockId, 'supplier' => $supplierId]), 'stock'           => $stock, 'isEdit'          => false, 'supplierStockId' => $supplierStock->getId()])]);

        return $response;
    }

    public function editSupplierStock(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $response      = new JsonResponse();
        $supplierStock = $this->getRepo(SupplierStock::class)->find($id);

        if (!$supplierStock) {
            $response->setData(['success'     => false, 'displayForm' => false, 'refresh'     => false]);

            $this->setFlash('error', 'The supplier stock "' . $id . '" was not found.');

            return $response;
        }

        $form = $this->createForm(SupplierStockType::class, $supplierStock);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $supplierStock = $form->getData();

            $costPrice = $supplierStock->getCostPrice();
            $supplierId = $supplierStock->getSupplier()->getId();
            $issuePrice = $supplierStock->getIssuePrice();
            $profitMargin = $this->getRepo(SupplierStockIssuePriceRule::class)->getMargin($supplierId, $costPrice);

            if ($profitMargin && !$issuePrice) {
                $issuePrice =  $costPrice + ($costPrice * $profitMargin / 100);
            }

            $supplierStock->setIssuePrice($issuePrice);

            $em->persist($supplierStock);
            $em->flush();

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => false, 'url'         => $this->generateUrl('list_stock')]);

            $this->setFlash('success', 'The supplier Stock item ' . $supplierStock->getId() . ' was successfully updated');

            return $response;
        }

        $response->setData(['success'     => true, 'displayForm' => true, 'isEdit'      => true, 'refresh'     => false, 'html'        => $this->renderView('Stock/_supplierStockForm.html.twig', ['form'          => $form->createView(), 'formRoute'     => $this->generateUrl('edit_supplier_stock', ['id' => $id]), 'supplierStock' => $supplierStock, 'isEdit'        => true])]);

        return $response;
    }

    public function deleteSupplierStock(EntityManagerInterface $em, $id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $supplierStock = $this->getRepo(SupplierStock::class)->find($id);

        if (!$supplierStock) {
            throw $this->createNotFoundException('The Supplier stock item with ID"' . $id . '" was not found.');
        }

        $id      = $supplierStock->getId();
        $stockId = $supplierStock->getStock()->getId();

        $em->remove($supplierStock);
        $em->flush();

        $this->setFlash('success', 'The Supplier stock item "' . $id . '" was successfully deleted');

        return $this->redirect($this->generateUrl('view_stock', ['id' => $stockId]));
    }

    public function searchSupplierStock(Request $request): JsonResponse
    {
        $content     = $request->getContent();
        $searchTerms = json_decode($content, true, 512, JSON_THROW_ON_ERROR);

        $stockItems  = $this->getRepo(SupplierStock::class)->getItemsASupplierStocks($searchTerms);

        $response = new JsonResponse();
        $response->setData(['items'      => $stockItems, 'searchTerm' => $searchTerms[0] ?? null]);

        return $response;
    }

    public function searchStockItems(Request $request, $jobId, $workLog): JsonResponse
    {
        $content    = $request->getContent();
        $searchTerm = json_decode($content, true, 512, JSON_THROW_ON_ERROR);
        $engineer   = null;

        if ($this->isGranted('ROLE_ENGINEER') && !$this->isGranted('ROLE_SUPER_ADMIN')) {
            $engineer =  $this->getCurrentUser();
        }

        $workLog = $workLog && $workLog !== 'false' ? true : false;

        $job = $this->getRepo(Job::class)->find($jobId);

        $stockItems = $this->getRepo(StockItem::class)->getStockItemsContaining($searchTerm, $engineer, $job, $workLog);

        $response = new JsonResponse();
        $response->setData(['items'      => $stockItems, 'searchTerm' => $searchTerm]);

        return $response;
    }

    public function searchStockCatalogue(Request $request, $currentOnly = false): JsonResponse
    {
        $content    = $request->getContent();
        $searchTerm = json_decode($content, true, 512, JSON_THROW_ON_ERROR);
        $stockItems = $this->getRepo(Stock::class)->getStockItemsContaining($searchTerm, $currentOnly);
        $supplierStockItems = $this->getRepo(SupplierStock::class)->getStockItemsContaining($searchTerm, $currentOnly);

        $stockItems  = array_map(fn ($stockItem): array => array_merge($stockItem, ['isStock' => true]), $stockItems);

        $supplierStockItems  = array_map(fn ($supplierStockItem): array => array_merge($supplierStockItem, ['isStock' => false]), $supplierStockItems);

        $items = array_merge($stockItems, $supplierStockItems);
        $response   = new JsonResponse();

        $response->setData(['items'       => $items, 'searchTerm'  => $searchTerm]);

        return $response;
    }

    public function issueStockToJob(Request $request, string $id, EntityManagerInterface $em): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $stock = $this->getRepo(Stock::class)->find($id);

        if (!$stock) {
            throw $this->CreateNotFoundException('No stock with ID "' . $id . '" was found in the database.');
        }

        $stockItemQty = $this->getRepo(StockItem::class)->getQuantity($id);

        if ($stockItemQty == 0) {
            throw $this->CreateNotFoundException('No items of stock with ID "' . $id . '" were found in the database. Quantity = zero.');
        }

        $assignment = new StockAssignment($stock);

        $response = new JsonResponse();
        $form     = $this->createForm(IssueStockToJobType::class, $assignment);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $assignment = $form->getData();
            $quantity   = $assignment->getQuantity();
            $supplierStock = $assignment->getSupplierStock();
            $job = $assignment->getJob();

            $stockItems = $this->getRepo(StockItem::class)->getAvailableItems($supplierStock, $assignment->getLocation(), $quantity);

            if ((is_countable($stockItems) ? count($stockItems) : 0) !== $quantity) {
                $form->get('location')->addError(new FormError('There are not enough items in stock at the specified location for this task'));
            }
        }

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var StockItem $stockItem */
            foreach ($stockItems as $stockItem) {
                $stockItem->setJob($job);
                $stockItem->setReturnedFromJobId(null);
                $em->persist($stockItem);
            }

            $stockTransaction = new StockTransaction();
            $stockTransaction->setTransactionCategory(TransactionCategory::OUT_JOB);
            $stockTransaction->setQuantity($quantity);
            $stockTransaction->setJob($job);
            $stockTransaction->setSupplierStock($supplierStock);
            $stockTransaction->setStock($assignment->getStock());

            $em->persist($stockTransaction);

            $em->flush();

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $jobCode = $assignment->getJob()->getJobCode();

            $this->setFlash('success', $quantity . ' x "' . $stock->getDescription() . '" successfully issued to Job "' . $jobCode . '"');

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Stock/_issueStockToJobForm.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('stock_issue_to_job', ['id' => $id]), 'isEdit'    => false, 'stock'     => $stock])]);

        return $response;
    }

    public function manualStockLevelAdjustment(Request $request, string $id, EntityManagerInterface $em): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $stock = $this->getRepo(Stock::class)->find($id);

        if (!$stock) {
            throw $this->CreateNotFoundException('No stock with ID "' . $id . '" was found in the database.');
        }

        $adjustment = new StockAdjustment($stock);
        $response     = new JsonResponse();

        $form = $this->createForm(ManualStockLevelAdjustmentType::class, $adjustment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $adjustment    = $form->getData();
            $location      = $adjustment->getLocation();
            $quantity      = $adjustment->getQuantity();
            $supplierStock = $adjustment->getSupplierStock();
            $bin           = $adjustment->getBin();

            $currentStockLevel = (int) $this->getRepo(StockItem::class)->getCountItemsAtLocation($supplierStock, $location);

            if ($quantity == $currentStockLevel) {
                $stock->setBin($bin);
                $em->persist($stock);
                $em->flush();

                $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

                $this->setFlash('success', 'The stock level for "' . $stock->getDescription() . '" has been successfully updated.');

                return $response;
            }

            $stockTransaction = new StockTransaction();
            $stockTransaction->setSupplierStock($supplierStock);
            $stockTransaction->setLocation($location);

            $numberUpdated = 0;

            if ($quantity > $currentStockLevel) {
                $transactionType = TransactionCategory::IN_MANUAL;

                $difference = $quantity - $currentStockLevel;

                for ($i = 0; $i < $difference; ++$i) {
                    $stockItem = new StockItem();
                    $stockItem->setStock($stock);
                    $stockItem->setSupplierStock($supplierStock);
                    $stockItem->setLocation($location);
                    $stockItem->setReturnedFromJobId(null);
                    $em->persist($stockItem);
                }

                $numberUpdated = $difference;
            } else {
                $transactionType = TransactionCategory::OUT_MANUAL;
                $difference      = $currentStockLevel - $quantity;
                $stockItems      = $this->getRepo(StockItem::class)->getAvailableItems($supplierStock, $location, $difference);

                foreach ($stockItems as $stockItem) {
                    ++$numberUpdated;
                    $em->remove($stockItem);
                }
            }
            $stock->setBin($bin);
            $em->persist($stock);

            $stockTransaction->setTransactionCategory($transactionType);
            $stockTransaction->setQuantity($numberUpdated);
            $stockTransaction->setStock($stock);

            $em->persist($stockTransaction);
            $em->flush();

            if ($numberUpdated === $difference) {
                $flashMessage = 'The stock level for "' . $stock->getDescription() . '" has been successfully updated.';
                $this->setFlash('success', $flashMessage);
            } else {
                $flashMessage = 'The stock level for "' . $stock->getDescription() . '" has been updated, but the new stock level may not match what was requested.';
                $this->setFlash('info', $flashMessage);
            }

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Stock/_manualStockLevelAdjustmentForm.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('stock_level_manual_adjustment', ['id' => $id]), 'isEdit'    => false, 'stock'     => $stock])]);

        return $response;
    }

    public function replacePart(Request $request, EntityManagerInterface $em, $stockId = null): JsonResponse
    {
        $this->securityCheck('ROLE_SUPER_ADMIN');

        if ($stockId === null) {
            throw $this->CreateNotFoundException('No Stock ID supplied.');
        }

        $stock = $this->getRepo(Stock::class)->find($stockId);

        if (!$stock) {
            throw $this->CreateNotFoundException('No stock with ID "' . $stockId . '" was found in the database.');
        }

        $newStock = clone $stock;
        $newStock->setId(null);
        $newStock->setCode(null);
        $response = new JsonResponse();

        $form = $this->createForm(ReplacePartType::class, $newStock);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $newStock = $form->getData();

            $newStock->setReplacesPart($stock);

            $em->persist($newStock);
            $em->flush();

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $description = $stock->getDescription();
            $this->setFlash('success', 'The stock item "' . $stock->getCode() . '" have been superceded by part "' . $newStock->getCode() . '".');

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Stock/_replacePartForm.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('stock_replace_part', ['stockId' => $stockId]), 'isEdit'    => false, 'stockId'     => $stockId])]);

        return $response;
    }

    public function choosePreferredSupplier(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SUPER_ADMIN');

        if ($id === null) {
            throw $this->CreateNotFoundException('No Stock ID supplied.');
        }

        $stock = $this->getRepo(Stock::class)->find($id);

        if (!$stock) {
            throw $this->CreateNotFoundException('No stock with ID "' . $id . '" was found in the database.');
        }

        $response = new JsonResponse();
        $form     = $this->createForm(PreferredSupplierType::class, $stock, ['id' => $id]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $stock = $form->getData();

            $em->persist($stock);
            $em->flush();

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $supplier = $stock->getPreferredSupplier()->getName();
            $this->setFlash('success', 'The supplier "' . $supplier . '" is now preferred supplier for "' . $stock->getStockString() . '".');

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Stock/_choosePreferredSupplierForm.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('stock_replace_part', ['id' => $id]), 'id'        => $id, 'stock'        => $stock])]);

        return $response;
    }

    public function newStockItemForPurchaseOrder(Request $request, EntityManagerInterface $em, $supplierId = null): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if ($supplierId === null) {
            throw new \Exception('No Supplier ID supplied.');
        }

        $supplier = $this->getRepo(Supplier::class)->find($supplierId);

        if (!$supplier) {
            throw new \Exception('No supplier with ID "' . $supplierId . '" was found in the database.');
        }

        $response = new JsonResponse();
        $stock    = new Stock();
        $stock->setPreferredSupplier($supplier);

        $form = $this->createForm(StockType::class, $stock);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $stock       = $form->getData();
            $code        = $stock->getCode();
            $description = $stock->getDescription();

            $em->persist($stock);

            $supplierStock = new SupplierStock();

            $supplierStock->setStock($stock);
            $supplierStock->setSupplier($supplier);
            $supplierStock->setCostPrice($stock->getOemPrice());

            $em->persist($supplierStock);
            $em->flush();

            $response->setData(['success'          => true, 'displayForm'      => false, 'refresh'          => false, 'stockDescription' => $stock->getDescription(), 'stockId'          => $stock->getId()]);

            $this->setFlash('success', 'The stock item "' . $stock->getDescription() . '" has been successfully added');

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Stock/_newForPurchaseOrderForm.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('new_stock_for_purchase_order', ['supplierId' => $supplierId]), 'stock'     => $stock, 'isEdit'    => false])]);

        return $response;
    }

    public function setBin(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $response = new JsonResponse();

        $stock = $this->getRepo(Stock::class)->find($id);

        if (!$stock) {
            $response->setData(['success'     => false, 'displayForm' => false, 'message'     => 'The stock item was not found', 'refresh'     => false]);

            return $response;
        }

        $form = $this->createForm(SetStockBinType::class, $stock);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($stock);
            $em->flush();

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The bin number was set for item "' . $stock->getStockString() . '"');

            return $response;
        }

        $response->setData(['success'     => true, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Stock/_setStockBinForm.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('stock_set_bin', ['id' => $id]), 'stock'     => $stock])]);

        return $response;
    }

    public function getCountStockItemsByLocation(Request $request): JsonResponse
    {
        $fromLocation = $request->get('fromLocation');
        $stockId = $request->get('stockId');

        $countItems = $this->getRepo(StockItem::class)->getCountAtLocation($fromLocation, $stockId);

        return new JsonResponse(['countItems' => $countItems]);
    }

    public function selectStockLocation(Request $request, $id, EntityManagerInterface $em): JsonResponse
    {
        /** @var Location $location */
        $location = $this->getRepo(Location::class)->find($id);
        /** @var User $user */
        $user = $location->getEngineer();
        /** @var SiteAddress $siteAddress */
        $siteAddress = $location->getSiteAddress();
        $response = new JsonResponse();

        if (!$location) {
            $response->setData(['success'     => false, 'displayForm' => false, 'refresh'     => false]);

            $this->setFlash('error', 'Location was not found.');

            return $response;
        }

        $form = $this->createForm(SelectStockLocationType::class, null, ['id' => $id]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $selectedLocation = $form['location']->getData();
            $items = $location->getStockItems();

            /** @var StockItem $item */
            foreach ($items as $item) {
                $item->setLocation($selectedLocation);
                $em->persist($item);
            }

            if ($user) {
                if (sizeof($jobs = $user->getJobs())) {
                    /** @var Job $job */
                    foreach ($jobs as $job) {
                        $job->removeEngineer($user);
                        if (!sizeof($job->getEngineer())) {
                            $job->setCompletionStage(1);
                        }
                        $em->persist($job);
                    }
                }
                $user->setLocation(null);
                $em->persist($user);
                $em->remove($location);

                $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

                $em->flush();

                $this->setFlash('success', 'The user "' . $user->getUsername() . '" has been successfully edited');
            } elseif ($siteAddress) {
                $em->remove($siteAddress);
                $em->remove($location);
                $em->flush();

                $this->setFlash('success', 'The site address "' . $siteAddress->getAddressString() . '" was successfully deleted');
                $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => false, 'url'         => $this->generateUrl('list_stock_locations')]);
            }

            return $response;
        }
        $response->setData(['success'     => true, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Stock/_selectStockLocationForm.html.twig', ['form' => $form->createView(), 'formRoute' => $this->generateUrl('select_stock_location', ['id' => $id])])]);

        return $response;
    }
}
