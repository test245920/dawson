<?php

namespace App\Controller;

use App\Entity\AbsenceLog;
use App\Entity\Asset;
use App\Entity\AssetLifecyclePeriod;
use App\Entity\Estimate;
use App\Entity\Job;
use App\Entity\Lead;
use App\Entity\Location;
use App\Entity\StockItem;
use App\Entity\Truck;
use App\Entity\User;
use App\Entity\WorkLog;
use App\Time\TimeService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ReportController extends Controller
{
    public function sectionReports($section = null): Response
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_SALES_ADMIN']);

        $section = strtolower((string) $section);

        switch ($section) {
            case 'job':
                $this->securityCheck('ROLE_SERVICE_ADMIN');

                $engineers = $this->getRepo(User::class)->getEngineers();

                return $this->render('Job/reports.html.twig', ['engineers' => $engineers]);
                break;
            case 'lead':
                $this->securityCheck('ROLE_SALES_ADMIN');

                $leadOwners = $this->isGranted('ROLE_SUPER_ADMIN') ? $this->getRepo(User::class)->getUsersWithLeads() : [];

                return $this->render('Lead/reports.html.twig', ['leadOwners' => $leadOwners]);
                break;
            case 'estimate':
                $this->securityCheck('ROLE_SERVICE_ADMIN');

                return $this->render('Estimate/reports.html.twig');
                break;
            case 'truck':
                $this->securityCheck('ROLE_SERVICE_ADMIN');

                $assetStatus = $this->getRepo(AssetLifecyclePeriod::class)->getAssetStatus();

                return $this->render('Truck/reports.html.twig', [
                    'assetStatus' => $assetStatus,
                ]);

                break;
            case 'stock':
                $this->securityCheck('ROLE_SERVICE_ADMIN');

                $locations = $this->getRepo(Location::class)->getStockReportOverviewData();

                $totalStockValue = [];
                foreach ($locations as $location) {
                    $locationStockItems = $this->getLocationStockItems($location);
                    $totalStockValue[$location->getId()] = $location->getId() === $locationStockItems['locationId'] ? $locationStockItems['totalStockValue'] : 0;
                }

                return $this->render('Stock/reports.html.twig', ['locations' => $locations, 'totalStockValue' => $totalStockValue]);
                break;

            default:
                throw new \Exception('Reports for "' . $section . '" are not available in this system.');
                break;
        }

        return $this->redirect($this->generateUrl('index'));
    }

    public function jobsCurrentMonthInVsOutReport(): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');
        $datetime = new \DateTime();
        $month    = $datetime->format('F, Y');
        $start = new \DateTime();
        $start->modify('first day of');
        $end = new \DateTime();
        $end->modify('last day of');
        $jobsIn  = $this->getRepo(Job::class)->getJobsInBetween($start, $end);
        $jobsOut = $this->getRepo(Job::class)->getJobsOutBetween($start, $end);
        $response = new Response();
        $response->headers->set('Content-Type', 'text/x-markdown');
        $response->sendHeaders();

        return $this->render('Job/jobsCurrentMonthInVsJobsOut.html.twig', ['jobsIn'  => $jobsIn, 'jobsOut' => $jobsOut, 'month'   => $month]);
    }

    public function jobsInVsOutReport(Request $request, $start = null, $end = null, $status = null, $chargeMethod = null, $customer = null, $engineer = null): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');
        $jobsIn  = $this->getRepo(Job::class)->getJobsInBetween($start, $end, $status, $chargeMethod, $customer, $engineer);
        $jobsOut = $this->getRepo(Job::class)->getJobsOutBetween($start, $end, $status, $chargeMethod, $customer, $engineer);

        $jobsIn = $this->fillJobsArrayWithLabourCost($jobsIn);
        $jobsIn = $this->fillJobsArrayWithTotalJobCost($jobsIn);

        $jobsOut = $this->fillJobsArrayWithLabourCost($jobsOut);
        $jobsOut = $this->fillJobsArrayWithTotalJobCost($jobsOut);

        return $this->render('Job/jobsInVsJobsOut.html.twig', ['jobsIn'  => $jobsIn, 'jobsOut' => $jobsOut, 'start'   => $start, 'end'     => $end]);
    }

    public function jobPartsReturnedToVanStockReport(): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $returnedParts = $this->getRepo(StockItem::class)->getStockItemReturnedFromJob();

        return $this->render('Job/jobPartsReturnedToVanStock.html.twig', [
            'returnedParts' => $returnedParts,
        ]);
    }

    public function engineerTimeLoggedReport($start = null, $end = null, $id = null): Response
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);
        $currentUser = $this->getCurrentUser();

        if ($this->isGranted('ROLE_ENGINEER') && !$this->isGranted('ROLE_SERVICE_ADMIN')) {
            if (!$id) {
                throw $this->createAccessDeniedException('Access Denied.');
            }
            if ((int) $id !== $currentUser->getId()) {
                throw $this->createAccessDeniedException('Access Denied.');
            }
        }

        $timesLogged = $this->getRepo(WorkLog::class)->getEngineerTimeLogged($start, $end, $id);
        $absenceLogged = $this->getRepo(AbsenceLog::class)->getEngineerAbsenceLogged($start, $end, $id);
        $engineerLogged = array_merge($timesLogged, $absenceLogged);

        $engineersTimesLogged = [];
        $engineersTimeLogged = [];
        $totalLabourLogged = ['normal' => 0, 'overtime' => 0, 'extremeOvertime' => 0];
        $totalTravelLogged = ['normal' => 0, 'overtime' => 0, 'extremeOvertime' => 0];
        $totalAbsenceTime = 0;

        foreach ($engineerLogged as $timeLogged) {
            $date = $timeLogged['date']->format('d/m/Y');

            if (!array_key_exists($date, $engineersTimesLogged)) {
                $engineersTimesLogged[$date] = [
                    'timesLogged' => [],
                    'totalLabourLogged' => ['normal' => 0, 'overtime' => 0, 'extremeOvertime' => 0],
                    'totalTravelLogged' => ['normal' => 0, 'overtime' => 0, 'extremeOvertime' => 0],
                    'totalAbsenceTime' => ['normal' => 0, 'overtime' => 0, 'extremeOvertime' => 0],
                ];
            }

            if (!array_key_exists($timeLogged['engineer'], $engineersTimeLogged)) {
                $engineersTimeLogged[$timeLogged['engineer']] = [
                    'timeLabour' => ['normal' => 0, 'overtime' => 0, 'extremeOvertime' => 0],
                    'timeTravel' => ['normal' => 0, 'overtime' => 0, 'extremeOvertime' => 0],
                    'absenceTime' => ['normal' => 0, 'overtime' => 0, 'extremeOvertime' => 0],
                ];
            }

            $engineersTimesLogged[$date]['timesLogged'][] = $timeLogged;

            if (array_key_exists('absence', $timeLogged)) {
                $engineersTimesLogged[$date]['totalAbsenceTime']['normal'] += $timeLogged['time'];
                $engineersTimeLogged[$timeLogged['engineer']]['absenceTime']['normal'] += $timeLogged['time'];
                $totalAbsenceTime += $timeLogged['time'];
            }

            if (array_key_exists('timeLabour', $timeLogged)) {
                $engineersTimesLogged[$date]['totalLabourLogged']['normal'] += $timeLogged['timeLabour'];
                $engineersTimesLogged[$date]['totalLabourLogged']['overtime'] += $timeLogged['timeLabourOvertime'];
                $engineersTimesLogged[$date]['totalLabourLogged']['extremeOvertime'] += $timeLogged['timeLabourExtremeOvertime'];

                $engineersTimesLogged[$date]['totalTravelLogged']['normal'] += $timeLogged['timeTravel'];
                $engineersTimesLogged[$date]['totalTravelLogged']['overtime'] += $timeLogged['timeTravelOvertime'];
                $engineersTimesLogged[$date]['totalTravelLogged']['extremeOvertime'] += $timeLogged['timeTravelExtremeOvertime'];

                $totalLabourLogged['normal'] += $timeLogged['timeLabour'];
                $totalLabourLogged['overtime'] += $timeLogged['timeLabourOvertime'];
                $totalLabourLogged['extremeOvertime'] += $timeLogged['timeLabourExtremeOvertime'];
                $totalTravelLogged['normal'] += $timeLogged['timeTravel'];
                $totalTravelLogged['overtime'] += $timeLogged['timeTravelOvertime'];
                $totalTravelLogged['extremeOvertime'] += $timeLogged['timeTravelExtremeOvertime'];

                $engineersTimeLogged[$timeLogged['engineer']]['timeLabour']['normal'] += $timeLogged['timeLabour'];
                $engineersTimeLogged[$timeLogged['engineer']]['timeLabour']['overtime'] += $timeLogged['timeLabourOvertime'];
                $engineersTimeLogged[$timeLogged['engineer']]['timeLabour']['extremeOvertime'] += $timeLogged['timeLabourExtremeOvertime'];
                $engineersTimeLogged[$timeLogged['engineer']]['timeTravel']['normal'] += $timeLogged['timeTravel'];
                $engineersTimeLogged[$timeLogged['engineer']]['timeTravel']['overtime'] += $timeLogged['timeTravelOvertime'];
                $engineersTimeLogged[$timeLogged['engineer']]['timeTravel']['extremeOvertime'] += $timeLogged['timeTravelExtremeOvertime'];
            }
        }

        return $this->render('Job/engineerTimeLogged.html.twig', [
            'engineersTimesLogged' => $engineersTimesLogged,
            'start'                => $start,
            'end'                  => $end,
            'totalLabourLogged'    => $totalLabourLogged,
            'totalTravelLogged'    => $totalTravelLogged,
            'totalAbsenceTime'     => $totalAbsenceTime,
            'all'                  => $id ? false : true,
            'engineersTimeLogged'  => $engineersTimeLogged,
            'canEdit'              => $this->isGranted('ROLE_SERVICE_ADMIN'),
        ]);
    }

    public function leadsInVsOutReport($start = null, $end = null, $user = null): Response
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($user == 'all') {
            $user = null;
        }

        $leadsIn  = $this->getRepo(Lead::class)->getLeadsInBetween($start, $end, $user);
        $leadsOut = $this->getRepo(Lead::class)->getLeadsOutBetween($start, $end, $user);

        return $this->render('Lead/leadsInVsLeadsOut.html.twig', ['leadsIn'  => $leadsIn, 'leadsOut' => $leadsOut, 'start'    => $start, 'end'      => $end]);
    }

    public function getAssetJobHistoryReport($id = null, $lifecyclePeriodId = null, $start = null, $end = null): Response
    {
        $message = null;

        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);

        if (!$id) {
            throw $this->createNotFoundException('No asset ID supplied.');
        }

        $asset = $this->getRepo(Asset::class)->findByIdAndDateInBetween($id, $start, $end);

        if (!$asset) {
            if (($start and $start != 'null') && ($end and $end != 'null')) {
                $message =
                    'Asset with ID ' . $id . ' and start_date ' . $start . ' end end_date ' . $end . ' not found.';
            } elseif ($start and $start != 'null') {
                $message =
                    'Asset with ID ' . $id . ' and start_date ' . $start . ' not found.';
            } elseif ($end and $end != 'null') {
                $message =
                    'Asset with ID ' . $id . ' and end_date ' . $end . ' not found.';
            } else {
                $message =
                    'Asset with ID ' . $id . ' not found.';
            }

            $asset = $this->getRepo(Asset::class)->find($id);
        }

        if ($lifecyclePeriodId and $lifecyclePeriodId != 'null') {
            $period = $this->getRepo(AssetLifecyclePeriod::class)->find($lifecyclePeriodId);

            if (!$period) {
                throw $this->createNotFoundException('Asset lifecycle period with ID ' . $lifecyclePeriodId . ' not found.');
            }

            if ($period->getAsset() != $asset) {
                throw $this->createNotFoundException('Asset lifecycle period with ID ' . $lifecyclePeriodId . ' not found.');
            }
        } else {
            $this->getAssetManager()->ensureCurrentLifecyclePeriod($asset);
            $period = $asset->getCurrentLifecyclePeriod();
        }

        return $this->render('Asset/jobHistory.html.twig', [
            'asset'  => $asset,
            'period' => $period,
            'message' => $message,
        ]);
    }

    public function estimatesInVsOutReport($start = null, $end = null): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');
        $estimatesIn  = $this->getRepo(Estimate::class)->getEstimatesInBetween($start, $end);
        $estimatesOut = $this->getRepo(Estimate::class)->getEstimatesOutBetween($start, $end);

        return $this->render('Estimate/estimatesInVsEstimatesOut.html.twig', ['estimatesIn'  => $estimatesIn, 'estimatesOut' => $estimatesOut, 'start'        => $start, 'end'          => $end]);
    }

    public function leadsCreatedOrUpdatedReport(TimeService $timeService, $start = null, $id = null): Response
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id == 'all') {
            $userId   = null;
            $username = null;
        } else {
            $user = $this->getRepo(User::class)->find($id);

            if (!$user) {
                throw new createNotFoundException('A user with ID "' . $id . '" was not found.');
            }

            $userId   = $user->getId();
            $username = $user->getUsername();
        }

        $end          = $timeService->getNewDateTime();
        $leadsIn      = $this->getRepo(Lead::class)->getLeadsInBetween($start, $end->format('Y-m-d H:i:s'), $userId);
        $leadsUpdated = $this->getRepo(Lead::class)->getLeadsUpdatedBetween($start, $end->format('Y-m-d H:i:s'), $userId);

        return $this->render('Lead/leadsCreatedOrUpdated.html.twig', ['leadsIn'      => $leadsIn, 'leadsUpdated' => $leadsUpdated, 'start'        => $start, 'end'          => $end, 'username'     => $username]);
    }

    public function leadsUpdatedBetweenReport($start = null, $end = null, $id = null): Response
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id == 'all') {
            $userId   = null;
            $username = null;
        } else {
            $user = $this->getRepo(User::class)->find($id);

            if (!$user) {
                throw $this->CreateNotFoundException('A user with ID "' . $id . '" was not found.');
            }

            $userId   = $user->getId();
            $username = $user->getUsername();
        }

        $leadsUpdated = $this->getRepo(Lead::class)->getLeadsUpdatedBetween($start, $end, $userId);

        return $this->render('Lead/leadsUpdatedBetween.html.twig', ['leadsUpdated' => $leadsUpdated, 'start'        => $start, 'end'          => $end, 'username'     => $username]);
    }

    public function leadsValueAtQuoting($id = null): Response
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id == 'all') {
            $userId   = null;
            $username = null;
        } else {
            $user = $this->getRepo(User::class)->find($id);

            if (!$user) {
                throw $this->CreateNotFoundException('A user with ID "' . $id . '" was not found.');
            }

            $userId   = $user->getId();
            $username = $user->getUsername();
        }

        $leads = $this->getRepo(Lead::class)->getLeadsValueAtQuoting($userId);
        $total = 0;

        foreach ($leads as $lead) {
            if ($lead['value'] === null || !is_numeric($lead['value'])) {
                $total = null;

                break;
            }
            $total += $lead['value'];
        }

        return $this->render('Lead/LeadsValueAtQuoting.html.twig', ['leads'    => $leads, 'total'    => $total, 'username' => $username]);
    }

    public function leadsWithReminders(TimeService $timeService, $id = null): Response
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id == 'all') {
            $userId   = null;
            $username = null;
        } else {
            $user = $this->getRepo(User::class)->find($id);

            if (!$user) {
                throw $this->CreateNotFoundException('A user with ID "' . $id . '" was not found.');
            }

            $userId   = $user->getId();
            $username = $user->getUsername();
        }

        $leads                      = $this->getRepo(Lead::class)->getLeadsWithReminders($userId);
        $now                        = $timeService->getNewDateTime();
        $leadsWithRemindersInPast   = [];
        $leadsWithRemindersInFuture = [];

        foreach ($leads as $lead) {
            $reminders        = $lead['reminders'];
            $allLeadsInFuture = true;
            foreach ($reminders as $reminder) {
                $date = $reminder['date'];
                if ($date < $now) {
                    array_push($leadsWithRemindersInPast, $lead);
                } else {
                    array_push($leadsWithRemindersInFuture, $lead);
                }
            }
        }

        return $this->render('Lead/LeadsWithReminders.html.twig', ['leadsWithRemindersInPast'   => $leadsWithRemindersInPast, 'leadsWithRemindersInFuture' => $leadsWithRemindersInFuture, 'username'                   => $username]);
    }

    public function leadsInactiveSince($start = null, $id = null, $status = null): Response
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id == 'all') {
            $userId   = null;
            $username = null;
        } else {
            $user = $this->getRepo(User::class)->find($id);

            if (!$user) {
                throw $this->CreateNotFoundException('A user with ID "' . $id . '" was not found.');
            }

            $userId   = $user->getId();
            $username = $user->getUsername();
        }

        $leads = $this->getRepo(Lead::class)->getLeadsInactiveSince($start, $userId, $status);

        return $this->render('Lead/LeadsInactiveSince.html.twig', ['leads'    => $leads, 'username' => $username, 'start'    => $start, 'status'   => $status]);
    }

    public function leadsInVsInWithNoContactReport($start = null, $end = null, $user = null): Response
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($user == 'all') {
            $user = null;
        }

        $leadsIn              = $this->getRepo(Lead::class)->getLeadsInBetween($start, $end, $user);
        $leadsInWithNoContact = $this->getRepo(Lead::class)->getLeadsInWhichStayedAtNoContact($start, $end, $user);

        return $this->render('Lead/leadsInVsLeadsInWithNoContact.html.twig', ['leadsIn'              => $leadsIn, 'leadsInWithNoContact' => $leadsInWithNoContact, 'start'                => $start, 'end'                  => $end]);
    }

    public function getAssetMaintenanceReport(?string $customer = null): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $assets = $this->getRepo(Asset::class)->getAssetWithMaintenanceContractOnly($customer);
        $customers = $this->getRepo(Asset::class)->getCustomerWithMaintenanceContractOnly();

        for ($i = 0; $i < (is_countable($assets) ? count($assets) : 0); ++$i) {
            $specificPeriod = $this->getRepo(AssetLifecyclePeriod::class)->findOneBy([
                'asset' => $assets[$i],
            ]);
            $assets[$i] += ['mcb' => $specificPeriod->getMaintenanceContractBalance()];
        }

        return $this->render('Asset/maintenanceReport.html.twig', [
            'assets'    => $assets,
            'customers' => $customers,
        ]);
    }

    public function getTrucksNotLeasedOrSoldReport(): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');
        $trucks = $this->getRepo(Truck::class)->getTrucksNotLeasedOrSold();

        return $this->render('Truck/trucksNotLeasedOrSoldReport.html.twig', ['trucks' => $trucks]);
    }

    public function getTrucksServicesDueReport(?int $status = null): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');
        $trucks = $this->getRepo(Truck::class)->getTrucksServicesDue($status);

        return $this->render('Truck/trucksServicesDueReport.html.twig', [
            'trucks' => array_values($trucks),
        ]);
    }

    public function getStockLocationReport($id = null): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $location = $this->getRepo(Location::class)->find($id);

        $locationStockItems = $this->getLocationStockItems($location);

        return $this->render('Stock/stockLocation.html.twig', $locationStockItems);
    }

    private function fillJobsArrayWithLabourCost(array $jobs): array
    {
        for ($i = 0; $i < count($jobs); ++$i) {
            $jobs[$i]['getLabourCost'] = $this->getRepo(Job::class)->find($jobs[$i]['id'])->getLabourCost();
        }

        return $jobs;
    }

    private function fillJobsArrayWithTotalJobCost(array $jobs): array
    {
        for ($i = 0; $i < count($jobs); ++$i) {
            $jobs[$i]['getTotalJobCost'] = $this->getRepo(Job::class)->find($jobs[$i]['id'])->getTotalJobCost();
        }

        return $jobs;
    }

    private function getLocationStockItems(Location $location): array
    {
        $data = $this->getRepo(StockItem::class)->getLocationReportData($location);

        $totalStockValue = 0;
        $stockData = [];

        foreach ($data as $supplierStockData) {
            $totalStockValue += $supplierStockData['qty'] * $supplierStockData['costPrice'];

            if (!array_key_exists($supplierStockData['stockId'], $stockData)) {
                $stockData[$supplierStockData['stockId']]['instances'] = 0;
                $stockData[$supplierStockData['stockId']]['totalQuantity'] = 0;
            }

            ++$stockData[$supplierStockData['stockId']]['instances'];
            $stockData[$supplierStockData['stockId']]['totalQuantity'] += $supplierStockData['qty'];
        }

        return [
            'locationId' => $location->getId(),
            'stockItems' => $data,
            'stockData' => $stockData,
            'totalStockValue' => $totalStockValue !== 0 ? '£' . number_format($totalStockValue, 2) : 0,
        ];
    }
}
