<?php

namespace App\Controller;

use App\Entity\Note;
use App\Form\Type\NoteType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class NoteController extends Controller
{
    public function view($id = null): Response
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No Note ID was supplied.');
        }

        $note = $this->getRepo(Note::class)->find($id);

        if (!$note) {
            throw $this->createNotFoundException('Note with ID "' . $id . '" not found.');
        }

        $versions = $this->getVersionRepo()->getLogEntries($note);

        return $this->render('Note/view.html.twig', ['note'     => $note, 'versions' => $versions]);
    }

    public function new(Request $request, EntityManagerInterface $em): JsonResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        $response = new JsonResponse();
        $note  = new Note();

        $form = $this->createForm(NoteType::class, $note);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $note = $form->getData();

            $hr   = date('H');
            $min  = date('i');
            $sec  = date('s');
            $date = $note->getDate();
            $date->setTime($hr, $min, $sec);
            $note->setDate($date);

            $em->persist($note);
            $em->flush();

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The note "' . $note->getId() . '" was successfully created.');

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Note/_form.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('new_note'), 'isEdit'    => false, 'note'      => $note])]);

        return $response;
    }

    public function edit(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No Note ID was supplied.');
        }

        $response = new JsonResponse();
        $note = $this->getRepo(Note::class)->find($id);

        if (!$note) {
            throw $this->createNotFoundException('Note with ID "' . $id . '" not found.');
        }

        $storedDate    = $note->getDate();
        $storedDateYmd = $note->getDate()->format('Y-m-d');

        $showReason = !$note->getAsset() && !$note->getAssetLifecyclePeriod();

        $form = $this->createForm(NoteType::class, $note, [
            'includeReason' => $showReason,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $note = $form->getData();

            $newDateYmd = $note->getDate()->format('Y-m-d');
            if ($newDateYmd == $storedDateYmd) {
                $note->setDate($storedDate);
            } else {
                $hr   = date('H');
                $min  = date('i');
                $sec  = date('s');
                $date = $note->getDate();
                $date->setTime($hr, $min, $sec);
                $note->setDate($date);
            }

            $em->persist($note);
            $em->flush();

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The Note "' . $note->getId() . '" was successfully edited.');

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Note/_form.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('edit_note', ['id' => $id]), 'isEdit'    => true, 'note'      => $note])]);

        return $response;
    }

    public function delete(EntityManagerInterface $em, $id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No Note ID was supplied.');
        }

        $note = $this->getRepo(Note::class)->find($id);

        if (!$note) {
            throw $this->createNotFoundException('Note with ID "' . $id . '" not found.');
        }

        $em->remove($note);
        $em->flush();

        $this->setFlash('success', 'The Note "' . $note->getId() . '" was successfully deleted.');

        return $this->redirect($this->generateUrl('index'));
    }
}
