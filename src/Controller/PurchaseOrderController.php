<?php

namespace App\Controller;

use App\Entity\EstimateItem;
use App\Entity\Job;
use App\Entity\Location;
use App\Entity\PurchaseOrder;
use App\Entity\PurchaseOrderItem;
use App\Entity\SalesJob;
use App\Entity\StockItem;
use App\Entity\StockTransaction;
use App\Entity\Supplier;
use App\Form\Type\DMHEPurchaseOrderType;
use App\Form\Type\EstimateItemType;
use App\Form\Type\OrderDateType;
use App\Form\Type\PurchaseOrderItemType;
use App\Form\Type\PurchaseOrderType;
use App\Model\TransactionCategory;
use App\Time\TimeService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PurchaseOrderController extends Controller
{
    public function list($page = 1): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $purchaseOrders = $this->getRepo(PurchaseOrder::class)->getAllNotArchived();

        return $this->render('PurchaseOrder/list.html.twig', ['purchaseOrders' => $purchaseOrders]);
    }

    public function listDMHE($page = 1): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $purchaseOrders = $this->getRepo(PurchaseOrder::class)->getAllDMHENotArchived();

        return $this->render('DMHEPurchaseOrder/list.html.twig', [
            'purchaseOrders' => $purchaseOrders,
        ]);
    }

    public function listDMHEStock($page = 1): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $purchaseOrders = $this->getRepo(PurchaseOrder::class)->getAllDMHEStockNotArchived();

        return $this->render('DMHEStock/list.html.twig', [
            'purchaseOrders' => $purchaseOrders,
        ]);
    }

    public function view(?string $id): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if (!$id) {
            throw $this->createNotFoundException('No purchase order ID supplied.');
        }

        $purchaseOrder = $this->getRepo(PurchaseOrder::class)->find($id);

        if (!$purchaseOrder) {
            throw $this->createNotFoundException('Purchase order with ID ' . $id . ' not found.');
        }

        $versions = $this->getVersionRepo()->getLogEntries($purchaseOrder);

        return $this->render('PurchaseOrder/view.html.twig', ['purchaseOrder' => $purchaseOrder, 'versions' => $versions]);
    }

    public function purchaseOrderParticulars($id = null): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $purchaseOrder = $this->getRepo(PurchaseOrder::class)->find($id);

        return $this->render('PurchaseOrder/particulars.html.twig', ['purchaseOrder' => $purchaseOrder]);
    }

    public function purchaseOrderDMHEParticulars($id = null): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $purchaseOrder = $this->getRepo(PurchaseOrder::class)->find($id);

        return $this->render('DMHEPurchaseOrder/particulars.html.twig', [
            'purchaseOrder' => $purchaseOrder,
        ]);
    }

    public function purchaseOrderDMHEStockParticulars($id = null): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $purchaseOrder = $this->getRepo(PurchaseOrder::class)->find($id);

        return $this->render('DMHEStock/particulars.html.twig', [
            'purchaseOrder' => $purchaseOrder,
        ]);
    }

    public function new(Request $request, EntityManagerInterface $em, $jobId = null): JsonResponse
    {
        $response = new JsonResponse();
        $purchaseOrder = new PurchaseOrder();
        $engineer = $this->isGranted('ROLE_SERVICE_ADMIN') ? false : true;

        if ($jobId) {
            $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);
            $job = $this->getRepo(Job::class)->find($jobId);
            $purchaseOrder->setJob($job);
        } else {
            $this->securityCheck('ROLE_SERVICE_ADMIN');
        }

        $form = $this->createForm(PurchaseOrderType::class, $purchaseOrder, ['isEngineer' => $engineer]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $purchaseOrder = $form->getData();
            $job = $purchaseOrder->getJob();

            if ($job) {
                $purchaseOrder->setCustomer($job->getCustomer());
            } else {
                $purchaseOrder->setCustomer(null);
            }

            $purchaseOrderItems = $purchaseOrder->getPurchaseOrderItems();

            foreach ($purchaseOrderItems as $purchaseOrderItem) {
                $sItem = $purchaseOrderItem->getItem();
                if (!$sItem) {
                    $this->setFlash('error', 'Item Required');
                }

                $unitCost = $sItem->getCostPrice();
                $unitIssue = $sItem->getIssuePrice();

                $purchaseOrderItem->setUnitCost($unitCost);
                $purchaseOrderItem->setUnitIssue($unitIssue);

                $purchaseOrderItem->setPurchaseOrder($purchaseOrder);
            }

            if ($purchaseOrder->getEngineerCollection()) {
                $purchaseOrder->setOrderDate(new \DateTime());
            }

            $em->persist($purchaseOrder);
            $em->flush();

            if ($job && !$engineer) {
                $unassign = $form->has('unassign') ? $form->get('unassign')->getData() : false;

                if ($unassign) {
                    $job->removeAllEngineersFromJob();
                }

                $this->getJobManager()->saveJob($job);
            }

            $this->setFlash('success', 'The purchase order "' . $purchaseOrder->getId() . '" has been successfully added');

            $responseData = ['success' => true, 'isEdit' => false, 'displayForm' => false, 'refresh' => true];

            if ($purchaseOrder->getEngineerCollection()) {
                $responseData['refresh'] = false;
                $responseData['confirm'] = ['message'      => 'Do you want to check in all items of this order?', 'url'          => $this->generateUrl('check_in_all_po_items', ['id' => $purchaseOrder->getId()])];
            }

            $response->setData($responseData);

            return $response;
        }

        $supplierCodes = $this->getRepo(Supplier::class)->getCodes();
        $supplierItems = $this->getRepo(PurchaseOrder::class)->getSupplierItems();

        $formRoute = $jobId ? $this->generateUrl('new_purchaseorder', ['jobId' => $jobId]) : $this->generateUrl('new_purchaseorder');

        $response->setData(['success' => true, 'displayForm' => true, 'refresh' => false, 'html' => $this->renderView('PurchaseOrder/_form.html.twig', ['form' => $form->createView(), 'formRoute' => $formRoute, 'isEdit' => false, 'purchaseOrder' => $purchaseOrder, 'items' => null, 'supplierCodes' => json_encode($supplierCodes, JSON_THROW_ON_ERROR), 'supplierItems' => json_encode($supplierItems, JSON_THROW_ON_ERROR)])]);

        return $response;
    }

    public function newDMHE(Request $request)
    {
        $response = new JsonResponse();
        $purchaseOrder = new PurchaseOrder();
        $em = $this->getEntityManager();
        $jobId = $request->query->get('jobId');

        if ($jobId) {
            $job = $this->getRepo(SalesJob::class)->find($jobId);
            $purchaseOrder->setSalesJob($job);
        }

        $form = $this->createForm(DMHEPurchaseOrderType::class, $purchaseOrder);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $purchaseOrder = $form->getData();
            $purchaseOrder->setType(PurchaseOrder::TYPE_DMHE);

            $purchaseOrderItems = $purchaseOrder->getPurchaseOrderItems();

            foreach ($purchaseOrderItems as $purchaseOrderItem) {
                $sItem = $purchaseOrderItem->getItem();
                if (!$sItem) {
                    $this->setFlash('error', 'Item Required');
                }

                $unitCost = $sItem->getCostPrice();
                $unitIssue = $sItem->getIssuePrice();

                $purchaseOrderItem->setUnitCost($unitCost);
                $purchaseOrderItem->setUnitIssue($unitIssue);

                $purchaseOrderItem->setPurchaseOrder($purchaseOrder);
            }

            $em->persist($purchaseOrder);
            $em->flush();

            $this->setPurchaseOrderCode($purchaseOrder);

            $this->setFlash('success', 'The DMHE purchase order "' . $purchaseOrder->getId() . '" has been successfully added');

            $responseData = [
                'success' => true,
                'isEdit' => false,
                'displayForm' => false,
                'refresh' => true,
            ];

            $response->setData($responseData);

            return $response;
        }

        $supplierCodes = $this->getRepo(Supplier::class)->getCodes();
        $supplierItems = $this->getRepo(PurchaseOrder::class)->getSupplierItems();

        $formRoute = $this->generateUrl('new_dmhe_purchaseorder');

        $response->setData([
            'success' => true,
            'displayForm' => true,
            'refresh' => false,
            'html' => $this->renderView('DMHEPurchaseOrder/_form.html.twig', [
                'form' => $form->createView(),
                'formRoute' => $formRoute,
                'isEdit' => false,
                'purchaseOrder' => $purchaseOrder,
                'items' => null,
                'supplierCodes' => json_encode($supplierCodes, JSON_THROW_ON_ERROR),
                'supplierItems' => json_encode($supplierItems, JSON_THROW_ON_ERROR),
                'existsSalesJob' => (bool) $purchaseOrder->getSalesJob(),
            ]),
        ]);

        return $response;
    }

    public function edit(Request $request, EntityManagerInterface $em, TimeService $timeService, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $response = new JsonResponse();
        $purchaseOrder = $this->getRepo(PurchaseOrder::class)->find($id);

        if (!$purchaseOrder) {
            $response->setData(['success' => false, 'displayForm' => false, 'refresh' => false, 'error' => 'The purchase order with ID "' . $id . '" was not found.']);

            return $response;
        }

        $storedPurchaseOrderItems = new ArrayCollection();
        foreach ($purchaseOrder->getPurchaseOrderItems() as $purchaseOrderItem) {
            $storedPurchaseOrderItems->add($purchaseOrderItem);
        }

        $storedEstimateItems = new ArrayCollection();
        foreach ($purchaseOrder->getEstimateItems() as $estimateItem) {
            $storedEstimateItems->add($estimateItem);
        }

        if ($job = $purchaseOrder->getJob()) {
            $this->getJobManager()->saveJob($job);
        }

        $form = $this->createForm(PurchaseOrderType::class, $purchaseOrder, ['isEngineer' => false]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $purchaseOrder = $form->getData();
            $formItems = $purchaseOrder->getPurchaseOrderItems();
            $estimateItems = $purchaseOrder->getEstimateItems();

            if ($job = $purchaseOrder->getJob()) {
                $purchaseOrder->setCustomer($job->getCustomer());
            } else {
                $purchaseOrder->setCustomer(null);
            }

            foreach ($storedPurchaseOrderItems as $storedPurchaseOrderItem) {
                if (!$purchaseOrder->getPurchaseOrderItems()->contains($storedPurchaseOrderItem)) {
                    $purchaseOrder->removePurchaseOrderItem($storedPurchaseOrderItem);
                    $em->remove($storedPurchaseOrderItem);
                }
            }

            foreach ($storedEstimateItems as $storedEstimateItem) {
                if (!$purchaseOrder->getEstimateItems()->contains($storedEstimateItem)) {
                    $purchaseOrder->removeEstimateItem($storedEstimateItem);
                    $em->remove($storedEstimateItem);
                }
            }

            foreach ($formItems as $formItem) {
                $stockItem = $formItem->getItem();

                if (!$stockItem) {
                    $response->setData(['success' => false, 'displayForm' => false, 'refresh' => false, 'error' => 'Item Required.']);

                    return $response;
                }

                $unitCost = $stockItem->getCostPrice();
                $unitIssue = $stockItem->getIssuePrice();
                $formItem->setUnitCost($unitCost);
                $formItem->setUnitIssue($unitIssue);

                $formItem->setPurchaseOrder($purchaseOrder);
            }

            $orderComplete = true;

            foreach ($formItems as $item) {
                if (!$item->getReceivedDate()) {
                    $orderComplete = false;

                    break;
                }
            }

            foreach ($estimateItems as $estimateItem) {
                if (!$estimateItem->getReceivedDate()) {
                    $orderComplete = false;

                    break;
                }
            }

            if ($orderComplete) {
                $purchaseOrder->setCompleted(1);
                $now = $timeService->getNewDateTime();
                $purchaseOrder->setCompletedAt($now);
            }

            $em->persist($purchaseOrder);
            $em->flush();

            $response->setData(['success' => true, 'displayForm' => false, 'refresh' => true]);

            $this->setFlash('success', 'The purchase order item "' . $purchaseOrder->getId() . '" has been successfully edited');

            return $response;
        }

        $itemData = [];
        $items = null;

        foreach ($purchaseOrder->getPurchaseOrderItems() as $item) {
            $stock = $item->getItem()->getStock();
            $items[$item->getItem()->getId()] = ['description' => $stock->getDescription(), 'code' => $stock->getCode(), 'unitCost' => $item->getUnitCost(), 'quantity' => $item->getQuantity()];
        }

        $supplierCodes = $this->getRepo(Supplier::class)->getCodes();
        $supplierItems = $this->getRepo(PurchaseOrder::class)->getSupplierItems();

        $response->setData([
            'success' => true,
            'displayForm' => true,
            'isEdit' => true,
            'refresh' => false,
            'html' => $this->renderView('PurchaseOrder/_form.html.twig', [
                'form' => $form->createView(),
                'formRoute' => $this->generateUrl('edit_purchaseorder', ['id' => $id]),
                'purchaseOrder' => $purchaseOrder,
                'isEdit' => true,
                'items' => json_encode($items, JSON_THROW_ON_ERROR),
                'supplierCodes' => json_encode($supplierCodes, JSON_THROW_ON_ERROR),
                'supplierItems' => json_encode($supplierItems, JSON_THROW_ON_ERROR),
            ]),
        ]);

        return $response;
    }

    public function editDMHE(Request $request, $id = null)
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $response = new JsonResponse();
        $purchaseOrder = $this->getRepo(PurchaseOrder::class)->find($id);

        if (!$purchaseOrder) {
            $response->setData([
                'success' => false,
                'displayForm' => false,
                'refresh' => false,
                'error' => 'The purchase order with ID "' . $id . '" was not found.',
            ]);

            return $response;
        }

        $storedPurchaseOrderItems = new ArrayCollection();
        foreach ($purchaseOrder->getPurchaseOrderItems() as $purchaseOrderItem) {
            $storedPurchaseOrderItems->add($purchaseOrderItem);
        }

        $storedEstimateItems = new ArrayCollection();
        foreach ($purchaseOrder->getEstimateItems() as $estimateItem) {
            $storedEstimateItems->add($estimateItem);
        }

        $form = $this->createForm(DMHEPurchaseOrderType::class, $purchaseOrder);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $purchaseOrder = $form->getData();
            $em = $this->getEntityManager();
            $formItems = $purchaseOrder->getPurchaseOrderItems();
            $estimateItems = $purchaseOrder->getEstimateItems();
            $purchaseOrder->setCustomer(null);

            foreach ($storedPurchaseOrderItems as $storedPurchaseOrderItem) {
                if (!$purchaseOrder->getPurchaseOrderItems()->contains($storedPurchaseOrderItem)) {
                    $purchaseOrder->removePurchaseOrderItem($storedPurchaseOrderItem);
                    $em->remove($storedPurchaseOrderItem);
                }
            }

            foreach ($storedEstimateItems as $storedEstimateItem) {
                if (!$purchaseOrder->getEstimateItems()->contains($storedEstimateItem)) {
                    $purchaseOrder->removeEstimateItem($storedEstimateItem);
                    $em->remove($storedEstimateItem);
                }
            }

            foreach ($formItems as $formItem) {
                $stockItem = $formItem->getItem();

                if (!$stockItem) {
                    $response->setData([
                        'success' => false,
                        'displayForm' => false,
                        'refresh' => false,
                        'error' => 'Item Required.',
                    ]);

                    return $response;
                }

                $unitCost = $stockItem->getCostPrice();
                $unitIssue = $stockItem->getIssuePrice();
                $formItem->setUnitCost($unitCost);
                $formItem->setUnitIssue($unitIssue);

                $formItem->setPurchaseOrder($purchaseOrder);
            }

            $orderComplete = true;

            foreach ($formItems as $item) {
                if (!$item->getReceivedDate()) {
                    $orderComplete = false;

                    break;
                }
            }

            foreach ($estimateItems as $estimateItem) {
                if (!$estimateItem->getReceivedDate()) {
                    $orderComplete = false;

                    break;
                }
            }

            if ($orderComplete) {
                $purchaseOrder->setCompleted(1);
                $timeService = $this->get('v42.time_service');
                $now = $timeService->getNewDateTime();
                $purchaseOrder->setCompletedAt($now);
            }

            $em->persist($purchaseOrder);
            $em->flush();

            $response->setData([
                'success' => true,
                'displayForm' => false,
                'refresh' => true,
            ]);

            $this->setFlash('success', 'The DMHE purchase order item "' . $purchaseOrder->getId() . '" has been successfully edited');

            return $response;
        }

        $itemData = [];
        $items = null;

        foreach ($purchaseOrder->getPurchaseOrderItems() as $item) {
            $stock = $item->getItem()->getStock();
            $items[$item->getItem()->getId()] = [
                'description' => $stock->getDescription(),
                'code' => $stock->getCode(),
                'unitCost' => $item->getUnitCost(),
                'quantity' => $item->getQuantity(),
            ];
        }

        $supplierCodes = $this->getRepo(Supplier::class)->getCodes();
        $supplierItems = $this->getRepo(PurchaseOrder::class)->getSupplierItems();

        $response->setData([
            'success' => true,
            'displayForm' => true,
            'isEdit' => true,
            'refresh' => false,
            'html' => $this->renderView('DMHEPurchaseOrder/_form.html.twig', [
                'form' => $form->createView(),
                'formRoute' => $this->generateUrl('edit_dmhe_purchaseorder', ['id' => $id]),
                'purchaseOrder' => $purchaseOrder,
                'isEdit' => true,
                'items' => json_encode($items, JSON_THROW_ON_ERROR),
                'supplierCodes' => json_encode($supplierCodes, JSON_THROW_ON_ERROR),
                'supplierItems' => json_encode($supplierItems, JSON_THROW_ON_ERROR),
                'existsSalesJob' => (bool) $purchaseOrder->getSalesJob(),
            ]),
        ]);

        return $response;
    }

    public function delete(EntityManagerInterface $em, $id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $purchaseOrder = $this->getRepo(PurchaseOrder::class)->find($id);

        if (!$purchaseOrder) {
            throw $this->createNotFoundException('The purchase order with ID "' . $id . '" was not found.');
        }

        if ($purchaseOrder->getJob()) {
            $job = $purchaseOrder->getJob();

            $this->getJobManager()->saveJob($job);
        }

        $em->remove($purchaseOrder);
        $em->flush();

        $this->setFlash('success', 'Purchase Order "' . $id . '" was successfully deleted');

        return $this->redirect($this->generateUrl('list_purchaseorders'));
    }

    public function deletePurchaseOrderItem(Request $request, EntityManagerInterface $em, $id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $purchaseOrderItem = $this->getRepo(PurchaseOrderItem::class)->find($id);

        if (!$purchaseOrderItem) {
            throw $this->createNotFoundException('The purchase order item with ID "' . $id . '" was not found.');
        }

        $job = $purchaseOrderItem->getPurchaseOrder()->getJob();
        $purchaseOrder = $purchaseOrderItem->getPurchaseOrder();

        $em->remove($purchaseOrderItem);
        $em->flush();

        $purchaseOrder = $this->getRepo(PurchaseOrder::class)->find($purchaseOrder);
        if ((is_countable($purchaseOrder->getEstimateItems()) ? is_countable($purchaseOrder->getEstimateItems()) ? is_countable($purchaseOrder->getEstimateItems()) ? is_countable($purchaseOrder->getEstimateItems()) ? count($purchaseOrder->getEstimateItems()) : 0 : 0 : 0 : 0) == 0 && (is_countable($purchaseOrder->getPurchaseOrderItems()) ? is_countable($purchaseOrder->getPurchaseOrderItems()) ? is_countable($purchaseOrder->getPurchaseOrderItems()) ? is_countable($purchaseOrder->getPurchaseOrderItems()) ? count($purchaseOrder->getPurchaseOrderItems()) : 0 : 0 : 0 : 0) == 0) {
            $em->remove($purchaseOrder);
            $em->flush();
            $this->setFlash('success', 'Purchase Order "' . $purchaseOrder->getId() . '" was successfully deleted');
        }

        if ($job) {
            $this->getJobManager()->saveJob($job);
        }

        $this->setFlash('success', 'Purchase Order item"' . $id . '" was successfully deleted');

        return $this->redirect($request->headers->get('referer'));
    }

    public function deletePurchaseOrderSpecialItem(Request $request, EntityManagerInterface $em, $id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $estimateItem = $this->getRepo(EstimateItem::class)->find($id);

        if (!$estimateItem) {
            throw $this->createNotFoundException('The purchase order special item with ID "' . $id . '" was not found.');
        }

        $job = $estimateItem->getPurchaseOrder()->getJob();

        $em->remove($estimateItem);
        $em->flush();

        if ($job) {
            $this->getJobManager()->saveJob($job);
        }

        $this->setFlash('success', 'Purchase Order Special Item "' . $id . '" was successfully deleted');

        return $this->redirect($request->headers->get('referer'));
    }

    public function purchaseOrderItemArrival(Request $request, EntityManagerInterface $em, TimeService $timeService, $id = null): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $purchaseOrderItem = $this->getRepo(PurchaseOrderItem::class)->find($id);

        if (!$purchaseOrderItem) {
            throw $this->createNotFoundException('The purchase order item with ID "' . $id . '" was not found.');
        }

        $purchaseOrderItem->setReceivedDate($timeService->getNewDateTime());

        $response = new JsonResponse();
        $form = $this->createForm(PurchaseOrderItemType::class, $purchaseOrderItem, ['includeLocation' => true]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $purchaseOrderItem = $form->getData();

            $em->persist($purchaseOrderItem);
            $em->flush();

            $purchaseOrder = $purchaseOrderItem->getPurchaseOrder();
            $job = $purchaseOrder->getJob();
            $quantity = $purchaseOrderItem->getQuantity();
            $supplierStock = $purchaseOrderItem->getItem();
            $stock = $supplierStock->getStock();
            $orderComplete = $purchaseOrder->getOrderComplete();
            $location = $form->get('location')->getData();

            if (!$location) {
                $form->get('location')->addError(new FormError('Required'));
            }

            $stockTransaction = new StockTransaction();

            $stockTransaction->setQuantity($quantity);
            $stockTransaction->setPurchaseOrder($purchaseOrder);
            $stockTransaction->setSupplierStock($supplierStock);
            $stockTransaction->setStock($stock);

            $stockTransaction->setTransactionCategory(TransactionCategory::IN_PURCHASE_ORDER);

            $em->persist($stockTransaction);

            for ($i = 0; $i < $quantity; ++$i) {
                $stockItem = new StockItem();
                $stockItem->setStock($stock);
                $stockItem->setSupplierStock($supplierStock);
                $stockItem->setLocation($location);

                if ($job) {
                    $stockItem->setJob($job);
                }

                $em->persist($stockItem);
            }

            if ($orderComplete) {
                $purchaseOrder->setCompleted(1);
                $purchaseOrder->setCompletedAt($timeService->getNewDateTime());

                if ($job) {
                    $em->persist($job);
                }

                $em->persist($purchaseOrder);
            }

            $em->flush();

            if ($job) {
                $this->getJobManager()->saveJob($job);
            }

            $this->setFlash('success', 'The item "' . $supplierStock->getDisplayString() . '" was checked in for purchase order "' . $purchaseOrder->getId() . '"');

            if ($location->getIsMainStock() === true && !$job && !$stock->getBin()) {
                return $this->redirect($this->generateUrl('stock_set_bin', ['id' => $stock->getId()]));
            }

            $response->setData(['success' => true, 'isEdit' => false, 'displayForm' => false, 'refresh' => true]);

            return $response;
        }

        $response->setData(['success' => false, 'displayForm' => true, 'refresh' => false, 'html' => $this->renderView('PurchaseOrder/_itemDeliveryForm.html.twig', ['form' => $form->createView(), 'formRoute' => $this->generateUrl('check_in_item_arrival', ['id' => $id]), 'purchaseOrderItem' => $purchaseOrderItem])]);

        return $response;
    }

    public function specialItemArrival(Request $request, EntityManagerInterface $em, TimeService $timeService, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $estimateItem = $this->getRepo(EstimateItem::class)->find($id);

        if (!$estimateItem) {
            throw $this->createNotFoundException('The estimate item with ID "' . $id . '" was not found.');
        }

        $response = new JsonResponse();
        $form = $this->createForm(EstimateItemType::class, $estimateItem);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $estimateItem = $form->getData();

            $em->persist($estimateItem);
            $em->flush();

            $purchaseOrder = $estimateItem->getPurchaseOrder();
            $job = $purchaseOrder->getJob();
            $orderComplete = $purchaseOrder->getOrderComplete();

            if ($orderComplete) {
                $purchaseOrder->setCompleted(1);
                $purchaseOrder->setCompletedAt($timeService->getNewDateTime());

                $job = $purchaseOrder->getJob();

                if ($job) {
                    $em->persist($job);
                }

                $em->persist($purchaseOrder);
                $em->flush();
            }

            if ($job) {
                $this->getJobManager()->saveJob($job);
            }

            $response->setData(['success' => true, 'isEdit' => false, 'displayForm' => false, 'refresh' => true]);

            $this->setFlash('success', 'The item "' . $estimateItem->getItem() . '" was checked in for purchase order "' . $purchaseOrder->getId() . '"');

            return $response;
        }

        $response->setData(['success' => false, 'displayForm' => true, 'refresh' => false, 'html' => $this->renderView('PurchaseOrder/_specialItemDeliveryForm.html.twig', ['form' => $form->createView(), 'formRoute' => $this->generateUrl('check_in_special_item_arrival', ['id' => $id]), 'estimateItem' => $estimateItem])]);

        return $response;
    }

    public function setOrderDate(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);

        $purchaseOrder = $this->getRepo(PurchaseOrder::class)->find($id);
        $engineer = $this->isGranted('ROLE_SERVICE_ADMIN') ? false : true;

        if (!$purchaseOrder) {
            throw $this->createNotFoundException('The purchase order with ID "' . $id . '" was not found.');
        }

        $response = new JsonResponse();
        $form = $this->createForm(OrderDateType::class, $purchaseOrder, ['isEngineer' => $engineer]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $purchaseOrder = $form->getData();

            $em->persist($purchaseOrder);

            if ($purchaseOrder->getJob() && !$engineer) {
                $job = $purchaseOrder->getJob();

                $unassign = $form->has('unassign') ? $form->get('unassign')->getData() : false;

                if ($unassign) {
                    $job->removeAllEngineersFromJob();
                    $job->setAccepted(0);
                    $job->setIsLive(0);
                }

                $this->getJobManager()->saveJob($job);
            }

            $em->flush();

            $response->setData(['success' => true, 'isEdit' => false, 'displayForm' => false, 'refresh' => true]);

            $this->setFlash('success', 'The order date for "' . $purchaseOrder->getId() . '" was successfully set.');

            return $response;
        }

        $response->setData(['success' => false, 'displayForm' => true, 'refresh' => false, 'html' => $this->renderView('PurchaseOrder/_itemOrderForm.html.twig', ['form' => $form->createView(), 'formRoute' => $this->generateUrl('set_purchaseorder_order_date', ['id' => $id]), 'purchaseOrder' => $purchaseOrder])]);

        return $response;
    }

    public function print(?string $id): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if (!$id) {
            throw $this->createNotFoundException('No purchase order ID supplied.');
        }

        $purchaseOrder = $this->getRepo(PurchaseOrder::class)->find($id);
        if (!$purchaseOrder) {
            throw $this->createNotFoundException('Purchase order with ID ' . $id . ' not found.');
        }

        return $this->render('PurchaseOrder/printPurchaseOrder.html.twig', ['purchaseOrder' => $purchaseOrder]);
    }

    public function checkInAllItems($id, EntityManagerInterface $em, TimeService $timeService): RedirectResponse
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);

        if (!$id) {
            $this->setFlash('error', 'No Purchase Order Id was provided.');

            throw new \Exception('No Purchase Order Id was provided.');
        }

        /** @var PurchaseOrder $purchaseOrder */
        $purchaseOrder = $this->getRepo(PurchaseOrder::class)->find($id);
        if (!$purchaseOrder) {
            $this->setFlash('error', 'No Purchase Order Id was provided.');

            return $this->redirect($this->generateUrl('list_jobs'));
        }

        /** @var Job $job */
        $job = $purchaseOrder->getJob();
        if (!$job) {
            $this->setFlash('error', 'No Job was provided.');

            return $this->redirect($this->generateUrl('list_jobs'));
        }

        $now = $timeService->getNewDateTime();

        $workLogs = $job->getWorkLogs();
        $location = null;
        if ($workLogs) {
            $lastWorkLog = end($workLogs);
            $location = $lastWorkLog->getUser()->getVan();
        } elseif ($job->getEngineer()) {
            $location = $job->getEngineer()->getvan();
        }
        if ($this->isGranted('ROLE_ENGINEER') && !$this->isGranted('ROLE_SERVICE_ADMIN')) {
            $location = $this->getCurrentUser()->getVan();
        }

        if (!$location) {
            $location = $this->getRepo(Location::class)->findOneBy(['isMainStock' => true]);
            if (!$location) {
                $this->setFlash('error', 'Main stock not found.');

                return $this->redirect($this->generateUrl('list_jobs') . '?selectedItem=' . $job->getId());
            }
        }
        /** @var PurchaseOrderItem $purchaseOrderItem */
        foreach ($purchaseOrder->getPurchaseOrderItems() as $purchaseOrderItem) {
            $purchaseOrderItem->setReceivedDate($now);
            $em->persist($purchaseOrderItem);

            $quantity = $purchaseOrderItem->getQuantity();
            $supplierStock = $purchaseOrderItem->getItem();
            $stock = $supplierStock->getStock();
            $orderComplete = $purchaseOrder->getOrderComplete();

            $stockTransaction = new StockTransaction();

            $stockTransaction->setQuantity($quantity);
            $stockTransaction->setPurchaseOrder($purchaseOrder);
            $stockTransaction->setSupplierStock($supplierStock);
            $stockTransaction->setStock($stock);

            $stockTransaction->setTransactionCategory(TransactionCategory::IN_PURCHASE_ORDER);

            $em->persist($stockTransaction);

            for ($i = 0; $i < $quantity; ++$i) {
                $stockItem = new StockItem();
                $stockItem->setStock($stock);
                $stockItem->setSupplierStock($supplierStock);
                $stockItem->setLocation($location);

                if ($job) {
                    $stockItem->setJob($job);
                }

                $em->persist($stockItem);
            }

            if ($orderComplete) {
                $purchaseOrder->setCompleted(1);
                $purchaseOrder->setCompletedAt($now);

                if ($job) {
                    $em->persist($job);
                }

                $em->persist($purchaseOrder);
            }

            if ($job) {
                $this->getJobManager()->saveJob($job, true);
            }
        }

        foreach ($purchaseOrder->getEstimateItems() as $item) {
            $job = $purchaseOrder->getJob();

            $item->setReceivedDate($now);
            $item->setJob($job);
            $em->persist($item);

            $orderComplete = $purchaseOrder->getOrderComplete();

            if ($orderComplete) {
                $purchaseOrder->setCompleted(1);
                $purchaseOrder->setCompletedAt($now);

                if ($job) {
                    $em->persist($job);
                }

                $em->persist($purchaseOrder);
            }

            if ($job) {
                $this->getJobManager()->saveJob($job);
            }
        }

        $em->flush();

        $this->setFlash('success', 'All items for purchase order "' . $purchaseOrder->getId() . '" has been successfully checked in.');

        return $this->redirect($this->generateUrl('list_jobs') . '?selectedItem=' . $job->getId());
    }

    public function archive(EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $purchaseOrder = $this->getRepo(PurchaseOrder::class)->find($id);

        if (!$purchaseOrder) {
            throw $this->createNotFoundException('The purchase order with ID "' . $id . '" was not found.');
        }

        $purchaseOrder->setArchived(true);
        $purchaseOrder->setReinstated(false);

        $em->persist($purchaseOrder);
        $em->flush();

        $this->setFlash('success', 'Purchase Order "' . $id . '" was successfully archived');

        return new JsonResponse(['success' => true]);
    }

    public function reinstate(EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $purchaseOrder = $this->getRepo(PurchaseOrder::class)->find($id);

        if (!$purchaseOrder) {
            throw $this->createNotFoundException('The purchase order with ID "' . $id . '" was not found.');
        }

        $purchaseOrder->setArchived(false);
        $purchaseOrder->setReinstated(true);

        $em->persist($purchaseOrder);
        $em->flush();

        $this->setFlash('success', 'Purchase Order "' . $id . '" was successfully re-instated');

        return new JsonResponse(['success' => true]);
    }

    public function listArchived($page = 1): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $purchaseOrders = $this->getRepo(PurchaseOrder::class)->getArchivedPurchaseOrdersPaged($page);

        return $this->render('PurchaseOrder/listArchived.html.twig', ['purchaseOrders' => $purchaseOrders]);
    }

    public function searchArchive(Request $request): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $content = $request->getContent();
        $searchTerm = json_decode($content, true, 512, JSON_THROW_ON_ERROR);
        $purchaseOrders = $this->getRepo(PurchaseOrder::class)->getArchivedPurchaseOrdersWhere($searchTerm);

        $response = new JsonResponse();
        $response->setData(['purchaseOrders' => $purchaseOrders, 'searchTerm' => $searchTerm]);

        return $response;
    }

    private function setPurchaseOrderCode(PurchaseOrder $purchaseOrder): void
    {
        if ($purchaseOrder->getType() === PurchaseOrder::TYPE_DMHE) {
            $code =  PurchaseOrder::TYPE_DMHE . '-' . $purchaseOrder->getId();
        } else {
            $code =  PurchaseOrder::TYPE_DFS . '-' . $purchaseOrder->getId();
        }

        $purchaseOrder->setCode($code);

        $em = $this->getEntityManager();
        $em->persist($purchaseOrder);
        $em->flush();
    }
}
