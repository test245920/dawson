<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Entity\Customer;
use App\Entity\Role;
use App\Entity\Supplier;
use App\Entity\User;
use App\Form\Type\ContactAccountType;
use App\Form\Type\ContactType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

class ContactController extends Controller
{
    public function list(): Response
    {
        $this->securityCheck('ROLE_SALES_ADMIN');
        $customers = $this->getRepo(Customer::class)->getCustomersWithContacts();
        $suppliers = $this->getRepo(Supplier::class)->getSuppliersWithContacts();
        $contacts  = $this->getRepo(Contact::class)->findAll();

        return $this->render('Contact/list.html.twig', ['customers' => $customers, 'suppliers' => $suppliers, 'contacts'  => $contacts]);
    }

    public function view($id = null): Response
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No Contact ID was supplied.');
        }

        $contact = $this->getRepo(Contact::class)->find($id);

        if (!$contact) {
            throw $this->createNotFoundException('Contact with ID "' . $id . '" not found.');
        }

        $versions = $this->getVersionRepo()->getLogEntries($contact);

        return $this->render('Contact/view.html.twig', ['contact'    => $contact, 'versions' => $versions]);
    }

    public function new(Request $request, EntityManagerInterface $em): JsonResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        $response = new JsonResponse();
        $contact  = new Contact();

        $form = $this->createForm(ContactType::class, $contact, ['withCustomer' => true]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contact = $form->getData();

            $newCustomer = $form['newCustomer']->getData() != '';
            if ($newCustomer) {
                $customer    = new Customer();
                $newCustName = $form['newCustomer']->getData();
                $customer->setName($newCustName);
                $customer->setIsActive(0);
                $em->persist($customer);
                $customer->setActiveAt(null);
                $em->flush();
                $contact->setCustomer($customer);
            }

            $em->persist($contact);
            $em->flush();

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The contact "' . $contact->getName() . '" was successfully created.');

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => true, 'html'        => $this->renderView('Contact/_form.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('new_contact'), 'isEdit'    => false, 'contact'   => $contact])]);

        return $response;
    }

    public function edit(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No Contact ID was supplied.');
        }

        $response = new JsonResponse();
        $contact  = $this->getRepo(Contact::class)->find($id);

        if (!$contact) {
            throw $this->createNotFoundException('Contact with ID "' . $id . '" not found.');
        }

        $form = $this->createForm(ContactType::class, $contact, ['withCustomer' => true]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contact = $form->getData();

            $newCustomer = $form['newCustomer']->getData() != '';
            if ($newCustomer) {
                $customer    = new Customer();
                $newCustName = $form['newCustomer']->getData();
                $customer->setName($newCustName);
                $customer->setIsActive(0);
                $em->persist($customer);
                $em->flush();
                $contact->setCustomer($customer);
            }

            $em->persist($contact);
            $em->flush();

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The Contact "' . $contact->getName() . '" was successfully edited.');

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Contact/_form.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('edit_contact', ['id' => $id]), 'isEdit'    => true, 'contact'     => $contact])]);

        return $response;
    }

    public function delete(EntityManagerInterface $em, $id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SUPER_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No Contact ID was supplied.');
        }

        $contact = $this->getRepo(Contact::class)->find($id);

        if (!$contact) {
            throw $this->createNotFoundException('Contact with ID "' . $id . '" not found.');
        }

        $em->remove($contact);
        $em->flush();

        $this->setFlash('success', 'The Contact "' . $contact->getName() . '" was successfully deleted.');

        return $this->redirect($this->generateUrl('list_contacts'));
    }

    public function getParticulars($id = null): Response
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        $contact = $this->getRepo(Contact::class)->find($id);

        if (!$contact) {
            throw new \RuntimeException('Page Not Found');
        }

        return $this->render('Contact/particulars.html.twig', ['contact' => $contact]);
    }

    public function createAccount(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SALES_ADMIN');

        $response = new JsonResponse();
        $contact  = $this->getRepo(Contact::class)->find($id);

        $email         = $contact ? $contact->getEmail() : null;
        $name          = $contact ? $contact->getName() : null;
        $lowercaseName = strtolower((string) $name);
        $formattedName = str_replace(' ', '.', $lowercaseName);

        $customer = $contact ? $contact->getCustomer() : null;
        $supplier = $contact ? $contact->getSupplier() : null;

        if ($customer) {
            $customerName          = $customer->getName();
            $lowercaseCustomerName =  strtolower((string) $customerName);
            $formattedCustomerName = str_replace(' ', '.', $lowercaseCustomerName);
            $username              = $formattedName . '-' . $formattedCustomerName;
        } elseif ($supplier) {
            $supplierName          = $supplier->getName();
            $lowercaseSupplierName =  strtolower((string) $supplierName);
            $formattedSupplierName = str_replace(' ', '.', $lowercaseSupplierName);
            $username              = $formattedName . '-' . $formattedSupplierName;
        } else {
            $username = $formattedName;
        }

        $user = new User();
        $user->setName($name);
        $user->setUsername($username);

        if ($email) {
            $user->setEmail($email);
        }

        $form = $this->createForm(ContactAccountType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user              = $form->getData();
            $defaultEncoder = new MessageDigestPasswordEncoder('sha512', true, 5000);
            $encoders = [
                User::class => $defaultEncoder,
            ];
            $encoderFactory = new EncoderFactory($encoders);
            $encoder           = $encoderFactory->getEncoder($user);
            $shaPassword       = hash('sha256', uniqid(random_int(0, mt_getrandmax()), true));
            $bcrypted_password = $encoder->encodePassword($shaPassword, $user->getSalt());
            $user->setPassword($bcrypted_password);

            $customerRole = $this->getRepo(Role::class)->findOneByRole('ROLE_CUSTOMER');
            $user->addRole($customerRole);

            $user->setContact($contact);

            $token      = hash('sha256', uniqid(random_int(0, mt_getrandmax()), true));
            $url        = $this->generateAbsoluteUrl('set_password', ['token' => $token]);
            $expiryDate = new \DateTime('+ 7 days');

            $user->setPasswordResetToken($token);
            $user->setPasswordResetTokenExpiry($expiryDate);

            $em->persist($user);
            $em->flush();

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $currentUser = $this->getCurrentUser();

            $body = $this->renderView('Contact/_welcomeEmail.html.twig', ['name'       => $user->getName(), 'username'   => $user->getUsername(), 'email'      => $user->getEmail(), 'url'        => $url, 'expiryDate' => $expiryDate]);

            $email = $this->getEmailManager()->sendEmail('Welcome to your new Dawson Forklift Services account.', $body, $currentUser->getEmail(), true);

            $this->setFlash('success', 'The user "' . $user->getUsername() . '" has been successfully added');

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Contact/_newUserAccountForm.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('create_user_account_for_contact', ['id' => $id]), 'contact'   => $contact])]);

        return $response;
    }
}
