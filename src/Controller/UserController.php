<?php

namespace App\Controller;

use App\Entity\Job;
use App\Entity\Location;
use App\Entity\StockItem;
use App\Entity\User;
use App\Form\Type\UserType;
use App\Manager\RoleManager;
use App\Time\TimeService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class UserController extends Controller
{
    public function view(?string $id): Response
    {
        $this->securityCheck('ROLE_SUPER_ADMIN');

        if (!$id) {
            throw $this->createNotFoundException('No user id supplied.');
        }

        $user = $this->getRepo(User::class)->find($id);

        if (!$user) {
            throw $this->createNotFoundException('User with id "' . $id . '"" not found.');
        }

        $versions = $this->getVersionRepo()->getLogEntries($user);

        return $this->render('User/view.html.twig', ['user'     => $user, 'versions' => $versions]);
    }

    public function new(Request $request, RoleManager $roleManager, EncoderFactoryInterface $encoderFactory): JsonResponse
    {
        $this->securityCheck('ROLE_SUPER_ADMIN');

        $response = new JsonResponse();

        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user              = $form->getData();

            $encoder          = $encoderFactory->getEncoder($user);
            $bcryptedPassword = $encoder->encodePassword($form->get('plainPassword')->getData(), $user->getSalt());

            $user->setPassword($bcryptedPassword);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            if ($roleManager->doesUserHaveRole($user, 'ROLE_ENGINEER') && !$this->get('velocity42.manager.role_manager')->doesUserHaveRole($user, 'ROLE_SUPER_ADMIN')) {
                $this->createVanIfRequired($user);
            }

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $body = $this->renderView('User/_signupEmail.html.twig', ['username' => $user->getUsername(), 'email'    => $user->getEmail()]);

            $currentUser = $this->getCurrentUser();
            $email       = $this->getEmailManager()->sendEmail('Welcome to your new Dawson Forklift Services account.', $body, $user->getEmail(), true);

            $this->setFlash('success', 'The user "' . $user->getUsername() . '" has been successfully added');

            return $response;
        }

        $response->setData(['success'     => true, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('User/_form.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('new_user')])]);

        return $response;
    }

    public function list($page = 1): Response
    {
        $this->securityCheck('ROLE_SUPER_ADMIN');

        $pager = $this->getRepo(User::class)->getUsersPaged($page);
        $users = $pager->getCurrentPageResults();

        return $this->render('User/list.html.twig', ['pager' => $pager, 'users' => $users]);
    }

    public function edit(Request $request, EntityManagerInterface $em, RoleManager $roleManager, EncoderFactoryInterface $encoderFactory, $id = null): Response
    {
        $this->securityCheck('ROLE_SUPER_ADMIN');

        $response = new JsonResponse();
        /** @var User $user */
        $user     = $this->getRepo(User::class)->find($id);

        if (!$user) {
            $response->setData(['success'     => false, 'displayForm' => false, 'refresh'     => false]);

            $this->setFlash('error', 'The user with id "' . $id . '" was not found.');

            return $response;
        }

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();

            if ($user->getPlainPassword() !== null) {
                $encoder          = $encoderFactory->getEncoder($user);
                $bcryptedPassword = $encoder->encodePassword($form->get('plainPassword')->getData(), $user->getSalt());

                $user->setPassword($bcryptedPassword);
            }

            $em->persist($user);
            $em->flush();

            if ($roleManager->doesUserHaveRole($user, 'ROLE_ENGINEER') && !$this->get('velocity42.manager.role_manager')->doesUserHaveRole($user, 'ROLE_SUPER_ADMIN')) {
                $this->createVanIfRequired($user);
            }

            if (!$roleManager->doesUserHaveRole($user, 'ROLE_ENGINEER') && $van = $user->getVan()) {
                if (sizeof($van->getStockItems())) {
                    return $this->redirect($this->generateUrl('select_stock_location', ['id' => $van->getId()]));
                }
                if (sizeof($jobs = $user->getJobs())) {
                    /** @var Job $job */
                    foreach ($jobs as $job) {
                        $job->removeEngineer($user);
                        if (!sizeof($job->getEngineer())) {
                            $job->setCompletionStage(1);
                        }
                        $em->persist($job);
                    }
                }
                $em->remove($van);
                $em->flush();
            }

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The user "' . $user->getUsername() . '" has been successfully edited');

            return $response;
        }

        $response->setData(['success'     => true, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('User/_form.html.twig', ['form' => $form->createView(), 'formRoute' => $this->generateUrl('edit_user', ['id' => $id])])]);

        return $response;
    }

    public function delete(EntityManagerInterface $em, $id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SUPER_ADMIN');

        /** @var User $user */
        $user = $this->getRepo(User::class)->find($id);

        if (!$user) {
            throw $this->createNotFoundException('The user with ID"' . $id . '" was not found.');
        }

        /** @var Location $van */
        if ($van = $user->getVan()) {
            $stockItems = $van->getStockItems();
            if (sizeof($stockItems)) {
                $location = $this->getRepo(Location::class)->findOneBy(['isMainStock' => true]);
                /** @var StockItem $stockItem */
                foreach ($stockItems as $stockItem) {
                    $stockItem->setLocation($location);
                    $em->persist($stockItem);
                }
                $em->flush();
            }
        }

        $em->remove($user);
        $em->flush();

        $this->setFlash('success', 'User "' . $user->getUsername() . '" was successfully deleted');

        return $this->redirect($this->generateUrl('list_users'));
    }

    public function passwordResetEmail(TimeService $timeService, $id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SUPER_ADMIN');

        $user = $this->getRepo(User::class)->find($id);

        if (!$user) {
            throw $this->createNotFoundException('The user with ID"' . $id . '" was not found.');
        }

        $token = hash('sha256', uniqid(random_int(0, mt_getrandmax()), true));
        $url   = $this->generateAbsoluteUrl('reset_password', ['token' => $token]);

        $user->setPasswordResetToken($token);
        $user->setPasswordResetTokenExpiry($timeService->getNewDateTime('+ 2 days'));

        $body = $this->renderView('User/_passwordResetEmail.html.twig', ['username' => $user->getUsername(), 'email'    => $user->getEmail(), 'url'      => $url]);

        $this->getEmailManager()->sendEmail('Password reset link for your Dawson Forklift Services account.', $body, $user->getEmail(), true);

        $this->setFlash('success', 'The password reset email for "' . $user->getUsername() . '" has been successfully sent.');

        return $this->redirect($this->generateUrl('view_user', ['id' => $user->getId()]));
    }

    private function createVanIfRequired($user, EntityManagerInterface $em): void
    {
        /** @var Location $van */
        if ($van = $user->getVan()) {
            if ($van->getDeletedAt() !== null) {
                $van->setDeletedAt(null);
                $em->persist($van);
                $em->flush();
                $user->setVan($van);
                $em->persist($user);
                $em->flush();
            }

            return;
        }

        $van = new Location();
        $van->setEngineer($user);

        $em->persist($van);
        $em->flush();
        $user->setVan($van);
        $em->persist($user);
        $em->flush();
    }
}
