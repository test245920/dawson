<?php

namespace App\Controller;

use App\Entity\Asset;
use App\Entity\AssetLifecyclePeriod;
use App\Entity\AttachedFile;
use App\Entity\Contact;
use App\Entity\Customer;
use App\Entity\Estimate;
use App\Entity\EstimateItem;
use App\Entity\Invoice;
use App\Entity\Job;
use App\Entity\Note;
use App\Entity\PurchaseOrder;
use App\Entity\SalesJob;
use App\Entity\Signature;
use App\Entity\Stock;
use App\Entity\StockItem;
use App\Entity\StockTransaction;
use App\Entity\SupplierStock;
use App\Entity\Truck;
use App\Entity\TruckMake;
use App\Entity\TruckModel;
use App\Entity\WorkLog;
use App\Form\Type\AssignEngineerType;
use App\Form\Type\AssignStockToJobType;
use App\Form\Type\ChargeableJobType;
use App\Form\Type\JobDeleteReasonType;
use App\Form\Type\JobImagesType;
use App\Form\Type\JobSignatureType;
use App\Form\Type\JobStatusType;
use App\Form\Type\JobType;
use App\Form\Type\PossibleAdditionalWorkType;
use App\Form\Type\SalesJobType;
use App\Form\Type\SetStockUsedOnJobType;
use App\Form\Type\WorkLogType;
use App\Manager\InvoiceManager;
use App\Model\ChargeMethod;
use App\Model\JobStage;
use App\Model\RecurringType;
use App\Model\SalesJobRequest;
use App\Model\TransactionCategory;
use App\Time\TimeService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class JobController extends Controller
{
    public function view(?string $id, EntityManagerInterface $em): Response
    {
        $em->getFilters()->disable('softdeleteable');

        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);

        if (!$id) {
            throw $this->createNotFoundException('No job ID supplied');
        }

        $job = $this->getRepo(Job::class)->find($id);

        if (!$job) {
            throw $this->createNotFoundException('Job with ID "' . $id . '" not found.');
        }

        $versions = $this->getVersionRepo()->getLogEntries($job);

        $em->getFilters()->enable('softdeleteable');

        return $this->render('Job/view.html.twig', ['job'      => $job, 'versions' => $versions]);
    }

    public function list(): Response
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);
        if ($this->isGranted('ROLE_ENGINEER') && !$this->isGranted('ROLE_SUPER_ADMIN')) {
            $jobs = $this->getRepo(Job::class)->getJobsForListing($this->getCurrentUser());
        } else {
            $jobs = $this->getRepo(Job::class)->getJobsForListing();
        }

        $jobManager = $this->getJobManager();
        $priorities = $jobManager->getJobPriorityForListingsJobs($jobs);
        $signatureStatuses = $jobManager->getJobSignatureForListingsJobs($jobs);
        $customers  = $this->getRepo(Customer::class)->getTruckManagerInfo();

        return $this->render('Job/list.html.twig', ['jobs'       => $jobs, 'customers'  => $customers, 'priorities' => $priorities, 'signatureStatuses' => $signatureStatuses]);
    }

    public function listSalesJob(): Response
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);

        $jobs = $this->getRepo(SalesJob::class)->getGroupByStatus();

        return $this->render('SalesJob/list.html.twig', [
            'jobs' => $jobs,
        ]);
    }

    public function new(Request $request, EntityManagerInterface $em): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $response = new JsonResponse();
        $job      = new Job();
        $assetId  = $request->query->get('asset');
        $salesJobId  = $request->query->get('sales_job');
        $customer = $this->getRepo(Customer::class)->findOneBy(['name' => Job::DEFAULT_SALES_JOB]);
        $includeCustomer = true;
        $includeAsset = true;
        $includeSalesJob = false;

        if ($assetId) {
            $asset = $this->getRepo(Asset::class)->find($assetId);
            if ($asset) {
                $this->getAssetManager()->ensureCurrentLifecyclePeriod($asset);

                $job->setAssetLifecyclePeriod($asset->getCurrentLifecyclePeriod());
                $job->setCustomer($asset->getCurrentLifecyclePeriod()->getCustomer());
                $job->setSiteAddress($asset->getCurrentLifecyclePeriod()->getSiteAddress());

                $includeCustomer = false;
                $includeAsset = false;
            }
        }

        if ($salesJobId) {
            $includeSalesJob = true;
            $salesJob = $this->getRepo(SalesJob::class)->find($salesJobId);
            $asset = $salesJob->getTruck() ? $this->getRepo(Asset::class)->find($salesJob->getTruck()->getId()) : null;
            $job->setSalesJob($salesJob);
            if ($asset) {
                $this->getAssetManager()->ensureCurrentLifecyclePeriod($asset);

                $job->setAssetLifecyclePeriod($asset->getCurrentLifecyclePeriod());
                $job->setCustomer($customer);
                $job->setSiteAddress($asset->getCurrentLifecyclePeriod()->getSiteAddress());
            }
        }

        $em   = $this->getEntityManager();
        $form = $this->createForm(
            JobType::class,
            $job,
            [
                'includeCustomer' => $includeCustomer,
                'includeAsset' => $includeAsset,
            ],
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $job = $form->getData();

            $salesJob = $form->get('salesJob')->getData();

            if ($salesJob) {
                $job->setCustomer($customer);
                $asset = $salesJob->getTruck() ? $this->getRepo(Asset::class)->find($salesJob->getTruck()->getId()) : null;
                $job->setSalesJob($salesJob);
                if ($asset) {
                    $this->getAssetManager()->ensureCurrentLifecyclePeriod($asset);

                    $job->setAssetLifecyclePeriod($asset->getCurrentLifecyclePeriod());
                    $job->setSiteAddress($asset->getCurrentLifecyclePeriod()->getSiteAddress());
                }
            }
            if ($job->getIsRecurring() == true) {
                $date = new \DateTime();
                $dateModifyFrom = $job->getRecurringType() === RecurringType::WEEKLY ? ($date->format('D') == 'Mon') ? $date : $date->modify('previous monday') : $date->modify('first day of this month');
                $job->setJobActiveFrom($dateModifyFrom);
                $dateRecurring = new \DateTime();
                $dateModify = $job->getRecurringType() === RecurringType::WEEKLY ? $dateRecurring->modify('this sunday') : $dateRecurring->modify('last day of this month');
                $job->setJobRecurringTime($dateModify);
            } else {
                $job->setRecurringType(null);
            }

            $newContacts = $form['newContacts']->getData();
            foreach ($newContacts as $newContact) {
                $contact = new Contact();
                $name    = $newContact->getName();

                if ($name == '') {
                    continue;
                }
                $contact->setName($name);

                $contact->setTelephone($newContact->getTelephone());
                $contact->setEmail($newContact->getEmail());
                $contact->setCustomer($job->getCustomer());
                $contact->setJobTitle($newContact->getJobTitle());
                $contact->setMobile($newContact->getMobile());
                $contact->addJob($job);

                $job->addContact($contact);
                $em->persist($contact);
            }

            $images = $job->getImages();

            foreach ($images as $image) {
                $image->setPath(sprintf(Job::PATH, $job->getUuid()));
            }

            $this->saveJob($job, $em);

            $em->flush();

            $url = $this->generateUrl('list_jobs', ['selectedItem' => $job->getId()]);

            if ($request->get('assign')) {
                $url = $this->generateUrl('list_jobs', ['selectedItem' => $job->getId(), 'assign' => true]);
            }

            $response = new JsonResponse(['success'     => true, 'displayForm' => false, 'refresh'     => true, 'url'         => $url]);

            $this->setFlash('success', 'The job "' . $job->getJobCode() . '" has been successfully added');

            return $response;
        }

        $response->setData([
            'success'     => true,
            'displayForm' => true,
            'refresh'     => false,
            'html'        => $this->renderView('Job/_form.html.twig', [
                'form'            => $form->createView(),
                'formRoute'       => $this->generateUrl('new_job', $assetId ? ['asset' => $assetId] : []),
                'job'             => $job,
                'includeSalesJob' => $includeSalesJob,
            ]),
        ]);

        return $response;
    }

    public function newSalesJob(Request $request)
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $response = new JsonResponse();
        $job      = new SalesJobRequest();
        $em   = $this->getEntityManager();
        $form = $this->createForm(SalesJobType::class, $job);
        $showCustomer = $request->query->get('showCustomer') ?? false;

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $salesJob   = $form->getData();
            $newMake    = $salesJob->newMake;
            $newModel   = $salesJob->newModel;
            $truckMake  = null;
            $noteDetail = $salesJob->detail;
            $truck      = $this->getAssetManager()->preparedTruckAsset(new Truck(), $salesJob);

            if ($newMake !== null) {
                $truckMake = new TruckMake();
                $truckMake->setName($newMake);
                $em->persist($truckMake);
            }

            if (!empty($newModel)) {
                $truckModel = new TruckModel();
                $truckModel->setName($newModel);
                if ($truckMake) {
                    $truckModel->setMake($truckMake);
                } else {
                    $truckModel->setMake($form['make']->getData());
                }
                $em->persist($truckModel);
                $truck->setModel($truckModel);
            }

            if ($noteDetail) {
                $note = new Note();

                $hr   = date('H');
                $min  = date('i');
                $sec  = date('s');
                $date = $form['date']->getData();
                $date->setTime($hr, $min, $sec);
                $note->setDate($date);

                $note->setAsset($truck);
                $note->setDetail($form['detail']->getData());
                $em->persist($note);
            }

            $customer = $form['customer']->getData();
            $clientFleet = $form['clientFleet']->getData();

            if ($customer) {
                $period = new AssetLifecyclePeriod($truck);
                $period->setCustomer($customer);
                $period->setClientFleet($clientFleet);
                $period->setStart(new \DateTime());
                $period->setType(AssetLifecyclePeriod::PERIOD_TYPE_CUSTOMER_OWNED);

                $contacts = $form['contacts']->getData();
                foreach ($contacts as $contact) {
                    $contact->addAssetLifecyclePeriod($period);
                    $em->persist($contact);
                    $period->addContact($contact);
                }
                $em->persist($period);
                $em->flush();

                $truck->setCurrentLifecyclePeriod($period);
            }

            $this->getAssetManager()->saveAsset($truck);

            $salesJob = new SalesJob();
            $salesJob->setStatus(SalesJob::STATUS_INCOMPLETE);
            $salesJob->setTruck($truck);
            $salesJob->setModel($truck->getModel()->getName());
            $salesJob->setMake($truck->getModel()->getMake()->getName());
            $salesJob->setSerial($truck->getSerial());
            $em->persist($salesJob);
            $em->flush();

            $this->getJobManager()->setSalesJobNumber($salesJob);

            $url = $this->generateUrl('list_sales_jobs', ['selectedItem' => $salesJob->getId()]);

            $response = new JsonResponse([
                'success'     => true,
                'displayForm' => false,
                'refresh'     => true,
                'url'         => $url,
            ]);

            $this->setFlash('success', 'The sales job "' . $salesJob->getId() . '" has been successfully added');

            return $response;
        }

        $response->setData([
            'success'      => true,
            'displayForm'  => true,
            'refresh'      => false,
            'showCustomer' => $showCustomer,
            'html'         => $this->renderView('SalesJob/_form.html.twig', [
                'form'      => $form->createView(),
                'formRoute' => $this->generateUrl('new_sales_job'),
            ]),
        ]);

        return $response;
    }

    public function getSalesJob($id = null)
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN']);

        if ($id === null) {
            throw $this->createNotFoundException('No sales job ID supplied');
        }

        $salesJob = $this->getRepo(SalesJob::class)->findOneById($id);

        $response = new JsonResponse();

        return $response->setData([
            'salesJob' => $salesJob,
        ]);
    }

    public function jobParticulars(EntityManagerInterface $em, $id = null): Response
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);

        if ($id === null) {
            throw $this->createNotFoundException('No job ID supplied');
        }

        $em->getFilters()->disable('softdeleteable');
        $job = $this->getRepo(Job::class)->find($id);

        if (!$job) {
            throw $this->createNotFoundException('Job  with ID "' . $id . '" not found.');
        }

        $returnedStock = $this->getRepo(StockTransaction::class)->getStockReturnedFromJob($job);
        $stageForm     = $this->createForm(JobStatusType::class, ['stage' => $job->getCompletionStage()]);
        $assignedToJob = false;
        $currentUser   = $this->getCurrentUser();

        foreach ($job->getEngineers() as $engineer) {
            if ($engineer == $currentUser) {
                $assignedToJob = true;
            }
        }

        $em->getFilters()->enable('softdeleteable');

        return $this->render('Job/particulars.html.twig', ['job' => $job, 'returnedStock' => $returnedStock, 'assignedToJob' => $assignedToJob, 'stageForm' => $stageForm->createView(), 'buttons' => $this->isGranted('ROLE_SERVICE_ADMIN'), 'completedIdx' => JobStage::COMPLETE]);
    }

    public function salesJobParticulars($id = null): Response
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN']);

        if ($id === null) {
            throw $this->createNotFoundException('No job ID supplied');
        }
        $em = $this->getEntityManager();

        $em->getFilters()->disable('softdeleteable');
        $job = $this->getRepo(SalesJob::class)->find($id);

        if (!$job) {
            throw $this->createNotFoundException('Job  with ID "' . $id . '" not found.');
        }

        $em->getFilters()->enable('softdeleteable');

        return $this->render('SalesJob/particulars.html.twig', [
            'job'           => $job,
            'buttons'       => $this->isGranted('ROLE_SERVICE_ADMIN'),
        ]);
    }

    public function changeStatusSalesJob($id = null, $status = null): RedirectResponse
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN']);

        if ($id === null) {
            throw $this->createNotFoundException('No job ID supplied');
        }
        $em = $this->getEntityManager();

        $job = $this->getRepo(SalesJob::class)->find($id);

        if (!$job) {
            throw $this->createNotFoundException('Job  with ID "' . $id . '" not found.');
        }

        $job->setStatus((int) $status);
        $em->persist($job);
        $em->flush();

        $this->setFlash('success', 'The sales job "' . $job->getId() . '" has been changed status');

        return $this->redirect($this->generateUrl('list_sales_jobs'));
    }

    public function unlinkOrderFromSalesJob($id = null): RedirectResponse
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN']);

        if ($id === null) {
            throw $this->createNotFoundException('No job ID supplied');
        }
        $em = $this->getEntityManager();

        $job = $this->getRepo(SalesJob::class)->find($id);

        if (!$job) {
            throw $this->createNotFoundException('Job  with ID "' . $id . '" not found.');
        }

        $purchaseOrders = $job->getPurchaseOrders();

        foreach ($purchaseOrders as $purchaseOrder) {
            $job->removePurchaseOrder($purchaseOrder);
            $em->persist($job);
        }

        $em->remove($job);
        $em->flush();

        $this->setFlash('success', 'Purchase Order unlinked');

        return $this->redirect($this->generateUrl('list_sales_jobs'));
    }

    public function unlinkServiceJobFromSalesJob($id = null): RedirectResponse
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN']);

        if ($id === null) {
            throw $this->createNotFoundException('No job ID supplied');
        }
        $em = $this->getEntityManager();

        $job = $this->getRepo(Job::class)->find($id);

        if (!$job) {
            throw $this->createNotFoundException('Job  with ID "' . $id . '" not found.');
        }
        $em->remove($job);
        $em->flush();

        $this->setFlash('success', 'Purchase Order unlinked');

        return $this->redirect($this->generateUrl('list_sales_jobs'));
    }

    public function edit(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $response = new JsonResponse();

        $job = $this->getRepo(Job::class)->find($id);

        if (!$job) {
            $response->setData(['success'     => false, 'displayForm' => false, 'refresh'     => false]);

            $this->setFlash('error', 'The job with ID "' . $id . '" was not found.');

            return $response;
        }

        $storedImages = new ArrayCollection();
        foreach ($job->getImages() as $image) {
            $storedImages->add($image);
        }

        $form = $this->createForm(JobType::class, $job, ['includeCustomer' => false, 'includeAsset' => true]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $job = $form->getData();

            if ($job->getIsRecurring() == true) {
                $date = new \DateTime();
                $dateModifyFrom = $job->getRecurringType() === RecurringType::WEEKLY ? ($date->format('D') == 'Mon') ? $date : $date->modify('previous monday') : $date->modify('first day of this month');
                $job->setJobActiveFrom($dateModifyFrom);
                $dateRecurring = new \DateTime();
                $dateModify = $job->getRecurringType() === RecurringType::WEEKLY ? $dateRecurring->modify('this sunday') : $dateRecurring->modify('last day of this month');
                $job->setJobRecurringTime($dateModify);
            } else {
                $job->setRecurringType(null);
            }

            $newContacts = $form['newContacts']->getData();
            foreach ($newContacts as $newContact) {
                $contact = new Contact();
                $contact->setName($newContact->getName());
                $contact->setTelephone($newContact->getTelephone());
                $contact->setEmail($newContact->getEmail());
                $contact->setCustomer($job->getCustomer());
                $contact->setJobTitle($newContact->getJobTitle());
                $contact->setMobile($newContact->getMobile());

                $contact->addJob($job);
                $job->addContact($contact);
                $em->persist($contact);
                $em->flush();
            }

            foreach ($storedImages as $storedImage) {
                if ($job->getImages()->contains($storedImage) === false) {
                    $storedImage->setJob(null);
                    $em->persist($storedImage);
                    $job->removeImage($storedImage);
                }
            }

            $images = $job->getImages();
            foreach ($images as $image) {
                $image->setPath(sprintf(job::PATH, $job->getUuid()));
                $image->setJob($job);
            }

            $this->saveJob($job, $em);

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The job "' . $job->getJobCode() . '" has been successfully edited');

            return $response;
        }

        $response->setData([
            'success'     => true,
            'displayForm' => true,
            'refresh'     => false,
            'isEdit'      => true,
            'html'        => $this->renderView('Job/_form.html.twig', [
                'form'            => $form->createView(),
                'formRoute'       => $this->generateUrl('edit_job', ['id' => $id]),
                'job'             => $job,
                'includeSalesJob' => false,
            ]),
        ]);

        return $response;
    }

    public function delete(EntityManagerInterface $em, $id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        /** @var Job $job */
        $job = $this->getRepo(Job::class)->find($id);

        if (!$job) {
            throw $this->createNotFoundException('The job with ID"' . $id . '" was not found.');
        }

        if ($items = $job->getAttachedStockItems()) {
            /** @var StockItem $item */
            foreach ($items as $item) {
                if ($item->getJob() && !$item->getUsedOnJob()) {
                    $item->setJob(null);
                    $em->persist($item);
                }
                if ($item->getUsedOnJob()) {
                    $em->remove($item);
                }
            }
            $em->flush();
        }

        if ($purchaseOrders = $job->getPurchaseOrders()) {
            /** @var PurchaseOrder $po */
            foreach ($purchaseOrders as $po) {
                $po->setJob(null);
                $em->persist($po);
            }

            $em->flush();
        }

        if ($stockTransactions = $job->getStockTransactions()) {
            foreach ($stockTransactions as $st) {
                $st->setJob(null);
                $em->persist($st);
            }

            $em->flush();
        }

        $this->getJobManager()->removeJob($job);

        $this->setFlash('success', 'Job "' . $job->getJobCode() . '" was successfully deleted');

        return $this->redirect($this->generateUrl('list_jobs'));
    }

    public function searchByTruck(): Response
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');
        $trucks = $this->getRepo(Truck::class)->getActiveListings();

        return $this->render('Job/searchByTruck.html.twig', ['trucks' => $trucks]);
    }

    public function getJobsForCustomer($id = null): JsonResponse
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);

        if ($id === null) {
            throw $this->createNotFoundException('No customer ID supplied.');
        }

        $customer = $this->getRepo(Customer::class)->find($id);

        if (!$customer) {
            throw $this->createNotFoundException('Customer not found.');
        }

        $jobs = $this->getRepo(Job::class)->findJobsForCustomer($id);

        $response = new JsonResponse();

        return $response->setData(['jobs'       => $jobs, 'labourRate' => $customer->getLabourRate()]);
    }

    public function assignEngineer(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No Job ID was supplied.');
        }

        $job = $this->getRepo(Job::class)->find($id);

        if (!$job) {
            throw $this->createNotFoundException('No job with ID "' . $id . '" was found.');
        }

        $response = new JsonResponse();
        $form     = $this->createForm(AssignEngineerType::class, $job);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $engineers = $form['engineers']->getData();

            foreach ($engineers as $engineer) {
                $job->addEngineer($engineer);
            }

            $job->setIsLive(1);
            $job->setAccepted(1);
            $job->setIsLive(1);

            $engineersAssignedToJob = $job->getEngineers();

            if ($job->getAttachedJobs()) {
                foreach ($job->getAttachedJobs() as $attachedJob) {
                    foreach ($engineersAssignedToJob as $engineer) {
                        $attachedJob->addEngineer($engineer);
                        $em->persist($attachedJob);
                        $engineers .= $engineer->getUsername();
                    }
                }
            }

            $this->saveJob($job, $em);

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $engineerNames = [];
            foreach ($engineersAssignedToJob as $engineer) {
                $engineerNames[] = $engineer->getUsername();
            }

            if (sizeof($job->getEngineers()->toArray()) > 1) {
                $this->setFlash('success', 'The engineers "' . implode(', ', $engineerNames) . '" were assigned to job "' . $job->getJobCode() . '".');
            } else {
                $this->setFlash('success', 'The engineer "' . implode(', ', $engineerNames) . '" was assigned to job "' . $job->getJobCode() . '".');
            }

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Job/_assignEngineerToJobform.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('assign_engineer_to_job', ['id' => $id]), 'job'       => $job])]);

        return $response;
    }

    public function logWork(Request $request, EntityManagerInterface $em, $id = null): Response
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);

        if ($id === null) {
            throw $this->createNotFoundException('No job ID was supplied');
        }

        /** @var Job $job */
        $job = $this->getRepo(Job::class)->find($id);

        if (!$job) {
            throw $this->createNotFoundException('The Job with ID "' . $id . '" was not found.');
        }

        if ($job->getSignedOff()) {
            throw $this->createNotFoundException('The Job with ID "' . $id . '" has already been signed off and can longer have work logged to it.');
            $this->setFlash('error', 'The Job with ID "' . $id . '" has already been signed off and can longer have work logged to it.');
        }

        $engineer = $this->isGranted('ROLE_ENGINEER') && !$this->isGranted('ROLE_SUPER_ADMIN') ? true : false;

        $jobDetailLabel = $job->getUrgent() ? 'Fault reported' : 'Job detail';
        $jobDetail      = $job->getDetail();
        $response       = new JsonResponse();
        $workLog        = new WorkLog($job, $engineer);
        $currentUser    = $this->getCurrentUser();

        if ($engineer) {
            $workLog->setUser($currentUser);
        } else {
            $workLog->allowNoTimeLogged = true;
        }

        $isFirstWorkLog = sizeof($job->getWorkLogs()) == 0;

        $form = $this->createForm(WorkLogType::class, $workLog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $workLog = $form->getData();

            if ($workLog->getUser() && $workLog->getUser()->getCostMultiplier() != null) {
                $workLog->setUserCostMultiplier($workLog->getUser()->getCostMultiplier());
            }

            $workLog->calculateAndSetLabourRates();

            $em->persist($workLog);
            $em->flush();

            $truck = $job->getTruck();
            $hourReading = $workLog->getHourReading();

            if ($hourReading) {
                $job->setHourReading($hourReading);
                $em->persist($job);
                $em->flush();
            }

            if ($workLog->getRequestedParts()) {
                $job->setUrgent(1);

                if ($workLog->unassignJob) {
                    $job->removeAllEngineersFromJob();
                }

                $this->saveJob($job, $em);
            }

            if ($workLog->postponeJob) {
                $this->getJobManager()->postponeJob($job);
                $this->saveJob($job, $em);
            } else {
                if ($workLog->completeJob) {
                    $workLogs = $job->getWorkLogs();
                    $engineers = $job->getEngineers();
                    $engineersRequiringLogs = [];

                    foreach ($engineers as $engineer) {
                        if ($engineer->getId() == $workLog->getUser()->getId()) {
                            continue;
                        }

                        $engineerWorkLogs = array_filter($workLogs, fn ($workLog): bool => $workLog->getUser()->getId() == $engineer->getId());

                        if (!count((array) $engineerWorkLogs)) {
                            $engineersRequiringLogs[] = $engineer;
                        }
                    }

                    if (count($engineersRequiringLogs)) {
                        $response->setData(['success'     => false, 'displayForm' => false, 'refresh'     => true, 'html'        => $this->renderView('Job/_jobRequiresWorklogs.html.twig', ['engineers' => $engineersRequiringLogs])]);

                        return $response;
                    }

                    $em->persist($job);
                    $em->flush();

                    if ($workLog->assetLifecyclePeriod) {
                        $job->setAssetLifecyclePeriod($workLog->assetLifecyclePeriod);
                    }

                    $job->setCompleted(1);
                    $job->setCompletedAt($workLog->getStartDate());

                    $this->saveJob($job, $em);

                    $this->setFlash('success', 'The time labour "' . $workLog->getTimeLabour(true) . '" and time travel "' . $workLog->getTimeTravel(true) . '" has been successfully logged on job "' . $job->getId() . '".');

                    $url = $job->getPartsRequisitions() ? 'job_confirm_special_items' : 'set_stock_used_on_job';

                    return $this->redirect($this->generateUrl($url, ['id' => $job->getId()]));
                }
                $this->setFlash('success', 'The time labour "' . $workLog->getTimeLabour(true) . '" and time travel "' . $workLog->getTimeTravel(true) . '" has been successfully assigned on job "' . $job->getId() . '".');
                $url = $job->getPartsRequisitions() ? 'job_confirm_special_items' : 'set_stock_used_on_job';

                return $this->redirect($this->generateUrl($url, ['id' => $id]));
            }

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The time labour "' . $workLog->getTimeLabour(true) . '" and time travel "' . $workLog->getTimeTravel(true) . '" has been successfully assigned on job "' . $job->getId() . '".');

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Job/_workLogForm.html.twig', ['form'           => $form->createView(), 'formRoute'      => $this->generateUrl('log_work', ['id' => $id]), 'job'            => $job, 'workLog'        => $workLog, 'jobDetail'      => $jobDetail, 'jobDetailLabel' => $jobDetailLabel, 'isFirstWorkLog' => $isFirstWorkLog, 'isAdmin'        => $this->isGranted('ROLE_SERVICE_ADMIN')])]);

        return $response;
    }

    public function editLogWork(Request $request, EntityManagerInterface $em, $id = null): Response
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);

        if ($id === null) {
            throw $this->createNotFoundException('No Work Log ID was supplied');
        }

        /** @var WorkLog $workLog */
        $workLog = $this->getRepo(WorkLog::class)->find($id);

        if (!$workLog) {
            throw $this->createNotFoundException('The Work Log with ID "' . $id . '" was not found.');
        }

        /** @var Job $job */
        $job = $workLog->getJob();

        $engineer = $this->isGranted('ROLE_ENGINEER') && !$this->isGranted('ROLE_SUPER_ADMIN') ? true : false;

        $jobDetailLabel = $job->getUrgent() ? 'Fault reported' : 'Job detail';
        $jobDetail      = $job->getDetail();
        $response       = new JsonResponse();
        $currentUser    = $this->getCurrentUser();

        if ($engineer) {
            if ($currentUser != $workLog->getUser()) {
                throw $this->createNotFoundException('The Work Log with ID "' . $id . '" was not found.');
            }

            $workLog->setUser($currentUser);
        }

        $form = $this->createForm(WorkLogType::class, $workLog, [
            'isEdit' => true,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $workLog = $form->getData();

            if ($workLog->getDescription() && (!$engineer or ($workLog->getTimeLabour() or $workLog->getTimeTravel()))) {
                if ($workLog->getUser() && $workLog->getUser()->getCostMultiplier() != null) {
                    $workLog->setUserCostMultiplier($workLog->getUser()->getCostMultiplier());
                }

                $em->persist($workLog);
                $em->flush();
            }

            $truck = $job->getTruck();
            $hourReading = $workLog->getHourReading();

            if ($truck && $hourReading) {
                $truck->setHourReading($hourReading);
                $em->persist($truck);
                $em->flush();
            }

            if ($workLog->getRequestedParts()) {
                $job->setUrgent(1);

                if ($workLog->unassignJob) {
                    $job->removeAllEngineersFromJob();
                }

                $this->saveJob($job, $em);
            }

            if ($workLog->postponeJob) {
                $this->getJobManager()->postponeJob($job);
                $this->saveJob($job, $em);
            } else {
                if ($workLog->completeJob) {
                    $workLogs = $job->getWorkLogs();
                    $engineers = $job->getEngineers();
                    $engineersRequiringLogs = [];

                    foreach ($engineers as $engineer) {
                        if ($engineer->getId() == $workLog->getUser()->getId()) {
                            continue;
                        }

                        $engineerWorkLogs = array_filter($workLogs, fn ($workLog): bool => $workLog->getUser()->getId() == $engineer->getId());

                        if (!count((array) $engineerWorkLogs)) {
                            $engineersRequiringLogs[] = $engineer;
                        }
                    }

                    if (count($engineersRequiringLogs)) {
                        $response->setData(['success'     => false, 'displayForm' => false, 'refresh'     => true, 'html'        => $this->renderView('Job/_jobRequiresWorklogs.html.twig', ['engineers' => $engineersRequiringLogs])]);

                        return $response;
                    }

                    $em->persist($job);
                    $em->flush();

                    $this->setFlash('success', 'The time labour "' . $workLog->getTimeLabour(true) . '" and time travel "' . $workLog->getTimeTravel(true) . '" has been successfully logged on job "' . $job->getId() . '".');

                    return $this->redirect($this->generateUrl('job_assign_stock', ['id' => $job->getId(), 'workLog' => true]));
                }

                $this->setFlash('success', 'The time labour "' . $workLog->getTimeLabour(true) . '" and time travel "' . $workLog->getTimeTravel(true) . '" has been successfully assigned on job "' . $job->getId() . '".');

                return $this->redirect($this->generateUrl('set_stock_used_on_job_log', ['id' => $job->getId()]));
            }

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The time labour "' . $workLog->getTimeLabour(true) . '" and time travel "' . $workLog->getTimeTravel(true) . '" has been successfully assigned on job "' . $job->getId() . '".');

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Job/_workLogForm.html.twig', ['form'           => $form->createView(), 'formRoute'      => $this->generateUrl('edit_work_log', ['id' => $id]), 'job'            => $job, 'workLog'        => $workLog, 'jobDetail'      => $jobDetail, 'jobDetailLabel' => $jobDetailLabel, 'isFirstWorkLog' => 0, 'isAdmin'        => $this->isGranted('ROLE_SERVICE_ADMIN'), 'isEdit'         => true])]);

        return $response;
    }

    public function signOff(EntityManagerInterface $em, $id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No job ID was supplied');
        }

        $job = $this->getRepo(Job::class)->find($id);

        if (!$job) {
            throw $this->createNotFoundException('The Job with ID "' . $id . '" was not found.');
        }

        if ($job->getSignedOff()) {
            $this->setFlash('success', 'The job "' . $job->getJobCode() . '" has been successfully signed off.');

            return $this->redirect($this->generateUrl('list_jobs'));
        }

        $job->setSignedOff(1);

        if ($job->getChargeMethod() === ChargeMethod::CHARGE_AGAINST_MAINTENANCE) {
            $totalCost = $job->getTotalCost();
            $period = $job->getAssetLifecyclePeriod();

            if ($period) {
                $period->incrementMaintenanceExpenditure($totalCost);
                $em->persist($period);
            }

            $job->setPaidAt(new \DateTime());

            $this->saveJob($job, $em);

            $this->setFlash('success', 'The job "' . $job->getJobCode() . '" has been successfully signed off.');
        } else {
            if (!$this->createInvoiceForJob($job)) {
                $this->setFlash('error', 'There was an error when generating an invoice for the job "' . $job->getJobCode() . '". The job has not been signed off.');

                return $this->redirect($this->generateUrl('list_jobs'));
            }

            $this->saveJob($job, $em);

            $this->setFlash('success', 'The job "' . $job->getJobCode() . '" has been successfully signed off and invoice created.');
        }

        $customer = $job->getCustomer();

        $url = $this->generateAbsoluteUrl(
            'job_signature_customer',
            ['id' => $job->getId(), 'uuid' => $customer->getUuid()],
        );
        $asset = $job->getAssetLifecyclePeriod();

        $contactsCustomer = $asset ? $asset->getContacts()->toArray() : null;
        if (!$contactsCustomer) {
            $this->setFlash('error', 'Email not sent - not contacts');

            return $this->redirect($this->generateUrl('list_jobs'));
        }
        $sentEmail = null;

        foreach ($contactsCustomer as $contact) {
            $body = $this->renderView(
                'Job/_signOffEmail.html.twig',
                ['name' => $contact->getName(), 'url' => $url, 'asset' => $asset->getAsset(), 'job' => $job],
            );

            $sentEmail = $this->getEmailManager()->sendEmail('Dawson - Job Complete', $body, $contact->getEmail(), true);

            if ($sentEmail && $sentEmail->getSendStatusCode() === 200) {
                $this->setFlash('success', 'Email send successfully.');
            } else {
                $this->setFlash('error', 'Email not sent - something went wrong.');
            }
        }

        return $this->redirect($this->generateUrl('list_jobs'));
    }

    public function unSignOff(EntityManagerInterface $em, $id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No job ID was supplied');
        }

        $job = $this->getRepo(Job::class)->find($id);

        if (!$job) {
            throw $this->createNotFoundException('The Job with ID "' . $id . '" was not found.');
        }

        $job->setSignedOff(0);
        $job->setCompletionStage(JobStage::COMPLETE);
        $em->persist($job);

        $invoice = $this->getRepo(Invoice::class)->findOneBy(['job' => $job]);

        if ($invoice) {
            $em->remove($invoice);
        }

        $em->flush();

        return $this->redirect($this->generateUrl('list_jobs'));
    }

    public function undelete(EntityManagerInterface $em, $id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No job ID was supplied');
        }

        $em->getFilters()->disable('softdeleteable');

        $job = $this->getRepo(Job::class)->find($id);

        if (!$job) {
            throw $this->createNotFoundException('The Job with ID "' . $id . '" was not found.');
        }

        $job->setDeletedAt(null);
        $em->persist($job);
        $em->flush();

        $em->getFilters()->enable('softdeleteable');

        return $this->redirect($this->generateUrl('list_jobs'));
    }

    public function confirmSpecialItems(Request $request, EntityManagerInterface $em, $id = null, $workLog = false): Response
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);

        if ($id === null) {
            throw $this->createNotFoundException('No job ID was supplied');
        }

        /** @var Job $job */
        $job = $this->getRepo(Job::class)->find($id);

        if (!$job) {
            throw $this->createNotFoundException('The Job with ID "' . $id . '" was not found.');
        }

        $response = new JsonResponse();

        if ($request->getMethod() == 'POST') {
            $data = json_decode((string) $request->get('confirmed_ids', '[]'), true, 512, JSON_THROW_ON_ERROR);

            if (isset($data)) {
                foreach ($data as $id => $used) {
                    $part = $this->getRepo(EstimateItem::class)->find($id);

                    if (!$part) {
                        continue;
                    }

                    $initialUsedOnJob = $part->getUsedOnJob();
                    $part->setUsedOnJob($initialUsedOnJob + $used);
                    $em->persist($part);

                    // We need to check if it's 0, because if it has been
                    // assigned previously that means the stock has already been created
                    if ($initialUsedOnJob == 0) {
                        $this->createStockFromSpecialItem($part);
                    }
                }
            }

            $em->flush();

            return $this->redirect($this->generateUrl('set_stock_used_on_job', ['id' => $job->getId()]));
        }

        $response->setData([
            'success'     => false,
            'displayForm' => true,
            'refresh'     => false,
            'html'        => $this->renderView('Job/_confirmSpecialItemsForm.html.twig', [
                'action'  => $this->generateUrl('job_confirm_special_items', ['id' => $id, 'workLog' => $workLog]),
                'job'     => $job,
            ]),
        ]);

        return $response;
    }

    public function assignStock(Request $request, EntityManagerInterface $em, $id = null, $workLog = false): Response
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);

        if ($id === null) {
            throw $this->createNotFoundException('No job ID was supplied');
        }
        /** @var Job $job */
        $job = $this->getRepo(Job::class)->find($id);

        if (!$job) {
            throw $this->createNotFoundException('The Job with ID "' . $id . '" was not found.');
        }

        $response = new JsonResponse();
        $form     = $this->createForm(AssignStockToJobType::class, $job);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $job        = $form->getData();
            $stockItems = $form->get('attachedStock')->getData();

            $stockItemsToAssign = [];
            $stockTransactions  = [];

            $index = -1;

            foreach ($stockItems as $stockItem) {
                ++$index;

                $supplierStock = $stockItem['supplierStock'];
                $quantity      = $stockItem['quantity'];
                $location      = $stockItem['location'];

                $availableStockItems = $this->getRepo(StockItem::class)->getAvailableItems($supplierStock, $location, $quantity);

                if ((is_countable($availableStockItems) ? count($availableStockItems) : 0) !== $quantity) {
                    $error = new FormError('There are not enough items in stock at the requested location.');
                    $form->get('attachedStock')->get($index)->get('location')->addError($error);
                }

                $stockItemsToAssign = array_merge($stockItemsToAssign, $availableStockItems);

                $stockTransaction = new StockTransaction();
                $stockTransaction->setTransactionCategory(TransactionCategory::OUT_JOB);
                $stockTransaction->setQuantity($quantity);
                $stockTransaction->setJob($job);
                $stockTransaction->setSupplierStock($supplierStock);
                $stockTransaction->setStock($supplierStock->getStock());
                $stockTransaction->setLocation($location);

                $stockTransactions[] = $stockTransaction;
            }
            /** @var StockItem $item */
            foreach ($stockItemsToAssign as $item) {
                $item->setJob($job);
                $item->setReturnedFromJobId(null);
                if ($workLog == true) {
                    $item->setUsedOnJob(true);
                }
                $em->persist($item);
            }

            foreach ($stockTransactions as $stockTransaction) {
                $em->persist($stockTransaction);
            }

            $em->flush();

            $this->saveJob($job, $em);

            $response->setData([
                'success'     => true,
                'isEdit'      => false,
                'displayForm' => false,
                'refresh'     => true,
            ]);

            $this->setFlash('success', 'The stock was successfully assigned to "' . $job->getJobCode() . '".');

            if ($job->getCompleted()) {
                return $this->redirect($this->generateUrl('is_chargeable_job', ['id' => $id]));
            }

            return $response;
        }

        $response->setData([
            'success'     => false,
            'displayForm' => true,
            'refresh'     => false,
            'html'        => $this->renderView('Job/_assignStockForm.html.twig', [
                'form'      => $form->createView(),
                'formRoute' => $this->generateUrl('job_assign_stock', ['id' => $id, 'workLog' => $workLog]),
                'isEdit'    => false,
                'job'       => $job,
                'workLog'   => $workLog,
            ]),
        ]);

        return $response;
    }

    public function postpone(EntityManagerInterface $em, $id = null): RedirectResponse
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);

        if ($id === null) {
            throw $this->createNotFoundException('No job ID was supplied');
        }

        $job = $this->getRepo(Job::class)->find($id);

        if (!$job) {
            throw $this->createNotFoundException('The Job with ID "' . $id . '" was not found.');
        }

        $this->getJobManager()->postponeJob($job);
        $this->saveJob($job, $em);

        $this->setFlash('success', 'The job "' . $job->getJobCode() . '" has been successfully postponed.');

        return $this->redirect($this->generateUrl('list_jobs'));
    }

    public function getAttachedStockForJob($id = null): JsonResponse
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);

        if ($id === null) {
            throw $this->createNotFoundException('No job ID was supplied');
        }

        $stockItems = $this->getRepo(Job::class)->findStockAttachedToJob($id);

        $response = new JsonResponse();

        return $response->setData(['stockItems' => $stockItems]);
    }

    public function returnStock(EntityManagerInterface $em, $jobId = null, $stockId = null, $locationId = null, $quantity = 1): JsonResponse
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);

        $response = new JsonResponse();

        if ($jobId === null) {
            return $response->setData(['success' => false, 'message' => 'No job ID was supplied.']);
        }

        if ($stockId === null) {
            return $response->setData(['success' => false, 'message' => 'No stock ID was supplied.']);
        }

        if ($locationId === null) {
            return $response->setData(['success' => false, 'message' => 'No location ID was supplied.']);
        }

        $jobId = (int) $jobId;

        $jobRepo = $this->getRepo(Job::class);
        $job     = $jobRepo->find($jobId);

        $stockItems = $this->getRepo(StockItem::class)->getSupplierStockItemsOnJob($stockId, $locationId, $jobId, $quantity);

        if (empty($stockItems)) {
            return $response->setData(['success' => false, 'message' => 'No stock items found on job.']);
        }

        $supplierStock = $stockItems[0]->getSupplierStock();
        /** @var StockItem $stockItem */
        foreach ($stockItems as $stockItem) {
            $jobIdForStockItem    = $stockItem->getJob()->getId();
            $stockItemExistsOnJob = $jobId === $jobIdForStockItem ? true : false;

            if (!$stockItemExistsOnJob) {
                return $response->setData(['success' => false, 'message' => 'This stock item is not assigned to this job and therefore cannot be returned from it.']);
            }

            $location = $stockItem->getLocation();

            if ($this->isGranted('ROLE_ENGINEER') && !$this->isGranted('ROLE_SERVICE_ADMIN')) {
                $location = $this->getCurrentUser()->getVan();
                $stockItem->setLocation($location);
            }

            $stockTransaction = new StockTransaction();
            $stockTransaction->setTransactionCategory(TransactionCategory::IN_JOB);
            $stockTransaction->setQuantity(1);
            $stockTransaction->setJob($job);
            $stockTransaction->setSupplierStock($supplierStock);
            $stockTransaction->setStock($supplierStock->getStock());
            $stockTransaction->setLocation($location);
            $em->persist($stockTransaction);

            $stockItem->setJob(null);
            $stockItem->setReturnedFromJobId($job->getId());
            $em->persist($stockItem);
        }

        $em->flush();

        return $response->setData(['success'  => true, 'message'  => 'The stock Item "' . $supplierStock->getStock()->getDescription() . '" has been successfully returned from Job ' . $job->getJobIdentifier()]);
    }

    public function returnJob(EntityManagerInterface $em, $jobId = null, $stockId = null, $locationId = null, $quantity = 1): JsonResponse
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);

        $response = new JsonResponse();

        if ($jobId === null) {
            return $response->setData(['success' => false, 'message' => 'No job ID was supplied.']);
        }

        if ($stockId === null) {
            return $response->setData(['success' => false, 'message' => 'No stock ID was supplied.']);
        }

        if ($locationId === null) {
            return $response->setData(['success' => false, 'message' => 'No location ID was supplied.']);
        }

        $jobId = (int) $jobId;

        $jobRepo = $this->getRepo(Job::class);
        $job     = $jobRepo->find($jobId);

        $stockItems = $this->getRepo(StockItem::class)->getSupplierStockItemsOnJob($stockId, $locationId, $jobId, $quantity, true);

        $supplierStock = $stockItems[0]->getSupplierStock();

        /** @var StockItem $stockItem */
        foreach ($stockItems as $stockItem) {
            $stockItem->setUsedOnJob(null);
            $jobIdForStockItem    = $stockItem->getJob()->getId();
            $stockItemExistsOnJob = $jobId === $jobIdForStockItem ? true : false;

            if (!$stockItemExistsOnJob) {
                return $response->setData(['success' => false, 'message' => 'This stock item is not assigned to this job and therefore cannot be returned from it.']);
            }
            $em->persist($stockItem);
        }

        $em->flush();

        return $response->setData(['success'  => true, 'message'  => 'The stock Item "' . $supplierStock->getStock()->getDescription() . '" has been successfully returned from Job ' . $job->getJobIdentifier()]);
    }

    public function deleteWorkLog(?string $id, EntityManagerInterface $em): RedirectResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if (!$id) {
            throw $this->createNotFoundException('No work log ID supplied');
        }

        $workLog = $this->getRepo(WorkLog::class)->find($id);
        $em->remove($workLog);
        $em->flush();

        $this->setFlash('success', 'Work log "' . $id . '" was successfully deleted');

        return $this->redirect($this->generateUrl('list_jobs'));
    }

    public function workLogPartsReqComplete($id, EntityManagerInterface $em): RedirectResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if (!$id) {
            throw $this->createNotFoundException('No work log ID supplied');
        }

        $workLog = $this->getRepo(WorkLog::class)->find($id);

        if (!$workLog) {
            throw $this->createNotFoundException('Work log not found');
        }

        $job = $workLog->getJob();

        if (!$workLog->getRequestedParts()) {
            return $this->redirect($this->generateUrl('list_jobs', ['selectedItem' => $job->getId()]));
        }

        $workLog->setRequestedPartsComplete(true);

        $em->persist($workLog);
        $em->flush();

        $this->saveJob($job, $em);

        $this->setFlash('success', 'Parts request was successfully marked as satisfied.');

        return $this->redirect($this->generateUrl('list_jobs', ['selectedItem' => $job->getId()]));
    }

    public function getContactsforJob($id = null): JsonResponse
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);

        if ($id === null) {
            throw $this->createNotFoundException('No job ID supplied');
        }

        $contacts = $this->getRepo(Contact::class)->getContactsForJob($id);

        $response = new JsonResponse();

        return $response->setData(['contacts' => $contacts]);
    }

    public function addImage(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);

        if ($id === null) {
            throw $this->createNotFoundException('No job ID was supplied');
        }

        $job = $this->getRepo(Job::class)->find($id);

        if (!$job) {
            throw $this->createNotFoundException('The Job with ID "' . $id . '" was not found.');
        }

        $response = new JsonResponse();
        $form     = $this->createForm(JobImagesType::class, $job);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $job = $form->getData();

            $images = $job->getImages();
            foreach ($images as $image) {
                $image->setPath(sprintf(job::PATH, $job->getUuid()));
            }

            $this->saveJob($job, $em);

            $response->setData(['success'     => true, 'isEdit'      => false, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The image was successfully assigned to "' . $job->getJobCode() . '".');

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Job/_addImageForm.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('job_add_image', ['id' => $id]), 'isEdit'    => false, 'job'       => $job])]);

        return $response;
    }

    public function possibleAdditionalWork(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);

        if ($id === null) {
            throw $this->createNotFoundException('No Job ID was supplied.');
        }

        $job = $this->getRepo(Job::class)->find($id);

        if (!$job) {
            throw $this->createNotFoundException('No job with ID "' . $id . '" was found.');
        }

        $response = new JsonResponse();
        $form     = $this->createForm(PossibleAdditionalWorkType::class, $job);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $work = $form->getData();
            $em->persist($work);

            $recommendations = $work->getFutureWorkRecommendations();
            $estimateManager = $this->getEstimateManager();

            foreach ($recommendations as $recommendation) {
                $estimate = new Estimate();
                $estimate->setPending(true);
                $estimate->setCustomer($job->getCustomer());
                $estimate->setDetails($recommendation->getTask());
                $estimate->addFutureWorkRecommendation($recommendation);
                $estimateManager->saveEstimate($estimate);
            }

            $em->flush();

            $this->setFlash('success', 'The job "' . $job->getJobCode() . '" was successfully completed.');

            $response->setData([
                'success'     => true,
                'isEdit'      => false,
                'displayForm' => false,
                'refresh'     => true,
            ]);

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Job/_possibleAdditionalWorkForm.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('possible_additional_work', ['id' => $id]), 'job'       => $job])]);

        return $response;
    }

    public function isChargeableJob(Request $request, string $id, EntityManagerInterface $em): Response
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);

        /** @var Job $job */
        $job = $this->getRepo(Job::class)->find($id);

        if (!$job) {
            throw $this->createNotFoundException('No job with ID "' . $id . '" was found.');
        }

        $jobAssetLifecyclePeriod = $job->getAssetLifecyclePeriod();

        $contract = $jobAssetLifecyclePeriod->hasActiveMaintenance() || in_array($jobAssetLifecyclePeriod->getType(), [AssetLifecyclePeriod::PERIOD_TYPE_CONTRACT, AssetLifecyclePeriod::PERIOD_TYPE_CASUAL]) ? true : false;

        $warranty = $job->isWarranty();

        $response = new JsonResponse();
        $form     = $this->createForm(ChargeableJobType::class, $job, [
            'contract' => $contract,
            'warranty' => $warranty,
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $job = $form->getData();

            $em->persist($job);
            $em->flush();

            return $this->redirect($this->generateUrl('possible_additional_work', ['id' => $id]));
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Job/_chargeableJobForm.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('is_chargeable_job', ['id' => $id]), 'job'       => $job])]);

        return $response;
    }

    public function getJobsForTruck($id = null): JsonResponse
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);
        $response = new JsonResponse();

        if ($id === null) {
            throw $this->createNotFoundException('No Truck ID was supplied.');
        }

        $truck = $this->getRepo(Truck::class)->find($id);

        if (!$truck) {
            $response->setData(['success' => false, 'jobs'    => null]);

            return $response;
        }

        $jobs = $this->getRepo(Job::class)->findJobsForTruck($id);

        $response->setData(['success' => true, 'jobs'    => $jobs]);

        return $response;
    }

    public function stockUsedOnJob(Request $request, EntityManagerInterface $em, $id = null): Response
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);

        if ($id === null) {
            throw $this->createNotFoundException('No Job ID was supplied.');
        }

        /** @var Job $job */
        $job = $this->getRepo(Job::class)->find($id);

        if (!$job) {
            throw $this->createNotFoundException('No job with ID "' . $id . '" was found.');
        }

        $workLogs = $job->getWorkLogs();
        $lastWorkLog = end($workLogs);
        $location = $lastWorkLog->getUser()->getVan();

        if ($this->isGranted('ROLE_ENGINEER') && !$this->isGranted('ROLE_SUPER_ADMIN')) {
            $location = $this->getCurrentUser()->getVan();
        }

        $response = new JsonResponse();

        $availableStockItems = [];
        $storedStockItems = $job->getAttachedStockItems();

        /** @var StockItem $storedStockItem */
        foreach ($storedStockItems as $storedStockItem) {
            if ($storedStockItem->getUsedOnJob() === null) {
                $availableStockItems[] = $storedStockItem;
            }
        }

        if (sizeof($availableStockItems) == 0) {
            return $this->redirect($this->generateUrl('job_assign_stock', ['id' => $job->getId(), 'workLog' => true]));
        }

        $form     = $this->createForm(SetStockUsedOnJobType::class, $job);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $job                 = $form->getData();
            $submittedStockItems = new ArrayCollection($job->getAttachedStockItems());

            /** @var StockItem $availableStockItem */
            foreach ($availableStockItems as $availableStockItem) {
                if ($submittedStockItems->contains($availableStockItem) === false) {
                    $supplierStock = $availableStockItem->getSupplierStock();
                    $stockTransaction = new StockTransaction();
                    $stockTransaction->setTransactionCategory(TransactionCategory::IN_JOB);
                    $stockTransaction->setQuantity(1);
                    $stockTransaction->setJob($job);
                    $stockTransaction->setSupplierStock($supplierStock);
                    $stockTransaction->setStock($availableStockItem->getStock());
                    $stockTransaction->setLocation($location);
                    $em->persist($stockTransaction);

                    $job->removeAttachedStockItem($availableStockItem);

                    $availableStockItem->setJob(null);
                    $availableStockItem->setReturnedFromJobId($job->getId());
                    $availableStockItem->setLocation($location);
                    $em->persist($availableStockItem);
                }
            }

            $em->persist($job);
            $em->flush();

            return $this->redirect($this->generateUrl('job_assign_stock', ['id' => $job->getId(), 'workLog' => true]));
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Job/_stockUsedOnJobForm.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('set_stock_used_on_job', ['id' => $id]), 'job'       => $job, 'logWork'   => false])]);

        return $response;
    }

    public function stockUsedOnJobLog(Request $request, EntityManagerInterface $em, $id = null): Response
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);

        if ($id === null) {
            throw $this->createNotFoundException('No Job ID was supplied.');
        }

        $job = $this->getRepo(Job::class)->find($id);

        if (!$job) {
            throw $this->createNotFoundException('No job with ID "' . $id . '" was found.');
        }

        $response = new JsonResponse();

        $availableStockItems = [];
        $storedStockItems = $job->getAttachedStockItems();

        /** @var StockItem $storedStockItem */
        foreach ($storedStockItems as $storedStockItem) {
            if ($storedStockItem->getUsedOnJob() === null) {
                $availableStockItems[] = $storedStockItem;
            }
        }

        if (sizeof($availableStockItems) == 0) {
            return $this->redirect($this->generateUrl('job_assign_stock', ['id' => $id, 'workLog' => true]));
        }

        $form     = $this->createForm(SetStockUsedOnJobType::class, $job);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $job                 = $form->getData();
            $submittedStockItems = new ArrayCollection($job->getAttachedStockItems());

            /** @var StockItem $storedStockItem */
            foreach ($availableStockItems as $availableStockItem) {
                if ($submittedStockItems->contains($availableStockItem) === true) {
                    $availableStockItem->setUsedOnJob(1);
                    $em->persist($availableStockItem);
                }
            }

            $em->flush();

            return $this->redirect($this->generateUrl('job_assign_stock', ['id' => $id, 'workLog' => true]));
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Job/_stockUsedOnJobForm.html.twig', ['form'      => $form->createView(), 'formRoute' => $this->generateUrl('set_stock_used_on_job_log', ['id' => $id]), 'job'       => $job, 'logWork'   => true])]);

        return $response;
    }

    public function setJobStage($id, $stage, TimeService $timeService): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        $response = new JsonResponse();
        $responseData = ['success' => false];
        $now = $timeService->getNewDateTime();

        if ($id === null) {
            $responseData['error'] = 'No job specified.';
            $response->setData($responseData);

            return $response;
        }

        $stage = (int) $stage;

        if ($stage === null) {
            $responseData['error'] = 'No stage specified.';
            $response->setData($responseData);

            return $response;
        }

        $job = $this->getRepo(Job::class)->find($id);

        if (!$job) {
            $responseData['error'] = 'Job not found.';
            $response->setData($responseData);

            return $response;
        }

        if ($job->getSignedOff()) {
            $responseData['error'] = 'Job is already signed off.';
            $response->setData($responseData);

            return $response;
        }

        if (!array_key_exists($stage, JobStage::$stageChoices)) {
            $responseData['error'] = 'Invalid status.';
            $response->setData($responseData);

            return $response;
        }

        $jobManager = $this->getJobManager();

        $job->setCompletionStageOverridden(true);
        $job->setCompletionStage($stage);

        if ($stage === JobStage::COMPLETE) {
            $workLogs = $job->getWorkLogs();
            $lastWorkLog = end($workLogs);
            if ($workLogs && $lastWorkLog) {
                $now = $lastWorkLog->getStartDate();
            }
            $job->setCompleted(true);
            $job->setCompletedAt($now);
            $jobManager->saveJob($job);
            $responseData['complete'] = true;
        }

        $jobManager->saveJob($job);

        $responseData['success'] = true;
        $response->setData($responseData);

        return $response;
    }

    /**
     * Create stock items from unused special parts from a job.
     */
    public function createStockFromSpecialItem(EstimateItem $item, EntityManagerInterface $em): void
    {
        if ($item->getUsedOnJob() == $item->getQuantity()) {
            return;
        }

        $stock = new Stock();
        $stock->setDescription($item->getItem());
        $stock->setMinStock(0);
        $stock->setLastOrdered($item->getReceivedDate());

        $supplier = $item->getPurchaseOrder() && $item->getPurchaseOrder()->getSupplier() ? $item->getPurchaseOrder()->getSupplier() : null;
        $supplierStock = null;

        if ($supplier) {
            $stock->setPreferredSupplier($supplier);

            $supplierStock = new SupplierStock();

            $supplierStock->setDescription($item->getItem());
            $supplierStock->setStock($stock);
            $supplierStock->setSupplier($supplier);
            $supplierStock->setCostPrice($item->getPrice());
            $supplierStock->setIssuePrice($item->getRetailPrice());
            $supplierStock->setQuantityOnOrder(0);

            $em->persist($supplierStock);
        }

        $em->persist($stock);

        $logs = $item->getJob()->getWorkLogs();
        $lastWorkLog = end($logs);
        $location = ($lastWorkLog) ? $lastWorkLog->getUser()->getVan() : null;

        $count = $item->getQuantity() - ($item->getUsedOnJob() ?? 0);

        $stockTransaction = new StockTransaction();
        $stockTransaction->setTransactionCategory(TransactionCategory::IN_JOB);
        $stockTransaction->setQuantity($count);
        $stockTransaction->setJob($item->getJob());
        $stockTransaction->setSupplierStock($supplierStock);
        $stockTransaction->setStock($stock);
        $stockTransaction->setLocation($location);
        $em->persist($stockTransaction);

        for ($i = 0; $i < $count; ++$i) {
            $stockItem = new StockItem();
            $stockItem->setStock($stock);

            if ($supplierStock) {
                $stockItem->setSupplierStock($supplierStock);
            }

            $stockItem->setLocation($location);
            $stockItem->setReturnedFromJobId($item->getJob()->getId());

            $em->persist($stockItem);
        }

        $em->flush();
    }

    public function infoForSheet(Request $request, int $id): JsonResponse
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);

        if (!$id) {
            throw $this->createNotFoundException('No job ID supplied');
        }

        $response = new JsonResponse();

        $job = $this->getRepo(Job::class)->find($id);

        if (!$job) {
            $response->setData(
                ['success' => false, 'displayForm' => false, 'refresh' => false],
            );

            $this->setFlash('error', 'The job with ID "' . $id . '" was not found.');

            return $response;
        }
        $signature = new Signature();
        $form     = $this->createForm(JobSignatureType::class, $signature);

        $form->handleRequest($request);

        $stockItems = $this->getRepo(Job::class)->findStockAttachedToJob($id);

        $response->setData(
            ['success' => true, 'displayForm' => true, 'refresh' => false, 'signature' => (bool) $job->getSignature(), 'html' => $this->renderView(
                'Job/_formSignature.html.twig',
                ['modal' => true, 'form' => $form->createView(), 'formRoute' => $this->generateUrl('job_info', ['id' => $id]), 'job' => $job, 'stockItems' => $stockItems, 'totalLabourTime' => $job->getWorkLogTimeLabour(), 'totalTravelTime' => $job->getWorkLogTimeTravel()],
            )],
        );

        return $response;
    }

    /**
     * @throws \Exception
     */
    public function signatureJob(Request $request, int $id, EntityManagerInterface $em): JsonResponse
    {
        $signature = new Signature();
        $form     = $this->createForm(JobSignatureType::class, $signature);
        $job = $this->getRepo(Job::class)->find($id);
        $response = new JsonResponse();
        if (!$job) {
            $response->setData(
                ['success' => false, 'displayForm' => false, 'refresh' => false],
            );

            $this->setFlash('error', 'The job was not found.');
        }
        $form->handleRequest($request);

        $form = $form->getData();
        $dateTimeZone = $request->get('job_signature_form')['createdAtTimeZone'];
        $date = new \DateTime('now', new \DateTimeZone($dateTimeZone));

        $image = $form->getImage();
        $image->setPath(sprintf(Signature::PATH, str_replace("\n", '', shell_exec('uuidgen -r'))));
        $em->persist($image);

        $signature->setJob($job);
        $signature->setCreatedAt($date);
        $em->persist($signature);

        $job->setSignature($signature);
        $em->persist($job);
        $em->flush();

        $response->setData(
            ['success' => true, 'displayForm' => false, 'refresh' => true],
        );

        $this->setFlash('success', 'Signature saved successfully.');

        return $response;
    }

    public function sendEmailSignatureJob(int $id, EntityManagerInterface $em): JsonResponse
    {
        $job = $this->getRepo(Job::class)->find($id);
        $response = new JsonResponse();

        if (!$job) {
            $response->setData(
                ['success' => false, 'refresh' => false],
            );

            $this->setFlash('error', 'The job was not found.');
        }
        $customer = $job->getCustomer();

        $url = $this->generateAbsoluteUrl(
            'job_signature_customer',
            ['id' => $job->getId(), 'uuid' => $customer->getUuid()],
        );

        $asset = $job->getAssetLifecyclePeriod();

        $contactsCustomer = $asset ? $asset->getContacts()->toArray() : null;
        if (!$contactsCustomer) {
            $this->setFlash('error', 'Email not sent - not contacts');

            return $response;
        }
        $sentEmail = null;

        foreach ($contactsCustomer as $contact) {
            $body = $this->renderView(
                'Job/_signatureEmail.html.twig',
                ['name' => $contact->getName(), 'url' => $url, 'asset' => $asset->getAsset(), 'job' => $job],
            );
            $subject = $job->getCompleted() ? 'Job Sheet Approval Request' : "Dawson Job {$id} - not yet complete";

            $sentEmail = $this->getEmailManager()->sendEmail($subject, $body, $contact->getEmail(), true);
            $job->setSignatureEmailSent($sentEmail->getSendStatusCode());
            $job->setSignatureEmailSendAt(new \DateTime());
        }

        $em->persist($job);
        $em->flush();

        $response->setData(
            ['success' => true, 'displayForm' => false, 'refresh' => true],
        );
        if ($sentEmail && $sentEmail->getSendStatusCode() === 200) {
            $this->setFlash('success', 'Email send successfully.');
        } else {
            $this->setFlash('error', 'Email not sent - something went wrong.');
        }

        return $response;
    }

    public function getSignatureJobCustomer(int $id, string $uuid): Response
    {
        $customer = $this->getRepo(Customer::class)->findBy(['uuid' => $uuid]);

        if (!$customer) {
            return $this->redirectToRoute('login');
        }
        $job = $this->getRepo(Job::class)->find($id);

        if (!$job) {
            return $this->redirectToRoute('login');
        }

        $signature = new Signature();
        $form = $this->createForm(JobSignatureType::class, $signature);
        $stockItems = $this->getRepo(Job::class)->findStockAttachedToJob($id);

        return $this->render(
            'Job/viewSignatureForm.html.twig',
            ['job' => $job, 'form' => $form->createView(), 'formRoute' => $this->generateUrl('job_info', ['id' => $id]), 'stockItems' => $stockItems, 'modal' => false, 'totalLabourTime' => $job->getWorkLogTimeLabour(), 'totalTravelTime' => $job->getWorkLogTimeTravel()],
        );
    }

    public function getJobsUpdate($date): JsonResponse
    {
        $response = new JsonResponse();
        $jobsUpdates = $this->getRepo(Job::class)->getJobUpdatesForTheLastTime($date);

        if ($jobsUpdates) {
            return $response->setData(
                ['success' => true, 'jobsUpdates' => $jobsUpdates],
            );
        }

        return $response->setData(
            ['success' => false, 'jobsUpdates' => []],
        );
    }

    public function deleteJobWithReason(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No job ID was supplied');
        }

        $job = $this->getRepo(Job::class)->find($id);

        if (!$job) {
            throw $this->createNotFoundException('The Job with ID "' . $id . '" was not found.');
        }

        $response = new JsonResponse();
        $form     = $this->createForm(JobDeleteReasonType::class, $job);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $currentUser = $this->getCurrentUser();
            $job = $form->getData();
            $job->setReasonDeleted($job->getReasonDeleted());
            $job->setDeletedBy($currentUser);
            $em->persist($job);

            if ($items = $job->getAttachedStockItems()) {
                /** @var StockItem $item */
                foreach ($items as $item) {
                    if ($item->getJob() && !$item->getUsedOnJob()) {
                        $item->setJob(null);
                        $em->persist($item);
                    }
                    if ($item->getUsedOnJob()) {
                        $em->remove($item);
                    }
                }
                $em->flush();
            }

            if ($purchaseOrders = $job->getPurchaseOrders()) {
                /** @var PurchaseOrder $po */
                foreach ($purchaseOrders as $po) {
                    $po->setJob(null);
                    $em->persist($po);
                }

                $em->flush();
            }

            if ($stockTransactions = $job->getStockTransactions()) {
                foreach ($stockTransactions as $st) {
                    $st->setJob(null);
                    $em->persist($st);
                }

                $em->flush();
            }

            $em->flush();
            $this->getJobManager()->removeJob($job);

            $response->setData([
                'success'     => true,
                'isEdit'      => false,
                'displayForm' => false,
                'refresh'     => true,
            ]);

            $this->setFlash('success', 'The job "' . $job->getJobCode() . '" was successfully deleted');

            return $response;
        }

        $response->setData([
            'success'     => false,
            'displayForm' => true,
            'refresh'     => false,
            'html'        => $this->renderView('Job/_deleteReasonForm.html.twig', [
                'form'      => $form->createView(),
                'formRoute' => $this->generateUrl('delete_job', ['id' => $id]),
                'isEdit'    => false,
                'job'       => $job,
            ]),
        ]);

        return $response;
    }

    public function deleteSalesJob(Request $request, $id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No job ID was supplied');
        }

        $job = $this->getRepo(SalesJob::class)->find($id);

        if (!$job) {
            throw $this->createNotFoundException('The Sales job with ID "' . $id . '" was not found.');
        }

        $em = $this->getEntityManager();

        $currentUser = $this->getCurrentUser();
        $job->setDeletedBy($currentUser);
        $job->setDeletedAt(new DateTime());
        $em->persist($job);
        $em->flush();

        $this->setFlash('success', 'The job "' . $job->getJobCode() . '" was successfully deleted');

        return $this->redirect($request->headers->get('referer'));
    }

    public function unarchive($id = null): RedirectResponse
    {
        $this->securityCheck('ROLE_SERVICE_ADMIN');

        if ($id === null) {
            throw $this->createNotFoundException('No job ID was supplied');
        }

        $em = $this->getEntityManager();

        $job = $this->getRepo(Job::class)->find($id);

        if (!$job) {
            throw $this->createNotFoundException('The Job with ID "' . $id . '" was not found.');
        }

        $job->setArchived(false);
        $job->setArchivedAt(null);
        $em->persist($job);
        $em->flush();

        return $this->redirect($this->generateUrl('list_jobs'));
    }

    private function createInvoiceForJob(object $job): bool
    {
        $em = $this->getEntityManager();
        $em->getFilters()->disable('softdeleteable');

        $invoiceExists = $this->getRepo(Invoice::class)->findOneBy(['job' => $job]);
        $invoice = $invoiceExists ?: new Invoice();

        if ($invoiceExists) {
            $invoice->setDeletedAt(null);
        }
        $invoice->setJob($job);

        $job->saveCost();
        $em->persist($job);

        $customer    = $job->getCustomer();

        if (!$customer) {
            return false;
        }
        if ($customer->getRequiresOrderNumberOnInvoices()) {
            $invoice->setIsRequestedOrder(1);
            $invoice->setCompletionStage(InvoiceManager::STAGE_REQUESTED_ORDER);
        } elseif ($job->getChargeMethod() === ChargeMethod::WARRANTY) {
            $invoice->setCompletionStage(InvoiceManager::STAGE_SENT);
        } else {
            $invoice->setCompletionStage(InvoiceManager::STAGE_NOT_SENT);
        }

        $timeService = $this->container->get('v42.time_service');
        $now         = $timeService->getNewDateTime();
        $tmpDir      = sys_get_temp_dir() . '/';
        $filename    = $job->getId() . '-invoice-' . $now->format('d-m-Y') . '.pdf';
        $filename    = str_replace(' ', '-', $filename);
        $filePath    = $tmpDir . $filename;
        $s3Path      = sprintf(Customer::PATH, $customer->getUuid());
        $pdfContent  = $this->renderView('Invoice/invoicePdfTemplate.html.twig', ['invoice' => $invoice]);

        if (!$this->getPdfManager()->generatePdfFile($pdfContent, $filePath)) {
            return false;
        }
        $bucket = $this->container->getParameter('bucket');

        try {
            $this->s3->putObject([
                'Bucket'     => $bucket,
                'Key'        => $s3Path . '/' . $filename,
                'SourceFile' => $filePath,
            ]);
        } catch (\Exception) {
            return false;
        }

        $invoicePdf = $this->getRepo(AttachedFile::class)->findOneBy(['invoice' => $invoice]) ?: new AttachedFile();
        $invoicePdf->setInvoice($invoice);
        $invoicePdf->setName($filename);
        $invoicePdf->setPath($s3Path);

        $em->persist($invoicePdf);
        $em->persist($invoice);

        $em->flush();

        unlink($filePath);

        $em->getFilters()->enable('softdeleteable');

        return true;
    }

    private function saveJob($job, EntityManagerInterface $em)
    {
        if ($job->getAccepted()) {
            $job->setIsLive(1);
        }

        if ($job->getCompleted()) {
            $job->setIsLive(0);
        }

        if ($job->getSignedOff()) {
            $job->setCompleted(1);
            $job->setIsLive(0);
        }

        if ($job->getDoWithNextJob() && !$job->getDoWithJob()) {
            $upcomingJob = $this->getRepo(Job::class)->findJobToAttachJobTo($job);

            if ($upcomingJob) {
                $job->setDoWithJob($upcomingJob);
            }
        }

        if (!$job->getDoWithNextJob()) {
            $delayedJobs = $this->getRepo(Job::class)->findBy(['customer' => $job->getCustomer(), 'doWithNextJob' => true, 'doWithJob' => null]);

            if ($delayedJobs) {
                foreach ($delayedJobs as $delayedJob) {
                    $delayedJob->setDoWithJob($job);
                    $em->persist($delayedJob);
                    $em->flush($delayedJob);
                    $job->addAttachedJob($delayedJob);
                }
            }
        }

        $this->getJobManager()->saveJob($job);

        return $job;
    }
}
