<?php

namespace App\Controller;

use App\Entity\Repository\EstimateRepository;
use Symfony\Component\HttpFoundation\Response;

class AccountController extends Controller
{
    public function list(EstimateRepository $estimateRepository): Response
    {
        $this->securityCheck('ROLE_USER');
        $user     = $this->getCurrentUser();
        $customer = $user->getCustomer();
        if (!$customer) {
            $contact  = $user->getContact();
            $customer = $contact ? $contact->getCustomer() : null;

            if (!$customer) {
                return $this->render('Account/list.html.twig', ['user'              => $user, 'currentEstimates'  => [], 'previousEstimates' => []]);
            }
        }
        $currentEstimates  = $estimateRepository->getCurrentEstimatesForCustomer($customer);
        $previousEstimates = $estimateRepository->getPreviousEstimatesForCustomer($customer);

        return $this->render('Account/list.html.twig', ['user'              => $user, 'currentEstimates'  => $currentEstimates, 'previousEstimates' => $previousEstimates]);
    }
}
