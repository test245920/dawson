<?php

namespace App\Controller;

use App\Entity\AbsenceLog;
use App\Entity\Job;
use App\Entity\User;
use App\Form\Type\AbsenceLogType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EngineerController extends Controller
{
    public function list($id = null): Response
    {
        $this->securityCheck('ROLE_ENGINEER');

        $currentUser = $this->getCurrentUser();
        $superAdmin  = $this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN', $currentUser);
        $jobOwners   = null;

        if (!$superAdmin) {
            $jobs = $this->getRepo(Job::class)->getJobsforEngineer($currentUser->getId());
        } elseif ($id) {
            $jobs      = $this->getRepo(Job::class)->getJobsforEngineer($id);
            $jobOwners = $this->getRepo(User::class)->getUsersWithJobs();
        } else {
            $jobs      = $this->getRepo(Job::class)->getJobsforEngineer(null);
            $jobOwners = $this->getRepo(User::class)->getUsersWithJobs();
        }

        return $this->render('Engineer/list.html.twig', ['jobs'      => $jobs, 'jobOwners' => $jobOwners, 'userId'    => $id]);
    }

    public function addLogAbsence(Request $request, EntityManagerInterface $em): JsonResponse
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);

        $response       = new JsonResponse();
        $absenceLog     = new AbsenceLog();
        $currentUser    = $this->getCurrentUser();

        $form = $this->createForm(AbsenceLogType::class, $absenceLog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->isGranted('ROLE_ENGINEER') && !$this->isGranted('ROLE_SUPER_ADMIN')) {
                $absenceLog->setEngineer($currentUser);
            }
            $absenceLog->setCreatedBy($currentUser);
            $em->persist($absenceLog);
            $em->flush();

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The time absence has been successfully');

            return $response;
        }

        $response->setData(['success'     => false, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Default/Engineer/_addAbsenceForm.html.twig', ['form' => $form->createView(), 'user' => $currentUser, 'formRoute' => $this->generateUrl('add_log_absence')])]);

        return $response;
    }

    public function editAbsence(Request $request, EntityManagerInterface $em, $id = null): JsonResponse
    {
        $this->securityCheck(['ROLE_SUPER_ADMIN', 'ROLE_ENGINEER']);
        $currentUser    = $this->getCurrentUser();
        $response = new JsonResponse();
        /** @var AbsenceLog $absenceLog */
        $absenceLog     = $this->getRepo(AbsenceLog::class)->find($id);

        if (!$absenceLog) {
            $response->setData(['success'     => false, 'displayForm' => false, 'refresh'     => false]);

            $this->setFlash('error', 'The absence log was not found.');

            return $response;
        }

        $form = $this->createForm(AbsenceLogType::class, $absenceLog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $absenceLog = $form->getData();
            $absenceLog->setCreatedBy($currentUser);

            $em->persist($absenceLog);
            $em->flush();

            $response->setData(['success'     => true, 'displayForm' => false, 'refresh'     => true]);

            $this->setFlash('success', 'The absence log has been successfully edited');

            return $response;
        }

        $response->setData(['success'     => true, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Default/Engineer/_addAbsenceForm.html.twig', ['user' => $currentUser, 'form' => $form->createView(), 'formRoute' => $this->generateUrl('edit_absence', ['id' => $id])])]);

        return $response;
    }

    public function getEngineerRecurringJobs(): JsonResponse
    {
        $this->securityCheck(['ROLE_SERVICE_ADMIN', 'ROLE_ENGINEER']);

        $currentUser    = !$this->isGranted('ROLE_SUPER_ADMIN') ? $this->getCurrentUser() : null;

        $engineerRecurringJobs = $this->getRepo(Job::class)->getEngineerRecurringJobsList($currentUser);

        $response = new JsonResponse();
        $response->setData(['success'     => true, 'displayForm' => true, 'refresh'     => false, 'html'        => $this->renderView('Default/Engineer/_engineerRecurringJobsList.html.twig', ['jobs' => $engineerRecurringJobs])]);

        return $response;
    }
}
