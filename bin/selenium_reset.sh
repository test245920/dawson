#!/bin/bash

CURRENT_DIR=${PWD}
SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
DESIRED_DIR=$(dirname -- "$SCRIPT_DIR")

if [ $CURRENT_DIR != $DESIRED_DIR ]; then
    echo
    echo "You must run this script from the project root ($DESIRED_DIR)."
    echo
    exit
fi

CHROME_VER_REQUIRED=56.0
CHROMEDRIVER_VER_REQUIRED=2.25
SELENIUM_VERSION=3.0
SELENIUM_JAR="selenium-server-standalone-3.0.0.jar"

echo
echo "Killing any, xvfb, selenium, or chrome processes."
pkill -f '(Xvfb|xvfb-run|selenium-server|chrome)'

if [ -z "$(type -p bc)" ]; then
    echo
echo "BC not found - installing."
    sudo apt-get install -y -q bc
fi

if [ ! -d "/var/selenium" ]; then
    echo
    echo "Selenium2 directory not found - creating."
    sudo mkdir /var/selenium

    if [ ! -d "/var/selenium" ]; then
        echo
        echo "Unable to create selenium directory. Exiting."
        echo
        exit 1
    fi
fi

if [ ! -f "/var/selenium/$SELENIUM_JAR" ]; then
    echo
    echo "Selenium2 not found - installing."

    cd /var/selenium
    sudo wget http://selenium-release.storage.googleapis.com/$SELENIUM_VERSION/$SELENIUM_JAR
    cd -
fi

if [ -z "$(type -p java)" ]; then
    echo
    echo "Java not found - installing."
    sudo apt-get install -y openjdk-9-jre-headless
fi

CHROME_INSTALL_NEEDED=0

if [ -z "$(type -p google-chrome)" ]; then
    echo
    echo "Chrome not found - installing."
    CHROME_INSTALL_NEEDED=1
else
    CHROME_VER="$(google-chrome --version)"
    CHROME_VER="${CHROME_VER:14}"

    if [[ "$CHROME_VER" < $CHROME_VER_REQUIRED ]]; then
        echo
        echo "Chrome version $CHROME_VER is less than the required version ($CHROME_VER_REQUIRED)."
        CHROME_INSTALL_NEEDED=1
    fi
fi

if [ "$CHROME_INSTALL_NEEDED" == 1 ]; then
    cd /tmp
    sudo wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
    sudo dpkg --force-depends -i google-chrome*.deb
    sudo apt-get install -y -f
    cd -
fi

CHROMEDRIVER_INSTALL_NEEDED=0

if [ ! -f "/home/vagrant/chromedriver" ]; then
    echo
    echo "chromedriver not found - installing."
    CHROMEDRIVER_INSTALL_NEEDED=1
else
    CHROMEDRIVER_VER="$(/home/vagrant/chromedriver -v)"
    CHROMEDRIVER_VER="${CHROMEDRIVER_VER:13}"

    if [[ "$CHROMEDRIVER_VER" < $CHROMEDRIVER_VER_REQUIRED ]]; then
        echo
        echo "ChromeDriver version $CHROMEDRIVER_VER is less than the required version ($CHROMEDRIVER_VER_REQUIRED)."
        CHROMEDRIVER_INSTALL_NEEDED=1
    fi
fi

if [ "$CHROMEDRIVER_INSTALL_NEEDED" == 1 ]; then
    cd ~/
    sudo apt-get install unzip -y
    wget -N http://chromedriver.storage.googleapis.com/$CHROMEDRIVER_VER_REQUIRED/chromedriver_linux64.zip -P /tmp
    unzip /tmp/chromedriver_linux64.zip -d /tmp
    chmod +x /tmp/chromedriver
    sudo mv -f /tmp/chromedriver .
    cd -
fi

if [ -z "$(type -p xvfb-run)" ]; then
    echo
    echo "XVFB not found - installing."
    sudo apt-get install -y xvfb
fi

if [ -z "$(type -p dbus-launch)" ]; then
    echo
    echo "DBUS-X11 not found - installing."
    sudo apt-get install -y dbus-x11
fi

if [ -z "$(type -p haveged)" ]; then
    echo
    echo "Haveged not found - installing."
    sudo apt-get install -y haveged
fi

if ps aux | grep "xvfb-run java -jar /var/selenium/$SELENIUM_JAR" | grep -v "grep" > /dev/null
then
    echo "XVFB already running"
else
    echo "Launching XVFB and Selenium2"
    $SCRIPT_DIR/xvfb-run.sh java -Dwebdriver.chrome.driver=/home/vagrant/chromedriver -Dwebdriver.chrome.bin=/usr/bin/google-chrome -jar /var/selenium/$SELENIUM_JAR  2>&1 >/dev/null &
    sleep 5
    echo "XVFB and Selenium2 running."
fi

cd $CURRENT_DIR
