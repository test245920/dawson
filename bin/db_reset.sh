#!/bin/bash

# This script requires that you set an environmental variable that points to your symfony root directory (this allows
# each developer to have their site in a different location, as well as ensuring that this script never gets
# accidentally executed on a production server).
#
# To set this variable, edit your ~/.bashrc file and at the bottom add the following (obviously with your path, mine is
# here as an example):
#
#   export dawsonDir=/home/vagrant/sites/dawson
#
# Save your ~/.bashrc file and close your editor, then type:
#
#   source ~/.bashrc
#
# Now you should be able to use this script.

echo
echo "---------------------------------------------------"
echo "|                                                 |"
echo "|  Velocity42 Database Reset Script v1.8          |"
echo "|                                                 |"
echo "---------------------------------------------------"
echo
echo "NOTE: All output from the commands executed by this script are available in 'db_reset.log'."
echo

if [ -z "$dawsonDir" ]; then
    echo "Aborting... Could not find dawsonDir environmental variable...".
    echo
    echo "ENSURE you are not on a production server."
    echo
    exit
fi

cd $dawsonDir > /dev/null

rm bin/db_reset.log 2> /dev/null

if [ `pwd` != $dawsonDir ]; then
    echo "Aborting... Could not switch directory to the dawsonDir..."
    echo
    exit
fi

echo
echo "Resetting development database:"
echo "  - Dropping database..."

./bin/console doctrine:database:drop --env=dev --force >> bin/db_reset.log

echo "  - Creating database..."

./bin/console doctrine:database:create --env=dev >> bin/db_reset.log

echo "  - Migrating database..."

./bin/console doctrine:migrations:migrate --no-interaction --env=dev >> bin/db_reset.log

echo "  - Loading fixtures..."

./bin/console doctrine:fixtures:load --no-interaction --env=dev >> bin/db_reset.log

echo
echo "Resetting test database:"
echo "  - Dropping database..."

./bin/console doctrine:database:drop --env=test --force >> bin/db_reset.log

echo "  - Creating database..."

./bin/console doctrine:database:create --env=test >> bin/db_reset.log

echo "  - Migrating database..."

./bin/console doctrine:migrations:migrate --no-interaction --env=test  >> bin/db_reset.log

echo "  - Loading fixtures..."

./bin/console doctrine:fixtures:load --no-interaction --env=test >> bin/db_reset.log

cd - > /dev/null

echo
