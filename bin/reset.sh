#!/bin/bash

# This script requires that you set an environmental variable that points to your symfony root directory (this allows
# each developer to have their site in a different location, as well as ensuring that this script never gets
# accidentally executed on a production server).
#
# To set this variable, edit your ~/.bashrc file and at the bottom add the following (obviously with your path, mine is
# here as an example):
#
#   export dawsonDir=/home/vagrant/sites/dawson
#
# Save your ~/.bashrc file and close your editor, then type:
#
#   source ~/.bashrc
#
# Now you should be able to use this script.

echo
echo "---------------------------------------------------"
echo "|                                                 |"
echo "|  Velocity42 Reset Script v1.8                   |"
echo "|                                                 |"
echo "---------------------------------------------------"
echo
echo "NOTE: All output from the commands executed by this script are available in 'reset.log'."
echo

if [ -z "$dawsonDir" ]; then
    echo "Aborting... Could not find dawsonDir environmental variable...".
    echo
    echo "ENSURE you are not on a production server."
    echo
    exit
fi

cd $dawsonDir > /dev/null

rm bin/reset.log 2> /dev/null

if [ `pwd` != $dawsonDir ]; then
    echo "Aborting... Could not switch directory to the dawsonDir..."
    echo
    exit
fi

echo "Syncing local clock and renewing 'sudo' token..."
echo

sudo ntpdate -u pool.ntp.org

sudo rm -rf app/cache app/logs
mkdir app/cache app/logs

if [ -z "$(type -p setfacl)" ]; then
    echo
    echo "ACL not installed - installing."
    sudo apt-get install acl
fi

if ! grep -q acl /etc/fstab; then
    echo
    echo "ACL not enabled on filesystem - enabling."
    sudo awk '$2~"^/$"{$4=$4",acl"}1' OFS="\t" /etc/fstab > /tmp/fstab_tmp && sudo mv /tmp/fstab_tmp /etc/fstab
    sudo mount -o remount /
fi

echo
echo "Ensuring cache ACL is in place..."

sudo setfacl -R -m u:"www-data":rwX -m u:`whoami`:rwX app/cache app/logs 2>&1 >> bin/reset.log
sudo setfacl -dR -m u:"www-data":rwX -m u:`whoami`:rwX app/cache app/logs 2>&1 >> bin/reset.log

echo
echo "Updating Vendor files..."
echo

php composer.phar install --no-interaction --prefer-source 2>&1 >> bin/reset.log

echo
echo "Resetting development database:"
echo "  - Dropping database..."

./bin/console doctrine:database:drop --env=dev --force >> bin/reset.log

echo "  - Creating database..."

./bin/console doctrine:database:create --env=dev >> bin/reset.log

echo "  - Migrating database..."

./bin/console doctrine:migrations:migrate --no-interaction --env=dev >> bin/reset.log

echo "  - Loading fixtures..."

./bin/console doctrine:fixtures:load --no-interaction --env=dev >> bin/reset.log


echo
echo "Clearing development cache:"

./bin/console cache:clear --no-debug --env=dev >> bin/reset.log

echo
echo "Installing web assets..."

./bin/console assets:install --symlink web >> bin/reset.log

echo
echo "Resetting test database:"
echo "  - Dropping database..."

./bin/console doctrine:database:drop --env=test --force >> bin/reset.log

echo "  - Creating database..."

./bin/console doctrine:database:create --env=test >> bin/reset.log

echo "  - Migrating database..."

./bin/console doctrine:migrations:migrate --no-interaction --env=test  >> bin/reset.log

echo "  - Loading fixtures..."

./bin/console doctrine:fixtures:load --no-interaction --env=test >> bin/reset.log

echo
echo "Clearing test cache:"

./bin/console cache:clear --no-debug --env=test >> bin/reset.log

echo
echo "Changing cache & logs permissions..."

chmod -R 777 app/cache/ app/logs/

#echo
#echo "Running Behat tests..."
#echo
#
#./bin/behat --format=progress

cd - > /dev/null

echo
