#!/bin/bash

SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
$SCRIPT_DIR/selenium_reset.sh

#https://github.com/SeleniumHQ/docker-selenium/issues/87#issuecomment-187580115
export DBUS_SESSION_BUS_ADDRESS=/dev/null

if [ -f "/tmp/results.txt" ]; then
    echo
    echo "Results file from previous run found - deleting."
    echo

    rm /tmp/results.txt
fi

if [ -f "/tmp/parsedOutput.txt" ]; then
    echo
    echo "Parsed output file from previous run found - deleting."
    echo

    rm /tmp/parsedOutput.txt
fi

touch /tmp/results.txt

for feature_path in `find src/ -path '*/Features'|sort`; do
    bundle=$(echo $feature_path | sed -e 's/^[^\/]\+\/\([^\/]\+\)\/\([^\/]\+\)\/.*/\2/');
    echo
    echo "Running suite for $bundle";

    for feature_file in `find $feature_path -path *.feature |sort`; do
        feature_filename=$(basename $feature_file);
        echo
        echo " - Running feature file $feature_filename";
        ./bin/behat "$feature_file" --ansi | tee -a /tmp/results.txt;
    done
done

cat /tmp/results.txt | sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[m|K]//g" > /tmp/parsedOutput.txt
totalTests=`grep -e "scenario" /tmp/parsedOutput.txt | awk '{ print $1; num+=$1; print num}' | sort -nr | head -1`
totalPassingTests=`grep -E "^[0-9]+ scenarios* \(" /tmp/parsedOutput.txt | awk '{ split($3, a, "("); a[2]+0; if(a[2] > 0) { passes+=a[2]; print passes;} }' | sort -nr | head -1`
secondsFromMinutes=`grep -e "^[0-9]*\{1,2\}m[0-9]*\.\{1\}" /tmp/parsedOutput.txt | awk '{split($1, a, "m"); mins+=0+a[1]; print mins}' | sort -nr | head -1 | awk '{int(secondsFromMinutes = $1 * 60); print secondsFromMinutes}'`
seconds=`grep -e "^[0-9]*\{1,2\}m[0-9]*\.\{1\}" /tmp/parsedOutput.txt | awk '{split($1, a, "m"); secs+=int(0+a[2]); print secs}' | sort -nr | head -1`
total=$((secondsFromMinutes + seconds))
avgTestTime=`echo "scale=2; $total/$totalTests" | bc`
gitBranches=`git ls-remote --heads | wc -l`

echo
echo "Total tests:         $totalTests"
echo "Total passing tests: $totalPassingTests"
echo "Total test time:     $total seconds"
echo "Avg test time:       $avgTestTime seconds"
echo "Total git branches:  $gitBranches"
echo

rm /tmp/results.txt
rm /tmp/parsedOutput.txt
