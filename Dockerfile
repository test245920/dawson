#### Prod vs Dev differences ####
#
# - /etc/nginx/nginx.conf error_log should be a local file on dev, send to Datadog on prod
# - perform bin/AwaReset.sh tasks in Dockerfile/as part of startup
#
####

ARG PHP_INSTALL_VERSION=8.2
ARG APP_ENV=dev

ARG ALPINE_VERSION=3.17

# Handle dev vs prod project differences (mounted volume vs file copy)
FROM php:8.2.1-fpm-alpine$ALPINE_VERSION as build_prod
ONBUILD COPY . /var/www/html

FROM php:8.2.1-fpm-alpine$ALPINE_VERSION as build_dev
ONBUILD COPY docker/dev-home/* /root/

# Build Stage 2
FROM build_$APP_ENV

# Add wait-for-it
ADD https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh /bin/wait-for-it.sh
RUN chmod +x /bin/wait-for-it.sh

# Add S6 supervisor (for graceful stop)
ADD https://github.com/just-containers/s6-overlay/releases/download/v1.21.1.1/s6-overlay-amd64.tar.gz /tmp/
RUN tar xzf /tmp/s6-overlay-amd64.tar.gz -C /
ENTRYPOINT ["/init"]
CMD []

RUN echo "UTC" > /etc/timezone

# Not sure if these are needed
#ENV LANG en_US.UTF-8
#ENV LANGUAGE en_US:en

ARG APP_ENV
ENV APP_ENV ${APP_ENV}

ENV PHP_OPCACHE_VALIDATE_TIMESTAMPS="0"
ENV PHP_MEMORY_LIMIT="512M"

RUN apk add --no-cache --virtual .build-deps \
        pcre-dev \
        $PHPIZE_DEPS \
        curl \
        libtool \
        libxml2-dev \
    && apk add --no-cache \
        nginx \
        bash \
        acl \
        libpng \
        libpng-dev \
        libwebp-dev \
        libjpeg-turbo-dev \
        icu-dev \
        libzip-dev \
        util-linux \
    && apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/testing/ --allow-untrusted gnu-libiconv \
    && pecl install redis \
    && docker-php-ext-configure \
        # ref: https://github.com/docker-library/php/issues/920#issuecomment-562864296
        gd --enable-gd --with-jpeg --with-webp \
    && docker-php-ext-install \
        bcmath \
        gd \
        intl \
        mysqli \
        opcache \
        pdo_mysql \
        xml \
        zip \
        pcntl \
    && apk del -f .build-deps \
    && apk del libpng-dev \
    && docker-php-ext-enable \
       redis \
    && rm  -rf /tmp/* /var/cache/apk/*

ENV LD_PRELOAD /usr/lib/preloadable_libiconv.so php

# Set bash as default shell
RUN sed -i 's/bin\/ash/bin\/bash/g' /etc/passwd

RUN if [[ "$APP_ENV" = "dev" || "$APP_ENV" = "local" ]] ; then \
        apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/community/ --allow-untrusted php82-dev \
        && apk add --no-cache \
            git \
            openssh \
            openssl-dev \
            wget \
            xdg-utils \
            hicolor-icon-theme \
            bash-completion \
            git-bash-completion \
            openjdk11-jre-headless \
            chromium \
            screen \
            vim \
            htop \
            haveged \
            dbus-x11 \
        && apk add --no-cache --virtual .phpize-deps $PHPIZE_DEPS imagemagick-dev libtool \
        && export CFLAGS="$PHP_CFLAGS" CPPFLAGS="$PHP_CPPFLAGS" LDFLAGS="$PHP_LDFLAGS" \
        && pecl install imagick-3.7.0 \
        && docker-php-ext-enable imagick \
        && apk add --no-cache --virtual .imagick-runtime-deps imagemagick \
        && apk del .phpize-deps \
        && wget https://get.symfony.com/cli/installer -nv -O - | bash \
        && mv /root/.symfony5/bin/symfony /usr/local/bin/symfony \
        && curl https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh -o /usr/share/bash-completion/completions/git-prompt \
        && rm -rf /tmp/* /var/cache/apk/* \
    ; fi

# nginx configuration
COPY docker/nginx/nginx.conf /etc/nginx/nginx.conf

# PHP configuration
COPY docker/php/00_php.ini "$PHP_INI_DIR/conf.d/00_php.ini"

# PHP FPM configuration
COPY docker/php/zzz-php-fpm.conf "/usr/local/etc/php-fpm.d/zzz-php-fpm.conf"

# Copy NGINX service script
COPY docker/start-nginx.sh /etc/services.d/nginx/run
RUN chmod 755 /etc/services.d/nginx/run

# Copy PHP-FPM service script
COPY docker/start-fpm.sh /etc/services.d/php_fpm/run
RUN chmod 755 /etc/services.d/php_fpm/run

WORKDIR /var/www/html
