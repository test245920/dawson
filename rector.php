<?php

declare(strict_types=1);

use Rector\CodeQuality\Rector\Class_\InlineConstructorDefaultToPropertyRector;
use Rector\Config\RectorConfig;
use Rector\Core\Configuration\Option;
use Rector\Doctrine\Set\DoctrineSetList;
use Rector\Set\ValueObject\LevelSetList;
use Rector\Set\ValueObject\SetList;
use Rector\Symfony\Set\SymfonySetList;

return static function (RectorConfig $rectorConfig): void {
    $rectorConfig->paths([
        __DIR__ . '/src',
        __DIR__ . '/tests',
        __DIR__ . '/templates',
        __DIR__ . '/migrations',
    ]);

    // define sets of rules
        $rectorConfig->sets([
            SymfonySetList::SYMFONY_40,
            SymfonySetList::SYMFONY_41,
            SymfonySetList::SYMFONY_42,
            SymfonySetList::SYMFONY_43,
            SymfonySetList::SYMFONY_44,
            SymfonySetList::SYMFONY_50,
            SymfonySetList::SYMFONY_51,
            SymfonySetList::SYMFONY_52,
            SymfonySetList::SYMFONY_53,
            SymfonySetList::SYMFONY_54,
            LevelSetList::UP_TO_PHP_70,
            LevelSetList::UP_TO_PHP_71,
            LevelSetList::UP_TO_PHP_72,
            LevelSetList::UP_TO_PHP_73,
            LevelSetList::UP_TO_PHP_74,
            LevelSetList::UP_TO_PHP_80,
            LevelSetList::UP_TO_PHP_81,
            LevelSetList::UP_TO_PHP_82,
            SetList::TYPE_DECLARATION,
            SymfonySetList::SYMFONY_CODE_QUALITY,
            SymfonySetList::SYMFONY_CONSTRUCTOR_INJECTION,
            DoctrineSetList::DOCTRINE_CODE_QUALITY,
        ]);
};
