<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140919130003 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE stock ADD supplier_id INT DEFAULT NULL, ADD last_updated DATETIME NOT NULL');
        $this->addSql('ALTER TABLE stock ADD CONSTRAINT FK_4B3656602ADD6D8C FOREIGN KEY (supplier_id) REFERENCES supplier (id)');
        $this->addSql('CREATE INDEX IDX_4B3656602ADD6D8C ON stock (supplier_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE stock DROP FOREIGN KEY FK_4B3656602ADD6D8C');
        $this->addSql('DROP INDEX IDX_4B3656602ADD6D8C ON stock');
        $this->addSql('ALTER TABLE stock DROP supplier_id, DROP last_updated');
    }
}
