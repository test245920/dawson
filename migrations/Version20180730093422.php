<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180730093422 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE asset_lifecycle_period ADD parentPeriod_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE asset_lifecycle_period ADD CONSTRAINT FK_7B1F27D4DF7711CC FOREIGN KEY (parentPeriod_id) REFERENCES asset_lifecycle_period (id)');
        $this->addSql('CREATE INDEX IDX_7B1F27D4DF7711CC ON asset_lifecycle_period (parentPeriod_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE asset_lifecycle_period DROP FOREIGN KEY FK_7B1F27D4DF7711CC');
        $this->addSql('DROP INDEX IDX_7B1F27D4DF7711CC ON asset_lifecycle_period');
        $this->addSql('ALTER TABLE asset_lifecycle_period DROP parentPeriod_id');
    }
}
