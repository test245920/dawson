<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151012114146 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE invoice DROP FOREIGN KEY FK_90651744BE04EA9');
        $this->addSql('ALTER TABLE invoice ADD CONSTRAINT FK_90651744BE04EA9 FOREIGN KEY (job_id) REFERENCES job (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE location DROP FOREIGN KEY FK_5E9E89CBF8D8CDF1');
        $this->addSql('DROP INDEX UNIQ_5E9E89CBF8D8CDF1 ON location');
        $this->addSql('ALTER TABLE location DROP engineer_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE invoice DROP FOREIGN KEY FK_90651744BE04EA9');
        $this->addSql('ALTER TABLE invoice ADD CONSTRAINT FK_90651744BE04EA9 FOREIGN KEY (job_id) REFERENCES job (id)');
        $this->addSql('ALTER TABLE location ADD engineer_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE location ADD CONSTRAINT FK_5E9E89CBF8D8CDF1 FOREIGN KEY (engineer_id) REFERENCES user (id) ON DELETE SET NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5E9E89CBF8D8CDF1 ON location (engineer_id)');
    }
}
