<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140710170628 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE job CHANGE repeated_task_interval repeated_task_interval INT DEFAULT NULL, CHANGE cancelled cancelled TINYINT(1) DEFAULT NULL, CHANGE signed_off signed_off TINYINT(1) DEFAULT NULL, CHANGE accepted accepted TINYINT(1) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE job CHANGE repeated_task_interval repeated_task_interval INT NOT NULL, CHANGE cancelled cancelled TINYINT(1) NOT NULL, CHANGE signed_off signed_off TINYINT(1) NOT NULL, CHANGE accepted accepted TINYINT(1) NOT NULL');
    }
}
