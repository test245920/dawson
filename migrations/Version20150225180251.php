<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150225180251 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE reminder (id INT AUTO_INCREMENT NOT NULL, lead_id INT DEFAULT NULL, detail VARCHAR(255) DEFAULT NULL, date DATETIME NOT NULL, type VARCHAR(255) DEFAULT NULL, deletedAt DATETIME DEFAULT NULL, INDEX IDX_40374F4055458D (lead_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reminder_user (reminder_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_9A6F0A60D987BE75 (reminder_id), INDEX IDX_9A6F0A60A76ED395 (user_id), PRIMARY KEY(reminder_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE reminder ADD CONSTRAINT FK_40374F4055458D FOREIGN KEY (lead_id) REFERENCES lead (id)');
        $this->addSql('ALTER TABLE reminder_user ADD CONSTRAINT FK_9A6F0A60D987BE75 FOREIGN KEY (reminder_id) REFERENCES reminder (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE reminder_user ADD CONSTRAINT FK_9A6F0A60A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE purchase_order ADD supplier_id INT DEFAULT NULL, ADD deletedAt DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE purchase_order ADD CONSTRAINT FK_21E210B22ADD6D8C FOREIGN KEY (supplier_id) REFERENCES supplier (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_21E210B22ADD6D8C ON purchase_order (supplier_id)');
        $this->addSql('ALTER TABLE purchase_order_item ADD deletedAt DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE reminder_user DROP FOREIGN KEY FK_9A6F0A60D987BE75');
        $this->addSql('DROP TABLE reminder');
        $this->addSql('DROP TABLE reminder_user');
        $this->addSql('ALTER TABLE purchase_order DROP FOREIGN KEY FK_21E210B22ADD6D8C');
        $this->addSql('DROP INDEX IDX_21E210B22ADD6D8C ON purchase_order');
        $this->addSql('ALTER TABLE purchase_order DROP supplier_id, DROP deletedAt');
        $this->addSql('ALTER TABLE purchase_order_item DROP deletedAt');
    }
}
