<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170530152328 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE estimate_stock DROP FOREIGN KEY FK_3C7767AF1969624');
        $this->addSql('ALTER TABLE estimate_stock ADD CONSTRAINT FK_3C7767AF1969624 FOREIGN KEY (supplierStock_id) REFERENCES supplier_stock (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE estimate_stock DROP FOREIGN KEY FK_3C7767AF1969624');
        $this->addSql('ALTER TABLE estimate_stock ADD CONSTRAINT FK_3C7767AF1969624 FOREIGN KEY (supplierStock_id) REFERENCES supplier_stock (id)');
    }
}
