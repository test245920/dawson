<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220111124147 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE job ADD signature_email_send_at DATETIME DEFAULT NULL AFTER signature_email_sent');
        $this->addSql('ALTER TABLE job ADD signature_email_resent INT DEFAULT NULL AFTER signature_email_send_at');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE job DROP signature_email_send_at');
        $this->addSql('ALTER TABLE job DROP signature_email_resent');
    }
}
