<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160224150636 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE truck DROP FOREIGN KEY FK_CDCCF30A7975B7E7');
        $this->addSql('ALTER TABLE truck ADD CONSTRAINT FK_CDCCF30A7975B7E7 FOREIGN KEY (model_id) REFERENCES truck_model (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE truck_model DROP FOREIGN KEY FK_564EC30340BE4F32');
        $this->addSql('ALTER TABLE truck_model DROP FOREIGN KEY FK_564EC303763C5D06');
        $this->addSql('ALTER TABLE truck_model DROP FOREIGN KEY FK_564EC303A66A3F43');
        $this->addSql('ALTER TABLE truck_model DROP FOREIGN KEY FK_564EC303CFBF73EB');
        $this->addSql('ALTER TABLE truck_model ADD CONSTRAINT FK_564EC30340BE4F32 FOREIGN KEY (fuelFilter_id) REFERENCES stock (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE truck_model ADD CONSTRAINT FK_564EC303763C5D06 FOREIGN KEY (oilFilter_id) REFERENCES stock (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE truck_model ADD CONSTRAINT FK_564EC303A66A3F43 FOREIGN KEY (airFilter_id) REFERENCES stock (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE truck_model ADD CONSTRAINT FK_564EC303CFBF73EB FOREIGN KEY (make_id) REFERENCES truck_make (id) ON DELETE SET NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE truck DROP FOREIGN KEY FK_CDCCF30A7975B7E7');
        $this->addSql('ALTER TABLE truck ADD CONSTRAINT FK_CDCCF30A7975B7E7 FOREIGN KEY (model_id) REFERENCES truck_model (id)');
        $this->addSql('ALTER TABLE truck_model DROP FOREIGN KEY FK_564EC303CFBF73EB');
        $this->addSql('ALTER TABLE truck_model DROP FOREIGN KEY FK_564EC303763C5D06');
        $this->addSql('ALTER TABLE truck_model DROP FOREIGN KEY FK_564EC303A66A3F43');
        $this->addSql('ALTER TABLE truck_model DROP FOREIGN KEY FK_564EC30340BE4F32');
        $this->addSql('ALTER TABLE truck_model ADD CONSTRAINT FK_564EC303CFBF73EB FOREIGN KEY (make_id) REFERENCES truck_make (id)');
        $this->addSql('ALTER TABLE truck_model ADD CONSTRAINT FK_564EC303763C5D06 FOREIGN KEY (oilFilter_id) REFERENCES stock (id)');
        $this->addSql('ALTER TABLE truck_model ADD CONSTRAINT FK_564EC303A66A3F43 FOREIGN KEY (airFilter_id) REFERENCES stock (id)');
        $this->addSql('ALTER TABLE truck_model ADD CONSTRAINT FK_564EC30340BE4F32 FOREIGN KEY (fuelFilter_id) REFERENCES stock (id)');
    }
}
