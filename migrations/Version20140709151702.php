<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140709151702 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('CREATE TABLE stock_type (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE purchase_order ADD reference VARCHAR(100) DEFAULT NULL');
        $this->addSql('ALTER TABLE stock ADD type_id INT DEFAULT NULL, DROP type');
        $this->addSql('ALTER TABLE stock ADD CONSTRAINT FK_4B365660C54C8C93 FOREIGN KEY (type_id) REFERENCES stock_type (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_4B365660C54C8C93 ON stock (type_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE stock DROP FOREIGN KEY FK_4B365660C54C8C93');
        $this->addSql('DROP TABLE stock_type');
        $this->addSql('ALTER TABLE purchase_order DROP reference');
        $this->addSql('DROP INDEX IDX_4B365660C54C8C93 ON stock');
        $this->addSql('ALTER TABLE stock ADD type VARCHAR(100) DEFAULT NULL, DROP type_id');
    }
}
