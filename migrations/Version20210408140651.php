<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210408140651 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE work_log ADD labour_rate NUMERIC(8, 2) DEFAULT NULL, ADD labour_rate_internal NUMERIC(8, 2) DEFAULT NULL');
        $this->addSql('ALTER TABLE stock_item ADD cost_price NUMERIC(8, 2) DEFAULT NULL, ADD issue_price NUMERIC(8, 2) DEFAULT NULL');

        $this->addSql('UPDATE work_log
            LEFT JOIN job ON job.id = work_log.job_id
            LEFT JOIN customer ON customer.id = job.customer_id
            SET work_log.labour_rate_internal = 40,
                work_log.labour_rate = (
                    CASE
                        WHEN job.charge_method <> 0 THEN 40
                        WHEN job.customer_id IS NULL THEN 40
                        WHEN customer.labour_rate IS NOT NULL THEN customer.labour_rate
                        ELSE 50
                    END
                )
        ');

        $this->addSql('UPDATE stock_item
            LEFT JOIN supplier_stock ON supplier_stock.id = stock_item.supplierStock_id
            SET stock_item.cost_price = supplier_stock.cost_price,
                stock_item.issue_price = supplier_stock.issue_price
            WHERE stock_item.job_id IS NOT NULL
        ');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE stock_item DROP cost_price, DROP issue_price');
        $this->addSql('ALTER TABLE work_log DROP labour_rate, DROP labour_rate_internal');
    }
}
