<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140709124500 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE stock DROP FOREIGN KEY FK_4B365660B4AA9D01');
        $this->addSql('DROP INDEX IDX_4B365660B4AA9D01 ON stock');
        $this->addSql('ALTER TABLE stock ADD supplier_notes LONGTEXT DEFAULT NULL, DROP altSupplier_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE stock ADD altSupplier_id INT DEFAULT NULL, DROP supplier_notes');
        $this->addSql('ALTER TABLE stock ADD CONSTRAINT FK_4B365660B4AA9D01 FOREIGN KEY (altSupplier_id) REFERENCES supplier (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_4B365660B4AA9D01 ON stock (altSupplier_id)');
    }
}
