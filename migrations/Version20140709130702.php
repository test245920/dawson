<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140709130702 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('DROP INDEX UNIQ_9B2A6C7E5E237E06 ON supplier');
        $this->addSql('ALTER TABLE supplier ADD code VARCHAR(50) DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9B2A6C7E77153098 ON supplier (code)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('DROP INDEX UNIQ_9B2A6C7E77153098 ON supplier');
        $this->addSql('ALTER TABLE supplier DROP code');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9B2A6C7E5E237E06 ON supplier (name)');
    }
}
