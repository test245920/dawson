<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140626144846 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('CREATE TABLE customer_number (id INT AUTO_INCREMENT NOT NULL, customer_id INT DEFAULT NULL, letter TINYTEXT NOT NULL, number INT NOT NULL, customerNumber TINYTEXT NOT NULL, assigned_at DATETIME NOT NULL, INDEX IDX_2755C3059395C3F3 (customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE customer (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, address TINYTEXT DEFAULT NULL, contactName TINYTEXT DEFAULT NULL, telephone TINYTEXT DEFAULT NULL, fax TINYTEXT DEFAULT NULL, email TINYTEXT DEFAULT NULL, complete TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_81398E095E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE customer_number ADD CONSTRAINT FK_2755C3059395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE customer_number DROP FOREIGN KEY FK_2755C3059395C3F3');
        $this->addSql('DROP TABLE customer_number');
        $this->addSql('DROP TABLE customer');
    }
}
