<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150105112403 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE visit_user (visit_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_52CB2B8B75FA0FF2 (visit_id), INDEX IDX_52CB2B8BA76ED395 (user_id), PRIMARY KEY(visit_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact_visit (contact_id INT NOT NULL, visit_id INT NOT NULL, INDEX IDX_6E54F936E7A1254A (contact_id), INDEX IDX_6E54F93675FA0FF2 (visit_id), PRIMARY KEY(contact_id, visit_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE visit_user ADD CONSTRAINT FK_52CB2B8B75FA0FF2 FOREIGN KEY (visit_id) REFERENCES visit (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE visit_user ADD CONSTRAINT FK_52CB2B8BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contact_visit ADD CONSTRAINT FK_6E54F936E7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contact_visit ADD CONSTRAINT FK_6E54F93675FA0FF2 FOREIGN KEY (visit_id) REFERENCES visit (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE contact_role');
        $this->addSql('DROP TABLE user_visit');
        $this->addSql('ALTER TABLE visit ADD summary VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE contact_role (contact_id INT NOT NULL, role_id INT NOT NULL, INDEX IDX_7F960895E7A1254A (contact_id), INDEX IDX_7F960895D60322AC (role_id), PRIMARY KEY(contact_id, role_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_visit (user_id INT NOT NULL, visit_id INT NOT NULL, INDEX IDX_A1BC1261A76ED395 (user_id), INDEX IDX_A1BC126175FA0FF2 (visit_id), PRIMARY KEY(user_id, visit_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE contact_role ADD CONSTRAINT FK_7F960895D60322AC FOREIGN KEY (role_id) REFERENCES role (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contact_role ADD CONSTRAINT FK_7F960895E7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_visit ADD CONSTRAINT FK_A1BC126175FA0FF2 FOREIGN KEY (visit_id) REFERENCES visit (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_visit ADD CONSTRAINT FK_A1BC1261A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE visit_user');
        $this->addSql('DROP TABLE contact_visit');
        $this->addSql('ALTER TABLE visit DROP summary');
    }
}
