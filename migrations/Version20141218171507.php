<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20141218171507 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE note (id INT AUTO_INCREMENT NOT NULL, lead_id INT DEFAULT NULL, visit_id INT DEFAULT NULL, reason VARCHAR(255) NOT NULL, detail VARCHAR(255) NOT NULL, date DATETIME NOT NULL, deletedAt DATETIME DEFAULT NULL, INDEX IDX_CFBDFA1455458D (lead_id), INDEX IDX_CFBDFA1475FA0FF2 (visit_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE source (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, deletedAt DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE visit (id INT AUTO_INCREMENT NOT NULL, lead_id INT DEFAULT NULL, date DATETIME NOT NULL, deletedAt DATETIME DEFAULT NULL, INDEX IDX_437EE93955458D (lead_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_visit (user_id INT NOT NULL, visit_id INT NOT NULL, INDEX IDX_A1BC1261A76ED395 (user_id), INDEX IDX_A1BC126175FA0FF2 (visit_id), PRIMARY KEY(user_id, visit_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact_role (contact_id INT NOT NULL, role_id INT NOT NULL, INDEX IDX_7F960895E7A1254A (contact_id), INDEX IDX_7F960895D60322AC (role_id), PRIMARY KEY(contact_id, role_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact_lead (contact_id INT NOT NULL, lead_id INT NOT NULL, INDEX IDX_6EE334E7A1254A (contact_id), INDEX IDX_6EE33455458D (lead_id), PRIMARY KEY(contact_id, lead_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lead (id INT AUTO_INCREMENT NOT NULL, customer_id INT DEFAULT NULL, source_id INT DEFAULT NULL, status VARCHAR(255) NOT NULL, summary VARCHAR(255) NOT NULL, initialContact DATETIME NOT NULL, lastContact DATETIME NOT NULL, priority VARCHAR(255) NOT NULL, webFormEnquiry TINYINT(1) NOT NULL, deletedAt DATETIME DEFAULT NULL, INDEX IDX_289161CB9395C3F3 (customer_id), INDEX IDX_289161CB953C1C61 (source_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE note ADD CONSTRAINT FK_CFBDFA1455458D FOREIGN KEY (lead_id) REFERENCES lead (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE note ADD CONSTRAINT FK_CFBDFA1475FA0FF2 FOREIGN KEY (visit_id) REFERENCES visit (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE visit ADD CONSTRAINT FK_437EE93955458D FOREIGN KEY (lead_id) REFERENCES lead (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE user_visit ADD CONSTRAINT FK_A1BC1261A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_visit ADD CONSTRAINT FK_A1BC126175FA0FF2 FOREIGN KEY (visit_id) REFERENCES visit (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contact_role ADD CONSTRAINT FK_7F960895E7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contact_role ADD CONSTRAINT FK_7F960895D60322AC FOREIGN KEY (role_id) REFERENCES role (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contact_lead ADD CONSTRAINT FK_6EE334E7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contact_lead ADD CONSTRAINT FK_6EE33455458D FOREIGN KEY (lead_id) REFERENCES lead (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE lead ADD CONSTRAINT FK_289161CB9395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE lead ADD CONSTRAINT FK_289161CB953C1C61 FOREIGN KEY (source_id) REFERENCES source (id) ON DELETE SET NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE lead DROP FOREIGN KEY FK_289161CB953C1C61');
        $this->addSql('ALTER TABLE note DROP FOREIGN KEY FK_CFBDFA1475FA0FF2');
        $this->addSql('ALTER TABLE user_visit DROP FOREIGN KEY FK_A1BC126175FA0FF2');
        $this->addSql('ALTER TABLE note DROP FOREIGN KEY FK_CFBDFA1455458D');
        $this->addSql('ALTER TABLE visit DROP FOREIGN KEY FK_437EE93955458D');
        $this->addSql('ALTER TABLE contact_lead DROP FOREIGN KEY FK_6EE33455458D');
        $this->addSql('DROP TABLE note');
        $this->addSql('DROP TABLE source');
        $this->addSql('DROP TABLE visit');
        $this->addSql('DROP TABLE user_visit');
        $this->addSql('DROP TABLE contact_role');
        $this->addSql('DROP TABLE contact_lead');
        $this->addSql('DROP TABLE lead');
    }
}
