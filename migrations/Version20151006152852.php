<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151006152852 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE truck_model DROP FOREIGN KEY FK_564EC303CE62929E');
        $this->addSql('DROP INDEX IDX_564EC303CE62929E ON truck_model');
        $this->addSql('ALTER TABLE truck_model ADD tyre_type LONGTEXT DEFAULT NULL, CHANGE waterfilter_id fuelFilter_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE truck_model ADD CONSTRAINT FK_564EC30340BE4F32 FOREIGN KEY (fuelFilter_id) REFERENCES stock (id)');
        $this->addSql('CREATE INDEX IDX_564EC30340BE4F32 ON truck_model (fuelFilter_id)');

        $this->addSql('UPDATE truck_model SET tyre_type = (SELECT tyre_type FROM truck WHERE tyre_type IS NOT NULL AND model_id = truck_model.id LIMIT 1)');

        $this->addSql('ALTER TABLE truck DROP tyre_type');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE truck ADD tyre_type LONGTEXT DEFAULT NULL');

        $this->addSql('UPDATE truck SET tyre_type = (SELECT tyre_type FROM truck_model WHERE tyre_type IS NOT NULL AND id = truck.model_id LIMIT 1)');

        $this->addSql('ALTER TABLE truck_model DROP FOREIGN KEY FK_564EC30340BE4F32');
        $this->addSql('DROP INDEX IDX_564EC30340BE4F32 ON truck_model');
        $this->addSql('ALTER TABLE truck_model DROP tyre_type, CHANGE fuelfilter_id waterFilter_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE truck_model ADD CONSTRAINT FK_564EC303CE62929E FOREIGN KEY (waterFilter_id) REFERENCES stock (id)');
        $this->addSql('CREATE INDEX IDX_564EC303CE62929E ON truck_model (waterFilter_id)');
    }
}
