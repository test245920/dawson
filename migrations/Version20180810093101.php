<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180810093101 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE asset_lifecycle_period DROP INDEX IDX_7B1F27D4DF7711CC, ADD UNIQUE INDEX UNIQ_7B1F27D4DF7711CC (parentPeriod_id)');
        $this->addSql('ALTER TABLE asset_lifecycle_period DROP FOREIGN KEY FK_7B1F27D4DF7711CC');
        $this->addSql('ALTER TABLE asset_lifecycle_period ADD contract_hire_duration VARCHAR(50) DEFAULT NULL');
        $this->addSql('ALTER TABLE asset_lifecycle_period ADD CONSTRAINT FK_7B1F27D4DF7711CC FOREIGN KEY (parentPeriod_id) REFERENCES asset_lifecycle_period (id) ON DELETE SET NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE asset_lifecycle_period DROP INDEX UNIQ_7B1F27D4DF7711CC, ADD INDEX IDX_7B1F27D4DF7711CC (parentPeriod_id)');
        $this->addSql('ALTER TABLE asset_lifecycle_period DROP FOREIGN KEY FK_7B1F27D4DF7711CC');
        $this->addSql('ALTER TABLE asset_lifecycle_period DROP contract_hire_duration');
        $this->addSql('ALTER TABLE asset_lifecycle_period ADD CONSTRAINT FK_7B1F27D4DF7711CC FOREIGN KEY (parentPeriod_id) REFERENCES asset_lifecycle_period (id)');
    }
}
