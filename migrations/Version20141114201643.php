<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20141114201643 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE purchase_order DROP is_job_purchase, DROP is_stock_purchase');
        $this->addSql('ALTER TABLE job DROP requested_parts_text');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE job ADD requested_parts_text VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE purchase_order ADD is_job_purchase TINYINT(1) NOT NULL, ADD is_stock_purchase TINYINT(1) NOT NULL');
    }
}
