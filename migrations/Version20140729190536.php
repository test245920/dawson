<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140729190536 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('CREATE TABLE truckmodel (id INT AUTO_INCREMENT NOT NULL, make VARCHAR(255) NOT NULL, model VARCHAR(255) DEFAULT NULL, mast TINYTEXT DEFAULT NULL, hydraulics LONGTEXT DEFAULT NULL, attachments LONGTEXT DEFAULT NULL, yom INT DEFAULT NULL, weather_protection LONGTEXT DEFAULT NULL, tyre_type LONGTEXT DEFAULT NULL, maintenance LONGTEXT DEFAULT NULL, loler_interval INT DEFAULT NULL, service_interval INT DEFAULT NULL, warranty LONGTEXT DEFAULT NULL, performance_indicator LONGTEXT DEFAULT NULL, deletedAt DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE truck DROP make, DROP warranty, DROP service_interval, DROP mast, DROP hydraulics, DROP attachments, DROP yom, DROP performance_indicator, DROP purchase, DROP maintenance, DROP loler_interval, DROP weather_protection, DROP tyre_type, CHANGE model model INT DEFAULT NULL');
        $this->addSql('ALTER TABLE truck ADD CONSTRAINT FK_CDCCF30AD79572D9 FOREIGN KEY (model) REFERENCES truckmodel (id)');
        $this->addSql('CREATE INDEX IDX_CDCCF30AD79572D9 ON truck (model)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE truck DROP FOREIGN KEY FK_CDCCF30AD79572D9');
        $this->addSql('DROP TABLE truckmodel');
        $this->addSql('DROP INDEX IDX_CDCCF30AD79572D9 ON truck');
        $this->addSql('ALTER TABLE truck ADD make LONGTEXT DEFAULT NULL, ADD warranty LONGTEXT DEFAULT NULL, ADD service_interval INT DEFAULT NULL, ADD mast LONGTEXT DEFAULT NULL, ADD hydraulics LONGTEXT DEFAULT NULL, ADD attachments LONGTEXT DEFAULT NULL, ADD yom INT DEFAULT NULL, ADD performance_indicator LONGTEXT DEFAULT NULL, ADD purchase TINYINT(1) DEFAULT NULL, ADD maintenance LONGTEXT DEFAULT NULL, ADD loler_interval INT DEFAULT NULL, ADD weather_protection LONGTEXT DEFAULT NULL, ADD tyre_type LONGTEXT DEFAULT NULL, CHANGE model model LONGTEXT DEFAULT NULL');
    }
}
