<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20141208223647 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE truck DROP email, DROP telephone');
        $this->addSql('ALTER TABLE stock ADD replacedBy_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stock ADD CONSTRAINT FK_4B365660A247C6F4 FOREIGN KEY (replacedBy_id) REFERENCES stock (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_4B365660A247C6F4 ON stock (replacedBy_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE stock DROP FOREIGN KEY FK_4B365660A247C6F4');
        $this->addSql('DROP INDEX IDX_4B365660A247C6F4 ON stock');
        $this->addSql('ALTER TABLE stock DROP replacedBy_id');
        $this->addSql('ALTER TABLE truck ADD email VARCHAR(255) DEFAULT NULL, ADD telephone VARCHAR(255) DEFAULT NULL');
    }
}
