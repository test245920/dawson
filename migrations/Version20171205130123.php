<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171205130123 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE stock_transaction DROP FOREIGN KEY FK_ADF9A3E51807D5FC');
        $this->addSql('ALTER TABLE truck_transaction DROP FOREIGN KEY FK_FD9754131807D5FC');
        $this->addSql('DROP INDEX IDX_ADF9A3E51807D5FC ON stock_transaction');
        $this->addSql('ALTER TABLE stock_transaction CHANGE transactionCategory_id transaction_category INT NOT NULL');
        $this->addSql('DROP INDEX IDX_FD9754131807D5FC ON truck_transaction');
        $this->addSql('ALTER TABLE truck_transaction CHANGE transactionCategory_id transaction_category INT NOT NULL');
        $this->addSql('DROP TABLE transaction_category');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE transaction_category (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) NOT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, deletedAt DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE stock_transaction ADD transactionCategory_id INT DEFAULT NULL, DROP transaction_category');
        $this->addSql('ALTER TABLE stock_transaction ADD CONSTRAINT FK_ADF9A3E51807D5FC FOREIGN KEY (transactionCategory_id) REFERENCES transaction_category (id)');
        $this->addSql('CREATE INDEX IDX_ADF9A3E51807D5FC ON stock_transaction (transactionCategory_id)');
        $this->addSql('ALTER TABLE truck_transaction ADD transactionCategory_id INT DEFAULT NULL, DROP transaction_category');
        $this->addSql('ALTER TABLE truck_transaction ADD CONSTRAINT FK_FD9754131807D5FC FOREIGN KEY (transactionCategory_id) REFERENCES transaction_category (id)');
        $this->addSql('CREATE INDEX IDX_FD9754131807D5FC ON truck_transaction (transactionCategory_id)');
    }
}
