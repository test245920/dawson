<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20141003195511 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE site_address ADD name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE truck ADD maintenance_start_date DATETIME DEFAULT NULL, ADD warranty_start_date DATETIME DEFAULT NULL, ADD contract_hire_start_date DATETIME DEFAULT NULL, ADD casual_hire_start_date DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE site_address DROP name');
        $this->addSql('ALTER TABLE truck DROP maintenance_start_date, DROP warranty_start_date, DROP contract_hire_start_date, DROP casual_hire_start_date');
    }
}
