<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140709161111 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('CREATE TABLE job (id INT AUTO_INCREMENT NOT NULL, customer INT DEFAULT NULL, truck_id INT DEFAULT NULL, engineer INT DEFAULT NULL, job_code VARCHAR(255) NOT NULL, brief VARCHAR(255) DEFAULT NULL, notes VARCHAR(255) DEFAULT NULL, repeated_task_interval INT NOT NULL, notice_date DATETIME NOT NULL, cancelled TINYINT(1) NOT NULL, signed_off TINYINT(1) NOT NULL, cancellation_reason VARCHAR(255) DEFAULT NULL, accepted TINYINT(1) NOT NULL, INDEX IDX_FBD8E0F881398E09 (customer), INDEX IDX_FBD8E0F8C6957CCE (truck_id), INDEX IDX_FBD8E0F894176AD9 (engineer), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE job ADD CONSTRAINT FK_FBD8E0F881398E09 FOREIGN KEY (customer) REFERENCES customer (id)');
        $this->addSql('ALTER TABLE job ADD CONSTRAINT FK_FBD8E0F8C6957CCE FOREIGN KEY (truck_id) REFERENCES truck (id)');
        $this->addSql('ALTER TABLE job ADD CONSTRAINT FK_FBD8E0F894176AD9 FOREIGN KEY (engineer) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('DROP TABLE job');
    }
}
