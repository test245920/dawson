<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170406151355 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE stock_stocktransaction');
        $this->addSql('ALTER TABLE stock_transaction ADD stock_id INT DEFAULT NULL, ADD supplierStock_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stock_transaction ADD CONSTRAINT FK_ADF9A3E5DCD6110 FOREIGN KEY (stock_id) REFERENCES stock (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE stock_transaction ADD CONSTRAINT FK_ADF9A3E51969624 FOREIGN KEY (supplierStock_id) REFERENCES supplier_stock (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_ADF9A3E5DCD6110 ON stock_transaction (stock_id)');
        $this->addSql('CREATE INDEX IDX_ADF9A3E51969624 ON stock_transaction (supplierStock_id)');
        $this->addSql('ALTER TABLE stock_item ADD supplierStock_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stock_item ADD CONSTRAINT FK_6017DDA1969624 FOREIGN KEY (supplierStock_id) REFERENCES supplier_stock (id)');
        $this->addSql('CREATE INDEX IDX_6017DDA1969624 ON stock_item (supplierStock_id)');
        $this->addSql('ALTER TABLE stock DROP issue_price, DROP list_price');
        $this->addSql('ALTER TABLE supplier_stock DROP FOREIGN KEY FK_922D544AEEC75ED7');
        $this->addSql('DROP INDEX stockitem_supplier_idx ON supplier_stock');
        $this->addSql('DROP INDEX IDX_922D544AEEC75ED7 ON supplier_stock');
        $this->addSql('ALTER TABLE supplier_stock ADD issue_price NUMERIC(8, 2) DEFAULT NULL, ADD description VARCHAR(255) DEFAULT NULL, CHANGE stockitem_id stock_id INT DEFAULT NULL, CHANGE alternate_code code VARCHAR(50) DEFAULT NULL');
        $this->addSql('ALTER TABLE supplier_stock ADD CONSTRAINT FK_922D544ADCD6110 FOREIGN KEY (stock_id) REFERENCES stock (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_922D544ADCD6110 ON supplier_stock (stock_id)');
        $this->addSql('CREATE UNIQUE INDEX stock_supplier_idx ON supplier_stock (stock_id, supplier_id)');
        $this->addSql('ALTER TABLE truck DROP FOREIGN KEY FK_CDCCF30A917C7BCD');
        $this->addSql('ALTER TABLE truck DROP FOREIGN KEY FK_CDCCF30A9395C3F3');
        $this->addSql('ALTER TABLE truck ADD CONSTRAINT FK_CDCCF30A917C7BCD FOREIGN KEY (siteAddress_id) REFERENCES site_address (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE truck ADD CONSTRAINT FK_CDCCF30A9395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE stock_item DROP FOREIGN KEY FK_6017DDA1969624');
        $this->addSql('ALTER TABLE stock_item DROP FOREIGN KEY FK_6017DDADCD6110');
        $this->addSql('ALTER TABLE stock_item ADD CONSTRAINT FK_6017DDA1969624 FOREIGN KEY (supplierStock_id) REFERENCES supplier_stock (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE stock_item ADD CONSTRAINT FK_6017DDADCD6110 FOREIGN KEY (stock_id) REFERENCES stock (id) ON DELETE SET NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE stock_item DROP FOREIGN KEY FK_6017DDADCD6110');
        $this->addSql('ALTER TABLE stock_item DROP FOREIGN KEY FK_6017DDA1969624');
        $this->addSql('ALTER TABLE stock_item ADD CONSTRAINT FK_6017DDADCD6110 FOREIGN KEY (stock_id) REFERENCES stock (id)');
        $this->addSql('ALTER TABLE stock_item ADD CONSTRAINT FK_6017DDA1969624 FOREIGN KEY (supplierStock_id) REFERENCES supplier_stock (id)');
        $this->addSql('CREATE TABLE stock_stocktransaction (stock_id INT NOT NULL, stocktransaction_id INT NOT NULL, INDEX IDX_1E053800DCD6110 (stock_id), INDEX IDX_1E05380040FA1C5 (stocktransaction_id), PRIMARY KEY(stock_id, stocktransaction_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE stock_stocktransaction ADD CONSTRAINT FK_1E05380040FA1C5 FOREIGN KEY (stocktransaction_id) REFERENCES stock_transaction (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE stock_stocktransaction ADD CONSTRAINT FK_1E053800DCD6110 FOREIGN KEY (stock_id) REFERENCES stock (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE stock ADD issue_price NUMERIC(20, 10) DEFAULT NULL, ADD list_price NUMERIC(20, 10) DEFAULT NULL');
        $this->addSql('ALTER TABLE stock_item DROP FOREIGN KEY FK_6017DDA1969624');
        $this->addSql('DROP INDEX IDX_6017DDA1969624 ON stock_item');
        $this->addSql('ALTER TABLE stock_item DROP supplierStock_id');
        $this->addSql('ALTER TABLE stock_transaction DROP FOREIGN KEY FK_ADF9A3E5DCD6110');
        $this->addSql('ALTER TABLE stock_transaction DROP FOREIGN KEY FK_ADF9A3E51969624');
        $this->addSql('DROP INDEX IDX_ADF9A3E5DCD6110 ON stock_transaction');
        $this->addSql('DROP INDEX IDX_ADF9A3E51969624 ON stock_transaction');
        $this->addSql('ALTER TABLE stock_transaction DROP stock_id, DROP supplierStock_id');
        $this->addSql('ALTER TABLE supplier_stock DROP FOREIGN KEY FK_922D544ADCD6110');
        $this->addSql('DROP INDEX IDX_922D544ADCD6110 ON supplier_stock');
        $this->addSql('DROP INDEX stock_supplier_idx ON supplier_stock');
        $this->addSql('ALTER TABLE supplier_stock DROP issue_price, DROP description, CHANGE stock_id stockItem_id INT DEFAULT NULL, CHANGE code alternate_code VARCHAR(50) DEFAULT NULL');
        $this->addSql('ALTER TABLE supplier_stock ADD CONSTRAINT FK_922D544AEEC75ED7 FOREIGN KEY (stockItem_id) REFERENCES stock (id) ON DELETE SET NULL');
        $this->addSql('CREATE UNIQUE INDEX stockitem_supplier_idx ON supplier_stock (stockItem_id, supplier_id)');
        $this->addSql('CREATE INDEX IDX_922D544AEEC75ED7 ON supplier_stock (stockItem_id)');
        $this->addSql('ALTER TABLE truck DROP FOREIGN KEY FK_CDCCF30A9395C3F3');
        $this->addSql('ALTER TABLE truck DROP FOREIGN KEY FK_CDCCF30A917C7BCD');
        $this->addSql('ALTER TABLE truck ADD CONSTRAINT FK_CDCCF30A9395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id)');
        $this->addSql('ALTER TABLE truck ADD CONSTRAINT FK_CDCCF30A917C7BCD FOREIGN KEY (siteAddress_id) REFERENCES site_address (id)');
    }
}
