<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180424134608 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        /**
         * Drop FK/Indexes.
         */
        $this->addSql('ALTER TABLE truck_transaction DROP FOREIGN KEY FK_FD975413EB24ABE5');
        $this->addSql('ALTER TABLE truck_maintenance DROP FOREIGN KEY FK_A024A92BA854A8F7');
        $this->addSql('ALTER TABLE image DROP FOREIGN KEY FK_C53D045F3FE31E98');
        $this->addSql('DROP INDEX IDX_C53D045F3FE31E98 ON image');
        $this->addSql('ALTER TABLE note DROP FOREIGN KEY FK_CFBDFA14C6957CCE');
        $this->addSql('DROP INDEX IDX_CFBDFA14C6957CCE ON note');
        $this->addSql('ALTER TABLE job DROP FOREIGN KEY FK_FBD8E0F8C6957CCE');
        $this->addSql('DROP INDEX IDX_FBD8E0F8C6957CCE ON job');
        $this->addSql('ALTER TABLE truck DROP FOREIGN KEY FK_CDCCF30A917C7BCD');
        $this->addSql('ALTER TABLE truck DROP FOREIGN KEY FK_CDCCF30A9395C3F3');
        $this->addSql('DROP INDEX IDX_CDCCF30A917C7BCD ON truck');
        $this->addSql('DROP INDEX IDX_CDCCF30A9395C3F3 ON truck');

        /**
         * Create tables.
         */
        $this->addSql('CREATE TABLE asset_transaction (id INT AUTO_INCREMENT NOT NULL, maintenance_id INT DEFAULT NULL, date DATETIME NOT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, deletedAt DATETIME DEFAULT NULL, amount INT NOT NULL, transaction_category INT NOT NULL, assetLifecyclePeriod_id INT DEFAULT NULL, INDEX IDX_F94F5CD822757901 (assetLifecyclePeriod_id), UNIQUE INDEX UNIQ_F94F5CD877E48174 (maintenance_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE asset_lifecycle_period (id INT AUTO_INCREMENT NOT NULL, customer_id INT DEFAULT NULL, asset_id INT DEFAULT NULL, deletedAt DATETIME DEFAULT NULL, start DATETIME NOT NULL, end DATETIME DEFAULT NULL, estimated_hire_duration VARCHAR(50) DEFAULT NULL, hire_rate NUMERIC(8, 2) DEFAULT NULL, client_fleet LONGTEXT DEFAULT NULL, type INT NOT NULL, underwriter LONGTEXT DEFAULT NULL, maintenance_contract TINYINT(1) NOT NULL, maintenance_expenditure NUMERIC(8, 2) DEFAULT NULL, maintenance_revenue NUMERIC(8, 2) DEFAULT NULL, maintenance_rate NUMERIC(8, 2) DEFAULT NULL, maintenance_start_date DATETIME DEFAULT NULL, maintenance_end_date DATETIME DEFAULT NULL, maintenance_duration INT DEFAULT NULL, sale_price NUMERIC(8, 2) DEFAULT NULL, sale_warranty INT DEFAULT NULL, sale_warranty_duration INT DEFAULT NULL, sale_warranty_start_date DATETIME DEFAULT NULL, residual_value NUMERIC(8, 2) DEFAULT NULL, trade_in_value NUMERIC(8, 2) DEFAULT NULL, hours_per_annum INT DEFAULT NULL, siteAddress_id INT DEFAULT NULL, INDEX IDX_7B1F27D49395C3F3 (customer_id), INDEX IDX_7B1F27D45DA1941 (asset_id), INDEX IDX_7B1F27D4917C7BCD (siteAddress_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE asset (id INT AUTO_INCREMENT NOT NULL, uuid LONGTEXT DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, deletedAt DATETIME DEFAULT NULL, serial LONGTEXT NOT NULL, year_of_manufacture INT DEFAULT NULL, purchase_price NUMERIC(8, 2) DEFAULT NULL, purchase_date DATETIME DEFAULT NULL, manufacturers_warranty INT NOT NULL, manufacturers_warranty_duration INT NOT NULL, second_level_manufacturers_warranty INT NOT NULL, second_level_manufacturers_warranty_duration INT NOT NULL, hire_rate NUMERIC(8, 2) DEFAULT NULL, currentLifecyclePeriod_id INT DEFAULT NULL, assetType VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_2AF5A5C27A2E7D5 (currentLifecyclePeriod_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE truck_charger (id INT NOT NULL, make VARCHAR(255) NOT NULL, model VARCHAR(255) NOT NULL, phase LONGTEXT NOT NULL, output_voltage INT DEFAULT NULL, output_current INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE asset_maintenance (id INT AUTO_INCREMENT NOT NULL, transaction_id INT DEFAULT NULL, detail VARCHAR(255) NOT NULL, date DATETIME DEFAULT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, deletedAt DATETIME DEFAULT NULL, amount INT NOT NULL, assetLifecyclePeriod_id INT DEFAULT NULL, INDEX IDX_A4FCA1E022757901 (assetLifecyclePeriod_id), UNIQUE INDEX UNIQ_A4FCA1E02FC0CB0F (transaction_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE truck_attachment (id INT NOT NULL, make VARCHAR(255) NOT NULL, model VARCHAR(255) NOT NULL, type LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact_assetlifecycleperiod (contact_id INT NOT NULL, assetlifecycleperiod_id INT NOT NULL, INDEX IDX_BBCA7A6FE7A1254A (contact_id), INDEX IDX_BBCA7A6FFDC24392 (assetlifecycleperiod_id), PRIMARY KEY(contact_id, assetlifecycleperiod_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');

        /**
         * Migrate trucks -> asset.
         */
        $this->addSql('INSERT INTO asset (id, uuid, created_at, updated_at, deletedAt, serial, year_of_manufacture, purchase_price, purchase_date, manufacturers_warranty, manufacturers_warranty_duration, second_level_manufacturers_warranty, second_level_manufacturers_warranty_duration, hire_rate, currentLifecyclePeriod_id, assetType) SELECT id, uuid, created_at, updated_at, deletedAt, serial, yom, NULL, NULL, 0, 0, 0, 0, hire_rate, NULL, "truck" FROM truck');

        /**
         * Migrate contract hire.
         */
        $this->addSql('INSERT INTO asset_lifecycle_period (customer_id, asset_id, deletedAt, start, end, estimated_hire_duration, hire_rate, client_fleet, type, underwriter, maintenance_contract, maintenance_expenditure, maintenance_revenue, maintenance_rate, maintenance_start_date, maintenance_end_date, maintenance_duration, sale_price, sale_warranty, sale_warranty_duration, sale_warranty_start_date, residual_value, trade_in_value, hours_per_annum, siteAddress_id) SELECT customer_id, id, NULL, contract_hire_start_date, expiry, hire_term, hire_rate, client_flt_num, 1, underwriter, 1, 0, 0, maintenance_revenue, contract_hire_start_date, expiry, hire_term, NULL, NULL, NULL, NULL, residual_value, NULL, NULL, siteAddress_id FROM truck WHERE truck.deletedAt IS NULL AND purchased = 0 AND casual_hire = 0');

        /**
         * Migrate casual hire.
         */
        $this->addSql('INSERT INTO asset_lifecycle_period (customer_id, asset_id, deletedAt, start, end, estimated_hire_duration, hire_rate, client_fleet, type, underwriter, maintenance_contract, maintenance_expenditure, maintenance_revenue, maintenance_rate, maintenance_start_date, maintenance_end_date, maintenance_duration, sale_price, sale_warranty, sale_warranty_duration, sale_warranty_start_date, residual_value, trade_in_value, hours_per_annum, siteAddress_id)  SELECT customer_id, id, NULL, casual_hire_start_date, NULL, hire_term, hire_price_monthly, client_flt_num, 2, underwriter, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, residual_value, NULL, NULL, siteAddress_id FROM truck WHERE truck.deletedAt IS NULL AND purchased = 0 AND casual_hire = 1');

        /**
         * Migrate sold.
         */
        $this->addSql('INSERT INTO asset_lifecycle_period (customer_id, asset_id, deletedAt, start, end, estimated_hire_duration, hire_rate, client_fleet, type, underwriter, maintenance_contract, maintenance_expenditure, maintenance_revenue, maintenance_rate, maintenance_start_date, maintenance_end_date, maintenance_duration, sale_price, sale_warranty, sale_warranty_duration, sale_warranty_start_date, residual_value, trade_in_value, hours_per_annum, siteAddress_id)  SELECT customer_id, id, NULL, purchased_date, NULL, NULL, NULL, client_flt_num, 3, underwriter, purchase_maintenance, NULL, NULL, NULL, maintenance_start_date, CASE WHEN (maintenance_start_date IS NOT NULL AND maintenance_duration IS NOT NULL) THEN DATE_ADD(maintenance_start_date, INTERVAL maintenance_duration MONTH) ELSE NULL END, maintenance_duration, sale_price, CASE WHEN warranty IS NOT NULL AND warranty > 0 THEN 1 ELSE 0 END, warranty, warranty_start_date, residual_value, NULL, NULL, siteAddress_id FROM truck WHERE truck.deletedAt IS NULL AND purchased = 1');

        /**
         * Set current lifecycle period on trucks.
         */
        $this->addSql('UPDATE asset SET currentLifecyclePeriod_id = (SELECT MAX(id) FROM asset_lifecycle_period WHERE asset_id = asset.id)');

        /**
         * Migrate truck contacts -> asset lifecycle period contacts.
         */
        $this->addSql('INSERT INTO contact_assetlifecycleperiod (contact_id, assetlifecycleperiod_id) SELECT sub.contact, sub.lcp FROM (SELECT contact_id contact, (SELECT currentLifecyclePeriod_id FROM asset WHERE asset.id = truck_contact.truck_id) lcp FROM truck_contact) sub WHERE lcp IS NOT NULL');

        /**
         * Alter tables.
         */
        $this->addSql('ALTER TABLE image ADD assetLifecyclePeriod_id INT DEFAULT NULL, CHANGE truck_id asset_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE note ADD assetLifecyclePeriod_id INT DEFAULT NULL, CHANGE truck_id asset_id INT DEFAULT NULL');

        $this->addSql('ALTER TABLE job ADD assetLifecyclePeriod_id INT DEFAULT NULL');

        /**
         * Migrate jobs.
         */
        $this->addSql('UPDATE job SET assetLifecyclePeriod_id=(SELECT MAX(id) FROM asset_lifecycle_period WHERE asset_lifecycle_period.asset_id=job.truck_id)');

        $this->addSql('ALTER TABLE job DROP truck_id');

        /**
         * Drop tables.
         */
        $this->addSql('DROP TABLE contact_truck');
        $this->addSql('DROP TABLE estimate_truck');
        $this->addSql('DROP TABLE truck_contact');
        $this->addSql('DROP TABLE truck_maintenance');
        $this->addSql('DROP TABLE truck_transaction');

        $this->addSql('ALTER TABLE truck ADD attachment_id INT DEFAULT NULL, ADD charger_id INT DEFAULT NULL, ADD sideshift LONGTEXT DEFAULT NULL, ADD fork_dimensions LONGTEXT DEFAULT NULL, ADD battery_spec LONGTEXT DEFAULT NULL, DROP customer_id, DROP purchased, DROP hirable, DROP sale_price, DROP hire_price_monthly, DROP yom, DROP serial, DROP maintenance_revenue, DROP maintenance_expenditure, DROP contract_hire, DROP casual_hire, DROP expected_utilisation, DROP hire_term, DROP hire_rate, DROP max_utilisation, DROP client_flt_num, DROP deletedAt, DROP last_service, DROP last_loler, DROP purchased_date, DROP warranty, DROP siteAddress_id, DROP maintenance_start_date, DROP warranty_start_date, DROP contract_hire_start_date, DROP casual_hire_start_date, DROP maintenance_duration, DROP purchase_maintenance, DROP attachments, DROP underwriter, DROP performance_indicator, DROP attachment_model_num, DROP location, DROP expiry, DROP uuid, DROP stock_value, DROP retail_value, DROP trade_value, DROP created_at, DROP updated_at, DROP residual_value, CHANGE id id INT NOT NULL');

        /**
         * Add FKs.
         */
        $this->addSql('ALTER TABLE asset_transaction ADD CONSTRAINT FK_F94F5CD822757901 FOREIGN KEY (assetLifecyclePeriod_id) REFERENCES asset_lifecycle_period (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE asset_transaction ADD CONSTRAINT FK_F94F5CD877E48174 FOREIGN KEY (maintenance_id) REFERENCES asset_maintenance (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE asset_lifecycle_period ADD CONSTRAINT FK_7B1F27D49395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE asset_lifecycle_period ADD CONSTRAINT FK_7B1F27D45DA1941 FOREIGN KEY (asset_id) REFERENCES asset (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE asset_lifecycle_period ADD CONSTRAINT FK_7B1F27D4917C7BCD FOREIGN KEY (siteAddress_id) REFERENCES site_address (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE asset ADD CONSTRAINT FK_2AF5A5C27A2E7D5 FOREIGN KEY (currentLifecyclePeriod_id) REFERENCES asset_lifecycle_period (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE truck_charger ADD CONSTRAINT FK_8A594FD8BF396750 FOREIGN KEY (id) REFERENCES asset (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE asset_maintenance ADD CONSTRAINT FK_A4FCA1E022757901 FOREIGN KEY (assetLifecyclePeriod_id) REFERENCES asset_lifecycle_period (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE asset_maintenance ADD CONSTRAINT FK_A4FCA1E02FC0CB0F FOREIGN KEY (transaction_id) REFERENCES asset_transaction (id)');
        $this->addSql('ALTER TABLE truck_attachment ADD CONSTRAINT FK_BC90E467BF396750 FOREIGN KEY (id) REFERENCES asset (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contact_assetlifecycleperiod ADD CONSTRAINT FK_BBCA7A6FE7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contact_assetlifecycleperiod ADD CONSTRAINT FK_BBCA7A6FFDC24392 FOREIGN KEY (assetlifecycleperiod_id) REFERENCES asset_lifecycle_period (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE image ADD CONSTRAINT FK_C53D045F22757901 FOREIGN KEY (assetLifecyclePeriod_id) REFERENCES asset_lifecycle_period (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE image ADD CONSTRAINT FK_C53D045F5DA1941 FOREIGN KEY (asset_id) REFERENCES asset (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE job ADD CONSTRAINT FK_FBD8E0F822757901 FOREIGN KEY (assetLifecyclePeriod_id) REFERENCES asset_lifecycle_period (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE note ADD CONSTRAINT FK_CFBDFA1422757901 FOREIGN KEY (assetLifecyclePeriod_id) REFERENCES asset_lifecycle_period (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE note ADD CONSTRAINT FK_CFBDFA145DA1941 FOREIGN KEY (asset_id) REFERENCES asset (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE truck ADD CONSTRAINT FK_CDCCF30A464E68B FOREIGN KEY (attachment_id) REFERENCES truck_attachment (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE truck ADD CONSTRAINT FK_CDCCF30A45422674 FOREIGN KEY (charger_id) REFERENCES truck_charger (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE truck ADD CONSTRAINT FK_CDCCF30ABF396750 FOREIGN KEY (id) REFERENCES asset (id) ON DELETE CASCADE');

        /**
         * Add Indexes.
         */
        $this->addSql('CREATE INDEX IDX_FBD8E0F822757901 ON job (assetLifecyclePeriod_id)');
        $this->addSql('CREATE INDEX IDX_CFBDFA1422757901 ON note (assetLifecyclePeriod_id)');
        $this->addSql('CREATE INDEX IDX_CFBDFA145DA1941 ON note (asset_id)');
        $this->addSql('CREATE INDEX IDX_CDCCF30A464E68B ON truck (attachment_id)');
        $this->addSql('CREATE INDEX IDX_CDCCF30A45422674 ON truck (charger_id)');
        $this->addSql('CREATE INDEX IDX_C53D045F22757901 ON image (assetLifecyclePeriod_id)');
        $this->addSql('CREATE INDEX IDX_C53D045F5DA1941 ON image (asset_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE asset_maintenance DROP FOREIGN KEY FK_A4FCA1E02FC0CB0F');
        $this->addSql('ALTER TABLE asset_transaction DROP FOREIGN KEY FK_F94F5CD822757901');
        $this->addSql('ALTER TABLE asset DROP FOREIGN KEY FK_2AF5A5C27A2E7D5');
        $this->addSql('ALTER TABLE asset_maintenance DROP FOREIGN KEY FK_A4FCA1E022757901');
        $this->addSql('ALTER TABLE image DROP FOREIGN KEY FK_C53D045F22757901');
        $this->addSql('ALTER TABLE note DROP FOREIGN KEY FK_CFBDFA1422757901');
        $this->addSql('ALTER TABLE job DROP FOREIGN KEY FK_FBD8E0F822757901');
        $this->addSql('ALTER TABLE contact_assetlifecycleperiod DROP FOREIGN KEY FK_BBCA7A6FFDC24392');
        $this->addSql('ALTER TABLE asset_lifecycle_period DROP FOREIGN KEY FK_7B1F27D45DA1941');
        $this->addSql('ALTER TABLE truck_charger DROP FOREIGN KEY FK_8A594FD8BF396750');
        $this->addSql('ALTER TABLE image DROP FOREIGN KEY FK_C53D045F5DA1941');
        $this->addSql('ALTER TABLE note DROP FOREIGN KEY FK_CFBDFA145DA1941');
        $this->addSql('ALTER TABLE truck_attachment DROP FOREIGN KEY FK_BC90E467BF396750');
        $this->addSql('ALTER TABLE truck DROP FOREIGN KEY FK_CDCCF30ABF396750');
        $this->addSql('ALTER TABLE truck DROP FOREIGN KEY FK_CDCCF30A45422674');
        $this->addSql('ALTER TABLE asset_transaction DROP FOREIGN KEY FK_F94F5CD877E48174');
        $this->addSql('ALTER TABLE truck DROP FOREIGN KEY FK_CDCCF30A464E68B');
        $this->addSql('CREATE TABLE contact_truck (contact_id INT NOT NULL, truck_id INT NOT NULL, INDEX IDX_E0E6E305E7A1254A (contact_id), INDEX IDX_E0E6E305C6957CCE (truck_id), PRIMARY KEY(contact_id, truck_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE estimate_truck (id INT AUTO_INCREMENT NOT NULL, model_id INT DEFAULT NULL, estimate_id INT DEFAULT NULL, customer_id INT DEFAULT NULL, client_flt_num LONGTEXT DEFAULT NULL, contract_hire TINYINT(1) DEFAULT NULL, casual_hire TINYINT(1) DEFAULT NULL, hire_term INT DEFAULT NULL, hire_rate NUMERIC(8, 2) DEFAULT NULL, max_utilisation LONGTEXT DEFAULT NULL, purchased TINYINT(1) NOT NULL, purchased_date DATETIME DEFAULT NULL, hirable TINYINT(1) NOT NULL, sale_price NUMERIC(8, 2) DEFAULT NULL, hire_price_monthly NUMERIC(8, 2) DEFAULT NULL, maintenance_expenditure NUMERIC(8, 2) DEFAULT NULL, expected_utilisation LONGTEXT DEFAULT NULL, serial LONGTEXT DEFAULT NULL, maintenance_revenue NUMERIC(8, 2) DEFAULT NULL, deletedAt DATETIME DEFAULT NULL, last_service DATETIME DEFAULT NULL, last_loler DATETIME DEFAULT NULL, loler_interval INT DEFAULT NULL, service_interval INT DEFAULT NULL, warranty LONGTEXT DEFAULT NULL, yom INT DEFAULT NULL, maintenance_start_date DATETIME DEFAULT NULL, warranty_start_date DATETIME DEFAULT NULL, contract_hire_start_date DATETIME DEFAULT NULL, casual_hire_start_date DATETIME DEFAULT NULL, maintenance_duration INT DEFAULT NULL, purchase_maintenance TINYINT(1) DEFAULT NULL, mast LONGTEXT DEFAULT NULL, mastHeight INT DEFAULT NULL, hydraulics LONGTEXT DEFAULT NULL, attachments LONGTEXT DEFAULT NULL, weather_protection LONGTEXT DEFAULT NULL, tyre_type LONGTEXT DEFAULT NULL, notes LONGTEXT DEFAULT NULL, performance_indicator LONGTEXT DEFAULT NULL, attachment_model_num LONGTEXT DEFAULT NULL, fuel LONGTEXT DEFAULT NULL, siteAddress_id INT DEFAULT NULL, INDEX IDX_BA8DC2C59395C3F3 (customer_id), INDEX IDX_BA8DC2C57975B7E7 (model_id), INDEX IDX_BA8DC2C5917C7BCD (siteAddress_id), INDEX IDX_BA8DC2C585F23082 (estimate_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE truck_contact (truck_id INT NOT NULL, contact_id INT NOT NULL, INDEX IDX_8BD3C96CC6957CCE (truck_id), INDEX IDX_8BD3C96CE7A1254A (contact_id), PRIMARY KEY(truck_id, contact_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE truck_maintenance (id INT AUTO_INCREMENT NOT NULL, truck_id INT DEFAULT NULL, detail VARCHAR(255) NOT NULL, date DATETIME DEFAULT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, deletedAt DATETIME DEFAULT NULL, amount INT NOT NULL, truckTransaction_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_A024A92BA854A8F7 (truckTransaction_id), INDEX IDX_A024A92BC6957CCE (truck_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE truck_transaction (id INT AUTO_INCREMENT NOT NULL, truck_id INT DEFAULT NULL, date DATETIME NOT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, deletedAt DATETIME DEFAULT NULL, amount INT NOT NULL, transaction_category INT NOT NULL, truckMaintenace_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_FD975413EB24ABE5 (truckMaintenace_id), INDEX IDX_FD975413C6957CCE (truck_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE contact_truck ADD CONSTRAINT FK_E0E6E305C6957CCE FOREIGN KEY (truck_id) REFERENCES truck (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contact_truck ADD CONSTRAINT FK_E0E6E305E7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE estimate_truck ADD CONSTRAINT FK_BA8DC2C57975B7E7 FOREIGN KEY (model_id) REFERENCES truck_model (id)');
        $this->addSql('ALTER TABLE estimate_truck ADD CONSTRAINT FK_BA8DC2C585F23082 FOREIGN KEY (estimate_id) REFERENCES estimate (id)');
        $this->addSql('ALTER TABLE estimate_truck ADD CONSTRAINT FK_BA8DC2C5917C7BCD FOREIGN KEY (siteAddress_id) REFERENCES site_address (id)');
        $this->addSql('ALTER TABLE estimate_truck ADD CONSTRAINT FK_BA8DC2C59395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id)');
        $this->addSql('ALTER TABLE truck_contact ADD CONSTRAINT FK_8BD3C96CC6957CCE FOREIGN KEY (truck_id) REFERENCES truck (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE truck_contact ADD CONSTRAINT FK_8BD3C96CE7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE truck_maintenance ADD CONSTRAINT FK_A024A92BA854A8F7 FOREIGN KEY (truckTransaction_id) REFERENCES truck_transaction (id)');
        $this->addSql('ALTER TABLE truck_maintenance ADD CONSTRAINT FK_A024A92BC6957CCE FOREIGN KEY (truck_id) REFERENCES truck (id)');
        $this->addSql('ALTER TABLE truck_transaction ADD CONSTRAINT FK_FD975413C6957CCE FOREIGN KEY (truck_id) REFERENCES truck (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE truck_transaction ADD CONSTRAINT FK_FD975413EB24ABE5 FOREIGN KEY (truckMaintenace_id) REFERENCES truck_maintenance (id) ON DELETE SET NULL');
        $this->addSql('DROP TABLE asset_transaction');
        $this->addSql('DROP TABLE asset_lifecycle_period');
        $this->addSql('DROP TABLE asset');
        $this->addSql('DROP TABLE truck_charger');
        $this->addSql('DROP TABLE asset_maintenance');
        $this->addSql('DROP TABLE truck_attachment');
        $this->addSql('DROP TABLE contact_assetlifecycleperiod');
        $this->addSql('DROP INDEX IDX_C53D045F22757901 ON image');
        $this->addSql('DROP INDEX IDX_C53D045F5DA1941 ON image');
        $this->addSql('ALTER TABLE image ADD Truck_id INT DEFAULT NULL, DROP asset_id, DROP assetLifecyclePeriod_id');
        $this->addSql('ALTER TABLE image ADD CONSTRAINT FK_C53D045F3FE31E98 FOREIGN KEY (Truck_id) REFERENCES truck (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_C53D045F3FE31E98 ON image (Truck_id)');
        $this->addSql('DROP INDEX IDX_FBD8E0F822757901 ON job');
        $this->addSql('ALTER TABLE job CHANGE assetlifecycleperiod_id truck_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE job ADD CONSTRAINT FK_FBD8E0F8C6957CCE FOREIGN KEY (truck_id) REFERENCES truck (id)');
        $this->addSql('CREATE INDEX IDX_FBD8E0F8C6957CCE ON job (truck_id)');
        $this->addSql('DROP INDEX IDX_CFBDFA1422757901 ON note');
        $this->addSql('DROP INDEX IDX_CFBDFA145DA1941 ON note');
        $this->addSql('ALTER TABLE note ADD truck_id INT DEFAULT NULL, DROP asset_id, DROP assetLifecyclePeriod_id');
        $this->addSql('ALTER TABLE note ADD CONSTRAINT FK_CFBDFA14C6957CCE FOREIGN KEY (truck_id) REFERENCES truck (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_CFBDFA14C6957CCE ON note (truck_id)');
        $this->addSql('DROP INDEX IDX_CDCCF30A464E68B ON truck');
        $this->addSql('DROP INDEX IDX_CDCCF30A45422674 ON truck');
        $this->addSql('ALTER TABLE truck ADD customer_id INT DEFAULT NULL, ADD purchased TINYINT(1) NOT NULL, ADD hirable TINYINT(1) NOT NULL, ADD sale_price NUMERIC(8, 2) DEFAULT NULL, ADD hire_price_monthly NUMERIC(8, 2) DEFAULT NULL, ADD yom INT DEFAULT NULL, ADD serial LONGTEXT NOT NULL, ADD maintenance_revenue NUMERIC(8, 2) DEFAULT NULL, ADD maintenance_expenditure NUMERIC(8, 2) DEFAULT NULL, ADD contract_hire TINYINT(1) DEFAULT NULL, ADD casual_hire TINYINT(1) DEFAULT NULL, ADD expected_utilisation LONGTEXT DEFAULT NULL, ADD hire_term INT DEFAULT NULL, ADD hire_rate NUMERIC(8, 2) DEFAULT NULL, ADD max_utilisation LONGTEXT DEFAULT NULL, ADD client_flt_num LONGTEXT DEFAULT NULL, ADD deletedAt DATETIME DEFAULT NULL, ADD last_service DATETIME DEFAULT NULL, ADD last_loler DATETIME DEFAULT NULL, ADD purchased_date DATETIME DEFAULT NULL, ADD warranty LONGTEXT DEFAULT NULL, ADD siteAddress_id INT DEFAULT NULL, ADD maintenance_start_date DATETIME DEFAULT NULL, ADD warranty_start_date DATETIME DEFAULT NULL, ADD contract_hire_start_date DATETIME DEFAULT NULL, ADD casual_hire_start_date DATETIME DEFAULT NULL, ADD maintenance_duration INT DEFAULT NULL, ADD purchase_maintenance TINYINT(1) DEFAULT NULL, ADD attachments LONGTEXT DEFAULT NULL, ADD underwriter LONGTEXT DEFAULT NULL, ADD performance_indicator LONGTEXT DEFAULT NULL, ADD attachment_model_num LONGTEXT DEFAULT NULL, ADD location LONGTEXT DEFAULT NULL, ADD expiry DATETIME DEFAULT NULL, ADD uuid LONGTEXT DEFAULT NULL, ADD stock_value NUMERIC(8, 2) DEFAULT NULL, ADD retail_value NUMERIC(8, 2) DEFAULT NULL, ADD trade_value NUMERIC(8, 2) DEFAULT NULL, ADD created_at DATETIME DEFAULT NULL, ADD updated_at DATETIME DEFAULT NULL, ADD residual_value NUMERIC(8, 2) DEFAULT NULL, DROP attachment_id, DROP charger_id, DROP sideshift, DROP fork_dimensions, DROP battery_spec, CHANGE id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE truck ADD CONSTRAINT FK_CDCCF30A917C7BCD FOREIGN KEY (siteAddress_id) REFERENCES site_address (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE truck ADD CONSTRAINT FK_CDCCF30A9395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_CDCCF30A917C7BCD ON truck (siteAddress_id)');
        $this->addSql('CREATE INDEX IDX_CDCCF30A9395C3F3 ON truck (customer_id)');
    }
}
