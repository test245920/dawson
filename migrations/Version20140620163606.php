<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140620163606 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('CREATE TABLE truck (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, basic_specification LONGTEXT DEFAULT NULL, sellable TINYINT(1) NOT NULL, hirable TINYINT(1) NOT NULL, sale_price DOUBLE PRECISION DEFAULT NULL, hire_price_monthly DOUBLE PRECISION DEFAULT NULL, warranty INT DEFAULT NULL, service_interval INT DEFAULT NULL, UNIQUE INDEX UNIQ_CDCCF30A5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('DROP TABLE truck');
    }
}
