<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151231165157 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE future_work_recommendation ADD estimate_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE future_work_recommendation ADD CONSTRAINT FK_BF19C30B85F23082 FOREIGN KEY (estimate_id) REFERENCES estimate (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_BF19C30B85F23082 ON future_work_recommendation (estimate_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE future_work_recommendation DROP FOREIGN KEY FK_BF19C30B85F23082');
        $this->addSql('DROP INDEX IDX_BF19C30B85F23082 ON future_work_recommendation');
        $this->addSql('ALTER TABLE future_work_recommendation DROP estimate_id');
    }
}
