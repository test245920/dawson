<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20141009181809 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE truck ADD mast TINYTEXT DEFAULT NULL, ADD mastHeight INT DEFAULT NULL, ADD hydraulics LONGTEXT DEFAULT NULL, ADD attachments LONGTEXT DEFAULT NULL, ADD weather_protection LONGTEXT DEFAULT NULL, ADD tyre_type LONGTEXT DEFAULT NULL, ADD notes LONGTEXT DEFAULT NULL, ADD performance_indicator LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE truck_model DROP mast, DROP mastHeight, DROP hydraulics, DROP attachments, DROP weather_protection, DROP tyre_type, DROP performance_indicator, DROP notes');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE truck DROP mast, DROP mastHeight, DROP hydraulics, DROP attachments, DROP weather_protection, DROP tyre_type, DROP notes, DROP performance_indicator');
        $this->addSql('ALTER TABLE truck_model ADD mast LONGTEXT DEFAULT NULL, ADD mastHeight INT DEFAULT NULL, ADD hydraulics LONGTEXT DEFAULT NULL, ADD attachments LONGTEXT DEFAULT NULL, ADD weather_protection LONGTEXT DEFAULT NULL, ADD tyre_type LONGTEXT DEFAULT NULL, ADD performance_indicator LONGTEXT DEFAULT NULL, ADD notes LONGTEXT DEFAULT NULL');
    }
}
