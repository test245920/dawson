<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140703154743 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('CREATE TABLE supplier (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, address TINYTEXT DEFAULT NULL, contactName TINYTEXT DEFAULT NULL, telephone TINYTEXT DEFAULT NULL, fax TINYTEXT DEFAULT NULL, email TINYTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_9B2A6C7E5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE purchase_order (id INT AUTO_INCREMENT NOT NULL, customer_id INT DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, order_date DATETIME NOT NULL, order_instructions LONGTEXT DEFAULT NULL, quantity INT NOT NULL, stockItem_id INT DEFAULT NULL, INDEX IDX_21E210B29395C3F3 (customer_id), UNIQUE INDEX UNIQ_21E210B2EEC75ED7 (stockItem_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stock (id INT AUTO_INCREMENT NOT NULL, supplier_id INT DEFAULT NULL, code VARCHAR(50) NOT NULL, description VARCHAR(255) NOT NULL, type VARCHAR(100) DEFAULT NULL, units INT NOT NULL, stock_quantity INT NOT NULL, order_quantity INT NOT NULL, last_ordered DATETIME DEFAULT NULL, min_stock INT NOT NULL, reorder_quantity INT NOT NULL, cost_price DOUBLE PRECISION DEFAULT NULL, issue_price DOUBLE PRECISION DEFAULT NULL, list_price DOUBLE PRECISION DEFAULT NULL, carriage_price DOUBLE PRECISION DEFAULT NULL, location VARCHAR(50) DEFAULT NULL, non_stock TINYINT(1) NOT NULL, prev_code VARCHAR(50) DEFAULT NULL, altSupplier_id INT DEFAULT NULL, INDEX IDX_4B3656602ADD6D8C (supplier_id), INDEX IDX_4B365660B4AA9D01 (altSupplier_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE purchase_order ADD CONSTRAINT FK_21E210B29395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE purchase_order ADD CONSTRAINT FK_21E210B2EEC75ED7 FOREIGN KEY (stockItem_id) REFERENCES stock (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE stock ADD CONSTRAINT FK_4B3656602ADD6D8C FOREIGN KEY (supplier_id) REFERENCES supplier (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE stock ADD CONSTRAINT FK_4B365660B4AA9D01 FOREIGN KEY (altSupplier_id) REFERENCES supplier (id) ON DELETE SET NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE stock DROP FOREIGN KEY FK_4B3656602ADD6D8C');
        $this->addSql('ALTER TABLE stock DROP FOREIGN KEY FK_4B365660B4AA9D01');
        $this->addSql('ALTER TABLE purchase_order DROP FOREIGN KEY FK_21E210B2EEC75ED7');
        $this->addSql('DROP TABLE supplier');
        $this->addSql('DROP TABLE purchase_order');
        $this->addSql('DROP TABLE stock');
    }
}
