<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140718110644 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE truck CHANGE sale_price sale_price NUMERIC(8, 2) DEFAULT NULL, CHANGE hire_price_monthly hire_price_monthly NUMERIC(8, 2) DEFAULT NULL, CHANGE maintenance_revenue maintenance_revenue NUMERIC(8, 2) DEFAULT NULL, CHANGE maintenance_expenditure maintenance_expenditure NUMERIC(8, 2) DEFAULT NULL, CHANGE hire_rate hire_rate NUMERIC(8, 2) DEFAULT NULL');
        $this->addSql('ALTER TABLE stock CHANGE issue_price issue_price NUMERIC(8, 2) DEFAULT NULL, CHANGE list_price list_price NUMERIC(8, 2) DEFAULT NULL');
        $this->addSql('ALTER TABLE job ADD estimate NUMERIC(8, 2) DEFAULT NULL, CHANGE completion_stage completion_stage INT NOT NULL');
        $this->addSql('ALTER TABLE customer CHANGE labour_rate labour_rate NUMERIC(8, 2) DEFAULT NULL, CHANGE parts_discount parts_discount NUMERIC(8, 2) DEFAULT NULL');
        $this->addSql('ALTER TABLE supplier_stock CHANGE cost_price cost_price NUMERIC(8, 2) DEFAULT NULL, CHANGE carriage_price carriage_price NUMERIC(8, 2) DEFAULT NULL');
        $this->addSql('ALTER TABLE purchase_order ADD job_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE purchase_order ADD CONSTRAINT FK_21E210B2BE04EA9 FOREIGN KEY (job_id) REFERENCES job (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_21E210B2BE04EA9 ON purchase_order (job_id)');
        $this->addSql('ALTER TABLE job ADD is_live TINYINT(1) NOT NULL, ADD urgent TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE job ADD createdAt DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE customer CHANGE labour_rate labour_rate NUMERIC(10, 0) DEFAULT NULL, CHANGE parts_discount parts_discount NUMERIC(10, 0) DEFAULT NULL');
        $this->addSql('ALTER TABLE job DROP estimate, CHANGE completion_stage completion_stage INT DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE stock CHANGE issue_price issue_price DOUBLE PRECISION DEFAULT NULL, CHANGE list_price list_price DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE supplier_stock CHANGE cost_price cost_price DOUBLE PRECISION DEFAULT NULL, CHANGE carriage_price carriage_price DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE truck CHANGE maintenance_revenue maintenance_revenue DOUBLE PRECISION DEFAULT NULL, CHANGE maintenance_expenditure maintenance_expenditure DOUBLE PRECISION DEFAULT NULL, CHANGE hire_rate hire_rate NUMERIC(10, 0) DEFAULT NULL, CHANGE sale_price sale_price DOUBLE PRECISION DEFAULT NULL, CHANGE hire_price_monthly hire_price_monthly DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE purchase_order DROP FOREIGN KEY FK_21E210B2BE04EA9');
        $this->addSql('DROP INDEX IDX_21E210B2BE04EA9 ON purchase_order');
        $this->addSql('ALTER TABLE purchase_order DROP job_id');
        $this->addSql('ALTER TABLE job DROP is_live, DROP urgent');
        $this->addSql('ALTER TABLE job DROP createdAt');
    }
}
