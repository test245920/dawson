<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140813152126 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE job ADD do_with_next_job TINYINT(1) NOT NULL, ADD doWithJob_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE job ADD CONSTRAINT FK_FBD8E0F815682EFC FOREIGN KEY (doWithJob_id) REFERENCES job (id)');
        $this->addSql('CREATE INDEX IDX_FBD8E0F815682EFC ON job (doWithJob_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE job DROP FOREIGN KEY FK_FBD8E0F815682EFC');
        $this->addSql('DROP INDEX IDX_FBD8E0F815682EFC ON job');
        $this->addSql('ALTER TABLE job DROP do_with_next_job, DROP doWithJob_id');
    }
}
