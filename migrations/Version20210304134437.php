<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210304134437 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE work_log ADD time_labour_ot INT NOT NULL, ADD time_labour_eot INT NOT NULL, ADD time_travel_ot INT NOT NULL, ADD time_travel_eot INT NOT NULL, DROP labour_cost_multiplier, DROP travel_cost_multiplier');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE work_log ADD labour_cost_multiplier NUMERIC(8, 2) DEFAULT NULL, ADD travel_cost_multiplier NUMERIC(8, 2) DEFAULT NULL, DROP time_labour_ot, DROP time_labour_eot, DROP time_travel_ot, DROP time_travel_eot');
    }
}
