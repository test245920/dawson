<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160513122629 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE truck_transaction (id INT AUTO_INCREMENT NOT NULL, truck_id INT DEFAULT NULL, date DATETIME NOT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, deletedAt DATETIME DEFAULT NULL, amount INT NOT NULL, transactionCategory_id INT DEFAULT NULL, INDEX IDX_FD975413C6957CCE (truck_id), INDEX IDX_FD9754131807D5FC (transactionCategory_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE truck_transaction ADD CONSTRAINT FK_FD975413C6957CCE FOREIGN KEY (truck_id) REFERENCES truck (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE truck_transaction ADD CONSTRAINT FK_FD9754131807D5FC FOREIGN KEY (transactionCategory_id) REFERENCES transaction_category (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE truck_transaction');
    }
}
