<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140812093117 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE estimate_event ADD deletedAt DATETIME DEFAULT NULL, ADD date DATETIME NOT NULL, CHANGE isCustomerAction isCustomerAction TINYINT(1) DEFAULT NULL, CHANGE adminResponse adminResponse VARCHAR(255) DEFAULT NULL, CHANGE customerQuery customerQuery VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE truckmodel CHANGE make make VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE estimate_event DROP deletedAt, DROP date, CHANGE isCustomerAction isCustomerAction TINYINT(1) NOT NULL, CHANGE adminResponse adminResponse VARCHAR(255) NOT NULL, CHANGE customerQuery customerQuery VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE truckmodel CHANGE make make VARCHAR(255) NOT NULL');
    }
}
