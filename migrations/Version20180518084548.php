<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180518084548 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE asset_lifecycle_period ADD maintenance_rate_weekly NUMERIC(8, 2) DEFAULT NULL, ADD loler_interval INT DEFAULT NULL, ADD service_interval INT DEFAULT NULL, CHANGE maintenance_rate maintenance_rate_monthly NUMERIC(8, 2) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE asset_lifecycle_period ADD maintenance_rate NUMERIC(8, 2) DEFAULT NULL, DROP maintenance_rate_weekly, CHANGE maintenance_rate_monthly maintenance_rate, DROP loler_interval, DROP service_interval');
    }
}
