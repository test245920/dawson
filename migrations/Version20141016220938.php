<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20141016220938 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE estimate DROP FOREIGN KEY FK_D2EA460781398E09');
        $this->addSql('DROP INDEX IDX_D2EA460781398E09 ON estimate');
        $this->addSql('ALTER TABLE estimate ADD job_id INT DEFAULT NULL, CHANGE customer customer_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE estimate ADD CONSTRAINT FK_D2EA46079395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id)');
        $this->addSql('ALTER TABLE estimate ADD CONSTRAINT FK_D2EA4607BE04EA9 FOREIGN KEY (job_id) REFERENCES job (id)');
        $this->addSql('CREATE INDEX IDX_D2EA46079395C3F3 ON estimate (customer_id)');
        $this->addSql('CREATE INDEX IDX_D2EA4607BE04EA9 ON estimate (job_id)');
        $this->addSql('ALTER TABLE job ADD cause VARCHAR(255) DEFAULT NULL, ADD requested_parts_text VARCHAR(255) DEFAULT NULL, ADD parts_ordered TINYINT(1) NOT NULL, DROP estimate');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE estimate DROP FOREIGN KEY FK_D2EA46079395C3F3');
        $this->addSql('ALTER TABLE estimate DROP FOREIGN KEY FK_D2EA4607BE04EA9');
        $this->addSql('DROP INDEX IDX_D2EA46079395C3F3 ON estimate');
        $this->addSql('DROP INDEX IDX_D2EA4607BE04EA9 ON estimate');
        $this->addSql('ALTER TABLE estimate ADD customer INT DEFAULT NULL, DROP customer_id, DROP job_id');
        $this->addSql('ALTER TABLE estimate ADD CONSTRAINT FK_D2EA460781398E09 FOREIGN KEY (customer) REFERENCES customer (id)');
        $this->addSql('CREATE INDEX IDX_D2EA460781398E09 ON estimate (customer)');
        $this->addSql('ALTER TABLE job ADD estimate NUMERIC(8, 2) DEFAULT NULL, DROP cause, DROP requested_parts_text, DROP parts_ordered');
    }
}
