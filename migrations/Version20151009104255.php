<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151009104255 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE stock_transaction (id INT AUTO_INCREMENT NOT NULL, job_id INT DEFAULT NULL, date DATETIME NOT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, deletedAt DATETIME DEFAULT NULL, quantity INT NOT NULL, purchaseOrder_id INT DEFAULT NULL, transactionCategory_id INT DEFAULT NULL, INDEX IDX_ADF9A3E5BE04EA9 (job_id), INDEX IDX_ADF9A3E51B7B246A (purchaseOrder_id), INDEX IDX_ADF9A3E51807D5FC (transactionCategory_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stock_stocktransaction (stock_id INT NOT NULL, stocktransaction_id INT NOT NULL, INDEX IDX_1E053800DCD6110 (stock_id), INDEX IDX_1E05380040FA1C5 (stocktransaction_id), PRIMARY KEY(stock_id, stocktransaction_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transaction_category (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) NOT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, deletedAt DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE stock_transaction ADD CONSTRAINT FK_ADF9A3E5BE04EA9 FOREIGN KEY (job_id) REFERENCES job (id)');
        $this->addSql('ALTER TABLE stock_transaction ADD CONSTRAINT FK_ADF9A3E51B7B246A FOREIGN KEY (purchaseOrder_id) REFERENCES purchase_order (id)');
        $this->addSql('ALTER TABLE stock_transaction ADD CONSTRAINT FK_ADF9A3E51807D5FC FOREIGN KEY (transactionCategory_id) REFERENCES transaction_category (id)');
        $this->addSql('ALTER TABLE stock_stocktransaction ADD CONSTRAINT FK_1E053800DCD6110 FOREIGN KEY (stock_id) REFERENCES stock (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE stock_stocktransaction ADD CONSTRAINT FK_1E05380040FA1C5 FOREIGN KEY (stocktransaction_id) REFERENCES stock_transaction (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE stock_stocktransaction DROP FOREIGN KEY FK_1E05380040FA1C5');
        $this->addSql('ALTER TABLE stock_transaction DROP FOREIGN KEY FK_ADF9A3E51807D5FC');
        $this->addSql('DROP TABLE stock_transaction');
        $this->addSql('DROP TABLE stock_stocktransaction');
        $this->addSql('DROP TABLE transaction_category');
    }
}
