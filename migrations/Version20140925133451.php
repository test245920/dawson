<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140925133451 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE truck DROP FOREIGN KEY FK_CDCCF30AD79572D9');
        $this->addSql('CREATE TABLE truck_make (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, deletedAt DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE truck_model (id INT AUTO_INCREMENT NOT NULL, make_id INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, mast TINYTEXT DEFAULT NULL, mastHeight INT DEFAULT NULL, hydraulics LONGTEXT DEFAULT NULL, attachments LONGTEXT DEFAULT NULL, yom INT DEFAULT NULL, weather_protection LONGTEXT DEFAULT NULL, tyre_type LONGTEXT DEFAULT NULL, performance_indicator LONGTEXT DEFAULT NULL, deletedAt DATETIME DEFAULT NULL, INDEX IDX_564EC303CFBF73EB (make_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE truck_model ADD CONSTRAINT FK_564EC303CFBF73EB FOREIGN KEY (make_id) REFERENCES truck_make (id)');
        $this->addSql('DROP TABLE truckmodel');
        $this->addSql('DROP INDEX IDX_CDCCF30AD79572D9 ON truck');
        $this->addSql('ALTER TABLE truck ADD name INT DEFAULT NULL, ADD loler_interval INT DEFAULT NULL, ADD service_interval INT DEFAULT NULL, ADD warranty LONGTEXT DEFAULT NULL, ADD siteAddress_id INT DEFAULT NULL, CHANGE model truck_make INT DEFAULT NULL');
        $this->addSql('ALTER TABLE truck ADD CONSTRAINT FK_CDCCF30A7F6B61BA FOREIGN KEY (truck_make) REFERENCES truck_make (id)');
        $this->addSql('ALTER TABLE truck ADD CONSTRAINT FK_CDCCF30A5E237E06 FOREIGN KEY (name) REFERENCES truck_model (id)');
        $this->addSql('ALTER TABLE truck ADD CONSTRAINT FK_CDCCF30A917C7BCD FOREIGN KEY (siteAddress_id) REFERENCES site_address (id)');
        $this->addSql('CREATE INDEX IDX_CDCCF30A7F6B61BA ON truck (truck_make)');
        $this->addSql('CREATE INDEX IDX_CDCCF30A5E237E06 ON truck (name)');
        $this->addSql('CREATE INDEX IDX_CDCCF30A917C7BCD ON truck (siteAddress_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE truck DROP FOREIGN KEY FK_CDCCF30A7F6B61BA');
        $this->addSql('ALTER TABLE truck_model DROP FOREIGN KEY FK_564EC303CFBF73EB');
        $this->addSql('ALTER TABLE truck DROP FOREIGN KEY FK_CDCCF30A5E237E06');
        $this->addSql('CREATE TABLE truckmodel (id INT AUTO_INCREMENT NOT NULL, make VARCHAR(255) DEFAULT NULL, model VARCHAR(255) DEFAULT NULL, mast LONGTEXT DEFAULT NULL, hydraulics LONGTEXT DEFAULT NULL, attachments LONGTEXT DEFAULT NULL, yom INT DEFAULT NULL, weather_protection LONGTEXT DEFAULT NULL, tyre_type LONGTEXT DEFAULT NULL, loler_interval INT DEFAULT NULL, service_interval INT DEFAULT NULL, warranty LONGTEXT DEFAULT NULL, performance_indicator LONGTEXT DEFAULT NULL, deletedAt DATETIME DEFAULT NULL, mastHeight INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE truck_make');
        $this->addSql('DROP TABLE truck_model');
        $this->addSql('DROP INDEX IDX_CDCCF30A7F6B61BA ON truck');
        $this->addSql('DROP INDEX IDX_CDCCF30A5E237E06 ON truck');
        $this->addSql('DROP INDEX IDX_CDCCF30A917C7BCD ON truck');
        $this->addSql('ALTER TABLE truck ADD model INT DEFAULT NULL, DROP truck_make, DROP name, DROP loler_interval, DROP service_interval, DROP warranty, DROP siteAddress_id');
        $this->addSql('ALTER TABLE truck ADD CONSTRAINT FK_CDCCF30AD79572D9 FOREIGN KEY (model) REFERENCES truckmodel (id)');
        $this->addSql('CREATE INDEX IDX_CDCCF30AD79572D9 ON truck (model)');
    }
}
