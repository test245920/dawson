<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150323215407 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE supplier DROP contactName');
        $this->addSql('ALTER TABLE site_address DROP address_string');
        $this->addSql('ALTER TABLE billing_address DROP address_string');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE billing_address ADD address_string VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE site_address ADD address_string VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE supplier ADD contactName LONGTEXT DEFAULT NULL');
    }
}
