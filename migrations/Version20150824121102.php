<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150824121102 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE job ADD uuid LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE image ADD job_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE image ADD CONSTRAINT FK_C53D045FBE04EA9 FOREIGN KEY (job_id) REFERENCES job (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_C53D045FBE04EA9 ON image (job_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE image DROP FOREIGN KEY FK_C53D045FBE04EA9');
        $this->addSql('DROP INDEX IDX_C53D045FBE04EA9 ON image');
        $this->addSql('ALTER TABLE image DROP job_id');
        $this->addSql('ALTER TABLE job DROP uuid');
    }
}
