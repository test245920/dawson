<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140708154517 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE supplier ADD address2 TINYTEXT DEFAULT NULL, ADD city TINYTEXT DEFAULT NULL, ADD county TINYTEXT DEFAULT NULL, ADD country TINYTEXT DEFAULT NULL, ADD postcode TINYTEXT DEFAULT NULL, CHANGE address address1 TINYTEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE supplier ADD address LONGTEXT DEFAULT NULL, DROP address1, DROP address2, DROP city, DROP county, DROP country, DROP postcode');
    }
}
