<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220207150319 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE job ADD is_recurring TINYINT(1) NOT NULL, ADD recurring_type INT DEFAULT NULL, ADD job_recurring_time DATETIME DEFAULT NULL, CHANGE signature_email_resent signature_email_resent TINYINT(1) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE job DROP is_recurring, DROP recurring_type, DROP job_recurring_time, CHANGE signature_email_resent signature_email_resent INT DEFAULT NULL');
    }
}
