<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151027174513 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE future_work_recommendation (id INT AUTO_INCREMENT NOT NULL, job_id INT DEFAULT NULL, task LONGTEXT DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, created_at DATETIME DEFAULT NULL, INDEX IDX_BF19C30BBE04EA9 (job_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE future_work_recommendation ADD CONSTRAINT FK_BF19C30BBE04EA9 FOREIGN KEY (job_id) REFERENCES job (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE job DROP possible_additional_work');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE future_work_recommendation');
        $this->addSql('ALTER TABLE job ADD possible_additional_work LONGTEXT DEFAULT NULL');
    }
}
