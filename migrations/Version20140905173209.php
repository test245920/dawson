<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140905173209 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('CREATE TABLE stock_item (id INT AUTO_INCREMENT NOT NULL, stock_id INT DEFAULT NULL, job_id INT DEFAULT NULL, in_stock TINYINT(1) NOT NULL, sold DATETIME DEFAULT NULL, purchaseOrder_id INT DEFAULT NULL, INDEX IDX_6017DDADCD6110 (stock_id), INDEX IDX_6017DDABE04EA9 (job_id), INDEX IDX_6017DDA1B7B246A (purchaseOrder_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE purchase_order_item (id INT AUTO_INCREMENT NOT NULL, item_id INT DEFAULT NULL, unitCost NUMERIC(8, 2) DEFAULT NULL, quantity INT NOT NULL, received_date DATETIME DEFAULT NULL, purchaseOrder_id INT DEFAULT NULL, INDEX IDX_5ED948C3126F525E (item_id), INDEX IDX_5ED948C31B7B246A (purchaseOrder_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE stock_item ADD CONSTRAINT FK_6017DDADCD6110 FOREIGN KEY (stock_id) REFERENCES stock (id)');
        $this->addSql('ALTER TABLE stock_item ADD CONSTRAINT FK_6017DDABE04EA9 FOREIGN KEY (job_id) REFERENCES job (id)');
        $this->addSql('ALTER TABLE stock_item ADD CONSTRAINT FK_6017DDA1B7B246A FOREIGN KEY (purchaseOrder_id) REFERENCES purchase_order (id)');
        $this->addSql('ALTER TABLE purchase_order_item ADD CONSTRAINT FK_5ED948C3126F525E FOREIGN KEY (item_id) REFERENCES supplier_stock (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE purchase_order_item ADD CONSTRAINT FK_5ED948C31B7B246A FOREIGN KEY (purchaseOrder_id) REFERENCES purchase_order (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE purchase_order DROP FOREIGN KEY FK_21E210B2EEC75ED7');
        $this->addSql('DROP INDEX UNIQ_21E210B2EEC75ED7 ON purchase_order');
        $this->addSql('ALTER TABLE purchase_order ADD is_job_purchase TINYINT(1) NOT NULL, ADD is_stock_purchase TINYINT(1) NOT NULL, DROP stockItem_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('DROP TABLE stock_item');
        $this->addSql('DROP TABLE purchase_order_item');
        $this->addSql('ALTER TABLE purchase_order ADD stockItem_id INT DEFAULT NULL, DROP is_job_purchase, DROP is_stock_purchase');
        $this->addSql('ALTER TABLE purchase_order ADD CONSTRAINT FK_21E210B2EEC75ED7 FOREIGN KEY (stockItem_id) REFERENCES stock (id) ON DELETE SET NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_21E210B2EEC75ED7 ON purchase_order (stockItem_id)');
    }
}
