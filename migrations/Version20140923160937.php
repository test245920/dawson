<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140923160937 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('CREATE TABLE site_address (id INT AUTO_INCREMENT NOT NULL, customer INT DEFAULT NULL, address_1 VARCHAR(255) DEFAULT NULL, address_2 VARCHAR(255) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, postcode VARCHAR(8) DEFAULT NULL, deletedAt DATETIME DEFAULT NULL, address_string VARCHAR(255) DEFAULT NULL, INDEX IDX_F6EA8D4A81398E09 (customer), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE billing_address (id INT AUTO_INCREMENT NOT NULL, customer INT DEFAULT NULL, address_1 VARCHAR(255) DEFAULT NULL, address_2 VARCHAR(255) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, postcode VARCHAR(8) DEFAULT NULL, deletedAt DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_6660E45681398E09 (customer), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE site_address ADD CONSTRAINT FK_F6EA8D4A81398E09 FOREIGN KEY (customer) REFERENCES customer (id)');
        $this->addSql('ALTER TABLE billing_address ADD CONSTRAINT FK_6660E45681398E09 FOREIGN KEY (customer) REFERENCES customer (id)');
        $this->addSql('ALTER TABLE truck ADD purchased_date DATETIME DEFAULT NULL, ADD email VARCHAR(255) DEFAULT NULL, ADD telephone VARCHAR(255) DEFAULT NULL, DROP other_info');
        $this->addSql('ALTER TABLE customer DROP site_address, DROP postcode, DROP billing_address');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('DROP TABLE site_address');
        $this->addSql('DROP TABLE billing_address');
        $this->addSql('ALTER TABLE customer ADD site_address VARCHAR(255) DEFAULT NULL, ADD postcode LONGTEXT DEFAULT NULL, ADD billing_address LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE truck ADD other_info LONGTEXT DEFAULT NULL, DROP purchased_date, DROP email, DROP telephone');
    }
}
