<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170529145419 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE estimate_stock DROP FOREIGN KEY FK_3C7767AFDCD6110');
        $this->addSql('DROP INDEX IDX_3C7767AFDCD6110 ON estimate_stock');
        $this->addSql('ALTER TABLE estimate_stock CHANGE stock_id supplierStock_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE estimate_stock ADD CONSTRAINT FK_3C7767AF1969624 FOREIGN KEY (supplierStock_id) REFERENCES supplier_stock (id)');
        $this->addSql('CREATE INDEX IDX_3C7767AF1969624 ON estimate_stock (supplierStock_id)');
        $this->addSql('ALTER TABLE stock DROP stock_quantity');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE estimate_stock DROP FOREIGN KEY FK_3C7767AF1969624');
        $this->addSql('DROP INDEX IDX_3C7767AF1969624 ON estimate_stock');
        $this->addSql('ALTER TABLE estimate_stock CHANGE supplierstock_id stock_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE estimate_stock ADD CONSTRAINT FK_3C7767AFDCD6110 FOREIGN KEY (stock_id) REFERENCES stock (id)');
        $this->addSql('CREATE INDEX IDX_3C7767AFDCD6110 ON estimate_stock (stock_id)');
        $this->addSql('ALTER TABLE stock ADD stock_quantity INT NOT NULL');
    }
}
