<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20141205132710 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE estimate CHANGE price price NUMERIC(8, 2) DEFAULT NULL');
        $this->addSql('ALTER TABLE job DROP FOREIGN KEY FK_FBD8E0F843656FE6');
        $this->addSql('DROP INDEX IDX_FBD8E0F843656FE6 ON job');
        $this->addSql('ALTER TABLE job DROP billingAddress_id');
        $this->addSql('ALTER TABLE billing_address ADD address_string VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE billing_address DROP address_string');
        $this->addSql('ALTER TABLE estimate CHANGE price price NUMERIC(10, 0) DEFAULT NULL');
        $this->addSql('ALTER TABLE job ADD billingAddress_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE job ADD CONSTRAINT FK_FBD8E0F843656FE6 FOREIGN KEY (billingAddress_id) REFERENCES billing_address (id)');
        $this->addSql('CREATE INDEX IDX_FBD8E0F843656FE6 ON job (billingAddress_id)');
    }
}
