<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140808152230 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('CREATE TABLE estimate (id INT AUTO_INCREMENT NOT NULL, customer INT DEFAULT NULL, details VARCHAR(255) NOT NULL, awaitingResponse TINYINT(1) NOT NULL, accepted TINYINT(1) NOT NULL, rejected TINYINT(1) NOT NULL, queried TINYINT(1) NOT NULL, queryText VARCHAR(255) DEFAULT NULL, INDEX IDX_D2EA460781398E09 (customer), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE estimate_event (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, isCustomerAction TINYINT(1) NOT NULL, isAdminResponse TINYINT(1) NOT NULL, adminResponse VARCHAR(255) NOT NULL, accepted TINYINT(1) NOT NULL, rejected TINYINT(1) NOT NULL, isCustomerQuery TINYINT(1) NOT NULL, customerQuery VARCHAR(255) NOT NULL, estimateEvents INT DEFAULT NULL, INDEX IDX_4CEF3B68A9528F2F (estimateEvents), INDEX IDX_4CEF3B68A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE estimate ADD CONSTRAINT FK_D2EA460781398E09 FOREIGN KEY (customer) REFERENCES customer (id)');
        $this->addSql('ALTER TABLE estimate_event ADD CONSTRAINT FK_4CEF3B68A9528F2F FOREIGN KEY (estimateEvents) REFERENCES estimate (id)');
        $this->addSql('ALTER TABLE estimate_event ADD CONSTRAINT FK_4CEF3B68A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE estimate_event DROP FOREIGN KEY FK_4CEF3B68A9528F2F');
        $this->addSql('DROP TABLE estimate');
        $this->addSql('DROP TABLE estimate_event');
    }
}
