<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220303135558 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE asset_lifecycle_period DROP INDEX UNIQ_7B1F27D4DF7711CC, ADD INDEX IDX_7B1F27D4DF7711CC (parentPeriod_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE asset_lifecycle_period DROP INDEX IDX_7B1F27D4DF7711CC, ADD UNIQUE INDEX UNIQ_7B1F27D4DF7711CC (parentPeriod_id)');
    }
}
