<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20141003115722 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('CREATE TABLE supplier_address (id INT AUTO_INCREMENT NOT NULL, supplier_id INT DEFAULT NULL, address_1 VARCHAR(255) DEFAULT NULL, address_2 VARCHAR(255) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, county VARCHAR(255) DEFAULT NULL, country VARCHAR(255) DEFAULT NULL, postcode VARCHAR(8) DEFAULT NULL, deletedAt DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_8C2B1B9E2ADD6D8C (supplier_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE supplier_address ADD CONSTRAINT FK_8C2B1B9E2ADD6D8C FOREIGN KEY (supplier_id) REFERENCES supplier (id)');
        $this->addSql('ALTER TABLE supplier DROP address1, DROP address2, DROP city, DROP county, DROP country, DROP postcode');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('DROP TABLE supplier_address');
        $this->addSql('ALTER TABLE supplier ADD address1 LONGTEXT DEFAULT NULL, ADD address2 LONGTEXT DEFAULT NULL, ADD city LONGTEXT DEFAULT NULL, ADD county LONGTEXT DEFAULT NULL, ADD country LONGTEXT DEFAULT NULL, ADD postcode LONGTEXT DEFAULT NULL');
    }
}
