<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210923091210 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE asset_lifecycle_period ADD customer_contact_id INT DEFAULT NULL AFTER customer_id');
        $this->addSql('ALTER TABLE asset_lifecycle_period ADD CONSTRAINT FK_7B1F27D41A6821C8 FOREIGN KEY (customer_contact_id) REFERENCES contact (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_7B1F27D41A6821C8 ON asset_lifecycle_period (customer_contact_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE asset_lifecycle_period DROP FOREIGN KEY FK_7B1F27D41A6821C8');
        $this->addSql('DROP INDEX IDX_7B1F27D41A6821C8 ON asset_lifecycle_period');
        $this->addSql('ALTER TABLE asset_lifecycle_period DROP customer_contact_id');
    }
}
