<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210226154720 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE work_log ADD user_cost_multiplier NUMERIC(8, 2) DEFAULT NULL, ADD labour_cost_multiplier NUMERIC(8, 2) DEFAULT NULL, ADD travel_cost_multiplier NUMERIC(8, 2) DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD cost_multiplier NUMERIC(8, 2) DEFAULT NULL');

        $this->addSql('UPDATE user SET cost_multiplier=1');
        $this->addSql('UPDATE work_log SET user_cost_multiplier=1, labour_cost_multiplier=1, travel_cost_multiplier=1');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user DROP cost_multiplier');
        $this->addSql('ALTER TABLE work_log DROP user_cost_multiplier, DROP labour_cost_multiplier, DROP travel_cost_multiplier');
    }
}
