<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20141006144159 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE truck DROP FOREIGN KEY FK_CDCCF30A81398E09');
        $this->addSql('DROP INDEX IDX_CDCCF30A81398E09 ON truck');
        $this->addSql('ALTER TABLE truck CHANGE customer customer_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE truck ADD CONSTRAINT FK_CDCCF30A9395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id)');
        $this->addSql('CREATE INDEX IDX_CDCCF30A9395C3F3 ON truck (customer_id)');
        $this->addSql('ALTER TABLE purchase_order DROP quantity');
        $this->addSql('ALTER TABLE stock DROP FOREIGN KEY FK_4B3656602ADD6D8C');
        $this->addSql('DROP INDEX IDX_4B3656602ADD6D8C ON stock');
        $this->addSql('ALTER TABLE stock DROP supplier_id, CHANGE code code VARCHAR(50) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE purchase_order ADD quantity INT NOT NULL');
        $this->addSql('ALTER TABLE stock ADD supplier_id INT DEFAULT NULL, CHANGE code code VARCHAR(50) NOT NULL');
        $this->addSql('ALTER TABLE stock ADD CONSTRAINT FK_4B3656602ADD6D8C FOREIGN KEY (supplier_id) REFERENCES supplier (id)');
        $this->addSql('CREATE INDEX IDX_4B3656602ADD6D8C ON stock (supplier_id)');
        $this->addSql('ALTER TABLE truck DROP FOREIGN KEY FK_CDCCF30A9395C3F3');
        $this->addSql('DROP INDEX IDX_CDCCF30A9395C3F3 ON truck');
        $this->addSql('ALTER TABLE truck CHANGE customer_id customer INT DEFAULT NULL');
        $this->addSql('ALTER TABLE truck ADD CONSTRAINT FK_CDCCF30A81398E09 FOREIGN KEY (customer) REFERENCES customer (id)');
        $this->addSql('CREATE INDEX IDX_CDCCF30A81398E09 ON truck (customer)');
    }
}
