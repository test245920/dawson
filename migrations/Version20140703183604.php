<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140703183604 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE truck ADD customer INT DEFAULT NULL, ADD model LONGTEXT DEFAULT NULL, ADD serial LONGTEXT DEFAULT NULL, ADD mast TINYTEXT DEFAULT NULL, ADD hydraulics LONGTEXT DEFAULT NULL, ADD attachments LONGTEXT DEFAULT NULL, ADD yom INT DEFAULT NULL, ADD maintenance_revenue DOUBLE PRECISION DEFAULT NULL, ADD maintenance_expenditure DOUBLE PRECISION DEFAULT NULL, ADD performance_indicator LONGTEXT DEFAULT NULL, ADD purchase TINYINT(1) DEFAULT NULL, ADD contract_hire TINYINT(1) DEFAULT NULL, ADD casual_hire TINYINT(1) DEFAULT NULL, ADD maintenance LONGTEXT DEFAULT NULL, ADD expected_utilisation LONGTEXT DEFAULT NULL, ADD loler_interval INT DEFAULT NULL, ADD hire_term INT DEFAULT NULL, ADD hire_rate NUMERIC(10, 0) DEFAULT NULL, ADD max_utilisation LONGTEXT DEFAULT NULL, ADD other_info LONGTEXT DEFAULT NULL, ADD client_flt_num LONGTEXT DEFAULT NULL, CHANGE warranty warranty LONGTEXT DEFAULT NULL, CHANGE basic_specification make LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE truck ADD CONSTRAINT FK_CDCCF30A81398E09 FOREIGN KEY (customer) REFERENCES customer (id)');
        $this->addSql('CREATE INDEX IDX_CDCCF30A81398E09 ON truck (customer)');
        $this->addSql('ALTER TABLE customer_number ADD active TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE customer CHANGE site_address site_address VARCHAR(255) DEFAULT NULL, CHANGE email email VARCHAR(50) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE customer CHANGE site_address site_address LONGTEXT DEFAULT NULL, CHANGE email email LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE customer_number DROP active');
        $this->addSql('ALTER TABLE truck DROP FOREIGN KEY FK_CDCCF30A81398E09');
        $this->addSql('DROP INDEX IDX_CDCCF30A81398E09 ON truck');
        $this->addSql('ALTER TABLE truck ADD basic_specification LONGTEXT DEFAULT NULL, DROP customer, DROP make, DROP model, DROP serial, DROP mast, DROP hydraulics, DROP attachments, DROP yom, DROP maintenance_revenue, DROP maintenance_expenditure, DROP performance_indicator, DROP purchase, DROP contract_hire, DROP casual_hire, DROP maintenance, DROP expected_utilisation, DROP loler_interval, DROP hire_term, DROP hire_rate, DROP max_utilisation, DROP other_info, DROP client_flt_num, CHANGE warranty warranty INT DEFAULT NULL');
    }
}
