<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150824155952 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE estimate_item ADD uuid LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE image ADD estimateItem_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE image ADD CONSTRAINT FK_C53D045F2F4D1251 FOREIGN KEY (estimateItem_id) REFERENCES estimate_item (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_C53D045F2F4D1251 ON image (estimateItem_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE estimate_item DROP uuid');
        $this->addSql('ALTER TABLE image DROP FOREIGN KEY FK_C53D045F2F4D1251');
        $this->addSql('DROP INDEX IDX_C53D045F2F4D1251 ON image');
        $this->addSql('ALTER TABLE image DROP estimateItem_id');
    }
}
