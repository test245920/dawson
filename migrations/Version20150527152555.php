<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150527152555 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_4B36566077153098 ON stock');
        $this->addSql('ALTER TABLE stock CHANGE issue_price issue_price NUMERIC(20, 10) DEFAULT NULL, CHANGE list_price list_price NUMERIC(20, 10) DEFAULT NULL, CHANGE oem_price oem_price NUMERIC(20, 10) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE stock CHANGE issue_price issue_price NUMERIC(8, 2) DEFAULT NULL, CHANGE list_price list_price NUMERIC(8, 2) DEFAULT NULL, CHANGE oem_price oem_price NUMERIC(8, 2) DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4B36566077153098 ON stock (code)');
    }
}
