<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151012155802 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE estimate DROP FOREIGN KEY FK_D2EA460755458D');
        $this->addSql('DROP INDEX IDX_D2EA460755458D ON estimate');
        $this->addSql('ALTER TABLE estimate DROP lead_id');
        $this->addSql('ALTER TABLE billing_address DROP INDEX IDX_6660E45664D218E, ADD UNIQUE INDEX UNIQ_6660E45664D218E (location_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE billing_address DROP INDEX UNIQ_6660E45664D218E, ADD INDEX IDX_6660E45664D218E (location_id)');
        $this->addSql('ALTER TABLE estimate ADD lead_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE estimate ADD CONSTRAINT FK_D2EA460755458D FOREIGN KEY (lead_id) REFERENCES lead (id)');
        $this->addSql('CREATE INDEX IDX_D2EA460755458D ON estimate (lead_id)');
    }
}
