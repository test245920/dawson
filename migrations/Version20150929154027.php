<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150929154027 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE location (id INT AUTO_INCREMENT NOT NULL, engineer_id INT DEFAULT NULL, type VARCHAR(255) DEFAULT NULL, deletedAt DATETIME DEFAULT NULL, siteAddress_id INT DEFAULT NULL, billingAddress_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_5E9E89CB917C7BCD (siteAddress_id), UNIQUE INDEX UNIQ_5E9E89CB43656FE6 (billingAddress_id), UNIQUE INDEX UNIQ_5E9E89CBF8D8CDF1 (engineer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE location ADD CONSTRAINT FK_5E9E89CB917C7BCD FOREIGN KEY (siteAddress_id) REFERENCES site_address (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE location ADD CONSTRAINT FK_5E9E89CB43656FE6 FOREIGN KEY (billingAddress_id) REFERENCES billing_address (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE location ADD CONSTRAINT FK_5E9E89CBF8D8CDF1 FOREIGN KEY (engineer_id) REFERENCES user (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE site_address ADD is_stock_location TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE stock DROP location');
        $this->addSql('ALTER TABLE stock_item ADD location_id INT DEFAULT NULL, DROP in_stock');
        $this->addSql('ALTER TABLE stock_item ADD CONSTRAINT FK_6017DDA64D218E FOREIGN KEY (location_id) REFERENCES location (id)');
        $this->addSql('CREATE INDEX IDX_6017DDA64D218E ON stock_item (location_id)');
        $this->addSql('ALTER TABLE customer ADD is_internal_customer TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE user ADD location_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64964D218E FOREIGN KEY (location_id) REFERENCES location (id)');
        $this->addSql('CREATE INDEX IDX_8D93D64964D218E ON user (location_id)');
        $this->addSql('ALTER TABLE billing_address ADD location_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE billing_address ADD CONSTRAINT FK_6660E45664D218E FOREIGN KEY (location_id) REFERENCES location (id)');
        $this->addSql('CREATE INDEX IDX_6660E45664D218E ON billing_address (location_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE stock_item DROP FOREIGN KEY FK_6017DDA64D218E');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64964D218E');
        $this->addSql('ALTER TABLE billing_address DROP FOREIGN KEY FK_6660E45664D218E');
        $this->addSql('DROP TABLE location');
        $this->addSql('DROP INDEX IDX_6660E45664D218E ON billing_address');
        $this->addSql('ALTER TABLE billing_address DROP location_id');
        $this->addSql('ALTER TABLE customer DROP is_internal_customer');
        $this->addSql('ALTER TABLE site_address DROP is_stock_location');
        $this->addSql('ALTER TABLE stock ADD location VARCHAR(50) DEFAULT NULL');
        $this->addSql('DROP INDEX IDX_6017DDA64D218E ON stock_item');
        $this->addSql('ALTER TABLE stock_item ADD in_stock TINYINT(1) NOT NULL, DROP location_id');
        $this->addSql('DROP INDEX IDX_8D93D64964D218E ON user');
        $this->addSql('ALTER TABLE user DROP location_id');
    }
}
