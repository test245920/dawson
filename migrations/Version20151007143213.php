<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151007143213 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE truck ADD tyre_type LONGTEXT DEFAULT NULL, ADD created_at DATETIME DEFAULT NULL, ADD updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE truck_model ADD rear_tyres LONGTEXT DEFAULT NULL, ADD front_tyres LONGTEXT DEFAULT NULL, ADD created_at DATETIME DEFAULT NULL, ADD updated_at DATETIME DEFAULT NULL, ADD fuel LONGTEXT DEFAULT NULL');

        $this->addSql('UPDATE truck_model SET front_tyres = (SELECT front_tyres FROM truck WHERE front_tyres IS NOT NULL AND model_id = truck_model.id LIMIT 1)');
        $this->addSql('UPDATE truck_model SET rear_tyres = (SELECT rear_tyres FROM truck WHERE rear_tyres IS NOT NULL AND model_id = truck_model.id LIMIT 1)');
        $this->addSql('UPDATE truck_model SET fuel = (SELECT fuel FROM truck WHERE fuel IS NOT NULL AND model_id = truck_model.id LIMIT 1)');
        $this->addSql('UPDATE truck SET tyre_type = (SELECT tyre_type FROM truck_model WHERE tyre_type IS NOT NULL AND id = truck.model_id LIMIT 1)');

        $this->addSql('ALTER TABLE truck DROP fuel, DROP front_tyres, DROP rear_tyres');
        $this->addSql('ALTER TABLE truck_model DROP tyre_type');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE truck_model ADD tyre_type LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE truck ADD rear_tyres LONGTEXT DEFAULT NULL, ADD front_tyres LONGTEXT DEFAULT NULL, ADD fuel LONGTEXT DEFAULT NULL');

        $this->addSql('UPDATE truck SET front_tyres = (SELECT front_tyres FROM truck_model WHERE front_tyres IS NOT NULL AND id = truck.model_id LIMIT 1)');
        $this->addSql('UPDATE truck SET rear_tyres = (SELECT rear_tyres FROM truck_model WHERE rear_tyres IS NOT NULL AND id = truck.model_id LIMIT 1)');
        $this->addSql('UPDATE truck SET fuel = (SELECT fuel FROM truck_model WHERE fuel IS NOT NULL AND id = truck.model_id LIMIT 1)');
        $this->addSql('UPDATE truck_model SET tyre_type = (SELECT tyre_type FROM truck WHERE tyre_type IS NOT NULL AND model_id = truck_model.id LIMIT 1)');

        $this->addSql('ALTER TABLE truck_model DROP fuel, DROP front_tyres, DROP rear_tyres, DROP created_at, DROP updated_at');
        $this->addSql('ALTER TABLE truck DROP tyre_type, DROP created_at, DROP updated_at');
    }
}
