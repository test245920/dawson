<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20141016154916 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE truck DROP FOREIGN KEY FK_CDCCF30A5E237E06');
        $this->addSql('DROP INDEX IDX_CDCCF30A5E237E06 ON truck');
        $this->addSql('ALTER TABLE truck ADD attachment_model_num LONGTEXT DEFAULT NULL, CHANGE name model_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE truck ADD CONSTRAINT FK_CDCCF30A7975B7E7 FOREIGN KEY (model_id) REFERENCES truck_model (id)');
        $this->addSql('CREATE INDEX IDX_CDCCF30A7975B7E7 ON truck (model_id)');
        $this->addSql('ALTER TABLE truck_model ADD oilFilter_id INT DEFAULT NULL, ADD airFilter_id INT DEFAULT NULL, ADD waterFilter_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE truck_model ADD CONSTRAINT FK_564EC303763C5D06 FOREIGN KEY (oilFilter_id) REFERENCES stock (id)');
        $this->addSql('ALTER TABLE truck_model ADD CONSTRAINT FK_564EC303A66A3F43 FOREIGN KEY (airFilter_id) REFERENCES stock (id)');
        $this->addSql('ALTER TABLE truck_model ADD CONSTRAINT FK_564EC303CE62929E FOREIGN KEY (waterFilter_id) REFERENCES stock (id)');
        $this->addSql('CREATE INDEX IDX_564EC303763C5D06 ON truck_model (oilFilter_id)');
        $this->addSql('CREATE INDEX IDX_564EC303A66A3F43 ON truck_model (airFilter_id)');
        $this->addSql('CREATE INDEX IDX_564EC303CE62929E ON truck_model (waterFilter_id)');
        $this->addSql('ALTER TABLE stock ADD stock_string VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE stock DROP stock_string');
        $this->addSql('ALTER TABLE truck DROP FOREIGN KEY FK_CDCCF30A7975B7E7');
        $this->addSql('DROP INDEX IDX_CDCCF30A7975B7E7 ON truck');
        $this->addSql('ALTER TABLE truck DROP attachment_model_num, CHANGE model_id name INT DEFAULT NULL');
        $this->addSql('ALTER TABLE truck ADD CONSTRAINT FK_CDCCF30A5E237E06 FOREIGN KEY (name) REFERENCES truck_model (id)');
        $this->addSql('CREATE INDEX IDX_CDCCF30A5E237E06 ON truck (name)');
        $this->addSql('ALTER TABLE truck_model DROP FOREIGN KEY FK_564EC303763C5D06');
        $this->addSql('ALTER TABLE truck_model DROP FOREIGN KEY FK_564EC303A66A3F43');
        $this->addSql('ALTER TABLE truck_model DROP FOREIGN KEY FK_564EC303CE62929E');
        $this->addSql('DROP INDEX IDX_564EC303763C5D06 ON truck_model');
        $this->addSql('DROP INDEX IDX_564EC303A66A3F43 ON truck_model');
        $this->addSql('DROP INDEX IDX_564EC303CE62929E ON truck_model');
        $this->addSql('ALTER TABLE truck_model DROP oilFilter_id, DROP airFilter_id, DROP waterFilter_id');
    }
}
