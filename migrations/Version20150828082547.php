<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150828082547 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE attached_files (id INT AUTO_INCREMENT NOT NULL, invoice_id INT DEFAULT NULL, path VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated DATETIME NOT NULL, deletedAt DATETIME DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_B64A5792989F1FD (invoice_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE attached_files ADD CONSTRAINT FK_B64A5792989F1FD FOREIGN KEY (invoice_id) REFERENCES invoice (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE customer ADD uuid LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE invoice ADD attachedFile_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE invoice ADD CONSTRAINT FK_906517449E26D65E FOREIGN KEY (attachedFile_id) REFERENCES attached_files (id) ON DELETE SET NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_906517449E26D65E ON invoice (attachedFile_id)');
        $this->addSql('ALTER TABLE attached_files ADD uuid VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE invoice DROP FOREIGN KEY FK_906517449E26D65E');
        $this->addSql('DROP TABLE attached_files');
        $this->addSql('ALTER TABLE customer DROP uuid');
        $this->addSql('DROP INDEX UNIQ_906517449E26D65E ON invoice');
        $this->addSql('ALTER TABLE invoice DROP attachedFile_id');
        $this->addSql('ALTER TABLE attached_files DROP uuid');
    }
}
