<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150817153157 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE estimate_item ADD job_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE estimate_item ADD CONSTRAINT FK_591A7C1FBE04EA9 FOREIGN KEY (job_id) REFERENCES job (id)');
        $this->addSql('CREATE INDEX IDX_591A7C1FBE04EA9 ON estimate_item (job_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE estimate_item DROP FOREIGN KEY FK_591A7C1FBE04EA9');
        $this->addSql('DROP INDEX IDX_591A7C1FBE04EA9 ON estimate_item');
        $this->addSql('ALTER TABLE estimate_item DROP job_id');
    }
}
