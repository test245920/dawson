<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220615123304 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE sales_job (id INT AUTO_INCREMENT NOT NULL, customer_id INT DEFAULT NULL, deleted_by_id INT DEFAULT NULL, job_code INT DEFAULT NULL, type VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, make VARCHAR(255) DEFAULT NULL, model VARCHAR(255) DEFAULT NULL, serial VARCHAR(255) DEFAULT NULL, price NUMERIC(8, 2) DEFAULT NULL, status INT DEFAULT NULL, createdAt DATETIME DEFAULT NULL, updatedAt DATETIME DEFAULT NULL, deletedAt DATETIME DEFAULT NULL, INDEX IDX_ECC4FE29395C3F3 (customer_id), INDEX IDX_ECC4FE2C76F1F52 (deleted_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sales_job ADD CONSTRAINT FK_ECC4FE29395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id)');
        $this->addSql('ALTER TABLE sales_job ADD CONSTRAINT FK_ECC4FE2C76F1F52 FOREIGN KEY (deleted_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE purchase_order ADD sales_job_id INT DEFAULT NULL, CHANGE type type VARCHAR(16) NOT NULL, CHANGE order_type order_type VARCHAR(16) DEFAULT NULL');
        $this->addSql('ALTER TABLE purchase_order ADD CONSTRAINT FK_21E210B229141CA1 FOREIGN KEY (sales_job_id) REFERENCES sales_job (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_21E210B229141CA1 ON purchase_order (sales_job_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE purchase_order DROP FOREIGN KEY FK_21E210B229141CA1');
        $this->addSql('DROP TABLE sales_job');
        $this->addSql('DROP INDEX IDX_21E210B229141CA1 ON purchase_order');
        $this->addSql('ALTER TABLE purchase_order DROP sales_job_id, CHANGE type type VARCHAR(16) CHARACTER SET utf8 DEFAULT \'DFS\' COLLATE `utf8_unicode_ci`, CHANGE order_type order_type VARCHAR(16) CHARACTER SET utf8 DEFAULT \'\' COLLATE `utf8_unicode_ci`');
    }
}
