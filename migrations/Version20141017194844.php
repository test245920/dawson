<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20141017194844 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('CREATE TABLE estimate_stock (id INT AUTO_INCREMENT NOT NULL, estimate_id INT DEFAULT NULL, stock_id INT DEFAULT NULL, quantity INT DEFAULT NULL, deletedAt DATETIME DEFAULT NULL, INDEX IDX_3C7767AF85F23082 (estimate_id), INDEX IDX_3C7767AFDCD6110 (stock_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE estimate_item (id INT AUTO_INCREMENT NOT NULL, estimate_id INT DEFAULT NULL, item VARCHAR(255) NOT NULL, price NUMERIC(8, 2) DEFAULT NULL, quantity INT DEFAULT NULL, deletedAt DATETIME DEFAULT NULL, INDEX IDX_591A7C1F85F23082 (estimate_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE estimate_stock ADD CONSTRAINT FK_3C7767AF85F23082 FOREIGN KEY (estimate_id) REFERENCES estimate (id)');
        $this->addSql('ALTER TABLE estimate_stock ADD CONSTRAINT FK_3C7767AFDCD6110 FOREIGN KEY (stock_id) REFERENCES stock_item (id)');
        $this->addSql('ALTER TABLE estimate_item ADD CONSTRAINT FK_591A7C1F85F23082 FOREIGN KEY (estimate_id) REFERENCES estimate (id)');
        $this->addSql('ALTER TABLE estimate ADD issued TINYINT(1) NOT NULL, ADD labour_time INT DEFAULT NULL, ADD travel_time INT DEFAULT NULL, CHANGE details details VARCHAR(255) DEFAULT NULL, CHANGE price price NUMERIC(10, 0) DEFAULT NULL, CHANGE awaitingresponse awaiting_response TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE estimate_event DROP FOREIGN KEY FK_4CEF3B68A9528F2F');
        $this->addSql('DROP INDEX IDX_4CEF3B68A9528F2F ON estimate_event');
        $this->addSql('ALTER TABLE estimate_event CHANGE estimateevents estimate_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE estimate_event ADD CONSTRAINT FK_4CEF3B6885F23082 FOREIGN KEY (estimate_id) REFERENCES estimate (id)');
        $this->addSql('CREATE INDEX IDX_4CEF3B6885F23082 ON estimate_event (estimate_id)');
        $this->addSql('ALTER TABLE stock ADD oem_price NUMERIC(8, 2) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('DROP TABLE estimate_stock');
        $this->addSql('DROP TABLE estimate_item');
        $this->addSql('ALTER TABLE estimate ADD awaitingResponse TINYINT(1) NOT NULL, DROP awaiting_response, DROP issued, DROP labour_time, DROP travel_time, CHANGE details details VARCHAR(255) NOT NULL, CHANGE price price NUMERIC(10, 0) NOT NULL');
        $this->addSql('ALTER TABLE estimate_event DROP FOREIGN KEY FK_4CEF3B6885F23082');
        $this->addSql('DROP INDEX IDX_4CEF3B6885F23082 ON estimate_event');
        $this->addSql('ALTER TABLE estimate_event CHANGE estimate_id estimateEvents INT DEFAULT NULL');
        $this->addSql('ALTER TABLE estimate_event ADD CONSTRAINT FK_4CEF3B68A9528F2F FOREIGN KEY (estimateEvents) REFERENCES estimate (id)');
        $this->addSql('CREATE INDEX IDX_4CEF3B68A9528F2F ON estimate_event (estimateEvents)');
        $this->addSql('ALTER TABLE stock DROP oem_price');
    }
}
