<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20141119132607 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE estimate_item ADD received_date DATETIME DEFAULT NULL, ADD purchaseOrder_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE estimate_item ADD CONSTRAINT FK_591A7C1F1B7B246A FOREIGN KEY (purchaseOrder_id) REFERENCES purchase_order (id)');
        $this->addSql('CREATE INDEX IDX_591A7C1F1B7B246A ON estimate_item (purchaseOrder_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE estimate_item DROP FOREIGN KEY FK_591A7C1F1B7B246A');
        $this->addSql('DROP INDEX IDX_591A7C1F1B7B246A ON estimate_item');
        $this->addSql('ALTER TABLE estimate_item DROP received_date, DROP purchaseOrder_id');
    }
}
