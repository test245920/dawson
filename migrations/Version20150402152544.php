<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150402152544 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE note DROP FOREIGN KEY FK_CFBDFA1475FA0FF2');
        $this->addSql('ALTER TABLE visit_contact DROP FOREIGN KEY FK_1AEBE2FF75FA0FF2');
        $this->addSql('ALTER TABLE visit_user DROP FOREIGN KEY FK_52CB2B8B75FA0FF2');
        $this->addSql('DROP TABLE visit');
        $this->addSql('DROP TABLE visit_contact');
        $this->addSql('DROP TABLE visit_user');
        $this->addSql('DROP INDEX IDX_CFBDFA1475FA0FF2 ON note');
        $this->addSql('ALTER TABLE note DROP visit_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE visit (id INT AUTO_INCREMENT NOT NULL, lead_id INT DEFAULT NULL, date DATETIME NOT NULL, deletedAt DATETIME DEFAULT NULL, summary VARCHAR(255) NOT NULL, INDEX IDX_437EE93955458D (lead_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE visit_contact (visit_id INT NOT NULL, contact_id INT NOT NULL, INDEX IDX_1AEBE2FF75FA0FF2 (visit_id), INDEX IDX_1AEBE2FFE7A1254A (contact_id), PRIMARY KEY(visit_id, contact_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE visit_user (visit_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_52CB2B8B75FA0FF2 (visit_id), INDEX IDX_52CB2B8BA76ED395 (user_id), PRIMARY KEY(visit_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE visit ADD CONSTRAINT FK_437EE93955458D FOREIGN KEY (lead_id) REFERENCES lead (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE visit_contact ADD CONSTRAINT FK_1AEBE2FFE7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE visit_contact ADD CONSTRAINT FK_1AEBE2FF75FA0FF2 FOREIGN KEY (visit_id) REFERENCES visit (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE visit_user ADD CONSTRAINT FK_52CB2B8BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE visit_user ADD CONSTRAINT FK_52CB2B8B75FA0FF2 FOREIGN KEY (visit_id) REFERENCES visit (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE note ADD visit_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE note ADD CONSTRAINT FK_CFBDFA1475FA0FF2 FOREIGN KEY (visit_id) REFERENCES visit (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_CFBDFA1475FA0FF2 ON note (visit_id)');
    }
}
