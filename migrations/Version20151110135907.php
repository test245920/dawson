<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151110135907 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE supplierstock_contact');
        $this->addSql('ALTER TABLE stock DROP INDEX IDX_4B3656606748DD8E, ADD UNIQUE INDEX UNIQ_4B3656606748DD8E (replacesPart_id)');
        $this->addSql('ALTER TABLE stock DROP INDEX IDX_4B365660A247C6F4, ADD UNIQUE INDEX UNIQ_4B365660A247C6F4 (replacedBy_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE supplierstock_contact (supplierstock_id INT NOT NULL, contact_id INT NOT NULL, INDEX IDX_65F6036BF8E0F472 (supplierstock_id), INDEX IDX_65F6036BE7A1254A (contact_id), PRIMARY KEY(supplierstock_id, contact_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE supplierstock_contact ADD CONSTRAINT FK_65F6036BE7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE supplierstock_contact ADD CONSTRAINT FK_65F6036BF8E0F472 FOREIGN KEY (supplierstock_id) REFERENCES supplier_stock (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE stock DROP INDEX UNIQ_4B3656606748DD8E, ADD INDEX IDX_4B3656606748DD8E (replacesPart_id)');
        $this->addSql('ALTER TABLE stock DROP INDEX UNIQ_4B365660A247C6F4, ADD INDEX IDX_4B365660A247C6F4 (replacedBy_id)');
    }
}
