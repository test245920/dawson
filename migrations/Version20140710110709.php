<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140710110709 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('CREATE TABLE supplier_stock (id INT AUTO_INCREMENT NOT NULL, supplier_id INT DEFAULT NULL, quantity_on_order INT NOT NULL, units INT NOT NULL, cost_price DOUBLE PRECISION DEFAULT NULL, carriage_price DOUBLE PRECISION DEFAULT NULL, supplier_notes LONGTEXT DEFAULT NULL, stockItem_id INT DEFAULT NULL, INDEX IDX_922D544AEEC75ED7 (stockItem_id), INDEX IDX_922D544A2ADD6D8C (supplier_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE supplier_stock ADD CONSTRAINT FK_922D544AEEC75ED7 FOREIGN KEY (stockItem_id) REFERENCES stock (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE supplier_stock ADD CONSTRAINT FK_922D544A2ADD6D8C FOREIGN KEY (supplier_id) REFERENCES supplier (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE stock DROP FOREIGN KEY FK_4B3656602ADD6D8C');
        $this->addSql('DROP INDEX IDX_4B3656602ADD6D8C ON stock');
        $this->addSql('ALTER TABLE stock DROP supplier_id, DROP units, DROP order_quantity, DROP cost_price, DROP carriage_price, DROP supplier_notes');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('DROP TABLE supplier_stock');
        $this->addSql('ALTER TABLE stock ADD supplier_id INT DEFAULT NULL, ADD units INT NOT NULL, ADD order_quantity INT NOT NULL, ADD cost_price DOUBLE PRECISION DEFAULT NULL, ADD carriage_price DOUBLE PRECISION DEFAULT NULL, ADD supplier_notes LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE stock ADD CONSTRAINT FK_4B3656602ADD6D8C FOREIGN KEY (supplier_id) REFERENCES supplier (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_4B3656602ADD6D8C ON stock (supplier_id)');
    }
}
