<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140714180947 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE truck ADD deletedAt DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE customer_number ADD deletedAt DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE job ADD deletedAt DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE customer ADD deletedAt DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD deletedAt DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE customer DROP deletedAt');
        $this->addSql('ALTER TABLE customer_number DROP deletedAt');
        $this->addSql('ALTER TABLE job DROP deletedAt');
        $this->addSql('ALTER TABLE truck DROP deletedAt');
        $this->addSql('ALTER TABLE user DROP deletedAt');
    }
}
