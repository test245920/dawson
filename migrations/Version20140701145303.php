<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140701145303 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE customer ADD postcode TINYTEXT DEFAULT NULL, ADD billing_address TINYTEXT DEFAULT NULL, ADD email_invoice TINYINT(1) DEFAULT NULL, ADD order_number LONGTEXT DEFAULT NULL, ADD payment_term INT DEFAULT NULL, ADD account_limit INT DEFAULT NULL, ADD labour_rate NUMERIC(10, 0) DEFAULT NULL, ADD parts_discount NUMERIC(10, 0) DEFAULT NULL, CHANGE address site_address TINYTEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE customer ADD address LONGTEXT DEFAULT NULL, DROP site_address, DROP postcode, DROP billing_address, DROP email_invoice, DROP order_number, DROP payment_term, DROP account_limit, DROP labour_rate, DROP parts_discount');
    }
}
