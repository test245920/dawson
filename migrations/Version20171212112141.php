<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171212112141 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE work_log ADD requested_parts VARCHAR(255) DEFAULT NULL, ADD requested_parts_complete TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE job DROP requested_parts_text');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE job ADD requested_parts_text VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE work_log DROP requested_parts, DROP requested_parts_complete');
    }
}
