<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150112214358 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE truck_contact (truck_id INT NOT NULL, contact_id INT NOT NULL, INDEX IDX_8BD3C96CC6957CCE (truck_id), INDEX IDX_8BD3C96CE7A1254A (contact_id), PRIMARY KEY(truck_id, contact_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE estimate_contact (estimate_id INT NOT NULL, contact_id INT NOT NULL, INDEX IDX_51DE5AC285F23082 (estimate_id), INDEX IDX_51DE5AC2E7A1254A (contact_id), PRIMARY KEY(estimate_id, contact_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE estimate_truck (id INT AUTO_INCREMENT NOT NULL, customer_id INT DEFAULT NULL, model_id INT DEFAULT NULL, estimate_id INT DEFAULT NULL, client_flt_num LONGTEXT DEFAULT NULL, contract_hire TINYINT(1) DEFAULT NULL, casual_hire TINYINT(1) DEFAULT NULL, hire_term INT DEFAULT NULL, hire_rate NUMERIC(8, 2) DEFAULT NULL, max_utilisation LONGTEXT DEFAULT NULL, purchased TINYINT(1) NOT NULL, purchased_date DATETIME DEFAULT NULL, hirable TINYINT(1) NOT NULL, sale_price NUMERIC(8, 2) DEFAULT NULL, hire_price_monthly NUMERIC(8, 2) DEFAULT NULL, maintenance_expenditure NUMERIC(8, 2) DEFAULT NULL, expected_utilisation LONGTEXT DEFAULT NULL, serial LONGTEXT DEFAULT NULL, maintenance_revenue NUMERIC(8, 2) DEFAULT NULL, deletedAt DATETIME DEFAULT NULL, last_service DATETIME DEFAULT NULL, last_loler DATETIME DEFAULT NULL, loler_interval INT DEFAULT NULL, service_interval INT DEFAULT NULL, warranty LONGTEXT DEFAULT NULL, yom INT DEFAULT NULL, maintenance_start_date DATETIME DEFAULT NULL, warranty_start_date DATETIME DEFAULT NULL, contract_hire_start_date DATETIME DEFAULT NULL, casual_hire_start_date DATETIME DEFAULT NULL, maintenance_duration INT DEFAULT NULL, purchase_maintenance TINYINT(1) DEFAULT NULL, mast TINYTEXT DEFAULT NULL, mastHeight INT DEFAULT NULL, hydraulics LONGTEXT DEFAULT NULL, attachments LONGTEXT DEFAULT NULL, weather_protection LONGTEXT DEFAULT NULL, tyre_type LONGTEXT DEFAULT NULL, notes LONGTEXT DEFAULT NULL, performance_indicator LONGTEXT DEFAULT NULL, attachment_model_num LONGTEXT DEFAULT NULL, fuel LONGTEXT DEFAULT NULL, siteAddress_id INT DEFAULT NULL, INDEX IDX_BA8DC2C59395C3F3 (customer_id), INDEX IDX_BA8DC2C57975B7E7 (model_id), INDEX IDX_BA8DC2C5917C7BCD (siteAddress_id), INDEX IDX_BA8DC2C585F23082 (estimate_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE visit_contact (visit_id INT NOT NULL, contact_id INT NOT NULL, INDEX IDX_1AEBE2FF75FA0FF2 (visit_id), INDEX IDX_1AEBE2FFE7A1254A (contact_id), PRIMARY KEY(visit_id, contact_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE job_contact (job_id INT NOT NULL, contact_id INT NOT NULL, INDEX IDX_D68D51A3BE04EA9 (job_id), INDEX IDX_D68D51A3E7A1254A (contact_id), PRIMARY KEY(job_id, contact_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE supplierstock_contact (supplierstock_id INT NOT NULL, contact_id INT NOT NULL, INDEX IDX_65F6036BF8E0F472 (supplierstock_id), INDEX IDX_65F6036BE7A1254A (contact_id), PRIMARY KEY(supplierstock_id, contact_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lead_contact (lead_id INT NOT NULL, contact_id INT NOT NULL, INDEX IDX_15ACBA5655458D (lead_id), INDEX IDX_15ACBA56E7A1254A (contact_id), PRIMARY KEY(lead_id, contact_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE truck_contact ADD CONSTRAINT FK_8BD3C96CC6957CCE FOREIGN KEY (truck_id) REFERENCES truck (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE truck_contact ADD CONSTRAINT FK_8BD3C96CE7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE estimate_contact ADD CONSTRAINT FK_51DE5AC285F23082 FOREIGN KEY (estimate_id) REFERENCES estimate (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE estimate_contact ADD CONSTRAINT FK_51DE5AC2E7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE estimate_truck ADD CONSTRAINT FK_BA8DC2C59395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id)');
        $this->addSql('ALTER TABLE estimate_truck ADD CONSTRAINT FK_BA8DC2C57975B7E7 FOREIGN KEY (model_id) REFERENCES truck_model (id)');
        $this->addSql('ALTER TABLE estimate_truck ADD CONSTRAINT FK_BA8DC2C5917C7BCD FOREIGN KEY (siteAddress_id) REFERENCES site_address (id)');
        $this->addSql('ALTER TABLE estimate_truck ADD CONSTRAINT FK_BA8DC2C585F23082 FOREIGN KEY (estimate_id) REFERENCES estimate (id)');
        $this->addSql('ALTER TABLE visit_contact ADD CONSTRAINT FK_1AEBE2FF75FA0FF2 FOREIGN KEY (visit_id) REFERENCES visit (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE visit_contact ADD CONSTRAINT FK_1AEBE2FFE7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE job_contact ADD CONSTRAINT FK_D68D51A3BE04EA9 FOREIGN KEY (job_id) REFERENCES job (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE job_contact ADD CONSTRAINT FK_D68D51A3E7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE supplierstock_contact ADD CONSTRAINT FK_65F6036BF8E0F472 FOREIGN KEY (supplierstock_id) REFERENCES supplier_stock (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE supplierstock_contact ADD CONSTRAINT FK_65F6036BE7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE lead_contact ADD CONSTRAINT FK_15ACBA5655458D FOREIGN KEY (lead_id) REFERENCES lead (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE lead_contact ADD CONSTRAINT FK_15ACBA56E7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE estimate ADD lead_id INT DEFAULT NULL, ADD is_lead TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE estimate ADD CONSTRAINT FK_D2EA460755458D FOREIGN KEY (lead_id) REFERENCES lead (id)');
        $this->addSql('CREATE INDEX IDX_D2EA460755458D ON estimate (lead_id)');
        $this->addSql('ALTER TABLE customer ADD is_lead TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE contact DROP FOREIGN KEY FK_4C62E638C6957CCE');
        $this->addSql('DROP INDEX IDX_4C62E638C6957CCE ON contact');
        $this->addSql('ALTER TABLE contact CHANGE truck_id supplier_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contact ADD CONSTRAINT FK_4C62E6382ADD6D8C FOREIGN KEY (supplier_id) REFERENCES supplier (id)');
        $this->addSql('CREATE INDEX IDX_4C62E6382ADD6D8C ON contact (supplier_id)');
        $this->addSql('ALTER TABLE lead ADD initial_contact DATETIME DEFAULT NULL, ADD last_contact DATETIME DEFAULT NULL, ADD completion_stage INT NOT NULL, DROP initialContact, DROP lastContact, CHANGE webformenquiry future_target TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE truck_contact');
        $this->addSql('DROP TABLE estimate_contact');
        $this->addSql('DROP TABLE estimate_truck');
        $this->addSql('DROP TABLE visit_contact');
        $this->addSql('DROP TABLE job_contact');
        $this->addSql('DROP TABLE supplierstock_contact');
        $this->addSql('DROP TABLE lead_contact');
        $this->addSql('ALTER TABLE contact DROP FOREIGN KEY FK_4C62E6382ADD6D8C');
        $this->addSql('DROP INDEX IDX_4C62E6382ADD6D8C ON contact');
        $this->addSql('ALTER TABLE contact CHANGE supplier_id truck_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contact ADD CONSTRAINT FK_4C62E638C6957CCE FOREIGN KEY (truck_id) REFERENCES truck (id)');
        $this->addSql('CREATE INDEX IDX_4C62E638C6957CCE ON contact (truck_id)');
        $this->addSql('ALTER TABLE customer DROP is_lead');
        $this->addSql('ALTER TABLE estimate DROP FOREIGN KEY FK_D2EA460755458D');
        $this->addSql('DROP INDEX IDX_D2EA460755458D ON estimate');
        $this->addSql('ALTER TABLE estimate DROP lead_id, DROP is_lead');
        $this->addSql('ALTER TABLE lead ADD initialContact DATETIME NOT NULL, ADD lastContact DATETIME NOT NULL, DROP initial_contact, DROP last_contact, DROP completion_stage, CHANGE future_target webFormEnquiry TINYINT(1) NOT NULL');
    }
}
