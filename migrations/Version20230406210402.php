<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230406210402 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE job DROP FOREIGN KEY FK_FBD8E0F82989F1FD');
        $this->addSql('DROP INDEX UNIQ_FBD8E0F82989F1FD ON job');
        $this->addSql('ALTER TABLE job ADD sales_job_id INT DEFAULT NULL, ADD is_sales_job TINYINT(1) NOT NULL, DROP invoice_id, DROP labour_rate, CHANGE сall_out_charge сall_out_charge NUMERIC(10, 0) NOT NULL');
        $this->addSql('ALTER TABLE job ADD CONSTRAINT FK_FBD8E0F829141CA1 FOREIGN KEY (sales_job_id) REFERENCES sales_job (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_FBD8E0F829141CA1 ON job (sales_job_id)');
        $this->addSql('ALTER TABLE sales_job ADD truck_id INT DEFAULT NULL, DROP type, DROP description, DROP price');
        $this->addSql('ALTER TABLE sales_job ADD CONSTRAINT FK_ECC4FE2C6957CCE FOREIGN KEY (truck_id) REFERENCES truck (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_ECC4FE2C6957CCE ON sales_job (truck_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE job DROP FOREIGN KEY FK_FBD8E0F829141CA1');
        $this->addSql('DROP INDEX IDX_FBD8E0F829141CA1 ON job');
        $this->addSql('ALTER TABLE job ADD labour_rate INT DEFAULT NULL, DROP is_sales_job, CHANGE сall_out_charge сall_out_charge TINYINT(1) NOT NULL, CHANGE sales_job_id invoice_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE job ADD CONSTRAINT FK_FBD8E0F82989F1FD FOREIGN KEY (invoice_id) REFERENCES invoice (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FBD8E0F82989F1FD ON job (invoice_id)');
        $this->addSql('ALTER TABLE sales_job DROP FOREIGN KEY FK_ECC4FE2C6957CCE');
        $this->addSql('DROP INDEX UNIQ_ECC4FE2C6957CCE ON sales_job');
        $this->addSql('ALTER TABLE sales_job ADD type VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_unicode_ci`, ADD description LONGTEXT CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_unicode_ci`, ADD price NUMERIC(8, 2) DEFAULT NULL, DROP truck_id');
    }
}
