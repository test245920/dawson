<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20141022171109 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE estimate ADD pending TINYINT(1) NOT NULL, ADD completion_stage INT DEFAULT NULL');
        $this->addSql('ALTER TABLE estimate_stock DROP FOREIGN KEY FK_3C7767AFDCD6110');
        $this->addSql('ALTER TABLE estimate_stock ADD CONSTRAINT FK_3C7767AFDCD6110 FOREIGN KEY (stock_id) REFERENCES stock (id)');
        $this->addSql('ALTER TABLE job DROP FOREIGN KEY FK_FBD8E0F881398E09');
        $this->addSql('DROP INDEX IDX_FBD8E0F881398E09 ON job');
        $this->addSql('ALTER TABLE job CHANGE customer customer_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE job ADD CONSTRAINT FK_FBD8E0F89395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id)');
        $this->addSql('CREATE INDEX IDX_FBD8E0F89395C3F3 ON job (customer_id)');
        $this->addSql('ALTER TABLE stock_item ADD deletedAt DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE estimate DROP pending, DROP completion_stage');
        $this->addSql('ALTER TABLE estimate_stock DROP FOREIGN KEY FK_3C7767AFDCD6110');
        $this->addSql('ALTER TABLE estimate_stock ADD CONSTRAINT FK_3C7767AFDCD6110 FOREIGN KEY (stock_id) REFERENCES stock_item (id)');
        $this->addSql('ALTER TABLE job DROP FOREIGN KEY FK_FBD8E0F89395C3F3');
        $this->addSql('DROP INDEX IDX_FBD8E0F89395C3F3 ON job');
        $this->addSql('ALTER TABLE job CHANGE customer_id customer INT DEFAULT NULL');
        $this->addSql('ALTER TABLE job ADD CONSTRAINT FK_FBD8E0F881398E09 FOREIGN KEY (customer) REFERENCES customer (id)');
        $this->addSql('CREATE INDEX IDX_FBD8E0F881398E09 ON job (customer)');
        $this->addSql('ALTER TABLE stock_item DROP deletedAt');
    }
}
