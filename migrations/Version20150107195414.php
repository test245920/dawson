<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20150107195414 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE contact_lead');
        $this->addSql('DROP TABLE contact_visit');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE contact_lead (contact_id INT NOT NULL, lead_id INT NOT NULL, INDEX IDX_6EE334E7A1254A (contact_id), INDEX IDX_6EE33455458D (lead_id), PRIMARY KEY(contact_id, lead_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact_visit (contact_id INT NOT NULL, visit_id INT NOT NULL, INDEX IDX_6E54F936E7A1254A (contact_id), INDEX IDX_6E54F93675FA0FF2 (visit_id), PRIMARY KEY(contact_id, visit_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE contact_lead ADD CONSTRAINT FK_6EE33455458D FOREIGN KEY (lead_id) REFERENCES lead (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contact_lead ADD CONSTRAINT FK_6EE334E7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contact_visit ADD CONSTRAINT FK_6E54F93675FA0FF2 FOREIGN KEY (visit_id) REFERENCES visit (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contact_visit ADD CONSTRAINT FK_6E54F936E7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id) ON DELETE CASCADE');
    }
}
