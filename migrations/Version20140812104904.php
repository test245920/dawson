<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140812104904 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql("CREATE TABLE sent_email (id INT AUTO_INCREMENT NOT NULL, recipients LONGTEXT NOT NULL COMMENT '(DC2Type:array)', subject VARCHAR(255) NOT NULL, body LONGTEXT NOT NULL, sesMessageId VARCHAR(255) DEFAULT NULL, sendStatusOk TINYINT(1) NOT NULL, sendStatusCode INT NOT NULL, created_at DATETIME NOT NULL, errorMessage VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('DROP TABLE sent_email');
    }
}
