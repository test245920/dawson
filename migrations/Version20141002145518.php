<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20141002145518 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE truck DROP FOREIGN KEY FK_CDCCF30A7F6B61BA');
        $this->addSql('DROP INDEX IDX_CDCCF30A7F6B61BA ON truck');
        $this->addSql('ALTER TABLE truck CHANGE truck_make yom INT DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7F6B61BA5E237E06 ON truck_make (name)');
        $this->addSql('ALTER TABLE truck_model DROP yom');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', "Migration can only be executed safely on 'mysql'.");

        $this->addSql('ALTER TABLE truck CHANGE yom truck_make INT DEFAULT NULL');
        $this->addSql('ALTER TABLE truck ADD CONSTRAINT FK_CDCCF30A7F6B61BA FOREIGN KEY (truck_make) REFERENCES truck_make (id)');
        $this->addSql('CREATE INDEX IDX_CDCCF30A7F6B61BA ON truck (truck_make)');
        $this->addSql('DROP INDEX UNIQ_7F6B61BA5E237E06 ON truck_make');
        $this->addSql('ALTER TABLE truck_model ADD yom INT DEFAULT NULL');
    }
}
