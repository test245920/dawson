<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160513162808 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE truck_maintenance (id INT AUTO_INCREMENT NOT NULL, truck_id INT DEFAULT NULL, detail VARCHAR(255) NOT NULL, date DATETIME NOT NULL, createdAt DATETIME NOT NULL, updatedAt DATETIME NOT NULL, deletedAt DATETIME DEFAULT NULL, amount INT NOT NULL, truckTransaction_id INT DEFAULT NULL, INDEX IDX_A024A92BC6957CCE (truck_id), UNIQUE INDEX UNIQ_A024A92BA854A8F7 (truckTransaction_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE truck_maintenance ADD CONSTRAINT FK_A024A92BC6957CCE FOREIGN KEY (truck_id) REFERENCES truck (id)');
        $this->addSql('ALTER TABLE truck_maintenance ADD CONSTRAINT FK_A024A92BA854A8F7 FOREIGN KEY (truckTransaction_id) REFERENCES truck_transaction (id)');
        $this->addSql('ALTER TABLE truck_transaction ADD truckMaintenace_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE truck_transaction ADD CONSTRAINT FK_FD975413EB24ABE5 FOREIGN KEY (truckMaintenace_id) REFERENCES truck_maintenance (id) ON DELETE SET NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FD975413EB24ABE5 ON truck_transaction (truckMaintenace_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE truck_transaction DROP FOREIGN KEY FK_FD975413EB24ABE5');
        $this->addSql('DROP TABLE truck_maintenance');
        $this->addSql('DROP INDEX UNIQ_FD975413EB24ABE5 ON truck_transaction');
        $this->addSql('ALTER TABLE truck_transaction DROP truckMaintenace_id');
    }
}
