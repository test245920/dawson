<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220124154045 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE absence_log (id INT AUTO_INCREMENT NOT NULL, engineer_id INT DEFAULT NULL, created_by_id INT DEFAULT NULL, date DATE NOT NULL, time INT NOT NULL, reason VARCHAR(255) NOT NULL, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_99C161C4F8D8CDF1 (engineer_id), INDEX IDX_99C161C4B03A8386 (created_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE absence_log ADD CONSTRAINT FK_99C161C4F8D8CDF1 FOREIGN KEY (engineer_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE absence_log ADD CONSTRAINT FK_99C161C4B03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE absence_log');
    }
}
