<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160224123129 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE note ADD truck_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE note ADD CONSTRAINT FK_CFBDFA14C6957CCE FOREIGN KEY (truck_id) REFERENCES truck (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_CFBDFA14C6957CCE ON note (truck_id)');
        $this->addSql('ALTER TABLE truck CHANGE notes underwriter LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE lead CHANGE completion_stage completion_stage INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE lead CHANGE completion_stage completion_stage INT NOT NULL');
        $this->addSql('ALTER TABLE note DROP FOREIGN KEY FK_CFBDFA14C6957CCE');
        $this->addSql('DROP INDEX IDX_CFBDFA14C6957CCE ON note');
        $this->addSql('ALTER TABLE note DROP truck_id');
        $this->addSql('ALTER TABLE truck CHANGE underwriter notes LONGTEXT DEFAULT NULL');
    }
}
