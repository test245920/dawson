var col            = 1;
var row            = 1;
var selectedItemId = $('.selected').data('id');
dataset            = {};

$(function() {
    $('.waterfall .panel-link').addClass('visible');
    buildColumnSearchArrays();
});

$(document).on('keydown', function(e){
    if ($('.modal').hasClass('in')) {
        return;
    }

    if ($(e.target).is('input.waterfall-filter')) {
        if (e.keyCode == 40 || e.keyCode == 9) {
            event.preventDefault();
            $(e.target).blur();
        } else {
            return;
        }
    } else {
        if ($(e.target).is("input") || $(e.target).is("textarea")) {
            return;
        }
    }

    $maxCols = $('.panel-body').length;

    // up
    if (e.keyCode == 38) {
        event.preventDefault();
        setSelected('up');
    }

    // down
    if (e.keyCode == 40) {
        event.preventDefault();
        setSelected('down');
    }

    // left
    if (e.keyCode == 37) {
        event.preventDefault();
        setSelected('left');
    }

    // right
    if (e.keyCode == 39) {
        event.preventDefault();
        setSelected('right');
    }

    // enter or space
    if (e.keyCode == 13 || e.keyCode == 32) {
        $('.waterfall [data-column="'+col+'"][data-row="'+row+'"]').trigger('click');
        selectedItemId = $('.selected').data('id');
    }
});

$filterBox = $('.waterfall-filter');
$filterBox.on('keyup', findItem);

function buildColumnSearchArrays() {
    $cols = $('.waterfall-filter');
    dataset = {};

    $cols.each(function() {
        col          = $(this).next().data('column');
        dataset[col] = [];
        numRows      = $('.waterfall [data-column="'+col+'"]').length;

        for (var i =1; i <= numRows ; i++) {
            item          = {};
            item.id    = $('.waterfall [data-column="'+col+'"][data-row="'+ i +'"]').data('id');
            item.row   = $('.waterfall [data-column="'+col+'"][data-row="'+ i +'"]').data('row');
            item.col   = $('.waterfall [data-column="'+col+'"][data-row="'+ i +'"]').data('column');
            item.type  = $('.waterfall [data-column="'+col+'"][data-row="'+ i +'"]').data('type');
            item.text  = $('.waterfall [data-column="'+col+'"][data-row="'+ i +'"]').text().trim();

            if ($('.waterfall [data-column="'+col+'"][data-row="'+ i +'"]').data('other')) {
                item.other = $('.waterfall [data-column="'+col+'"][data-row="'+ i +'"]').data('other');
            }

            dataset[col].push(item);
        }
    });

    return dataset;
}

function findItem(e) {
    if (e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40 || e.keycode == 13 ) {
        return;
    }

    var key       = e.which;
    var filterCol = $(e.target).next().data('column');
    $filterBox    = $(e.target).closest('.waterfall-filter');

    if (dataset.length === 0) {
        dataset = buildColumnSearchArrays();
    }

    $('.panel-link[data-column="' + filterCol + '"').removeClass('visible');

    if ($filterBox.val() === '') {
        $('.panel-link[data-column="' + filterCol + '"]').not('.visible').addClass('visible').slideDown();
        return;
    }

    var filtered = filter($filterBox.val(), dataset[filterCol], ['linkidentifier', 'category']);

    $.each(filtered, function(){
        var original = $(this)[0].original;
        var $item = $('.panel-link[data-column="' + filterCol + '"][data-row="' + original.row + '"]');
        $item.not('.visible').addClass('visible').slideDown();
    });

    $('.panel-link[data-column="' + filterCol + '"').not('.visible').slideUp();
}

function getNextRow() {
    $nextRow = $('.waterfall .visible[data-row]').filter(function() {
        return $(this).data('row') > row;
    }).first();

    newRow = $nextRow.length > 0 ? $nextRow.data('row') : row;

    return newRow;
}

function getPrevRow() {
    $prevRow = $('.waterfall .visible[data-row]').filter(function() {
        return $(this).data('row') < row;
    }).last();

    newRow = $prevRow.length > 0 ? $prevRow.data('row') : row;

    return newRow;
}

function setSelected(direction) {

    $('.waterfall .panel-link').removeClass('selected');

    if (direction == 'right') {
        col = getNextCol();
        row = getClosestRow();
    }

    if (direction == 'left') {
        col = getPrevCol();
        row = getClosestRow();
    }

    if (direction == 'up') {
        row = getPrevRow();
    }

    if (direction == 'down') {
        row = getNextRow();
    }

    if (direction == 'current') {
        $('.waterfall [data-column="'+col+'"][data-row="'+row+'"]').addClass('selected');
        selectedItemId = $('.selected').data('id');
        return;
    }

    $('.waterfall [data-column="'+col+'"][data-row="'+row+'"]').addClass('selected');
    selectedItemId = $('.selected').data('id');
}

function getNextCol() {
    $nextCol = $('.waterfall .visible[data-column]').filter(function() {
        return $(this).data('column') > col;
    }).first();

    newCol = $nextCol.length > 0 ? $nextCol.data('column') : col;

    return newCol;
}

function getPrevCol() {
    $prevCol = $('.waterfall .visible[data-column]').filter(function() {
        return $(this).data('column') < col;
    }).last();

    newCol = $prevCol.length > 0 ? $prevCol.data('column') : col;

    return newCol;
}

function getClosestRow() {
    $closestRow = $('.waterfall .visible[data-column="' + col + '"]').filter(function() {
        return $(this).data('row') <= row;
    }).last();

    newRow = $closestRow.length > 0 ? $closestRow.data('row') : row;

    return newRow;
}
