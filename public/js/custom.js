$(function() {
    $('input.datepicker').datetimepicker({
        pickTime: false
    });
    $('.datetimepicker').datetimepicker();

    $.fn.modal.Constructor.DEFAULTS.keyboard = false;
    $.fn.modal.Constructor.DEFAULTS.backdrop = 'static';

    $('.lead-report-row').on('click', function(){
        $ele = $(this);
        url  = $ele.data('href');

        $(location).attr('href', url);
    });

    $('.truck-report-row').on('click', function(){
        $ele = $(this);
        url  = $ele.data('href');

        $(location).attr('href', url);
    });

    $("a[href$='.jpg'],a[href$='.jpeg'],a[href$='.png'],a[href$='.gif']").attr('rel', 'gallery').fancybox();

    bindReveals();

    $(document).ready(function() {
        $('.fancybox').fancybox({
            prevEffect : 'none',
            nextEffect : 'none',

            closeBtn  : true,
            arrows    : false,
            nextClick : true,

            helpers : {
                thumbs : {
                    width  : 50,
                    height : 50
                }
            }
        });
    });
});

function bindReveals() {
    $('[data-reveal]').each(function() {
        revealLinkedElements($(this));
    });

    $('[data-reveal]').off('change.reveal');

    $('[data-reveal]').on('change.reveal', function() {
        revealLinkedElements($(this));
    });
}

function revealLinkedElements($element) {
    var $target = $($element.data('reveal'));

    if (!$target.length) {
        return;
    }

    var val = $element.val();
    var isNumeric = !isNaN(val);

    if ($element.val() && (!isNumeric || parseInt(val) > 0)) {
        $target.show();
    } else {
        $target.hide();
    }
}

function setStackedModals() {
    var modalZIndex = 1040;
    $('.modal.in').each(function() {
        var $ele    = $(this);
        modalZIndex += 2;
        $ele.css('zIndex', modalZIndex);
        $('.modal-backdrop.in').last().addClass('hidden').css('zIndex', modalZIndex - 1);
    });
    $('.modal-backdrop.in').last().removeClass('hidden');
}

function getTopModalOnStack() {
    var index_highest = 0;
    $('.modal.in').each(function(){
        $ele              = $(this);
        var indexCurrent = parseInt($ele.css("z-index"), 10);

        if(indexCurrent > index_highest) {
            $topModal = $ele;
        }
    });
    return $topModal;
}

function getParameterByName(name) {
    name      = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results   = regex.exec(location.search);

    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

// Adds/updates parameters in URL query strings
function addQueryParams(url, params) {
    urlParts = url.split("#");
    url      = urlParts[0];
    hashPart = 1 in urlParts ? urlParts[1] : '';
    url      =  url.match(/\?/) ? url : url + '?';

    for ( var key in params ) {
        var re = RegExp( '&?' + key + '=?[^&]*', 'g' );
        url    = url.replace( re, '');

        if (!params[key]) {
            continue;
        }

        url += '&' + key + '=' + params[key];
    }

    url = url.replace(/[&]$/, '');
    url = url.replace(/\?[&]/, '?');
    url = url.replace(/[&]{2}/g, '&');
    url = url + hashPart;

    return url;
}

function bindSelectables() {
    $('.selectable-item').unbind('click.selectables');
    $('.selectable-item').bind('click.selectables', function() {
        if (history.pushState) {
            updateSelectedItem($(this).data('id'));
        }
    });
}

function updateSelectedItem(selectedItem) {
    var newurl = addQueryParams(window.location.href, { 'selectedItem': selectedItem });
    window.history.pushState({path:newurl},'',newurl);
}

function growlError(message) {
    $.growl(message, {
        type: 'danger',
        delay: 0,
        template: {
            container: '<div class="col-md-3 growl-animated alert">'
        }
    });
}

function growlSuccess(message) {
    $.growl(message, {
        type: 'success',
        delay: 0,
        template: {
            container: '<div class="col-md-3 growl-animated alert">'
        }
    });
}

function listenForFormChanges() {
    $('form').on('change', function(){
        formChanged = true;
    });

    $('form').on('submit', function(){
        formChanged = false;
    });

    $('.modal').on('hide.bs.modal', function (e) {
        formChanged = false;
    });

    window.addEventListener('beforeunload', function (e) {
        if (formChanged) {
            (e || window.event).returnValue = 'All changes will be lost if you leave this page.';
        }
        return null;
    });
}

Number.prototype.formatMoney = function(c, d, t){
    c = isNaN(c = Math.abs(c)) ? 2 : c;
    d = d === undefined ? "." : d;
    t = t === undefined ? "," : t;

    var n = this,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };

function throttle(fn, delay) {
    var timer = null;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
          fn.apply(context, args);
        }, delay);
    };
}

function roundPrice(price) {
    return (Math.round((price + 0.00001) * 100) / 100).toFixed(2); // workaround for floating point weirdness in JS
}

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

Date.isLeapYear = function (year) {
    return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0));
};

Date.getDaysInMonth = function (year, month) {
    return [31, (Date.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
};

Date.prototype.isLeapYear = function () {
    return Date.isLeapYear(this.getFullYear());
};

Date.prototype.getDaysInMonth = function () {
    return Date.getDaysInMonth(this.getFullYear(), this.getMonth());
};

Date.prototype.addMonths = function (value) {
    var n = this.getDate();
    this.setDate(1);
    this.setMonth(this.getMonth() + value);
    this.setDate(Math.min(n, this.getDaysInMonth()));
    return this;
};
