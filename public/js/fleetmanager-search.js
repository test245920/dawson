/*jshint multistr: true */

$.getScript("/js/jsrender.min.js", function(){});

var templatesInitalized = false;

function initialiseTemplates() {
    $.templates({
        default: ' \
            <tr> \
                <td class="search-result-details" data-id="{{:id}}" data-description="{{:description}}" data-code="{{:code}}"> \
                    {{:code}} - {{:description}}  \
                </td> \
                <td> \
                    <a href="#" class="btn btn-primary btn-xs search-add addBtn">View</a> \
                </td> \
            </tr>',
        truck: ' \
            <tr> \
                <td class="search-result-details" data-id="{{:id}}" data-make="{{:make}}" data-model="{{:model}}" data-serial="{{:serial}}" data-flt="{{:flt}}" data-customer="{{:customer}}"> \
                    {{:make}} - {{:model}} - {{:flt}} - {{:customer}} - {{:serial}}\
                </td> \
                <td> \
                    <a href="#" class="btn btn-primary btn-xs search-add addBtn">Select</a> \
                </td> \
            </tr>',
        stock: ' \
            <tr> \
                <td class="search-result-details" data-id="{{:id}}" data-description="{{:description}}" data-code="{{:code}}" data-is-stock="{{:isStock}}"> \
                    {{:code}} - {{:description}}  \
                </td> \
                <td> \
                    <a href="#" class="btn btn-primary btn-xs search-add addBtn">View</a> \
                </td> \
            </tr>',
        stockForJob: ' \
            <tr> \
                <td class="search-result-details" data-id="{{:id}}" data-description="{{:description}}" data-code="{{:code}}" data-quantity="{{:stockCount}}"> \
                    {{:code}} - {{:description}} ({{:stockCount}} in stock) \
                </td> \
                <td class="search-result-actions"> \
                    <a href="#" class="btn btn-primary btn-xs search-add assignStockBtn">Add</a> \
                </td> \
            </tr>',
        stockForEstimate: ' \
            <tr> \
                <td class="search-result-details" data-id="{{:id}}" data-listprice="{{:listPrice}}" data-description="{{:description}}" data-code="{{:code}}"> \
                    {{:code}} - {{:description}}  \
                </td> \
                <td class="search-result-actions"> \
                    <a href="#" class="btn btn-primary btn-xs search-add addBtn">Add</a> \
                </td> \
            </tr>',
        stockForPurchaseOrder: ' \
            <tr> \
                <td class="search-result-details" data-id="{{:id}}" data-costprice="{{:costPrice}}" data-code="{{:code}}" data-description="{{:description}}" data-preferredsupplierid="{{:preferredSupplierId}}" data-preferredsuppliername="{{:preferredSupplierName}}"> \
                    {{:code}} - {{:description}}, £{{:costPrice}}  \
                </td> \
                <td class="search-result-actions"> \
                    <a href="#" class="btn btn-primary btn-xs search-add addBtn">Add</a> \
                </td> \
            </tr>',
        stockForTruckModel: ' \
            <tr> \
                <td class="search-result-details" data-id="{{:id}}" data-description="{{:description}}" data-code="{{:code}}"> \
                    {{:code}} - {{:description}}  \
                </td> \
                <td> \
                    <a href="#" class="btn btn-primary btn-xs search-add addBtn">Add</a> \
                </td> \
            </tr>',
        purchaseOrder: ' \
            <tr> \
                <td class="search-result-details" data-id="{{:id}}" data-description="{{:description}}"> \
                    {{:id}} - {{:description}}  \
                </td> \
                <td> \
                    <a href="#" class="btn btn-primary btn-xs search-add addBtn">View</a> \
                </td> \
            </tr>',
    });

    templatesInitalized = true;
}

function fleetmanagerSearch(needle, haystack, resultsContainer, itemType, ignoreKeys) {
    if (needle.length < 3) {
        resultsContainer.empty();
        return false;
    }

    var filteredResults = filter(needle, haystack, ignoreKeys);

    listResults(filteredResults, resultsContainer, itemType);

    return true;
}

function filter(needle, haystack, ignoreKeys) {
    ignoreKeys = ignoreKeys || [];

    var filteredResults = [];

    for (var i = 0; i < haystack.length; i++) {
        var item = haystack[i];
        var str = searchSerialize(item, ignoreKeys);
console.log(str);
        if (str.toLowerCase().indexOf(needle.toLowerCase()) !== -1) {
            filteredResults.push({original: item, string: str});
        }
    }

    return filteredResults;
}

function searchSerialize(item, ignoreKeys) {
    var str = "";
    var id  = "";
    for (var key in item) {
        if (ignoreKeys.indexOf(key) !== -1) {
            continue;
        }

        if (key == 'id') {
            id = item[key];
        } else {
            str = (str === "" ? item[key] : str + ':::' + item[key]);
        }
    }

    str = str + ':::' + id;
    return str;
}

function listResults(results, resultsContainer, template) {
    var htmlToDisplay = results.slice(0,50).map(function(entry){
        var items = entry.string.split(':::');

        switch (template) {
            case 'truck':
                return renderResults({
                    serial: items.shift(),
                    make: items.shift(),
                    model: items.shift(),
                    flt: items.shift(),
                    periodType: items.shift(),
                    customer: items.shift(),
                    parentCustomer: items.shift(),
                    id: items.shift(),
                    details: (items.length > 0 ? ' - ' + items.join(' - '): '')
                }, 'truck');

            case 'stock':
                return renderResults({
                    id: items.shift(),
                    code: items.shift(),
                    description: items.shift(),
                    isStock: items.shift(),
                    details: (items.length > 0 ? ' - ' + items.join(' - '): '')
                }, 'stock');

            case 'stockForJob':
                return renderResults({
                    code: items.shift(),
                    description: items.shift(),
                    stockCount: items.shift(),
                    id: items.shift(),
                    details: (items.length > 0 ? ' - ' + items.join(' - '): '')
                }, 'stockForJob');

            case 'stockForEstimate':
            case 'stockForPurchaseOrder':
                return renderResults({
                    code: items.shift(),
                    description: items.shift(),
                    listPrice: items.shift(),
                    costPrice: items.shift(),
                    preferredSupplierId: items.shift(),
                    preferredSupplierName: items.shift(),
                    id: items.shift(),
                    details: (items.length > 0 ? ' - ' + items.join(' - '): '')
                }, template);

            case 'stockForTruckModel':
                return renderResults({
                    code: items.shift(),
                    description: items.shift(),
                    altDescriptions: items.shift(),
                    altCodes: items.shift(),
                    id: items.shift(),
                    details: (items.length > 0 ? ' - ' + items.join(' - '): '')
                }, 'stockForTruckModel');

            case 'purchaseOrder':
                return renderResults({
                    description: items.shift(),
                    id: items.shift(),
                    details: (items.length > 0 ? ' - ' + items.join(' - '): '')
                }, 'purchaseOrder');

            default:
                return false;
        }
    });

    resultsContainer.empty();
    $.each(htmlToDisplay, function(){
        resultsContainer.append(this);
    });

    bindArrowKeys(resultsContainer);
}

function renderResults(data, template) {
    data = $.each(data, function(key, value){
        value = value.replace(/<([^>]+)>/ig,'');
    });

    if (!templatesInitalized) {
        initialiseTemplates();
    }

    switch (template) {
        case 'truck':
            html = $.templates.truck.render(data);
            break;

        case 'stock':
            html = $.templates.stock.render(data);
            break;

        case 'stockForJob':
            html = $.templates.stockForJob.render(data);
            break;

        case 'stockForEstimate':
            html = $.templates.stockForEstimate.render(data);
            break;

        case 'stockForPurchaseOrder':
            html = $.templates.stockForPurchaseOrder.render(data);
            break;

        case 'stockForTruckModel':
            html = $.templates.stockForTruckModel.render(data);
            break;

        case 'purchaseOrder':
            html = $.templates.purchaseOrder.render(data);
            break;

        default:
            html = '';
    }

    return html;
}

function bindArrowKeys(resultsContainer) {
    $(document).on('keyup', function(e){
        if (e.keyCode == "38") { //up
            prevResult(resultsContainer);
            e.target.blur();
            return false;
        }

        if (e.keyCode == "40") { //down
            nextResult(resultsContainer);
            e.target.blur();
            return false;
        }

        if (e.keyCode == "13") {
            resultsContainer.find('tr.active').first().children('td').last().children('a').first().trigger('click');
            resultsContainer.empty();
            return false;
        }
    });
}

function prevResult(resultsContainer) {
    $oldActive = resultsContainer.find("tr.active").length > 0 ? resultsContainer.find("tr.active") : resultsContainer.find("tr").first();
    $oldActive.removeClass('active');
    $newActive = $oldActive.prev("tr").length > 0 ? $oldActive.prev("tr") : $oldActive;
    $newActive.addClass('active');
}

function nextResult(resultsContainer) {
    $oldActive = resultsContainer.find("tr.active");
    $oldActive.removeClass('active');
    $newActive = $oldActive.length > 0 ? ($oldActive.next("tr").length > 0 ? $oldActive.next("tr") : $oldActive) : resultsContainer.find("tr").first();
    $newActive.addClass('active');
}
