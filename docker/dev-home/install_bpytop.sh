#!/bin/bash

apk add python3 make
apk add --virtual .build-deps gcc linux-headers musl-dev python3-dev
python3 -m pip install psutil
apk del -f .build-deps
git clone https://github.com/aristocratos/bpytop.git
cd bpytop/
make install
apk del make
