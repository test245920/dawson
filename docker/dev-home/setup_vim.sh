#!/bin/bash

git clone -q https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim \
    && mkdir -p ~/.vim/backup ~/.vim/temp \
    && mv ~/.vimrc.updated ~/.vimrc \
    && vim +PluginUpdate +qall \
