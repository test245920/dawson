#!/usr/bin/with-contenv sh
set -e;

PHP_INI_RECOMMENDED="$PHP_INI_DIR/php.ini-development"
CONSOLE_ENV="dev"

if [ "$APP_ENV" == 'prod' ]; then
    PHP_INI_RECOMMENDED="$PHP_INI_DIR/php.ini-production"
    rm -f /var/www/html/web/app_*.php
    CONSOLE_ENV="prod"
fi

ln -sf "$PHP_INI_RECOMMENDED" "$PHP_INI_DIR/php.ini"

#if [ "$APP_ENV" == 'dev' ]; then
#    # db reset if needed
#fi

rm -rf /var/www/html/var/cache /var/www/html/var/log \
    && mkdir -p /var/www/html/var/cache /var/www/html/var/log \
    && chmod -R 777 /var/www/html/var/log /var/www/html/var/cache \
    && php /var/www/html/bin/console cache:clear -e $CONSOLE_ENV \
    && php /var/www/html/bin/console assets:install public --symlink -e $CONSOLE_ENV \
    && php-fpm -R --nodaemonize