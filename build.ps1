param(
    [Parameter(Mandatory = $true)][string]$BuildCounter = '',
    [Parameter(Mandatory = $true)][string]$Environment = ''
)

trap
{
    write-output $_
    ##teamcity[buildStatus status='FAILURE' ]
    exit 1
}

function EnsureSuccessStatusCode()
{
    if ($LastExitCode -ne 0)
    {
        throw "Last command did not return successful status code. Status code: $LastExitCode"
    }
}

$global:BuildNumber = $Environment + "-1.0." + $BuildCounter + ".0";
$global:Repo = "243047866579.dkr.ecr.eu-west-1.amazonaws.com"

aws ecr get-login-password --region eu-west-1 | docker login --username AWS --password-stdin $global:Repo | Write-Host | EnsureSuccessStatusCode

docker build --build-arg APP_ENV=prod -t dawson . | Write-Host | EnsureSuccessStatusCode
docker tag dawson:latest $global:Repo/dawson:$global:BuildNumber | Write-Host | EnsureSuccessStatusCode

docker push $global:Repo/dawson:$global:BuildNumber | Write-Host | EnsureSuccessStatusCode
