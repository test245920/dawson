<?php

namespace App\Tests;

use App\Entity\Repository\UserRepository;
use Pierstoval\SmokeTesting\FunctionalSmokeTester;
use Pierstoval\SmokeTesting\FunctionalTestData;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

// TODO
/*
 For now the first test in class should use runFunctionalTestWithUser and other should use runFunctionalTest
 That makes the order of tests in class important and it should be refactored
 Also login and logout tests are at the end of class because of that
*/
class SmokeTest extends WebTestCase
{
    use FunctionalSmokeTester;

    public function testRouteAssetRegisterWithMethodGet(): void
    {
        $this->runFunctionalTestWithUser(
            FunctionalTestData::withUrl('/assets/register')
                ->withMethod('GET')
                ->expectRouteName('asset_register')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/assets/register')),
        );
    }

    public function testRouteViewAccountWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/account')
                ->withMethod('GET')
                ->expectRouteName('view_account')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/account')),
        );
    }

    public function testRouteActiveAssetsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/assets/active')
                ->withMethod('GET')
                ->expectRouteName('active_assets')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/assets/active')),
        );
    }

    public function testRouteViewAssetWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/assets/view/1')
                ->withMethod('GET')
                ->expectRouteName('view_asset')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/assets/view/1')),
        );
    }

    public function testRouteGetAssetAddressWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/assets/get-address/1')
                ->withMethod('GET')
                ->expectRouteName('get_asset_address')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/assets/get-address/1')),
        );
    }

    public function testRouteGetAssetContactsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/assets/get-contacts/1')
                ->withMethod('GET')
                ->expectRouteName('get_asset_contacts')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/assets/get-contacts/1')),
        );
    }

    public function testRouteNewAssetNoteWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/assets/note/new')
                ->withMethod('GET')
                ->expectRouteName('new_asset_note')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/assets/note/new')),
        );
    }

    public function testRouteReturnAssetWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/assets/return/1')
                ->withMethod('GET')
                ->expectRouteName('return_asset')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/assets/return/1')),
        );
    }

    public function testRouteTransferAssetWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/assets/transfer/1')
                ->withMethod('GET')
                ->expectRouteName('transfer_asset')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/assets/transfer/1')),
        );
    }

    public function testRouteDeleteAssetWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/assets/delete/1')
                ->withMethod('GET')
                ->expectRouteName('delete_asset')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/assets/delete/1')),
        );
    }

    // TODO fixtures
    /*public function testRouteAssetParticularsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/assets/particulars/1')
                ->withMethod('GET')
                ->expectRouteName('asset_particulars')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/assets/particulars/1')),
        );
    }*/

    public function testRouteAssetAutocompleteSearchWithMethodGet(): void
    {
        $this->runFunctionalTestWithUser(
            FunctionalTestData::withUrl('/assets/autocomplete')
                ->withMethod('GET')
                ->expectRouteName('asset_autocomplete_search')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/assets/autocomplete')),
        );
    }

    public function testRouteNewTruckWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/assets/trucks/new')
                ->withMethod('GET')
                ->expectRouteName('new_truck')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/assets/trucks/new')),
        );
    }

    public function testRouteEditTruckWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/assets/trucks/edit/1')
                ->withMethod('GET')
                ->expectRouteName('edit_truck')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/assets/trucks/edit/1')),
        );
    }

    public function testRouteGetContactCustomerWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/contact-customer/1')
                ->withMethod('GET')
                ->expectRouteName('get_contact_customer')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/contact-customer/1')),
        );
    }

    public function testRouteGetTruckModelsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/assets/trucks/models')
                ->withMethod('GET')
                ->expectRouteName('get_truck_models')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/assets/trucks/models')),
        );
    }

    // TODO fixtures
    /*public function testRouteEditTruckModelWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/assets/trucks/model/edit/1')
                ->withMethod('GET')
                ->expectRouteName('edit_truck_model')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/assets/trucks/model/edit/1')),
        );
    }*/

    public function testRouteGetTruckListWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/assets/get-list')
                ->withMethod('GET')
                ->expectRouteName('get_truck_list')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/assets/get-list')),
        );
    }

    public function testRouteNewAssetMaintenanceWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/assets/maintenance/new')
                ->withMethod('GET')
                ->expectRouteName('new_asset_maintenance')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/assets/maintenance/new')),
        );
    }

    public function testRouteEditMaintenanceWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/assets/maintenance/edit/1')
                ->withMethod('GET')
                ->expectRouteName('edit_maintenance')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/assets/maintenance/edit/1')),
        );
    }

    public function testRouteViewMaintenanceWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/assets/maintenance/view/1')
                ->withMethod('GET')
                ->expectRouteName('view_maintenance')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/assets/maintenance/view/1')),
        );
    }

    public function testRouteDeleteMaintenanceWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/assets/maintenance/delete/1')
                ->withMethod('GET')
                ->expectRouteName('delete_maintenance')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/assets/maintenance/delete/1')),
        );
    }

    public function testRouteNewTruckAttachmentWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/assets/attachments/new')
                ->withMethod('GET')
                ->expectRouteName('new_truck_attachment')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/assets/attachments/new')),
        );
    }

    public function testRouteEditTruckAttachmentWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/assets/attachments/edit/1')
                ->withMethod('GET')
                ->expectRouteName('edit_truck_attachment')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/assets/attachments/edit/1')),
        );
    }

    public function testRouteNewTruckChargerWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/assets/chargers/new')
                ->withMethod('GET')
                ->expectRouteName('new_truck_charger')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/assets/chargers/new')),
        );
    }

    public function testRouteEditTruckChargerWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/assets/chargers/edit/1')
                ->withMethod('GET')
                ->expectRouteName('edit_truck_charger')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/assets/chargers/edit/1')),
        );
    }

    public function testRouteNewContactWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/contacts/new')
                ->withMethod('GET')
                ->expectRouteName('new_contact')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/contacts/new')),
        );
    }

    public function testRouteViewContactWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/contacts/view/1')
                ->withMethod('GET')
                ->expectRouteName('view_contact')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/contacts/view/1')),
        );
    }

    public function testRouteEditContactWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/contacts/edit/1')
                ->withMethod('GET')
                ->expectRouteName('edit_contact')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/contacts/edit/1')),
        );
    }

    public function testRouteDeleteContactWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/contacts/delete/1')
                ->withMethod('GET')
                ->expectRouteName('delete_contact')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/contacts/delete/1')),
        );
    }

    // TODO fixtures
    /*public function testRouteGetContactParticularsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/contacts/particulars/1')
                ->withMethod('GET')
                ->expectRouteName('get_contact_particulars')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/contacts/particulars/1')),
        );
    }*/

    public function testRouteCreateUserAccountForContactWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/contacts/user-account/create/1')
                ->withMethod('GET')
                ->expectRouteName('create_user_account_for_contact')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/contacts/user-account/create/1')),
        );
    }

    public function testRouteListContactsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/contacts')
                ->withMethod('GET')
                ->expectRouteName('list_contacts')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/contacts')),
        );
    }

    public function testRouteNewCustomerWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/customers/new')
                ->withMethod('GET')
                ->expectRouteName('new_customer')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/customers/new')),
        );
    }

    public function testRouteEditCustomerWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/customers/edit/1')
                ->withMethod('GET')
                ->expectRouteName('edit_customer')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/customers/edit/1')),
        );
    }

    public function testRouteViewCustomerWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/customers/view/1')
                ->withMethod('GET')
                ->expectRouteName('view_customer')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/customers/view/1')),
        );
    }

    // TODO fixtures
    /*public function testRouteDeleteCustomerWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/customers/delete/1')
                ->withMethod('GET')
                ->expectRouteName('delete_customer')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/customers/delete/1')),
        );
    }*/

    public function testRouteGetCustomerSitesWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/customers/sites')
                ->withMethod('GET')
                ->expectRouteName('get_customer_sites')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/customers/sites')),
        );
    }

    public function testRouteGetContactsForCustomerWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/customers/get-contacts')
                ->withMethod('GET')
                ->expectRouteName('get_contacts_for_customer')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/customers/get-contacts')),
        );
    }

    public function testRouteGetCustomerjobHistoryReportWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/customers/reports/job-history')
                ->withMethod('GET')
                ->expectRouteName('get_customerjob_history_report')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/customers/reports/job-history')),
        );
    }

    public function testRouteGetCustomerAddressJobHistoryReportWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/customers/reports/address-job-history')
                ->withMethod('GET')
                ->expectRouteName('get_customer_address_job_history_report')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/customers/reports/address-job-history')),
        );
    }

    public function testRouteListCustomersWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/customers')
                ->withMethod('GET')
                ->expectRouteName('list_customers')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/customers')),
        );
    }

    public function testRouteListJobsForEngineerWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/engineer/list-jobs')
                ->withMethod('GET')
                ->expectRouteName('list_jobs_for_engineer')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/engineer/list-jobs')),
        );
    }

    public function testRouteAddLogAbsenceWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/engineer/add-absence')
                ->withMethod('GET')
                ->expectRouteName('add_log_absence')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/engineer/add-absence')),
        );
    }

    public function testRouteEditAbsenceWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/absence/edit/1')
                ->withMethod('GET')
                ->expectRouteName('edit_absence')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/absence/edit/1')),
        );
    }

    public function testRouteEngineerRecurringJobsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/engineer/recurring-jobs')
                ->withMethod('GET')
                ->expectRouteName('engineer_recurring_jobs')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/engineer/recurring-jobs')),
        );
    }

    public function testRouteNewEstimateWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/estimates/new')
                ->withMethod('GET')
                ->expectRouteName('new_estimate')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/estimates/new')),
        );
    }

    public function testRouteViewEstimateWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/estimates/view/1')
                ->withMethod('GET')
                ->expectRouteName('view_estimate')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/estimates/view/1')),
        );
    }

    public function testRouteEditEstimateWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/estimates/edit/1')
                ->withMethod('GET')
                ->expectRouteName('edit_estimate')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/estimates/edit/1')),
        );
    }

    public function testRouteDeleteEstimateWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/estimates/delete/1')
                ->withMethod('GET')
                ->expectRouteName('delete_estimate')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/estimates/delete/1')),
        );
    }

    // TODO fixtures
    /*public function testRouteGetEstimateParticularsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/estimates/get-particulars/1')
                ->withMethod('GET')
                ->expectRouteName('get_estimate_particulars')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/estimates/get-particulars/1')),
        );
    }*/

    public function testRouteEstimateNegotiationWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/estimates/negotiate')
                ->withMethod('GET')
                ->expectRouteName('estimate_negotiation')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/estimates/negotiate')),
        );
    }

    public function testRouteCreateJobFromEstimateWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/estimates/create-job/1')
                ->withMethod('GET')
                ->expectRouteName('create_job_from_estimate')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/estimates/create-job/1')),
        );
    }

    public function testRouteIssueEstimateEmailWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/estimates/email/issue/1')
                ->withMethod('GET')
                ->expectRouteName('issue_estimate_email')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/estimates/email/issue/1')),
        );
    }

    public function testRouteAcceptEstimateWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/estimates/accept')
                ->withMethod('GET')
                ->expectRouteName('accept_estimate')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/estimates/accept')),
        );
    }

    public function testRouteEstimateRejectionReasonWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/estimates/rejection-reason')
                ->withMethod('GET')
                ->expectRouteName('estimate_rejection_reason')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/estimates/rejection-reason')),
        );
    }

    public function testRouteListEstimatesWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/estimates')
                ->withMethod('GET')
                ->expectRouteName('list_estimates')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/estimates')),
        );
    }

    public function testRouteListInvoicesWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/invoices')
                ->withMethod('GET')
                ->expectRouteName('list_invoices')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/invoices')),
        );
    }

    public function testRouteNewInvoiceWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/invoices/new')
                ->withMethod('GET')
                ->expectRouteName('new_invoice')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/invoices/new')),
        );
    }

    public function testRouteViewInvoiceWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/invoices/view/1')
                ->withMethod('GET')
                ->expectRouteName('view_invoice')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/invoices/view/1')),
        );
    }

    public function testRouteEditInvoiceWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/invoices/edit/1')
                ->withMethod('GET')
                ->expectRouteName('edit_invoice')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/invoices/edit/1')),
        );
    }

    public function testRouteDeleteInvoiceWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/invoices/delete/1')
                ->withMethod('GET')
                ->expectRouteName('delete_invoice')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/invoices/delete/1')),
        );
    }

    public function testRouteGetInvoiceParticularsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/invoices/get-particulars')
                ->withMethod('GET')
                ->expectRouteName('get_invoice_particulars')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/invoices/get-particulars')),
        );
    }

    public function testRouteSendEmailInvoiceWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/invoices/send-email')
                ->withMethod('GET')
                ->expectRouteName('send_email_invoice')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/invoices/send-email')),
        );
    }

    public function testRoutePreviewInvoiceWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/invoices/preview')
                ->withMethod('GET')
                ->expectRouteName('preview_invoice')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/invoices/preview')),
        );
    }

    public function testRouteDownloadInvoiceWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/invoices/download')
                ->withMethod('GET')
                ->expectRouteName('download_invoice')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/invoices/download')),
        );
    }

    public function testRouteSetInvoiceAsQueriedWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/invoices/set-as-queried')
                ->withMethod('GET')
                ->expectRouteName('set_invoice_as_queried')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/invoices/set-as-queried')),
        );
    }

    public function testRouteSetInvoicesetRequestedOrderWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/invoices/requested-order')
                ->withMethod('GET')
                ->expectRouteName('set_invoiceset_requested_order')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/invoices/requested-order')),
        );
    }

    public function testRouteSetInvoiceAsPaidWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/invoices/set-as-paid')
                ->withMethod('GET')
                ->expectRouteName('set_invoice_as_paid')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/invoices/set-as-paid')),
        );
    }

    public function testRouteSetInvoiceAsSentWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/invoices/set-as-sent')
                ->withMethod('GET')
                ->expectRouteName('set_invoice_as_sent')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/invoices/set-as-sent')),
        );
    }

    public function testRouteNewJobWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/new')
                ->withMethod('GET')
                ->expectRouteName('new_job')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/new')),
        );
    }

    public function testRouteListJobsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs')
                ->withMethod('GET')
                ->expectRouteName('list_jobs')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs')),
        );
    }

    public function testRouteEditJobWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/edit/1')
                ->withMethod('GET')
                ->expectRouteName('edit_job')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/edit/1')),
        );
    }

    public function testRouteViewJobWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/view/1')
                ->withMethod('GET')
                ->expectRouteName('view_job')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/view/1')),
        );
    }

    public function testRouteGetJobParticularsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/get-particulars')
                ->withMethod('GET')
                ->expectRouteName('get_job_particulars')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/get-particulars')),
        );
    }

    public function testRouteDeleteJobWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/delete/1')
                ->withMethod('GET')
                ->expectRouteName('delete_job')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/delete/1')),
        );
    }

    public function testRouteGetJobsForCustomerWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/customer')
                ->withMethod('GET')
                ->expectRouteName('get_jobs_for_customer')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/customer')),
        );
    }

    public function testRouteSearchByTruckWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/trucks')
                ->withMethod('GET')
                ->expectRouteName('search_by_truck')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/trucks')),
        );
    }

    public function testRouteAssignEngineerToJobWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/assign-engineer')
                ->withMethod('GET')
                ->expectRouteName('assign_engineer_to_job')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/assign-engineer')),
        );
    }

    public function testRouteLogWorkWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/log-work')
                ->withMethod('GET')
                ->expectRouteName('log_work')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/log-work')),
        );
    }

    public function testRouteEditWorkLogWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/log-work/edit/1')
                ->withMethod('GET')
                ->expectRouteName('edit_work_log')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/log-work/edit/1')),
        );
    }

    // TODO fixtures
    /*public function testRouteDeleteWorkLogWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/log-work/delete/1')
                ->withMethod('GET')
                ->expectRouteName('delete_work_log')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/log-work/delete/1')),
        );
    }*/

    public function testRouteWorkLogPartsReqCompleteWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/worklog/parts/complete')
                ->withMethod('GET')
                ->expectRouteName('work_log_parts_req_complete')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/worklog/parts/complete')),
        );
    }

    public function testRouteSignOffJobWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/sign-off')
                ->withMethod('GET')
                ->expectRouteName('sign_off_job')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/sign-off')),
        );
    }

    public function testRouteUnSignOffJobWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/un-sign-off')
                ->withMethod('GET')
                ->expectRouteName('un_sign_off_job')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/un-sign-off')),
        );
    }

    public function testRouteUndeleteJobWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/undelete')
                ->withMethod('GET')
                ->expectRouteName('undelete_job')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/undelete')),
        );
    }

    public function testRouteIsChargeableJobWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/is-chargeable/test')
                ->withMethod('GET')
                ->expectRouteName('is_chargeable_job')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/is-chargeable/test')),
        );
    }

    public function testRouteJobAssignStockWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/assign-stock')
                ->withMethod('GET')
                ->expectRouteName('job_assign_stock')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/assign-stock')),
        );
    }

    public function testRouteJobConfirmSpecialItemsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/confirm-special')
                ->withMethod('GET')
                ->expectRouteName('job_confirm_special_items')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/confirm-special')),
        );
    }

    public function testRoutePostponeJobWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/postpone')
                ->withMethod('GET')
                ->expectRouteName('postpone_job')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/postpone')),
        );
    }

    public function testRouteGetStockForJobWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/attachedstockitems')
                ->withMethod('GET')
                ->expectRouteName('get_stock_for_job')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/attachedstockitems')),
        );
    }

    public function testRouteReturnStockFromJobParticularsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/returnstock')
                ->withMethod('GET')
                ->expectRouteName('return_stock_from_job_particulars')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/returnstock')),
        );
    }

    public function testRouteReturnJobFromJobParticularsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/returnjob')
                ->withMethod('GET')
                ->expectRouteName('return_job_from_job_particulars')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/returnjob')),
        );
    }

    public function testRouteGetContactsForJobWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/get-contacts')
                ->withMethod('GET')
                ->expectRouteName('get_contacts_for_job')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/get-contacts')),
        );
    }

    public function testRouteJobAddImageWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/add-image')
                ->withMethod('GET')
                ->expectRouteName('job_add_image')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/add-image')),
        );
    }

    public function testRoutePossibleAdditionalWorkWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/complete/possible-additional-work')
                ->withMethod('GET')
                ->expectRouteName('possible_additional_work')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/complete/possible-additional-work')),
        );
    }

    public function testRouteSetStockUsedOnJobWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/set-used-stock')
                ->withMethod('GET')
                ->expectRouteName('set_stock_used_on_job')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/set-used-stock')),
        );
    }

    public function testRouteSetStockUsedOnJobLogWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/set-used-stock-on-job-log')
                ->withMethod('GET')
                ->expectRouteName('set_stock_used_on_job_log')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/set-used-stock-on-job-log')),
        );
    }

    public function testRouteOverrideJobStageWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/stage')
                ->withMethod('GET')
                ->expectRouteName('override_job_stage')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/stage')),
        );
    }

    public function testRouteGetJobsForTruckWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/get-jobs-for-truck')
                ->withMethod('GET')
                ->expectRouteName('get_jobs_for_truck')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/get-jobs-for-truck')),
        );
    }

    public function testRouteJobSignatureEmailRequestWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/1/signature-email')
                ->withMethod('GET')
                ->expectRouteName('job_signature_email_request')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/1/signature-email')),
        );
    }

    public function testRouteJobSignatureCustomerWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/1/signature-email/aaa033cf-d0ce-446b-bc4a-30bf19b3de1b')
                ->withMethod('GET')
                ->expectRouteName('job_signature_customer')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs/1/signature-email/aaa033cf-d0ce-446b-bc4a-30bf19b3de1b', true)),
        );
    }

    public function testRouteJobsUpdatedWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs-status-update')
                ->withMethod('GET')
                ->expectRouteName('jobs_updated')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/jobs-status-update')),
        );
    }

    public function testRouteNewLeadWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/new')
                ->withMethod('GET')
                ->expectRouteName('new_lead')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/new')),
        );
    }

    public function testRouteEditLeadWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/edit/1')
                ->withMethod('GET')
                ->expectRouteName('edit_lead')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/edit/1')),
        );
    }

    public function testRouteDeleteLeadWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/delete/1')
                ->withMethod('GET')
                ->expectRouteName('delete_lead')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/delete/1')),
        );
    }

    public function testRouteListLeadsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/1')
                ->withMethod('GET')
                ->expectRouteName('list_leads')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/1')),
        );

        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads')
                ->withMethod('GET')
                ->expectRouteName('list_leads')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads')),
        );
    }

    public function testRouteViewLeadWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/view/1')
                ->withMethod('GET')
                ->expectRouteName('view_lead')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/view/1')),
        );
    }

    public function testRouteNewNoteForLeadWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/note/new')
                ->withMethod('GET')
                ->expectRouteName('new_note_for_lead')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/note/new')),
        );
    }

    public function testRouteNewContactForLeadWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/contact/new')
                ->withMethod('GET')
                ->expectRouteName('new_contact_for_lead')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/contact/new')),
        );
    }

    public function testRouteNewReminderForLeadWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/reminder/new')
                ->withMethod('GET')
                ->expectRouteName('new_reminder_for_lead')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/reminder/new')),
        );
    }

    public function testRouteListFuturetargetsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/futureTargets/list')
                ->withMethod('GET')
                ->expectRouteName('list_futuretargets')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/futureTargets/list')),
        );
    }

    public function testRouteUpdateLeadStatusWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/status-update/1/test')
                ->withMethod('GET')
                ->expectRouteName('update_lead_status')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/status-update/1/test')),
        );
    }

    public function testRouteConvertSuccessfulLeadWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/convert-to-customer/1')
                ->withMethod('GET')
                ->expectRouteName('convert_successful_lead')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/convert-to-customer/1')),
        );
    }

    public function testRouteConvertUnsuccessfulLeadWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/convert-to-unsuccessful/1')
                ->withMethod('GET')
                ->expectRouteName('convert_unsuccessful_lead')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/convert-to-unsuccessful/1')),
        );
    }

    public function testRouteListUnsuccessfulLeadsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/list-unsuccessful')
                ->withMethod('GET')
                ->expectRouteName('list_unsuccessful_leads')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/list-unsuccessful')),
        );
    }

    public function testRouteListCasualHireLeadsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/casual-hire-leads/1')
                ->withMethod('GET')
                ->expectRouteName('list_casual_hire_leads')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/casual-hire-leads/1')),
        );
    }

    public function testRouteNewCasualHireLeadWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/casual-hire-lead/new')
                ->withMethod('GET')
                ->expectRouteName('new_casual_hire_lead')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/casual-hire-lead/new')),
        );
    }

    public function testRouteListPrimaryTargetsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/primary-targets/1')
                ->withMethod('GET')
                ->expectRouteName('list_primary_targets')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/primary-targets/1')),
        );
    }

    public function testRouteNewPrimaryTargetWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/primary-target/new')
                ->withMethod('POST')
                ->expectRouteName('new_primary_target')
                ->appendCallableExpectation($this->assertResponseIsOk('POST', '/leads/primary-target/new')),
        );
    }

    public function testRouteUpdateLeadPriorityWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/priority-update/1/test')
                ->withMethod('GET')
                ->expectRouteName('update_lead_priority')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/priority-update/1/test')),
        );
    }

    public function testRouteUpdateLeadTypeWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/type-update/1/test')
                ->withMethod('GET')
                ->expectRouteName('update_lead_type')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/type-update/1/test')),
        );
    }

    public function testRouteUpdateLeadValueWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/value-update/1/test')
                ->withMethod('GET')
                ->expectRouteName('update_lead_value')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/value-update/1/test')),
        );
    }

    public function testRouteUpdateLeadTargetTypeWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/target-type-update/1/test')
                ->withMethod('GET')
                ->expectRouteName('update_lead_target_type')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/target-type-update/1/test')),
        );
    }

    public function testRouteUpdateLeadAddressWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/address-update')
                ->withMethod('GET')
                ->expectRouteName('update_lead_address')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/address-update')),
        );
    }

    public function testRouteListSuccessfulLeadsAwaitingDeliveryWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/list-successful-awaiting-delivery')
                ->withMethod('GET')
                ->expectRouteName('list_successful_leads_awaiting_delivery')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/list-successful-awaiting-delivery')),
        );
    }

    public function testRouteLeadDeliveredToCustomerWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/delivered-to-customer')
                ->withMethod('GET')
                ->expectRouteName('lead_delivered_to_customer')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/delivered-to-customer')),
        );
    }

    public function testRouteUpdateLeadDeliveryDateWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/delivery-date-update')
                ->withMethod('GET')
                ->expectRouteName('update_lead_delivery_date')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/delivery-date-update')),
        );
    }

    public function testRouteUpdateLeadContractRenewalDateWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/contract-renewal-date-update')
                ->withMethod('GET')
                ->expectRouteName('update_lead_contract_renewal_date')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/contract-renewal-date-update')),
        );
    }

    public function testRouteListServicePartsLeadsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/list-service-parts-leads')
                ->withMethod('GET')
                ->expectRouteName('list_service_parts_leads')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/list-service-parts-leads')),
        );
    }

    public function testRouteListVisitMeLeadsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/visit-me/list')
                ->withMethod('GET')
                ->expectRouteName('list_visit_me_leads')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/visit-me/list')),
        );
    }

    public function testRouteLeadSetVisitMeWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/visit-me/set')
                ->withMethod('GET')
                ->expectRouteName('lead_set_visit_me')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/visit-me/set')),
        );
    }

    public function testRouteLeadRemoveVisitMeWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/visit-me/remove')
                ->withMethod('GET')
                ->expectRouteName('lead_remove_visit_me')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/visit-me/remove')),
        );
    }

    public function testRouteListArchivedLeadsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/archived/list')
                ->withMethod('GET')
                ->expectRouteName('list_archived_leads')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/archived/list')),
        );
    }

    public function testRouteArchiveLeadWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/archive')
                ->withMethod('GET')
                ->expectRouteName('archive_lead')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/archive')),
        );
    }

    public function testRouteReinstateLeadWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/leads/reinstate')
                ->withMethod('GET')
                ->expectRouteName('reinstate_lead')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/leads/reinstate')),
        );
    }

    public function testRouteNewStockLocationWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/locations/new')
                ->withMethod('GET')
                ->expectRouteName('new_stock_location')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/locations/new')),
        );
    }

    public function testRouteEditStockLocationWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/locations/edit/1')
                ->withMethod('GET')
                ->expectRouteName('edit_stock_location')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/locations/edit/1')),
        );
    }

    public function testRouteViewStockLocationWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/locations/view/1')
                ->withMethod('GET')
                ->expectRouteName('view_stock_location')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/locations/view/1')),
        );
    }

    public function testRouteGetStockLocationParticularsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/locations/get-particulars/1')
                ->withMethod('GET')
                ->expectRouteName('get_stock_location_particulars')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/locations/get-particulars/1')),
        );
    }

    // TODO fixtures
    /*public function testRouteDeleteStockLocationWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/locations/delete/1')
                ->withMethod('GET')
                ->expectRouteName('delete_stock_location')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/locations/delete/1')),
        );
    }*/

    public function testRouteStockGetQuantityForLocationWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/locations/get-stock-quantity')
                ->withMethod('GET')
                ->expectRouteName('stock_get_quantity_for_location')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/locations/get-stock-quantity')),
        );
    }

    public function testRouteSupplierStockGetLocationsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/locations/get-stock-locations')
                ->withMethod('GET')
                ->expectRouteName('supplier_stock_get_locations')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/locations/get-stock-locations')),
        );
    }

    public function testRouteNewNoteWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/notes/new')
                ->withMethod('GET')
                ->expectRouteName('new_note')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/notes/new')),
        );
    }

    public function testRouteViewNoteWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/notes/view/1')
                ->withMethod('GET')
                ->expectRouteName('view_note')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/notes/view/1')),
        );
    }

    public function testRouteEditNoteWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/notes/edit/1')
                ->withMethod('GET')
                ->expectRouteName('edit_note')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/notes/edit/1')),
        );
    }

    public function testRouteDeleteNoteWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/notes/delete/1')
                ->withMethod('GET')
                ->expectRouteName('delete_note')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/notes/delete/1')),
        );
    }

    public function testRouteListPurchaseordersWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/purchaseorders')
                ->withMethod('GET')
                ->expectRouteName('list_purchaseorders')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/purchaseorders')),
        );
    }

    public function testRouteNewPurchaseorderWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/purchaseorders/new')
                ->withMethod('GET')
                ->expectRouteName('new_purchaseorder')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/purchaseorders/new')),
        );
    }

    public function testRouteViewPurchaseorderWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/purchaseorders/view/1')
                ->withMethod('GET')
                ->expectRouteName('view_purchaseorder')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/purchaseorders/view/1')),
        );
    }

    public function testRouteEditPurchaseorderWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/purchaseorders/edit/1')
                ->withMethod('GET')
                ->expectRouteName('edit_purchaseorder')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/purchaseorders/edit/1')),
        );
    }

    public function testRouteDeletePurchaseorderWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/purchaseorders/delete/1')
                ->withMethod('GET')
                ->expectRouteName('delete_purchaseorder')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/purchaseorders/delete/1')),
        );
    }

    public function testRouteDeletePurchaseorderitemWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/purchaseorders/item/delete/1')
                ->withMethod('GET')
                ->expectRouteName('delete_purchaseorderitem')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/purchaseorders/item/delete/1')),
        );
    }

    public function testRouteDeletePurchaseorderSpecialItemWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/purchaseorders/specil-item/delete/1')
                ->withMethod('GET')
                ->expectRouteName('delete_purchaseorder_special_item')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/purchaseorders/specil-item/delete/1')),
        );
    }

    // TODO fixtures
    /*public function testRouteGetPurchaseorderParticularsWithMethodGet(): void
    {
        $this->runFunctionalTestWithUser(
            FunctionalTestData::withUrl('/purchaseorders/get-particulars/1')
                ->withMethod('GET')
                ->expectRouteName('get_purchaseorder_particulars')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/purchaseorders/get-particulars/1')),
        );
    }*/

    public function testRouteCheckInItemArrivalWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/purchaseorders/purchaseorderitem/arrival/1')
                ->withMethod('GET')
                ->expectRouteName('check_in_item_arrival')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/purchaseorders/purchaseorderitem/arrival/1')),
        );
    }

    public function testRouteCheckInSpecialItemArrivalWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/purchaseorders/specialitem/arrival/1')
                ->withMethod('GET')
                ->expectRouteName('check_in_special_item_arrival')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/purchaseorders/specialitem/arrival/1')),
        );
    }

    public function testRouteSetPurchaseorderOrderDateWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/purchaseorders/order-date/1')
                ->withMethod('GET')
                ->expectRouteName('set_purchaseorder_order_date')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/purchaseorders/order-date/1')),
        );
    }

    public function testRoutePrintPurchaseOrderWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/purchaseorders/print')
                ->withMethod('GET')
                ->expectRouteName('print_purchase_order')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/purchaseorders/print')),
        );
    }

    public function testRouteArchivePurchaseorderWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/purchaseorders/archive/1')
                ->withMethod('GET')
                ->expectRouteName('archive_purchaseorder')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/purchaseorders/archive/1')),
        );
    }

    public function testRouteReinstatePurchaseorderWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/purchaseorders/reinstate/1')
                ->withMethod('GET')
                ->expectRouteName('reinstate_purchaseorder')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/purchaseorders/reinstate/1')),
        );
    }

    public function testRouteListArchivedPurchaseOrdersWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/purchaseorders/list-archived/')
                ->withMethod('GET')
                ->expectRouteName('list_archived_purchase_orders')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/purchaseorders/list-archived/')),
        );
    }

    // TODO fix the test
    /*public function testRoutePurchaseorderArchiveSearchWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/purchaseorders/archive-search')
                ->withMethod('GET')
                ->expectRouteName('purchaseorder_archive_search')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/purchaseorders/archive-search')),
        );
    }*/

    public function testRouteCheckInAllPoItemsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/purchaseorders/check-in-all-items')
                ->withMethod('GET')
                ->expectRouteName('check_in_all_po_items')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/purchaseorders/check-in-all-items')),
        );
    }

    // TODO fixtures
    /*public function testRouteNewStockForPurchaseOrderWithMethodGet(): void
    {
        $this->runFunctionalTestWithUser(
            FunctionalTestData::withUrl('/purchaseorders/stock/new/test')
                ->withMethod('GET')
                ->expectRouteName('new_stock_for_purchase_order')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/purchaseorders/stock/new/test')),
        );
    }*/

    public function testRouteListRemindersWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/reminders/list')
                ->withMethod('GET')
                ->expectRouteName('list_reminders')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/reminders/list')),
        );
    }

    public function testRouteNewReminderWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/reminders/new')
                ->withMethod('GET')
                ->expectRouteName('new_reminder')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/reminders/new')),
        );
    }

    public function testRouteViewReminderWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/reminders/view/1')
                ->withMethod('GET')
                ->expectRouteName('view_reminder')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/reminders/view/1')),
        );
    }

    public function testRouteEditReminderWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/reminders/edit/1')
                ->withMethod('GET')
                ->expectRouteName('edit_reminder')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/reminders/edit/1')),
        );
    }

    public function testRouteDeleteReminderWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/reminders/delete/1')
                ->withMethod('GET')
                ->expectRouteName('delete_reminder')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/reminders/delete/1')),
        );
    }

    public function testRouteGetReminderParticularsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/reminders/get-particulars')
                ->withMethod('GET')
                ->expectRouteName('get_reminder_particulars')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/reminders/get-particulars')),
        );
    }

    public function testRouteMarkReminderAsCompletedWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/reminders/mark-as-completed')
                ->withMethod('GET')
                ->expectRouteName('mark_reminder_as_completed')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/reminders/mark-as-completed')),
        );
    }

    public function testRouteReactivateReminderWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/reminders/reactivate')
                ->withMethod('GET')
                ->expectRouteName('reactivate_reminder')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/reminders/reactivate')),
        );
    }

    public function testRouteSectionReportsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/reports/list/1')
                ->withMethod('GET')
                ->expectRouteName('section_reports')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/reports/list/1')),
        );
    }

    public function testRouteJobCurrentMonthWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/reports/jobs/current-month')
                ->withMethod('GET')
                ->expectRouteName('job_current_month')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/reports/jobs/current-month')),
        );
    }

    public function testRouteJobsInVsOutWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/reports/jobs/in-vs-out')
                ->withMethod('GET')
                ->expectRouteName('jobs_in_vs_out')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/reports/jobs/in-vs-out/2000-01-01/2000-01-02')),
        );
    }

    public function testRouteJobPartsReturnedToVanStockWithMethodGet(): void
    {
        $this->runFunctionalTestWithUser(
            FunctionalTestData::withUrl('/reports/job/parts-returned-to-van-stock')
                ->withMethod('GET')
                ->expectRouteName('job_parts_returned_to_van_stock')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/reports/job/parts-returned-to-van-stock')),
        );
    }

    public function testRouteGetEngineerTimeLoggedWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/reports/job/engineer-time-logged')
                ->withMethod('GET')
                ->expectRouteName('get_engineer_time_logged')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/reports/job/engineer-time-logged')),
        );
    }

    public function testRouteLeadsInVsOutWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/reports/leads/in-vs-out')
                ->withMethod('GET')
                ->expectRouteName('leads_in_vs_out')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/reports/leads/in-vs-out')),
        );
    }

    public function testRouteGetAssetJobHistoryReportWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/reports/asset/job-history')
                ->withMethod('GET')
                ->expectRouteName('get_asset_job_history_report')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/reports/asset/job-history/1/1')),
        );
    }

    public function testRouteEstimatesInVsOutWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/reports/estimates/in-vs-out')
                ->withMethod('GET')
                ->expectRouteName('estimates_in_vs_out')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/reports/estimates/in-vs-out')),
        );
    }

    public function testRouteLeadsCreatedOrUpdatedWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/reports/leads/created-or-updated/01-01-2000/1')
                ->withMethod('GET')
                ->expectRouteName('leads_created_or_updated')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/reports/leads/created-or-updated/01-01-2000/1')),
        );
    }

    public function testRouteLeadsUpdatedBetweenWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/reports/leads/updated-between/01-01-2000/01-01-2001/1')
                ->withMethod('GET')
                ->expectRouteName('leads_updated_between')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/reports/leads/updated-between/01-01-2000/01-01-2001/1')),
        );
    }

    public function testRouteLeadsValueAtQuotingWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/reports/leads/quoting-value/1')
                ->withMethod('GET')
                ->expectRouteName('leads_value_at_quoting')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/reports/leads/quoting-value/1')),
        );
    }

    public function testRouteLeadsWithRemindersWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/reports/leads/with-reminders/1')
                ->withMethod('GET')
                ->expectRouteName('leads_with_reminders')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/reports/leads/with-reminders/1')),
        );
    }

    // TODO fixtures
    /*public function testRouteLeadsInactiveSinceWithMethodGet(): void
    {
        $this->runFunctionalTestWithUser(
            FunctionalTestData::withUrl('/reports/leads/inactive-since/1')
                ->withMethod('GET')
                ->expectRouteName('leads_inactive_since')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/reports/leads/inactive-since/1')),
        );
    }*/

    public function testRouteLeadsInVsLeadsInWithNoContactWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/reports/leads/in-vs-in-with-no-contact')
                ->withMethod('GET')
                ->expectRouteName('leads_in_vs_leads_in_with_no_contact')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/reports/leads/in-vs-in-with-no-contact')),
        );
    }

    public function testRouteAssetMaintenanceReportWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/reports/asset/maintenance')
                ->withMethod('GET')
                ->expectRouteName('asset_maintenance_report')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/reports/asset/maintenance')),
        );
    }

    public function testRouteGetTrucksNotLeasedOrSoldReportWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/reports/truck/not-leased-or-sold')
                ->withMethod('GET')
                ->expectRouteName('get_trucks_not_leased_or_sold_report')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/reports/truck/not-leased-or-sold')),
        );
    }

    public function testRouteGetTrucksServicesDueReportWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/reports/truck/services-due')
                ->withMethod('GET')
                ->expectRouteName('get_trucks_services_due_report')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/reports/truck/services-due')),
        );
    }

    public function testRouteGetStockLocationReportWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/reports/stock-location/1')
                ->withMethod('GET')
                ->expectRouteName('get_stock_location_report')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/reports/stock-location/1')),
        );
    }

    public function testRouteLoginCheckWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/login_check')
                ->withMethod('GET')
                ->expectRouteName('login_check')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/login_check')),
        );
    }

    public function testRouteListSourcesWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/sources')
                ->withMethod('GET')
                ->expectRouteName('list_sources')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/sources')),
        );
    }

    public function testRouteNewSourceWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/sources/new')
                ->withMethod('GET')
                ->expectRouteName('new_source')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/sources/new')),
        );
    }

    public function testRouteViewSourceWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/sources/view/1')
                ->withMethod('GET')
                ->expectRouteName('view_source')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/sources/view/1')),
        );
    }

    public function testRouteEditSourceWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/sources/edit/1')
                ->withMethod('GET')
                ->expectRouteName('edit_source')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/sources/edit/1')),
        );
    }

    public function testRouteDeleteSourceWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/sources/delete/1')
                ->withMethod('GET')
                ->expectRouteName('delete_source')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/sources/delete/1')),
        );
    }

    public function testRouteListStockWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/stock/list')
                ->withMethod('GET')
                ->expectRouteName('list_stock')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/stock/list')),
        );
    }

    public function testRouteListStockLocationsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/stock/list-locations')
                ->withMethod('GET')
                ->expectRouteName('list_stock_locations')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/stock/list-locations')),
        );
    }

    public function testRouteNewStockWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/stock/new')
                ->withMethod('GET')
                ->expectRouteName('new_stock')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/stock/new')),
        );
    }

    public function testRouteViewStockWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/stock/view/1')
                ->withMethod('GET')
                ->expectRouteName('view_stock')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/stock/view/1')),
        );
    }

    public function testRouteEditStockWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/stock/edit/1')
                ->withMethod('GET')
                ->expectRouteName('edit_stock')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/stock/edit/1')),
        );
    }

    public function testRouteDeleteStockWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/stock/delete/1')
                ->withMethod('GET')
                ->expectRouteName('delete_stock')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/stock/delete/1', true)),
        );
    }

    // TODO fixtures
    /*public function testRouteGetStockParticularsWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/stock/get-particulars/1/1')
                ->withMethod('GET')
                ->expectRouteName('get_stock_particulars')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/stock/get-particulars/1/1')),
        );
    }*/

    public function testRouteMoveStockWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/stock/move')
                ->withMethod('GET')
                ->expectRouteName('move_stock')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/stock/move')),
        );
    }

    public function testRouteReturnStockFromJobWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/stock/job-return')
                ->withMethod('GET')
                ->expectRouteName('return_stock_from_job')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/stock/job-return')),
        );
    }

    public function testRouteReturnPartWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/stock/part-return')
                ->withMethod('GET')
                ->expectRouteName('return_part')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/stock/part-return')),
        );
    }

    public function testRouteAddSupplierStockWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/stock/supplierStock/add')
                ->withMethod('GET')
                ->expectRouteName('add_supplier_stock')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/stock/supplierStock/add')),
        );
    }

    public function testRouteEditSupplierStockWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/stock/supplierStock/edit/1')
                ->withMethod('GET')
                ->expectRouteName('edit_supplier_stock')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/stock/supplierStock/edit/1')),
        );
    }

    // TODO fixtures
    /*public function testRouteDeleteSupplierStockWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/stock/supplierStock/delete/1')
                ->withMethod('GET')
                ->expectRouteName('delete_supplier_stock')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/stock/supplierStock/delete/1')),
        );
    }*/

    public function testRouteViewSupplierStockWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/stock/supplierstock/view/1')
                ->withMethod('GET')
                ->expectRouteName('view_supplier_stock')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/stock/supplierstock/view/1')),
        );
    }

    public function testRouteStockIssueToJobWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/stock/issue/test')
                ->withMethod('GET')
                ->expectRouteName('stock_issue_to_job')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/stock/issue/test')),
        );
    }

    public function testRouteStockLevelManualAdjustmentWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/stock/adjust-level/tesst')
                ->withMethod('GET')
                ->expectRouteName('stock_level_manual_adjustment')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/stock/adjust-level/test')),
        );
    }

    public function testRouteStockReplacePartWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/stock/replace-part')
                ->withMethod('GET')
                ->expectRouteName('stock_replace_part')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/stock/replace-part')),
        );
    }

    // TODO fixtures
    /*public function testRouteStockSetBinWithMethodGet(): void
    {
        $this->runFunctionalTestWithUser(
            FunctionalTestData::withUrl('/stock/bin/1')
                ->withMethod('GET')
                ->expectRouteName('stock_set_bin')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/stock/bin/1')),
        );
    }*/

    // TODO POST with json
    /*public function testRouteSupplierStockSearchWithMethodGet(): void
    {
        $this->runFunctionalTestWithUser(
            FunctionalTestData::withUrl('/stock/supplier-stock-search')
                ->withMethod('GET')
                ->expectRouteName('supplier_stock_search')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/stock/supplier-stock-search')),
        );
    }

    public function testRouteStockItemSearchWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/stock/stock-item-search')
                ->withMethod('GET')
                ->expectRouteName('stock_item_search')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/stock/stock-item-search')),
        );
    }

    public function testRouteStockCatalogueSearchWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/stock/stock-catalogue-search')
                ->withMethod('GET')
                ->expectRouteName('stock_catalogue_search')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/stock/stock-catalogue-search')),
        );
    }*/

    public function testRouteStockChoosePreferredSupplierWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/stock/choose-preferred-supplier')
                ->withMethod('GET')
                ->expectRouteName('stock_choose_preferred_supplier')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/stock/choose-preferred-supplier')),
        );
    }

    public function testRouteCountStockItemsByLocationWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/stock/count-stock-items-by-location')
                ->withMethod('GET')
                ->expectRouteName('count_stock_items_by_location')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/stock/count-stock-items-by-location')),
        );
    }

    public function testRouteSelectStockLocationWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/stock/select-stock-location/1')
                ->withMethod('GET')
                ->expectRouteName('select_stock_location')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/stock/select-stock-location/1')),
        );
    }

    public function testRouteNewSupplierWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/suppliers/new')
                ->withMethod('GET')
                ->expectRouteName('new_supplier')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/suppliers/new')),
        );
    }

    public function testRouteViewSupplierWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/suppliers/view/1')
                ->withMethod('GET')
                ->expectRouteName('view_supplier')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/suppliers/view/1')),
        );
    }

    public function testRouteEditSupplierWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/suppliers/edit/1')
                ->withMethod('GET')
                ->expectRouteName('edit_supplier')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/suppliers/edit/1')),
        );
    }

    public function testRouteDeleteSupplierWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/suppliers/delete/1')
                ->withMethod('GET')
                ->expectRouteName('delete_supplier')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/suppliers/delete/1', true)),
        );
    }

    public function testRouteGetContactsForSupplierWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/suppliers/get-contacts')
                ->withMethod('GET')
                ->expectRouteName('get_contacts_for_supplier')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/suppliers/get-contacts')),
        );
    }

    public function testRoutePrintOutstandingOrdersWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/suppliers/print-outstanding-orders')
                ->withMethod('GET')
                ->expectRouteName('print_outstanding_orders')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/suppliers/print-outstanding-orders')),
        );
    }

    public function testRouteListSuppliersWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/suppliers')
                ->withMethod('GET')
                ->expectRouteName('list_suppliers')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/suppliers')),
        );
    }

    public function testRouteNewStockIssuePriceRuleWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/suppliers/stock-issue-price-rule/new')
                ->withMethod('GET')
                ->expectRouteName('new_stock_issue_price_rule')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/suppliers/stock-issue-price-rule/new')),
        );
    }

    public function testRouteEditStockIssuePriceRuleWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/suppliers/stock-issue-price-rule/edit/1')
                ->withMethod('GET')
                ->expectRouteName('edit_stock_issue_price_rule')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/suppliers/stock-issue-price-rule/edit/1')),
        );
    }

    public function testRouteDeleteStockIssuePriceRuleWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/suppliers/stock-issue-price-rule/delete/1')
                ->withMethod('GET')
                ->expectRouteName('delete_stock_issue_price_rule')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/suppliers/stock-issue-price-rule/delete/1')),
        );
    }

    public function testRouteNewUserWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/users/new')
                ->withMethod('GET')
                ->expectRouteName('new_user')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/users/new')),
        );
    }

    public function testRouteEditUserWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/users/edit/1')
                ->withMethod('GET')
                ->expectRouteName('edit_user')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/users/edit/1')),
        );
    }

    public function testRouteViewUserWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/users/view/1')
                ->withMethod('GET')
                ->expectRouteName('view_user')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/users/view/1')),
        );
    }

    public function testRouteDeleteUserWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/users/delete/1')
                ->withMethod('GET')
                ->expectRouteName('delete_user')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/users/delete/1', true)),
        );
    }

    public function testRouteResetPasswordEmailWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/users/password-reset-email/1')
                ->withMethod('GET')
                ->expectRouteName('reset_password_email')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/users/password-reset-email/1', true)),
        );
    }

    public function testRouteResetPasswordWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/users/password-reset/1')
                ->withMethod('GET')
                ->expectRouteName('reset_password')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/users/password-reset/1')),
        );
    }

    public function testRouteSetPasswordWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/users/password-set/1')
                ->withMethod('GET')
                ->expectRouteName('set_password')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/users/password-set/1')),
        );
    }

    public function testRouteListUsersWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/users')
                ->withMethod('GET')
                ->expectRouteName('list_users')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/users')),
        );
    }

    public function testRouteIndexWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/')
                ->withMethod('GET')
                ->expectRouteName('index')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/')),
        );
    }

    public function testRouteSearchWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/find')
                ->withMethod('GET')
                ->expectRouteName('search')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/find')),
        );
    }

    public function testRouteLogoutWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/logout')
                ->withMethod('GET')
                ->expectRouteName('logout')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/logout', true)),
        );
    }

    public function testRouteLoginWithMethodGet(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/login')
                ->withMethod('GET')
                ->expectRouteName('login')
                ->appendCallableExpectation($this->assertResponseIsOk('GET', '/login')),
        );
    }

    // TODO: Fix this test
    /* public function testRouteJobSignatureWithMethodPost(): void
    {
        $this->runFunctionalTest(
            FunctionalTestData::withUrl('/jobs/1/signature')
                ->withMethod('POST')
                ->expectRouteName('job_signature')
                ->appendCallableExpectation($this->assertStatusCodeLessThan500('POST', '/jobs/1/signature'))
        );
    } */

    protected function runFunctionalTestWithUser(FunctionalTestData $testData, bool $bootKernel = false): void
    {
        $this->runFunctionalTest(
            $testData->withCallbackBeforeRequest(function (): void {
                $userRepository = static::getContainer()->get(UserRepository::class);

                // retrieve the test user
                $testUser = $userRepository->findOneByUsername('megaadmin');

                // simulate $testUser being logged in
                $this->getHttpClientInternal()->loginUser($testUser, 'secured_area');
            }),
        );
    }

    private function assertResponseIsOk(string $method, string $url, bool $allow302 = false): \Closure
    {
        return function (KernelBrowser $browser) use ($method, $url, $allow302): void {
            $statusCode = $browser->getResponse()->getStatusCode();
            $routeName = $browser->getRequest()->attributes->get('_route', 'unknown');

            static::assertTrue(
                ($statusCode >= 200 && $statusCode < 300) || ($statusCode >= 400 && $statusCode < 500) || ($allow302 && $statusCode === 302),
                sprintf('Request "%s %s" for %s route returned an internal error (%s).', $method, $url, $routeName, $statusCode),
            );
        };
    }
}
