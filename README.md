Dawson
========================

**Installation**

Clone the repository

```
git clone git@github.com:Velocity42/dawson.git
```

If you don't have PHP installed on your host machine, you can download it from (Windows) https://windows.php.net/download/#php-7.4-ts-vc15-x64. Extract this to c:\PHP and move php.ini-development to php.ini, open this file and uncomment `;extension=openssl`.

Move into the directory and then install composer dependencies
```
cd dawson
```
As Symfony uses Composer to manage its dependencies, the recommended way to create a new project is to use it.

If you don't have Composer yet, download it following the instructions on http://getcomposer.org/ or just run the following command:
```
curl -s http://getcomposer.org/installer | php

```
then
```
php composer.phar install --prefer-dist --no-scripts --no-progress --no-interaction --ignore-platform-reqs
```
Copy the template parameters file to .env.local and populate
```
cp .env .env.local
```

Configure permissions
```
sudo chown -R $USER ./
sudo chmod 777 var/cache var/log -R
```
Build the docker image
```
docker-compose up --build
```
Initial setup (first run after building)

- Access the container command line using `docker exec -it dawson_web_1 sh`
- run `bin/db_reset.sh`

**Accessing**

The site should now be available at `http://localhost`.

Additionally you can add `dawson.local` to your OS hosts file (`c:\Windows\system32\drivers\etc\hosts` on Windows, `/etc/hosts` on mac/linux) - `127.0.0.1 dawson.local` and access the site at `http://dawson.local`.

**Composer**
If you find you need to manage composer deps after the initial install, issue any composer commands from inside the container via `docker exec -it dawson_web_1 bash`

**Tests**

```vendor/bin/phpunit```

**Quality Tools**

```tools/php-cs-fixer/vendor/bin/php-cs-fixer fix```

```vendor/bin/rector process --clear-cache```
